﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DarkShadowSmoother : MonoBehaviour
{
    RenderTexture _rt;
    private void OnRenderImage(RenderTexture input, RenderTexture output)
    {
        // 내가 추가한
        int w = Camera.main.pixelWidth;
        int h = Camera.main.pixelHeight;
        if (!_rt) _rt = RenderTexture.GetTemporary(w >> 2, h >> 2, 0);
        if(_rt && _rt.width != w){
            _rt = RenderTexture.GetTemporary(w >> 2, h >> 2, 0);
        }
        Graphics.Blit(input, output);

        //RenderTexture.ReleaseTemporary(_rt);
    }
}
