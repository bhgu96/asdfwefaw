﻿//using System.Collections;
//using System.Collections.Generic;
//using UnityEngine;
//using UnityEngine.UI;


//public class Product : MonoBehaviour
//{
//    public bool IsSold { get { return mIsDeactivating; } }
//    public bool CanBuy
//    {
//        get
//        {
//            return mPrice <= GoldAccessor_Fairy.Gold;
//        }
//    }

//    [SerializeField]
//    private EItemFlags mItemFlag;
//    [SerializeField]
//    private int mPrice;
//    [SerializeField]
//    private Text mPriceText;
//    [SerializeField]
//    private SpriteRenderer[] mSpriteRenderers;
//    private Collider2D mCollider;

//    private float mDeactivatingRemainTime;
//    private const float DEACTIVATING_TIME = 1.0F;
//    private bool mIsDeactivating = false;

//    public Item Buy(Wallet wallet)
//    {
//        if (wallet.Gold - mPrice >= 0)
//        {
//            wallet.Gold -= mPrice;
//            mDeactivatingRemainTime = DEACTIVATING_TIME;
//            mIsDeactivating = true;
//            mCollider.enabled = false;
//            mSpriteRenderers[mSpriteRenderers.Length - 1].gameObject.SetActive(false);
//            Item product = ItemPools.Get(mItemFlag);
//            product.transform.position = transform.position;
//            return product;
//        }
//        return null;
//    }

//    protected virtual void Awake()
//    {
//        mCollider = GetComponent<Collider2D>();
//        mPriceText.text = mPrice.ToString();
//    }

//    protected virtual void Update()
//    {
//        if(mIsDeactivating)
//        {
//            if(mDeactivatingRemainTime > 0.0f)
//            {
//                mDeactivatingRemainTime -= Time.deltaTime;
//                float deltaAlpha = Mathf.Lerp(0.0f, 1.0f, mDeactivatingRemainTime / DEACTIVATING_TIME);
//                for (int i=0; i<mSpriteRenderers.Length - 1; i++)
//                {
//                    mSpriteRenderers[i].color = new Color(1.0f, 1.0f, 1.0f, deltaAlpha);
//                }
//                mPriceText.color = new Color(1.0f, 1.0f, 1.0f, deltaAlpha);
//            }
//            else
//            {
//                mIsDeactivating = false;
//                for (int i = 0; i < mSpriteRenderers.Length - 1; i++)
//                {
//                    mSpriteRenderers[i].color = new Color(1.0f, 1.0f, 1.0f, 0.0F);
//                }
//                mPriceText.color = new Color(1.0f, 1.0f, 1.0f, 0.0f);
//                gameObject.SetActive(false);
//            }
//        }
//        else
//        {
//            if(mPrice > GoldAccessor_Fairy.Gold)
//            {
//                mPriceText.color = new Color(0.5f, 0.5f, 0.5f, 0.6f);
//            }
//            else
//            {
//                mPriceText.color = new Color(1.0f, 1.0f, 1.0f, 1.0f);
//            }
//        }
//    }
//}
