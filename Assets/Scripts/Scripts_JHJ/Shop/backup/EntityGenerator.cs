﻿//using System.Collections;
//using System.Collections.Generic;
//using UnityEngine;

//[System.Serializable]
//public struct EntityPattern
//{
//    public EntityInfo[] SpawnInfo;
//    public EHorizontalDirection Direction;
//}

//[System.Serializable]
//public struct EntityInfo
//{
//    public GameObject GameObject;
//    [Range(0.0f, 1.0f)]
//    public float Possibility;
//}

//public class EntityGenerator : MonoBehaviour
//{
//    [SerializeField]
//    private float mDistanceOfEntities;

//    [SerializeField]
//    private Transform mEndOfGen;

//    [SerializeField]
//    private float mRadiusOfGizmos;

//    [SerializeField]
//    private EntityPattern[] mPatterns;

//    private void OnDrawGizmos()
//    {
//        if (mEndOfGen != null && mDistanceOfEntities > 0.0f)
//        {
//            Gizmos.color = new Color(1.0f, 0.0f, 0.8f, 0.8f);

//            Vector2 startPosition = transform.position;
//            Gizmos.DrawSphere(startPosition, mRadiusOfGizmos);

//            Vector2 endPosition = mEndOfGen.position;
//            float distanceFromStartToEnd = Vector2.Distance(startPosition, endPosition);

//            Vector2 spawnVector = endPosition - startPosition;
//            spawnVector.Normalize();
//            float velocity_x = spawnVector.x * mDistanceOfEntities;
//            float velocity_y = spawnVector.y * mDistanceOfEntities;

//            Vector2 spawnPosition = new Vector2(startPosition.x + velocity_x, startPosition.y + velocity_y);
//            float sumOfDistance = mDistanceOfEntities;

//            while (sumOfDistance < distanceFromStartToEnd)
//            {
//                Gizmos.DrawSphere(spawnPosition, mRadiusOfGizmos);
//                spawnPosition = new Vector2(spawnPosition.x + velocity_x, spawnPosition.y + velocity_y);
//                sumOfDistance += mDistanceOfEntities;
//            }
//        }
//    }

//    private void Awake()
//    {
//        Vector2 startPosition = transform.position;
//        Generate(0, startPosition);
//        Vector2 endPosition = mEndOfGen.position;
//        float distanceFromStartToEnd = Vector2.Distance(startPosition, endPosition);

//        Vector2 spawnVector = endPosition - startPosition;
//        spawnVector.Normalize();
//        float velocity_x = spawnVector.x * mDistanceOfEntities;
//        float velocity_y = spawnVector.y * mDistanceOfEntities;

//        Vector2 spawnPosition = new Vector2(startPosition.x + velocity_x, startPosition.y + velocity_y);
//        float sumOfDistance = mDistanceOfEntities;
//        int level = 1;
//        while (sumOfDistance < distanceFromStartToEnd)
//        {
//            Generate(level, spawnPosition);
//            spawnPosition = new Vector2(spawnPosition.x + velocity_x, spawnPosition.y + velocity_y);
//            sumOfDistance += mDistanceOfEntities;
//            level++;
//        }
//    }

//    private void Generate(int level, Vector2 position)
//    {
//        if (level >= mPatterns.Length)
//        {
//            level = mPatterns.Length - 1;
//        }

//        float possibility = Random.Range(0.0f, 1.0f);
//        float sumOfPossibility = 0.0f;

//        for (int i = 0; i < mPatterns[level].SpawnInfo.Length; i++)
//        {
//            sumOfPossibility += mPatterns[level].SpawnInfo[i].Possibility;
//            if (possibility < sumOfPossibility)
//            {
//                GameObject instObject = Instantiate(mPatterns[level].SpawnInfo[i].GameObject, position, Quaternion.identity, transform);
//                instObject.transform.localScale = new Vector3((float)mPatterns[level].Direction, 1.0f, 1.0f);
//                return;
//            }
//        }
//    }
//}
