﻿using UnityEngine;
using System.Collections;

public class ParallexLayer : MonoBehaviour
{
    public Vector2 AnchorMin { get { return mAnchorMin; } }
    public Vector2 AnchorMax { get { return mAnchorMax; } }
    public Vector2 AnchorCenter { get { return mAnchorCenter; } }
    public float HorizontalSize { get { return mHorizontalSize; } }
    public float VerticalSize { get { return mVerticalSize; } }

    [SerializeField]
    private Transform mAnchorMaxTransform;
    [SerializeField]
    private Transform mAnchorMinTransform;
    private Vector2 mAnchorMin;
    private Vector2 mAnchorMax;
    private Vector2 mAnchorCenter;
    private float mHorizontalSize;
    private float mVerticalSize;
    private ParallexBackground mBackground;

    public void SetCameraController(CameraController cameraController)
    {
        mAnchorMin = new Vector2(mAnchorMin.x + cameraController.HorizontalSize, mAnchorMin.y + cameraController.VerticalSize);
        mAnchorMax = new Vector2(mAnchorMax.x - cameraController.HorizontalSize, mAnchorMax.y - cameraController.VerticalSize);
        mAnchorCenter = new Vector2((mAnchorMin.x + mAnchorMax.x) * 0.5f, (mAnchorMin.y + mAnchorMax.y) * 0.5f);
        mHorizontalSize = mAnchorMax.x - mAnchorMin.x;
        mVerticalSize = mAnchorMax.y - mAnchorMin.y;
    }

    private void OnDrawGizmos()
    {
        if (mAnchorMaxTransform != null && mAnchorMinTransform != null)
        {
            Gizmos.color = new Color(0.0f, 0.0f, 1.0f, 0.2f);
            Vector2 anchorMinPosition = mAnchorMinTransform.position;
            Vector2 anchorMaxPosition = mAnchorMaxTransform.position;
            Vector2 anchorCenterPosition = new Vector2((anchorMaxPosition.x + anchorMinPosition.x) * 0.5f, (anchorMaxPosition.y + anchorMinPosition.y) * 0.5f);

            Gizmos.DrawCube(anchorCenterPosition, new Vector3(anchorMaxPosition.x - anchorMinPosition.x, anchorMaxPosition.y - anchorMinPosition.y, 0.0f));
        }
    }

    private void Awake()
    {
        mBackground = GetComponentInParent<ParallexBackground>();
        mAnchorMin = mAnchorMinTransform.localPosition;
        mAnchorMax = mAnchorMaxTransform.localPosition;
    }
}
