﻿using UnityEngine;
using System.Collections;

/// <summary>
/// 뒷배경 패럴렉싱을 작동시키는 클래스로 CameraController에 의존적이다.
/// 패럴렉싱을 무한히 생성할 수 없는 단점이 있다
/// </summary>
public class ParallexBackground : MonoBehaviour
{
    [SerializeField]
    private bool mIsFixed;
    [SerializeField]
    private float mFixedY;
    [SerializeField]
    private float mFixedLayerY;

    public float MapHorizontalSize { get { return mMapHorizontalSize; } }
    public float MapVerticalSize { get { return mMapVerticalSize; } }
    public Vector2 MapAnchorMin
    {
        get { return mMapAnchorMin; }
    }
    public Vector2 MapAnchorMax
    {
        get { return mMapAnchorMax; }
    }
    public Vector2 MapAnchorCenter
    {
        get
        {
            return mMapAnchorCenter;
        }
    }

    [SerializeField]
    private Vector2 mMapAnchorMin;
    [SerializeField]
    private Vector2 mMapAnchorMax;

    private Vector2 mMapAnchorCenter;

    private CameraController mCameraController;
    public CameraController CameraController
    {
        get
        {
            return mCameraController;
        }
        set
        {
            mCameraController = value;
            for(int i=0; i<mLayers.Length; i++)
            {
                mLayers[i].SetCameraController(mCameraController);
            }
        }
    }

    private ParallexLayer[] mLayers;
    private float mMapHorizontalSize;
    private float mMapVerticalSize;

    private void Awake()
    {
        mLayers = GetComponentsInChildren<ParallexLayer>(true);
        mMapAnchorCenter = new Vector2((mMapAnchorMin.x + mMapAnchorMax.x) * 0.5f, (mMapAnchorMin.y + mMapAnchorMax.y) * 0.5f);
        mMapHorizontalSize = mMapAnchorMax.x - mMapAnchorMin.x;
        mMapVerticalSize = mMapAnchorMax.y - mMapAnchorMin.y;
    }

    private void Start()
    {
        CameraController = CameraController.Main;
    }

    private bool m_isFixedUpdated = false;
    private void FixedUpdate()
    {
        m_isFixedUpdated = true;
    }

    private void Update()
    {
        if (m_isFixedUpdated)
        {
            m_isFixedUpdated = false;
            transform.position = mCameraController.transform.position;
            Vector2 myPosition = transform.position;
            Vector2 localPosition = new Vector2(myPosition.x - mMapAnchorCenter.x, myPosition.y - mMapAnchorCenter.y);

            if(!mIsFixed)
            {
                for (int i = 0; i < mLayers.Length; i++)
                {
                    Vector2 anchorCenter = mLayers[i].AnchorCenter;
                    Vector2 backgroundPosition = mLayers[i].transform.position;
                    mLayers[i].transform.localPosition = new Vector3(-(mLayers[i].HorizontalSize * localPosition.x) / mMapHorizontalSize - anchorCenter.x
                    , -(mLayers[i].VerticalSize * localPosition.y) / mMapVerticalSize - anchorCenter.y);
                }
            }
            else
            {
                this.transform.position = new Vector3(mCameraController.transform.position.x, mFixedY);
                for (int i = 0; i < mLayers.Length; i++)
                {
                    Vector2 anchorCenter = mLayers[i].AnchorCenter;
                    Vector2 backgroundPosition = mLayers[i].transform.position;
                    mLayers[i].transform.localPosition = new Vector3(-(mLayers[i].HorizontalSize * localPosition.x) / mMapHorizontalSize - anchorCenter.x
                    , mFixedLayerY);
                }
            }
        }
    }

    private void OnDrawGizmosSelected()
    {
        Gizmos.color = new Color(0.4f, 0.0f, 0.0f, 0.4f);
        Gizmos.DrawCube(new Vector3((mMapAnchorMin.x + mMapAnchorMax.x) * 0.5f, (mMapAnchorMin.y + mMapAnchorMax.y) * 0.5f)
            , new Vector3(mMapAnchorMax.x - mMapAnchorMin.x, mMapAnchorMax.y - mMapAnchorMin.y));
    }
}
