﻿using UnityEngine;
using System.Collections;

public class Skill_ElementalShooter_Monster_Bottom_Ranged_CastPrepare : State<EStateType_Skill_ElementalShooter_Monster_Bottom_Ranged>
{
    private Skill_ElementalShooter_Monster_Bottom_Ranged mSkill;
    public Skill_ElementalShooter_Monster_Bottom_Ranged_CastPrepare(Skill_ElementalShooter_Monster_Bottom_Ranged skill) : base(EStateType_Skill_ElementalShooter_Monster_Bottom_Ranged.CastPrepare)
    {
        mSkill = skill;
    }

    public override void Start()
    {
        mSkill.Animator.SetInteger("state", (int)Type);
        mSkill.Animator.speed = mSkill.CastSpeed;
    }

    public override void End()
    {
        mSkill.Animator.speed = 1.0f;
    }
}
