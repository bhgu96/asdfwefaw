﻿using UnityEngine;
using System.Collections;

public enum EStateType_Skill_ElementalShooter_Monster_Bottom_Ranged
{
    None = -1, 

    Idle = 0, 
    CastPrepare = 1, 
    Cast = 2, 
    CastEnd = 3, 

    Count
}

public enum EAudioType_Skill_ElementalShooter_Monster_Bottom_Ranged
{
    None = -1, 

    Count
}

[RequireComponent(typeof(AudioHandler_Skill_ElementalShooter_Monster_Bottom_Ranged))]
public class Skill_ElementalShooter_Monster_Bottom_Ranged : ASkill_Monster_Bottom_Ranged<EStateType_Skill_ElementalShooter_Monster_Bottom_Ranged>
{
    [SerializeField]
    private ESkillType_Monster_Bottom_Ranged mType;
    /// <summary>
    /// 스킬 타입
    /// </summary>
    public override ESkillType_Monster_Bottom_Ranged Type
    {
        get { return mType; }
    }

    [SerializeField]
    private EElementalType mElementalType;
    /// <summary>
    /// 속성 타입
    /// </summary>
    public EElementalType ElementalType
    {
        get { return mElementalType; }
    }

    /// <summary>
    /// 시전 시간
    /// </summary>
    public override float CastTime
    {
        get { return 1.0f; }
    }

    public float SkillOffense_Default
    {
        get { return Parameters[0]; }
    }
    public float SkillOffense_Increment
    {
        get;
    }
    public float SkillOffense_Increase
    {
        get;
    }
    /// <summary>
    /// 스킬 공격력
    /// </summary>
    public float SkillOffense
    {
        get { return (SkillOffense_Default + SkillOffense_Increment) * (1.0f + SkillOffense_Increase); }
    }

    public int CountOfBullet_Default
    {
        get { return (int)Parameters[1]; }
    }
    public int CountOfBullet_Increment
    {
        get;
    }
    /// <summary>
    /// 탄의 수
    /// </summary>
    public int CountOfBullet
    {
        get { return CountOfBullet_Default + CountOfBullet_Increment; }
    }

    public float SpeedOfBullet_Default
    {
        get { return Parameters[2]; }
    }
    public float SpeedOfBullet_Increase
    {
        get;
    }
    /// <summary>
    /// 탄의 속도
    /// </summary>
    public float SpeedOfBullet
    {
        get { return SpeedOfBullet_Default * (1.0f + SpeedOfBullet_Increase); }
    }

    [SerializeField]
    private Transform mFrontAim;
    public Transform FrontAim { get { return mFrontAim; } }

    public AudioHandler_Skill_ElementalShooter_Monster_Bottom_Ranged AudioHandler { get; private set; }

    public int RestOfBullets { get; private set; }

    /// <summary>
    /// 속성 탕 발사 메소드
    /// </summary>
    public void Fire()
    {
        if (RestOfBullets > 0)
        {
            Projectile_ElementalBullet bullet = null;

            if (ElementalType == EElementalType.Fire)
            {
                bullet = ProjectilePool.Get(EProjectileType.FireBullet) as Projectile_ElementalBullet;
            }
            else if(ElementalType == EElementalType.Wind)
            {
                bullet = ProjectilePool.Get(EProjectileType.WindBullet) as Projectile_ElementalBullet;
            }
            else if(ElementalType == EElementalType.Darkness)
            {
                bullet = ProjectilePool.Get(EProjectileType.DarknessBullet) as Projectile_ElementalBullet;
            }


            if (bullet != null)
            {
                bullet.Offense = User.Status.Offense * SkillOffense;
                bullet.Speed = SpeedOfBullet;
                bullet.Force = User.Status.Force;
                bullet.ElementalAffinity = User.Status.GetEA(ElementalType);
                bullet.Level = User.Status.Level;
                bullet.DamageType = EDamageType.Monster;
                bullet.CriticalChance = 0.0f;
                bullet.CriticalMag = 1.0f;

                Vector2 direction = mFrontAim.position - transform.position;

                bullet.SetDirection(direction, true);
                bullet.transform.position = transform.position;

                bullet.IsBasedOnGiant = true;

                bullet.Activate();
                RestOfBullets--;
            }
        }
    }

    protected override void OnActivate()
    {
        base.OnActivate();
        SetDisableCast();
        RestOfBullets = CountOfBullet;
        State = EStateType_Skill_ElementalShooter_Monster_Bottom_Ranged.CastPrepare;
        User.State = EStateType_Monster_Bottom_Ranged.CastPrepare;
    }

    protected override void OnDeactivate()
    {
        base.OnDeactivate();
        SetEnableCast();
        State = EStateType_Skill_ElementalShooter_Monster_Bottom_Ranged.CastEnd;
        User.State = EStateType_Monster_Bottom_Ranged.CastEnd;
    }

    protected override void Awake()
    {
        base.Awake();

        AudioHandler = GetComponent<AudioHandler_Skill_ElementalShooter_Monster_Bottom_Ranged>();

        SetStates(new Skill_ElementalShooter_Monster_Bottom_Ranged_Idle(this)
            , new Skill_ElementalShooter_Monster_Bottom_Ranged_CastPrepare(this)
            , new Skill_ElementalShooter_Monster_Bottom_Ranged_Cast(this)
            , new Skill_ElementalShooter_Monster_Bottom_Ranged_CastEnd(this));
    }
}
