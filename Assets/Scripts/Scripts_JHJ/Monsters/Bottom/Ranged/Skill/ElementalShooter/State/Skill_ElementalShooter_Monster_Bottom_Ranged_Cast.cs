﻿using UnityEngine;

public class Skill_ElementalShooter_Monster_Bottom_Ranged_Cast : State<EStateType_Skill_ElementalShooter_Monster_Bottom_Ranged>
{
    private Skill_ElementalShooter_Monster_Bottom_Ranged mSkill;

    public Skill_ElementalShooter_Monster_Bottom_Ranged_Cast(Skill_ElementalShooter_Monster_Bottom_Ranged skill) : base(EStateType_Skill_ElementalShooter_Monster_Bottom_Ranged.Cast)
    {
        mSkill = skill;
    }

    public override void Start()
    {
        mSkill.Animator.SetInteger("state", (int)Type);
        mSkill.Animator.speed = mSkill.CastSpeed;
        mSkill.User.State = EStateType_Monster_Bottom_Ranged.Cast;
    }

    public override void End()
    {
        mSkill.Animator.speed = 1.0f;
    }

    public override void Update()
    {
        if(mSkill.RestOfBullets <= 0)
        {
            mSkill.Deactivate();
        }
    }
}
