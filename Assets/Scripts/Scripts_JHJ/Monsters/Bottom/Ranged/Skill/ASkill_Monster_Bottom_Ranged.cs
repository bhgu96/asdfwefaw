﻿using UnityEngine;
using Object;
using System;
using System.Collections.Generic;

public enum ESkillType_Monster_Bottom_Ranged
{
    None = -1, 

    FireShooter = 0, 
    WindShooter = 1, 
    DarknessShooter = 2, 

    Count
}

public interface ISkill_Monster_Bottom_Ranged 
    : ISkill_Monster_Bottom, IHandleable<ESkillType_Monster_Bottom_Ranged>
{

}

public abstract class ASkill_Monster_Bottom_Ranged<EState> 
    : ASkill_Monster_Bottom<Monster_Bottom_Ranged, EState>, ISkill_Monster_Bottom_Ranged
    where EState : Enum
{
    public abstract ESkillType_Monster_Bottom_Ranged Type { get; }

    public override string NameTag
    {
        get { return mInfo.NameTag; }
    }

    public override string DescriptionTag
    {
        get { return mInfo.DescriptionTag; }
    }

    public override string OptionTag
    {
        get { return mInfo.OptionTag; }
    }

    public override string AdditionalOptionTag
    {
        get { return mInfo.AdditionalOptionTag; }
    }

    public override float CoolTime_Default
    {
        get { return mInfo.CoolTime; }
    }

    public override float[] Parameters
    {
        get { return mInfo.Parameters; }
    }

    private SkillInfo_Monster_Bottom_Ranged mInfo;

    protected override void Awake()
    {
        base.Awake();

        mInfo = DataManager.GetSkillInfo(Type);
    }
}

public class SkillInfo_Monster_Bottom_Ranged 
{
    public string NameTag { get; }
    public string DescriptionTag { get; }
    public string OptionTag { get; }
    public string AdditionalOptionTag { get; }
    public float CoolTime { get; }
    public float[] Parameters { get; }

    public SkillInfo_Monster_Bottom_Ranged(Dictionary<string, string> skillDict)
    {
        NameTag = skillDict["Name Tag"];
        DescriptionTag = skillDict["Desc Tag"];
        OptionTag = skillDict["Option Tag"];
        AdditionalOptionTag = skillDict["Additional Tag"];
        CoolTime = float.Parse(skillDict["Cool Time"]);

        Parameters = new float[GameConstant.MAX_SKILL_PARAMETERS];

        for (int i = 0; i < GameConstant.MAX_SKILL_PARAMETERS; i++)
        {
            Parameters[i] = float.Parse(skillDict[i.ToString()]);
        }
    }
}