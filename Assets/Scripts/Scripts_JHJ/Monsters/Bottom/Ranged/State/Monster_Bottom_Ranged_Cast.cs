﻿using UnityEngine;

public class Monster_Bottom_Ranged_Cast : State<EStateType_Monster_Bottom_Ranged>
{
    private Monster_Bottom_Ranged mActor;

    public Monster_Bottom_Ranged_Cast(Monster_Bottom_Ranged actor) : base(EStateType_Monster_Bottom_Ranged.Cast)
    {
        mActor = actor;
    }

    public override void Start()
    {
        mActor.Animator.SetInteger("state", (int)Type);
        mActor.Animator.speed = mActor.Status.CastSpeed;
    }

    public override void End()
    {
        mActor.Animator.speed = 1.0f;
    }

    public override void FixedUpdate()
    {
        if (mActor.Input.MoveHorizontal > Mathf.Epsilon)
        {
            mActor.MoveHorizontal(-Giant_Player.Main.Status.MoveSpeed, EHorizontalDirection.Left);
        }
    }
}
