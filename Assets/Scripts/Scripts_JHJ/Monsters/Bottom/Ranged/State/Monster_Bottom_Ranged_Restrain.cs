﻿using UnityEngine;

public class Monster_Bottom_Ranged_Restrain : State<EStateType_Monster_Bottom_Ranged>
{
    private Monster_Bottom_Ranged mActor;

    public Monster_Bottom_Ranged_Restrain(Monster_Bottom_Ranged actor) : base(EStateType_Monster_Bottom_Ranged.Restrain)
    {
        mActor = actor;
    }

    public override void Start()
    {
        mActor.Animator.SetInteger("state", (int)Type);
    }

    public override void Update()
    {
        if(!mActor.DebuffHandler.Get(EDebuffType.Restrain_Bush).IsActivated)
        {
            mActor.State = EStateType_Monster_Bottom_Ranged.Idle;
        }
    }
}