﻿using UnityEngine;

public class Monster_Bottom_Ranged_Retreat : State<EStateType_Monster_Bottom_Ranged>
{
    private Monster_Bottom_Ranged mActor;

    public Monster_Bottom_Ranged_Retreat(Monster_Bottom_Ranged actor) : base(EStateType_Monster_Bottom_Ranged.Retreat)
    {
        mActor = actor;
    }

    public override void Start()
    {
        mActor.Animator.SetInteger("state", (int)Type);
    }

    public override void FixedUpdate()
    {
        if (mActor.CheckSkill())
        {
            return;
        }

        if (mActor.Input.MoveHorizontal > Mathf.Epsilon)
        {
            mActor.MoveHorizontal(-Giant_Player.Main.Status.MoveSpeed, EHorizontalDirection.Left);
        }
        else
        {
            mActor.State = EStateType_Monster_Bottom_Ranged.Idle;
            return;
        }
    }
}
