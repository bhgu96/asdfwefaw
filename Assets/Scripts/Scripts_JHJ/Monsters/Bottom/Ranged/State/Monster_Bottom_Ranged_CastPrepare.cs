﻿using UnityEngine;

public class Monster_Bottom_Ranged_CastPrepare : State<EStateType_Monster_Bottom_Ranged>
{
    private Monster_Bottom_Ranged mActor;

    public Monster_Bottom_Ranged_CastPrepare(Monster_Bottom_Ranged actor) : base(EStateType_Monster_Bottom_Ranged.CastPrepare)
    {
        mActor = actor;
    }

    public override void Start()
    {
        mActor.Animator.SetInteger("state", (int)Type);
        mActor.Animator.speed = mActor.Status.CastSpeed;
    }

    public override void End()
    {
        mActor.Animator.speed = 1.0f;
    }

    public override void FixedUpdate()
    {
        // 오른쪽 이동 입력시
        if (mActor.Input.MoveHorizontal > Mathf.Epsilon)
        {
            mActor.MoveHorizontal(-Giant_Player.Main.Status.MoveSpeed, EHorizontalDirection.Left);
        }
    }
}
