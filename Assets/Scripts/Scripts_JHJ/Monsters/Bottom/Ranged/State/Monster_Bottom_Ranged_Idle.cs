﻿using UnityEngine;

public class Monster_Bottom_Ranged_Idle : State<EStateType_Monster_Bottom_Ranged>
{
    private Monster_Bottom_Ranged mActor;

    public Monster_Bottom_Ranged_Idle(Monster_Bottom_Ranged actor) : base(EStateType_Monster_Bottom_Ranged.Idle)
    {
        mActor = actor;
    }

    public override void Start()
    {
        mActor.Animator.SetInteger("state", (int)Type);
    }

    public override void Update()
    {
        // 스킬 사용에 성공했다면
        if(mActor.CheckSkill())
        {
            return;
        }
        // 오른쪽 이동 입력시, Retreat 전이
        if(mActor.Input.MoveHorizontal > Mathf.Epsilon)
        {
            mActor.State = EStateType_Monster_Bottom_Ranged.Retreat;
            return;
        }
    }
}
