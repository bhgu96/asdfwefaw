﻿using UnityEngine;
using System.Collections;

public enum EStateType_Agent_Monster_Bottom_Ranged
{
    None = -1, 

    Normal = 0, 

    Count
}

public class Agent_Monster_Bottom_Ranged : Agent<EStateType_Agent_Monster_Bottom_Ranged>
{
    public Monster_Bottom_Ranged Actor { get; private set; }

    public Agent_Monster_Bottom_Ranged(Monster_Bottom_Ranged actor, int countOfSkills) : base(countOfSkills)
    {
        Actor = actor;

        SetStates(new Agent_Monster_Bottom_Ranged_Normal(this));
    }
}
