﻿using UnityEngine;

public class Agent_Monster_Bottom_Ranged_Normal : State<EStateType_Agent_Monster_Bottom_Ranged>
{
    private Agent_Monster_Bottom_Ranged mAgent;

    private float mDistance;

    public Agent_Monster_Bottom_Ranged_Normal(Agent_Monster_Bottom_Ranged agent) : base(EStateType_Agent_Monster_Bottom_Ranged.Normal)
    {
        mAgent = agent;

        mDistance = Random.Range(mAgent.Actor.Status.MinDistance, mAgent.Actor.Status.MaxDistance);
    }

    public override void Update()
    {
        Collider2D[] colliders = mAgent.Actor.TriggerHandler.GetColliders(ETriggerType_Monster_Bottom_Ranged.Enemy);

        for (int i = 0; i < colliders.Length && colliders[i] != null; i++)
        {
            Giant_Player giant = colliders[i].GetComponentInParent<Giant_Player>();
            if (giant != null && giant.State != EStateType_Giant_Player.Die)
            {
                Vector2 giantPosition = giant.transform.position;
                Vector2 agentPosition = mAgent.Actor.transform.position;

                float distance = agentPosition.x - giantPosition.x;

                if (mDistance > distance)
                {
                    mAgent.SetMoveHorizontal(1.0f);
                }
                else
                {
                    mAgent.SetMoveHorizontal(0.0f);
                }

                for(int j=0; j<mAgent.Skill.Length; j++)
                {
                    if(mAgent.Actor.SkillHandler.Get(mAgent.Actor.Skills[j]).CanActivate)
                    {
                        if(!mAgent.Skill[j])
                        {
                            mAgent.SetSkill(true, j);
                        }
                        else
                        {
                            mAgent.SetSkill(false, j);
                        }
                        return;
                    }
                    else
                    {
                        mAgent.SetSkill(false, j);
                    }
                }

                return;
            }
        }
    }
}
