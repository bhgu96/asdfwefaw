﻿using UnityEngine;
using Object;

public enum EStateType_Monster_Bottom_Ranged
{
    None = -1, 

    // 대기
    Idle = 0, 
    // 후퇴
    Retreat = 1, 
    // 피격
    Hit = 2, 
    // 죽음
    Die = 3, 
    // 구속
    Restrain = 4, 
    // 스킬 시전
    CastPrepare = 5, 
    // 스킬 발동중
    Cast = 6, 
    // 스킬 발동 중지
    CastEnd = 7,

    Count
}

public enum ETriggerType_Monster_Bottom_Ranged
{
    None = -1, 

    // AI 적 감지 트리거
    Enemy = 0, 
    Reflex = 1,

    Count
}

public enum EAudioType_Monster_Bottom_Ranged
{
    None = -1, 


    Count
}


[RequireComponent(typeof(TriggerHandler_Monster_Bottom_Ranged), typeof(AttackHandler_Monster_Bottom_Ranged), typeof(AudioHandler_Monster_Bottom_Ranged))]
[RequireComponent(typeof(SkillHandler_Monster_Bottom_Ranged))]
public class Monster_Bottom_Ranged 
    : AActor_Monster_Bottom<EStateType_Monster_Bottom_Ranged>, IPoolable<EMonsterType>
{
    [SerializeField]
    private EMonsterType_Bottom_Ranged mType;
    public new EMonsterType_Bottom_Ranged Type { get { return mType; } }

    [SerializeField]
    private EMonsterRank mMonsterRank;
    /// <summary>
    /// 몬스터 등급
    /// </summary>
    public override EMonsterRank MonsterRank
    {
        get { return mMonsterRank; }
    }

    public override ECombatType CombatType
    {
        get { return ECombatType.Ranged; }
    }

    private Status_Monster_Bottom_Ranged mStatus;
    public new Status_Monster_Bottom_Ranged Status
    {
        get { return mStatus; }
        set { base.Status = value; mStatus = value; }
    }

    public TriggerHandler_Monster_Bottom_Ranged TriggerHandler { get; private set; }
    public AttackHandler_Monster_Bottom_Ranged AttackHandler { get; private set; }
    public AudioHandler_Monster_Bottom_Ranged AudioHandler { get; private set; }

    [SerializeField]
    private ESkillType_Monster_Bottom_Ranged[] mSkills;
    public ESkillType_Monster_Bottom_Ranged[] Skills { get { return mSkills; } }

    public SkillHandler_Monster_Bottom_Ranged SkillHandler { get; private set; }

    /// <summary>
    /// 스킬 사용 여부를 검사하고 시전하는 메소드
    /// </summary>
    public bool CheckSkill()
    {
        if(CanCast)
        {
            for(int i=0; i<Skills.Length; i++)
            {
                if(Input.SkillDown[i])
                {
                    ISkill_Monster_Bottom_Ranged skill = SkillHandler.Get(Skills[i]);
                    if(skill != null && skill.Activate())
                    {
                        return true;
                    }
                }
            }
        }

        return false;
    }

    public override void OnRestrain()
    {
        if(CastingSkill != null && CastingSkill.IsActivated)
        {
            CastingSkill.Deactivate();
        }

        State = EStateType_Monster_Bottom_Ranged.Restrain;
    }

    protected override void OnActivate()
    {
        base.OnActivate();
        Status.Life = Status.MaxLife;
    }

    protected override void OnDamage()
    {
        base.OnDamage();
        if (!DebuffHandler.Get(EDebuffType.Restrain_Bush).IsActivated
            && Damage.CanReflex && Damage.SrcInstance is IDamagable damagable)
        {
            Damage damage = damagable.Damage;

            damage.SetDamage(false, true, false
                , EEffectType.None, EDamageType.Monster, ElementalType
                , Status.Level, Status.GetEA(ElementalType), Status.Offense, Status.Force
                , AttackHandler.Get(ETriggerType_Monster_Bottom_Ranged.Reflex).transform.position, this);

            damagable.ActivateDamage();
        }
    }

    protected override void OnHit()
    {
        if(CastingSkill != null && CastingSkill.IsActivated)
        {
            CastingSkill.Deactivate();
        }

        State = EStateType_Monster_Bottom_Ranged.Hit;
    }

    protected override void OnDie()
    {
        base.OnDie();

        State = EStateType_Monster_Bottom_Ranged.Die;
    }

    protected override void Awake()
    {
        base.Awake();

        Status = new Status_Monster_Bottom_Ranged(this);

        TriggerHandler = GetComponent<TriggerHandler_Monster_Bottom_Ranged>();
        AudioHandler = GetComponent<AudioHandler_Monster_Bottom_Ranged>();
        AttackHandler = GetComponent<AttackHandler_Monster_Bottom_Ranged>();
        SkillHandler = GetComponent<SkillHandler_Monster_Bottom_Ranged>();

        SetInputs(new Agent_Monster_Bottom_Ranged(this, Skills.Length));

        SetStates(new Monster_Bottom_Ranged_Idle(this)
            , new Monster_Bottom_Ranged_Retreat(this)
            , new Monster_Bottom_Ranged_Hit(this)
            , new Monster_Bottom_Ranged_Die(this)
            , new Monster_Bottom_Ranged_Restrain(this)
            , new Monster_Bottom_Ranged_CastPrepare(this)
            , new Monster_Bottom_Ranged_Cast(this)
            , new Monster_Bottom_Ranged_CastEnd(this));
    }
}
