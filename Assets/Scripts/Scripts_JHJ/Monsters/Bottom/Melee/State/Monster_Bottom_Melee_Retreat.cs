﻿using UnityEngine;

public class Monster_Bottom_Melee_Retreat : State<EStateType_Monster_Bottom_Melee>
{
    private Monster_Bottom_Melee mActor;

    public Monster_Bottom_Melee_Retreat(Monster_Bottom_Melee actor) : base(EStateType_Monster_Bottom_Melee.Retreat)
    {
        mActor = actor;
    }

    public override void Start()
    {
        mActor.Animator.SetInteger("state", (int)Type);
    }

    public override void FixedUpdate()
    {
        // 스킬 사용이 감지되었다면
        if (mActor.CheckSkill())
        {
            return;
        }
        // 오른쪽 움직임이 감지되었다면
        if (mActor.Input.MoveHorizontal > Mathf.Epsilon)
        {
            // 왼쪽을 본 상태로 오른쪽으로 이동
            mActor.MoveHorizontal(-mActor.Status.MoveSpeed, EHorizontalDirection.Left);
            return;
        }
        // 오른쪽 움직임이 감지되지 않았다면
        else
        {
            // Idle 상태로 전이
            mActor.State = EStateType_Monster_Bottom_Melee.Idle;
            return;
        }
    }
}
