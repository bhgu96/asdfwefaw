﻿using UnityEngine;

public class Monster_Bottom_Melee_RushPrepare : State<EStateType_Monster_Bottom_Melee>
{
    private Monster_Bottom_Melee mActor;

    public Monster_Bottom_Melee_RushPrepare(Monster_Bottom_Melee actor) : base(EStateType_Monster_Bottom_Melee.RushPrepare)
    {
        mActor = actor;
    }

    public override void Start()
    {
        mActor.Animator.SetInteger("state", (int)Type);
        mActor.Animator.speed = mActor.Status.CastSpeed;
    }

    public override void End()
    {
        mActor.Animator.speed = 1.0f;
    }
}
