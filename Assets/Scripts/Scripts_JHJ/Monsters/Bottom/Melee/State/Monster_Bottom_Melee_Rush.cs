﻿using UnityEngine;

public class Monster_Bottom_Melee_Rush : State<EStateType_Monster_Bottom_Melee>
{
    private Monster_Bottom_Melee mActor;

    public Monster_Bottom_Melee_Rush(Monster_Bottom_Melee actor) : base(EStateType_Monster_Bottom_Melee.Rush)
    {
        mActor = actor;
    }

    public override void Start()
    {
        mActor.Animator.SetInteger("state", (int)Type);
    }
}
