﻿using UnityEngine;

public class Monster_Bottom_Melee_Idle : State<EStateType_Monster_Bottom_Melee>
{
    private Monster_Bottom_Melee mActor;

    public Monster_Bottom_Melee_Idle(Monster_Bottom_Melee actor) : base(EStateType_Monster_Bottom_Melee.Idle)
    {
        mActor = actor;
    }

    public override void Start()
    {
        mActor.Animator.SetInteger("state", (int)Type);
    }

    public override void Update()
    {
        // 스킬 사용 검사
        if(mActor.CheckSkill())
        {
            return;
        }

        // 오른쪽 움직임 입력이 감지되었다면, Retreat 전이
        if(mActor.Input.MoveHorizontal > Mathf.Epsilon)
        {
            mActor.State = EStateType_Monster_Bottom_Melee.Retreat;
            return;
        }
    }
}
