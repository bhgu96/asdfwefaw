﻿using UnityEngine;

public class Monster_Bottom_Melee_Restrain : State<EStateType_Monster_Bottom_Melee>
{
    private Monster_Bottom_Melee mActor;

    public Monster_Bottom_Melee_Restrain(Monster_Bottom_Melee actor) : base(EStateType_Monster_Bottom_Melee.Restrain)
    {
        mActor = actor;
    }

    public override void Start()
    {
        mActor.Animator.SetInteger("state", (int)Type);
    }

    public override void Update()
    {
        if(!mActor.DebuffHandler.Get(EDebuffType.Restrain_Bush).IsActivated)
        {
            mActor.State = EStateType_Monster_Bottom_Melee.Idle;
        }
    }
}
