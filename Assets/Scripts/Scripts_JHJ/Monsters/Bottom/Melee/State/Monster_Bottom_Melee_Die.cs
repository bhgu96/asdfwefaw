﻿using UnityEngine;

public class Monster_Bottom_Melee_Die : State<EStateType_Monster_Bottom_Melee>
{
    private Monster_Bottom_Melee mActor;

    public Monster_Bottom_Melee_Die(Monster_Bottom_Melee actor) : base(EStateType_Monster_Bottom_Melee.Die)
    {
        mActor = actor;
    }

    public override void Start()
    {
        mActor.Animator.SetInteger("state", (int)Type);
        mActor.Velocity = Vector2.zero;
        mActor.MainCollider.enabled = false;
        mActor.Rigidbody.constraints = RigidbodyConstraints2D.FreezeAll;

        if (!StateManager.GameOver.State)
        {
            // 경험치 획득
            PlayerData.Exp += mActor.Status.Exp;
            // 소울 획득
            PlayerData.Souls += mActor.Status.Souls;
            // 소울 개수 / 50 만큼 마나도 획득
            Fairy_Player.Main.Status.Mana += mActor.Status.Souls * 0.02f;
        }
    }

    public override void End()
    {
        mActor.Velocity = Vector2.zero;
        mActor.MainCollider.enabled = true;
        mActor.Rigidbody.constraints = RigidbodyConstraints2D.FreezeRotation;
    }
}
