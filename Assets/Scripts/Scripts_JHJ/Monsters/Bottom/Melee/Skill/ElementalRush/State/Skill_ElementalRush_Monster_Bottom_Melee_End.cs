﻿using UnityEngine;

public class Skill_ElementalRush_Monster_Bottom_Melee_End : State<EStateType_Skill_ElementalRush_Monster_Bottom_Melee>
{
    private Skill_ElementalRush_Monster_Bottom_Melee mSkill;

    public Skill_ElementalRush_Monster_Bottom_Melee_End(Skill_ElementalRush_Monster_Bottom_Melee skill) : base(EStateType_Skill_ElementalRush_Monster_Bottom_Melee.End)
    {
        mSkill = skill;
    }

    public override void Update()
    {
        mSkill.Animator.SetInteger("state", (int)Type);
    }
}
