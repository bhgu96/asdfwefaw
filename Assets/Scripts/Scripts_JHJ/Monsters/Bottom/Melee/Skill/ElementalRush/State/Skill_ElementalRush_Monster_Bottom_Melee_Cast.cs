﻿using UnityEngine;

public class Skill_ElementalRush_Monster_Bottom_Melee_Cast : State<EStateType_Skill_ElementalRush_Monster_Bottom_Melee>
{
    private Skill_ElementalRush_Monster_Bottom_Melee mSkill;
    private float mRemainTime;

    public Skill_ElementalRush_Monster_Bottom_Melee_Cast(Skill_ElementalRush_Monster_Bottom_Melee skill) : base(EStateType_Skill_ElementalRush_Monster_Bottom_Melee.Cast)
    {
        mSkill = skill;
    }

    public override void Start()
    {
        mSkill.Animator.SetInteger("state", (int)Type);
        mSkill.User.State = EStateType_Monster_Bottom_Melee.Rush;
        mRemainTime = mSkill.Duration;
    }

    public override void Update()
    {
        if(mRemainTime > 0.0f)
        {
            mRemainTime -= Time.deltaTime;
        }
        else
        {
            mSkill.Deactivate();
        }
    }

    public override void FixedUpdate()
    {
        mSkill.User.MoveHorizontal(mSkill.User.Status.MoveSpeed * mSkill.SkillMoveSpeed, mSkill.User.HorizontalDirection);
        if(mSkill.Rush())
        {
            mSkill.Deactivate();
        }
    }
}