﻿using UnityEngine;
using System.Collections;

public enum EStateType_Skill_ElementalRush_Monster_Bottom_Melee
{
    None = -1, 

    Idle = 0, 
    Prepare = 1, 
    Cast = 2, 
    End = 3, 

    Count
}

public enum ETriggerType_Skill_ElementalRush_Monster_Bottom_Melee
{
    None = -1, 

    Rush = 0, 

    Count
}

public enum EAudioType_Skill_ElementalRush_Monster_Bottom_Melee
{
    None = -1, 

    Count
}

[RequireComponent(typeof(TriggerHandler_Skill_ElementalRush_Monster_Bottom_Melee), typeof(AudioHandler_Skill_ElementalRush_Monster_Bottom_Melee), typeof(AttackHandler_Skill_ElementalRush_Monster_Bottom_Melee))]
public class Skill_ElementalRush_Monster_Bottom_Melee : ASkill_Monster_Bottom_Melee<EStateType_Skill_ElementalRush_Monster_Bottom_Melee>
{
    [SerializeField]
    private ESkillType_Monster_Bottom_Melee mType;
    /// <summary>
    /// 스킬 타입
    /// </summary>
    public override ESkillType_Monster_Bottom_Melee Type
    {
        get { return mType; }
    }
    

    public TriggerHandler_Skill_ElementalRush_Monster_Bottom_Melee TriggerHandler { get; private set; }
    public AudioHandler_Skill_ElementalRush_Monster_Bottom_Melee AudioHandler { get; private set; }
    public AttackHandler_Skill_ElementalRush_Monster_Bottom_Melee AttackHandler { get; private set; }

    /// <summary>
    /// 캐스팅 시간
    /// </summary>
    public override float CastTime => 0.5f;

    [SerializeField]
    private EElementalType mElementalType;
    public EElementalType ElementalType_Default
    {
        get { return mElementalType; }
    }
    public EElementalType ElementalType
    {
        get { return ElementalType_Default; }
    }

    public float SkillOffense_Default
    {
        get { return Parameters[0]; }
    }
    public float SkillOffense_Increment
    {
        get;
    }
    public float SkillOffense_Increase
    {
        get;
    }
    public float SkillOffense
    {
        get { return (SkillOffense_Default + SkillOffense_Increment) * (1.0f + SkillOffense_Increase); }
    }

    public float Duration_Default
    {
        get { return Parameters[1]; }
    }
    public float Duration_Increase
    {
        get;
    }
    public float Duration_Increment
    {
        get;
    }
    public float Duration
    {
        get { return (Duration_Default + Duration_Increment) * (1.0f + Duration_Increase); }
    }

    public float SkillMoveSpeed_Default
    {
        get { return Parameters[2]; }
    }
    public float SkillMoveSpeed_Increase
    {
        get;
    }
    public float SkillMoveSpeed
    {
        get { return SkillMoveSpeed_Default * (1.0f + SkillMoveSpeed_Increase); }
    }

    /// <summary>
    /// 돌격 공격 트리거 검사
    /// </summary>
    public bool Rush()
    {
        Collider2D[] colliders = TriggerHandler.GetColliders(ETriggerType_Skill_ElementalRush_Monster_Bottom_Melee.Rush);
        for(int i=0; i<colliders.Length && colliders[i] != null; i++)
        {
            IDamagable damagable = colliders[i].GetComponentInParent<IDamagable>();
            if(damagable != null)
            {
                Damage damage = damagable.Damage;
                damage.SetDamage(true, true, false
                    , EEffectType.None, EDamageType.Monster, ElementalType
                    , User.Status.Level, User.Status.GetEA(ElementalType), SkillOffense * User.Status.Offense, User.Status.Force
                    , AttackHandler.Get(ETriggerType_Skill_ElementalRush_Monster_Bottom_Melee.Rush).transform.position, User);

                damagable.ActivateDamage();

                Giant_Player giant = damagable as Giant_Player;
                if (giant != null)
                {
                    return true;
                }
            }
        }
        return false;
    }

    protected override void OnActivate()
    {
        base.OnActivate();
        State = EStateType_Skill_ElementalRush_Monster_Bottom_Melee.Prepare;
        User.State = EStateType_Monster_Bottom_Melee.RushPrepare;
        SetDisableCast();
    }

    protected override void OnDeactivate()
    {
        base.OnDeactivate();
        State = EStateType_Skill_ElementalRush_Monster_Bottom_Melee.End;
        SetEnableCast();
        User.State = EStateType_Monster_Bottom_Melee.RushEnd;
    }

    protected override void Awake()
    {
        base.Awake();

        TriggerHandler = GetComponent<TriggerHandler_Skill_ElementalRush_Monster_Bottom_Melee>();
        AudioHandler = GetComponent<AudioHandler_Skill_ElementalRush_Monster_Bottom_Melee>();
        AttackHandler = GetComponent<AttackHandler_Skill_ElementalRush_Monster_Bottom_Melee>();

        SetStates(new Skill_ElementalRush_Monster_Bottom_Melee_Idle(this)
            , new Skill_ElementalRush_Monster_Bottom_Melee_Prepare(this)
            , new Skill_ElementalRush_Monster_Bottom_Melee_Cast(this)
            , new Skill_ElementalRush_Monster_Bottom_Melee_End(this));
    }
}
