﻿using UnityEngine;

public class Skill_ElementalRush_Monster_Bottom_Melee_Prepare : State<EStateType_Skill_ElementalRush_Monster_Bottom_Melee>
{
    private Skill_ElementalRush_Monster_Bottom_Melee mSkill;

    public Skill_ElementalRush_Monster_Bottom_Melee_Prepare(Skill_ElementalRush_Monster_Bottom_Melee skill) : base(EStateType_Skill_ElementalRush_Monster_Bottom_Melee.Prepare)
    {
        mSkill = skill;
    }

    public override void Start()
    {
        mSkill.Animator.SetInteger("state", (int)Type);
        mSkill.Animator.speed = mSkill.CastSpeed;
    }

    public override void End()
    {
        mSkill.Animator.speed = 1.0f;
    }
}
