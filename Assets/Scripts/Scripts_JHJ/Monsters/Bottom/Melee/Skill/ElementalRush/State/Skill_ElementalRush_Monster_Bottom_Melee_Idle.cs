﻿using UnityEngine;

public class Skill_ElementalRush_Monster_Bottom_Melee_Idle : State<EStateType_Skill_ElementalRush_Monster_Bottom_Melee>
{
    private Skill_ElementalRush_Monster_Bottom_Melee mSkill;

    public Skill_ElementalRush_Monster_Bottom_Melee_Idle(Skill_ElementalRush_Monster_Bottom_Melee skill) : base(EStateType_Skill_ElementalRush_Monster_Bottom_Melee.Idle)
    {
        mSkill = skill;
    }

    public override void Start()
    {
        mSkill.Animator.SetInteger("state", (int)Type);
    }
}
