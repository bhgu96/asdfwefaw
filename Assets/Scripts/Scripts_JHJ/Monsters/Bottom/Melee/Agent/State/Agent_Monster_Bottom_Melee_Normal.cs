﻿using UnityEngine;

public class Agent_Monster_Bottom_Melee_Normal : State<EStateType_Agent_Monster_Bottom_Melee>
{
    private Agent_Monster_Bottom_Melee mAgent;

    public Agent_Monster_Bottom_Melee_Normal(Agent_Monster_Bottom_Melee agent) : base(EStateType_Agent_Monster_Bottom_Melee.Normal)
    {
        mAgent = agent;
    }

    public override void Update()
    {
        Collider2D[] colliders = mAgent.Actor.TriggerHandler.GetColliders(ETriggerType_Monster_Bottom_Melee.Enemy);
        for(int i=0; i<colliders.Length && colliders[i] != null; i++)
        {
            Giant_Player giant = colliders[i].GetComponentInParent<Giant_Player>();
            if(giant != null && giant.State != EStateType_Giant_Player.Die)
            {
                for(int j=0; j<mAgent.Skill.Length; j++)
                {
                    if(mAgent.Actor.SkillHandler.Get(mAgent.Actor.Skills[j]).CanActivate)
                    {
                        // 스킬 사용 가능한 경우
                        mAgent.SetMoveHorizontal(0.0f);
                        if(!mAgent.Skill[j])
                        {
                            mAgent.SetSkill(true, j);
                        }
                        else
                        {
                            mAgent.SetSkill(false, j);
                        }
                        return;
                    }
                    else
                    {
                        mAgent.SetSkill(false, j);
                    }
                }

                // 스킬 사용이 불가능한 경우, 오른쪽 이동
                mAgent.SetMoveHorizontal(1.0f);
                return;
            }
        }

        // 거인을 감지하지 못한 경우
        mAgent.SetMoveHorizontal(0.0f);
        for(int i=0; i<mAgent.Skill.Length; i++)
        {
            mAgent.SetSkill(false, i);
        }
    }
}
