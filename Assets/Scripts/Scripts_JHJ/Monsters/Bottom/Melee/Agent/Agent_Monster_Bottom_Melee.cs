﻿using UnityEngine;
using System.Collections;

public enum EStateType_Agent_Monster_Bottom_Melee
{
    None = -1, 
    
    Normal = 0, 

    Count
}

public class Agent_Monster_Bottom_Melee : Agent<EStateType_Agent_Monster_Bottom_Melee>
{
    public Monster_Bottom_Melee Actor { get; private set; }

    public IActor_Damagable Enemy { get; set; }

    public Agent_Monster_Bottom_Melee(Monster_Bottom_Melee actor, int countOfSkills) : base(countOfSkills)
    {
        Actor = actor;

        SetStates(new Agent_Monster_Bottom_Melee_Normal(this));
    }
}
