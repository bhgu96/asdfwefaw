﻿using UnityEngine;
using System.Collections;
using Object;

public enum EStateType_Monster_Bottom_Melee
{
    None = -1, 

    Idle = 0, 
    Retreat = 1,
    Hit = 2, 
    Die = 3, 
    Restrain = 4, 
    RushPrepare = 5, 
    Rush = 6, 
    RushEnd = 7, 

    Count
}
public enum ETriggerType_Monster_Bottom_Melee
{
    None = -1, 

    Reflex = 0, 
    Rush = 1, 
    Enemy = 2, 

    Count
}
public enum EAudioType_Monster_Bottom_Melee
{
    None = -1, 

    Count
}

[RequireComponent(typeof(TriggerHandler_Monster_Bottom_Melee), typeof(AudioHandler_Monster_Bottom_Melee), typeof(AttackHandler_Monster_Bottom_Melee))]
[RequireComponent(typeof(SkillHandler_Monster_Bottom_Melee))]
public class Monster_Bottom_Melee 
    : AActor_Monster_Bottom<EStateType_Monster_Bottom_Melee>, IPoolable<EMonsterType>
{
    [SerializeField]
    private EMonsterType_Bottom_Melee mType;
    public new EMonsterType_Bottom_Melee Type { get { return mType; } }

    [SerializeField]
    private EMonsterRank mMonsterRank;
    public override EMonsterRank MonsterRank { get { return mMonsterRank; } }

    public override ECombatType CombatType
    {
        get { return ECombatType.Melee; }
    }

    public TriggerHandler_Monster_Bottom_Melee TriggerHandler { get; private set; }
    public AudioHandler_Monster_Bottom_Melee AudioHandler { get; private set; }
    public AttackHandler_Monster_Bottom_Melee AttackHandler { get; private set; }
    public SkillHandler_Monster_Bottom_Melee SkillHandler { get; private set; }

    private Status_Monster_Bottom_Melee mStatus;
    public new Status_Monster_Bottom_Melee Status
    {
        get { return mStatus; }
        protected set { base.Status = value; mStatus = value; }
    }

    [SerializeField]
    private ESkillType_Monster_Bottom_Melee[] mSkills;
    /// <summary>
    /// 사용할 스킬
    /// </summary>
    public ESkillType_Monster_Bottom_Melee[] Skills { get { return mSkills; } }

    /// <summary>
    /// 스킬 사용 입력을 체크하는 메소드
    /// </summary>
    public bool CheckSkill()
    {
        if(CanCast)
        {
            for(int i=0; i<Skills.Length; i++)
            {
                if(Input.SkillDown[i])
                {
                    ISkill_Monster_Bottom_Melee skill = SkillHandler.Get(Skills[i]);
                    if(skill != null && skill.Activate())
                    {
                        return true;
                    }
                }
            }
        }

        return false;
    }

    public override void OnRestrain()
    {
        if (CastingSkill != null)
        {
            CastingSkill.Deactivate();
        }
        State = EStateType_Monster_Bottom_Melee.Restrain;
    }

    protected override void OnDamage()
    {
        base.OnDamage();
        if (!DebuffHandler.Get(EDebuffType.Restrain_Bush).IsActivated
            && Damage.CanReflex && Damage.SrcInstance is IDamagable damagable)
        {
            Damage damage = damagable.Damage;

            damage.SetDamage(false, true, false
                , EEffectType.None, EDamageType.Monster, ElementalType
                , Status.Level, Status.GetEA(ElementalType), Status.Offense, Status.Force
                , AttackHandler.Get(ETriggerType_Monster_Bottom_Melee.Reflex).transform.position, this);

            damagable.ActivateDamage();
        }
    }

    protected override void OnDeactivate()
    {
        base.OnDeactivate();
        Status.Life = Status.MaxLife;
        State = EStateType_Monster_Bottom_Melee.Idle;
    }

    protected override void OnDie()
    {
        base.OnDie();

        State = EStateType_Monster_Bottom_Melee.Die;
    }

    protected override void OnHit()
    {
        if(CastingSkill != null)
        {
            CastingSkill.Deactivate();
        }
        State = EStateType_Monster_Bottom_Melee.Hit;
    }

    protected override void Awake()
    {
        base.Awake();
        Status = new Status_Monster_Bottom_Melee(this);
        SetInputs(new Agent_Monster_Bottom_Melee(this, Skills.Length));
        TriggerHandler = GetComponent<TriggerHandler_Monster_Bottom_Melee>();
        AudioHandler = GetComponent<AudioHandler_Monster_Bottom_Melee>();
        AttackHandler = GetComponent<AttackHandler_Monster_Bottom_Melee>();
        SkillHandler = GetComponent<SkillHandler_Monster_Bottom_Melee>();

        SetStates(new Monster_Bottom_Melee_Idle(this)
            , new Monster_Bottom_Melee_Retreat(this)
            , new Monster_Bottom_Melee_Hit(this)
            , new Monster_Bottom_Melee_Die(this)
            , new Monster_Bottom_Melee_Restrain(this)
            , new Monster_Bottom_Melee_RushPrepare(this)
            , new Monster_Bottom_Melee_Rush(this)
            , new Monster_Bottom_Melee_RushEnd(this));
    }
}
