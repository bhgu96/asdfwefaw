﻿using System.Collections.Generic;

public class Status_Monster_Bottom_Tank : AStatus_Monster_Bottom
{
    public new Monster_Bottom_Tank Actor { get; }

    public override string NameTag
    {
        get { return mDefault.NameTag; }
    }

    public override string FlavorTag
    {
        get { return FlavorTag; }
    }

    public override int Level_Default
    {
        get { return mDefault.Level; }
    }

    public override int Exp_Default
    {
        get { return mDefault.Exp; }
    }

    public override int Souls_Default
    {
        get { return mDefault.Souls; }
    }

    public override float MoveSpeed_Default
    {
        get { return mDefault.MoveSpeed; }
    }

    public override float MaxLife_Default
    {
        get { return mDefault.MaxLife; }
    }

    public override float Offense_Default
    {
        get { return mDefault.Offense; }
    }

    public override float Defense_Default
    {
        get { return mDefault.Defense; }
    }

    public override float Force_Default
    {
        get { return mDefault.Force; }
    }

    public override float Weight_Default
    {
        get { return mDefault.Weight; }
    }

    private DefaultStatus_Monster_Bottom_Tank mDefault;

    public Status_Monster_Bottom_Tank(Monster_Bottom_Tank actor) : base(actor)
    {
        Actor = actor;
        mDefault = DataManager.GetDefaultStatus(actor.Type);

        Life = MaxLife;
    }

    public override int GetEA_Default(EElementalType e)
    {
        return mDefault.GetEA(e);
    }
}

public class DefaultStatus_Monster_Bottom_Tank
{
    public string NameTag { get; }
    public string FlavorTag { get; }
    public int Level { get; }
    public int Exp { get; }
    public int Souls { get; }
    public float MoveSpeed { get; }
    public float MaxLife { get; }
    public float Offense { get; }
    public float Defense { get; }
    public float Force { get; }
    public float Weight { get; }

    private Dictionary<EElementalType, int> mEa;

    public DefaultStatus_Monster_Bottom_Tank(Dictionary<string, string> statusDict)
    {
        mEa = new Dictionary<EElementalType, int>();

        NameTag = statusDict["Name Tag"];
        FlavorTag = statusDict["Flavor Tag"];
        Level = int.Parse(statusDict["Level"]);
        Exp = int.Parse(statusDict["Exp"]);
        Souls = int.Parse(statusDict["Souls"]);
        MoveSpeed = float.Parse(statusDict["Move Speed"]);
        MaxLife = float.Parse(statusDict["Max Life"]);
        Offense = float.Parse(statusDict["Offense"]);
        Defense = float.Parse(statusDict["Defense"]);
        Force = float.Parse(statusDict["Force"]);
        Weight = float.Parse(statusDict["Weight"]);

        for (EElementalType e = EElementalType.None + 1; e < EElementalType.Count; e++)
        {
            mEa.Add(e, int.Parse(statusDict[e.ToString()]));
        }
    }

    public int GetEA(EElementalType type)
    {
        return mEa[type];
    }
}
