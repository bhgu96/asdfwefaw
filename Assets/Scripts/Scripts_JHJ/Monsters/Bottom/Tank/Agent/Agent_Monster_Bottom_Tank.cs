﻿using UnityEngine;

public enum EStateType_Agent_Monster_Bottom_Tank
{
    None = -1, 

    Normal = 0, 

    Count
}

public class Agent_Monster_Bottom_Tank : Agent<EStateType_Agent_Monster_Bottom_Tank>
{
    public Monster_Bottom_Tank Actor { get; private set; }

    public Agent_Monster_Bottom_Tank(Monster_Bottom_Tank actor, int countOfSkills) : base(countOfSkills)
    {
        Actor = actor;

        SetStates(new Agent_Monster_Bottom_Tank_Normal(this));
    }
}
