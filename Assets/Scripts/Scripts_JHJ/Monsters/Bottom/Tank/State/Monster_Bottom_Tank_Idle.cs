﻿using UnityEngine;

public class Monster_Bottom_Tank_Idle : State<EStateType_Monster_Bottom_Tank>
{
    private Monster_Bottom_Tank mActor;

    public Monster_Bottom_Tank_Idle(Monster_Bottom_Tank actor) : base(EStateType_Monster_Bottom_Tank.Idle)
    {
        mActor = actor;
    }

    public override void Start()
    {
        mActor.Animator.SetInteger("state", (int)Type);
    }

    public override void Update()
    {
        // 왼쪽 이동 감지시
        if(mActor.Input.MoveHorizontal < -Mathf.Epsilon)
        {
            mActor.State = EStateType_Monster_Bottom_Tank.Advance;
            return;
        }
    }
}
