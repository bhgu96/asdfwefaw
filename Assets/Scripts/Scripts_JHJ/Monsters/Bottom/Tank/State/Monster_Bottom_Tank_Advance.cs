﻿using UnityEngine;

public class Monster_Bottom_Tank_Advance : State<EStateType_Monster_Bottom_Tank>
{
    private Monster_Bottom_Tank mActor;

    public Monster_Bottom_Tank_Advance(Monster_Bottom_Tank actor) : base(EStateType_Monster_Bottom_Tank.Advance)
    {
        mActor = actor;
    }

    public override void Start()
    {
        mActor.Animator.SetInteger("state", (int)Type);
    }

    public override void FixedUpdate()
    {
        if(mActor.Input.MoveHorizontal < -Mathf.Epsilon)
        {
            mActor.MoveHorizontal(mActor.Status.MoveSpeed, EHorizontalDirection.Left);
        }
        else
        {
            mActor.State = EStateType_Monster_Bottom_Tank.Idle;
            return;
        }
    }
}
