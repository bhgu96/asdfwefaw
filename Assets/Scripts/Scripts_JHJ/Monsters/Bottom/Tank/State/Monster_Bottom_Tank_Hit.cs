﻿using UnityEngine;

public class Monster_Bottom_Tank_Hit : State<EStateType_Monster_Bottom_Tank>
{
    private Monster_Bottom_Tank mActor;
    private bool mIsStarted = false;

    public Monster_Bottom_Tank_Hit(Monster_Bottom_Tank actor) : base(EStateType_Monster_Bottom_Tank.Hit)
    {
        mActor = actor;
    }

    public override void Start()
    {
        if(!mIsStarted)
        {
            mIsStarted = true;
            mActor.Animator.SetInteger("state", (int)Type);
        }
        else
        {
            mActor.Animator.SetTrigger("hit");
        }
    }

    public override void End()
    {
        mIsStarted = false;
    }

    public override void FixedUpdate()
    {
        Vector2 velocity = mActor.Velocity;

        if(Mathf.Abs(velocity.x) < GameConstant.THRESHOLD_HIT
            && Mathf.Abs(velocity.y) < GameConstant.THRESHOLD_HIT)
        {
            mActor.State = EStateType_Monster_Bottom_Tank.Idle;
            return;
        }
    }
}
