﻿using UnityEngine;

public class Monster_Bottom_Tank_Restrain : State<EStateType_Monster_Bottom_Tank>
{
    private Monster_Bottom_Tank mActor;

    public Monster_Bottom_Tank_Restrain(Monster_Bottom_Tank actor) : base(EStateType_Monster_Bottom_Tank.Restrain)
    {
        mActor = actor;
    }

    public override void Start()
    {
        mActor.Animator.SetInteger("state", (int)Type);
    }

    public override void Update()
    {
        if(!mActor.DebuffHandler.Get(EDebuffType.Restrain_Bush).IsActivated)
        {
            mActor.State = EStateType_Monster_Bottom_Tank.Idle;
            return;
        }
    }
}
