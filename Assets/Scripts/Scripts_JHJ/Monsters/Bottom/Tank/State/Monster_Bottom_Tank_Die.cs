﻿using UnityEngine;

public class Monster_Bottom_Tank_Die : State<EStateType_Monster_Bottom_Tank>
{
    private Monster_Bottom_Tank mActor;

    public Monster_Bottom_Tank_Die(Monster_Bottom_Tank actor) : base(EStateType_Monster_Bottom_Tank.Die)
    {
        mActor = actor;
    }

    public override void Start()
    {
        mActor.Animator.SetInteger("state", (int)Type);

        mActor.Velocity = Vector2.zero;
        mActor.MainCollider.enabled = false;
        mActor.Rigidbody.constraints = RigidbodyConstraints2D.FreezeAll;

        if(!StateManager.GameOver.State)
        {
            PlayerData.Exp += mActor.Status.Exp;
            PlayerData.Souls += mActor.Status.Souls;
            Fairy_Player.Main.Status.Mana += mActor.Status.Souls * 0.02f;
        }
    }

    public override void End()
    {
        mActor.Velocity = Vector2.zero;
        mActor.MainCollider.enabled = true;
        mActor.Rigidbody.constraints = RigidbodyConstraints2D.FreezeRotation;
    }
}
