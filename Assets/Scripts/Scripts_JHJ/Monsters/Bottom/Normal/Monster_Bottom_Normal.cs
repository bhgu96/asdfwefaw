﻿using UnityEngine;
using Object;

public enum EStateType_Monster_Bottom_Normal
{
    None = -1, 

    Idle = 0, 
    Advance = 1, 
    Hit = 2, 
    Die = 3, 
    Restrain = 4, 

    Count
}

public enum ETriggerType_Monster_Bottom_Normal
{
    None = -1, 

    Reflex = 0, 
    Enemy = 1, 

    Count
}

public enum EAudioType_Monster_Bottom_Normal
{
    None = -1, 

    Count
}

[RequireComponent(typeof(TriggerHandler_Monster_Bottom_Normal), typeof(AudioHandler_Monster_Bottom_Normal), typeof(AttackHandler_Monster_Bottom_Normal))]
[RequireComponent(typeof(SkillHandler_Monster_Bottom_Normal))]
public class Monster_Bottom_Normal
    : AActor_Monster_Bottom<EStateType_Monster_Bottom_Normal>, IPoolable<EMonsterType>
{
    [SerializeField]
    [Tooltip("몬스터 타입")]
    private EMonsterType_Bottom_Normal mType;
    /// <summary>
    /// 몬스터 타입
    /// </summary>
    public new EMonsterType_Bottom_Normal Type { get { return mType; } }

    [SerializeField]
    private EMonsterRank mMonsterRank;
    /// <summary>
    /// 몬스터 등급
    /// </summary>
    public override EMonsterRank MonsterRank { get { return mMonsterRank; } }

    public override ECombatType CombatType
    {
        get { return ECombatType.Normal; }
    }

    [SerializeField]
    [Tooltip("히트시 발생하는 파티클")]
    private ParticleSystem mHitParticle;

    [SerializeField]
    [Tooltip("사용할 스킬")]
    private ESkillType_Monster_Bottom_Normal[] mSkills;
    /// <summary>
    /// 사용할 스킬
    /// </summary>
    public ESkillType_Monster_Bottom_Normal[] Skills { get { return mSkills; } }
    public SkillHandler_Monster_Bottom_Normal SkillHandler { get; private set; }

    public TriggerHandler_Monster_Bottom_Normal TriggerHandler { get; private set; }
    public AudioHandler_Monster_Bottom_Normal AudioHandler { get; private set; }
    public AttackHandler_Monster_Bottom_Normal AttackHandler { get; private set; }

    private Status_Monster_Bottom_Normal mStatus;
    public new Status_Monster_Bottom_Normal Status
    {
        get { return mStatus; }
        protected set { base.Status = value; mStatus = value; }
    }

    public override void OnRestrain()
    {
        State = EStateType_Monster_Bottom_Normal.Restrain;
    }


    protected override void OnDamage()
    {
        if (!DebuffHandler.Get(EDebuffType.Restrain_Bush).IsActivated
            && Damage.CanReflex && Damage.SrcInstance is IDamagable damagable)
        {
            Damage damage = damagable.Damage;

            damage.SetDamage(false, true, false
                , EEffectType.None, EDamageType.Monster, ElementalType
                , Status.Level, Status.GetEA(ElementalType), Status.Offense, Status.Force
                , AttackHandler.Get(ETriggerType_Monster_Bottom_Normal.Reflex).transform.position, this);

            damagable.ActivateDamage();
        }
    }
    protected override void OnHit()
    {
        State = EStateType_Monster_Bottom_Normal.Hit;
        if(mHitParticle != null)
        {
            mHitParticle.Play();
        }
    }
    protected override void OnDie()
    {
        base.OnDie();
        State = EStateType_Monster_Bottom_Normal.Die;
        if (mHitParticle != null)
        {
            mHitParticle.Play();
        }
    }

    protected override void OnDeactivate()
    {
        base.OnDeactivate();
        Status.Life = Status.MaxLife;
        State = EStateType_Monster_Bottom_Normal.Idle;
    }

    protected override void Awake()
    {
        base.Awake();

        Status = new Status_Monster_Bottom_Normal(this);

        TriggerHandler = GetComponent<TriggerHandler_Monster_Bottom_Normal>();
        AudioHandler = GetComponent<AudioHandler_Monster_Bottom_Normal>();
        AttackHandler = GetComponent<AttackHandler_Monster_Bottom_Normal>();
        SkillHandler = GetComponent<SkillHandler_Monster_Bottom_Normal>();

        // 입력 객체 할당
        SetInputs(new Agent_Monster_Bottom_Normal(this));

        // 상태 객체 할당
        SetStates(new Monster_Bottom_Normal_Idle(this)
            , new Monster_Bottom_Normal_Advance(this)
            , new Monster_Bottom_Normal_Hit(this)
            , new Monster_Bottom_Normal_Die(this)
            , new Monster_Bottom_Normal_Restrain(this));
    }
}