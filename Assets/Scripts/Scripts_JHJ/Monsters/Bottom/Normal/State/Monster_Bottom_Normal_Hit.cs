﻿using UnityEngine;

public class Monster_Bottom_Normal_Hit : State<EStateType_Monster_Bottom_Normal>
{
    private Monster_Bottom_Normal mActor;
    private bool mIsStarted = false;

    public Monster_Bottom_Normal_Hit(Monster_Bottom_Normal actor) : base(EStateType_Monster_Bottom_Normal.Hit)
    {
        mActor = actor;
    }

    public override void Start()
    {
        // 처음 Hit가 실행된 경우
        if (!mIsStarted)
        {
            mIsStarted = true;
            mActor.Animator.SetInteger("state", (int)Type);
        }
        // Hit가 이미 실행중인 경우
        else
        {
            mActor.Animator.SetTrigger("hit");
        }
    }

    public override void End()
    {
        mIsStarted = false;
    }

    public override void FixedUpdate()
    {
        Vector2 velocity = mActor.Velocity;

        // 넉백으로 인해 적용된 속도가 넉백 적용 한계값 미만이 된다면
        if (Mathf.Abs(velocity.x) < GameConstant.THRESHOLD_HIT - Mathf.Epsilon
            && Mathf.Abs(velocity.y) < GameConstant.THRESHOLD_HIT - Mathf.Epsilon)
        {
            // Idle 상태로 전이
            mActor.State = EStateType_Monster_Bottom_Normal.Idle;
        }
    }
}
