﻿using UnityEngine;

public class Monster_Bottom_Normal_Advance : State<EStateType_Monster_Bottom_Normal>
{
    private Monster_Bottom_Normal mActor;
    public Monster_Bottom_Normal_Advance(Monster_Bottom_Normal actor) : base(EStateType_Monster_Bottom_Normal.Advance)
    {
        mActor = actor;
    }

    public override void Start()
    {
        mActor.Animator.SetInteger("state", (int)Type);
    }

    public override void FixedUpdate()
    {
        // 왼쪽 움직임이 감지된 경우
        if(mActor.Input.MoveHorizontal < -Mathf.Epsilon)
        {
            // 왼쪽으로 이동
            mActor.MoveHorizontal(mActor.Status.MoveSpeed, EHorizontalDirection.Left);
        }
        // 왼쪽 움직임이 감지되지 않은 경우
        else
        {
            // Idle 상태로 전이
            mActor.State = EStateType_Monster_Bottom_Normal.Idle;
            return;
        }
    }
}
