﻿using UnityEngine;

public class Monster_Bottom_Normal_Idle : State<EStateType_Monster_Bottom_Normal>
{
    private Monster_Bottom_Normal mActor;

    public Monster_Bottom_Normal_Idle(Monster_Bottom_Normal actor) : base(EStateType_Monster_Bottom_Normal.Idle)
    {
        mActor = actor;
    }

    public override void Start()
    {
        mActor.Animator.SetInteger("state", (int)Type);
    }

    public override void Update()
    {
        // 왼쪽 이동시 Advace 전이
        if(mActor.Input.MoveHorizontal < -Mathf.Epsilon)
        {
            mActor.State = EStateType_Monster_Bottom_Normal.Advance;
        }
    }
}
