﻿using UnityEngine;

public class Monster_Bottom_Normal_Restrain : State<EStateType_Monster_Bottom_Normal>
{
    private Monster_Bottom_Normal mActor;

    public Monster_Bottom_Normal_Restrain(Monster_Bottom_Normal actor) : base(EStateType_Monster_Bottom_Normal.Restrain)
    {
        mActor = actor;
    }

    public override void Start()
    {
        mActor.Animator.SetInteger("state", (int)Type);
    }

    public override void Update()
    {
        // 구속상태에서 풀렸다면
        if(!mActor.DebuffHandler.Get(EDebuffType.Restrain_Bush).IsActivated)
        {
            mActor.State = EStateType_Monster_Bottom_Normal.Idle;
        }
    }
}
