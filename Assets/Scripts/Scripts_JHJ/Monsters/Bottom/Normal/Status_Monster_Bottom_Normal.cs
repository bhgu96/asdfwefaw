﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Status_Monster_Bottom_Normal
    : AStatus_Monster_Bottom
{
    public new Monster_Bottom_Normal Actor { get; }

    /// <summary>
    /// 이름 태그
    /// </summary>
    public override string NameTag
    {
        get { return mDefault.NameTag; }
    }
    /// <summary>
    /// 플레이버 텍스트 태그
    /// </summary>
    public override string FlavorTag
    {
        get { return mDefault.FlavorTag; }
    }
    /// <summary>
    /// 기본 레벨
    /// </summary>
    public override int Level_Default
    {
        get { return mDefault.Level; }
    }
    /// <summary>
    /// 기본 경험치 획득량
    /// </summary>
    public override int Exp_Default
    {
        get { return mDefault.Exp; }
    }
    /// <summary>
    /// 기본 소울
    /// </summary>
    public override int Souls_Default
    {
        get { return mDefault.Souls; }
    }
    /// <summary>
    /// 기본 이동 속도
    /// </summary>
    public override float MoveSpeed_Default
    {
        get { return mDefault.MoveSpeed; }
    }
    /// <summary>
    /// 기본 최대 생명력
    /// </summary>
    public override float MaxLife_Default
    {
        get { return mDefault.MaxLife; }
    }
    /// <summary>
    /// 기본 공격력
    /// </summary>
    public override float Offense_Default
    {
        get { return mDefault.Offense; }
    }
    /// <summary>
    /// 기본 방어력
    /// </summary>
    public override float Defense_Default
    {
        get { return mDefault.Defense; }
    }
    /// <summary>
    /// 기본 힘
    /// </summary>
    public override float Force_Default
    {
        get { return mDefault.Force; }
    }
    /// <summary>
    /// 기본 무게
    /// </summary>
    public override float Weight_Default
    {
        get { return mDefault.Weight; }
    }
    /// <summary>
    /// 기본 속성 친화도
    /// </summary>
    /// <param name="e"></param>
    /// <returns></returns>
    public override int GetEA_Default(EElementalType e)
    {
        return mDefault.GetEA(e);
    }

    private DefaultStatus_Monster_Bottom_Normal mDefault;

    public Status_Monster_Bottom_Normal(Monster_Bottom_Normal actor) : base(actor)
    {
        Actor = actor;
        mDefault = DataManager.GetDefaultStatus(actor.Type);
        Life = MaxLife;
    }
}

public class DefaultStatus_Monster_Bottom_Normal
{
    public string NameTag { get; }
    public string FlavorTag { get; }
    public int Level { get; }
    public int Exp { get; }
    public int Souls { get; }
    public float MoveSpeed { get; }
    public float MaxLife { get; }
    public float Offense { get; }
    public float Defense { get; }
    public float Force { get; }
    public float Weight { get; }

    private Dictionary<EElementalType, int> mEa;

    public DefaultStatus_Monster_Bottom_Normal(Dictionary<string, string> statusDict)
    {
        mEa = new Dictionary<EElementalType, int>();

        NameTag = statusDict["Name Tag"];
        FlavorTag = statusDict["Flavor Tag"];
        Level = int.Parse(statusDict["Level"]);
        Exp = int.Parse(statusDict["Exp"]);
        Souls = int.Parse(statusDict["Souls"]);
        MoveSpeed = float.Parse(statusDict["Move Speed"]);
        MaxLife = float.Parse(statusDict["Max Life"]);
        Offense = float.Parse(statusDict["Offense"]);
        Defense = float.Parse(statusDict["Defense"]);
        Force = float.Parse(statusDict["Force"]);
        Weight = float.Parse(statusDict["Weight"]);

        for(EElementalType e = EElementalType.None + 1; e < EElementalType.Count; e++)
        {
            mEa.Add(e, int.Parse(statusDict[e.ToString()]));
        }
    }

    public int GetEA(EElementalType type)
    {
        return mEa[type];
    }
}
