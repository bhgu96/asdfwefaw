﻿using UnityEngine;
using System.Collections;

public class Agent_Monster_Bottom_Normal_Normal : State<EStateType_Agent_Monster_Bottom_Normal>
{
    private Agent_Monster_Bottom_Normal mAgent;

    public Agent_Monster_Bottom_Normal_Normal(Agent_Monster_Bottom_Normal agent) : base(EStateType_Agent_Monster_Bottom_Normal.Normal)
    {
        mAgent = agent;
    }

    public override void Update()
    {
        Collider2D[] colliders = mAgent.Actor.TriggerHandler.GetColliders(ETriggerType_Monster_Bottom_Normal.Enemy);

        for(int i=0; i<colliders.Length && colliders[i] != null; i++)
        {
            Giant_Player giant = colliders[i].GetComponentInParent<Giant_Player>();
            if (giant != null && giant.State != EStateType_Giant_Player.Die)
            {
                // 거인 감지시 왼쪽 이동
                mAgent.SetMoveHorizontal(-1.0f);
                return;
            }
        }
        // 거인 미감지시 정지
        mAgent.SetMoveHorizontal(0.0f);
    }
}
