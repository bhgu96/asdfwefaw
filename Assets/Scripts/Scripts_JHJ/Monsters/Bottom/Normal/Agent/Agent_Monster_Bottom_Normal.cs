﻿using UnityEngine;

public enum EStateType_Agent_Monster_Bottom_Normal
{
    None = -1, 

    Normal = 0, 

    Idle = 0, 
    Move = 1, 

    Count
}

public class Agent_Monster_Bottom_Normal : Agent<EStateType_Agent_Monster_Bottom_Normal>
{
    public Monster_Bottom_Normal Actor { get; private set; }

    public Agent_Monster_Bottom_Normal(Monster_Bottom_Normal actor) : base(0)
    {
        Actor = actor;

        SetStates(new Agent_Monster_Bottom_Normal_Normal(this));
    }
}
