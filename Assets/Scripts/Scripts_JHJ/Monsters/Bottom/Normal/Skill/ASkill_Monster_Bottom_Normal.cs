﻿using UnityEngine;
using System;
using System.Collections.Generic;
using Object;

public enum ESkillType_Monster_Bottom_Normal
{
    None = -1,



    Count
}

public interface ISkill_Monster_Bottom_Normal
    : ISkill_Monster_Bottom, IHandleable<ESkillType_Monster_Bottom_Normal>
{

}

public abstract class ASkill_Monster_Bottom_Normal<TUser, EState>
    : ASkill_Monster_Bottom<TUser, EState>, ISkill_Monster_Bottom_Normal
    where TUser : IActor_Monster_Bottom
    where EState : Enum
{
    /// <summary>
    /// 이름 태그
    /// </summary>
    public override string NameTag
    {
        get { return mInfo.NameTag; }
    }
    /// <summary>
    /// 설명 태그
    /// </summary>
    public override string DescriptionTag
    {
        get { return mInfo.DescriptionTag; }
    }
    /// <summary>
    /// 옵션 태그
    /// </summary>
    public override string OptionTag
    {
        get { return mInfo.OptionTag; }
    }
    /// <summary>
    /// 부가 옵션 태그
    /// </summary>
    public override string AdditionalOptionTag
    {
        get { return mInfo.AdditionalOptionTag; }
    }
    /// <summary>
    /// 기본 쿨타입
    /// </summary>
    public override float CoolTime_Default
    {
        get { return mInfo.CoolTime; }
    }
    /// <summary>
    /// 가변인자
    /// </summary>
    public override float[] Parameters
    {
        get { return mInfo.Parameters; }
    }

    /// <summary>
    /// 스킬 타입
    /// </summary>
    public abstract ESkillType_Monster_Bottom_Normal Type { get; }

    private SkillInfo_Monster_Bottom_Normal mInfo;

    protected override void Awake()
    {
        base.Awake();

        mInfo = DataManager.GetSkillInfo(Type);
    }
}

public class SkillInfo_Monster_Bottom_Normal
{
    public string NameTag { get; }
    public string DescriptionTag { get; }
    public string OptionTag { get; }
    public string AdditionalOptionTag { get; }
    public float CoolTime { get; }
    public float[] Parameters { get; }

    public SkillInfo_Monster_Bottom_Normal(Dictionary<string, string> skillDict)
    {
        Parameters = new float[GameConstant.MAX_SKILL_PARAMETERS];

        NameTag = skillDict["Name Tag"];
        DescriptionTag = skillDict["Desc Tag"];
        OptionTag = skillDict["Option Tag"];
        AdditionalOptionTag = skillDict["Additional Tag"];
        CoolTime = float.Parse(skillDict["Cool Time"]);
        
        for(int i=0; i< GameConstant.MAX_SKILL_PARAMETERS; i++)
        {
            Parameters[i] = float.Parse(skillDict[i.ToString()]);
        }

    }
}
