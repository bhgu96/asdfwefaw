﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public struct SpawnPattern
{
    public SpawnInfo[] SpawnInfo;
}

[System.Serializable]
public struct SpawnInfo
{
    public GameObject GameObject;
    [Range(0.0f, 1.0f)]
    public float Possibility;
}

public class MonsterSpawner : MonoBehaviour
{
    [SerializeField]
    private float mDistanceOfMonsters;

    [SerializeField]
    private Transform mEndOfSpawn;

    [SerializeField]
    private float mRadiusOfGizmos;

    [SerializeField]
    private SpawnPattern[] mSpawnPatterns;
    

    private void OnDrawGizmos()
    {
        if (mEndOfSpawn != null && mDistanceOfMonsters > 0.0f)
        {
            Gizmos.color = new Color(1.0f, 0.0f, 0.8f, 0.8f);

            Vector2 startPosition = transform.position;
            Gizmos.DrawSphere(startPosition, mRadiusOfGizmos);

            Vector2 endPosition = mEndOfSpawn.position;
            float distanceFromStartToEnd = Vector2.Distance(startPosition, endPosition);

            Vector2 spawnVector = endPosition - startPosition;
            spawnVector.Normalize();
            float velocity_x = spawnVector.x * mDistanceOfMonsters;
            float velocity_y = spawnVector.y * mDistanceOfMonsters;

            Vector2 spawnPosition = new Vector2(startPosition.x + velocity_x, startPosition.y + velocity_y);
            float sumOfDistance = mDistanceOfMonsters;

            while (sumOfDistance < distanceFromStartToEnd)
            {
                Gizmos.DrawSphere(spawnPosition, mRadiusOfGizmos);
                spawnPosition = new Vector2(spawnPosition.x + velocity_x, spawnPosition.y + velocity_y);
                sumOfDistance += mDistanceOfMonsters;
            }
        }
    }

    private void Awake()
    {
        Vector2 startPosition = transform.position;
        Generate(0, startPosition);
        Vector2 endPosition = mEndOfSpawn.position;
        float distanceFromStartToEnd = Vector2.Distance(startPosition, endPosition);

        Vector2 spawnVector = endPosition - startPosition;
        spawnVector.Normalize();
        float velocity_x = spawnVector.x * mDistanceOfMonsters;
        float velocity_y = spawnVector.y * mDistanceOfMonsters;

        Vector2 spawnPosition = new Vector2(startPosition.x + velocity_x, startPosition.y + velocity_y);
        float sumOfDistance = mDistanceOfMonsters;
        int level = 1;
        while (sumOfDistance < distanceFromStartToEnd)
        {
            Generate(level, spawnPosition);
            spawnPosition = new Vector2(spawnPosition.x + velocity_x, spawnPosition.y + velocity_y);
            sumOfDistance += mDistanceOfMonsters;
            level++;
        }
    }

    private void Generate(int level, Vector2 position)
    {
        if (level >= mSpawnPatterns.Length)
        {
            level = mSpawnPatterns.Length - 1;
        }

        float possibility = Random.Range(0.0f, 1.0f);
        float sumOfPossibility = 0.0f;
        
        for(int i=0; i<mSpawnPatterns[level].SpawnInfo.Length; i++)
        {
            sumOfPossibility += mSpawnPatterns[level].SpawnInfo[i].Possibility;
            if(possibility < sumOfPossibility)
            {
                IActor actor = Instantiate(mSpawnPatterns[level].SpawnInfo[i].GameObject, position, Quaternion.identity, transform).GetComponent<IActor>();
                actor.HorizontalDirection = EHorizontalDirection.Left;
                return;
            }
        }
    }
}
