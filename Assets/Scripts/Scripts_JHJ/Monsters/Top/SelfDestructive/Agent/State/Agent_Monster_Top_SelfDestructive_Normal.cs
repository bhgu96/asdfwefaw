﻿using UnityEngine;

public class Agent_Monster_Top_SelfDestructive_Normal : State<EStateType_Agent_Monster_Top_SelfDestructive>
{
    private Agent_Monster_Top_SelfDestructive mAgent;

    public Agent_Monster_Top_SelfDestructive_Normal(Agent_Monster_Top_SelfDestructive agent) : base(EStateType_Agent_Monster_Top_SelfDestructive.Normal)
    {
        mAgent = agent;
    }

    public override void Update()
    {
        Collider2D[] colliders = mAgent.Actor.TriggerHandler.GetColliders(ETriggerType_Monster_Top_SelfDestructive.Destructive);
        if(colliders[0] != null)
        {
            // 무언가가 떨어지는 조건 트리거에 감지된 경우
            mAgent.SetMoveHorizontal(0.0f);
            for(int i=0; i<mAgent.Skill.Length; i++)
            {
                if(!mAgent.Skill[i])
                {
                    mAgent.SetSkill(true, i);
                }
                else
                {
                    mAgent.SetSkill(false, i);
                }
            }
            return;
        }

        colliders = mAgent.Actor.TriggerHandler.GetColliders(ETriggerType_Monster_Top_SelfDestructive.Enemy);

        for(int i=0; i<colliders.Length && colliders[i] != null; i++)
        {
            Giant_Player giant = colliders[i].GetComponentInParent<Giant_Player>();

            if(giant != null && giant.State != EStateType_Giant_Player.Die)
            {
                // 거인을 감지한 경우
                mAgent.SetMoveHorizontal(-1.0f);
                for(int j=0; j<mAgent.Skill.Length; j++)
                {
                    mAgent.SetSkill(false, j);
                }
                return;
            }
        }

        // 어떠한 것도 감지하지 못한 경우
        mAgent.SetMoveHorizontal(0.0f);
        
        for(int i=0; i<mAgent.Skill.Length; i++)
        {
            mAgent.SetSkill(false, i);
        }
    }
}
