﻿using UnityEngine;

public enum EStateType_Agent_Monster_Top_SelfDestructive
{
    None = -1, 

    Normal = 0, 

    Count
}

public class Agent_Monster_Top_SelfDestructive : Agent<EStateType_Agent_Monster_Top_SelfDestructive>
{
    public Monster_Top_SelfDestructive Actor { get; private set; }

    public Agent_Monster_Top_SelfDestructive(Monster_Top_SelfDestructive actor, int countOfSkills) : base(countOfSkills)
    {
        Actor = actor;

        SetStates(new Agent_Monster_Top_SelfDestructive_Normal(this));
    }
}
