﻿using UnityEngine;
using Object;

public enum EStateType_Monster_Top_SelfDestructive
{
    None = -1,

    Idle = 0,
    Advance = 1,
    Hit = 2,
    Die = 3,
    Restrain = 4,
    FallPrepare = 5,
    Fall = 6,
    Destruct = 7,

    Count
}

public enum ETriggerType_Monster_Top_SelfDestructive
{
    None = -1,

    Reflex = 0,
    Enemy = 1,
    Destructive = 2, 

    Count
}

public enum EAudioType_Monster_Top_SelfDestructive
{
    None = -1,

    Count
}

[RequireComponent(typeof(TriggerHandler_Monster_Top_SelfDestructive), typeof(AudioHandler_Monster_Top_SelfDestructive), typeof(AttackHandler_Monster_Top_SelfDestructive))]
[RequireComponent(typeof(SkillHandler_Monster_Top_SelfDestructive))]
public class Monster_Top_SelfDestructive
    : AActor_Monster_Top<EStateType_Monster_Top_SelfDestructive>, IPoolable<EMonsterType>
{
    [SerializeField]
    private EMonsterType_Top_SelfDestructive mType;
    public new EMonsterType_Top_SelfDestructive Type { get { return mType; } }

    [SerializeField]
    private EMonsterRank mMonsterRank;
    public override EMonsterRank MonsterRank { get { return mMonsterRank; } }

    public override ECombatType CombatType
    {
        get { return ECombatType.SelfDestructive; }
    }

    public TriggerHandler_Monster_Top_SelfDestructive TriggerHandler { get; private set; }
    public AudioHandler_Monster_Top_SelfDestructive AudioHandler { get; private set; }
    public AttackHandler_Monster_Top_SelfDestructive AttackHandler { get; private set; }
    public SkillHandler_Monster_Top_SelfDestructive SkillHandler { get; private set; }

    private Status_Monster_Top_SelfDestructive mStatus;
    public new Status_Monster_Top_SelfDestructive Status
    {
        get { return mStatus; }
        protected set { base.Status = value; mStatus = value; }
    }

    [SerializeField]
    private ESkillType_Monster_Top_SelfDestructive[] mSkills;
    /// <summary>
    /// 사용할 스킬
    /// </summary>
    public ESkillType_Monster_Top_SelfDestructive[] Skills { get { return mSkills; } }

    /// <summary>
    /// 스킬 사용 입력을 검사하는 메소드
    /// </summary>
    public bool CheckSkill()
    {
        if (CanCast)
        {
            for (int i = 0; i < Skills.Length; i++)
            {
                if (Input.SkillDown[i])
                {
                    ISkill_Monster_Top_SelfDestructive skill = SkillHandler.Get(Skills[i]);
                    if (skill != null && skill.Activate())
                    {
                        return true;
                    }
                }
            }
        }

        return false;
    }

    public override void OnRestrain()
    {
        if (CastingSkill != null)
        {
            CastingSkill.Deactivate();
        }
        State = EStateType_Monster_Top_SelfDestructive.Restrain;
    }

    protected override void OnDamage()
    {
        base.OnDamage();
        if (!DebuffHandler.Get(EDebuffType.Restrain_Bush).IsActivated
            && Damage.CanReflex && Damage.SrcInstance is IDamagable damagable)
        {
            Damage damage = damagable.Damage;

            damage.SetDamage(false, true, false
                , EEffectType.None, EDamageType.Monster, ElementalType
                , Status.Level, Status.GetEA(ElementalType), Status.Offense, Status.Force
                , AttackHandler.Get(ETriggerType_Monster_Top_SelfDestructive.Reflex).transform.position, this);

            damagable.ActivateDamage();
        }
    }

    protected override void OnDeactivate()
    {
        base.OnDeactivate();
        Status.Life = Status.MaxLife;
        State = EStateType_Monster_Top_SelfDestructive.Idle;
    }

    protected override void OnDie()
    {
        base.OnDie();

        if (State != EStateType_Monster_Top_SelfDestructive.Destruct)
        {
            State = EStateType_Monster_Top_SelfDestructive.Die;
        }
    }

    protected override void OnHit()
    {
        if (CastingSkill != null)
        {
            CastingSkill.Deactivate();
        }

        if (State != EStateType_Monster_Top_SelfDestructive.Destruct)
        {
            State = EStateType_Monster_Top_SelfDestructive.Hit;
        }
    }

    protected override void Awake()
    {
        base.Awake();
        Status = new Status_Monster_Top_SelfDestructive(this);
        SetInputs(new Agent_Monster_Top_SelfDestructive(this, Skills.Length));
        TriggerHandler = GetComponent<TriggerHandler_Monster_Top_SelfDestructive>();
        AudioHandler = GetComponent<AudioHandler_Monster_Top_SelfDestructive>();
        AttackHandler = GetComponent<AttackHandler_Monster_Top_SelfDestructive>();
        SkillHandler = GetComponent<SkillHandler_Monster_Top_SelfDestructive>();

        SetStates(new Monster_Top_SelfDestructive_Idle(this)
            , new Monster_Top_SelfDestructive_Advance(this)
            , new Monster_Top_SelfDestructive_Hit(this)
            , new Monster_Top_SelfDestructive_Die(this)
            , new Monster_Top_SelfDestructive_Restrain(this)
            , new Monster_Top_SelfDestructive_FallPrepare(this)
            , new Monster_Top_SelfDestructive_Fall(this)
            , new Monster_Top_SelfDestructive_Destruct(this));
    }
}
