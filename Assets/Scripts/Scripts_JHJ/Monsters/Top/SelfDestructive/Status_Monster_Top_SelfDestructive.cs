﻿using System.Collections.Generic;

public class Status_Monster_Top_SelfDestructive : AStatus_Monster_Top
{
    public new Monster_Top_SelfDestructive Actor { get; }

    /// <summary>
    /// 이름 태그
    /// </summary>
    public override string NameTag
    {
        get { return mDefault.NameTag; }
    }
    /// <summary>
    /// 플레이버 텍스트 태그
    /// </summary>
    public override string FlavorTag
    {
        get { return mDefault.FlavorTag; }
    }
    /// <summary>
    /// 기본 레벨
    /// </summary>
    public override int Level_Default
    {
        get { return mDefault.Level; }
    }
    /// <summary>
    /// 기본 경험치 획득량
    /// </summary>
    public override int Exp_Default
    {
        get { return mDefault.Exp; }
    }
    /// <summary>
    /// 기본 소울 획득량
    /// </summary>
    public override int Souls_Default
    {
        get { return mDefault.Souls; }
    }
    /// <summary>
    /// 기본 이동 속도
    /// </summary>
    public override float MoveSpeed_Default
    {
        get { return mDefault.MoveSpeed; }
    }
    /// <summary>
    /// 기본 최대 생명력
    /// </summary>
    public override float MaxLife_Default
    {
        get { return mDefault.MaxLife; }
    }
    /// <summary>
    /// 기본 공격력
    /// </summary>
    public override float Offense_Default
    {
        get { return mDefault.Offense; }
    }
    /// <summary>
    /// 기본 방어력
    /// </summary>
    public override float Defense_Default
    {
        get { return mDefault.Defense; }
    }
    /// <summary>
    /// 기본 힘
    /// </summary>
    public override float Force_Default
    {
        get { return mDefault.Force; }
    }
    /// <summary>
    /// 기본 무게
    /// </summary>
    public override float Weight_Default
    {
        get { return mDefault.Weight; }
    }

    // 기본 마찰 계수
    public override float Friction_Default
    {
        get { return mDefault.Friction; }
    }

    /// <summary>
    /// 기본 속성 친화도
    /// </summary>
    /// <param name="e"></param>
    /// <returns></returns>
    public override int GetEA_Default(EElementalType e)
    {
        return mDefault.GetEA(e);
    }

    private DefaultStatus_Monster_Top_SelfDestructive mDefault;

    public Status_Monster_Top_SelfDestructive(Monster_Top_SelfDestructive actor) : base(actor)
    {
        Actor = actor;
        mDefault = DataManager.GetDefaultStatus(actor.Type);
        Life = MaxLife;
    }
}

public class DefaultStatus_Monster_Top_SelfDestructive
{
    public string NameTag { get; }
    public string FlavorTag { get; }
    public int Level { get; }
    public int Exp { get; }
    public int Souls { get; }
    public float MoveSpeed { get; }
    public float MaxLife { get; }
    public float Offense { get; }
    public float Defense { get; }
    public float Force { get; }
    public float Weight { get; }

    public float Friction { get; }

    private Dictionary<EElementalType, int> mEaDict;

    public DefaultStatus_Monster_Top_SelfDestructive(Dictionary<string, string> statusDict)
    {
        mEaDict = new Dictionary<EElementalType, int>();

        NameTag = statusDict["Name Tag"];
        FlavorTag = statusDict["Flavor Tag"];
        Level = int.Parse(statusDict["Level"]);
        Exp = int.Parse(statusDict["Exp"]);
        Souls = int.Parse(statusDict["Souls"]);
        MoveSpeed = float.Parse(statusDict["Move Speed"]);
        MaxLife = float.Parse(statusDict["Max Life"]);
        Offense = float.Parse(statusDict["Offense"]);
        Defense = float.Parse(statusDict["Defense"]);
        Force = float.Parse(statusDict["Force"]);
        Weight = float.Parse(statusDict["Weight"]);

        Friction = float.Parse(statusDict["Friction"]);

        for (EElementalType e = EElementalType.None + 1; e < EElementalType.Count; e++)
        {
            mEaDict.Add(e, int.Parse(statusDict[e.ToString()]));
        }
    }

    public int GetEA(EElementalType e)
    {
        return mEaDict[e];
    }
}
