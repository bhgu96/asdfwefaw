﻿using UnityEngine;

public class Monster_Top_SelfDestructive_Idle : State<EStateType_Monster_Top_SelfDestructive>
{
    private Monster_Top_SelfDestructive mActor;

    public Monster_Top_SelfDestructive_Idle(Monster_Top_SelfDestructive actor) : base(EStateType_Monster_Top_SelfDestructive.Idle)
    {
        mActor = actor;
    }

    public override void Start()
    {
        mActor.Animator.SetInteger("state", (int)Type);
    }

    public override void Update()
    {
        mActor.CheckSkill();

        if(mActor.Input.MoveHorizontal < -Mathf.Epsilon)
        {
            mActor.State = EStateType_Monster_Top_SelfDestructive.Advance;
            return;
        }
    }
}
