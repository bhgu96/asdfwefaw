﻿using UnityEngine;

public class Monster_Top_SelfDestructive_Fall : State<EStateType_Monster_Top_SelfDestructive>
{
    private Monster_Top_SelfDestructive mActor;

    public Monster_Top_SelfDestructive_Fall(Monster_Top_SelfDestructive actor) : base(EStateType_Monster_Top_SelfDestructive.Fall)
    {
        mActor = actor;
    }

    public override void Start()
    {
        mActor.Animator.SetInteger("state", (int)Type);
        mActor.Rigidbody.gravityScale = 1.0f;
    }

    public override void End()
    {
        mActor.Rigidbody.gravityScale = -1.0f;
    }
}
