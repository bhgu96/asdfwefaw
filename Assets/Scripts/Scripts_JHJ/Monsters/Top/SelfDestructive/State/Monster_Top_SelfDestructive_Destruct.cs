﻿using UnityEngine;

public class Monster_Top_SelfDestructive_Destruct : State<EStateType_Monster_Top_SelfDestructive>
{
    private Monster_Top_SelfDestructive mActor;

    public Monster_Top_SelfDestructive_Destruct(Monster_Top_SelfDestructive actor) : base(EStateType_Monster_Top_SelfDestructive.Destruct)
    {
        mActor = actor;
    }

    public override void Start()
    {
        mActor.Animator.SetInteger("state", (int)Type);

        mActor.Velocity = Vector2.zero;
        mActor.Rigidbody.constraints = RigidbodyConstraints2D.FreezeAll;
        mActor.MainCollider.enabled = false;
    }

    public override void End()
    {
        mActor.Velocity = Vector2.zero;
        mActor.Rigidbody.constraints = RigidbodyConstraints2D.FreezeRotation;
        mActor.MainCollider.enabled = true;
    }
}
