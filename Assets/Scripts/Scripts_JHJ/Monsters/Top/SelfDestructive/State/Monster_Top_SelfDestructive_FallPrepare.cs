﻿using UnityEngine;

public class Monster_Top_SelfDestructive_FallPrepare : State<EStateType_Monster_Top_SelfDestructive>
{
    private Monster_Top_SelfDestructive mActor;

    public Monster_Top_SelfDestructive_FallPrepare(Monster_Top_SelfDestructive actor) : base(EStateType_Monster_Top_SelfDestructive.FallPrepare)
    {
        mActor = actor;
    }

    public override void Start()
    {
        mActor.Animator.SetInteger("state", (int)Type);
        mActor.Animator.speed = mActor.Status.CastSpeed;
    }

    public override void End()
    {
        mActor.Animator.speed = 1.0f;
    }
}
