﻿using UnityEngine;

public class Monster_Top_SelfDestructive_Hit : State<EStateType_Monster_Top_SelfDestructive>
{
    private Monster_Top_SelfDestructive mActor;
    private bool mIsStarted = false;

    public Monster_Top_SelfDestructive_Hit(Monster_Top_SelfDestructive actor) : base(EStateType_Monster_Top_SelfDestructive.Hit)
    {
        mActor = actor;
    }

    public override void Start()
    {
        if(!mIsStarted)
        {
            mIsStarted = true;
            mActor.Animator.SetInteger("state", (int)Type);
        }
        else
        {
            mActor.Animator.SetTrigger("hit");
        }
    }

    public override void End()
    {
        mIsStarted = false;
    }

    public override void FixedUpdate()
    {
        Vector2 curVelocity = mActor.Velocity;

        if(Mathf.Abs(curVelocity.x) < GameConstant.THRESHOLD_HIT
            && Mathf.Abs(curVelocity.y) < GameConstant.THRESHOLD_HIT)
        {
            mActor.State = EStateType_Monster_Top_SelfDestructive.Idle;
            return;
        }
    }
}
