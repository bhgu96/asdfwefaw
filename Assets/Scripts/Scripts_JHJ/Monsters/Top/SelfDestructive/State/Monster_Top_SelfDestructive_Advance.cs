﻿using UnityEngine;

public class Monster_Top_SelfDestructive_Advance : State<EStateType_Monster_Top_SelfDestructive>
{
    private Monster_Top_SelfDestructive mActor;

    public Monster_Top_SelfDestructive_Advance(Monster_Top_SelfDestructive actor) : base(EStateType_Monster_Top_SelfDestructive.Advance)
    {
        mActor = actor;
    }

    public override void Start()
    {
        mActor.Animator.SetInteger("state", (int)Type);
    }

    public override void FixedUpdate()
    {
        if(mActor.Input.MoveHorizontal < -Mathf.Epsilon)
        {
            mActor.MoveHorizontal(mActor.Status.MoveSpeed, EHorizontalDirection.Left);
        }
        else
        {
            mActor.State = EStateType_Monster_Top_SelfDestructive.Idle;
            return;
        }
    }

    public override void Update()
    {
        mActor.CheckSkill();
    }
}
