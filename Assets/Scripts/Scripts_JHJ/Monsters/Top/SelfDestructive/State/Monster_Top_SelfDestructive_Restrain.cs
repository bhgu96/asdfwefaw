﻿using UnityEngine;

public class Monster_Top_SelfDestructive_Restrain : State<EStateType_Monster_Top_SelfDestructive>
{
    private Monster_Top_SelfDestructive mActor;

    public Monster_Top_SelfDestructive_Restrain(Monster_Top_SelfDestructive actor) : base(EStateType_Monster_Top_SelfDestructive.Restrain)
    {
        mActor = actor;
    }

    public override void Start()
    {
        mActor.Animator.SetInteger("state", (int)Type);
    }

    public override void Update()
    {
        if(!mActor.DebuffHandler.Get(EDebuffType.Restrain_Bush).IsActivated)
        {
            mActor.State = EStateType_Monster_Top_SelfDestructive.Idle;
            return;
        }
    }
}
