﻿using UnityEngine;

public class Skill_Destruction_Monster_Top_SelfDestructive_Prepare : State<EStateType_Skill_Destruction_Monster_Top_SelfDestructive>
{
    private Skill_Destruction_Monster_Top_SelfDestructive mSkill;

    public Skill_Destruction_Monster_Top_SelfDestructive_Prepare(Skill_Destruction_Monster_Top_SelfDestructive skill) : base(EStateType_Skill_Destruction_Monster_Top_SelfDestructive.Prepare)
    {
        mSkill = skill;
    }

    public override void Start()
    {
        mSkill.Animator.SetInteger("state", (int)Type);
        mSkill.Animator.speed = mSkill.CastSpeed;
    }

    public override void End()
    {
        mSkill.Animator.speed = 1.0f;
    }
}
