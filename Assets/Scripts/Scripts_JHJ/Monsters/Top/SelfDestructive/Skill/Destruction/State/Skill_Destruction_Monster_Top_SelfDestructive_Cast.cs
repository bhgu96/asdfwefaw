﻿using UnityEngine;

public class Skill_Destruction_Monster_Top_SelfDestructive_Cast : State<EStateType_Skill_Destruction_Monster_Top_SelfDestructive>
{
    private Skill_Destruction_Monster_Top_SelfDestructive mSkill;

    public Skill_Destruction_Monster_Top_SelfDestructive_Cast(Skill_Destruction_Monster_Top_SelfDestructive skill) : base(EStateType_Skill_Destruction_Monster_Top_SelfDestructive.Cast)
    {
        mSkill = skill;
    }

    public override void Start()
    {
        mSkill.Animator.SetInteger("state", (int)Type);
        mSkill.User.State = EStateType_Monster_Top_SelfDestructive.Fall;
    }

    public override void FixedUpdate()
    {
        if(mSkill.CanDestruct())
        {
            mSkill.Deactivate();
        }
    }
}
