﻿using UnityEngine;

public class Skill_Destruction_Monster_Top_SelfDestructive_Destruct : State<EStateType_Skill_Destruction_Monster_Top_SelfDestructive>
{
    private Skill_Destruction_Monster_Top_SelfDestructive mSkill;

    public Skill_Destruction_Monster_Top_SelfDestructive_Destruct(Skill_Destruction_Monster_Top_SelfDestructive skill) : base(EStateType_Skill_Destruction_Monster_Top_SelfDestructive.Destruct)
    {
        mSkill = skill;
    }

    public override void Start()
    {
        mSkill.Animator.SetInteger("state", (int)Type);
    }
}
