﻿using UnityEngine;
using System.Collections;

public enum EStateType_Skill_Destruction_Monster_Top_SelfDestructive
{
    None = -1, 

    Idle = 0,
    Prepare = 1,
    Cast = 2,
    Destruct = 3,

    Count
}

public enum ETriggerType_Skill_Destruction_Monster_Top_SelfDestructive
{
    None = -1, 

    DestructiveTrigger = 0, 
    TerrainTrigger = 1, 
    Destruct = 2, 

    Count
}

public enum EAudioType_Skill_Destruction_Monster_Top_SelfDestructive
{
    None = -1, 


    Count
}

[RequireComponent(typeof(TriggerHandler_Skill_Destruction_Monster_Top_SelfDestructive), typeof(AudioHandler_Skill_Destruction_Monster_Top_SelfDestructive), typeof(AttackHandler_Skill_Destruction_Monster_Top_SelfDestructive))]
public class Skill_Destruction_Monster_Top_SelfDestructive : ASkill_Monster_Top_SelfDestructive<EStateType_Skill_Destruction_Monster_Top_SelfDestructive>
{
    public override float CastTime
    {
        get { return 0.3f; }
    }

    [SerializeField]
    private ESkillType_Monster_Top_SelfDestructive mType;
    public override ESkillType_Monster_Top_SelfDestructive Type
    {
        get { return mType; }
    }

    [SerializeField]
    private EElementalType mElementalType;
    public EElementalType ElementalType
    {
        get { return mElementalType; }
    }

    public float SkillOffense_Default
    {
        get { return Parameters[0]; }
    }
    public float SkillOffense_Increase
    {
        get;
    }
    public float SkillOffense
    {
        get { return SkillOffense_Default * (1.0f + SkillOffense_Increase); }
    }

    public TriggerHandler_Skill_Destruction_Monster_Top_SelfDestructive TriggerHandler { get; private set; }
    public AudioHandler_Skill_Destruction_Monster_Top_SelfDestructive AudioHandler { get; private set; }
    public AttackHandler_Skill_Destruction_Monster_Top_SelfDestructive AttackHandler { get; private set; }

    /// <summary>
    /// 폭발 가능한 상태인지 반환하는 메소드
    /// </summary>
    /// <returns></returns>
    public bool CanDestruct()
    {
        Collider2D[] colliders = TriggerHandler.GetColliders(ETriggerType_Skill_Destruction_Monster_Top_SelfDestructive.DestructiveTrigger);

        if(colliders[0] != null)
        {
            return true;
        }

        colliders = TriggerHandler.GetColliders(ETriggerType_Skill_Destruction_Monster_Top_SelfDestructive.TerrainTrigger);

        if(colliders[0] != null)
        {
            return true;
        }

        return false;
    }

    /// <summary>
    /// 폭발 피해 전달 메소드
    /// </summary>
    public void Bomb()
    {
        Collider2D[] colliders = TriggerHandler.GetColliders(ETriggerType_Skill_Destruction_Monster_Top_SelfDestructive.Destruct);

        for(int i=0; i<colliders.Length && colliders[i] != null; i++)
        {
            IDamagable damagable = colliders[i].GetComponentInParent<IDamagable>();

            if(damagable != null)
            {
                damagable.Damage.SetDamage(false, true, false, EEffectType.None, EDamageType.Monster, ElementalType,
                    User.Status.Level, User.Status.GetEA(ElementalType), SkillOffense * User.Status.Offense, User.Status.Force,
                    AttackHandler.Get(ETriggerType_Skill_Destruction_Monster_Top_SelfDestructive.Destruct).transform.position, User);

                damagable.ActivateDamage();
            }
        }
    }

    protected override void OnActivate()
    {
        base.OnActivate();
        SetDisableCast();
        State = EStateType_Skill_Destruction_Monster_Top_SelfDestructive.Prepare;
        User.State = EStateType_Monster_Top_SelfDestructive.FallPrepare;
    }

    protected override void OnDeactivate()
    {
        base.OnDeactivate();
        SetEnableCast();
        State = EStateType_Skill_Destruction_Monster_Top_SelfDestructive.Destruct;
        User.State = EStateType_Monster_Top_SelfDestructive.Destruct;
    }

    protected override void Awake()
    {
        base.Awake();

        TriggerHandler = GetComponent<TriggerHandler_Skill_Destruction_Monster_Top_SelfDestructive>();
        AudioHandler = GetComponent<AudioHandler_Skill_Destruction_Monster_Top_SelfDestructive>();
        AttackHandler = GetComponent<AttackHandler_Skill_Destruction_Monster_Top_SelfDestructive>();

        SetStates(new Skill_Destruction_Monster_Top_SelfDestructive_Idle(this)
            , new Skill_Destruction_Monster_Top_SelfDestructive_Prepare(this)
            , new Skill_Destruction_Monster_Top_SelfDestructive_Cast(this)
            , new Skill_Destruction_Monster_Top_SelfDestructive_Destruct(this));
    }

}
