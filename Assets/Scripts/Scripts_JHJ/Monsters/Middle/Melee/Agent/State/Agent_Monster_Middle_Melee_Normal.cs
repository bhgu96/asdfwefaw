﻿using UnityEngine;

public class Agent_Monster_Middle_Melee_Normal : State<EStateType_Agent_Monster_Middle_Melee>
{
    private Agent_Monster_Middle_Melee mAgent;

    private float mMinDistance;
    private float mMaxDistance;
    private float mMinHeight;
    private float mMaxHeight;

    public Agent_Monster_Middle_Melee_Normal(Agent_Monster_Middle_Melee agent) : base(EStateType_Agent_Monster_Middle_Melee.Normal)
    {
        mAgent = agent;

        mMinDistance = Random.Range(mAgent.Actor.Status.MinDistance, mAgent.Actor.Status.MaxDistance) - 1.0f;
        mMaxDistance = mMinDistance + 2.0f;
        mMinHeight = Random.Range(mAgent.Actor.Status.MinHeight, mAgent.Actor.Status.MaxHeight) - 1.0f;
        mMaxHeight = mMinHeight + 2.0f;
    }

    public override void Start()
    {
        mMinDistance = Random.Range(mAgent.Actor.Status.MinDistance, mAgent.Actor.Status.MaxDistance) - 1.0f;
        mMaxDistance = mMinDistance + 2.0f;
        mMinHeight = Random.Range(mAgent.Actor.Status.MinHeight, mAgent.Actor.Status.MaxHeight) - 1.0f;
        mMaxHeight = mMinHeight + 2.0f;
    }

    public override void Update()
    {
        // 현재 스킬을 시전중이라면 Cast 상태로 전이
        if(mAgent.Actor.CastingSkill != null && mAgent.Actor.CastingSkill.IsActivated)
        {
            mAgent.SetMoveVertical(0.0f);
            mAgent.SetMoveHorizontal(1.0f);
            mAgent.SetDash(false);

            for(int i=0; i<mAgent.Skill.Length; i++)
            {
                mAgent.SetSkill(false, i);
            }

            mAgent.State = EStateType_Agent_Monster_Middle_Melee.Cast;
            return;
        }

        Collider2D[] colliders = mAgent.Actor.TriggerHandler.GetColliders(ETriggerType_Monster_Middle_Melee.Enemy);

        for (int i = 0; i < colliders.Length && colliders[i] != null; i++)
        {
            Giant_Player giant = colliders[i].GetComponentInParent<Giant_Player>();
            // 거인을 감지한 경우
            if (giant != null && giant.State != EStateType_Giant_Player.Die)
            {
                Vector2 agentPosition = mAgent.Actor.transform.position;
                Vector2 giantPosition = giant.transform.position;

                float height = agentPosition.y - giantPosition.y;
                float distance = agentPosition.x - giantPosition.x;

                bool canActivate = false;
                // 모든 스킬에 대해
                for (int j = 0; j < mAgent.Skill.Length; j++)
                {
                    // 스킬이 사용가능하다면
                    if (mAgent.Actor.SkillHandler.Get(mAgent.Actor.Skills[j]).CanActivate)
                    {
                        // 스킬 유효 범위에 존재한다면
                        if (mAgent.Actor.TriggerHandler.GetColliders(ETriggerType_Monster_Middle_Melee.Scratchable)[0] != null)
                        {
                            mAgent.SetMoveVertical(0.0f);
                            mAgent.SetMoveHorizontal(1.0f);
                            mAgent.SetDash(false);
                            if (!mAgent.Skill[j])
                            {
                                mAgent.SetSkill(true, j);
                            }
                            else
                            {
                                mAgent.SetSkill(false, j);
                            }
                        }
                        // 스킬 유효 범위에 존재하지 않는다면
                        else
                        {
                            // 거인의 위보다 아래에 위치하는 경우
                            if (height < GameConstant.GIANT_UP)
                            {
                                mAgent.SetMoveVertical(1.0f);
                            }
                            // 거인의 위보다 더 위에 위치하는 경우
                            else if(height > GameConstant.GIANT_UP + GameConstant.GIANT_UP_ERROR)
                            {
                                mAgent.SetMoveVertical(-1.0f);
                            }
                            // 적정 높이에 위치하는 경우
                            else
                            {
                                mAgent.SetMoveVertical(0.0f);
                            }

                            // 거인의 앞보다 더 앞에 위치하는 경우
                            if (distance < GameConstant.GIANT_FRONT)
                            {
                                mAgent.SetMoveHorizontal(1.0f);
                                mAgent.SetDash(true);
                                mAgent.SetSkill(false, j);
                            }
                            // 거인의 앞보다 더 뒤에 위치하는 경우
                            else if(distance > GameConstant.GIANT_FRONT + GameConstant.GIANT_FRONT_ERROR)
                            {
                                mAgent.SetMoveHorizontal(-1.0f);
                                mAgent.SetDash(false);
                                mAgent.SetSkill(false, j);
                            }
                            // 적정 거리에 위치하는 경우
                            else
                            {
                                mAgent.SetMoveHorizontal(1.0f);
                                mAgent.SetDash(false);
                                if (!mAgent.Skill[j])
                                {
                                    mAgent.SetSkill(true, j);
                                }
                                else
                                {
                                    mAgent.SetSkill(false, j);
                                }
                            }
                        }
                        canActivate = true;
                        break;
                    }
                    // 스킬이 사용가능하지 않은 경우
                    else
                    {
                        mAgent.SetSkill(false, j);
                    }
                }
                // 스킬이 사용가능하지 않은 경우
                if(!canActivate)
                {
                    // 최소 높이보다 더 아래에 위치하는 경우
                    if (height < mMinHeight)
                    {
                        mAgent.SetMoveVertical(1.0f);
                    }
                    // 적정 높이에 위치하는 경우
                    else if (height < mMaxHeight)
                    {
                        mAgent.SetMoveVertical(0.0f);
                    }
                    // 최대 높이보다 더 위에 위치하는 경우
                    else
                    {
                        mAgent.SetMoveVertical(-1.0f);
                    }

                    // 최소 거리보다 더 앞에 위치하는 경우
                    if (distance < mMinDistance)
                    {
                        mAgent.SetMoveHorizontal(1.0f);
                        mAgent.SetDash(true);
                    }
                    // 적정 거리에 위치하는 경우
                    else if (distance < mMaxDistance)
                    {
                        mAgent.SetMoveHorizontal(1.0f);
                        mAgent.SetDash(false);
                    }
                    // 최대 거리보다 더 뒤에 위치하는 경우
                    else
                    {
                        mAgent.SetMoveHorizontal(0.0f);
                        mAgent.SetDash(false);
                    }
                }
                return;
            }
        }
        // 거인을 감지하지 못한 경우
        mAgent.SetMoveHorizontal(0.0f);
        mAgent.SetMoveVertical(0.0f);
        mAgent.SetDash(false);
        for(int i=0; i<mAgent.Skill.Length; i++)
        {
            mAgent.SetSkill(false, i);
        }
    }
}
