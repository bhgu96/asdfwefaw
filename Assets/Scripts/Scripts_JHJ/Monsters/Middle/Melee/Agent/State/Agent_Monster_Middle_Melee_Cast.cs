﻿using UnityEngine;

public class Agent_Monster_Middle_Melee_Cast : State<EStateType_Agent_Monster_Middle_Melee>
{
    private Agent_Monster_Middle_Melee mAgent;

    public Agent_Monster_Middle_Melee_Cast(Agent_Monster_Middle_Melee agent) : base(EStateType_Agent_Monster_Middle_Melee.Cast)
    {
        mAgent = agent;
    }

    public override void Update()
    {
        for(int i=0; i<mAgent.Skill.Length; i++)
        {
            mAgent.SetSkill(false, i);
        }

        mAgent.SetMoveVertical(0.0f);
        mAgent.SetMoveHorizontal(1.0f);
        mAgent.SetDash(false);

        // 스킬이 비활성화 된 경우 Normal 상태 전이
        if(mAgent.Actor.CastingSkill == null || !mAgent.Actor.CastingSkill.IsActivated)
        {
            mAgent.State = EStateType_Agent_Monster_Middle_Melee.Normal;
            return;
        }
    }
}
