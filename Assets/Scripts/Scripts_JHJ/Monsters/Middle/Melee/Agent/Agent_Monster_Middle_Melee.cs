﻿using UnityEngine;

public enum EStateType_Agent_Monster_Middle_Melee
{
    None = -1, 

    Normal = 0, 
    Cast = 1, 

    Count
}

public class Agent_Monster_Middle_Melee : Agent<EStateType_Agent_Monster_Middle_Melee>
{
    public Monster_Middle_Melee Actor { get; private set; }

    public Agent_Monster_Middle_Melee(Monster_Middle_Melee actor, int countOfSkills) : base(countOfSkills)
    {
        Actor = actor;

        SetStates(new Agent_Monster_Middle_Melee_Normal(this)
            , new Agent_Monster_Middle_Melee_Cast(this));
    }

}
