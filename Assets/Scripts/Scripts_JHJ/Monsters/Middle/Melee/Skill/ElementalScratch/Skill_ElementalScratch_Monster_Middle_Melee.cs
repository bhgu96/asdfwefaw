﻿using UnityEngine;

public enum EStateType_Skill_ElementalScratch_Monster_Middle_Melee
{
    None = -1, 

    Idle = 0, 
    MiddlePrepare = 1, 
    MiddleCast = 2, 
    MiddleEnd = 3, 
    BottomPrepare = 4, 
    BottomCast = 5, 
    BottomEnd = 6, 

    Count
}

public enum ETriggerType_Skill_ElementalScratch_Monster_Middle_Melee
{
    None = -1, 

    Scratch = 0, 

    Count
}

public enum EAudioType_Skill_ElementalScratch_Monster_Middle_Melee
{
    None = -1, 

    Count
}

[RequireComponent(typeof(TriggerHandler_Skill_ElementalScratch_Monster_Middle_Melee), typeof(AudioHandler_Skill_ElementalScratch_Monster_Middle_Melee), typeof(AttackHandler_Skill_ElementalScratch_Monster_Middle_Melee))]
public class Skill_ElementalScratch_Monster_Middle_Melee : ASkill_Monster_Middle_Melee<EStateType_Skill_ElementalScratch_Monster_Middle_Melee>
{
    public override float CastTime
    {
        get { return 0.6f; }
    }

    [SerializeField]
    private ESkillType_Monster_Middle_Melee mType;
    public override ESkillType_Monster_Middle_Melee Type
    {
        get { return mType; }
    }

    [SerializeField]
    private EElementalType mElementalType;
    public EElementalType ElementalType
    {
        get { return mElementalType; }
    }

    public float SkillOffense_Default
    {
        get { return Parameters[0]; }
    }
    public float SkillOffense_Increase
    {
        get;
    }
    public float SkillOffense
    {
        get { return SkillOffense_Default * (1.0f + SkillOffense_Increase); }
    }

    public TriggerHandler_Skill_ElementalScratch_Monster_Middle_Melee TriggerHandler { get; private set; }
    public AudioHandler_Skill_ElementalScratch_Monster_Middle_Melee AudioHandler { get; private set; }
    public AttackHandler_Skill_ElementalScratch_Monster_Middle_Melee AttackHandler { get; private set; }

    /// <summary>
    /// 공격 메소드
    /// </summary>
    /// <param name="type"></param>
    public void Attack(ETriggerType_Skill_ElementalScratch_Monster_Middle_Melee type)
    {
        Collider2D[] colliders = TriggerHandler.GetColliders(type);

        for(int i=0; i<colliders.Length && colliders[i] != null; i++)
        {
            IDamagable damagable = colliders[i].GetComponentInParent<IDamagable>();
            if(damagable != null)
            {
                damagable.Damage.SetDamage(false, true, false
                    , EEffectType.None, EDamageType.Monster, ElementalType
                    , User.Status.Level, User.Status.GetEA(ElementalType), User.Status.Offense * SkillOffense, User.Status.Force
                    , AttackHandler.Get(type).transform.position, User);

                damagable.ActivateDamage();
            }
        }
    }

    protected override void OnActivate()
    {
        base.OnActivate();
        SetDisableCast();
        // Middle 위치시
        if(User.PositionType == EPositionType.Middle)
        {
            State = EStateType_Skill_ElementalScratch_Monster_Middle_Melee.MiddlePrepare;
            User.State = EStateType_Monster_Middle_Melee.MiddleScratchPrepare;
        }
        // Bottom 위치시
        else if(User.PositionType == EPositionType.Bottom)
        {
            State = EStateType_Skill_ElementalScratch_Monster_Middle_Melee.BottomPrepare;
            User.State = EStateType_Monster_Middle_Melee.BottomScratchPrepare;
        }
    }

    protected override void OnDeactivate()
    {
        base.OnDeactivate();
        SetEnableCast();
        // Middle 위치시
        if (User.PositionType == EPositionType.Middle)
        {
            State = EStateType_Skill_ElementalScratch_Monster_Middle_Melee.MiddleEnd;
            User.State = EStateType_Monster_Middle_Melee.MiddleScratchEnd;
        }
        // Bottom 위치시
        else if (User.PositionType == EPositionType.Bottom)
        {
            State = EStateType_Skill_ElementalScratch_Monster_Middle_Melee.BottomEnd;
            User.State = EStateType_Monster_Middle_Melee.BottomScratchEnd;
        }
    }

    protected override void Awake()
    {
        base.Awake();

        TriggerHandler = GetComponent<TriggerHandler_Skill_ElementalScratch_Monster_Middle_Melee>();
        AudioHandler = GetComponent<AudioHandler_Skill_ElementalScratch_Monster_Middle_Melee>();
        AttackHandler = GetComponent<AttackHandler_Skill_ElementalScratch_Monster_Middle_Melee>();

        SetStates(new Skill_ElementalScratch_Monster_Middle_Melee_Idle(this)
            , new Skill_ElementalScratch_Monster_Middle_Melee_MiddlePrepare(this)
            , new Skill_ElementalScratch_Monster_Middle_Melee_MiddleCast(this)
            , new Skill_ElementalScratch_Monster_Middle_Melee_MiddleEnd(this)
            , new Skill_ElementalScratch_Monster_Middle_Melee_BottomPrepare(this)
            , new Skill_ElementalScratch_Monster_Middle_Melee_BottomCast(this)
            , new Skill_ElementalScratch_Monster_Middle_Melee_BottomEnd(this));
    }
}
