﻿using UnityEngine;

public class Skill_ElementalScratch_Monster_Middle_Melee_BottomCast : State<EStateType_Skill_ElementalScratch_Monster_Middle_Melee>
{
    private Skill_ElementalScratch_Monster_Middle_Melee mSkill;

    public Skill_ElementalScratch_Monster_Middle_Melee_BottomCast(Skill_ElementalScratch_Monster_Middle_Melee skill) : base(EStateType_Skill_ElementalScratch_Monster_Middle_Melee.BottomCast)
    {
        mSkill = skill;
    }

    public override void Start()
    {
        mSkill.Animator.SetInteger("state", (int)Type);
        mSkill.User.State = EStateType_Monster_Middle_Melee.BottomScratch;
    }
}
