﻿using UnityEngine;

public class Skill_ElementalScratch_Monster_Middle_Melee_MiddlePrepare : State<EStateType_Skill_ElementalScratch_Monster_Middle_Melee>
{
    private Skill_ElementalScratch_Monster_Middle_Melee mSkill;

    public Skill_ElementalScratch_Monster_Middle_Melee_MiddlePrepare(Skill_ElementalScratch_Monster_Middle_Melee skill) : base(EStateType_Skill_ElementalScratch_Monster_Middle_Melee.MiddlePrepare)
    {
        mSkill = skill;
    }

    public override void Start()
    {
        mSkill.Animator.SetInteger("state", (int)Type);
        mSkill.Animator.speed = mSkill.CastSpeed;
    }

    public override void End()
    {
        mSkill.Animator.speed = 1.0f;
    }
}
