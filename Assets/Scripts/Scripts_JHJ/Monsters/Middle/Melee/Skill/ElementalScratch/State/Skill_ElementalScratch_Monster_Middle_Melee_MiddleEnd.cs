﻿using UnityEngine;
using System.Collections;

public class Skill_ElementalScratch_Monster_Middle_Melee_MiddleEnd : State<EStateType_Skill_ElementalScratch_Monster_Middle_Melee>
{
    private Skill_ElementalScratch_Monster_Middle_Melee mSkill;

    public Skill_ElementalScratch_Monster_Middle_Melee_MiddleEnd(Skill_ElementalScratch_Monster_Middle_Melee skill) : base(EStateType_Skill_ElementalScratch_Monster_Middle_Melee.MiddleEnd)
    {
        mSkill = skill;
    }

    public override void Start()
    {
        mSkill.Animator.SetInteger("state", (int)Type);
    }
}
