﻿using UnityEngine;
using System.Collections;

public class Skill_ElementalScratch_Monster_Middle_Melee_BottomEnd : State<EStateType_Skill_ElementalScratch_Monster_Middle_Melee>
{
    private Skill_ElementalScratch_Monster_Middle_Melee mSkill;

    public Skill_ElementalScratch_Monster_Middle_Melee_BottomEnd(Skill_ElementalScratch_Monster_Middle_Melee skill) : base(EStateType_Skill_ElementalScratch_Monster_Middle_Melee.BottomEnd)
    {
        mSkill = skill;
    }

    public override void Start()
    {
        mSkill.Animator.SetInteger("state", (int)Type);
    }
}
