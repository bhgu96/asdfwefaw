﻿using UnityEngine;
using System.Collections;

public class Monster_Middle_Melee_MiddleScratch : State<EStateType_Monster_Middle_Melee>
{
    private Monster_Middle_Melee mActor;

    public Monster_Middle_Melee_MiddleScratch(Monster_Middle_Melee actor) : base(EStateType_Monster_Middle_Melee.MiddleScratch)
    {
        mActor = actor;
    }

    public override void Start()
    {
        mActor.Animator.SetInteger("state", (int)Type);
    }

    public override void FixedUpdate()
    {
        mActor.CheckMoveVertical();
        // 오른쪽 이동 입력시 거인 속도를 최대 속도로 가속도 적용
        if (mActor.Input.MoveHorizontal > Mathf.Epsilon)
        {
            mActor.AddVelocity(Giant_Player.Main.Status.MoveSpeed
                , new Vector2(mActor.Status.Acceleration * Time.fixedDeltaTime, 0.0f));
        }
    }
}
