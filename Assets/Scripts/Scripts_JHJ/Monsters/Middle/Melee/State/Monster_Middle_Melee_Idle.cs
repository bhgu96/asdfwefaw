﻿using UnityEngine;

public class Monster_Middle_Melee_Idle : State<EStateType_Monster_Middle_Melee>
{
    private Monster_Middle_Melee mActor;

    public Monster_Middle_Melee_Idle(Monster_Middle_Melee actor) : base(EStateType_Monster_Middle_Melee.Idle)
    {
        mActor = actor;
    }

    public override void Start()
    {
        mActor.Animator.SetInteger("state", (int)Type);
    }

    public override void Update()
    {
        if(mActor.CheckSkill())
        {
            return;
        }

        // 오른쪽 이동 입력시 Retreat 상태 전이
        if(mActor.Input.MoveHorizontal > Mathf.Epsilon)
        {
            mActor.State = EStateType_Monster_Middle_Melee.Retreat;
            return;
        }
        // 왼쪽 이동 입력시 Retreat 상태 전이
        else if(mActor.Input.MoveHorizontal < -Mathf.Epsilon)
        {
            mActor.State = EStateType_Monster_Middle_Melee.Advance;
        }
    }

    public override void FixedUpdate()
    {
        mActor.CheckMoveVertical();
    }
}
