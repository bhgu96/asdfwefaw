﻿using UnityEngine;

public class Monster_Middle_Melee_Advance : State<EStateType_Monster_Middle_Melee>
{
    private Monster_Middle_Melee mActor;

    public Monster_Middle_Melee_Advance(Monster_Middle_Melee actor) : base(EStateType_Monster_Middle_Melee.Advance)
    {
        mActor = actor;
    }

    public override void Start()
    {
        mActor.Animator.SetInteger("state", (int)Type);
    }

    public override void Update()
    {
        if(mActor.CheckSkill())
        {
            return;
        }
    }

    public override void FixedUpdate()
    {
        // 수직 이동 검사
        mActor.CheckMoveVertical();

        // 왼쪽 이동 입력시 왼쪽 가속도 적용
        if(mActor.Input.MoveHorizontal < -Mathf.Epsilon)
        {
            mActor.AddVelocity(mActor.Status.MoveSpeed, new Vector2(-mActor.Status.Acceleration * Time.fixedDeltaTime, 0.0f));
        }
        // 오른쪽 이동 입력시 Retreat 상태 전이
        else if (mActor.Input.MoveHorizontal > Mathf.Epsilon)
        {
            mActor.State = EStateType_Monster_Middle_Melee.Retreat;
            return;
        }
        // 이동 입력 미감지시 Idle 상태 전이
        else
        {
            mActor.State = EStateType_Monster_Middle_Melee.Idle;
            return;
        }
    }
}
