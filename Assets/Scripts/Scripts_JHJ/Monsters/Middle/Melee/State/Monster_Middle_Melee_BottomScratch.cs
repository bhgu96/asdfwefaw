﻿using UnityEngine;

public class Monster_Middle_Melee_BottomScratch : State<EStateType_Monster_Middle_Melee>
{
    private Monster_Middle_Melee mActor;

    public Monster_Middle_Melee_BottomScratch(Monster_Middle_Melee actor) : base(EStateType_Monster_Middle_Melee.BottomScratch)
    {
        mActor = actor;
    }

    public override void Start()
    {
        mActor.Animator.SetInteger("state", (int)Type);
    }

    public override void FixedUpdate()
    {
        mActor.CheckMoveVertical();
        // 오른쪽 이동 입력시 이동속도를 최대속도로 가속도 적용
        if (mActor.Input.MoveHorizontal > Mathf.Epsilon)
        {
            mActor.AddVelocity(mActor.Status.MoveSpeed
                , new Vector2(mActor.Status.Acceleration * Time.fixedDeltaTime, 0.0f));
        }
    }
}
