﻿using UnityEngine;

public class Monster_Middle_Melee_Retreat : State<EStateType_Monster_Middle_Melee>
{
    private Monster_Middle_Melee mActor;

    public Monster_Middle_Melee_Retreat(Monster_Middle_Melee actor) : base(EStateType_Monster_Middle_Melee.Retreat)
    {
        mActor = actor;
    }

    public override void Start()
    {
        mActor.Animator.SetInteger("state", (int)Type);
    }

    public override void Update()
    {
        if (mActor.CheckSkill())
        {
            return;
        }
    }

    public override void FixedUpdate()
    {
        // 수직 이동 검사
        mActor.CheckMoveVertical();

        // 오른쪽 이동 감지시
        if (mActor.Input.MoveHorizontal > Mathf.Epsilon)
        {
            // 중앙 위치에 존재하는 경우
            if (mActor.PositionType == EPositionType.Middle)
            {
                // 대시 입력 감지시
                if (mActor.Input.Dash)
                {
                    // 거인 속도 + 몬스터 속도를 최대속도로 가속도 적용
                    mActor.AddVelocity(Giant_Player.Main.Status.MoveSpeed + mActor.Status.MoveSpeed
                        , new Vector2(mActor.Status.Acceleration * Time.fixedDeltaTime, 0.0f));
                }
                // 대시 입력 미감지시
                else
                {
                    // 거인 속도를 최대속도로 가속도 적용
                    mActor.AddVelocity(Giant_Player.Main.Status.MoveSpeed
                        , new Vector2(mActor.Status.Acceleration * Time.fixedDeltaTime, 0.0f));
                }
            }
            // 하단 위치에 존재하는 경우
            else if (mActor.PositionType == EPositionType.Bottom)
            {
                // 오른쪽 이동 감지시
                if (mActor.Input.MoveHorizontal > Mathf.Epsilon)
                {
                    // 몬스터 속도를 최대속도로 가속도 적용
                    mActor.AddVelocity(mActor.Status.MoveSpeed
                        , new Vector2(mActor.Status.Acceleration * Time.fixedDeltaTime, 0.0f));
                }
            }
        }
        // 왼쪽 이동 입력시 Advance 상태 전이
        else if (mActor.Input.MoveHorizontal < -Mathf.Epsilon)
        {
            mActor.State = EStateType_Monster_Middle_Melee.Advance;
            return;
        }
        else
        {
            mActor.State = EStateType_Monster_Middle_Melee.Idle;
            return;
        }
    }
}
