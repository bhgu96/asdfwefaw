﻿using UnityEngine;

public class Monster_Middle_Melee_MiddleScratchPrepare : State<EStateType_Monster_Middle_Melee>
{
    private Monster_Middle_Melee mActor;

    public Monster_Middle_Melee_MiddleScratchPrepare(Monster_Middle_Melee actor) : base(EStateType_Monster_Middle_Melee.MiddleScratchPrepare)
    {
        mActor = actor;
    }

    public override void Start()
    {
        mActor.Animator.SetInteger("state", (int)Type);
        mActor.Animator.speed = mActor.Status.CastSpeed;
    }

    public override void End()
    {
        mActor.Animator.speed = 1.0f;
    }

    public override void FixedUpdate()
    {
        mActor.CheckMoveVertical();
        // 오른쪽 이동 입력시 거인 이동속도를 최대속도로 가속도 적용
        if (mActor.Input.MoveHorizontal > Mathf.Epsilon)
        {
            mActor.AddVelocity(Giant_Player.Main.Status.MoveSpeed
                , new Vector2(mActor.Status.Acceleration * Time.fixedDeltaTime, 0.0f));
        }
    }
}
