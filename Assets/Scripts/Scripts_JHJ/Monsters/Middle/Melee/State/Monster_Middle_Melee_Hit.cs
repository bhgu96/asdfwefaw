﻿using UnityEngine;

public class Monster_Middle_Melee_Hit : State<EStateType_Monster_Middle_Melee>
{
    private Monster_Middle_Melee mActor;
    private bool mIsStarted = false;

    public Monster_Middle_Melee_Hit(Monster_Middle_Melee actor) : base(EStateType_Monster_Middle_Melee.Hit)
    {
        mActor = actor;
    }

    public override void Start()
    {
        if(!mIsStarted)
        {
            mIsStarted = true;
            mActor.Animator.SetInteger("state", (int)Type);
        }
        else
        {
            mActor.Animator.SetTrigger("hit");
        }
    }

    public override void FixedUpdate()
    {
        Vector2 curVelocity = mActor.Velocity;

        if(Mathf.Abs(curVelocity.x) < GameConstant.THRESHOLD_HIT
            && Mathf.Abs(curVelocity.y) < GameConstant.THRESHOLD_HIT)
        {
            mActor.State = EStateType_Monster_Middle_Melee.Idle;
            return;
        }
    }
}
