﻿using UnityEngine;
using Object;

public enum EStateType_Monster_Middle_Melee
{
    None = -1, 

    Idle = 0, 
    Advance = 1, 
    Retreat = 2, 
    Hit = 3, 
    Die = 4, 
    Restrain = 5, 
    MiddleScratchPrepare = 6, 
    MiddleScratch = 7, 
    MiddleScratchEnd = 8, 
    BottomScratchPrepare = 9, 
    BottomScratch = 10, 
    BottomScratchEnd = 11, 

    Count
}

public enum ETriggerType_Monster_Middle_Melee
{
    None = -1, 

    Reflex = 0, 
    Enemy = 1, 
    Scratchable = 2, 

    Count
}

public enum EAudioType_Monster_Middle_Melee
{
    None = -1, 

    Count
}

[RequireComponent(typeof(TriggerHandler_Monster_Middle_Melee), typeof(AudioHandler_Monster_Middle_Melee), typeof(AttackHandler_Monster_Middle_Melee))]
[RequireComponent(typeof(SkillHandler_Monster_Middle_Melee))]
public class Monster_Middle_Melee
    : AActor_Monster_Middle<EStateType_Monster_Middle_Melee>, IPoolable<EMonsterType>
{
    [SerializeField]
    private EMonsterType_Middle_Melee mType;
    public new EMonsterType_Middle_Melee Type { get { return mType; } }

    [SerializeField]
    private EMonsterRank mMonsterRank;
    public override EMonsterRank MonsterRank { get { return mMonsterRank; } }

    public override ECombatType CombatType
    {
        get { return ECombatType.Melee; }
    }

    public TriggerHandler_Monster_Middle_Melee TriggerHandler { get; private set; }
    public AudioHandler_Monster_Middle_Melee AudioHandler { get; private set; }
    public AttackHandler_Monster_Middle_Melee AttackHandler { get; private set; }
    public SkillHandler_Monster_Middle_Melee SkillHandler { get; private set; }

    private Status_Monster_Middle_Melee mStatus;
    public new Status_Monster_Middle_Melee Status
    {
        get { return mStatus; }
        protected set { base.Status = value; mStatus = value; }
    }

    [SerializeField]
    private ESkillType_Monster_Middle_Melee[] mSkills;
    /// <summary>
    /// 사용할 스킬
    /// </summary>
    public ESkillType_Monster_Middle_Melee[] Skills { get { return mSkills; } }

    /// <summary>
    /// 스킬 사용 입력을 검사하는 메소드
    /// </summary>
    public bool CheckSkill()
    {
        if (CanCast)
        {
            for (int i = 0; i < Skills.Length; i++)
            {
                if (Input.SkillDown[i])
                {
                    ISkill_Monster_Middle_Melee skill = SkillHandler.Get(Skills[i]);
                    if (skill != null && skill.Activate())
                    {
                        return true;
                    }
                }
            }
        }

        return false;
    }

    public override void OnRestrain()
    {
        if (CastingSkill != null)
        {
            CastingSkill.Deactivate();
        }
        State = EStateType_Monster_Middle_Melee.Restrain;
    }

    protected override void OnDamage()
    {
        base.OnDamage();
        if (!DebuffHandler.Get(EDebuffType.Restrain_Bush).IsActivated
            && Damage.CanReflex && Damage.SrcInstance is IDamagable damagable)
        {
            Damage damage = damagable.Damage;

            damage.SetDamage(false, true, false
                , EEffectType.None, EDamageType.Monster, ElementalType
                , Status.Level, Status.GetEA(ElementalType), Status.Offense, Status.Force
                , AttackHandler.Get(ETriggerType_Monster_Middle_Melee.Reflex).transform.position, this);

            damagable.ActivateDamage();
        }
    }

    protected override void OnDeactivate()
    {
        base.OnDeactivate();
        Status.Life = Status.MaxLife;
        State = EStateType_Monster_Middle_Melee.Idle;
    }

    protected override void OnDie()
    {
        base.OnDie();

        State = EStateType_Monster_Middle_Melee.Die;
    }

    protected override void OnHit()
    {
        if (CastingSkill != null)
        {
            CastingSkill.Deactivate();
        }
        State = EStateType_Monster_Middle_Melee.Hit;
    }

    protected override void Awake()
    {
        base.Awake();
        Status = new Status_Monster_Middle_Melee(this);
        SetInputs(new Agent_Monster_Middle_Melee(this, Skills.Length));
        TriggerHandler = GetComponent<TriggerHandler_Monster_Middle_Melee>();
        AudioHandler = GetComponent<AudioHandler_Monster_Middle_Melee>();
        AttackHandler = GetComponent<AttackHandler_Monster_Middle_Melee>();
        SkillHandler = GetComponent<SkillHandler_Monster_Middle_Melee>();

        SetStates(new Monster_Middle_Melee_Idle(this)
            , new Monster_Middle_Melee_Advance(this)
            , new Monster_Middle_Melee_Retreat(this)
            , new Monster_Middle_Melee_Hit(this)
            , new Monster_Middle_Melee_Die(this)
            , new Monster_Middle_Melee_Restrain(this)
            , new Monster_Middle_Melee_MiddleScratchPrepare(this)
            , new Monster_Middle_Melee_MiddleScratch(this)
            , new Monster_Middle_Melee_MiddleScratchEnd(this)
            , new Monster_Middle_Melee_BottomScratchPrepare(this)
            , new Monster_Middle_Melee_BottomScratch(this)
            , new Monster_Middle_Melee_BottomScratchEnd(this));
    }
}
