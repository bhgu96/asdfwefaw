﻿using UnityEngine;
using Object;

public enum EStateType_Monster_Middle_Summoner
{
    None = -1,

    Idle = 0,
    Advance = 1,
    Retreat = 2,
    Hit = 3,
    Die = 4,
    Restrain = 5,
    SummonPrepare = 6, 
    Summon = 7, 
    SummonEnd = 8, 

    Count
}

public enum ETriggerType_Monster_Middle_Summoner
{
    None = -1,

    Reflex = 0,
    Enemy = 1,

    Count
}

public enum EAudioType_Monster_Middle_Summoner
{
    None = -1,

    Count
}

[RequireComponent(typeof(TriggerHandler_Monster_Middle_Summoner), typeof(AudioHandler_Monster_Middle_Summoner), typeof(AttackHandler_Monster_Middle_Summoner))]
[RequireComponent(typeof(SkillHandler_Monster_Middle_Summoner))]
public class Monster_Middle_Summoner
    : AActor_Monster_Middle<EStateType_Monster_Middle_Summoner>, IPoolable<EMonsterType>
{
    [SerializeField]
    private EMonsterType_Middle_Summoner mType;
    public new EMonsterType_Middle_Summoner Type { get { return mType; } }

    [SerializeField]
    private EMonsterRank mMonsterRank;
    public override EMonsterRank MonsterRank { get { return mMonsterRank; } }

    public override ECombatType CombatType
    {
        get { return ECombatType.Summoner; }
    }

    public TriggerHandler_Monster_Middle_Summoner TriggerHandler { get; private set; }
    public AudioHandler_Monster_Middle_Summoner AudioHandler { get; private set; }
    public AttackHandler_Monster_Middle_Summoner AttackHandler { get; private set; }
    public SkillHandler_Monster_Middle_Summoner SkillHandler { get; private set; }

    private Status_Monster_Middle_Summoner mStatus;
    public new Status_Monster_Middle_Summoner Status
    {
        get { return mStatus; }
        protected set { base.Status = value; mStatus = value; }
    }

    [SerializeField]
    private ESkillType_Monster_Middle_Summoner[] mSkills;
    /// <summary>
    /// 사용할 스킬
    /// </summary>
    public ESkillType_Monster_Middle_Summoner[] Skills { get { return mSkills; } }

    /// <summary>
    /// 스킬 사용 입력을 검사하는 메소드
    /// </summary>
    public bool CheckSkill()
    {
        if (CanCast)
        {
            for (int i = 0; i < Skills.Length; i++)
            {
                if (Input.SkillDown[i])
                {
                    ISkill_Monster_Middle_Summoner skill = SkillHandler.Get(Skills[i]);
                    if (skill != null && skill.Activate())
                    {
                        return true;
                    }
                }
            }
        }

        return false;
    }

    public override void OnRestrain()
    {
        if (CastingSkill != null)
        {
            CastingSkill.Deactivate();
        }
        State = EStateType_Monster_Middle_Summoner.Restrain;
    }

    protected override void OnDamage()
    {
        base.OnDamage();
        if (!DebuffHandler.Get(EDebuffType.Restrain_Bush).IsActivated
            && Damage.CanReflex && Damage.SrcInstance is IDamagable damagable)
        {
            Damage damage = damagable.Damage;

            damage.SetDamage(false, true, false
                , EEffectType.None, EDamageType.Monster, ElementalType
                , Status.Level, Status.GetEA(ElementalType), Status.Offense, Status.Force
                , AttackHandler.Get(ETriggerType_Monster_Middle_Summoner.Reflex).transform.position, this);

            damagable.ActivateDamage();
        }
    }

    protected override void OnDeactivate()
    {
        base.OnDeactivate();
        Status.Life = Status.MaxLife;
        State = EStateType_Monster_Middle_Summoner.Idle;
    }

    protected override void OnDie()
    {
        base.OnDie();

        State = EStateType_Monster_Middle_Summoner.Die;
    }

    protected override void OnHit()
    {
        if (CastingSkill != null)
        {
            CastingSkill.Deactivate();
        }
        State = EStateType_Monster_Middle_Summoner.Hit;
    }

    protected override void Awake()
    {
        base.Awake();
        Status = new Status_Monster_Middle_Summoner(this);
        SetInputs(new Agent_Monster_Middle_Summoner(this, Skills.Length));
        TriggerHandler = GetComponent<TriggerHandler_Monster_Middle_Summoner>();
        AudioHandler = GetComponent<AudioHandler_Monster_Middle_Summoner>();
        AttackHandler = GetComponent<AttackHandler_Monster_Middle_Summoner>();
        SkillHandler = GetComponent<SkillHandler_Monster_Middle_Summoner>();

        SetStates(new Monster_Middle_Summoner_Idle(this)
            , new Monster_Middle_Summoner_Advance(this)
            , new Monster_Middle_Summoner_Retreat(this)
            , new Monster_Middle_Summoner_Hit(this)
            , new Monster_Middle_Summoner_Die(this)
            , new Monster_Middle_Summoner_Restrain(this)
            , new Monster_Middle_Summoner_SummonPrepare(this)
            , new Monster_Middle_Summoner_Summon(this)
            , new Monster_Middle_Summoner_SummonEnd(this));
    }
}
