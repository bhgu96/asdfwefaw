﻿using UnityEngine;

public class Monster_Middle_Summoner_Retreat : State<EStateType_Monster_Middle_Summoner>
{
    private Monster_Middle_Summoner mActor;

    public Monster_Middle_Summoner_Retreat(Monster_Middle_Summoner actor) : base(EStateType_Monster_Middle_Summoner.Retreat)
    {
        mActor = actor;
    }

    public override void Start()
    {
        mActor.Animator.SetInteger("state", (int)Type);
    }

    public override void Update()
    {
        if(mActor.CheckSkill())
        {
            return;
        }
    }

    public override void FixedUpdate()
    {
        mActor.CheckMoveVertical();

        
        if(mActor.Input.MoveHorizontal > Mathf.Epsilon)
        {
            // 오른쪽 입력 + 대시 입력시 거인의 이동속도 + 몬스터 이동속도를 최대속도로 가속도 적용
            if (mActor.Input.Dash)
            {
                mActor.AddVelocity(Giant_Player.Main.Status.MoveSpeed + mActor.Status.MoveSpeed, new Vector2(mActor.Status.Acceleration * Time.fixedDeltaTime, 0.0f));
            }
            // 오른쪽 입력시 거인의 이동속도를 최대속도로 가속도 적용
            else
            {
                mActor.AddVelocity(Giant_Player.Main.Status.MoveSpeed, new Vector2(mActor.Status.Acceleration * Time.fixedDeltaTime, 0.0f));
            }
        }
        else if(mActor.Input.MoveHorizontal < -Mathf.Epsilon)
        {
            // 왼쪽 이동 입력시 Advance 전이
            mActor.State = EStateType_Monster_Middle_Summoner.Advance;
            return;
        }
        // 이동 미입력시 Idle 전이
        else
        {
            mActor.State = EStateType_Monster_Middle_Summoner.Idle;
        }
    }
}
