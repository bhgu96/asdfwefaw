﻿using UnityEngine;
using System.Collections;

public class Monster_Middle_Summoner_SummonEnd : State<EStateType_Monster_Middle_Summoner>
{
    private Monster_Middle_Summoner mActor;

    public Monster_Middle_Summoner_SummonEnd(Monster_Middle_Summoner actor) : base(EStateType_Monster_Middle_Summoner.SummonEnd)
    {
        mActor = actor;
    }

    public override void Start()
    {
        mActor.Animator.SetInteger("state", (int)Type);
    }

    public override void FixedUpdate()
    {
        if (mActor.Input.MoveHorizontal > Mathf.Epsilon)
        {
            mActor.AddVelocity(Giant_Player.Main.Status.MoveSpeed, new Vector2(mActor.Status.Acceleration, 0.0f));
        }
    }
}
