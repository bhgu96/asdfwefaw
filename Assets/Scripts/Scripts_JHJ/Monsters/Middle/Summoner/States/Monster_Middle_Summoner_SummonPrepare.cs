﻿using UnityEngine;
using System.Collections;

public class Monster_Middle_Summoner_SummonPrepare : State<EStateType_Monster_Middle_Summoner>
{
    private Monster_Middle_Summoner mActor;

    public Monster_Middle_Summoner_SummonPrepare(Monster_Middle_Summoner actor) : base(EStateType_Monster_Middle_Summoner.SummonPrepare)
    {
        mActor = actor;
    }

    public override void Start()
    {
        mActor.Animator.SetInteger("state", (int)Type);
        mActor.Animator.speed = mActor.Status.CastSpeed;
    }

    public override void End()
    {
        mActor.Animator.speed = 1.0f;
    }

    public override void FixedUpdate()
    {
        if (mActor.Input.MoveHorizontal > Mathf.Epsilon)
        {
            mActor.AddVelocity(Giant_Player.Main.Status.MoveSpeed, new Vector2(mActor.Status.Acceleration, 0.0f));
        }
    }
}
