﻿using UnityEngine;

public class Monster_Middle_Summoner_Hit : State<EStateType_Monster_Middle_Summoner>
{
    private Monster_Middle_Summoner mActor;
    private bool mIsStarted = false;

    public Monster_Middle_Summoner_Hit(Monster_Middle_Summoner actor) : base(EStateType_Monster_Middle_Summoner.Hit)
    {
        mActor = actor;
    }

    public override void Start()
    {
        if (!mIsStarted)
        {
            mIsStarted = true;
            mActor.Animator.SetInteger("state", (int)Type);
        }
        else
        {
            mActor.Animator.SetTrigger("hit");
        }
    }

    public override void End()
    {
        mIsStarted = false;
    }

    public override void FixedUpdate()
    {
        Vector2 curVelocity = mActor.Velocity;

        if (Mathf.Abs(curVelocity.x) < GameConstant.THRESHOLD_HIT
            && Mathf.Abs(curVelocity.y) < GameConstant.THRESHOLD_HIT)
        {
            mActor.State = EStateType_Monster_Middle_Summoner.Idle;
        }
    }
}
