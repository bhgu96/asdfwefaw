﻿using UnityEngine;

public class Monster_Middle_Summoner_Advance : State<EStateType_Monster_Middle_Summoner>
{
    private Monster_Middle_Summoner mActor;

    public Monster_Middle_Summoner_Advance(Monster_Middle_Summoner actor) : base(EStateType_Monster_Middle_Summoner.Advance)
    {
        mActor = actor;
    }

    public override void Start()
    {
        mActor.Animator.SetInteger("state", (int)Type);
    }

    public override void Update()
    {
        if(mActor.CheckSkill())
        {
            return;
        }
    }

    public override void FixedUpdate()
    {
        // 수직 이동 검사
        mActor.CheckMoveVertical();

        // 왼쪽 입력시 몬스터의 이동속도를 최대속도로 가속도 적용
        if(mActor.Input.MoveHorizontal < -Mathf.Epsilon)
        {
            mActor.AddVelocity(mActor.Status.MoveSpeed, new Vector2(-mActor.Status.Acceleration * Time.fixedDeltaTime, 0.0f));
        }
        // 오른쪽 이동시 Retreat 전이
        else if(mActor.Input.MoveHorizontal > Mathf.Epsilon)
        {
            mActor.State = EStateType_Monster_Middle_Summoner.Retreat;
            return;
        }
        // 이동 미입력시 Idle 전이
        else
        {
            mActor.State = EStateType_Monster_Middle_Summoner.Idle;
            return;
        }
    }
}
