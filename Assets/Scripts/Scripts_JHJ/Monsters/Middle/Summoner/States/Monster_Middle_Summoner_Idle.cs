﻿using UnityEngine;

public class Monster_Middle_Summoner_Idle : State<EStateType_Monster_Middle_Summoner>
{
    private Monster_Middle_Summoner mActor;
    
    public Monster_Middle_Summoner_Idle(Monster_Middle_Summoner actor) : base(EStateType_Monster_Middle_Summoner.Idle)
    {
        mActor = actor;
    }

    public override void Start()
    {
        mActor.Animator.SetInteger("state", (int)Type);
    }

    public override void Update()
    {
        if(mActor.CheckSkill())
        {
            return;
        }

        // 왼쪽 입력시 Advance 전이
        if(mActor.Input.MoveHorizontal < -Mathf.Epsilon)
        {
            mActor.State = EStateType_Monster_Middle_Summoner.Advance;
            return;
        }
        // 오른쪽 입력시 Retreat 전이
        else if(mActor.Input.MoveHorizontal > Mathf.Epsilon)
        {
            mActor.State = EStateType_Monster_Middle_Summoner.Retreat;
            return;
        }
    }

    public override void FixedUpdate()
    {
        // 수직 이동 검사
        mActor.CheckMoveVertical();
    }
}
