﻿using UnityEngine;

public class Monster_Middle_Summoner_Restrain : State<EStateType_Monster_Middle_Summoner>
{
    private Monster_Middle_Summoner mActor;

    public Monster_Middle_Summoner_Restrain(Monster_Middle_Summoner actor) : base(EStateType_Monster_Middle_Summoner.Restrain)
    {
        mActor = actor;
    }

    public override void Start()
    {
        mActor.Animator.SetInteger("state", (int)Type);
    }

    public override void Update()
    {
        if(!mActor.DebuffHandler.Get(EDebuffType.Restrain_Bush).IsActivated)
        {
            mActor.State = EStateType_Monster_Middle_Summoner.Idle;
            return;
        }
    }
}
