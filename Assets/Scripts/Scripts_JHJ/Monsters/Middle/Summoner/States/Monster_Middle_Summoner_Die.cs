﻿using UnityEngine;

public class Monster_Middle_Summoner_Die : State<EStateType_Monster_Middle_Summoner>
{
    private Monster_Middle_Summoner mActor;

    public Monster_Middle_Summoner_Die(Monster_Middle_Summoner actor) : base(EStateType_Monster_Middle_Summoner.Die)
    {
        mActor = actor;
    }

    public override void Start()
    {
        mActor.Animator.SetInteger("state", (int)Type);

        mActor.Velocity = Vector2.zero;
        mActor.MainCollider.enabled = false;
        mActor.Rigidbody.constraints = RigidbodyConstraints2D.FreezeAll;

        if(!StateManager.GameOver.State)
        {
            PlayerData.Exp += mActor.Status.Exp;
            PlayerData.Souls += mActor.Status.Souls;
            Fairy_Player.Main.Status.Mana += mActor.Status.Souls * 0.02f;
        }
    }

    public override void End()
    {
        mActor.MainCollider.enabled = true;
        mActor.Rigidbody.constraints = RigidbodyConstraints2D.FreezeRotation;
        mActor.Velocity = Vector2.zero;
    }
}
