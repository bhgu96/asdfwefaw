﻿using UnityEngine;

public class Skill_Summon_Monster_Middle_Summoner_Prepare : State<EStateType_Skill_Summon_Monster_Middle_Summon>
{
    private Skill_Summon_Monster_Middle_Summoner mSkill;

    public Skill_Summon_Monster_Middle_Summoner_Prepare(Skill_Summon_Monster_Middle_Summoner skill) : base(EStateType_Skill_Summon_Monster_Middle_Summon.Prepare)
    {
        mSkill = skill;
    }

    public override void Start()
    {
        mSkill.Animator.SetInteger("state", (int)Type);
        mSkill.Animator.speed = mSkill.CastTime;
    }

    public override void End()
    {
        mSkill.Animator.speed = 1.0f;
    }
}
