﻿using UnityEngine;

public enum EStateType_Skill_Summon_Monster_Middle_Summon
{
    None = -1, 

    Idle = 0, 
    Prepare = 1, 
    Cast = 2, 
    End = 3, 

    Count
}

public class Skill_Summon_Monster_Middle_Summoner : ASkill_Monster_Middle_Summoner<EStateType_Skill_Summon_Monster_Middle_Summon>
{
    public override float CastTime
    {
        get { return 1.0f; }
    }

    [SerializeField]
    private ESkillType_Monster_Middle_Summoner mType;
    public override ESkillType_Monster_Middle_Summoner Type
    {
        get { return mType; }
    }

    public int CountOfSummons_Default
    {
        get { return (int)Parameters[0]; }
    }
    public int CountOfSummons_Increment
    {
        get;
    }
    public int CountOfSummons
    {
        get { return CountOfSummons_Default + CountOfSummons_Increment; }
    }

    public float GetWeightOfSummons(int index)
    {
        return Parameters[index + 1];
    }

    [SerializeField]
    private Transform mSummonHolder;
    [SerializeField]
    private EMonsterType[] mMonsterTypes;

    public int RestOfSummons { get; private set; }

    /// <summary>
    /// 몬스터 소환 메소드
    /// </summary>
    public void Summon()
    {
        if (RestOfSummons > 0)
        {
            float weightSum = 0.0f;

            for (int i = 0; i < mMonsterTypes.Length; i++)
            {
                weightSum += GetWeightOfSummons(i);
            }

            float random = Random.Range(0.0f, weightSum);

            for (int i = 0; i < mMonsterTypes.Length; i++)
            {
                if (random <= GetWeightOfSummons(i))
                {
                    IActor_Monster monster = Pool_Monster.Get(mMonsterTypes[i]);
                    if (monster != null)
                    {
                        monster.transform.position = mSummonHolder.position;
                        monster.Activate();
                    }
                    RestOfSummons--;
                    return;
                }
                else
                {
                    random -= GetWeightOfSummons(i);
                }
            }
        }
    }

    protected override void OnActivate()
    {
        base.OnActivate();
        SetDisableCast();
        State = EStateType_Skill_Summon_Monster_Middle_Summon.Prepare;
        User.State = EStateType_Monster_Middle_Summoner.SummonPrepare;
        RestOfSummons = CountOfSummons;
    }

    protected override void OnDeactivate()
    {
        base.OnDeactivate();
        SetEnableCast();
        State = EStateType_Skill_Summon_Monster_Middle_Summon.End;
        User.State = EStateType_Monster_Middle_Summoner.SummonEnd;
    }

    protected override void Awake()
    {
        base.Awake();

        SetStates(new Skill_Summon_Monster_Middle_Summoner_Idle(this)
            , new Skill_Summon_Monster_Middle_Summoner_Prepare(this)
            , new Skill_Summon_Monster_Middle_Summoner_Cast(this)
            , new Skill_Summon_Monster_Middle_Summoner_End(this));
    }
}
