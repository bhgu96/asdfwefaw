﻿using UnityEngine;

public class Skill_Summon_Monster_Middle_Summoner_Idle : State<EStateType_Skill_Summon_Monster_Middle_Summon>
{
    private Skill_Summon_Monster_Middle_Summoner mSkill;

    public Skill_Summon_Monster_Middle_Summoner_Idle(Skill_Summon_Monster_Middle_Summoner skill) : base(EStateType_Skill_Summon_Monster_Middle_Summon.Idle)
    {
        mSkill = skill;
    }

    public override void Start()
    {
        mSkill.Animator.SetInteger("state", (int)Type);
    }
}
