﻿using UnityEngine;

public class Skill_Summon_Monster_Middle_Summoner_Cast : State<EStateType_Skill_Summon_Monster_Middle_Summon>
{
    private Skill_Summon_Monster_Middle_Summoner mSkill;

    public Skill_Summon_Monster_Middle_Summoner_Cast(Skill_Summon_Monster_Middle_Summoner skill) : base(EStateType_Skill_Summon_Monster_Middle_Summon.Cast)
    {
        mSkill = skill;
    }

    public override void Start()
    {
        mSkill.Animator.SetInteger("state", (int)Type);
        mSkill.Animator.speed = mSkill.CastSpeed;
        mSkill.User.State = EStateType_Monster_Middle_Summoner.Summon;
    }

    public override void End()
    {
        mSkill.Animator.speed = 1.0f;
    }

    public override void Update()
    {
        if(mSkill.RestOfSummons <= 0)
        {
            mSkill.Deactivate();
        }
    }
}
