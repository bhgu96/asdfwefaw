﻿using UnityEngine;

public class Agent_Monster_Middle_Summoner_Cast : State<EStateType_Agent_Monster_Middle_Summoner>
{
    private Agent_Monster_Middle_Summoner mAgent;

    public Agent_Monster_Middle_Summoner_Cast(Agent_Monster_Middle_Summoner agent) : base(EStateType_Agent_Monster_Middle_Summoner.Cast)
    {
        mAgent = agent;
    }


    public override void Update()
    {
        mAgent.SetMoveHorizontal(1.0f);
        mAgent.SetMoveVertical(0.0f);
        mAgent.SetDash(false);

        for(int i=0; i<mAgent.Skill.Length; i++)
        {
            mAgent.SetSkill(false, i);
        }

        // 활성화중인 스킬이 존재하지 않는다면 Normal 전이
        if(mAgent.Actor.CastingSkill == null || !mAgent.Actor.CastingSkill.IsActivated)
        {
            mAgent.State = EStateType_Agent_Monster_Middle_Summoner.Normal;
            return;
        }
    }
}
