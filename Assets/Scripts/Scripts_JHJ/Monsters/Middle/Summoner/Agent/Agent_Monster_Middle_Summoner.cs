﻿using UnityEngine;

public enum EStateType_Agent_Monster_Middle_Summoner
{
    None = -1, 

    Normal = 0, 
    Cast = 1, 

    Count
}

public class Agent_Monster_Middle_Summoner : Agent<EStateType_Agent_Monster_Middle_Summoner>
{
    public Monster_Middle_Summoner Actor { get; private set; }

    public Agent_Monster_Middle_Summoner(Monster_Middle_Summoner actor, int countOfSkills) : base(countOfSkills)
    {
        Actor = actor;

        SetStates(new Agent_Monster_Middle_Summoner_Normal(this)
            , new Agent_Monster_Middle_Summoner_Cast(this));
    }
}
