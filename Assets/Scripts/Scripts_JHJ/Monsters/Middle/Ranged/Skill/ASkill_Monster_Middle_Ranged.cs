﻿using UnityEngine;
using System;
using System.Collections.Generic;
using Object;

public enum ESkillType_Monster_Middle_Ranged
{
    None = -1,

    // 암속성 슈터
    DarknessShooter = 0,

    Count
}

public interface ISkill_Monster_Middle_Ranged
    : ISkill_Monster_Middle, IHandleable<ESkillType_Monster_Middle_Ranged>
{

}

public abstract class ASkill_Monster_Middle_Ranged<EState>
    : ASkill_Monster_Middle<Monster_Middle_Ranged, EState>, ISkill_Monster_Middle_Ranged
    where EState : Enum
{
    public abstract ESkillType_Monster_Middle_Ranged Type { get; }

    /// <summary>
    /// 이름 태그
    /// </summary>
    public override string NameTag
    {
        get { return mInfo.NameTag; }
    }
    /// <summary>
    /// 설명 태그
    /// </summary>
    public override string DescriptionTag
    {
        get { return mInfo.DescriptionTag; }
    }
    /// <summary>
    /// 옵션 태그
    /// </summary>
    public override string OptionTag
    {
        get { return mInfo.OptionTag; }
    }
    /// <summary>
    /// 부가 옵션 태그
    /// </summary>
    public override string AdditionalOptionTag
    {
        get { return mInfo.AdditionalOptionTag; }
    }
    /// <summary>
    /// 기본 쿨타임
    /// </summary>
    public override float CoolTime_Default
    {
        get { return mInfo.CoolTime; }
    }
    /// <summary>
    /// 가변인자
    /// </summary>
    public override float[] Parameters
    {
        get { return mInfo.Parameters; }
    }

    private SkillInfo_Monster_Middle_Ranged mInfo;

    protected override void Awake()
    {
        base.Awake();

        mInfo = DataManager.GetSkillInfo(Type);
    }
}


public class SkillInfo_Monster_Middle_Ranged
{
    public string NameTag { get; }
    public string DescriptionTag { get; }
    public string OptionTag { get; }
    public string AdditionalOptionTag { get; }
    public float CoolTime { get; }
    public float[] Parameters { get; }

    public SkillInfo_Monster_Middle_Ranged(Dictionary<string, string> skillDict)
    {
        NameTag = skillDict["Name Tag"];
        DescriptionTag = skillDict["Desc Tag"];
        OptionTag = skillDict["Option Tag"];
        AdditionalOptionTag = skillDict["Additional Tag"];
        CoolTime = float.Parse(skillDict["Cool Time"]);

        Parameters = new float[GameConstant.MAX_SKILL_PARAMETERS];

        for (int i = 0; i < GameConstant.MAX_SKILL_PARAMETERS; i++)
        {
            Parameters[i] = float.Parse(skillDict[i.ToString()]);
        }
    }
}