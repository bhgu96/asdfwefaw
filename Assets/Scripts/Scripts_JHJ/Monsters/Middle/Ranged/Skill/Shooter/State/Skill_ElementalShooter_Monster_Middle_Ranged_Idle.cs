﻿using UnityEngine;

public class Skill_ElementalShooter_Monster_Middle_Ranged_Idle : State<EStateType_Skill_ElementalShooter_Monster_Middle_Ranged>
{
    private Skill_ElementalShooter_Monster_Middle_Ranged mSkill;

    public Skill_ElementalShooter_Monster_Middle_Ranged_Idle(Skill_ElementalShooter_Monster_Middle_Ranged skill) : base(EStateType_Skill_ElementalShooter_Monster_Middle_Ranged.Idle)
    {
        mSkill = skill;
    }

    public override void Start()
    {
        mSkill.Animator.SetInteger("state", (int)Type);
    }
}
