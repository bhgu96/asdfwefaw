﻿using UnityEngine;

public enum EStateType_Skill_ElementalShooter_Monster_Middle_Ranged
{
    None = -1, 

    Idle = 0, 
    Prepare =1, 
    Cast = 2, 
    End = 3, 

    Count
}

public enum EAudioType_Skill_ElementalShooter_Monster_Middle_Ranged
{
    None = -1, 

    Count
}

[RequireComponent(typeof(AudioHandler_Skill_ElementalShooter_Monster_Middle_Ranged))]
public class Skill_ElementalShooter_Monster_Middle_Ranged : ASkill_Monster_Middle_Ranged<EStateType_Skill_ElementalShooter_Monster_Middle_Ranged>
{
    public override float CastTime
    {
        get { return 1.0f; }
    }

    [SerializeField]
    private ESkillType_Monster_Middle_Ranged mType;
    public override ESkillType_Monster_Middle_Ranged Type
    {
        get { return mType; }
    }

    [SerializeField]
    private EElementalType mElementalType;
    public EElementalType ElementalType
    {
        get { return mElementalType; }
    }

    public float SkillOffense_Default
    {
        get { return Parameters[0]; }
    }

    public float SkillOffense_Increase
    {
        get;
    }
    public float SkillOffense
    {
        get { return SkillOffense_Default * (1.0f + SkillOffense_Increase); }
    }

    public int CountOfBullets_Default
    {
        get { return (int)Parameters[1]; }
    }
    public int CountOfBullets_Increment
    {
        get;
    }
    public int CountOfBullets
    {
        get { return CountOfBullets_Default + CountOfBullets_Increment; }
    }

    public float SpeedOfBullets_Default
    {
        get { return Parameters[2]; }
    }

    public float SpeedOfBullets_Increase
    {
        get;
    }

    public float SpeedOfBullets
    {
        get { return SpeedOfBullets_Default * (1.0f + SpeedOfBullets_Increase); }
    }

    public int RestOfBullets { get; private set; }

    public AudioHandler_Skill_ElementalShooter_Monster_Middle_Ranged AudioHandler { get; private set; }

    [SerializeField]
    private Transform mBulletHolder;

    // 탄환 발사 메소드
    public void Fire()
    {
        if (RestOfBullets > 0)
        {
            Collider2D[] colliders = User.TriggerHandler.GetColliders(ETriggerType_Monster_Middle_Ranged.Enemy);
            for (int i = 0; i < colliders.Length && colliders[i] != null; i++)
            {
                IActor_Damagable actor = colliders[i].GetComponentInParent<IActor_Damagable>();
                if (actor != null)
                {
                    Projectile_ElementalBullet bullet = null;
                    if (ElementalType == EElementalType.Darkness)
                    {
                        bullet = ProjectilePool.Get(EProjectileType.DarknessBullet) as Projectile_ElementalBullet;
                    }

                    if (bullet != null)
                    {
                        Vector2 holderPosition = mBulletHolder.position;
                        Vector2 targetPosition = actor.MainCollider.bounds.center;

                        bullet.transform.position = holderPosition;
                        bullet.SetDirection(targetPosition - holderPosition, false);

                        bullet.Speed = SpeedOfBullets;

                        bullet.IsBasedOnGiant = true;

                        bullet.Offense = User.Status.Offense * SkillOffense;
                        bullet.ElementalAffinity = User.Status.GetEA(ElementalType);
                        bullet.Force = User.Status.Force;
                        bullet.CriticalChance = 0.0f;
                        bullet.CriticalMag = 1.0f;
                        bullet.DamageType = EDamageType.Monster;
                        bullet.Level = User.Status.Level;

                        bullet.Activate();
                    }
                    break;
                }
            }

            RestOfBullets--;
        }
    }

    protected override void OnActivate()
    {
        base.OnActivate();
        State = EStateType_Skill_ElementalShooter_Monster_Middle_Ranged.Prepare;
        User.State = EStateType_Monster_Middle_Ranged.CastPrepare;
        SetDisableCast();
        RestOfBullets = CountOfBullets;
    }

    protected override void OnDeactivate()
    {
        base.OnDeactivate();
        State = EStateType_Skill_ElementalShooter_Monster_Middle_Ranged.End;
        User.State = EStateType_Monster_Middle_Ranged.CastEnd;
        SetEnableCast();
    }

    protected override void Awake()
    {
        base.Awake();

        AudioHandler = GetComponent<AudioHandler_Skill_ElementalShooter_Monster_Middle_Ranged>();

        SetStates(new Skill_ElementalShooter_Monster_Middle_Ranged_Idle(this)
            , new Skill_ElementalShooter_Monster_Middle_Ranged_Prepare(this)
            , new Skill_ElementalShooter_Monster_Middle_Ranged_Cast(this)
            , new Skill_ElementalShooter_Monster_Middle_Ranged_End(this));
    }
}

