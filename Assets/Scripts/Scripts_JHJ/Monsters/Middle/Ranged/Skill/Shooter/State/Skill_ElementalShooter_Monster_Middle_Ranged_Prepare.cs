﻿using UnityEngine;

public class Skill_ElementalShooter_Monster_Middle_Ranged_Prepare : State<EStateType_Skill_ElementalShooter_Monster_Middle_Ranged>
{
    private Skill_ElementalShooter_Monster_Middle_Ranged mSkill;

    public Skill_ElementalShooter_Monster_Middle_Ranged_Prepare(Skill_ElementalShooter_Monster_Middle_Ranged skill) : base(EStateType_Skill_ElementalShooter_Monster_Middle_Ranged.Prepare)
    {
        mSkill = skill;
    }

    public override void Start()
    {
        mSkill.Animator.SetInteger("state", (int)Type);
        mSkill.Animator.speed = mSkill.CastSpeed;
    }

    public override void End()
    {
        mSkill.Animator.speed = 1.0f;
    }
}
