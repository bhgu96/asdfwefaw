﻿using UnityEngine;

public class Monster_Middle_Ranged_Advance : State<EStateType_Monster_Middle_Ranged>
{
    private Monster_Middle_Ranged mActor;

    public Monster_Middle_Ranged_Advance(Monster_Middle_Ranged actor) : base(EStateType_Monster_Middle_Ranged.Advance)
    {
        mActor = actor;
    }

    public override void Start()
    {
        mActor.Animator.SetInteger("state", (int)Type);
    }

    public override void Update()
    {
        mActor.CheckSkill();
    }

    public override void FixedUpdate()
    {
        mActor.CheckMoveVertical();

        if(mActor.Input.MoveHorizontal < -Mathf.Epsilon)
        {
            mActor.AddVelocity(mActor.Status.MoveSpeed, new Vector2(-mActor.Status.Acceleration * Time.fixedDeltaTime, 0.0f));
        }
        else if(mActor.Input.MoveHorizontal > Mathf.Epsilon)
        {
            mActor.State = EStateType_Monster_Middle_Ranged.Retreat;
            return;
        }
        else
        {
            mActor.State = EStateType_Monster_Middle_Ranged.Idle;
            return;
        }
    }
}
