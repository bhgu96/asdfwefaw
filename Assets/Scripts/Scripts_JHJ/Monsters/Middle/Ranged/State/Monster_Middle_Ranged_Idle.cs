﻿using UnityEngine;

public class Monster_Middle_Ranged_Idle : State<EStateType_Monster_Middle_Ranged>
{
    private Monster_Middle_Ranged mActor;

    public Monster_Middle_Ranged_Idle(Monster_Middle_Ranged actor) : base(EStateType_Monster_Middle_Ranged.Idle)
    {
        mActor = actor;
    }

    public override void Start()
    {
        mActor.Animator.SetInteger("state", (int)Type);
    }

    public override void Update()
    {
        if(mActor.CheckSkill())
        {
            return;
        }

        if(mActor.Input.MoveHorizontal < -Mathf.Epsilon)
        {
            mActor.State = EStateType_Monster_Middle_Ranged.Advance;
        }
        else if(mActor.Input.MoveHorizontal > Mathf.Epsilon)
        {
            mActor.State = EStateType_Monster_Middle_Ranged.Retreat;
        }
    }

    public override void FixedUpdate()
    {
        mActor.CheckMoveVertical();
    }
}
