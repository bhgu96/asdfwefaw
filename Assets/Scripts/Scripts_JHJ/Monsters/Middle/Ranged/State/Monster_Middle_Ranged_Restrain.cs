﻿using UnityEngine;

public class Monster_Middle_Ranged_Restrain : State<EStateType_Monster_Middle_Ranged>
{
    private Monster_Middle_Ranged mActor;
    public Monster_Middle_Ranged_Restrain(Monster_Middle_Ranged actor) : base(EStateType_Monster_Middle_Ranged.Restrain)
    {
        mActor = actor;
    }

    public override void Start()
    {
        mActor.Animator.SetInteger("state", (int)Type);
    }

    public override void Update()
    {
        if(!mActor.DebuffHandler.Get(EDebuffType.Restrain_Bush).IsActivated)
        {
            mActor.State = EStateType_Monster_Middle_Ranged.Idle;
        }
    }
}
