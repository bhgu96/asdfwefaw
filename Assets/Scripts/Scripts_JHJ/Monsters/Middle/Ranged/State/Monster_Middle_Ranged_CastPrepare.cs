﻿using UnityEngine;

public class Monster_Middle_Ranged_CastPrepare : State<EStateType_Monster_Middle_Ranged>
{
    private Monster_Middle_Ranged mActor;

    public Monster_Middle_Ranged_CastPrepare(Monster_Middle_Ranged actor) : base(EStateType_Monster_Middle_Ranged.CastPrepare)
    {
        mActor = actor;
    }

    public override void Start()
    {
        mActor.Animator.SetInteger("state", (int)Type);
        mActor.Animator.speed = mActor.Status.CastSpeed;
    }

    public override void End()
    {
        mActor.Animator.speed = 1.0f;
    }

    public override void FixedUpdate()
    {
        if(mActor.Input.MoveHorizontal > Mathf.Epsilon)
        {
            mActor.AddVelocity(Giant_Player.Main.Status.MoveSpeed, new Vector2(mActor.Status.Acceleration * Time.fixedDeltaTime, 0.0f));
        }
    }
}
