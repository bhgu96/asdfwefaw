﻿using UnityEngine;


public class Monster_Middle_Ranged_Cast : State<EStateType_Monster_Middle_Ranged>
{
    private Monster_Middle_Ranged mActor;

    public Monster_Middle_Ranged_Cast(Monster_Middle_Ranged actor) : base(EStateType_Monster_Middle_Ranged.Cast)
    {
        mActor = actor;
    }

    public override void Start()
    {
        mActor.Animator.SetInteger("state", (int)Type);
    }

    public override void FixedUpdate()
    {
        if (mActor.Input.MoveHorizontal > Mathf.Epsilon)
        {
            mActor.AddVelocity(Giant_Player.Main.Status.MoveSpeed, new Vector2(mActor.Status.Acceleration * Time.fixedDeltaTime, 0.0f));
        }
    }
}
