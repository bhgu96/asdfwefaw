﻿using UnityEngine;

public enum EStateType_Agent_Monster_Middle_Ranged
{
    None = -1, 

    Normal = 0, 
    Cast = 1, 

    Count
}

public class Agent_Monster_Middle_Ranged : Agent<EStateType_Agent_Monster_Middle_Ranged>
{
    public Monster_Middle_Ranged Actor { get; private set; }

    public Agent_Monster_Middle_Ranged(Monster_Middle_Ranged actor, int countOfSkills) : base(countOfSkills)
    {
        Actor = actor;

        SetStates(new Agent_Monster_Middle_Ranged_Normal(this)
            , new Agent_Monster_Middle_Ranged_Cast(this));
    }
}
