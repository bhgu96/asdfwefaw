﻿using UnityEngine;
using System.Collections;

public class Agent_Monster_Middle_Ranged_Cast : State<EStateType_Agent_Monster_Middle_Ranged>
{
    private Agent_Monster_Middle_Ranged mAgent;

    public Agent_Monster_Middle_Ranged_Cast(Agent_Monster_Middle_Ranged agent) : base(EStateType_Agent_Monster_Middle_Ranged.Cast)
    {
        mAgent = agent;
    }

    public override void Update()
    {
        mAgent.SetMoveHorizontal(1.0f);
        mAgent.SetMoveVertical(0.0f);
        mAgent.SetDash(false);
        
        for(int i=0; i<mAgent.Skill.Length; i++)
        {
            mAgent.SetSkill(false, i);
        }

        if(mAgent.Actor.CastingSkill == null || !mAgent.Actor.CastingSkill.IsActivated)
        {
            mAgent.State = EStateType_Agent_Monster_Middle_Ranged.Normal;
        }
    }
}
