﻿using UnityEngine;

public class Agent_Monster_Middle_Ranged_Normal : State<EStateType_Agent_Monster_Middle_Ranged>
{
    private Agent_Monster_Middle_Ranged mAgent;

    private float mMinDistance;
    private float mMaxDistance;
    private float mMinHeight;
    private float mMaxHeight;

    public Agent_Monster_Middle_Ranged_Normal(Agent_Monster_Middle_Ranged agent) : base(EStateType_Agent_Monster_Middle_Ranged.Normal)
    {
        mAgent = agent;

        mMinDistance = Random.Range(mAgent.Actor.Status.MinDistance, mAgent.Actor.Status.MaxDistance) - 1.0f;
        mMaxDistance = mMinDistance + 2.0f;
        mMinHeight = Random.Range(mAgent.Actor.Status.MinHeight, mAgent.Actor.Status.MaxHeight) - 1.0f;
        mMaxHeight = mMinHeight + 2.0f;
    }

    public override void Start()
    {
        mMinDistance = Random.Range(mAgent.Actor.Status.MinDistance, mAgent.Actor.Status.MaxDistance) - 1.0f;
        mMaxDistance = mMinDistance + 2.0f;
        mMinHeight = Random.Range(mAgent.Actor.Status.MinHeight, mAgent.Actor.Status.MaxHeight) - 1.0f;
        mMaxHeight = mMinHeight + 2.0f;
    }

    public override void Update()
    {
        if(mAgent.Actor.CastingSkill != null && mAgent.Actor.CastingSkill.IsActivated)
        {
            mAgent.SetMoveHorizontal(1.0f);
            mAgent.SetMoveVertical(0.0f);
            mAgent.SetDash(false);
            
            for(int i = 0; i<mAgent.Skill.Length; i++)
            {
                mAgent.SetSkill(false, i);
            }

            mAgent.State = EStateType_Agent_Monster_Middle_Ranged.Cast;
            return;
        }

        Collider2D[] colliders = mAgent.Actor.TriggerHandler.GetColliders(ETriggerType_Monster_Middle_Ranged.Enemy);
        for (int i = 0; i < colliders.Length && colliders[i] != null; i++)
        {
            Giant_Player giant = colliders[i].GetComponentInParent<Giant_Player>();
            if (giant != null && giant.State != EStateType_Giant_Player.Die)
            {
                Vector2 agentPosition = mAgent.Actor.transform.position;
                Vector2 giantPosition = giant.transform.position;

                float distance = agentPosition.x - giantPosition.x;
                float height = agentPosition.y - giantPosition.y;

                bool isProperDistance = false;
                bool isProperHeight = false;

                // 최소 위치보다 더 앞에 존재한다면
                if (distance < mMinDistance)
                {
                    mAgent.SetMoveHorizontal(1.0f);
                    mAgent.SetDash(true);
                }
                // 적정 위치에 존재한 경우
                else if (distance < mMaxDistance)
                {
                    mAgent.SetMoveHorizontal(1.0f);
                    mAgent.SetDash(false);
                    isProperDistance = true;
                }
                // 최대 위치보다 더 뒤에 존재하는 경우
                else
                {
                    mAgent.SetMoveHorizontal(-1.0f);
                    mAgent.SetDash(false);
                }
                // 최소 높이보다 더 아래에 위치하는 경우
                if (height < mMinHeight)
                {
                    mAgent.SetMoveVertical(1.0f);
                }
                // 적정 높이에 위치하는 경우
                else if (height < mMaxHeight)
                {
                    mAgent.SetMoveVertical(0.0f);
                    isProperHeight = true;
                }
                // 최대 높이보다 더위에 위치하는 경우
                else
                {
                    mAgent.SetMoveVertical(-1.0f);
                }

                // 적정 위치에 존재하는 경우
                if (isProperDistance && isProperHeight)
                {
                    for (int j = 0; j < mAgent.Skill.Length; j++)
                    {
                        // 사용가능한 스킬이 존재하는 경우
                        if (mAgent.Actor.SkillHandler.Get(mAgent.Actor.Skills[j]).CanActivate)
                        {
                            if (!mAgent.Skill[j])
                            {
                                mAgent.SetSkill(true, j);
                            }
                            else
                            {
                                mAgent.SetSkill(false, j);
                            }
                        }
                        else
                        {
                            mAgent.SetSkill(false, j);
                        }
                    }
                }
                return;
            }
        }

        // 거인을 감지하지 못한 경우
        mAgent.SetMoveHorizontal(0.0f);
        mAgent.SetMoveVertical(0.0f);
        mAgent.SetDash(false);
        for (int i = 0; i < mAgent.Skill.Length; i++)
        {
            mAgent.SetSkill(false, i);
        }
    }
}
