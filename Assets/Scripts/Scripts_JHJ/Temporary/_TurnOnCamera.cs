﻿using UnityEngine;
using System.Collections;

public class _TurnOnCamera : MonoBehaviour
{
    private void Start()
    {
        CameraController.Main.BlackOut(0.0f, 1.0f);
    }

    private void Update()
    {
        if(!CameraController.Main.IsBlackOutTriggered)
        {
            CameraController.Main.BlackOut(1.0f, 0.0f);
            gameObject.SetActive(false);
        }
    }
}
