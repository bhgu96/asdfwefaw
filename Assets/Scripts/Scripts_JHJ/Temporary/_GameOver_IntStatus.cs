﻿using UnityEngine;
using UnityEngine.UI;

public class _GameOver_IntStatus : MonoBehaviour
{
    [SerializeField]
    private EUserIntData mIntData;

    private Text mText;
    private int mPrevValue;

    private void Awake()
    {
        mText = GetComponent<Text>();
        mPrevValue = _UserData.Get(mIntData);
        mText.text = "x" + mPrevValue;
    }

    private void Update()
    {
        int value = _UserData.Get(mIntData);
        if (mPrevValue != value)
        {
            mPrevValue = value;
            mText.text = "x" + mPrevValue;
        }
    }

}
