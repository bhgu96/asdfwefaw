﻿using UnityEngine;
using UnityEngine.UI;

public class _Text_Level : MonoBehaviour
{
    private Text mText;
    private int mPrevLevel;

    private void Awake()
    {
        mText = GetComponent<Text>();
        mPrevLevel = PlayerData.Level;
        mText.text = "Level : " + mPrevLevel;
    }

    private void Update()
    {
        int level = PlayerData.Level;
        if(level != mPrevLevel)
        {
            mPrevLevel = level;
            mText.text = "Level : " + mPrevLevel;
        }
    }
}
