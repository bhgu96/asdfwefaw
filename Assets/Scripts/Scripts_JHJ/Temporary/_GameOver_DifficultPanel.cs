﻿using UnityEngine;
using UnityEngine.UI;

public class _GameOver_DifficultPanel : MonoBehaviour
{
    [SerializeField]
    private Text mText;

    [SerializeField]
    private Button mDownButton;

    [SerializeField]
    private Button mUpButton;

    private int mPrevDifficult;

    private void Awake()
    {
        mPrevDifficult = _UserData.Get(EUserIntData.Difficult);
        mText.text = "Difficult : " + mPrevDifficult;

        mDownButton.onClick.AddListener(OnDownClick);
        mUpButton.onClick.AddListener(OnUpClick);
    }

    private void Update()
    {
        int difficult = _UserData.Get(EUserIntData.Difficult);

        if(mPrevDifficult != difficult)
        {
            mPrevDifficult = difficult;
            mText.text = "Difficult : " + mPrevDifficult;
        }

        if (mDownButton.interactable && difficult == 1)
        {
            mDownButton.interactable = false;
        }
        else if(!mDownButton.interactable && difficult > 1)
        {
            mDownButton.interactable = true;
        }
    }

    private void OnDownClick()
    {
        _UserData.SetDifficult(--mPrevDifficult);
        mText.text = "Difficult : " + mPrevDifficult;
        if (mPrevDifficult == 1)
        {
            mDownButton.interactable = false;
        }
    }

    private void OnUpClick()
    {
        _UserData.SetDifficult(++mPrevDifficult);
        mText.text = "Difficult : " + mPrevDifficult;
        if (mPrevDifficult > 1)
        {
            mDownButton.interactable = true;
        }
    }
}
