﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class _GameOver_RestartButton : MonoBehaviour
{
    private Button mButton;

    private bool mIsRestarting = false;

    private void Awake()
    {
        mButton = GetComponent<Button>();
        mButton.onClick.AddListener(OnClick);
        mIsRestarting = false;
    }

    private void Update()
    {
        if(mIsRestarting && !CameraController.Main.IsBlackOutTriggered)
        {
            _UserData.Save();
            StateManager.GameOver.State = false;
            StateManager.GameClear.State = false;
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
        }
    }

    private void OnClick()
    {
        mIsRestarting = true;
        CameraController.Main.BlackOut(1.0f, 1.0f);
    }
}
