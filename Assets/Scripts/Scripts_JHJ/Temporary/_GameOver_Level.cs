﻿using UnityEngine;
using UnityEngine.UI;

public class _GameOver_Level : MonoBehaviour
{
    private Text mText;
    private int mPrevLevel;

    private void Awake()
    {
        mText = GetComponent<Text>();
        mPrevLevel = PlayerData.Level;
        mText.text = mPrevLevel.ToString();
    }

    private void Update()
    {
        int level = PlayerData.Level;
        if (mPrevLevel != level)
        {
            mPrevLevel = level;
            mText.text = mPrevLevel.ToString();
        }
    }
}
