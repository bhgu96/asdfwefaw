﻿using UnityEngine;
using UnityEngine.UI;

public class _GameOver_LeftLevelUp : MonoBehaviour
{
    private Text mText;
    private int prevValue;

    private void Awake()
    {
        mText = GetComponent<Text>();
        prevValue = _UserData.Get(EUserIntData.Level) - _UserData.Get(EUserIntData.StatusLevel);
        mText.text = prevValue.ToString();
    }

    private void Update()
    {
        int value = _UserData.Get(EUserIntData.Level) - _UserData.Get(EUserIntData.StatusLevel);
        if(prevValue != value)
        {
            prevValue = value;
            mText.text = prevValue.ToString();
        }
    }
}
