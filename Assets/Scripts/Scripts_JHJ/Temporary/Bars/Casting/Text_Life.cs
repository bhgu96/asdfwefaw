﻿using UnityEngine;
using UnityEngine.UI;

public class Text_Life : MonoBehaviour
{
    private _AGiant mGiant;
    private Text mText;

    private int mHp;
    private int mMaxHp;

    private void Awake()
    {
        mGiant = GetComponentInParent<_AGiant>();
        mText = GetComponent<Text>();
    }

    private void Start()
    {
        mHp = mGiant.Hp;
        mMaxHp = mGiant.MaxHp;

        mText.text = mHp + "/" + mMaxHp;
    }

    private void Update()
    {
        if(mHp != mGiant.Hp || mMaxHp != mGiant.MaxHp)
        {
            mHp = mGiant.Hp;
            mMaxHp = mGiant.MaxHp;
            mText.text = mHp + "/" + mMaxHp;
        }
    }
}
