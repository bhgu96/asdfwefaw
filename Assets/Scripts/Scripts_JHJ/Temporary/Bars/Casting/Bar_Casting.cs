﻿//using UnityEngine;
//using System.Collections;

//public enum EStateType_Bar_Casting
//{
//    Idle, 
//    Active, 
//    Deactivate, 
//}

//public class Bar_Casting : StateBase<EStateType_Bar_Casting>
//{
//    public IActor Actor { get; private set; }

//    [SerializeField]
//    private SpriteRenderer mFrameRenderer;
//    /// <summary>
//    /// 프레임 렌더러
//    /// </summary>
//    public SpriteRenderer FrameRenderer { get { return mFrameRenderer; } }

//    [SerializeField]
//    private SpriteRenderer mContentRenderer;
//    /// <summary>
//    /// 콘텐트 렌더러
//    /// </summary>
//    public SpriteRenderer ContentRenderer { get { return mContentRenderer; } }

//    private bool mIsActive = true;
//    /// <summary>
//    /// 프레임, 콘텐트, 콘텐트 마스크의 활성화 여부를 설정하는 프로퍼티
//    /// </summary>
//    public bool IsActive
//    {
//        get { return mIsActive; }
//        set
//        {
//            if(mIsActive != value)
//            {
//                mIsActive = value;
//                if(mIsActive)
//                {
//                    mFrameRenderer.gameObject.SetActive(true);
//                    mContentRenderer.gameObject.SetActive(true);
//                    mContentMask.gameObject.SetActive(true);
//                }
//                else
//                {
//                    mFrameRenderer.gameObject.SetActive(false);
//                    mContentRenderer.gameObject.SetActive(false);
//                    mContentMask.gameObject.SetActive(false);
//                }
//            }
//        }
//    }

//    // 콘텐트 렌더러를 가리는 마스크
//    [SerializeField]
//    private Transform mContentMask;

//    /// <summary>
//    /// 프레임과 콘텐트의 알파값을 조정하는 메소드
//    /// </summary>
//    /// <param name="alpha"></param>
//    public void SetAlpha(float alpha)
//    {
//        Color prevColor = mFrameRenderer.color;
//        mFrameRenderer.color = new Color(prevColor.r, prevColor.g, prevColor.b, alpha);
//        prevColor = mContentRenderer.color;
//        mContentRenderer.color = new Color(prevColor.r, prevColor.g, prevColor.b, alpha);
//    }

//    /// <summary>
//    /// amount로 전달된 값에 따라 콘텐트 마스크의 위치를 조정하는 메소드
//    /// </summary>
//    /// <param name="amount"></param>
//    public void FillAmount(float amount)
//    {
//        float norm = mContentRenderer.size.x;
//        Vector3 maskLocalPosition = mContentMask.localPosition;
//        mContentMask.localPosition = new Vector3(amount * norm, maskLocalPosition.y, maskLocalPosition.z);
//    }

//    protected override void Awake()
//    {
//        base.Awake();
//        Actor = GetComponentInParent<IActor>();
//        IsActive = false;
//    }
//}
