﻿using UnityEngine;
using System.Collections;

public class _GameOverPanel : MonoBehaviour
{
    [SerializeField]
    private GameObject mGameOverPanel;

    [SerializeField]
    private GameObject mGameClearPanel;

    private void Awake()
    {
        mGameOverPanel.SetActive(false);
        mGameClearPanel.SetActive(false);

        StateManager.GameOver.AddEnterEvent(true, OnGameOverEnter);
        StateManager.GameOver.AddExitEvent(true, OnGameOverExit);
        StateManager.GameClear.AddEnterEvent(true, OnGameClearEnter);
        StateManager.GameClear.AddExitEvent(true, OnGameClearExit);
    }

    private void OnDestroy()
    {
        StateManager.GameOver.RemoveEnterEvent(true, OnGameOverEnter);
        StateManager.GameOver.RemoveExitEvent(true, OnGameOverExit);
        StateManager.GameClear.RemoveEnterEvent(true, OnGameClearEnter);
        StateManager.GameClear.RemoveExitEvent(true, OnGameClearExit);
    }

    private void OnGameOverEnter()
    {
        mGameOverPanel.SetActive(true);
    }

    private void OnGameOverExit()
    {
        mGameOverPanel.SetActive(false);
    }

    private void OnGameClearEnter()
    {
        if (!StateManager.GameOver.State)
        {
            mGameClearPanel.SetActive(true);
        }
    }

    private void OnGameClearExit()
    {
        mGameClearPanel.SetActive(false);
    }
}
