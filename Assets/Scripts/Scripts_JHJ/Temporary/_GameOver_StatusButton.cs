﻿using UnityEngine;
using UnityEngine.UI;

public class _GameOver_StatusButton : MonoBehaviour
{
    private Button mButton;
    [SerializeField]
    private EUserFloatData mFloatData;

    private void Awake()
    {
        mButton = GetComponent<Button>();
        mButton.onClick.AddListener(OnClick);
    }

    private void Update()
    {
        if (!mButton.interactable && _UserData.CanLevelUp)
        {
            mButton.interactable = true;
        }
        else if(mButton.interactable && !_UserData.CanLevelUp)
        {
            mButton.interactable = false;
        }
    }

    private void OnClick()
    {
        _UserData.AddStatus(mFloatData);
        if(!_UserData.CanLevelUp)
        {
            mButton.interactable = false;
        }
    }
}
