﻿using UnityEngine;
using System;
using System.Collections.Generic;
using Csv;


public enum EUserIntData
{
    None = -1, 

    Level, 
    Exp, 
    StatusLevel,

    Difficult, 

    Count
}

public enum EUserFloatData
{
    None = -1,

    GiantHealthPoint,
    GiantMoveSpeed,
    GiantOffensivePower, 
    GiantForce,
    FairyOffensivePower,
    FairyAttackSpeed,
    FairyForce, 

    Count
}

public static class _UserData
{
    private static Dictionary<EUserIntData, int> mIntData;
    private static Dictionary<EUserFloatData, float> mFloatData;

    public static bool CanLevelUp
    {
        get
        {
            int level = mIntData[EUserIntData.Level];
            int statusLevel = mIntData[EUserIntData.StatusLevel];
            if(level > statusLevel)
            {
                return true;
            }
            return false;
        }
    }

    public static int MaxExp
    {
        get
        {
            int level = mIntData[EUserIntData.Level];
            if (level < 100 && level > 0)
            {
                return mExpDict[level];
            }
            return mExpDict[100];
        }
    }

    private static Dictionary<int, int> mExpDict;

    static _UserData()
    {
        mExpDict = new Dictionary<int, int>();
        Dictionary<string, Dictionary<string, string>> expDict = CsvReader.Read(Application.streamingAssetsPath + "/Exp.csv");
        for (int i = 0; i < 100; i++)
        {
            mExpDict.Add(i + 1, int.Parse(expDict[(i + 1).ToString()]["Exp"]));
        }

        mIntData = new Dictionary<EUserIntData, int>();
        mFloatData = new Dictionary<EUserFloatData, float>();

        Dictionary<string, Dictionary<string, string>> userDict = CsvReader.Read(Application.streamingAssetsPath + "/User.csv");

        Dictionary<string, Dictionary<string, string>>.Enumerator rowEnum = userDict.GetEnumerator();
        if (rowEnum.MoveNext())
        {
            Dictionary<string, string>.Enumerator columnEnum = rowEnum.Current.Value.GetEnumerator();
            while(columnEnum.MoveNext())
            {
                EUserIntData intData;
                EUserFloatData floatData;
                if (Enum.TryParse(columnEnum.Current.Key, out intData))
                {
                    mIntData.Add(intData, int.Parse(columnEnum.Current.Value));
                }
                else if(Enum.TryParse(columnEnum.Current.Key, out floatData))
                {
                    mFloatData.Add(floatData, float.Parse(columnEnum.Current.Value));
                }
            }
        }
    }

    public static int Get(EUserIntData intData)
    {
        return mIntData[intData];
    }

    public static float Get(EUserFloatData floatData)
    {
        return mFloatData[floatData];
    }

    public static void AddExp(int exp)
    {
        int nextExp = mIntData[EUserIntData.Exp] + exp;
        if(nextExp >= MaxExp)
        {
            mIntData[EUserIntData.Level]++;
            mIntData[EUserIntData.Exp] = 0;
        }
        else
        {
            mIntData[EUserIntData.Exp] = nextExp;
        }
    }

    public static bool AddStatus(EUserFloatData floatData)
    {
        if(CanLevelUp)
        {
            mFloatData[floatData] += 0.5f;
            mIntData[EUserIntData.StatusLevel]++;
            return true;
        }
        return false;
    }

    public static void SetDifficult(int difficult)
    {
        mIntData[EUserIntData.Difficult] = difficult;
    }

    public static void Save()
    {
        Dictionary<string, Dictionary<string, string>> userDict = new Dictionary<string, Dictionary<string, string>>();
        Dictionary<string, string> userInfo = new Dictionary<string, string>();

        Dictionary<EUserIntData, int>.Enumerator intEnum = mIntData.GetEnumerator();
        while(intEnum.MoveNext())
        {
            userInfo.Add(intEnum.Current.Key.ToString(), intEnum.Current.Value.ToString());
        }

        Dictionary<EUserFloatData, float>.Enumerator floatEnum = mFloatData.GetEnumerator();
        while (floatEnum.MoveNext())
        {
            userInfo.Add(floatEnum.Current.Key.ToString(), floatEnum.Current.Value.ToString());
        }

        userDict.Add("User", userInfo);
        CsvWriter.Write(userDict, Application.streamingAssetsPath + "/User.csv");
    }
}
