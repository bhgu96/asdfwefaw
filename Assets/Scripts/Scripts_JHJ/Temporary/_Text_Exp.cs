﻿using UnityEngine;
using UnityEngine.UI;

public class _Text_Exp : MonoBehaviour
{
    private Text mText;
    private int mPrevExp;

    private void Awake()
    {
        mText = GetComponent<Text>();
        mPrevExp = PlayerData.Exp;
        mText.text = "Exp : " + mPrevExp + "/" + PlayerData.MaxExp;
    }

    private void Update()
    {
        int exp = PlayerData.Exp;
        if (exp != mPrevExp)
        {
            mPrevExp = exp;
            mText.text = "Exp : " + mPrevExp + "/" + PlayerData.MaxExp;
        }
    }
}
