﻿using UnityEngine;
using UnityEngine.UI;

public class _Text_CastTime : MonoBehaviour
{
    private Text mText;
    private IActor mActor;
    private float mRemainTime;
    private float mCastTime;

    private void Awake()
    {
        mText = GetComponent<Text>();
        mActor = GetComponentInParent<IActor>();
        mText.enabled = false;
    }

    private void Update()
    {
        if (mActor.CastingSkill != null && mActor.CastingSkill.CastRemainTime > 0.0f)
        {
            mCastTime = mActor.CastingSkill.CastTime;
            mText.enabled = true;
            Color prevColor = mText.color;
            mText.color = new Color(prevColor.r, prevColor.g, prevColor.b, 1.0f);
            mText.text = "[" + Mathf.Round((mCastTime - mActor.CastingSkill.CastRemainTime) * 100.0f) * 0.01f + "/" + mCastTime + "]";
            mRemainTime = 1.0f;
        }
        else if (mRemainTime > 0.0f)
        {
            mRemainTime -= Time.deltaTime;
            mText.text = "[" + mCastTime + "/" + mCastTime + "]";
            Color prevColor = mText.color;
            mText.color = new Color(prevColor.r, prevColor.g, prevColor.b, Mathf.Lerp(0.0f, 1.0f, mRemainTime));
        }
        else if(mText.enabled)
        {
            mText.enabled = false;
        }

    }
}
