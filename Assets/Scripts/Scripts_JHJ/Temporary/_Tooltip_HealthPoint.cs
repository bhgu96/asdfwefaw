﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class _Tooltip_HealthPoint : MonoBehaviour
{

    private IActor_Damagable mActor;
    private Text mText;

    private int mPrevHp;
    private int mPrevMaxHp;

    private void Start()
    {
        mActor = GetComponentInParent<IActor_Damagable>();
        mText = GetComponent<Text>();

        mPrevHp = (int)mActor.Status.Life;
        mPrevMaxHp = (int)mActor.Status.MaxLife;

        mText.text = mPrevHp + "/" + mPrevMaxHp;
    }

    private void Update()
    {
        if(mPrevHp != (int)mActor.Status.Life || mPrevMaxHp != (int)mActor.Status.MaxLife)
        {
            mPrevHp = (int)mActor.Status.Life;
            mPrevMaxHp = (int)mActor.Status.MaxLife;
            mText.text = mPrevHp + "/" + mPrevMaxHp;
        }
    }
}
