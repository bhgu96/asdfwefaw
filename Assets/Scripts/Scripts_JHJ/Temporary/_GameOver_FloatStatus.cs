﻿using UnityEngine;
using UnityEngine.UI;

public class _GameOver_FloatStatus : MonoBehaviour
{
    [SerializeField]
    private EUserFloatData mFloatData;

    private Text mText;
    private float mPrevValue;

    private void Awake()
    {
        mText = GetComponent<Text>();
        mPrevValue = _UserData.Get(mFloatData);
        mText.text = "x" + mPrevValue;
    }

    private void Update()
    {
        float value = _UserData.Get(mFloatData);
        if (mPrevValue != value)
        {
            mPrevValue = value;
            mText.text = "x" + mPrevValue;
        }
    }
}
