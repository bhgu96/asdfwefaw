﻿using UnityEngine;
using UnityEngine.UI;

public class _Text_Difficult : MonoBehaviour
{
    private Text mText;
    private int mPrevDifficult;

    private void Awake()
    {
        mText = GetComponent<Text>();
        mPrevDifficult = PlayerData.Difficulty;
        mText.text = "Difficult : " + mPrevDifficult;
    }

    private void Update()
    {
        int difficult = PlayerData.Difficulty;
        if (difficult != mPrevDifficult)
        {
            mPrevDifficult = difficult;
            mText.text = "Difficult : " + mPrevDifficult;
        }
    }
}
