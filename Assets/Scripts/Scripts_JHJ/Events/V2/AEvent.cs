﻿//using System.Collections;
//using System.Collections.Generic;
//using UnityEngine;

//public abstract class AEvent : MonoBehaviour
//{
//    [SerializeField]
//    private EGamEStateType[] mActiveStates;
//    [SerializeField]
//    private EGamEStateType mNextState;

//    protected virtual void Awake()
//    {
//        InitializeEvent();
//        bool isActive = false;
//        EGamEStateType curState = StateManager.GameStateHandler.State;
//        for (int i = 0; i < mActiveStates.Length; i++)
//        {
//            StateManager.GameStateHandler.AddEnterEvent(mActiveStates[i], OnActiveStateEnter);
//            StateManager.GameStateHandler.AddExitEvent(mActiveStates[i], OnActiveStateExit);
//            if (curState == mActiveStates[i])
//            {
//                isActive = true;
//            }
//        }
//        if (isActive)
//        {
//            OnActiveStateEnter();
//        }
//    }

//    protected virtual void Update()
//    {
//        bool isActive = false;
//        EGamEStateType curState = StateManager.GameStateHandler.State;
//        for(int i=0; i<mActiveStates.Length; i++)
//        {
//            if(mActiveStates[i] == curState)
//            {
//                isActive = true;
//                break;
//            }
//        }

//        if(isActive && CheckEscape())
//        {
//            StateManager.GameStateHandler.State = mNextState;
//        }
//    }

//    protected virtual void OnDestroy()
//    {
//        for (int i = 0; i < mActiveStates.Length; i++)
//        {
//            StateManager.GameStateHandler.RemoveEnterEvent(mActiveStates[i], OnActiveStateEnter);
//            StateManager.GameStateHandler.RemoveExitEvent(mActiveStates[i], OnActiveStateExit);
//        }
//    }

//    protected abstract bool CheckEscape();

//    private void OnActiveStateEnter()
//    {
//        EGamEStateType prevState = StateManager.GameStateHandler.PrevState;
//        for (int i = 0; i < mActiveStates.Length; i++)
//        {
//            if (mActiveStates[i] == prevState)
//            {
//                return;
//            }
//        }

//        OnActivating();
//    }
//    private void OnActiveStateExit()
//    {
//        EGamEStateType curState = StateManager.GameStateHandler.State;
//        for (int i = 0; i < mActiveStates.Length; i++)
//        {
//            if (mActiveStates[i] == curState)
//            {
//                return;
//            }
//        }

//        OnDeactivating();
//    }

//    protected abstract void OnActivating();
//    protected abstract void OnDeactivating();
//    protected abstract void InitializeEvent();
//}
