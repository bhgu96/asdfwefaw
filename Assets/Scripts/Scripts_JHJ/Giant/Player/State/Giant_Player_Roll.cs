﻿using UnityEngine;

public class Giant_Player_Roll : State<EStateType_Giant_Player>
{
    private Giant_Player mActor;

    public Giant_Player_Roll(Giant_Player actor) : base(EStateType_Giant_Player.Roll)
    {
        mActor = actor;
    }

    public override void Start()
    {
        mActor.Animator.SetInteger("state", (int)Type);
    }
}
