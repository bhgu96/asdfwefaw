﻿using UnityEngine;

public class Giant_Player_Idle : State<EStateType_Giant_Player>
{
    private Giant_Player mActor;

    public Giant_Player_Idle(Giant_Player actor) : base(EStateType_Giant_Player.Idle)
    {
        mActor = actor;
    }

    public override void Start()
    {
        mActor.Animator.SetInteger("state", (int)Type);
    }

    public override void Update()
    {
        if(mActor.SetAttack(false))
        {
            return;
        }

        // 오른쪽으로 이동하는 입력이 감지된 경우
        if(mActor.Input.MoveHorizontal > 0.0f)
        {
            // 상태를 Move로 전이
            mActor.State = EStateType_Giant_Player.Move;
        }
    }
}
