﻿using UnityEngine;

public class Giant_Player_Attack : State<EStateType_Giant_Player>
{
    private Giant_Player mActor;

    private float mRemainTime;
    private bool mIsStarted = false;

    public Giant_Player_Attack(Giant_Player actor, EStateType_Giant_Player state) : base(state)
    {
        mActor = actor;
    }

    public override void Start()
    {
        mRemainTime = GameConstant.ONE_FRAME;
        if (!mIsStarted)
        {
            mIsStarted = true;
            mActor.Animator.SetInteger("state", (int)Type);
        }
        else
        {
            mActor.Animator.SetTrigger("attack");
        }
    }

    public override void End()
    {
        mIsStarted = false;
    }

    public override void FixedUpdate()
    {
        if (mActor.Input.MoveHorizontal > 0.0f)
        {
            mActor.MoveHorizontal(mActor.Status.MoveSpeed, EHorizontalDirection.Right);
        }

        // 한프레임 대기 후 공격 체크
        if (mRemainTime > 0.0f)
        {
            mRemainTime -= Time.deltaTime;
        }
        else if(mActor.SetAttack(true))
        {
            return;
        }
    }
}
