﻿using UnityEngine;

public class Giant_Player_Restrain : State<EStateType_Giant_Player>
{
    private Giant_Player mActor;

    public Giant_Player_Restrain(Giant_Player actor) : base(EStateType_Giant_Player.Restrain)
    {
        mActor = actor;
    }

    public override void Start()
    {
        mActor.Animator.SetInteger("state", (int)Type);
    }

    public override void Update()
    {
        // 구속 디버프가 해제된 상태라면
        if(!mActor.DebuffHandler.Get(EDebuffType.Restrain_Bush).IsActivated)
        {
            mActor.State = EStateType_Giant_Player.Idle;
        }
    }
}
