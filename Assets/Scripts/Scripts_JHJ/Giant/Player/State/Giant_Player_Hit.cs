﻿using UnityEngine;

public class Giant_Player_Hit : State<EStateType_Giant_Player>
{
    private Giant_Player mActor;

    public Giant_Player_Hit(Giant_Player actor) : base(EStateType_Giant_Player.Hit)
    {
        mActor = actor;
    }

    public override void Start()
    {
        mActor.Animator.SetInteger("state", (int)Type);
        // 넉백 도중 넉백 적용시 애니메이션 재적용
        mActor.Animator.SetTrigger("hit");
    }

    public override void FixedUpdate()
    {
        Vector2 velocity = mActor.Velocity;

        // 액터의 넉백 적용 후 속도가 한계값 미만이면
        if(Mathf.Abs(velocity.x) < GameConstant.THRESHOLD_HIT
            && Mathf.Abs(velocity.y) < GameConstant.THRESHOLD_HIT)
        {
            // Idle 상태로 전이
            mActor.State = EStateType_Giant_Player.Idle;
        }
    }
}
