﻿using UnityEngine;

public class Giant_Player_Move : State<EStateType_Giant_Player>
{
    private Giant_Player mActor;

    public Giant_Player_Move(Giant_Player actor) : base(EStateType_Giant_Player.Move)
    {
        mActor = actor;
    }

    public override void Start()
    {
        mActor.Animator.SetInteger("state", (int)Type);
    }

    public override void FixedUpdate()
    {
        // 오른쪽으로 움직이는 입력이 감지된 경우
        if (mActor.Input.MoveHorizontal > 0.0f)
        {
            // 오른쪽으로 이동
            mActor.MoveHorizontal(mActor.Status.MoveSpeed, EHorizontalDirection.Right);

            if (mActor.SetAttack(false))
            {
                return;
            }
        }
        // 오른쪽으로 움직이는 입력이 감지되지 않은 경우
        else
        {
            if (mActor.SetAttack(false))
            {
                return;
            }
            // Idle 상태로 전이
            mActor.State = EStateType_Giant_Player.Idle;
            return;
        }
    }
}
