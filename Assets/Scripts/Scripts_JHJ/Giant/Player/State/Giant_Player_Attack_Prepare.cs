﻿using UnityEngine;

public class Giant_Player_Attack_Prepare : State<EStateType_Giant_Player>
{
    private Giant_Player mActor;

    public Giant_Player_Attack_Prepare(Giant_Player actor, EStateType_Giant_Player state) : base(state)
    {
        mActor = actor;
    }

    public override void Start()
    {
        mActor.Animator.SetInteger("state", (int)Type);
    }

    public override void FixedUpdate()
    {
        if (mActor.Input.MoveHorizontal > 0.0f)
        {
            mActor.MoveHorizontal(mActor.Status.MoveSpeed, EHorizontalDirection.Right);
        }

        // 애니메이션이 실행중인 상태로 공격 상태 조사
        if (mActor.SetAttack(true))
        {
            return;
        }
    }
}
