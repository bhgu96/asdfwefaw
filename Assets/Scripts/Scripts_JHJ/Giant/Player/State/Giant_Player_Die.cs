﻿using UnityEngine;

public class Giant_Player_Die : State<EStateType_Giant_Player>
{
    private Giant_Player mActor;

    public Giant_Player_Die(Giant_Player actor) : base(EStateType_Giant_Player.Die)
    {
        mActor = actor;
    }

    public override void Start()
    {
        mActor.Animator.SetInteger("state", (int)Type);
        mActor.Velocity = Vector2.zero;
        StateManager.GameOver.State = true;
    }
}
