﻿using UnityEngine;

public enum EStateType_Giant_Player
{
    // 대기
    Idle = 0,
    // 이동
    Move = 1,
    // 피격
    Hit = 2,
    // 죽음
    Die = 3,
    // 구속
    Restrain = 4,
    // 방출
    Release = 5,
    // 앞 구르기
    Roll = 6,
    // 손 들어 올리기
    RaiseHand = 7,

    // 소형 몬스터 공격 준비
    Attack_Prepare_Small_00 = 8,
    // 소형 몬스터 공격
    Attack_Small_00 = 9,
    // 소형 몬스터 공격 준비
    Attack_Prepare_Small_01 = 10,
    // 소형 몬스터 공격
    Attack_Small_01 = 11,
    // 중형 몬스터 공격 준비
    Attack_Prepare_Medium_00 = 12,
    // 중형 몬스터 공격
    Attack_Medium_00 = 13,
    // 중형 몬스터 공격 준비
    Attack_Prepare_Medium_01 = 14,
    // 중형 몬스터 공격
    Attack_Medium_01 = 15,
    // 대형 몬스터 공격 준비
    Attack_Prepare_Large_00 = 16,
    // 대형 몬스터 공격
    Attack_Large_00 = 17,
    // 대형 몬스터 공격 준비
    Attack_Prepare_Large_01 = 18,
    // 대형 몬스터 공격
    Attack_Large_01 = 19,
}

public enum ETriggerType_Giant_Player
{
    None = -1,

    // 공격 범위
    Attack = 0,
    // 긴급 공격 범위
    Attack_Urgent = 1,


    Count
}

public enum EAudioFlags_Giant_Player
{
    None = -1,


    Count
}

[RequireComponent(typeof(TriggerHandler_Giant_Player), typeof(AudioHandler_Giant_Player), typeof(AttackHandler_Giant_Player))]
[RequireComponent(typeof(SkillHandler_Giant_Player))]
public class Giant_Player : AActor_Giant<EStateType_Giant_Player>
{
    public static Giant_Player Main { get; private set; } = null;

    [SerializeField]
    private EGiantType_Player mType;
    /// <summary>
    /// 플레이어 거인 타입
    /// </summary>
    public EGiantType_Player Type { get { return mType; } }

    public bool IsInvincible_Test { get; set; }

    private Status_Giant_Player mStatus;
    /// <summary>
    /// 플레이어 괴수의 속성
    /// </summary>
    public new Status_Giant_Player Status
    {
        get { return mStatus; }
        set { base.Status = value; mStatus = value; }
    }

    public EElementalType ElementalType { get { return EElementalType.Kinematic; } }

    /// <summary>
    /// 영혼석 장착 객체
    /// </summary>
    public SouliteFrame SouliteFrame { get; private set; }

    public SkillHandler_Giant_Player SkillHandler { get; private set; }

    public SkillFrame_Giant_Player SkillFrame { get; private set; }

    public TriggerHandler_Giant_Player TriggerHandler { get; private set; }
    public AudioHandler_Giant_Player AudioHandler { get; private set; }
    public AttackHandler_Giant_Player AttackHandler { get; private set; }

    /// <summary>
    /// 전달된 트리거에 존재하는 모든 객체에게 데미지를 전달하는 메소드
    /// </summary>
    /// <param name="type"></param>
    public void Attack(ETriggerType_Giant_Player type)
    {
        Collider2D[] colliders = TriggerHandler.GetColliders(type);

        for (int i = 0; i < colliders.Length && colliders[i] != null; i++)
        {
            IDamagable damagable = colliders[i].GetComponentInParent<IDamagable>();
            Damage damage = damagable.Damage;

            float random = Random.Range(0.0f, 1.0f);
            // 치명타인 경우
            if (random <= Status.CriticalChance)
            {
                damage.SetDamage(true, false, true, EEffectType.Hit_Break_01, EDamageType.Giant, ElementalType
                    , Status.Level, Status.GetEA(EElementalType.Kinematic), Status.Offense * Status.CriticalMag, Status.Force
                    , AttackHandler.Get(type).transform.position, this);
            }
            // 치명타가 아닌 경우
            else
            {
                damage.SetDamage(true, false, false, EEffectType.Hit_Break_01, EDamageType.Giant, ElementalType
                    , Status.Level, Status.GetEA(EElementalType.Kinematic), Status.Offense, Status.Force
                    , AttackHandler.Get(type).transform.position, this);
            }

            damagable.ActivateDamage();
        }
    }

    protected override void OnDamage()
    {
        if (Damage.CanReflex && Damage.SrcInstance is IDamagable damagable)
        {
            Damage damage = damagable.Damage;

            float random = Random.Range(0.0f, 1.0f);

            if (random <= Status.CriticalChance)
            {
                damage.SetDamage(false, true, true
                , EEffectType.None, EDamageType.Giant, ElementalType
                , Status.Level, Status.GetEA(ElementalType), Status.Offense * Status.CriticalMag, Status.Force
                , AttackHandler.Get(ETriggerType_Giant_Player.Attack).transform.position, this);
            }
            else
            {
                damage.SetDamage(false, true, false
                , EEffectType.None, EDamageType.Giant, ElementalType
                , Status.Level, Status.GetEA(ElementalType), Status.Offense, Status.Force
                , AttackHandler.Get(ETriggerType_Giant_Player.Attack).transform.position, this);
            }

            damagable.ActivateDamage();
        }
    }

    /// <summary>
    /// 공격 가능한 액터를 검사하고 상황과 액터 크기에 따라 적절한 상태로 설정하는 메소드
    /// </summary>
    /// <param name="isAttacking">공격을 시전중에 있는가에 관한 변수</param>
    public bool SetAttack(bool isAttacking)
    {
        ESizeType size = GetDamagableSize(ETriggerType_Giant_Player.Attack_Urgent);

        int random = (int)Random.Range(0.0f, 2.0f);

        if (size == ESizeType.Small)
        {
            State = EStateType_Giant_Player.Attack_Small_00 + random * 2;
            return true;
        }
        else if (size == ESizeType.Medium)
        {
            State = EStateType_Giant_Player.Attack_Medium_00 + random * 2;
            return true;
        }
        else if (size == ESizeType.Large || size == ESizeType.ExtraLarge)
        {
            State = EStateType_Giant_Player.Attack_Large_00 + random * 2;
            return true;
        }

        // 공격 시전중이 아니라면
        if (!isAttacking)
        {
            size = GetDamagableSize(ETriggerType_Giant_Player.Attack);

            if (size == ESizeType.Small)
            {
                State = EStateType_Giant_Player.Attack_Prepare_Small_00 + random * 2;
                return true;
            }
            else if (size == ESizeType.Medium)
            {
                State = EStateType_Giant_Player.Attack_Prepare_Medium_00 + random * 2;
                return true;
            }
            else if (size == ESizeType.Large || size == ESizeType.ExtraLarge)
            {
                State = EStateType_Giant_Player.Attack_Prepare_Large_00 + random * 2;
                return true;
            }
        }

        return false;
    }

    /// <summary>
    /// 전달된 트리거에 존재하는 객체 중 액터인 객체의 사이즈를 반환하는 메소드
    /// </summary>
    /// <param name="type"></param>
    /// <returns></returns>
    private ESizeType GetDamagableSize(ETriggerType_Giant_Player type)
    {
        Collider2D[] colliders = TriggerHandler.GetColliders(type);

        for (int i = 0; i < colliders.Length && colliders[i] != null; i++)
        {
            IActor_Damagable actor = colliders[i].GetComponentInParent<IActor_Damagable>();

            if (actor != null)
            {
                return actor.Size;
            }
        }
        return ESizeType.None;
    }

    public override void OnRestrain()
    {
        State = EStateType_Giant_Player.Restrain;
    }

    protected override void OnDie()
    {
        base.OnDie();
        if (!IsInvincible_Test)
        {
            State = EStateType_Giant_Player.Die;
            if(CastingSkill != null)
            {
                CastingSkill.Deactivate();
            }
        }
    }

    protected override void OnHit()
    {
        State = EStateType_Giant_Player.Hit;
        if(CastingSkill != null)
        {
            CastingSkill.Deactivate();
        }
    }

    protected override void Awake()
    {
        // 게임 태그가 플레이어라면 메인으로 등록
        if (gameObject.tag.Equals(GameTags.Player))
        {
            Main = this;
        }
        base.Awake();

        Status = new Status_Giant_Player(this);
        SouliteFrame = new SouliteFrame(6);

        SkillFrame = new SkillFrame_Giant_Player();

        TriggerHandler = GetComponent<TriggerHandler_Giant_Player>();
        AudioHandler = GetComponent<AudioHandler_Giant_Player>();
        AttackHandler = GetComponent<AttackHandler_Giant_Player>();
        SkillHandler = GetComponent<SkillHandler_Giant_Player>();

        // Input 을 설정
        SetInputs(new Agent_Giant_Player(this));
        // State를 설정
        SetStates(new Giant_Player_Idle(this)
            , new Giant_Player_Move(this)
            , new Giant_Player_Hit(this)
            , new Giant_Player_Die(this)
            , new Giant_Player_Restrain(this)
            , new Giant_Player_Release(this)
            , new Giant_Player_Roll(this)
            , new Giant_Player_RaiseHand(this)
            , new Giant_Player_Attack_Prepare(this, EStateType_Giant_Player.Attack_Prepare_Small_00)
            , new Giant_Player_Attack(this, EStateType_Giant_Player.Attack_Small_00)
            , new Giant_Player_Attack_Prepare(this, EStateType_Giant_Player.Attack_Prepare_Small_01)
            , new Giant_Player_Attack(this, EStateType_Giant_Player.Attack_Small_01)
            , new Giant_Player_Attack_Prepare(this, EStateType_Giant_Player.Attack_Prepare_Medium_00)
            , new Giant_Player_Attack(this, EStateType_Giant_Player.Attack_Medium_00)
            , new Giant_Player_Attack_Prepare(this, EStateType_Giant_Player.Attack_Prepare_Medium_01)
            , new Giant_Player_Attack(this, EStateType_Giant_Player.Attack_Medium_01)
            , new Giant_Player_Attack_Prepare(this, EStateType_Giant_Player.Attack_Prepare_Large_00)
            , new Giant_Player_Attack(this, EStateType_Giant_Player.Attack_Large_00)
            , new Giant_Player_Attack_Prepare(this, EStateType_Giant_Player.Attack_Prepare_Large_01)
            , new Giant_Player_Attack(this, EStateType_Giant_Player.Attack_Large_01));
    }

    protected override void Update()
    {
        if (CanCast && State != EStateType_Giant_Player.Die && State != EStateType_Giant_Player.Hit)
        {
            for (int i = 0; i < SkillFrame.Capacity; i++)
            {
                if (Input.SkillDown[i])
                {
                    ISkill_Giant_Player skill = SkillFrame.Peek(i);
                    // 프레임에 스킬이 존재하고 스킬 활성화에 성공했을 경우
                    if (skill != null && skill.Activate())
                    {
                        break;
                    }
                }
            }
        }

        base.Update();
    }

    private void Start()
    {
        // 스킬 데이터 불러오기
        for (ESkillType_Giant_Player e = ESkillType_Giant_Player.None + 1; e < ESkillType_Giant_Player.Count; e++)
        {
            int index = PlayerData.GetSkillMount(e);
            if (index >= 0)
            {
                SkillFrame.Mount(SkillHandler.Get(e), index);
            }
        }

        //ISkill_Giant_Player skill = SkillHandler.Get(ESkillType_Giant_Player.ArcaneTorrent);
        //skill.UnSeal();
        //SkillFrame.Mount(skill, 0);

        //skill = SkillHandler.Get(ESkillType_Giant_Player.DevourerStrike);
        //skill.UnSeal();
        //SkillFrame.Mount(skill, 1);

        //skill = SkillHandler.Get(ESkillType_Giant_Player.ShadowTearing);
        //skill.UnSeal();
        //SkillFrame.Mount(skill, 2);
    }

    private void OnDestroy()
    {
        if (gameObject.tag.Equals(GameTags.Player))
        {
            Main = null;
        }
    }
}