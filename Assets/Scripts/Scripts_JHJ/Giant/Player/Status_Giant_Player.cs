﻿using UnityEngine;
using System.Collections.Generic;

public class Status_Giant_Player : AStatus_Giant
{
    public new Giant_Player Actor { get; }

    /// <summary>
    /// 이름 태그
    /// </summary>
    public override string NameTag
    {
        get { return mDefault.NameTag; }
    }

    /// <summary>
    /// 플레이버 태그
    /// </summary>
    public override string FlavorTag
    {
        get { return mDefault.FlavorTag; }
    }

    /// <summary>
    /// 플레이어의 레벨 반환
    /// </summary>
    public override int Level
    {
        get { return PlayerData.Level; }
    }

    /// <summary>
    /// 플레이어의 경험치 반환
    /// </summary>
    public int Exp
    {
        get { return PlayerData.Exp; }
        set { PlayerData.Exp = value; }
    }

    /// <summary>
    /// 플레이어의 현재 레벨 기준 최대 경험치 반환
    /// </summary>
    public int MaxExp
    {
        get { return PlayerData.MaxExp; }
    }

    /// <summary>
    /// 기본 이동속도
    /// </summary>
    public override float MoveSpeed_Default
    {
        get { return mDefault.MoveSpeed; }
    }

    /// <summary>
    /// 어빌리티에 의한 이동속도 증가율
    /// </summary>
    public float MoveSpeed_Increase_Ability
    {
        get { return PlayerData.GetAbility(EFloatAbility.MoveSpeed_Giant); }
    }
    /// <summary>
    /// 이동속도 증가율
    /// </summary>
    public override float MoveSpeed_Increase
    {
        get { return base.MoveSpeed_Increase + MoveSpeed_Increase_Ability; }
    }

    /// <summary>
    /// 어빌리티에 의한 시전속도 증가율
    /// </summary>
    public float CastSpeed_Increase_Ability
    {
        get { return PlayerData.GetAbility(EFloatAbility.CastSpeed_Giant); }
    }
    /// <summary>
    /// 시전속도 증가율
    /// </summary>
    public override float CastSpeed_Increase
    {
        get { return base.CastSpeed_Increase + CastSpeed_Increase_Ability; }
    }

    /// <summary>
    /// 기본 항마력
    /// </summary>
    public int Exorcism_Default
    {
        get { return mDefault.Exorcism; }
    }
    /// <summary>
    /// 어빌리티에 의한 항마력 증가량
    /// </summary>
    public int Exorcism_Increment_Ability
    {
        get { return PlayerData.GetAbility(EIntAbility.Exorcism); }
    }
    /// <summary>
    /// 항마력 증가량
    /// </summary>
    public int Exorcism_Increment
    {
        get { return Exorcism_Increment_Ability; }
    }
    /// <summary>
    /// 항마력 증가율
    /// </summary>
    public float Exorcism_Increase
    {
        get;
    }
    /// <summary>
    /// 최종 항마력
    /// </summary>
    public int Exorcism
    {
        get { return (int)((Exorcism_Default + Exorcism_Increment) * (1.0f + Exorcism_Increase)); }
    }

    /// <summary>
    /// 기본 최대 생명력
    /// </summary>
    public override float MaxLife_Default
    {
        get { return mDefault.MaxLife; }
    }

    /// <summary>
    /// 어빌리티에 의한 최대 생명력 증가량
    /// </summary>
    public float MaxLife_Increment_Abiliity
    {
        get { return PlayerData.GetAbility(EFloatAbility.MaxLife); }
    }
    /// <summary>
    /// 최대 생명력 증가량
    /// </summary>
    public override float MaxLife_Increment
    {
        get { return base.MaxLife_Increment + MaxLife_Increment_Abiliity; }
    }

    /// <summary>
    /// 기본 생명력 재생
    /// </summary>
    public override float LifeRps_Default
    {
        get { return mDefault.LifeRps; }
    }
    /// <summary>
    /// 어빌리티에 의한 생명력 재생량 증가량
    /// </summary>
    public float LifeRps_Increment_Ability
    {
        get { return PlayerData.GetAbility(EFloatAbility.LifeRps); }
    }
    /// <summary>
    /// 생명력 재생량 증가량
    /// </summary>
    public override float LifeRps_Increment
    {
        get { return base.LifeRps_Increment + LifeRps_Increment_Ability; }
    }

    /// <summary>
    /// 기본 공격력
    /// </summary>
    public override float Offense_Default
    {
        get { return (1.0f + (Exorcism * GameConstant.OFFENSE_EXORCISM_CALIBRATION)); }
    }

    /// <summary>
    /// 기본 방어력
    /// </summary>
    public override float Defense_Default
    {
        get { return (1.0f + (Exorcism * GameConstant.DEFENSE_EXORCISM_CALIBRATION)); }
    }

    /// <summary>
    /// 기본 치명타 확률
    /// </summary>
    public override float CriticalChance_Default
    {
        get { return mDefault.CriticalChance; }
    }
    /// <summary>
    /// 어빌리티에 의한 치명타 확률 증가율
    /// </summary>
    public float CriticalChance_Increase_Ability
    {
        get { return PlayerData.GetAbility(EFloatAbility.CriticalChance_Giant); }
    }
    /// <summary>
    /// 치명타 확률 증가율
    /// </summary>
    public override float CriticalChance_Increase
    {
        get { return base.CriticalChance_Increase + CriticalChance_Increase_Ability; }
    }

    /// <summary>
    /// 기본 치명타 배율
    /// </summary>
    public override float CriticalMag_Default
    {
        get { return mDefault.CriticalMag; }
    }
    /// <summary>
    /// 어빌리티에 의한 치명타 배율 증가율
    /// </summary>
    public float CriticalMag_Increase_Ability
    {
        get { return PlayerData.GetAbility(EFloatAbility.CriticalMag_Giant); }
    }
    /// <summary>
    /// 치명타 배율 증가율
    /// </summary>
    public override float CriticalMag_Increase
    {
        get { return base.CriticalMag_Increase + CriticalMag_Increase_Ability; }
    }

    /// <summary>
    /// 기본 힘
    /// </summary>
    public override float Force_Default
    {
        get { return mDefault.Force; }
    }
    /// <summary>
    /// 기본 무게
    /// </summary>
    public override float Weight_Default
    {
        get { return mDefault.Weight; }
    }

    /// <summary>
    /// 기본 속성 친화도
    /// </summary>
    /// <param name="e"></param>
    /// <returns></returns>
    public override int GetEA_Default(EElementalType e)
    {
        return mDefault.GetEA(e);
    }

    /// <summary>
    /// 어빌리티에 의한 속성 친화도 증가량
    /// </summary>
    /// <param name="e"></param>
    /// <returns></returns>
    public int GetEA_Increment_Ability(EElementalType e)
    {
        return PlayerData.GetEA_Giant(e);
    }
    /// <summary>
    /// 속성 친화도 증가량
    /// </summary>
    /// <param name="e"></param>
    /// <returns></returns>
    public override int GetEA_Increment(EElementalType e)
    {
        return base.GetEA_Increment(e) + GetEA_Increment_Ability(e);
    }

    private DefaultStatus_Giant_Player mDefault;

    public Status_Giant_Player(Giant_Player actor) : base(actor)
    {
        Actor = actor;
        mDefault = DataManager.GetDefaultStatus(actor.Type);
        Life = MaxLife;
    }
}

public class DefaultStatus_Giant_Player
{
    public string NameTag { get; }
    public string FlavorTag { get; }

    public float MoveSpeed { get; }
    public float MaxLife { get; }
    public float LifeRps { get; }
    public int Exorcism { get; }
    public float CriticalChance { get; }
    public float CriticalMag { get; }
    public float Force { get; }
    public float Weight { get; }

    private Dictionary<EElementalType, int> mEaDict;

    public DefaultStatus_Giant_Player(Dictionary<string, string> statusDict)
    {
        mEaDict = new Dictionary<EElementalType, int>();

        NameTag = statusDict["Name Tag"];
        FlavorTag = statusDict["Flavor Tag"];
        MoveSpeed = float.Parse(statusDict["Move Speed"]);
        MaxLife = float.Parse(statusDict["Max Life"]);
        LifeRps = float.Parse(statusDict["Life Rps"]);
        Exorcism = int.Parse(statusDict["Exorcism"]);
        CriticalChance = float.Parse(statusDict["Critical Chance"]);
        CriticalMag = float.Parse(statusDict["Critical Mag"]);
        Force = float.Parse(statusDict["Force"]);
        Weight = float.Parse(statusDict["Weight"]);

        for(EElementalType e = EElementalType.None + 1; e < EElementalType.Count; e++)
        {
            mEaDict.Add(e, int.Parse(statusDict[e.ToString()]));
        }
    }

    public int GetEA(EElementalType e)
    {
        return mEaDict[e];
    }
}