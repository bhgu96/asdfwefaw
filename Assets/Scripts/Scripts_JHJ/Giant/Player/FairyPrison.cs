﻿using UnityEngine;
using System.Collections;

/// <summary>
/// 정령이 비활성화 된 상태에서 트리거 외부에서 활성화되는 경우 영역 내부로 들어오지 못하고, 
/// 정령이 밖으로 나가지 못하게 하는 콜라이더를 직접 설정해야 되는 단점이 있다. 
/// </summary>
public class FairyPrison : MonoBehaviour
{
    /// <summary>
    /// 정령이 지정된 구역 밖으로 벗어났다면 강제로 위치를 이동시킨다.
    /// </summary>
    /// <param name="collision"></param>
    private void OnTriggerExit2D(Collider2D collision)
    {
        if(collision.gameObject.layer == (int)ELayerFlags.Fairy)
        {
            Fairy_Player.Main.transform.position = transform.position;
        }
    }
}
