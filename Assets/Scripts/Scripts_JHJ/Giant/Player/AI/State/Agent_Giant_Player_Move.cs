﻿
public class Agent_Giant_Player_Move : State<EAgentFlags_Giant_Player>
{
    private Agent_Giant_Player mAgent;

    public Agent_Giant_Player_Move(Agent_Giant_Player agent) : base(EAgentFlags_Giant_Player.Move)
    {
        mAgent = agent;
    }

    public override void Update()
    {
        mAgent.SetMoveHorizontal(1.0f);

        // 모든 장착된 스킬에 대해
        for (int i = 0; i < mAgent.Actor.SkillFrame.Capacity; i++)
        {
            ISkill_Giant_Player skill = mAgent.Actor.SkillFrame.Peek(i);

            // 해당 스킬이 활성화 가능하고, 스킬 사용키가 눌리지 않았다면
            if (skill != null && skill.CanActivate && !mAgent.Skill[i])
            {
                // 해당 키를 누른다
                mAgent.SetSkill(true, i);
                return;
            }
            else
            {
                // 해당 키를 누르지 않는다.
                mAgent.SetSkill(false, i);
            }
        }

        // Move 상태 이외의 상태 조건이 정해지지 않았다
    }
}
