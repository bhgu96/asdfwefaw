﻿using UnityEngine;
using System.Collections;

public class Agent_Giant_Player_Idle : State<EAgentFlags_Giant_Player>
{
    private Agent_Giant_Player mAgent;

    public Agent_Giant_Player_Idle(Agent_Giant_Player agent) : base(EAgentFlags_Giant_Player.Idle)
    {
        mAgent = agent;
    }

    public override void Update()
    {
        mAgent.SetMoveHorizontal(0.0f);
        // 현재 Idle 상태 조건이 정해지지 않음
        mAgent.State = EAgentFlags_Giant_Player.Move;
    }
}
