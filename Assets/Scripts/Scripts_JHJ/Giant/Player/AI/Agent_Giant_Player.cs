﻿using UnityEngine;
using System.Collections;

public enum EAgentFlags_Giant_Player
{
    Idle = 0,
    Move = 1, 
}

public class Agent_Giant_Player : Agent<EAgentFlags_Giant_Player>
{
    public Giant_Player Actor { get; private set; }

    public Agent_Giant_Player(Giant_Player actor) : base(3)
    {
        Actor = actor;

        SetStates(new Agent_Giant_Player_Idle(this)
            , new Agent_Giant_Player_Move(this));
    }
}
