﻿using UnityEngine;

public class Skill_ArcaneTorrent_Cast : State<EStateType_Skill_ArcaneTorrent>
{
    private Skill_ArcaneTorrent mSkill;

    public Skill_ArcaneTorrent_Cast(Skill_ArcaneTorrent skill) : base(EStateType_Skill_ArcaneTorrent.Cast)
    {
        mSkill = skill;
    }

    public override void Start()
    {
        mSkill.Animator.SetInteger("state", (int)Type);
        mSkill.Animator.speed = mSkill.CastSpeed;
        // 스킬 캐스팅 불가능 설정
        mSkill.SetDisableCast();
        // 사용자 상태 방출로 설정
        mSkill.User.State = EStateType_Giant_Player.Release;
    }

    public override void End()
    {
        mSkill.Animator.speed = 1.0f;
    }
}
