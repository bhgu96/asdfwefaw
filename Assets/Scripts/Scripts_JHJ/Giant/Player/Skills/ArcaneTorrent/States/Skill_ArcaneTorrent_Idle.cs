﻿using UnityEngine;

public class Skill_ArcaneTorrent_Idle : State<EStateType_Skill_ArcaneTorrent>
{
    private Skill_ArcaneTorrent mSkill;

    public Skill_ArcaneTorrent_Idle(Skill_ArcaneTorrent skill) : base(EStateType_Skill_ArcaneTorrent.Idle)
    {
        mSkill = skill;
    }

    public override void Start()
    {
        mSkill.Animator.SetInteger("state", (int)Type);
    }
}
