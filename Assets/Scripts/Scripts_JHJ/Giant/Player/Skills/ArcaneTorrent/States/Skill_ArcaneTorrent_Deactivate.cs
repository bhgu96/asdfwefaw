﻿using UnityEngine;

public class Skill_ArcaneTorrent_Deactivate : State<EStateType_Skill_ArcaneTorrent>
{
    private Skill_ArcaneTorrent mSkill;

    public Skill_ArcaneTorrent_Deactivate(Skill_ArcaneTorrent skill) : base(EStateType_Skill_ArcaneTorrent.Deactivate)
    {
        mSkill = skill;
    }

    public override void Start()
    {
        mSkill.Animator.SetInteger("state", (int)Type);
        mSkill.SetEnableCast();
    }
}
