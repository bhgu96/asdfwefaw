﻿using UnityEngine;

public enum EStateType_Skill_ArcaneTorrent
{
    None = -1,

    Idle = 0,
    Cast = 1,
    Deactivate = 2,

    Count
}

public enum ETriggerType_Skill_ArcaneTorrent
{
    None = -1,

    Target,

    Count
}

public enum EAudioType_Skill_ArcaneTorrent
{
    None = -1, 



    Count
}

[RequireComponent(typeof(TriggerHandler_Skill_ArcaneTorrent), typeof(AudioHandler_Skill_ArcaneTorrent))]
public class Skill_ArcaneTorrent : ASkill_Giant_Player<EStateType_Skill_ArcaneTorrent>
{
    public override ESkillType_Giant_Player Type => ESkillType_Giant_Player.ArcaneTorrent;
    public override float CastTime => 0.3f;

    /// <summary>
    /// 기본 속성 타입
    /// </summary>
    public EElementalType ElementalType_Default { get { return EElementalType.Arcane; } }
    /// <summary>
    /// 변화된 속성 타입
    /// </summary>
    public EElementalType ElementalType_Change { get { return EElementalType.None; } }
    /// <summary>
    /// 최종 속성 타입
    /// </summary>
    public EElementalType ElementalType
    {
        get
        {
            EElementalType change = ElementalType_Change;
            if (change != EElementalType.None)
            {
                return change;
            }
            return ElementalType_Default;
        }
    }

    /// <summary>
    /// 기본 스킬 공격력
    /// </summary>
    public float SkillOffense_Default { get { return Parameters[0]; } }
    /// <summary>
    /// 스킬 공격력 증가량
    /// </summary>
    public float SkillOffense_Increment { get; }
    /// <summary>
    /// 스킬 공격력 증가율
    /// </summary>
    public float SkillOffense_Increase { get; }
    /// <summary>
    /// 최종 스킬 공격력
    /// </summary>
    public float SkillOffense
    {
        get { return (SkillOffense_Default + SkillOffense_Increment) * (1.0f + SkillOffense_Increase); }
    }

    /// <summary>
    /// 기본 포탄 개수
    /// </summary>
    public int ShellCount_Default { get { return (int)Parameters[1]; } }
    /// <summary>
    /// 포탄 개수 증가량
    /// </summary>
    public int ShellCount_Increment { get; }
    /// <summary>
    /// 최종 포탄 개수
    /// </summary>
    public int ShellCount
    {
        get { return ShellCount_Default + ShellCount_Increment; }
    }

    /// <summary>
    /// 최종 포탄 초기 가속도
    /// </summary>
    public float Acceleration_Start { get { return Parameters[2]; } }

    /// <summary>
    /// 최종 포탄 지속 가속도
    /// </summary>
    public float Acceleration_Update { get { return Parameters[3]; } }

    public TriggerHandler_Skill_ArcaneTorrent TriggerHandler { get; private set; }
    public AudioHandler_Skill_ArcaneTorrent AudioHandler { get; private set; }

    /// <summary>
    /// 포탄 생성 개수 만큼 포탄을 생성하고
    /// 트리거에 감지된 목표에 각 포탄을 순서대로 지정한다.
    /// </summary>
    public void GenerateShell(ETriggerType_Skill_ArcaneTorrent type)
    {
        Collider2D[] colliders = TriggerHandler.GetColliders(type);
        int shellCount = ShellCount;
        IActor_Damagable target = null;

        // 발사할 Shell 개수가 남아있고, 목표가 감지되었다면
        for (int i = 0; shellCount > 0 && i < colliders.Length && colliders[i] != null; i++)
        {
            target = colliders[i].GetComponentInParent<IActor_Damagable>();
            if (target != null)
            {
                Projectile_ElementalShell shell = null;

                // 스킬 속성이 Arcane인 경우
                if (ElementalType == EElementalType.Arcane)
                {
                    shell = ProjectilePool.Get(EProjectileType.ArcaneShell) as Projectile_ElementalShell;
                }

                if (shell != null)
                {
                    shell.transform.position = transform.position;
                    if (User.HorizontalDirection == EHorizontalDirection.Right)
                    {
                        shell.Angle = Random.Range(90.0f, 270.0f);
                    }
                    else if (User.HorizontalDirection == EHorizontalDirection.Left)
                    {
                        shell.Angle = Random.Range(-90.0f, 90.0f);
                    }
                    shell.IsFollowCamera = true;

                    shell.Target = target;
                    shell.DamageType = EDamageType.Giant;

                    shell.Acceleration_Start = Acceleration_Start;
                    shell.Acceleration_Update = Acceleration_Update;

                    shell.Offense = SkillOffense * User.Status.Offense;
                    shell.Level = User.Status.Level;
                    shell.ElementalAffinity = User.Status.GetEA(ElementalType);
                    shell.CriticalChance = User.Status.CriticalChance;
                    shell.CriticalMag = User.Status.CriticalMag;
                    shell.Force = User.Status.Force;

                    shell.Activate();

                    shellCount--;
                }
            }
        }

        // 목표는 감지되지 않았지만 Shell 개수가 남아있는 경우
        while (shellCount > 0)
        {
            Projectile_ElementalShell shell = null;

            // 스킬 속성이 Arcane인 경우
            if (ElementalType == EElementalType.Arcane)
            {
                shell = ProjectilePool.Get(EProjectileType.ArcaneShell) as Projectile_ElementalShell;
            }

            if (shell != null)
            {
                shell.transform.position = transform.position;
                if (User.HorizontalDirection == EHorizontalDirection.Right)
                {
                    shell.Angle = Random.Range(90.0f, 270.0f);
                }
                else if(User.HorizontalDirection == EHorizontalDirection.Left)
                {
                    shell.Angle = Random.Range(-90.0f, 90.0f);
                }
                shell.IsFollowCamera = true;

                shell.Target = target;
                shell.DamageType = EDamageType.Giant;

                shell.Acceleration_Start = Acceleration_Start;
                shell.Acceleration_Update = Acceleration_Update;

                shell.Offense = SkillOffense * User.Status.Offense;
                shell.Level = User.Status.Level;
                shell.ElementalAffinity = User.Status.GetEA(ElementalType);
                shell.CriticalChance = User.Status.CriticalChance;
                shell.CriticalMag = User.Status.CriticalMag;
                shell.Force = User.Status.Force;

                shell.Activate();

                shellCount--;
            }
        }
    }

    /// <summary>
    /// 스킬을 사용하여 타게팅할 대상이 있는가를 반환하는 메소드
    /// </summary>
    /// <param name="type"></param>
    /// <returns></returns>
    public bool CanTarget(ETriggerType_Skill_ArcaneTorrent type)
    {
        Collider2D[] colliders = TriggerHandler.GetColliders(type);
        if(colliders[0] != null)
        {
            return true;
        }
        return false;
    }

    /// <summary>
    /// 스킬 활성화 가능 여부
    /// </summary>
    public override bool CanActivate => base.CanActivate && CanTarget(ETriggerType_Skill_ArcaneTorrent.Target);

    protected override void OnActivate()
    {
        base.OnActivate();
        State = EStateType_Skill_ArcaneTorrent.Cast;
    }

    protected override void OnDeactivate()
    {
        base.OnDeactivate();
        State = EStateType_Skill_ArcaneTorrent.Deactivate;
    }

    protected override void Awake()
    {
        base.Awake();

        TriggerHandler = GetComponent<TriggerHandler_Skill_ArcaneTorrent>();
        AudioHandler = GetComponent<AudioHandler_Skill_ArcaneTorrent>();

        SetStates(new Skill_ArcaneTorrent_Idle(this)
            , new Skill_ArcaneTorrent_Cast(this)
            , new Skill_ArcaneTorrent_Deactivate(this));
    }
}
