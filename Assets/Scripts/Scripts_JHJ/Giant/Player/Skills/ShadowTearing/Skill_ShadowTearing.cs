﻿using UnityEngine;


public enum EStateType_Skill_ShadowTearing
{
    None = -1, 

    Idle = 0, 
    Cast = 1, 
    Active = 2, 
    Deactivate = 3, 

    Count
}

public enum EAudioType_Skill_ShadowTearing
{
    None = -1, 


    Count
}

public enum ETriggerType_Skill_ShadowTearing
{
    None = -1, 

    EffectiveMoment, 

    Count
}

[RequireComponent(typeof(AudioHandler_Skill_ShadowTearing), typeof(TriggerHandler_Skill_ShadowTearing))]
public class Skill_ShadowTearing : ASkill_Giant_Player<EStateType_Skill_ShadowTearing>
{
    /// <summary>
    /// 스킬 타입
    /// </summary>
    public override ESkillType_Giant_Player Type => ESkillType_Giant_Player.ShadowTearing;

    /// <summary>
    /// 시전 시간
    /// </summary>
    public override float CastTime => 1.2f;

    /// <summary>
    /// 분신의 개수
    /// </summary>
    public int CountShadow_Default { get { return (int)Parameters[0]; } }
    /// <summary>
    /// 최종 분신의 개수
    /// </summary>
    public int CountShadow
    {
        get { return CountShadow_Default; }
    }

    /// <summary>
    /// 분신 이동거리
    /// </summary>
    public float MovementShadow_Default { get { return Parameters[1]; } }
    /// <summary>
    /// 최종 분신 이동거리
    /// </summary>
    public float MovementShadow
    {
        get { return MovementShadow_Default; }
    }

    /// <summary>
    /// 기본 스킬 공격력
    /// </summary>
    public float SkillOffense_Default { get { return Parameters[2]; } }
    /// <summary>
    /// 최종 스킬 공격력
    /// </summary>
    public float SkillOffense
    {
        get { return SkillOffense_Default; }
    }

    /// <summary>
    /// 기본 분신 유지 시간
    /// </summary>
    public float ActiveTime_Default { get { return Parameters[3]; } }
    /// <summary>
    /// 최종 분신 유지 시간
    /// </summary>
    public float ActiveTime
    {
        get { return ActiveTime_Default; }
    }

    /// <summary>
    /// 기본 분신 생성 텀
    /// </summary>
    public float ActiveTerm_Default { get { return Parameters[4]; } }
    /// <summary>
    /// 최종 분신 생성 텀
    /// </summary>
    public float ActiveTerm
    {
        get { return ActiveTerm_Default; }
    }

    /// <summary>
    /// 활성화 가능 여부
    /// </summary>
    public override bool CanActivate => base.CanActivate && IsEffective();

    public AudioHandler_Skill_ShadowTearing AudioHandler { get; private set; }
    public TriggerHandler_Skill_ShadowTearing TriggerHandler { get; private set; }

    /// <summary>
    /// 현재 스킬을 사용하면 효율적인 순간인지 반환하는 메소드
    /// </summary>
    /// <returns></returns>
    public bool IsEffective()
    {
        if(TriggerHandler.GetColliders(ETriggerType_Skill_ShadowTearing.EffectiveMoment)[0] != null)
        {
            return true;
        }
        return false;
    }

    /// <summary>
    /// 투사체를 생성하는 메소드
    /// </summary>
    public void GenerateProjectile()
    {
        Projectile_ShadowAlterEgo projectile = ProjectilePool.Get(EProjectileType.ShadowAlterEgo) as Projectile_ShadowAlterEgo;
        if(projectile != null)
        {
            projectile.transform.position = transform.position;
            projectile.HorizontalDirection = User.HorizontalDirection;

            projectile.ActiveTime = ActiveTime;
            projectile.Speed = MovementShadow / ActiveTime;
            projectile.Offense = SkillOffense * User.Status.Offense;
            projectile.ElementalAffinity = User.Status.GetEA(projectile.ElementalType);
            projectile.Level = User.Status.Level;
            projectile.Force = User.Status.Force;
            projectile.CriticalChance = User.Status.CriticalChance;
            projectile.CriticalMag = User.Status.CriticalMag;
            projectile.IsFollowCamera = true;
            projectile.DamageType = EDamageType.Giant;

            projectile.Activate();
        }
    }

    protected override void OnActivate()
    {
        base.OnActivate();
        State = EStateType_Skill_ShadowTearing.Cast;
    }

    protected override void OnDeactivate()
    {
        base.OnDeactivate();
        State = EStateType_Skill_ShadowTearing.Deactivate;
    }

    protected override void Awake()
    {
        base.Awake();

        AudioHandler = GetComponent<AudioHandler_Skill_ShadowTearing>();
        TriggerHandler = GetComponent<TriggerHandler_Skill_ShadowTearing>();

        // 상태 설정
        SetStates(new Skill_ShadowTearing_Idle(this)
            , new Skill_ShadowTearing_Cast(this)
            , new Skill_ShadowTearing_Active(this)
            , new Skill_ShadowTearing_Deactivate(this));
    }

}
