﻿using UnityEngine;

public class Skill_ShadowTearing_Deactivate : State<EStateType_Skill_ShadowTearing>
{
    private Skill_ShadowTearing mSkill;
    
    public Skill_ShadowTearing_Deactivate(Skill_ShadowTearing skill) : base(EStateType_Skill_ShadowTearing.Deactivate)
    {
        mSkill = skill;
    }

    public override void Start()
    {
        mSkill.Animator.SetInteger("state", (int)Type);
        mSkill.SetEnableCast();
    }
}
