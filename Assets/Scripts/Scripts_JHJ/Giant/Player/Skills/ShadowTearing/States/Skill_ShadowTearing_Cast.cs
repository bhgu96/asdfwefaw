﻿using UnityEngine;

public class Skill_ShadowTearing_Cast : State<EStateType_Skill_ShadowTearing>
{
    private Skill_ShadowTearing mSkill;

    public Skill_ShadowTearing_Cast(Skill_ShadowTearing skill) : base(EStateType_Skill_ShadowTearing.Cast)
    {
        mSkill = skill;
    }

    public override void Start()
    {
        mSkill.Animator.SetInteger("state", (int)Type);
        mSkill.Animator.speed = mSkill.CastSpeed;
        mSkill.SetDisableCast();

        mSkill.User.State = EStateType_Giant_Player.RaiseHand;
    }

    public override void End()
    {
        mSkill.Animator.speed = 1.0f;
    }
}
