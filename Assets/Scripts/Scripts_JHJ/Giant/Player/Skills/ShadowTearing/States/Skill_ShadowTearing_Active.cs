﻿using UnityEngine;

public class Skill_ShadowTearing_Active : State<EStateType_Skill_ShadowTearing>
{
    private Skill_ShadowTearing mSkill;
    private float mActiveTerm;
    private int mCountShadow;

    public Skill_ShadowTearing_Active(Skill_ShadowTearing skill) : base(EStateType_Skill_ShadowTearing.Active)
    {
        mSkill = skill;
    }

    public override void Start()
    {
        mSkill.Animator.SetInteger("state", (int)Type);
        // 활성화 텀 초기화
        mActiveTerm = mSkill.ActiveTerm;
        // 분신 생성 가능 개수 초기화
        mCountShadow = mSkill.CountShadow;
        // 투사체 생성
        mSkill.GenerateProjectile();
        mCountShadow--;
        // 생성할 투사체의 개수가 0이하라면
        if(mCountShadow <= 0)
        {
            mSkill.State = EStateType_Skill_ShadowTearing.Deactivate;
        }
    }

    public override void Update()
    {
        // 활성화 텀이 남아있다면
        if (mActiveTerm > 0.0f)
        {
            // 활성화 텀 갱신
            mActiveTerm -= Time.deltaTime;
        }
        else
        {
            // 활성화 텀 초기화
            mActiveTerm = mSkill.ActiveTerm;

            // 투사체 생성
            mSkill.GenerateProjectile();
            mCountShadow--;
            // 생성할 투사체의 개수가 0이하라면
            if(mCountShadow <= 0)
            {
                mSkill.State = EStateType_Skill_ShadowTearing.Deactivate;
            }
        }
    }
}
