﻿using UnityEngine;

public class Skill_ShadowTearing_Idle : State<EStateType_Skill_ShadowTearing>
{
    private Skill_ShadowTearing mSkill;

    public Skill_ShadowTearing_Idle(Skill_ShadowTearing skill) : base(EStateType_Skill_ShadowTearing.Idle)
    {
        mSkill = skill;
    }

    public override void Start()
    {
        mSkill.Animator.SetInteger("state", (int)Type);
    }
}
