﻿using UnityEngine;
using System.Collections;
using Object;

public class SkillFrame_Giant_Player : Frame<ISkill_Giant_Player, ESkillType_Giant_Player>
{
    public SkillFrame_Giant_Player() : base(3)
    { }

    protected override void OnUnMount(int index)
    {
        ISkill_Giant_Player skill = Peek(index);
        if(skill != null)
        {
            PlayerData.SetSkillMount(skill.Type, -1);
        }
    }

    protected override void OnMount(int index)
    {
        ISkill_Giant_Player skill = Peek(index);
        if(skill != null)
        {
            PlayerData.SetSkillMount(skill.Type, index);
        }
    }
}
