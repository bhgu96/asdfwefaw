﻿using UnityEngine;

public enum EStateType_Skill_DevourerStrike
{
    None = -1, 

    Idle = 0, 
    Active = 1, 
    Deactivate = 2, 

    Count
}

public enum ETriggerType_Skill_DevourerStrike
{
    None = -1, 

    Hit = 0, 
    Target = 1, 

    Count
}

public enum EAudioType_Skill_DevourerStrike
{
    None = -1, 

    Count
}

[RequireComponent(typeof(TriggerHandler_Skill_DevourerStrike), typeof(AudioHandler_Skill_DevourerStrike), typeof(AttackHandler_Skill_DevourerStrike))]
public class Skill_DevourerStrike : ASkill_Giant_Player<EStateType_Skill_DevourerStrike>
{
    /// <summary>
    /// 스킬 타입
    /// </summary>
    public override ESkillType_Giant_Player Type => ESkillType_Giant_Player.DevourerStrike;
    /// <summary>
    /// 스킬 시전 시간
    /// </summary>
    public override float CastTime => 0.0f;

    /// <summary>
    /// 기본 스킬 속성
    /// </summary>
    public EElementalType ElementalType_Default { get { return EElementalType.Kinematic; } }
    /// <summary>
    /// 스킬 속성 변화
    /// </summary>
    public EElementalType ElementalType_Change { get { return EElementalType.None; } }
    /// <summary>
    /// 최종 스킬 속성
    /// </summary>
    public EElementalType ElementalType
    {
        get
        {
            EElementalType change = ElementalType_Change;
            // 속성변화가 존재한다면
            if(change == EElementalType.None)
            {
                return ElementalType_Default;
            }
            return change;
        }
    }

    /// <summary>
    /// 기본 스킬 공격력
    /// </summary>
    public float SkillOffense_Default { get { return Parameters[0]; } }
    /// <summary>
    /// 스킬 공격력 증가량
    /// </summary>
    public float SkillOffense_Increment { get; }
    /// <summary>
    /// 스킬 공격력 증가율
    /// </summary>
    public float SkillOffense_Increase { get; }
    /// <summary>
    /// 최종 스킬 공격력
    /// </summary>
    public float SkillOffense
    {
        get { return (SkillOffense_Default + SkillOffense_Increment) * (1.0f + SkillOffense_Increase); }
    }

    /// <summary>
    /// 기본 스킬 움직임
    /// </summary>
    public float Movement_Default { get { return Parameters[1]; } }
    /// <summary>
    /// 스킬 움직임 증가율
    /// </summary>
    public float Movement_Increase { get; }
    /// <summary>
    /// 최종 스킬 움직임
    /// </summary>
    public float Movement
    {
        get { return Movement_Default * (1.0f + Movement_Increase); }
    }

    /// <summary>
    /// 스킬 활성화 시간
    /// </summary>
    public float ActiveTime { get { return 0.9f; } }

    public TriggerHandler_Skill_DevourerStrike TriggerHandler { get; private set; }
    public AudioHandler_Skill_DevourerStrike AudioHandler { get; private set; }
    public AttackHandler_Skill_DevourerStrike AttackHandler { get; private set; }

    /// <summary>
    /// 공격 가능한 객체를 감지하고 공격하는 메소드
    /// </summary>
    /// <param name="type"></param>
    public void Attack(ETriggerType_Skill_DevourerStrike type)
    {
        Collider2D[] colliders = TriggerHandler.GetColliders(type);

        for(int i=0; i<colliders.Length && colliders[i] != null; i++)
        {
            IDamagable damagable = colliders[i].GetComponentInParent<IDamagable>();
            if(damagable != null)
            {
                Damage damage = damagable.Damage;

                float possibility = Random.Range(0.0f, 1.0f);
                // 치명타라면
                if (possibility <= User.Status.CriticalChance)
                {
                    damage.SetDamage(false, true, true, EEffectType.Hit_Shards_00, EDamageType.Giant, ElementalType
                        , User.Status.Level, User.Status.GetEA(ElementalType), User.Status.Offense * SkillOffense * User.Status.CriticalMag, User.Status.Force
                        , AttackHandler.Get(type).transform.position, User);
                }
                else
                {
                    damage.SetDamage(false, true, false, EEffectType.Hit_Shards_00, EDamageType.Giant, ElementalType
                        , User.Status.Level, User.Status.GetEA(ElementalType), User.Status.Offense * SkillOffense, User.Status.Force
                        , AttackHandler.Get(type).transform.position, User);
                }

                damagable.ActivateDamage();
            }
        }
    }

    /// <summary>
    /// 타게팅 가능 여부 메소드
    /// </summary>
    /// <param name="type"></param>
    /// <returns></returns>
    public bool CanTarget(ETriggerType_Skill_DevourerStrike type)
    {
        Collider2D[] colliders = TriggerHandler.GetColliders(type);

        if(colliders[0] != null)
        {
            return true;
        }
        return false;
    }

    /// <summary>
    /// 활성화 가능 여부
    /// </summary>
    public override bool CanActivate => base.CanActivate && CanTarget(ETriggerType_Skill_DevourerStrike.Target);

    protected override void OnActivate()
    {
        base.OnActivate();
        State = EStateType_Skill_DevourerStrike.Active;
    }

    protected override void OnDeactivate()
    {
        base.OnDeactivate();
        State = EStateType_Skill_DevourerStrike.Deactivate;
    }

    protected override void Awake()
    {
        base.Awake();

        TriggerHandler = GetComponent<TriggerHandler_Skill_DevourerStrike>();
        AudioHandler = GetComponent<AudioHandler_Skill_DevourerStrike>();
        AttackHandler = GetComponent<AttackHandler_Skill_DevourerStrike>();

        SetStates(new Skill_DevourerStrike_Idle(this)
            , new Skill_DevourerStrike_Active(this)
            , new Skill_DevourerStrike_Deactivate(this));
    }
}
