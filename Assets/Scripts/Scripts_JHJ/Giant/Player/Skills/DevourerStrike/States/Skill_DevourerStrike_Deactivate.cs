﻿using UnityEngine;

public class Skill_DevourerStrike_Deactivate : State<EStateType_Skill_DevourerStrike>
{
    private Skill_DevourerStrike mSkill;

    public Skill_DevourerStrike_Deactivate(Skill_DevourerStrike skill) : base(EStateType_Skill_DevourerStrike.Deactivate)
    {
        mSkill = skill;
    }

    public override void Start()
    {
        mSkill.Animator.SetInteger("state", (int)Type);
        mSkill.SetEnableCast();
    }
}
