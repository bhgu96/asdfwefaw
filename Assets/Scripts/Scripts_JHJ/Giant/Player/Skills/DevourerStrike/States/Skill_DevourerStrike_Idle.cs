﻿using UnityEngine;

public class Skill_DevourerStrike_Idle : State<EStateType_Skill_DevourerStrike>
{
    private Skill_DevourerStrike mSkill;

    public Skill_DevourerStrike_Idle(Skill_DevourerStrike skill) : base(EStateType_Skill_DevourerStrike.Idle)
    {
        mSkill = skill;
    }

    public override void Start()
    {
        mSkill.Animator.SetInteger("state", (int)Type);
    }
}
