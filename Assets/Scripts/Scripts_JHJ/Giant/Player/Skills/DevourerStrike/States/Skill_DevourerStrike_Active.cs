﻿using UnityEngine;

public class Skill_DevourerStrike_Active : State<EStateType_Skill_DevourerStrike>
{
    private Skill_DevourerStrike mSkill;
    private float mHorizontalSpeed;

    public Skill_DevourerStrike_Active(Skill_DevourerStrike skill) : base(EStateType_Skill_DevourerStrike.Active)
    {
        mSkill = skill;
    }

    public override void Start()
    {
        mSkill.Animator.SetInteger("state", (int)Type);
        mHorizontalSpeed = mSkill.Movement / mSkill.ActiveTime;
        mSkill.SetDisableCast();
        mSkill.User.State = EStateType_Giant_Player.Roll;
    }

    public override void FixedUpdate()
    {
        mSkill.User.MoveHorizontal(mHorizontalSpeed, mSkill.User.HorizontalDirection);

        mSkill.Attack(ETriggerType_Skill_DevourerStrike.Hit);
    }
}
