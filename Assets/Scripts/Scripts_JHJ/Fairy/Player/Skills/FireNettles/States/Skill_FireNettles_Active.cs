﻿using UnityEngine;
using System.Collections;

public class Skill_FireNettles_Active : State<EStateType_Skill_FireNettles>
{
    private Skill_FireNettles mSkill;
    private float mFireRemainTime;

    public Skill_FireNettles_Active(Skill_FireNettles skill) : base(EStateType_Skill_FireNettles.Active)
    {
        mSkill = skill;
    }

    public override void Start()
    {
        if(mSkill.FireArrow())
        {
            mSkill.Animator.SetInteger("state", (int)Type);
            mFireRemainTime = mSkill.FireTerm;
        }
        else
        {
            mSkill.Deactivate();
        }
    }

    public override void Update()
    {
        if(mSkill.User.State != EStateType_Fairy_Player.Dash)
        {
            if(mFireRemainTime > 0.0f)
            {
                mFireRemainTime -= Time.deltaTime;
            }
            else if(mSkill.FireArrow())
            {
                mFireRemainTime = mSkill.FireTerm;
            }
            else
            {
                mSkill.Deactivate();
            }
        }
        else
        {
            mSkill.Deactivate();
        }
    }
}
