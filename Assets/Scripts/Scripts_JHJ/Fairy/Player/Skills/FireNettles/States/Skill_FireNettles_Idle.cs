﻿using UnityEngine;
using System.Collections;

public class Skill_FireNettles_Idle : State<EStateType_Skill_FireNettles>
{
    private Skill_FireNettles mSkill;

    public Skill_FireNettles_Idle(Skill_FireNettles skill) : base(EStateType_Skill_FireNettles.Idle)
    {
        mSkill = skill;
    }

    public override void Start()
    {
        mSkill.Animator.SetInteger("state", (int)Type);
    }
}
