﻿using UnityEngine;
using System.Collections;

public class Skill_FireNettles_Cast : State<EStateType_Skill_FireNettles>
{
    private Skill_FireNettles mSkill;

    public Skill_FireNettles_Cast(Skill_FireNettles skill) : base(EStateType_Skill_FireNettles.Cast)
    {
        mSkill = skill;
    }

    public override void Start()
    {
        mSkill.Animator.SetInteger("state", (int)Type);
        mSkill.Animator.speed = mSkill.CastSpeed;

        mSkill.User.State = EStateType_Fairy_Player.Shake;
    }

    public override void End()
    {
        mSkill.Animator.speed = 1.0f;
    }
}
