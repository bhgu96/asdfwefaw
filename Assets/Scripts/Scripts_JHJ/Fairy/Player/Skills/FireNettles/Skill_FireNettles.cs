﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum EStateType_Skill_FireNettles
{
    Idle, 
    Cast, 
    Active, 
    Deactivate, 
}


public class Skill_FireNettles : ASkill_Fairy_Player<EStateType_Skill_FireNettles>
{
    public override ESkillType_Fairy_Player Type => ESkillType_Fairy_Player.FireNettles;

    public override float CastTime => 0.55f;

    [SerializeField]
    private Transform mFrontAimTransform;
    public Transform FrontAimTransform { get { return mFrontAimTransform; } }

    /// <summary>
    /// 기본 속성
    /// </summary>
    public EElementalType ElementalType_Default { get { return EElementalType.Earth; } }
    /// <summary>
    /// 변환된 속성
    /// </summary>
    public EElementalType ElementalType_Change { get { return EElementalType.None; } }
    /// <summary>
    /// 최종 속성 타입
    /// </summary>
    public EElementalType ElementalType
    {
        get
        {
            EElementalType change = ElementalType_Change;
            if(change == EElementalType.None)
            {
                return ElementalType_Default;
            }
            return change;
        }
    }

    /// <summary>
    /// 기본 스킬 공격력
    /// </summary>
    public float SkillOffense_Default { get { return Parameters[0]; } }
    /// <summary>
    /// 스킬 공격력 증가량
    /// </summary>
    public float SkillOffense_Increment { get; }
    /// <summary>
    /// 스킬 공격력 증가율
    /// </summary>
    public float SkillOffense_Increase { get; }
    /// <summary>
    /// 최종 스킬 공격력
    /// </summary>
    public float SkillOffense
    {
        get { return (SkillOffense_Default + SkillOffense_Increment) * (1.0f + SkillOffense_Increase); }
    }

    /// <summary>
    /// 기본 화살 갯수
    /// </summary>
    public int ArrowCount_Default { get { return (int)Parameters[1]; } }
    /// <summary>
    /// 화살 갯수 증가량
    /// </summary>
    public int ArrowCount_Increment { get; }
    /// <summary>
    /// 최종 화살 갯수
    /// </summary>
    public int ArrowCount
    {
        get { return ArrowCount_Default + ArrowCount_Increment; } 
    }

    /// <summary>
    /// 기본 화살 발사 각도
    /// </summary>
    public float ArrowAngle_Default { get { return Parameters[2]; } }
    /// <summary>
    /// 화살 발사 각도 감소율
    /// </summary>
    public float ArrowAngle_Decrease { get; }
    /// <summary>
    /// 최종 화살 발사 각도
    /// </summary>
    public float ArrowAngle
    {
        get { return ArrowAngle_Default * (1.0f - ArrowAngle_Decrease); }
    }

    /// <summary>
    /// 기본 화살 발사 거리
    /// </summary>
    public float ArrowDistance_Default { get { return Parameters[3]; } }
    /// <summary>
    /// 화살 발사 거리 증가율
    /// </summary>
    public float ArrowDistance_Increase { get; }
    /// <summary>
    /// 최종 화살 발사 거리
    /// </summary>
    public float ArrowDistance
    {
        get { return Parameters[3] * (1.0f + ArrowDistance_Increase); }
    }

    /// <summary>
    /// 기본 화살 발사 간격
    /// </summary>
    public float FireTerm_Default { get { return Parameters[4]; } }
    /// <summary>
    /// 화살 발사 간격 감소율
    /// </summary>
    public float FireTerm_Decrease { get; }
    /// <summary>
    /// 최종 화살 발사 간격
    /// </summary>
    public float FireTerm
    {
        get { return FireTerm_Default * (1.0f - FireTerm_Decrease); }
    }

    /// <summary>
    /// 기본 화살 속도
    /// </summary>
    public float ArrowSpeed_Default { get { return Parameters[5]; } }
    /// <summary>
    /// 화살 속도 증가율
    /// </summary>
    public float ArrowSpeed_Increase { get; }
    /// <summary>
    /// 최종 화살 속도
    /// </summary>
    public float ArrowSpeed
    {
        get { return ArrowSpeed_Default * (1.0f + ArrowSpeed_Increase); }
    }

    /// <summary>
    /// 남은 화살 갯수
    /// </summary>
    public int ArrowRemain { get; set; }

    /// <summary>
    /// 화살 발사 메소드
    /// </summary>
    /// <returns></returns>
    public bool FireArrow()
    {
        if (ArrowRemain > 0)
        {
            ArrowRemain--;

            Projectile_ElementalArrow arrow = null;
            if (ElementalType == EElementalType.Earth)
            {
                arrow = ProjectilePool.Get(EProjectileType.EarthElementalArrow) as Projectile_ElementalArrow;
            }

            if (arrow != null)
            {
                arrow.DamageType = EDamageType.Fairy;
                arrow.transform.position = FrontAimTransform.position;
                arrow.IsFollowCamera = true;
                float angle = transform.eulerAngles.z;
                angle = Random.Range(angle - ArrowAngle * 0.5f, angle + ArrowAngle * 0.5f);
                arrow.Angle = angle;
                arrow.Distance = ArrowDistance;
                arrow.Speed = ArrowSpeed;
                arrow.Offense = SkillOffense * User.Status.Offense;
                arrow.ElementalAffinity = User.Status.GetEA(arrow.ElementalType);
                arrow.Level = User.Status.Level;
                arrow.CriticalChance = User.Status.CriticalChance;
                arrow.CriticalMag = User.Status.CriticalMag;
                arrow.Force = User.Status.Force;

                arrow.Activate();
            }

            return ArrowRemain > 0;
        }
        return false;
    }

    protected override void OnActivate()
    {
        base.OnActivate();
        ArrowRemain = ArrowCount;
        State = EStateType_Skill_FireNettles.Cast;
        SetDisableCast();
    }

    protected override void OnDeactivate()
    {
        base.OnDeactivate();
        State = EStateType_Skill_FireNettles.Deactivate;
        SetEnableCast();
    }

    protected override void Awake()
    {
        base.Awake();
        SetStates(new Skill_FireNettles_Idle(this)
            , new Skill_FireNettles_Cast(this)
            , new Skill_FireNettles_Active(this)
            , new Skill_FireNettles_Deactivate(this));
    }
}
