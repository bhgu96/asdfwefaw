﻿using UnityEngine;
using System.Collections;

public class Skill_ChloranthyHammer_Cast : State<EStateType_Skill_ChloranthyHammer>
{
    private Skill_ChloranthyHammer mSkill;
    private bool mIsStarted = false;

    public Skill_ChloranthyHammer_Cast(Skill_ChloranthyHammer skill) : base(EStateType_Skill_ChloranthyHammer.Cast)
    {
        mSkill = skill;
    }

    public override void Start()
    {
        // 스킬이 시전중이지 않다면
        if (!mIsStarted)
        {
            mIsStarted = true;
            mSkill.Animator.SetInteger("state", (int)Type);
            mSkill.Animator.speed = mSkill.CastSpeed;
        }
        // 스킬이 시전중이라면
        else
        {
            mSkill.Animator.SetTrigger("cast");
        }
    }

    public override void End()
    {
        mSkill.Animator.speed = 1.0f;
        mIsStarted = false;
        mSkill.Deactivate();
    }
}
