﻿using UnityEngine;
using System.Collections;

public class Skill_ChloranthyHammer_Idle : State<EStateType_Skill_ChloranthyHammer>
{
    private Skill_ChloranthyHammer mSkill;

    public Skill_ChloranthyHammer_Idle(Skill_ChloranthyHammer skill) : base(EStateType_Skill_ChloranthyHammer.Idle)
    {
        mSkill = skill;
    }

    public override void Start()
    {
        mSkill.Animator.SetInteger("state", (int)Type);
    }
}
