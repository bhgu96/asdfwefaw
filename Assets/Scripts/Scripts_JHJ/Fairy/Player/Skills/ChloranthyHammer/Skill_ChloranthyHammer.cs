﻿using UnityEngine;
using System.Collections;

public enum EStateType_Skill_ChloranthyHammer
{
    None = -1, 

    Idle, 
    Cast, 

    Count
}

public enum ETriggerType_Skill_ChloranthyHammer
{
    None = -1, 

    Cast, 

    Count
}

[RequireComponent(typeof(TriggerHandler_Skill_ChloranthyHammer), typeof(AttackHandler_Skill_ChloranthyHammer))]
public class Skill_ChloranthyHammer : ASkill_Fairy_Player<EStateType_Skill_ChloranthyHammer>
{
    public override ESkillType_Fairy_Player Type => ESkillType_Fairy_Player.ChloranthyHammer;
    /// <summary>
    /// 기본 속성 타입
    /// </summary>
    public EElementalType ElementalType_Default { get { return EElementalType.Earth; } }
    /// <summary>
    /// 변환된 속성 타입
    /// </summary>
    public EElementalType ElementalType_Change { get { return EElementalType.None; } }
    /// <summary>
    /// 최종 속성 타입    
    /// </summary>
    public EElementalType ElementalType
    {
        get
        {
            EElementalType change = ElementalType_Change;
            if(change == EElementalType.None)
            {
                return ElementalType_Default;
            }
            return change;
        }
    }

    public override float CastTime => 0.0f;

    /// <summary>
    /// 기본 스킬 공격력
    /// </summary>
    public float SkillOffense_Default { get { return Parameters[0]; } }
    /// <summary>
    /// 스킬 공격력 증가량
    /// </summary>
    public float SkillOffense_Increment { get; }
    /// <summary>
    /// 스킬 공격력 증가율
    /// </summary>
    public float SkillOffense_Increase { get; }
    /// <summary>
    /// 최종 스킬 공격력
    /// </summary>
    public float SkillOffense
    {
        get { return (SkillOffense_Default + SkillOffense_Increment) * (1.0f + SkillOffense_Increase); }
    }

    /// <summary>
    /// 기본 둔화 확률
    /// </summary>
    public float SlowingChance_Default { get { return Parameters[1]; } }
    /// <summary>
    /// 둔화 확률 증가율
    /// </summary>
    public float SlowingChance_Increase { get; }
    /// <summary>
    /// 최종 둔화 확률
    /// </summary>
    public float SlowingChance
    {
        get { return SlowingChance_Default + SlowingChance_Increase; }
    }

    /// <summary>
    /// 기본 둔화 지속시간
    /// </summary>
    public float SlowingTime_Default { get { return Parameters[2]; } }
    /// <summary>
    /// 둔화 지속시간 증가량
    /// </summary>
    public float SlowingTime_Increment { get; }
    /// <summary>
    /// 둔화 지속시간 증가율
    /// </summary>
    public float SlowingTime_Increase { get; }
    /// <summary>
    /// 최종 둔화 지속시간
    /// </summary>
    public float SlowingTime
    {
        get { return (SlowingTime_Default + SlowingTime_Increment) * (1.0f + SlowingTime_Increase); }
    }

    /// <summary>
    /// 둔화로 인한 기본 이동속도 감소율
    /// </summary>
    public float Slowing_MoveSpeed_Default { get { return Parameters[3]; } }
    /// <summary>
    /// 둔화로 인한 이동속도 감소 증가율
    /// </summary>
    public float Slowing_MoveSpeed_Increase { get; }
    /// <summary>
    /// 둔화로 인한 최종 이동속도 감소율
    /// </summary>
    public float Slowing_MoveSpeed
    {
        get { return Slowing_MoveSpeed_Default + Slowing_MoveSpeed_Increase; }
    }

    /// <summary>
    /// 둔화로 인한 기본 시전속도 감소율
    /// </summary>
    public float Slowing_CastSpeed_Default { get { return Parameters[4]; } }
    /// <summary>
    /// 둔화로 인한 시전속도 감소 증가율
    /// </summary>
    public float Slowing_CastSpeed_Increase { get; }
    /// <summary>
    /// 둔화로 인한 최종 시전속도 감소율
    /// </summary>
    public float Slowing_CastSpeed
    {
        get { return Slowing_CastSpeed_Default + Slowing_CastSpeed_Increase; }
    }

    public TriggerHandler_Skill_ChloranthyHammer TriggerHandler { get; private set; }

    public AttackHandler_Skill_ChloranthyHammer AttackHandler { get; private set; }

    /// <summary>
    /// 데미지를 발생시키는 메소드
    /// </summary>
    /// <param name="triggerType"></param>
    public void Attack(ETriggerType_Skill_ChloranthyHammer triggerType)
    {
        float random;

        Collider2D[] colliders = TriggerHandler.GetColliders(triggerType);

        for (int i=0; i<colliders.Length && colliders[i] != null; i++)
        {
            IDamagable damagable = colliders[i].GetComponentInParent<IDamagable>();

            // 공격 대상이 액터인 경우
            if (damagable is IActor actor)
            {
                random = Random.Range(0.0f, 1.0f);
                // 둔화 확률에 성공했다면
                if (random <= SlowingChance)
                {
                    // 둔화 디버프 시전
                    Debuff_Slowing debuff = actor.DebuffHandler.Get(EDebuffType.Slowing_Bush) as Debuff_Slowing;
                    debuff.MoveSpeed_Decrease = Slowing_MoveSpeed;
                    debuff.CastSpeed_Decrease = Slowing_CastSpeed;
                    debuff.Activate(SlowingTime);
                }
            }

            if (damagable != null)
            {
                Damage damage = damagable.Damage;

                random = Random.Range(0.0f, 1.0f);
                // 치명타인 경우
                if (random <= User.Status.CriticalChance)
                {
                    damage.SetDamage(false, true, true, EEffectType.Hit_Break_00, EDamageType.Fairy, ElementalType
                        , User.Status.Level, User.Status.GetEA(ElementalType), User.Status.Offense * SkillOffense * User.Status.CriticalMag, User.Status.Force
                        , AttackHandler.Get(triggerType).transform.position, User);
                }
                else
                {
                    damage.SetDamage(false, true, false, EEffectType.Hit_Break_00, EDamageType.Fairy, ElementalType
                        , User.Status.Level, User.Status.GetEA(ElementalType), User.Status.Offense * SkillOffense, User.Status.Force
                        , AttackHandler.Get(triggerType).transform.position, User);
                }

                damagable.ActivateDamage();
            }
        }
    }

    protected override void OnActivate()
    {
        base.OnActivate();
        State = EStateType_Skill_ChloranthyHammer.Cast;
        User.State = EStateType_Fairy_Player.Brandish;
        SetDisableCast();
    }

    protected override void OnDeactivate()
    {
        base.OnDeactivate();
        SetEnableCast();
    }

    protected override void Awake()
    {
        base.Awake();
        TriggerHandler = GetComponent<TriggerHandler_Skill_ChloranthyHammer>();
        AttackHandler = GetComponent<AttackHandler_Skill_ChloranthyHammer>();

        SetStates(new Skill_ChloranthyHammer_Idle(this)
            , new Skill_ChloranthyHammer_Cast(this));
    }
}

