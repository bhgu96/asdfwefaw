﻿using UnityEngine;
using System;
using System.Collections.Generic;
using Object;

public enum ESkillType_Fairy_Player
{
    None = -1,

    // 녹화의 망치
    ChloranthyHammer, 
    // 쐐기풀 속사
    FireNettles, 
    // 자연화
    Naturalize, 

    Count
}

public interface ISkill_Fairy_Player : ISkill_Fairy, IMountable<ESkillType_Fairy_Player>
{
    /// <summary>
    /// 스킬 봉인 해제를 위한 비용
    /// </summary>
    int Cost { get; }
    /// <summary>
    /// 스킬 봉인 해제 여부 프로퍼티
    /// </summary>
    bool IsUnSealed { get; }
    /// <summary>
    /// 스킬 봉인 해제 가능 여부 프로퍼티
    /// </summary>
    bool CanUnSeal { get; }
    /// <summary>
    /// 스킬 봉인 가능 여부 프로퍼티
    /// </summary>
    bool CanSeal { get; }

    /// <summary>
    /// 스킬을 봉인 해제 설정하는 메소드
    /// </summary>
    /// <returns></returns>
    bool UnSeal();

    /// <summary>
    /// 스킬을 봉인 설정하는 메소드
    /// </summary>
    bool Seal();
}


public abstract class ASkill_Fairy_Player<EState>
    : ASkill_Fairy<Fairy_Player, EState>
    , ISkill_Fairy_Player
    where EState : Enum
{
    public abstract ESkillType_Fairy_Player Type { get; }

    /// <summary>
    /// 이름 태그 프로퍼티
    /// </summary>
    public sealed override string NameTag { get { return mSkillInfo.NameTag; } }
    /// <summary>
    /// 설명 태그 프로퍼티
    /// </summary>
    public sealed override string DescriptionTag { get { return mSkillInfo.DescriptionTag; } }
    /// <summary>
    /// 옵션 태그 프로퍼티
    /// </summary>
    public sealed override string OptionTag { get { return mSkillInfo.OptionTag; } }
    /// <summary>
    /// 부가 옵션 태그 프로퍼티
    /// </summary>
    public sealed override string AdditionalOptionTag { get { return mSkillInfo.AdditionalOptionTag; } }

    /// <summary>
    /// 스킬을 봉인 해제하기 위한 비용
    /// </summary>
    public int Cost { get { return mSkillInfo.Cost; } }

    /// <summary>
    /// 기본 재사용 대기시간 프로퍼티
    /// </summary>
    public sealed override float CoolTime_Default { get { return mSkillInfo.CoolTime; } }

    /// <summary>
    /// 기본 마나 소모량 프로퍼티
    /// </summary>
    public sealed override float ManaConsumption_Default { get { return mSkillInfo.ManaConsumption; } }

    /// <summary>
    /// 스킬 가변 인자 상수 배열 프로퍼티
    /// </summary>
    public sealed override float[] Parameters { get { return mSkillInfo.Parameters; } }

    /// <summary>
    /// 스킬 봉인 해제 여부 프로퍼티
    /// 스킬 봉인 해제 여부는 플레이어 데이터에 저장되어야 한다.
    /// </summary>
    public bool IsUnSealed
    {
        get { return PlayerData.GetSkillUnSeal(Type); }
        private set { PlayerData.SetSkillUnSeal(Type, value); }
    }

    /// <summary>
    /// 스킬 장착 여부 프로퍼티
    /// 스킬 착용 여부는 플레이어 데이터에 저장되어야 한다.
    /// </summary>
    public bool IsMounted { get; private set; } = false;

    /// <summary>
    /// 스킬 봉인 해제 가능 여부 프로퍼티
    /// </summary>
    public virtual bool CanUnSeal
    {
        get { return !IsUnSealed && PlayerData.Souls >= Cost; }
    }

    /// <summary>
    /// 스킬 봉인 가능 여부 프로퍼티
    /// </summary>
    public virtual bool CanSeal
    {
        get { return IsUnSealed && !IsMounted; }
    }

    /// <summary>
    /// 스킬 장착 가능 여부 프로퍼티
    /// </summary>
    public virtual bool CanBeMounted
    {
        get { return IsUnSealed && !IsMounted; }
    }

    /// <summary>
    /// 스킬 장착 해제 가능 여부 프로퍼티
    /// </summary>
    public virtual bool CanBeUnMounted
    {
        get { return IsMounted; }
    }

    /// <summary>
    /// 스킬 활성화 가능 여부
    /// </summary>
    public override bool CanActivate
    {
        get { return base.CanActivate && IsMounted && User.Status.Mana >= ManaConsumption; }
    }

    private SkillInfo_Fairy_Player mSkillInfo;

    /// <summary>
    /// 스킬을 봉인 해제 설정하는 메소드
    /// </summary>
    /// <returns></returns>
    public bool UnSeal()
    {
        if (CanUnSeal)
        {
            IsUnSealed = true;
            PlayerData.Souls -= Cost;
            OnUnSeal();
            return true;
        }
        return false;
    }

    /// <summary>
    /// 스킬을 봉인 설정하는 메소드
    /// </summary>
    public bool Seal()
    {
        if (CanSeal)
        {
            IsUnSealed = false;
            OnSeal();
            return true;
        }
        return false;
    }

    /// <summary>
    /// 스킬을 장착 설정하는 메소드
    /// </summary>
    /// <returns></returns>
    public bool BeMounted()
    {
        if (CanBeMounted)
        {
            IsMounted = true;
            OnBeMounted();
            return true;
        }
        return false;
    }

    /// <summary>
    /// 스킬을 장착 해제 설정하는 메소드
    /// </summary>
    /// <returns></returns>
    public bool BeUnMounted()
    {
        if (CanBeUnMounted)
        {
            IsMounted = false;
            OnBeUnMounted();
            return true;
        }
        return false;
    }

    /// <summary>
    /// 스킬 활성화 성공시 호출되는 콜백 메소드
    /// </summary>
    protected override void OnActivate()
    {
        User.Status.Mana -= ManaConsumption;
    }

    /// <summary>
    /// 스킬 봉인 해제시 호출되는 콜백 메소드
    /// </summary>
    protected virtual void OnUnSeal()
    { }

    /// <summary>
    /// 스킬 봉인시 호출되는 콜백 메소드
    /// </summary>
    protected virtual void OnSeal()
    { }

    /// <summary>
    /// 스킬 장착 설정시 호출되는 콜백 메소드
    /// </summary>
    protected virtual void OnBeMounted()
    { }

    /// <summary>
    /// 스킬 장착 해제 설정시 호출되는 콜백 메소드
    /// </summary>
    protected virtual void OnBeUnMounted()
    { }

    protected override void Awake()
    {
        base.Awake();
        mSkillInfo = DataManager.GetSkillInfo(Type);
    }
}

/// <summary>
/// 플레이어 정령 액티브 스킬 정보 클래스
/// </summary>
public class SkillInfo_Fairy_Player
{
    public string NameTag { get; }
    public string DescriptionTag { get; }
    public string OptionTag { get; }
    public string AdditionalOptionTag { get; }
    public int Cost { get; }
    public float ManaConsumption { get; }
    public float CoolTime { get; }
    public float[] Parameters { get; }

    public SkillInfo_Fairy_Player(Dictionary<string, string> skillDict)
    {
        Parameters = new float[GameConstant.MAX_SKILL_PARAMETERS];

        NameTag = skillDict["Name Tag"];
        DescriptionTag = skillDict["Desc Tag"];
        OptionTag = skillDict["Option Tag"];
        AdditionalOptionTag = skillDict["Additional Tag"];
        Cost = int.Parse(skillDict["Cost"]);
        ManaConsumption = float.Parse(skillDict["Mana Consumption"]);
        CoolTime = float.Parse(skillDict["Cool Time"]);

        for(int i=0; i < GameConstant.MAX_SKILL_PARAMETERS; i++)
        {
            Parameters[i] = float.Parse(skillDict[i.ToString()]);
        }
    }
}