﻿using UnityEngine;
using System.Collections;

public enum EStateType_Skill_Naturalize
{
    Idle, 
    Cast, 
}

public class Skill_Naturalize : ASkill_Fairy_Player<EStateType_Skill_Naturalize>
{
    public override ESkillType_Fairy_Player Type => ESkillType_Fairy_Player.Naturalize;

    public override float CastTime => 1.0f;

    /// <summary>
    /// 기본 스킬 공격력
    /// </summary>
    public float SkillOffense_Default { get { return Parameters[0]; } }
    /// <summary>
    /// 스킬 공격력 증가량
    /// </summary>
    public float SkillOffense_Increment { get; }
    /// <summary>
    /// 스킬 공격력 증가율
    /// </summary>
    public float SkillOffense_Increase { get; }
    /// <summary>
    /// 최종 스킬 공격력
    /// </summary>
    public float SkillOffense
    {
        get { return (SkillOffense_Default + SkillOffense_Increment) * (1.0f + SkillOffense_Increase); }
    }

    /// <summary>
    /// 기본 시전 범위
    /// </summary>
    public float CastDistance_Default { get { return Parameters[1]; } }
    /// <summary>
    /// 시전 범위 증가율
    /// </summary>
    public float CastDistance_Increase { get; }
    /// <summary>
    /// 최종 시전 범위
    /// </summary>
    public float CastDistance
    {
        get { return CastDistance_Default * (1.0f + CastDistance_Increase); }
    }

    /// <summary>
    /// 기본 구속 시간
    /// </summary>
    public float RestrainTime_Default { get { return Parameters[2]; } }
    /// <summary>
    /// 구속 시간 증가량
    /// </summary>
    public float RestrainTime_Incremenet { get; }
    /// <summary>
    /// 구속 시간 증가율
    /// </summary>
    public float RestrainTime_Increase { get; }
    /// <summary>
    /// 최종 구속 시간
    /// </summary>
    public float RestrainTime
    {
        get { return (RestrainTime_Default + RestrainTime_Incremenet) * (1.0f + RestrainTime_Increase); }
    }

    public void Attack()
    {
        Projectile_Naturalize naturalize = ProjectilePool.Get(EProjectileType.Naturalize) as Projectile_Naturalize;
        if(naturalize != null)
        {
            naturalize.DamageType = EDamageType.Fairy;
            naturalize.transform.position = transform.position;
            naturalize.RestrainTime = RestrainTime;
            naturalize.Distance = CastDistance;
            naturalize.Offense = SkillOffense * User.Status.Offense;
            naturalize.ElementalAffinity = User.Status.GetEA(naturalize.ElementalType);
            naturalize.Level = User.Status.Level;
            naturalize.CriticalChance = User.Status.CriticalChance;
            naturalize.CriticalMag = User.Status.CriticalMag;
            naturalize.Force = 0.0f;

            naturalize.Activate();
        }
    }

    protected override void OnActivate()
    {
        base.OnActivate();
        State = EStateType_Skill_Naturalize.Cast;
        SetDisableCast();
    }

    protected override void OnDeactivate()
    {
        base.OnDeactivate();
        State = EStateType_Skill_Naturalize.Idle;
        SetEnableCast();
    }

    protected override void Awake()
    {
        base.Awake();

        SetStates(new Skill_Naturalize_Idle(this)
            , new Skill_Naturalize_Cast(this));
    }
}
