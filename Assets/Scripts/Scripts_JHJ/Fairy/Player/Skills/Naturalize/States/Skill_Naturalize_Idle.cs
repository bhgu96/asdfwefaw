﻿using UnityEngine;
using System.Collections;

public class Skill_Naturalize_Idle : State<EStateType_Skill_Naturalize>
{
    private Skill_Naturalize mSkill;

    public Skill_Naturalize_Idle(Skill_Naturalize skill) : base(EStateType_Skill_Naturalize.Idle)
    {
        mSkill = skill;
    }

    public override void Start()
    {
        mSkill.Animator.SetInteger("state", (int)Type);
    }
}
