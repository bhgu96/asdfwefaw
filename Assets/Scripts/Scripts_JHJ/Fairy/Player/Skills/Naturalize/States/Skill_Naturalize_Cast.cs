﻿using UnityEngine;
using System.Collections;

public class Skill_Naturalize_Cast : State<EStateType_Skill_Naturalize>
{
    private Skill_Naturalize mSkill;

    public Skill_Naturalize_Cast(Skill_Naturalize skill) : base(EStateType_Skill_Naturalize.Cast)
    {
        mSkill = skill;
    }

    public override void Start()
    {
        mSkill.Animator.SetInteger("state", (int)Type);
        mSkill.Animator.speed = mSkill.CastSpeed;
        mSkill.User.State = EStateType_Fairy_Player.Spout;
    }

    public override void End()
    {
        mSkill.Attack();
        mSkill.Animator.speed = 1.0f;
    }
}
