﻿using UnityEngine;
using System.Collections;
using Object;

public class SkillFrame_Fairy_Player : Frame<ISkill_Fairy_Player, ESkillType_Fairy_Player>
{
    public SkillFrame_Fairy_Player() : base(3)
    { }

    protected override void OnUnMount(int index)
    {
        ISkill_Fairy_Player skill = Peek(index);
        if(skill != null)
        {
            PlayerData.SetSkillMount(skill.Type, -1);
        }
    }

    protected override void OnMount(int index)
    {
        ISkill_Fairy_Player skill = Peek(index);
        if(skill != null)
        {
            PlayerData.SetSkillMount(skill.Type, index);
        }
    }
}
