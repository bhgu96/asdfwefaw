﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FairyAimHolder : MonoBehaviour
{
    [SerializeField]
    private Transform  mFrontAim;
    public Transform FrontAim { get { return mFrontAim; } }

    /// <summary>
    /// Aim의 현재 각도
    /// </summary>
    public float Angle
    {
        get { return transform.eulerAngles.z; }
        set { transform.eulerAngles = new Vector3(0.0f, 0.0f, value); }
    }

    public EHorizontalDirection HorizontalDirection
    {
        get
        {
            float angle = Angle;
            if(angle >= -90.0f && angle <= 90.0f)
            {
                return EHorizontalDirection.Right;
            }
            return EHorizontalDirection.Left;
        }
    }

    public void SetAngle(Vector2 direction)
    {
        if(direction.x != 0.0f || direction.y != 0.0f)
        {
            direction.Normalize();
            if(direction.y >= 0.0f)
            {
                transform.eulerAngles = new Vector3(0.0f, 0.0f, Mathf.Acos(direction.x) * Mathf.Rad2Deg);
            }
            else
            {
                transform.eulerAngles = new Vector3(0.0f, 0.0f, -Mathf.Acos(direction.x) * Mathf.Rad2Deg);
            }
        }
    }

    private void Update()
    {
        // 일시정지 상태가 아닌 경우
        if (!StateManager.Pause.State)
        {
            // Input Mode가 Keyboard-Mouse Mode인 경우
            if (StateManager.InputMode.State == EInputMode.Keyboard)
            {
                // 현재 위치 가져옴
                Vector2 curPosition = transform.position;
                // 메인 카메라의 카메라 하나를 가져옴
                Camera camera = CameraController.Main.GetCamera(ECameraType.Background);
                // 스크린 상의 마우스 위치를 World Position으로 변환
                Vector2 mousePosition = camera.ScreenToWorldPoint(Input.mousePosition);

                // 마우스의 위치와 현재 위치의 차 = 마우스의 위치로 향하는 방향 벡터
                Vector2 direction = mousePosition - curPosition;

                // ArcTan 함수의 에러 방지를 위해 x, y가 모두 0이 되는 경우를 피한다
                if (direction.x != 0.0f || direction.y != 0.0f)
                {
                    SetAngle(direction);
                }
            }
            // Input Mode가 GamePad Mode인 경우
            else if (StateManager.InputMode.State == EInputMode.GamePad)
            {
                // 게임 패드의 Right Axis 값을 이용해서 Aim 조준선의 위치를 바꾼다.
                Vector2 axisDirection = new Vector2(InputManager.GetFloat(EInputType.Right_Aim, EInputMode.GamePad)
                    - InputManager.GetFloat(EInputType.Left_Aim, EInputMode.GamePad)
                    , InputManager.GetFloat(EInputType.Up_Aim, EInputMode.GamePad)
                    - InputManager.GetFloat(EInputType.Down_Aim, EInputMode.GamePad));

                if (Mathf.Abs(axisDirection.x) > 0.1f || Mathf.Abs(axisDirection.y) > 0.1f)
                {
                    SetAngle(axisDirection);
                }
            }
        }
    }
}
