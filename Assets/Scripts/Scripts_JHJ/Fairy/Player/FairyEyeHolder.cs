﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FairyEyeHolder : MonoBehaviour
{
    [SerializeField]
    private Transform mEyeRenderer;

    private float mAngle;
    public float Angle
    {
        get
        {
            return mAngle;
        }
        set
        {
            // value 를 [-180.0, 180.0]의 범위로 만드는 작업
            while (value > 180.0f)
            {
                value -= 360.0f;
            }

            while (value < -180.0f)
            {
                value += 360.0f;
            }

            mAngle = value;

            if (!mIsSetDirection)
            {
                mIsSetDirection = true;
                Direction = new Vector2(Mathf.Sin(mAngle), Mathf.Cos(mAngle));
            }
            else
            {
                mIsSetDirection = false;
            }

            // 오른쪽 방향인 경우
            if (mAngle <= 90.0f && mAngle >= -90.0f)
            {
                HorizontalDirection = EHorizontalDirection.Right;
                transform.eulerAngles = new Vector3(0.0f, 0.0f, mAngle);
            }
            // 왼쪽 방향인 경우
            else
            {
                HorizontalDirection = EHorizontalDirection.Left;
                transform.eulerAngles = new Vector3(0.0f, 0.0f, mAngle - 180.0f);
            }

            mEyeRenderer.eulerAngles = Vector3.zero;
        }
    }

    private EHorizontalDirection mHorizontalDirection;
    public EHorizontalDirection HorizontalDirection
    {
        get { return mHorizontalDirection; }
        set
        {
            if (mHorizontalDirection != value)
            {
                mHorizontalDirection = value;
                Vector3 localScale = transform.localScale;
                transform.localScale = new Vector3(localScale.x * -1.0f, localScale.y, localScale.z);
            }
        }
    }

    public Vector2 Direction { get; private set; }
    private bool mIsSetDirection = false;

    public void SetDirection(Vector2 direction, bool isNormalized)
    {
        if (!isNormalized)
        {
            direction.Normalize();
        }
        mIsSetDirection = true;
        Direction = direction;

        if (direction.x != 0.0f || direction.y != 0.0f)
        {
            if (direction.y >= 0.0f)
            {
                Angle = Mathf.Acos(direction.x) * Mathf.Rad2Deg;
            }
            else
            {
                Angle = -Mathf.Acos(direction.x) * Mathf.Rad2Deg;
            }
        }
    }

    private void Awake()
    {
        if(transform.lossyScale.x > 0.0f)
        {
            mHorizontalDirection = EHorizontalDirection.Right;
        }
        else
        {
            mHorizontalDirection = EHorizontalDirection.Left;
        }
    }
}
