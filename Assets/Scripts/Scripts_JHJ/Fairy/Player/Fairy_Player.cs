﻿using UnityEngine;

public enum EStateType_Fairy_Player
{
    // 기본상태
    Idle = 0,
    // 이동상태
    Move = 1,
    // 대시 상태
    Dash = 2,
    // 대시 중 벽에 부딪힌 상태
    Dash_Blocked = 3,
    // 휘두르기 상태
    Brandish = 4,
    // 흔들리기 상태
    Shake = 5,
    // 뿜어내기 상태
    Spout = 6,
    // 구속 상태
    Restrain = 7,

}
public enum ETriggerType_Fairy_Player
{

}
public enum EAudioFlags_Fairy_Player
{

}

[RequireComponent(typeof(TriggerHandler_Fairy_Player), typeof(AudioHandler_Fairy_Player), typeof(AttackHandler_Fairy_Player))]
[RequireComponent(typeof(SkillHandler_Fairy_Player))]
public class Fairy_Player : AActor_Fairy<EStateType_Fairy_Player>
{
    public const float DASH_COOL_TIME = 0.2F;

    /// <summary>
    /// 메인 정령
    /// </summary>
    public static Fairy_Player Main { get; private set; } = null;

    [SerializeField]
    private EFairyType_Player mType;
    /// <summary>
    /// 플레이어 요정 타입
    /// </summary>
    public EFairyType_Player Type { get { return mType; } }

    /// <summary>
    /// 정령의 조준선
    /// </summary>
    public FairyAimHolder AimHolder { get; private set; }
    /// <summary>
    /// 정령의 눈
    /// </summary>
    public FairyEyeHolder EyeHolder { get; private set; }

    private Status_Fairy_Player mStatus;
    /// <summary>
    /// 정령의 능력치
    /// </summary>
    public new Status_Fairy_Player Status
    {
        get { return mStatus; }
        set { base.Status = value; mStatus = value; }
    }

    /// <summary>
    /// 사용 할 수 있는 모든 스킬들을 관리하는 Skill Handler
    /// </summary>
    public SkillHandler_Fairy_Player SkillHandler { get; private set; }
    /// <summary>
    /// 사용할 스킬을 등록할 수 있는 Skill Frame
    /// </summary>
    public SkillFrame_Fairy_Player SkillFrame { get; private set; }

    /// <summary>
    /// 해당 객체가 사용하는 모든 트리거를 관리하는 Trigger Handler
    /// </summary>
    public TriggerHandler_Fairy_Player TriggerHandler { get; private set; }
    /// <summary>
    /// 해당 객체가 사용하는 모든 오디오를 관리하는 Audio Handler
    /// </summary>
    public AudioHandler_Fairy_Player AudioHandler { get; private set; }
    /// <summary>
    /// 해당 객체가 가지는 모든 기본 공격 시작점을 관리하는 Attack Handler
    /// </summary>
    public AttackHandler_Fairy_Player AttackHandler { get; private set; }

    /// <summary>
    /// 인벤토리
    /// </summary>
    public Inventory Inventory { get; private set; }
    /// <summary>
    /// 영혼석을 장착할 수 있는 Soulite Frame
    /// </summary>
    public SouliteFrame SouliteFrame { get; private set; }

    /// <summary>
    /// 이전 Fixed Update 시의 좌표
    /// </summary>
    public Vector2 PrevPosition { get; private set; }

    private float mDashRemainTime;
    public bool CanDash
    {
        get { return mDashRemainTime <= 0.0f; }
        set
        {
            if (value)
            {
                mDashRemainTime = 0.0f;
            }
            else
            {
                mDashRemainTime = DASH_COOL_TIME;
            }
        }
    }


    /// <summary>
    /// 정령이 카메라를 따라다닐 것인지 설정하는 변수
    /// </summary>
    public bool IsFollowCamera
    {
        get { return mIsFollowCamera; }
        set
        {
            mIsFollowCamera = value;
            // 정령이 카메라를 따라다닌다면
            if (mIsFollowCamera)
            {
                // 카메라의 Position 저장
                mPrevCameraPosition = CameraController.Main.transform.position;
            }
        }
    }
    [SerializeField]
    [Tooltip("정령이 카메라를 따라다닐 것인지 설정하는 변수")]
    private bool mIsFollowCamera;
    // 카메라의 이전 루틴의 Position
    private Vector2 mPrevCameraPosition;

    private float mManaRpsRemainTime;

    /// <summary>
    /// 구속 상태가 되었을때 호출되는 콜백 메소드
    /// </summary>
    public override void OnRestrain()
    {
        State = EStateType_Fairy_Player.Restrain;
    }

    protected override void Awake()
    {
        // 게임 태그가 플레이어라면 메인으로 등록
        if (gameObject.tag.Equals(GameTags.Player))
        {
            Main = this;
        }
        base.Awake();

        Inventory = new Inventory(this, 16, 8);
        SouliteFrame = new SouliteFrame(6);

        SkillFrame = new SkillFrame_Fairy_Player();

        SkillHandler = GetComponent<SkillHandler_Fairy_Player>();

        Status = new Status_Fairy_Player(this);

        AimHolder = GetComponentInChildren<FairyAimHolder>(true);
        EyeHolder = GetComponentInChildren<FairyEyeHolder>(true);

        TriggerHandler = GetComponent<TriggerHandler_Fairy_Player>();
        AudioHandler = GetComponent<AudioHandler_Fairy_Player>();
        AttackHandler = GetComponent<AttackHandler_Fairy_Player>();

        SetInputs(new ActorController());

        // 상태 할당
        SetStates(new Fairy_Player_Idle(this)
            , new Fairy_Player_Move(this)
            , new Fairy_Player_Dash(this)
            , new Fairy_Player_DashBlocked(this)
            , new Fairy_Player_Brandish(this)
            , new Fairy_Player_Shake(this)
            , new Fairy_Player_Restrain(this)
            , new Fairy_Player_Spout(this));
    }

    private void Start()
    {
        // 스킬 플레이어 데이터 불러오기
        for (ESkillType_Fairy_Player e = ESkillType_Fairy_Player.None + 1; e < ESkillType_Fairy_Player.Count; e++)
        {
            int index = PlayerData.GetSkillMount(e);
            if (index >= 0)
            {
                SkillFrame.Mount(SkillHandler.Get(e), index);
            }
        }

        if (mIsFollowCamera)
        {
            mPrevCameraPosition = CameraController.Main.transform.position;
        }
        PrevPosition = transform.position;
        

        //ISkill_Fairy_Player skill = SkillHandler.Get(ESkillType_Fairy_Player.ChloranthyHammer);
        //skill.UnSeal();
        //SkillFrame.Mount(skill, 0);

        //skill = SkillHandler.Get(ESkillType_Fairy_Player.FireNettles);
        //skill.UnSeal();
        //SkillFrame.Mount(skill, 1);

        //skill = SkillHandler.Get(ESkillType_Fairy_Player.Naturalize);
        //skill.UnSeal();
        //SkillFrame.Mount(skill, 2);
    }

    protected override void Update()
    {
        if (!StateManager.Pause.State)
        {
            // 마나 자연 재생
            if (mManaRpsRemainTime > 0.0f)
            {
                mManaRpsRemainTime -= Time.deltaTime;
            }
            else
            {
                mManaRpsRemainTime = GameConstant.ONE_SECOND;
                Status.Mana += Status.ManaRps;
            }

            // 대시 쿨타임 갱신
            if (!CanDash)
            {
                mDashRemainTime -= Time.deltaTime;
            }

            // 스킬을 시전할 수 있다면
            if (CanCast)
            {
                for (int i = 0; i < SkillFrame.Capacity; i++)
                {
                    if (Input.SkillDown[i])
                    {
                        ISkill_Fairy_Player skill = SkillFrame.Peek(i);
                        if (skill != null)
                        {
                            skill.Activate();
                            break;
                        }
                    }
                }
            }

            base.Update();
        }
    }

    protected override void FixedUpdate()
    {
        // 일시정지 상태가 아니라면
        if (!StateManager.Pause.State)
        {
            // 카메라의 움직임을 따라 좌표를 갱신
            if (mIsFollowCamera)
            {
                Vector2 curCameraPosition = CameraController.Main.transform.position;
                transform.Translate(curCameraPosition - mPrevCameraPosition, Space.World);
                mPrevCameraPosition = curCameraPosition;
            }
            // 입력값에 대한 속도 갱신
            AddVelocity(Input.MoveHorizontal, Input.MoveVertical);

            base.FixedUpdate();
            // 이전 좌표 갱신
            PrevPosition = transform.position;
        }
    }

    private void OnDestroy()
    {
        if (gameObject.tag.Equals(GameTags.Player))
        {
            Main = null;
        }
    }
}
