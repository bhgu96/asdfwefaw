﻿using UnityEngine;
using System.Collections;

public class Fairy_Player_Dash : State<EStateType_Fairy_Player>
{
    private Fairy_Player mActor;
    private float mRemainTime;

    public Fairy_Player_Dash(Fairy_Player actor) : base(EStateType_Fairy_Player.Dash)
    {
        mActor = actor;
    }

    public override void Start()
    {
        mRemainTime = GameConstant.DASH_COOL_TIME;

        mActor.Animator.SetInteger("state", (int)Type);
        Vector2 aimDirection = mActor.AimHolder.FrontAim.position - mActor.AimHolder.transform.position;
        float moveSpeed = mActor.Status.MoveSpeed;
        mActor.Velocity = aimDirection * moveSpeed * 2.0f;

        mActor.SetDirection(aimDirection, true);
        mActor.EyeHolder.SetDirection(aimDirection, true);
    }

    public override void Update()
    {
        if(mRemainTime > 0.0f)
        {
            mRemainTime -= Time.deltaTime;
        }
        else if(mActor.Input.Dash)
        {
            mActor.Animator.SetTrigger("dash");
            Start();
        }
    }

    public override void FixedUpdate()
    {
        // 벽에 부딪힌 경우
        if(mActor.IsGround)
        {
            mActor.State = EStateType_Fairy_Player.Dash_Blocked;
        }
    }
}
