﻿using UnityEngine;
using System.Collections;

public class Fairy_Player_Restrain : State<EStateType_Fairy_Player>
{
    private Fairy_Player mActor;

    public Fairy_Player_Restrain(Fairy_Player actor) : base(EStateType_Fairy_Player.Restrain)
    {
        mActor = actor;
    }

    public override void Start()
    {
        mActor.Animator.SetInteger("state", (int)Type);
    }

    public override void Update()
    {
        if(!mActor.DebuffHandler.Get(EDebuffType.Restrain_Bush).IsActivated)
        {
            mActor.State = EStateType_Fairy_Player.Idle;
        }
    }
}
