﻿using UnityEngine;
using System.Collections;

public class Fairy_Player_DashBlocked : State<EStateType_Fairy_Player>
{
    private Fairy_Player mActor;

    private float mRemainTime;

    public Fairy_Player_DashBlocked(Fairy_Player actor) : base(EStateType_Fairy_Player.Dash_Blocked)
    {
        mActor = actor;
    }

    public override void Start()
    {
        mActor.Animator.SetInteger("state", (int)Type);
        mRemainTime = GameConstant.DASH_COOL_TIME;
    }

    public override void Update()
    {
        if(mRemainTime > 0.0f)
        {
            mRemainTime -= Time.deltaTime;
        }
        else if(mActor.Input.Dash)
        {
            mActor.State = EStateType_Fairy_Player.Dash;
        }
    }

}