﻿using UnityEngine;
using System.Collections;

public class Fairy_Player_Brandish : State<EStateType_Fairy_Player>
{
    private Fairy_Player mActor;
    private bool mIsStarted = false;

    public Fairy_Player_Brandish(Fairy_Player actor) : base(EStateType_Fairy_Player.Brandish)
    {
        mActor = actor;
    }

    public override void Start()
    {
        Vector2 aimDirection = mActor.AimHolder.FrontAim.position - mActor.AimHolder.transform.position;
        mActor.SetDirection(aimDirection, true);
        mActor.EyeHolder.SetDirection(aimDirection, true);

        if (!mIsStarted)
        {
            mIsStarted = true;
            mActor.Animator.SetInteger("state", (int)Type);
            mActor.Animator.speed = mActor.Status.CastSpeed;
        }
        else
        {
            mActor.Animator.SetTrigger("brandish");
        }
    }

    public override void End()
    {
        mIsStarted = false;
        mActor.Animator.speed = 1.0f;
    }
}
