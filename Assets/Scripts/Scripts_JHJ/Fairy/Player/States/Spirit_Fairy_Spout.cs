﻿using UnityEngine;
using System.Collections;

public class Fairy_Player_Spout : State<EStateType_Fairy_Player>
{
    private Fairy_Player mActor;

    public Fairy_Player_Spout(Fairy_Player actor) : base(EStateType_Fairy_Player.Spout)
    {
        mActor = actor;
    }

    public override void Start()
    {
        mActor.Animator.SetInteger("state", (int)Type);
        mActor.Angle = 0.0f;
    }
}
