﻿using UnityEngine;

public class Fairy_Player_Idle : State<EStateType_Fairy_Player>
{
    private Fairy_Player mActor;
    public Fairy_Player_Idle(Fairy_Player actor) : base(EStateType_Fairy_Player.Idle)
    {
        mActor = actor;
    }

    public override void Start()
    {
        mActor.Animator.SetInteger("state", (int)Type);
    }

    public override void Update()
    {
        if(mActor.Input.DashDown)
        {
            mActor.State = EStateType_Fairy_Player.Dash;
            return;
        }

        mActor.EyeHolder.Angle = mActor.AimHolder.Angle;
    }

    public override void FixedUpdate()
    {
        Vector2 prevPosition = mActor.PrevPosition;
        Vector2 curPosition = mActor.transform.position;
        Vector2 direction = curPosition - prevPosition;

        // 정령의 움직임이 한계값을 넘는 경우 Move 상태로 전이
        if (Mathf.Abs(direction.x) > Fairy_Player.MOVEMENT_THRESHOLD
            || Mathf.Abs(direction.y) > Fairy_Player.MOVEMENT_THRESHOLD)
        {
            mActor.SetDirection(direction, false);
            mActor.State = EStateType_Fairy_Player.Move;
        }
    }
}
