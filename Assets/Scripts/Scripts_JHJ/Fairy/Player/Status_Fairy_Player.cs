﻿using UnityEngine;
using System.Collections.Generic;

public class Status_Fairy_Player : AStatus_Fairy
{
    public new Fairy_Player Actor { get; }

    /// <summary>
    /// 이름 태그
    /// </summary>
    public override string NameTag
    {
        get { return mDefault.NameTag; }
    }
    /// <summary>
    /// 플레이버 텍스트 태그
    /// </summary>
    public override string FlavorTag
    {
        get { return mDefault.FlavorTag; }
    }
    /// <summary>
    /// 플레이어 레벨
    /// </summary>
    public override int Level { get { return PlayerData.Level; } }

    /// <summary>
    /// 플레이어 경험치
    /// </summary>
    public int Exp
    {
        get { return PlayerData.Exp; }
        set { PlayerData.Exp = value; }
    }

    /// <summary>
    /// 플레이어 최대 경험치
    /// </summary>
    public int MaxExp { get { return PlayerData.MaxExp; } }

    private float mMana;
    /// <summary>
    /// 플레이어 마나
    /// </summary>
    public float Mana
    {
        get
        {
            float maxMana = MaxMana;
            if (mMana > maxMana)
            {
                mMana = maxMana;
            }
            return mMana;
        }
        set
        {
            float maxMana = MaxMana;

            if (value < 0.0f)
            {
                value = 0.0f;
            }
            else if (value > maxMana)
            {
                value = maxMana;
            }

            mMana = value;
        }
    }

    /// <summary>
    /// 기본 이동속도
    /// </summary>
    public override float MoveSpeed_Default
    {
        get { return mDefault.MoveSpeed; }
    }

    /// <summary>
    /// 어빌리티에 의한 이동속도 증가량
    /// </summary>
    public float MoveSpeed_Increase_Ability
    {
        get { return PlayerData.GetAbility(EFloatAbility.MoveSpeed_Fairy); }
    }
    /// <summary>
    /// 이동속도 증가량
    /// </summary>
    public override float MoveSpeed_Increase
    {
        get { return base.MoveSpeed_Increase + MoveSpeed_Increase_Ability; }
    }

    /// <summary>
    /// 어빌리티에 의한 시전속도 증가량
    /// </summary>
    public float CastSpeed_Increase_Ability
    {
        get { return PlayerData.GetAbility(EFloatAbility.CastSpeed_Fairy); }
    }
    /// <summary>
    /// 시정속도 증가량
    /// </summary>
    public override float CastSpeed_Increase
    {
        get { return base.CastSpeed_Increase + CastSpeed_Increase_Ability; }
    }

    /// <summary>
    /// 기본 최대 마나
    /// </summary>
    public float MaxMana_Default
    {
        get { return mDefault.MaxMana; }
    }
    /// <summary>
    /// 어빌리티에 의한 최대 마나 증가량
    /// </summary>
    public float MaxMana_Increment_Ability
    {
        get { return PlayerData.GetAbility(EFloatAbility.MaxMana); }
    }
    /// <summary>
    /// 최대 마나 증가량
    /// </summary>
    public float MaxMana_Increment
    {
        get { return MaxMana_Increment_Ability; }
    }
    /// <summary>
    /// 최대 마나 증가율
    /// </summary>
    public float MaxMana_Increase
    {
        get;
    }
    /// <summary>
    /// 최종 최대 마나
    /// </summary>
    public float MaxMana
    {
        get { return (MaxMana_Default + MaxMana_Increment) * (1.0f + MaxMana_Increase); }
    }

    /// <summary>
    /// 기본 마나 회복량
    /// </summary>
    public float ManaRps_Default
    {
        get { return mDefault.ManaRps; }
    }
    /// <summary>
    /// 어빌리티에 의한 마나 재생량 증가량
    /// </summary>
    public float ManaRps_Increment_Ability
    {
        get { return PlayerData.GetAbility(EFloatAbility.ManaRps); }
    }
    /// <summary>
    /// 마나 재생량 증가량
    /// </summary>
    public float ManaRps_Increment
    {
        get { return ManaRps_Increment_Ability; }
    }
    /// <summary>
    /// 마나 재생 증가율
    /// </summary>
    public float ManaRps_Increase
    {
        get;
    }
    /// <summary>
    /// 최종 마나 재생
    /// </summary>
    public float ManaRps
    {
        get { return (ManaRps_Default + ManaRps_Increment) * (1.0f + ManaRps_Increase); }
    }

    /// <summary>
    /// 기본 정화력
    /// </summary>
    public int Purification_Default
    {
        get { return mDefault.Purification; }
    }
    /// <summary>
    /// 어빌리티에 의한 정화력 증가량
    /// </summary>
    public int Purification_Increment_Ability
    {
        get { return PlayerData.GetAbility(EIntAbility.Purification); }
    }
    /// <summary>
    /// 정화력 증가량
    /// </summary>
    public int Purification_Increment
    {
        get { return Purification_Increment_Ability; }
    }
    /// <summary>
    /// 정화력 증가율
    /// </summary>
    public float Purification_Increase
    {
        get;
    }
    /// <summary>
    /// 최종 정화력
    /// </summary>
    public int Purification
    {
        get { return (int)((Purification_Default + Purification_Increment) * (1.0f + Purification_Increase)); }
    }

    /// <summary>
    /// 기본 공격력
    /// </summary>
    public override float Offense_Default
    {
        get { return (1.0f + (Purification * GameConstant.OFFENSE_PURIFICATION_CALIBRATION)); }
    }

    /// <summary>
    /// 기본 치명타 확률
    /// </summary>
    public override float CriticalChance_Default
    {
        get { return mDefault.CriticalChance; }
    }
    /// <summary>
    /// 어빌리티에 의한 치명타 확률 증가율
    /// </summary>
    public float CriticalChance_Increase_Ability
    {
        get { return PlayerData.GetAbility(EFloatAbility.CriticalChance_Fairy); }
    }
    /// <summary>
    /// 치명타 확률 증가율
    /// </summary>
    public override float CriticalChance_Increase
    {
        get { return base.CriticalChance_Increase + CriticalChance_Increase_Ability; }
    }

    /// <summary>
    /// 기본 치명타 배율
    /// </summary>
    public override float CriticalMag_Default
    {
        get { return mDefault.CriticalMag; }
    }
    public float CriticalMag_Increase_Ability
    {
        get { return PlayerData.GetAbility(EFloatAbility.CriticalMag_Fairy); }
    }
    /// <summary>
    /// 치명타 배율 증가율
    /// </summary>
    public override float CriticalMag_Increase
    {
        get { return base.CriticalMag_Increase + CriticalMag_Increase_Ability; }
    }

    /// <summary>
    /// 기본 힘
    /// </summary>
    public override float Force_Default
    {
        get { return mDefault.Force; }
    }

    /// <summary>
    /// 기본 속성 친화도
    /// </summary>
    /// <param name="e"></param>
    /// <returns></returns>
    public override int GetEA_Default(EElementalType e)
    {
        return mDefault.GetEA(e);
    }
    /// <summary>
    /// 어빌리티에 의한 속성 친화도 증가량
    /// </summary>
    /// <param name="e"></param>
    /// <returns></returns>
    public int GetEA_Increment_Ability(EElementalType e)
    {
        return PlayerData.GetEA_Fairy(e);
    }
    /// <summary>
    /// 속성 친화도 증가량
    /// </summary>
    /// <param name="e"></param>
    /// <returns></returns>
    public override int GetEA_Increment(EElementalType e)
    {
        return base.GetEA_Increment(e) + GetEA_Increment_Ability(e);
    }

    private DefaultStatus_Fairy_Player mDefault;

    public Status_Fairy_Player(Fairy_Player actor) : base(actor)
    {
        Actor = actor;
        mDefault = DataManager.GetDefaultStatus(actor.Type);
        Mana = MaxMana;
    }
}

public class DefaultStatus_Fairy_Player
{
    public string NameTag { get; }
    public string FlavorTag { get; }
    public float MoveSpeed { get; }
    public float MaxMana { get; }
    public float ManaRps { get; }
    public int Purification { get; }
    public float CriticalChance { get; }
    public float CriticalMag { get; }
    public float Force { get; }

    private Dictionary<EElementalType, int> mEaDict;

    public DefaultStatus_Fairy_Player(Dictionary<string, string> statusDict)
    {
        mEaDict = new Dictionary<EElementalType, int>();

        NameTag = statusDict["Name Tag"];
        FlavorTag = statusDict["Flavor Tag"];

        MoveSpeed = float.Parse(statusDict["Move Speed"]);
        MaxMana = float.Parse(statusDict["Max Mana"]);
        ManaRps = float.Parse(statusDict["Mana Rps"]);
        Purification = int.Parse(statusDict["Purification"]);
        CriticalChance = float.Parse(statusDict["Critical Chance"]);
        CriticalMag = float.Parse(statusDict["Critical Mag"]);
        Force = float.Parse(statusDict["Force"]);

        for(EElementalType e = EElementalType.None + 1; e < EElementalType.Count; e++)
        {
            mEaDict.Add(e, int.Parse(statusDict[e.ToString()]));
        }
    }

    public int GetEA(EElementalType e)
    {
        return mEaDict[e];
    }
}