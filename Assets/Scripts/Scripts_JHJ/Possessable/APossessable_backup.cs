﻿//using System.Collections;
//using System.Collections.Generic;
//using UnityEngine;

//public abstract class APossessable : MonoBehaviour
//{
//    public enum EPossessableState
//    {
//        None = -1, Normal, ACTIVATING, ACTIVE, DEACTIVATING, INACTIVE, Count
//    }

//    public class PossessableEvent
//    {
//        public event GameConstant.Action OnStay;
//        public event GameConstant.Action OnEnter;
//        public event GameConstant.Action OnExit;

//        public void Stay()
//        {
//            OnStay?.Invoke();
//        }
//        public void Enter()
//        {
//            OnEnter?.Invoke();
//        }
//        public void Exit()
//        {
//            OnExit?.Invoke();
//        }
//    }

//    public EPossessableState State
//    {
//        get
//        {
//            return mState;
//        }
//        set
//        {
//            if(mState != value)
//            {
//                mStateEvent[mState].Exit();
//                mState = value;
//                mStateEvent[mState].Enter();
//            }
//        }
//    }
//    public Animator Animator { get; private set; }
//    public Fairy_V2 Fairy { get; private set; }
//    public Collider2D Collider2D { get; private set; }

//    private Dictionary<EPossessableState, PossessableEvent> mStateEvent;
//    private EPossessableState mState = EPossessableState.None;

//    public void AddEnterEvent(EPossessableState state, GameConstant.Action action)
//    {
//        mStateEvent[state].OnEnter += action;
//    }
//    public void AddExitEvent(EPossessableState state, GameConstant.Action action)
//    {
//        mStateEvent[state].OnExit += action;
//    }
//    public void AddStayEvent(EPossessableState state, GameConstant.Action action)
//    {
//        mStateEvent[state].OnStay += action;
//    }
//    public void RemoveEnterEvent(EPossessableState state, GameConstant.Action action)
//    {
//        mStateEvent[state].OnEnter -= action;
//    }
//    public void RemoveExitEvent(EPossessableState state, GameConstant.Action action)
//    {
//        mStateEvent[state].OnExit -= action;
//    }
//    public void RemoveStayEvent(EPossessableState state, GameConstant.Action action)
//    {
//        mStateEvent[state].OnStay -= action;
//    }

//    public void Possess(Fairy_V2 spirit)
//    {
//        Fairy = spirit;
//        Fairy.gameObject.SetActive(false);
//        Collider2D.enabled = false;
//        Animator.SetTrigger("activate");
//        State = EPossessableState.ACTIVATING;
//    }

//    protected virtual void Awake()
//    {
//        Animator = GetComponent<Animator>();
//        Collider2D = GetComponent<Collider2D>();
//        mStateEvent = new Dictionary<EPossessableState, PossessableEvent>();
//        for(EPossessableState s = EPossessableState.None; s < EPossessableState.Count; s++)
//        {
//            mStateEvent.Add(s, new PossessableEvent());
//        }
//    }
//    protected virtual void Update()
//    {
//        mStateEvent[mState].Stay();
//    }
//}
