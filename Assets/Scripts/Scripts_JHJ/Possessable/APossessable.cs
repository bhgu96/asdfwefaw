﻿using System;


public abstract class APossessable<E> : StateBase<E> where E : Enum
{
    public abstract void Possess(IActor actor);
}
