﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum _EEffectType
{
    HEAL, Normal_HIT
}

public class Effect : MonoBehaviour
{
    public _EEffectType EffectType { get { return mEffectType; } }

    private Animator mAnimator;
    [SerializeField]
    private _EEffectType mEffectType;
    [SerializeField]
    private int mNumOfAnim;

    public void Invoke(Vector2 position)
    {
        transform.position = position;
        if (mNumOfAnim < 2)
        {
            mAnimator.SetTrigger("activate");
        }
        else
        {
            int indexOfAnim = (int)Random.Range(0.0f, mNumOfAnim);
            mAnimator.SetTrigger("activate_" + indexOfAnim);
        }
    }

    public void EndOfAction_Animation()
    {
        gameObject.SetActive(false);
    }

    private void Awake()
    {
        mAnimator = GetComponent<Animator>();
    }
}
