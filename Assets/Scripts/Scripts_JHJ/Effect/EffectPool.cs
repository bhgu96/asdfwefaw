﻿using System.Collections.Generic;
using UnityEngine;

public class _EffectPool : MonoBehaviour
{
    [SerializeField]
    private GameObject[] mEffectPrefabs;
    [SerializeField]
    private int mNumOfEffects;
    private static Dictionary<_EEffectType, Effect[]> m_EffectPools;
    private static Dictionary<_EEffectType, int> mIndexOfPools;

    public static void Invoke(_EEffectType effectType, Vector2 position)
    {
        int indexOfPool = mIndexOfPools[effectType];
        Effect[] effectPool = m_EffectPools[effectType];

        for(int i=0; i<effectPool.Length; i++)
        {
            if(!effectPool[indexOfPool].gameObject.activeInHierarchy)
            {
                effectPool[indexOfPool].gameObject.SetActive(true);
                effectPool[indexOfPool].Invoke(position);
                indexOfPool = (indexOfPool + 1 < effectPool.Length ? indexOfPool + 1 : 0);
                return;
            }
            indexOfPool = (indexOfPool + 1 < effectPool.Length ? indexOfPool + 1 : 0);
        }
    }

    private void Awake()
    {
        if (m_EffectPools == null)
        {
            DontDestroyOnLoad(this);
            m_EffectPools = new Dictionary<_EEffectType, Effect[]>();
            mIndexOfPools = new Dictionary<_EEffectType, int>();

            for(int i=0; i<mEffectPrefabs.Length; i++)
            {
                GameObject instObject = Instantiate(mEffectPrefabs[i], transform);
                instObject.SetActive(false);
                Effect effect = instObject.GetComponent<Effect>();
                Effect[] effectPool = new Effect[mNumOfEffects];
                effectPool[0] = effect;
                m_EffectPools.Add(effect.EffectType, effectPool);
                mIndexOfPools.Add(effect.EffectType, 0);
                for(int j=1; j<mNumOfEffects; j++)
                {
                    instObject = Instantiate(mEffectPrefabs[i], transform);
                    instObject.SetActive(false);
                    effect = instObject.GetComponent<Effect>();
                    effectPool[j] = effect;
                }
            }
        }
    }
}