﻿using UnityEngine;
using System.Collections;

public class WayPointDetector : MonoBehaviour
{
    public WayPoint CurWayPoint { get; private set; }

    [SerializeField]
    private EWayPointFlags mWayPointFlag;

    [SerializeField]
    private Transform mTransform;

    private void Update()
    {
        Detect();
    }

    private void Detect()
    {
        WayPoint curWayPoint = WayPointManager.GetWayPoint(mTransform.position);

        if (CurWayPoint != null)
        {
            CurWayPoint.RemoveTarget(mWayPointFlag, mTransform);
        }

        CurWayPoint = curWayPoint;
        if (CurWayPoint != null)
        {
            CurWayPoint.AddTarget(mWayPointFlag, mTransform);
        }
    }

    private void OnDisable()
    {
        if(CurWayPoint != null)
        {
            CurWayPoint.RemoveTarget(mWayPointFlag, mTransform);
        }
    }
}
