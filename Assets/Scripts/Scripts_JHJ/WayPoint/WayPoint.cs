﻿using UnityEngine;
using System.Collections.Generic;

public class WayPoint
{
    public Vector2Int Key { get; private set; }

    private Dictionary<EWayPointFlags, HashSet<Transform>> mTargetDict;

    private Transform[] mTargets;

    public WayPoint(Vector2Int key)
    {
        mTargetDict = new Dictionary<EWayPointFlags, HashSet<Transform>>();
        Key = key;
        mTargets = new Transform[20];
    }

    public void AddTarget(EWayPointFlags wayPointFlag, Transform targetTransform)
    {
        if(mTargetDict[wayPointFlag] == null)
        {
            mTargetDict.Add(wayPointFlag, new HashSet<Transform>());
        }

        mTargetDict[wayPointFlag].Add(targetTransform);
    }

    public void RemoveTarget(EWayPointFlags wayPointFlag, Transform targetTransform)
    {
        mTargetDict[wayPointFlag].Remove(targetTransform);
    }

    public Transform[] GetTargets(EWayPointFlags wayPointFlag)
    {
        HashSet<Transform> targetSet = mTargetDict[wayPointFlag];
        HashSet<Transform>.Enumerator enumerator = targetSet.GetEnumerator();

        int indexOfTargets = 0;
        while(indexOfTargets < mTargets.Length && enumerator.MoveNext())
        {
            mTargets[indexOfTargets++] = enumerator.Current;
        }

        if(indexOfTargets < mTargets.Length)
        {
            mTargets[indexOfTargets] = null;
        }

        return mTargets;
    }
}
