﻿using UnityEngine;
using System.Collections.Generic;

public class WayPointGenerator : MonoBehaviour
{
    [SerializeField]
    private ELayerFlags[] mObstacleLayers;
    private int mObstacleLayer;

    [SerializeField]
    private bool mIsDebugging;

    [SerializeField]
    private Transform mStartTransform;

    [SerializeField]
    private Transform mLeftDownTransform;
    [SerializeField]
    private Transform mRightUpTransform;

    private Vector2 mDetectionSize;

    public void Generate()
    {
        Vector2Int leftDownKey = WayPointManager.ConvertPositionToKey(mLeftDownTransform.position);
        Vector2Int rightUpKey = WayPointManager.ConvertPositionToKey(mRightUpTransform.position);
        Vector2Int startKey = WayPointManager.ConvertPositionToKey(mStartTransform.position);

        Queue<Vector2Int> keyQueue = new Queue<Vector2Int>();
        Vector2Int[] nextKeys = new Vector2Int[4];
        Vector2 nextPosition;

        Vector2Int curKey = startKey;

        WayPointManager.AddWayPoint(curKey);
        keyQueue.Enqueue(curKey);

        // BFS Algorithm
        while (keyQueue.Count != 0)
        {
            curKey = keyQueue.Dequeue();

            // Keys of Right, Up, Left, Down Side
            nextKeys[0] = new Vector2Int(curKey.x + 1, curKey.y);
            nextKeys[1] = new Vector2Int(curKey.x, curKey.y + 1);
            nextKeys[2] = new Vector2Int(curKey.x - 1, curKey.y);
            nextKeys[3] = new Vector2Int(curKey.x, curKey.y - 1);

            for (int i = 0; i < 4; i++)
            {
                // if nextKeys were not out of boundary
                if (((nextKeys[i].x >= leftDownKey.x && nextKeys[i].y >= leftDownKey.y) &&
                    (nextKeys[i].x <= rightUpKey.x && nextKeys[i].y <= rightUpKey.y)))
                {
                    nextPosition = WayPointManager.ConvertKeyToPosition(nextKeys[i]);
                    Collider2D obstacle = Physics2D.OverlapBox(nextPosition, mDetectionSize, 0.0f, mObstacleLayer);
                    if (obstacle == null && WayPointManager.AddWayPoint(nextKeys[i]))
                    {
                        keyQueue.Enqueue(nextKeys[i]);
                    }
                }
            }
        }
    }

    private void OnDrawGizmosSelected()
    {
        if (mLeftDownTransform != null && mRightUpTransform != null)
        {
            Gizmos.color = new Color(0.5f, 0.0f, 0.5f, 0.1f);

            Vector2 leftDownPosition = mLeftDownTransform.position;
            Vector2 rightUpPosition = mRightUpTransform.position;
            Gizmos.DrawCube(new Vector3((leftDownPosition.x + rightUpPosition.x) * 0.5f, (leftDownPosition.y + rightUpPosition.y) * 0.5f, 0.0f)
                , new Vector3(rightUpPosition.x - leftDownPosition.x, rightUpPosition.y - leftDownPosition.y, 0.0f));
        }

        if (!Application.isPlaying && mIsDebugging
            && mLeftDownTransform != null && mRightUpTransform != null && mStartTransform != null)
        {
            Gizmos.color = new Color(0.5f, 0.0f, 0.5f, 0.5f);

            Vector2 gapSize = WayPointManager.GapSize;
            mDetectionSize = new Vector2(gapSize.x * 0.9f, gapSize.y * 0.9f);

            Vector2Int leftDownKey = WayPointManager.ConvertPositionToKey(mLeftDownTransform.position);
            Vector2Int rightUpKey = WayPointManager.ConvertPositionToKey(mRightUpTransform.position);
            Vector2Int startKey = WayPointManager.ConvertPositionToKey(mStartTransform.position);

            WayPointManager.Clear();

            Queue<Vector2Int> keyQueue = new Queue<Vector2Int>();
            Vector2Int[] nextKeys = new Vector2Int[4];
            Vector2 nextPosition;

            Vector2Int curKey = startKey;
            Gizmos.DrawSphere(WayPointManager.ConvertKeyToPosition(curKey), 0.1f);

            WayPointManager.AddWayPoint(curKey);
            keyQueue.Enqueue(curKey);

            // BFS Algorithm
            while (keyQueue.Count != 0)
            {
                curKey = keyQueue.Dequeue();

                // Keys of Right, Up, Left, Down Side
                nextKeys[0] = new Vector2Int(curKey.x + 1, curKey.y);
                nextKeys[1] = new Vector2Int(curKey.x, curKey.y + 1);
                nextKeys[2] = new Vector2Int(curKey.x - 1, curKey.y);
                nextKeys[3] = new Vector2Int(curKey.x, curKey.y - 1);

                for (int i = 0; i < 4; i++)
                {
                    // if nextKeys were not out of boundary
                    if (((nextKeys[i].x >= leftDownKey.x && nextKeys[i].y >= leftDownKey.y) &&
                        (nextKeys[i].x <= rightUpKey.x && nextKeys[i].y <= rightUpKey.y)))
                    {
                        nextPosition = WayPointManager.ConvertKeyToPosition(nextKeys[i]);
                        Collider2D obstacle = Physics2D.OverlapBox(nextPosition, mDetectionSize, 0.0f, mObstacleLayer);
                        if (obstacle == null)
                        {
                            Gizmos.DrawSphere(nextPosition, 0.1f);

                            if (WayPointManager.AddWayPoint(nextKeys[i]))
                            {
                                keyQueue.Enqueue(nextKeys[i]);
                            }
                        }
                    }
                }
            }

        }
    }

    private void Awake()
    {
        for (int i = 0; i < mObstacleLayers.Length; i++)
        {
            mObstacleLayer = mObstacleLayer | 1 << (int)mObstacleLayers[i];
        }

        Vector2 gapSize = WayPointManager.GapSize;
        mDetectionSize = new Vector2(gapSize.x * 0.9f, gapSize.y * 0.9f);
    }

    private void Start()
    {
        Generate();
    }
}
