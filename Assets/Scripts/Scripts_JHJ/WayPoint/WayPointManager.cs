﻿using UnityEngine;
using System.Collections.Generic;
using UnityEngine.SceneManagement;

public enum EWayPointFlags
{
    None = -1, 

    Fairy, 
    Giant, 
    NormalMonster, 

    Count
}

public static class WayPointManager
{
    public static Vector2 GapSize { get; } = new Vector2(1.0f, 1.0f);

    private static Dictionary<Vector2Int, WayPoint> mWayPointDict;

    static WayPointManager()
    {
        mWayPointDict = new Dictionary<Vector2Int, WayPoint>();
        SceneManager.activeSceneChanged += SceneManager_activeSceneChanged;
    }

    private static void SceneManager_activeSceneChanged(Scene arg0, Scene arg1)
    {
        Clear();
    }

    public static void Clear()
    {
        mWayPointDict.Clear();
    }

    public static Vector2Int ConvertPositionToKey(Vector2 position)
    {
        return new Vector2Int((int)(position.x / GapSize.x), (int)(position.y / GapSize.y));
    }

    public static Vector2 ConvertKeyToPosition(Vector2Int key)
    {
        return new Vector2(key.x * GapSize.x + GapSize.x * 0.5f, key.y * GapSize.y + GapSize.y * 0.5f);
    }

    public static bool AddWayPoint(Vector2Int key)
    {
        if(!mWayPointDict.ContainsKey(key))
        {
            mWayPointDict.Add(key, new WayPoint(key));
            return true;
        }
        return false;
    }

    public static WayPoint GetWayPoint(Vector2 position)
    {
        Vector2Int key = ConvertPositionToKey(position);

        if (mWayPointDict.ContainsKey(key))
        {
            return mWayPointDict[key];
        }
        return null;
    }

    public static WayPoint GetWayPoint(Vector2Int key)
    {
        if (mWayPointDict.ContainsKey(key))
        {
            return mWayPointDict[key];
        }
        return null;
    }
}