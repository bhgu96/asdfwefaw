﻿//using System.Collections;
//using System.Collections.Generic;
//using UnityEngine;
//using UnityEngine.UI;

//public class ActorEffectInfo : MonoBehaviour
//{
//    [System.Serializable]
//    private class ActorEffectIcon
//    {
//        public EActorEffectFlags ActorEffectFlag;
//        public Sprite Icon;
//    }

//    private IActor mActor;
//    [SerializeField]
//    private ActorEffectIcon[] mActorEffectIcons;

//    private Dictionary<EActorEffectFlags, Sprite> mActorEffectIconDict;
//    [SerializeField]
//    private Image[] mImages;

//    private void Awake()
//    {
//        mActorEffectIconDict = new Dictionary<EActorEffectFlags, Sprite>();
//        mActor = GetComponentInParent<IActor>();
//        for(int i=0; i<mActorEffectIcons.Length; i++)
//        {
//            mActorEffectIconDict.Add(mActorEffectIcons[i].ActorEffectFlag, mActorEffectIcons[i].Icon);
//        }
//        for(int i=0; i<mImages.Length; i++)
//        {
//            mImages[i].gameObject.SetActive(false);
//        }
//    }

//    private void Update()
//    {
//        if(transform.lossyScale.x < 0.0f)
//        {
//            Vector3 localScale = transform.localScale;
//            transform.localScale = new Vector3(localScale.x * -1.0f, localScale.y, localScale.z);
//        }

//        HashSet<EActorEffectFlags> effectSet = mActor.State.ActorEffectSet;
//        int indexOfImages = 0;

//        for(EActorEffectFlags e = EActorEffectFlags.None + 1; e < EActorEffectFlags.Count; e++)
//        {
//            if(effectSet.Contains(e))
//            {
//                if (!mImages[indexOfImages].gameObject.activeInHierarchy)
//                {
//                    mImages[indexOfImages].gameObject.SetActive(true);
//                }
//                mImages[indexOfImages++].sprite = mActorEffectIconDict[e];
//            }
//        }

//        for(; indexOfImages < mImages.Length; indexOfImages++)
//        {
//            if(mImages[indexOfImages].gameObject.activeInHierarchy)
//            {
//                mImages[indexOfImages].gameObject.SetActive(false);
//            }
//        }
//    }
//}
