﻿//using System.Collections;
//using System.Collections.Generic;
//using UnityEngine;
//using UnityEngine.UI;

//public class MonsterHealthPointBar : MonoBehaviour
//{
//    private enum EHPBarState
//    {
//        INACTIVE, ACTIVE
//    }

//    private enum EMainHPBarState
//    {
//        DECREASING, DECREASED
//    }

//    private enum ESubHPBarState
//    {
//        WAIT_DECREASE, DECREASING, DECREASED
//    }

//    private IActor mActor;

//    private SpriteRenderer mFrame;
//    private float mFrameWidth;
//    [SerializeField]
//    private SpriteRenderer mSubContent;
//    private float mSubWidth;
//    [SerializeField]
//    private SpriteRenderer mMainContent;
//    private float mMainWidth;

//    private int mLastHp;
//    private float mMainTimeTaken;
//    private float mSubTimeTaken;
//    private float m_srcSubAmount;
//    private float m_dstSubAmount;
//    private const float MAIN_CHANGE_TIME = 0.5f;
//    private const float SUB_WAIT_TIME = 0.5f;
//    private const float SUB_CHANGE_TIME = 0.3f;
//    private const float ACTIVE_TIME = 1.0F;
//    private const float INACTIVE_TIME = 1.0F;

//    private EHPBarState mState = EHPBarState.INACTIVE;
//    private EMainHPBarState mMainState = EMainHPBarState.DECREASED;
//    private ESubHPBarState mSubState = ESubHPBarState.DECREASED;

//    private void Awake()
//    {
//        mActor = GetComponentInParent<IActor>();
//        mFrame = GetComponent<SpriteRenderer>();
//    }
//    private void Start()
//    {
//        mFrameWidth = mFrame.size.x;
//        mMainWidth = mMainContent.size.x;
//        mSubWidth = mSubContent.size.x;
//        mFrame.color = new Color(1.0f, 1.0f, 1.0f, 0.0f);
//        mSubContent.color = new Color(1.0f, 1.0f, 1.0f, 0.0f);
//        mMainContent.color = new Color(1.0f, 1.0f, 1.0f, 0.0f);
//        mLastHp = mActor.Status.CurrentHealthPoint;
//    }

//    private void Update()
//    {

//        // Bush Stack Debuff 에 걸렸을 경우에 대한 예외 처리
//        BushStackDebuff debuff = mActor.State.DebuffDict[EDebuffFlags.BUSH_BOUND] as BushStackDebuff;
//        if(debuff != null && debuff.IsFullLevel)
//        {
//            if(mState != EHPBarState.INACTIVE)
//            {
//                mFrame.color = new Color(1.0f, 1.0f, 1.0f, 0.0f);
//                mSubContent.color = new Color(1.0f, 1.0f, 1.0f, 0.0f);
//                mMainContent.color = new Color(1.0f, 1.0f, 1.0f, 0.0f);
//                mState = EHPBarState.INACTIVE;
//            }

//            if (debuff.IsBurning)
//            {
//                mLastHp = 0;
//            }
//            return;
//        }

//        if(transform.lossyScale.x < 0.0f)
//        {
//            transform.localScale = new Vector3(transform.localScale.x * -1.0f, 1.0f, 1.0f);
//        }

//        switch (mState)
//        {
//            // 비활성화 상태
//            case EHPBarState.INACTIVE:
//                if (mActor.Status.CurrentHealthPoint != mLastHp)
//                {
//                    mState = EHPBarState.ACTIVE;
//                    mMainState = EMainHPBarState.DECREASING;
//                    mSubState = ESubHPBarState.WAIT_DECREASE;
//                    mFrame.color = new Color(1.0f, 1.0f, 1.0f, 1.0f);
//                    mSubContent.color = new Color(1.0f, 1.0f, 1.0f, 1.0f);
//                    mMainContent.color = new Color(1.0f, 1.0f, 1.0f, 1.0f);
//                    mSubTimeTaken = 0.0f;
//                    mMainTimeTaken = 0.0f;
//                }
//                break;
//            // 활성화 상태
//            case EHPBarState.ACTIVE:
//                // 메인 HP 바가 깎이는 상태
//                if (mMainState == EMainHPBarState.DECREASING)
//                {
//                    if (mMainTimeTaken < MAIN_CHANGE_TIME)
//                    {
//                        mMainTimeTaken += Time.deltaTime;
//                        mMainContent.size = new Vector2(Mathf.MoveTowards((float)mLastHp / mActor.Status.MaximumHealthPoint,
//                            (float)mActor.Status.CurrentHealthPoint / mActor.Status.MaximumHealthPoint, mMainTimeTaken / MAIN_CHANGE_TIME) * mMainWidth, mMainContent.size.y);
//                    }
//                    else
//                    {
//                        mMainContent.size = new Vector2((float)mActor.Status.CurrentHealthPoint / mActor.Status.MaximumHealthPoint * mMainWidth, mMainContent.size.y);
//                        mLastHp = mActor.Status.CurrentHealthPoint;
//                        mMainState = EMainHPBarState.DECREASED;
//                    }
//                } // 메인 HP 바가 다 깎인 상태
//                else if (mLastHp != mActor.Status.CurrentHealthPoint)
//                {
//                    mMainTimeTaken = 0.0f;
//                    mMainState = EMainHPBarState.DECREASING;
//                }

//                switch (mSubState)
//                {
//                    // 보조 HP 바가 깎이는걸 기다리는 상태
//                    case ESubHPBarState.WAIT_DECREASE:
//                        if (mSubTimeTaken < SUB_WAIT_TIME)
//                        {
//                            mSubTimeTaken += Time.deltaTime;
//                        }
//                        else
//                        {
//                            mSubTimeTaken = 0.0f;
//                            mSubState = ESubHPBarState.DECREASING;
//                            m_srcSubAmount = mSubContent.size.x;
//                            //m_dstSubAmount = (float)mLastHp / mActor.Status.MaximumHealthPoint;
//                        }
//                        break;
//                    // 보조 HP 바가 깎이는 상태
//                    case ESubHPBarState.DECREASING:
//                        if (mSubTimeTaken < SUB_CHANGE_TIME)
//                        {
//                            mSubTimeTaken += Time.deltaTime;
//                            mSubContent.size = new Vector2(Mathf.Lerp(m_srcSubAmount, mMainContent.size.x, mSubTimeTaken / SUB_CHANGE_TIME), mSubContent.size.y);
//                        }
//                        else
//                        {
//                            mSubState = ESubHPBarState.DECREASED;
//                            mSubContent.size = mMainContent.size;
//                            mSubTimeTaken = 0.0f;
//                        }
//                        break;
//                    // 보조 HP 바가 다 깎인  상태
//                    case ESubHPBarState.DECREASED:
//                        // 보조 HP 바와 메인 HP 바의 수치가 같지 않다면,
//                        if (!Mathf.Approximately(mSubContent.size.x, mMainContent.size.x))
//                        {
//                            mFrame.color = new Color(1.0f, 1.0f, 1.0f, 1.0f);
//                            mSubContent.color = new Color(1.0f, 1.0f, 1.0f, 1.0f);
//                            mMainContent.color = new Color(1.0f, 1.0f, 1.0f, 1.0f);
//                            mSubTimeTaken = 0.0f;
//                            mSubState = ESubHPBarState.WAIT_DECREASE;
//                        } // HP 바가 변하지 않을때 보여주는 시간
//                        else if (mSubTimeTaken < ACTIVE_TIME)
//                        {
//                            mSubTimeTaken += Time.deltaTime;
//                        } // HP 바가 사라지는 중
//                        else if (mSubTimeTaken < ACTIVE_TIME + INACTIVE_TIME)
//                        {
//                            mSubTimeTaken += Time.deltaTime;
//                            float deltaTime = (mSubTimeTaken - ACTIVE_TIME) / INACTIVE_TIME;
//                            mFrame.color = new Color(1.0f, 1.0f, 1.0f, Mathf.MoveTowards(1.0f, 0.0f, deltaTime));
//                            mSubContent.color = new Color(1.0f, 1.0f, 1.0f, 0.0f);
//                            mMainContent.color = new Color(1.0f, 1.0f, 1.0f, Mathf.MoveTowards(1.0f, 0.0f, deltaTime));
//                        } // HP 바가 비활성화 되는 중
//                        else
//                        {
//                            mFrame.color = new Color(1.0f, 1.0f, 1.0f, 0.0f);
//                            mSubContent.color = new Color(1.0f, 1.0f, 1.0f, 0.0f);
//                            mMainContent.color = new Color(1.0f, 1.0f, 1.0f, 0.0f);
//                            mState = EHPBarState.INACTIVE;
//                        }
//                        break;
//                }
//                break;
//        }
//    }


//}
