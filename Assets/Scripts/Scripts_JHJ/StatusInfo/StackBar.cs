﻿//using System.Collections;
//using System.Collections.Generic;
//using UnityEngine;

//public class StackBar : MonoBehaviour
//{
//    private enum EState
//    {
//        INACTIVE, ACTIVE, DEACTIVATE
//    }

//    private Animator mAnimator;
//    private SpriteRenderer mSpriteRenderer;
//    private IActor mActor;
//    private EState mState;
//    private float mRemainTime;
//    private const float DISAPPEAR_TIME = 1.0F;
//    private int mPrevLevel;
//    private bool mIsBurning;

//    private bool IsEnabled { get { return mSpriteRenderer.enabled; } set { mSpriteRenderer.enabled = value; } }

//    private void Awake()
//    {
//        mActor = GetComponentInParent<IActor>();
//        mAnimator = GetComponent<Animator>();
//        mSpriteRenderer = GetComponent<SpriteRenderer>();
//        mSpriteRenderer.color = new Color(1.0f, 1.0f, 1.0f, 0.0f);
//        IsEnabled = false;
//        mState = EState.INACTIVE;
//        mAnimator.SetInteger("size", (int)mActor.ActorSize);
//    }
//    private void OnEnable()
//    {
//        mAnimator.SetInteger("size", (int)mActor.ActorSize);
//    }

//    private void Update()
//    {
//        if(transform.lossyScale.x < 0.0f)
//        {
//            transform.localScale = new Vector3(transform.localScale.x * -1.0f, 1.0f, 1.0f);
//        }

//        BushStackDebuff debuff = mActor.State.DebuffDict[EDebuffFlags.BUSH_BOUND] as BushStackDebuff;

//        switch (mState)
//        {
//            case EState.INACTIVE:
//                if(debuff != null)
//                {
//                    mState = EState.ACTIVE;
//                    IsEnabled = true;
//                    mSpriteRenderer.color = new Color(1.0f, 1.0f, 1.0f, 1.0f);
//                    mPrevLevel = 0;
//                    mIsBurning = false;
//                }
//                break;
//            case EState.ACTIVE:
//                if(debuff == null)
//                {
//                    mState = EState.DEACTIVATE;
//                    mRemainTime = DISAPPEAR_TIME;
//                }
//                else
//                {
//                    if(mPrevLevel != debuff.Level)
//                    {
//                        mPrevLevel = debuff.Level;
//                        mAnimator.SetInteger("level", mPrevLevel);
//                    }

//                    if(mIsBurning != debuff.IsBurning)
//                    {
//                        mIsBurning = debuff.IsBurning;
//                        mAnimator.SetBool("burn", mIsBurning);
//                    }
//                }
//                break;
//            case EState.DEACTIVATE:
//                if(debuff != null)
//                {
//                    mSpriteRenderer.color = new Color(1.0f, 1.0f, 1.0f, 1.0f);
//                    mState = EState.ACTIVE;
//                }
//                else if(mRemainTime > 0.0f)
//                {
//                    mRemainTime -= Time.deltaTime;
//                    mSpriteRenderer.color = new Color(1.0f, 1.0f, 1.0f, Mathf.Lerp(0.0f, 1.0f, mRemainTime / DISAPPEAR_TIME));
//                }
//                else
//                {
//                    IsEnabled = false;
//                    mState = EState.INACTIVE;
//                }
//                break;
//        }
//    }
//}
