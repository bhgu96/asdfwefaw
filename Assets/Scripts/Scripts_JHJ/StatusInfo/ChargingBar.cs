﻿//using System.Collections;
//using System.Collections.Generic;
//using UnityEngine;

//public class ChargingBar : MonoBehaviour
//{
//    [SerializeField]
//    private SpriteRenderer mMainContent;
//    [SerializeField]
//    private float mMaximumWidth;

//    private float mWaitTime;
//    private float mDisableTime;

//    private const float WAIT_TIME = 0.5f;
//    private const float DISABLE_TIME = 0.5F;
//    private bool mIsActive;
//    private bool mIsWait;

//    private SpriteRenderer mFrameRenderer;

//    public void Set(float fillAmount)
//    {
//        mMainContent.size = new Vector2(fillAmount * mMaximumWidth, mMainContent.size.y);
//        mIsActive = true;
//        mIsWait = true;
//        mWaitTime = WAIT_TIME;
//        mDisableTime = DISABLE_TIME;
//        Color color = mMainContent.color;
//        mMainContent.color = new Color(color.r, color.g, color.b, 1.0f);
//        color = mFrameRenderer.color;
//        mFrameRenderer.color = new Color(color.r, color.g, color.b, 1.0f);
//        if (transform.eulerAngles.y > 0.0f)
//        {
//            transform.eulerAngles = new Vector3(0.0f, 0.0f, 0.0f);
//        }
//    }

//    private void Awake()
//    {
//        mFrameRenderer = GetComponent<SpriteRenderer>();
//        Color color = mMainContent.color;
//        mMainContent.color = new Color(color.r, color.g, color.b, 0.0f);
//        color = mFrameRenderer.color;
//        mFrameRenderer.color = new Color(color.r, color.g, color.b, 0.0f);
//    }

//    private void Update()
//    {
//        if(mIsWait)
//        {
//            if (transform.eulerAngles.y != 0.0f)
//            {
//                transform.eulerAngles = new Vector3(0.0f, 0.0f, 0.0f);
//            }
//            if (mWaitTime > 0.0f)
//            {
//                mWaitTime -= Time.deltaTime;
//            }
//            else
//            {
//                mIsWait = false;
//            }
//        }
//        else if(mIsActive)
//        {
//            if (transform.eulerAngles.y != 0.0f)
//            {
//                transform.eulerAngles = new Vector3(0.0f, 0.0f, 0.0f);
//            }
//            if (mDisableTime > 0.0f)
//            {
//                mDisableTime -= Time.deltaTime;
//                Color color = mMainContent.color;
//                mMainContent.color = new Color(color.r, color.g, color.b, Mathf.Lerp(0.0f, 1.0f, mDisableTime / DISABLE_TIME));
//                color = mFrameRenderer.color;
//                mFrameRenderer.color = new Color(color.r, color.g, color.b, Mathf.Lerp(0.0f, 1.0f, mDisableTime / DISABLE_TIME));

//            }
//            else
//            {
//                mIsActive = false;
//                Color color = mMainContent.color;
//                mMainContent.color = new Color(color.r, color.g, color.b, 0.0F);
//                color = mFrameRenderer.color;
//                mFrameRenderer.color = new Color(color.r, color.g, color.b, 0.0f);
//            }
//        }
//    }
//}
