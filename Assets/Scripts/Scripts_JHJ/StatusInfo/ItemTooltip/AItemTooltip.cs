﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public abstract class AItemTooltip : MonoBehaviour
{
    private enum ETooltipState
    {
        INACTIVE, ACTIVATING, ACTIVE, DEACTIVATING
    }

    [System.Serializable]
    private class TextInfo
    {
        public Text Text;
        public string Key;
    }

    [SerializeField]
    private float mMinHeightSize = 500f;
    private float mAdditionalHeightSize;
    private float mDstHeightSize;
    private float mSrcHeightSize;
    [SerializeField]
    private float mWidthSize = 1000f;

    [SerializeField]
    private RectTransform mPanel;

    [SerializeField]
    private TextInfo[] mTextInfo;

    private ETooltipState mState;

    [SerializeField]
    private float mPopUpTime = 0.2f;
    private float mRemainPopUpTime;

    public void PopUp()
    {
        gameObject.SetActive(true);
        mState = ETooltipState.ACTIVATING;
        mRemainPopUpTime = mPopUpTime;
        mPanel.gameObject.SetActive(true);

        mSrcHeightSize = mPanel.sizeDelta.y;

        mAdditionalHeightSize = 0.0f;
        for (int i = 0; i < mTextInfo.Length; i++)
        {
            string script = GetFormattedScript(mTextInfo[i].Key);
            mTextInfo[i].Text.text = script;
            int countOfLines = (int)(script.Length * 0.5f / (mTextInfo[i].Text.rectTransform.sizeDelta.x / mTextInfo[i].Text.fontSize));
            mAdditionalHeightSize += countOfLines * mTextInfo[i].Text.fontSize;
        }

        mDstHeightSize = mMinHeightSize + mAdditionalHeightSize;
    }

    public void PopDown()
    {
        mState = ETooltipState.DEACTIVATING;
        mRemainPopUpTime = mPopUpTime;
        mSrcHeightSize = mPanel.sizeDelta.y;
        for (int i = 0; i < mTextInfo.Length; i++)
        {
            mTextInfo[i].Text.gameObject.SetActive(false);
        }
    }

    protected abstract string GetFormattedScript(string key);

    protected virtual void Awake()
    {
        mState = ETooltipState.INACTIVE;
        mPanel.sizeDelta = new Vector2(mWidthSize, 0.0f);
        mPanel.gameObject.SetActive(false);
        gameObject.SetActive(false);
        for(int i=0; i<mTextInfo.Length; i++)
        {
            mTextInfo[i].Text.gameObject.SetActive(false);
        }
    }

    private void Update()
    {
        switch (mState)
        {
            case ETooltipState.ACTIVATING:
                if (mRemainPopUpTime > 0.0f)
                {
                    mRemainPopUpTime -= Time.deltaTime;
                    mPanel.sizeDelta = new Vector2(mWidthSize, Mathf.Lerp(mDstHeightSize, mSrcHeightSize, mRemainPopUpTime / mPopUpTime));
                }
                else
                {
                    mPanel.sizeDelta = new Vector2(mWidthSize, mDstHeightSize);
                    mState = ETooltipState.ACTIVE;
                    for(int i=0; i<mTextInfo.Length; i++)
                    {
                        mTextInfo[i].Text.gameObject.SetActive(true);
                    }
                }
                break;
            case ETooltipState.DEACTIVATING:
                if (mRemainPopUpTime > 0.0f)
                {
                    mRemainPopUpTime -= Time.deltaTime;
                    mPanel.sizeDelta = new Vector2(mWidthSize, Mathf.Lerp(0.0f, mSrcHeightSize, mRemainPopUpTime / mPopUpTime));
                }
                else
                {
                    mPanel.sizeDelta = new Vector2(mWidthSize, 0.0f);
                    mState = ETooltipState.INACTIVE;
                    mPanel.gameObject.SetActive(false);
                }
                break;
        }
    }
}
