﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RandomAnimationSetter : MonoBehaviour
{
    private Animator mAnimator;

    [SerializeField]
    private int mCountOfAnim;


    private void Awake()
    {
        mAnimator = GetComponent<Animator>();
        mAnimator.SetInteger("type", (int)Random.Range(0.0f, mCountOfAnim));
    }
}
