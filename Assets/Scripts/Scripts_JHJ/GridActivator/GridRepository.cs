﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GridSpace
{
    public HashSet<GridRegister> RegisterSet { get; } = new HashSet<GridRegister>();
    public HashSet<GridActivator> ActivatorSet { get; } = new HashSet<GridActivator>();
}

public static class GridRepository
{
    public static Vector2 GridSize { get; } = new Vector2(50.0f, 50.0f);
    private static Dictionary<Vector2Int, GridSpace> mRepository;

    public static void Register(GridRegister register, Vector2Int key)
    {
        if (!mRepository.ContainsKey(key))
        {
            mRepository.Add(key, new GridSpace());
        }
        GridSpace gridSpace = mRepository[key];
        if (gridSpace.ActivatorSet.Count == 0)
        {
            gridSpace.RegisterSet.Add(register);
            register.gameObject.SetActive(false);
        }
    }

    public static void Activate(GridActivator activator, Vector2Int key)
    {
        if (!mRepository.ContainsKey(key))
        {
            GridSpace newGrid = new GridSpace();
            newGrid.ActivatorSet.Add(activator);
            mRepository.Add(key, newGrid);
            return;
        }
        GridSpace grid = mRepository[key];
        if (grid.ActivatorSet.Count == 0)
        {
            HashSet<GridRegister>.Enumerator enumerator = grid.RegisterSet.GetEnumerator();
            while(enumerator.MoveNext())
            {
                enumerator.Current.gameObject.SetActive(true);
            }
            grid.RegisterSet.Clear();
        }
        grid.ActivatorSet.Add(activator);
    }

    public static void Deactivate(GridActivator activator, Vector2Int key)
    {
        if (!mRepository.ContainsKey(key))
        {
            mRepository.Add(key, new GridSpace());
            return;
        }

        GridSpace grid = mRepository[key];
        grid.ActivatorSet.Remove(activator);
    }

    public static void Clear()
    {
        mRepository.Clear();
    }

    static GridRepository()
    {
        mRepository = new Dictionary<Vector2Int, GridSpace>();
        SceneManager.activeSceneChanged += SceneManager_activeSceneChanged;
    }

    private static void SceneManager_activeSceneChanged(Scene arg0, Scene arg1)
    {
        mRepository.Clear();
    }
}
