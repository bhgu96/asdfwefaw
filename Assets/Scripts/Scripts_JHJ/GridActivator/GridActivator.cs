﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GridActivator : MonoBehaviour
{
    private Vector2Int mPrevKey;
    private Vector2Int[] mAdjacentKeys
        = new Vector2Int[]
        {
            new Vector2Int(0, 0), 
            new Vector2Int(0, 1), new Vector2Int(0, -1),
            new Vector2Int(-1, 0), new Vector2Int(1, 0),
            new Vector2Int(-1, 1), new Vector2Int(-1, -1),
            new Vector2Int(1, 1), new Vector2Int(1, -1)
        };

    private List<Vector2Int> mAdjacentList_0;
    private List<Vector2Int> mAdjacentList_1;
    private int mActiveList;

    private void Start()
    {
        mAdjacentList_0 = new List<Vector2Int>();
        mAdjacentList_1 = new List<Vector2Int>();
        mActiveList = 0;
        Vector2 curPosition = transform.position;
        Vector2 gridSize = GridRepository.GridSize;
        mPrevKey = new Vector2Int(Mathf.FloorToInt(curPosition.x / gridSize.x), Mathf.FloorToInt(curPosition.y / gridSize.y));
        for(int i=0; i<mAdjacentKeys.Length; i++)
        {
            Vector2Int adjacentKey = new Vector2Int(mPrevKey.x + mAdjacentKeys[i].x, mPrevKey.y + mAdjacentKeys[i].y);
            mAdjacentList_0.Add(adjacentKey);
            GridRepository.Activate(this, adjacentKey);
        }
    }

    private void Update()
    {
        Vector2 curPosition = transform.position;
        Vector2 gridSize = GridRepository.GridSize;
        Vector2Int key = new Vector2Int(Mathf.FloorToInt(curPosition.x / gridSize.x), Mathf.FloorToInt(curPosition.y / gridSize.y));

        if (mPrevKey != key)
        {
            if (mActiveList == 0)
            {
                for (int i = 0; i < mAdjacentKeys.Length; i++)
                {
                    Vector2Int adjacentKey = new Vector2Int(key.x + mAdjacentKeys[i].x, key.y + mAdjacentKeys[i].y);
                    mAdjacentList_1.Add(adjacentKey);
                    if(!mAdjacentList_0.Remove(adjacentKey))
                    {
                        GridRepository.Activate(this, adjacentKey);
                    }
                }

                for(int i=0; i<mAdjacentList_0.Count; i++)
                {
                    GridRepository.Deactivate(this, mAdjacentList_0[i]);
                }
                mAdjacentList_0.Clear();
                mActiveList = 1;
            }
            else if(mActiveList == 1)
            {
                for (int i = 0; i < mAdjacentKeys.Length; i++)
                {
                    Vector2Int adjacentKey = new Vector2Int(key.x + mAdjacentKeys[i].x, key.y + mAdjacentKeys[i].y);
                    mAdjacentList_0.Add(adjacentKey);
                    if (!mAdjacentList_1.Remove(adjacentKey))
                    {
                        GridRepository.Activate(this, adjacentKey);
                    }
                }

                for (int i = 0; i < mAdjacentList_1.Count; i++)
                {
                    GridRepository.Deactivate(this, mAdjacentList_1[i]);
                }
                mAdjacentList_1.Clear();
                mActiveList = 0;
            }

            mPrevKey = key;
        }
    }
}
