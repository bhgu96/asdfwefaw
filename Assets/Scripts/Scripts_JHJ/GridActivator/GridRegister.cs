﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GridRegister : MonoBehaviour
{
    private void Start()
    {
        Vector2 curPosition = transform.position;
        Vector2 gridSize = GridRepository.GridSize;
        Vector2Int key = new Vector2Int(Mathf.FloorToInt(curPosition.x / gridSize.x), Mathf.FloorToInt(curPosition.y / gridSize.y));
        GridRepository.Register(this, key);
    }

    private void Update()
    {
        Vector2 curPosition = transform.position;
        Vector2 gridSize = GridRepository.GridSize;
        Vector2Int key = new Vector2Int(Mathf.FloorToInt(curPosition.x / gridSize.x), Mathf.FloorToInt(curPosition.y / gridSize.y));
        GridRepository.Register(this, key);
    }
}
