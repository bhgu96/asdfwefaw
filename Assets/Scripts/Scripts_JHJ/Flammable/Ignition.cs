﻿//using UnityEngine;
//using System.Collections.Generic;

//public class Ignition : AProjectile
//{
//    public Transform MaxTransform { get { return mMaxTransform; } }
//    public Transform MinTransform { get { return mMinTransform; } }
//    public MinMax MinMaxRange { get { return mMinMaxRange; } }
//    public EDirectionFlags Direction { get { return mDirection; } }

//    private MinMax mMinMaxRange;

//    [SerializeField]
//    private float mMaskThickness;
//    [SerializeField]
//    private Transform mMinTransform;
//    [SerializeField]
//    private Transform mMaxTransform;

//    [SerializeField]
//    private GameObject mMinAnimation;
//    [SerializeField]
//    private GameObject mMaxAnimation;

//    private float mRemainTimeOfMin;
//    private float mRemainTimeOfMax;

//    private const float Ending_TIME = 0.5F;

//    private SpriteMask mMask;
//    private EDirectionFlags mDirection;

//    private List<Flammable> mFlammableList;

//    [SerializeField]
//    private float mSpeed;

//    public override bool CheckCondition()
//    {
//        if (mFlammableList.Count > 0)
//        {
//            //MinMax range = new MinMax();
//            MinMax range = mFlammableList[0].GetRange(this);

//            Vector2 minPosition = mMinTransform.position;
//            Vector2 maxPosition = mMaxTransform.position;

//            switch (mDirection)
//            {
//                case EDirectionFlags.Horizontal:

//                    for (int i = 1; i < mFlammableList.Count; i++)
//                    {
//                        MinMax nextRange = mFlammableList[i].GetRange(this);

//                        if (range.MinPosition.x > nextRange.MinPosition.x)
//                        {
//                            range.MinPosition = nextRange.MinPosition;
//                        }

//                        if (range.MaxPosition.x < nextRange.MaxPosition.x)
//                        {
//                            range.MaxPosition = nextRange.MaxPosition;
//                        }
//                    }

//                    if (mMinTransform.position.x <= range.MinPosition.x && mMaxTransform.position.x >= range.MaxPosition.x)
//                    {
//                        return false;
//                    }
//                    mMinMaxRange = range;
//                    break;
//                case EDirectionFlags.Vertical:

//                    for (int i = 1; i < mFlammableList.Count; i++)
//                    {
//                        MinMax nextRange = mFlammableList[i].GetRange(this);

//                        if (range.MinPosition.y > nextRange.MinPosition.y)
//                        {
//                            range.MinPosition = nextRange.MinPosition;
//                        }

//                        if (range.MaxPosition.y < nextRange.MaxPosition.y)
//                        {
//                            range.MaxPosition = nextRange.MaxPosition;
//                        }
//                    }

//                    if (mMinTransform.position.y <= range.MinPosition.y && mMaxTransform.position.y >= range.MaxPosition.y)
//                    {
//                        return false;
//                    }
//                    mMinMaxRange = range;
//                    break;
//            }

//            return true;
//        }
//        else
//        {
//            return false;
//        }
//    }

//    public override void Execute()
//    {
//        switch (mDirection)
//        {
//            case EDirectionFlags.Horizontal:
//                if (mMinTransform.position.x > mMinMaxRange.MinPosition.x)
//                {
//                    mMinTransform.Translate(-mSpeed * Time.deltaTime, 0.0f, 0.0f, Space.World);
//                    mRemainTimeOfMin = Ending_TIME;
//                }
//                else
//                {
//                    if(mRemainTimeOfMin > 0.0f)
//                    {
//                        mRemainTimeOfMin -= Time.deltaTime;
//                    }
//                    else
//                    {
//                        mMinAnimation.SetActive(false);
//                    }
//                }

//                if (mMaxTransform.position.x < mMinMaxRange.MaxPosition.x)
//                {
//                    mMaxTransform.Translate(mSpeed * Time.deltaTime, 0.0f, 0.0f, Space.World);
//                    mRemainTimeOfMax = Ending_TIME;
//                }
//                else
//                {
//                    if(mRemainTimeOfMax > 0.0f)
//                    {
//                        mRemainTimeOfMax -= Time.deltaTime;
//                    }
//                    else
//                    {
//                        mMaxAnimation.SetActive(false);
//                    }
//                }

//                Vector2 minPosition = mMinTransform.position;
//                Vector2 maxPosition = mMaxTransform.position;

//                mMask.transform.localScale = new Vector3((maxPosition.x - minPosition.x), mMaskThickness, 1.0f);
//                mMask.transform.position = new Vector3((maxPosition.x + minPosition.x) * 0.5f, mMinMaxRange.MinPosition.y);
//                break;
//            case EDirectionFlags.Vertical:
//                if (mMinTransform.position.y > mMinMaxRange.MinPosition.y)
//                {
//                    mMinTransform.Translate(0.0f, -mSpeed * Time.deltaTime, 0.0f, Space.World);
//                    mRemainTimeOfMin = Ending_TIME;
//                }
//                else
//                {
//                    if (mRemainTimeOfMin > 0.0f)
//                    {
//                        mRemainTimeOfMin -= Time.deltaTime;
//                    }
//                    else
//                    {
//                        mMinAnimation.SetActive(false);
//                    }
//                }
//                if (mMaxTransform.position.y < mMinMaxRange.MaxPosition.y)
//                {
//                    mMaxTransform.Translate(0.0f, mSpeed * Time.deltaTime, 0.0f, Space.World);
//                    mRemainTimeOfMax = Ending_TIME;
//                }
//                else
//                {
//                    if (mRemainTimeOfMax > 0.0f)
//                    {
//                        mRemainTimeOfMax -= Time.deltaTime;
//                    }
//                    else
//                    {
//                        mMaxAnimation.SetActive(false);
//                    }
//                }

//                minPosition = mMinTransform.position;
//                maxPosition = mMaxTransform.position;

//                mMask.transform.localScale = new Vector3(mMaskThickness, (maxPosition.y - minPosition.y), 1.0f);
//                mMask.transform.position = new Vector3(mMinMaxRange.MinPosition.x, (maxPosition.y + minPosition.y) * 0.5f);
//                break;
//        }

//        Collider2D[] flammables = TriggerHandler.GetColliders(ETriggerType_Projectile.PIVOT_MIN);
//        for (int i = 0; i < flammables.Length && flammables[i] != null; i++)
//        {
//            Flammable flammable = flammables[i].GetComponent<Flammable>();
//            if (flammable != null)
//            {
//                if (mDirection != flammable.Direction)
//                {
//                    MinMax range = flammable.GetRange(this);

//                    switch (flammable.Direction)
//                    {
//                        case EDirectionFlags.Horizontal:
//                            if (range.MinPosition.x < range.MaxPosition.x)
//                            {
//                                ProjectileHandler.Fire(EProjectileFlags.IGNITION, mMinTransform.position, Vector4.zero, new Vector3(1.0f, 1.0f, 1.0f), flammable.transform);
//                            }
//                            break;
//                        case EDirectionFlags.Vertical:
//                            if (range.MinPosition.y < range.MaxPosition.y)
//                            {
//                                ProjectileHandler.Fire(EProjectileFlags.IGNITION, mMinTransform.position, Vector4.zero, new Vector3(1.0f, 1.0f, 1.0f), flammable.transform);
//                            }
//                            break;
//                    }
//                }
//                else if (!mFlammableList.Contains(flammable))
//                {
//                    AddFlammable(flammable);
//                }
//            }
//            else
//            {
//                BushStackDebuff bush = flammables[i].GetComponent<BushStackDebuff>();
//                if(bush != null && !bush.IsBurning)
//                {
//                    bush.IsBurning = true;
//                }
//            }
//        }
//        flammables = TriggerHandler.GetColliders(ETriggerType_Projectile.PIVOT_MAX);
//        for (int i = 0; i < flammables.Length && flammables[i] != null; i++)
//        {
//            Flammable flammable = flammables[i].GetComponent<Flammable>();
//            if (flammable != null)
//            {
//                if (mDirection != flammable.Direction)
//                {
//                    MinMax range = flammable.GetRange(this);

//                    switch (flammable.Direction)
//                    {
//                        case EDirectionFlags.Horizontal:
//                            if (range.MinPosition.x < range.MaxPosition.x)
//                            {
//                                ProjectileHandler.Fire(EProjectileFlags.IGNITION, mMaxTransform.position, Vector4.zero, new Vector3(1.0f, 1.0f, 1.0f), flammable.transform);
//                            }
//                            break;
//                        case EDirectionFlags.Vertical:
//                            if (range.MinPosition.y < range.MaxPosition.y)
//                            {
//                                ProjectileHandler.Fire(EProjectileFlags.IGNITION, mMaxTransform.position, Vector4.zero, new Vector3(1.0f, 1.0f, 1.0f), flammable.transform);
//                            }
//                            break;
//                    }
//                }
//                else if (!mFlammableList.Contains(flammable))
//                {
//                    AddFlammable(flammable);
//                }
//            }
//            else
//            {
//                BushStackDebuff bush = flammables[i].GetComponent<BushStackDebuff>();
//                if (bush != null && !bush.IsBurning)
//                {
//                    bush.IsBurning = true;
//                }
//            }
//        }
//    }

//    public override void InitializeProjectile()
//    {
//        mFlammableList.Clear();
//        mMinAnimation.SetActive(true);
//        mMaxAnimation.SetActive(true);
//        Flammable flammable = DstTransform.GetComponent<Flammable>();
//        if (flammable != null)
//        {
//            AddFlammable(flammable);

//            switch (flammable.Direction)
//            {
//                case EDirectionFlags.Horizontal:
//                    mDirection = EDirectionFlags.Horizontal;
//                    mMask.transform.localScale = new Vector3(0.0f, 1.0f, 1.0f);
//                    transform.position = new Vector3(transform.position.x, flammable.transform.position.y, 0.0f);
//                    break;
//                case EDirectionFlags.Vertical:
//                    mDirection = EDirectionFlags.Vertical;
//                    mMask.transform.localScale = new Vector3(1.0f, 0.0f, 1.0f);
//                    transform.position = new Vector3(flammable.transform.position.x, transform.position.y, 0.0f);
//                    break;
//            }
//            mMask.transform.localPosition = Vector3.zero;
//            mMinTransform.localPosition = Vector3.zero;
//            mMaxTransform.localPosition = Vector3.zero;
//        }
//    }

//    public override void FinalizeProjectile()
//    {
//        for(int i=0; i<mFlammableList.Count; i++)
//        {
//            mFlammableList[i].RemoveIgnition(this);
//            mFlammableList[i].BurnOut();
//        }
//        gameObject.SetActive(false);
//    }

//    public void AddFlammable(Flammable flammable)
//    {
//        if(flammable.AddIgnition(this))
//        {
//            mFlammableList.Add(flammable);
//        }
//    }

//    public void RemoveFlammable(Flammable flammable)
//    {
//        mFlammableList.Remove(flammable);
//        flammable.RemoveIgnition(this);
//    }


//    protected override void Awake()
//    {
//        base.Awake();
//        mMask = GetComponentInChildren<SpriteMask>();
//        mFlammableList = new List<Flammable>();
//    }
//}
