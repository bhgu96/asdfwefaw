﻿//using UnityEngine;
//using System.Collections.Generic;

//public class Flammable : MonoBehaviour
//{
//    public Transform MinAnchor { get { return mMinAnchor; } }
//    public Transform MaxAnchor { get { return mMaxAnchor; } }
//    public EDirectionFlags Direction
//    {
//        get
//        {
//            float min_y = mMinAnchor.position.y;
//            float max_y = mMaxAnchor.position.y;
//            if (Mathf.Approximately(min_y, max_y))
//            {
//                Vector2 minPosition = mMinAnchor.position;
//                Vector2 maxPosition = mMaxAnchor.position;
//                if (minPosition.x > maxPosition.x)
//                {
//                    mMinAnchor.position = maxPosition;
//                    mMaxAnchor.position = minPosition;
//                }
//                return EDirectionFlags.Horizontal;
//            }
//            else
//            {
//                Vector2 minPosition = mMinAnchor.position;
//                Vector2 maxPosition = mMaxAnchor.position;
//                if (minPosition.y > maxPosition.y)
//                {
//                    mMinAnchor.position = maxPosition;
//                    mMaxAnchor.position = minPosition;
//                }
//                return EDirectionFlags.Vertical;
//            }
//        }
//    }
//    [SerializeField]
//    private SpriteRenderer mMainSprite;

//    [SerializeField]
//    private Transform mMinAnchor;
//    [SerializeField]
//    private Transform mMaxAnchor;
//    private SortedList<float, Ignition> mIgnitionList;

//    public void BurnOut()
//    {
//        mMainSprite.gameObject.SetActive(false);
//    }

//    public bool IsIgnitionRegistered(Ignition ignition)
//    {
//        return mIgnitionList.ContainsValue(ignition);
//    }

//    public bool AddIgnition(Ignition ignition)
//    {
//        if(mIgnitionList.Count == 0)
//        {
//            mMainSprite.maskInteraction = SpriteMaskInteraction.VisibleOutsideMask;
//        }

//        if (Direction == EDirectionFlags.Horizontal)
//        {
//            if (!mIgnitionList.ContainsKey(ignition.transform.position.x))
//            {
//                mIgnitionList.Add(ignition.transform.position.x, ignition);
//                return true;
//            }
//            return false;
//        }
//        else
//        {
//            if (!mIgnitionList.ContainsKey(ignition.transform.position.y))
//            {
//                mIgnitionList.Add(ignition.transform.position.y, ignition);
//                return true;
//            }
//            return false;
//        }
//    }
//    public void RemoveIgnition(Ignition ignition)
//    {
//        if(mIgnitionList.Count == 1)
//        {
//            mMainSprite.maskInteraction = SpriteMaskInteraction.None;
//        }

//        if (Direction == EDirectionFlags.Horizontal)
//        {
//            mIgnitionList.Remove(ignition.transform.position.x);
//        }
//        else
//        {
//            mIgnitionList.Remove(ignition.transform.position.y);
//        }
//    }

//    /// <summary>
//    /// Flammable 내에서 Ignition의 MinMax Range를 갱신하는 메소드
//    /// </summary>
//    /// <param name="ignition"></param>
//    public MinMax GetRange(Ignition ignition)
//    {
//        EDirectionFlags direction = Direction;
//        Vector2 curPosition = ignition.transform.position;
//        Vector2 minPosition = mMinAnchor.position;
//        MinMax range = new MinMax();
//        IList<Ignition> ignitionList = mIgnitionList.Values;
//        switch (direction)
//        {
//            case EDirectionFlags.Horizontal:
//                for(int i=0; i<ignitionList.Count; i++)
//                {
//                    if (ignition != ignitionList[i])
//                    {
//                        Vector2 nextMinPosition = ignitionList[i].MinTransform.position;
//                        if (curPosition.x < nextMinPosition.x)
//                        {
//                            range.MinPosition = minPosition;
//                            range.MaxPosition = nextMinPosition;
//                            return range;
//                        }
//                        minPosition = ignitionList[i].MaxTransform.position;
//                    }
//                }
//                range.MinPosition = minPosition;
//                range.MaxPosition = mMaxAnchor.position;
//                return range;
//            case EDirectionFlags.Vertical:
//                for (int i = 0; i < ignitionList.Count; i++)
//                {
//                    if (ignition != ignitionList[i])
//                    {
//                        Vector2 nextMinPosition = ignitionList[i].MinTransform.position;
//                        if (curPosition.y < nextMinPosition.y)
//                        {
//                            range.MinPosition = minPosition;
//                            range.MaxPosition = nextMinPosition;
//                            return range;
//                        }
//                        minPosition = ignitionList[i].MaxTransform.position;
//                    }
//                }
//                range.MinPosition = minPosition;
//                range.MaxPosition = mMaxAnchor.position;
//                return range;
//        }
//        return range;
//    }
//    private void OnDrawGizmos()
//    {
//        if (mMinAnchor != null && mMaxAnchor != null)
//        {
//            CapsuleCollider2D collider = GetComponent<CapsuleCollider2D>();

//            if (collider != null)
//            {
//                Vector2 minPosition = mMinAnchor.localPosition;
//                Vector2 maxPosition = mMaxAnchor.localPosition;
//                if (Mathf.Approximately(minPosition.y, maxPosition.y))
//                {
//                    float size_x = Mathf.Abs(maxPosition.x - minPosition.x);
//                    float offset_x = (maxPosition.x + minPosition.x) * 0.5f;
//                    collider.direction = CapsuleDirection2D.Horizontal;
//                    collider.size = new Vector2(size_x, collider.size.y);
//                    collider.offset = new Vector2(offset_x, collider.offset.y);
//                }
//                else
//                {
//                    float size_y = Mathf.Abs(maxPosition.x - minPosition.x);
//                    float offset_y = (maxPosition.y + minPosition.y) * 0.5f;
//                    collider.direction = CapsuleDirection2D.Vertical;
//                    collider.size = new Vector2(collider.size.x, size_y);
//                    collider.offset = new Vector2(collider.offset.x, offset_y);
//                }
//            }
//        }
//    }

//    private void Awake()
//    {
//        mIgnitionList = new SortedList<float, Ignition>();
//    }

//    private void Update()
//    {
//        if (mIgnitionList.Count > 0)
//        {
//            IList<Ignition> ignitionList = mIgnitionList.Values;
//            switch (Direction)
//            {
//                case EDirectionFlags.Horizontal:

//                    float minRange = ignitionList[0].MinTransform.position.x;
//                    float maxRange = ignitionList[0].MaxTransform.position.x;
//                    for(int i=1; i< ignitionList.Count; i++)
//                    {
//                        float nextMinRange = ignitionList[i].MinTransform.position.x;
//                        float nextMaxRange = ignitionList[i].MaxTransform.position.x;

//                        if(nextMinRange < minRange)
//                        {
//                            minRange = nextMinRange;
//                        }
//                        if(nextMaxRange > maxRange)
//                        {
//                            maxRange = nextMaxRange;
//                        }
//                    }

//                    if(MinAnchor.position.x > minRange && MaxAnchor.position.x < maxRange)
//                    {
//                        BurnOut();
//                    }
//                break;
//                case EDirectionFlags.Vertical:
//                    minRange = ignitionList[0].MinTransform.position.y;
//                    maxRange = ignitionList[0].MaxTransform.position.y;
//                    for (int i = 1; i < ignitionList.Count; i++)
//                    {
//                        float nextMinRange = ignitionList[i].MinTransform.position.y;
//                        float nextMaxRange = ignitionList[i].MaxTransform.position.y;

//                        if (nextMinRange < minRange)
//                        {
//                            minRange = nextMinRange;
//                        }
//                        if (nextMaxRange > maxRange)
//                        {
//                            maxRange = nextMaxRange;
//                        }
//                    }

//                    if (MinAnchor.position.y > minRange && MaxAnchor.position.y < maxRange)
//                    {
//                        BurnOut();
//                    }
//                    break;
//            }
//        }
//    }

//    private void OnDisable()
//    {
//        IList<Ignition> ignitionList = mIgnitionList.Values;
//        for (int i = 0; i < ignitionList.Count; i++)
//        {
//            ignitionList[i].RemoveFlammable(this);
//        }
//        mIgnitionList.Clear();
//    }
//}
