﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class StageText : MonoBehaviour
{
    private Animator mAnimator;
    private Text mText;

    private int mLevel = 0;

    private void Awake()
    {
        mAnimator = GetComponent<Animator>();
        mText = GetComponent<Text>();
    }

    private void Update()
    {
        if(mLevel != _Spawner.Main.Level)
        {
            mLevel = _Spawner.Main.Level;
            mText.text = "Rush Phase " + mLevel + " !";
            mAnimator.SetTrigger("play");
        }
    }
}
