﻿using UnityEngine;
using UnityEngine.UI;
using System;

public class ABar<TActor, EState> : StateBase<EState>
    where EState : Enum
{
    [SerializeField]
    private SpriteRenderer mFrame;

    [SerializeField]
    private Image mContent;

    /// <summary>
    /// 대상
    /// </summary>
    public TActor Target { get; private set; }

    /// <summary>
    /// 프레임 활성화 여부
    /// </summary>
    public bool IsActive
    {
        get { return mFrame.gameObject.activeInHierarchy; }
        set { mFrame.gameObject.SetActive(value); }
    }

    /// <summary>
    /// 콘텐트의 양
    /// </summary>
    public float FillAmount
    {
        get { return mContent.fillAmount; }
        set { mContent.fillAmount = value; }
    }

    /// <summary>
    /// 프레임, 콘텐트의 알파값을 설정하는 메소드
    /// </summary>
    /// <param name="alpha"></param>
    public virtual void SetAlpha(float alpha)
    {
        Color prevColor = mFrame.color;
        mFrame.color = new Color(prevColor.r, prevColor.g, prevColor.b, alpha);
        prevColor = mContent.color;
        mContent.color = new Color(prevColor.r, prevColor.g, prevColor.b, alpha);
    }

    protected override void Awake()
    {
        base.Awake();
        Target = GetComponentInParent<TActor>();
        IsActive = false;
    }
}

