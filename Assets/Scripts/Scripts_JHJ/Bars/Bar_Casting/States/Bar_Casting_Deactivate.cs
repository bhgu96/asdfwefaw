﻿using UnityEngine;
using System.Collections;

public class Bar_Casting_Deactivate : State<EStateType_Bar_Casting>
{
    private Bar_Casting mBar;

    private const float FADE_TIME = 1.0F;
    private float mRemainTime;

    public Bar_Casting_Deactivate(Bar_Casting bar) : base(EStateType_Bar_Casting.Deactivate)
    {
        mBar = bar;
    }

    public override void Start()
    {
        mRemainTime = FADE_TIME;
    }

    public override void Update()
    {
        ISkill skill = mBar.Target.CastingSkill;
        if(skill != null && skill.IsCasting)
        {
            mBar.State = EStateType_Bar_Casting.Active;
            return;
        }

        mRemainTime -= Time.deltaTime;
        if(mRemainTime > 0.0f)
        {
            mBar.SetAlpha(Mathf.Lerp(0.0f, 1.0f, mRemainTime / FADE_TIME));
        }
        else
        {
            mBar.State = EStateType_Bar_Casting.Idle;
        }
    }
}
