﻿using UnityEngine;
using System.Collections;

public class Bar_Casting_Idle : State<EStateType_Bar_Casting>
{
    private Bar_Casting mBar;

    public Bar_Casting_Idle(Bar_Casting bar) : base(EStateType_Bar_Casting.Idle)
    {
        mBar = bar;
    }

    public override void Start()
    {
        mBar.IsActive = false;
    }

    public override void Update()
    {
        ISkill skill = mBar.Target.CastingSkill;
        if(skill != null && skill.IsCasting && skill.CastRemainTime > Mathf.Epsilon)
        {
            mBar.State = EStateType_Bar_Casting.Active;
        }
    }
}
