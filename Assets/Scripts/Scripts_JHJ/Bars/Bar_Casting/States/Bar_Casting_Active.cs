﻿using UnityEngine;
using System.Collections;

public class Bar_Casting_Active : State<EStateType_Bar_Casting>
{
    private Bar_Casting mBar;

    public Bar_Casting_Active(Bar_Casting bar) : base(EStateType_Bar_Casting.Active)
    {
        mBar = bar;
    }

    public override void Start()
    {
        mBar.IsActive = true;
        mBar.SetAlpha(1.0f);
        mBar.FillAmount = 0.0f;
    }

    public override void Update()
    {
        ISkill castingSkill = mBar.Target.CastingSkill;
        float castTime = castingSkill.CastTime;
        float remainTime = castingSkill.CastRemainTime;
        // Amount 설정
        mBar.FillAmount = (castTime - remainTime) / castTime;
        // 캐스팅이 끝났다면 Deactivate상태로 전이
        if(!castingSkill.IsCasting)
        {
            mBar.State = EStateType_Bar_Casting.Deactivate;
        }
    }
}
