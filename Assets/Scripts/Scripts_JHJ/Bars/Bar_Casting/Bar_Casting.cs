﻿using UnityEngine;
using System.Collections;

public enum EStateType_Bar_Casting
{
    Idle = 0, 
    Active = 1, 
    Deactivate = 2, 
}

public class Bar_Casting : ABar<IActor, EStateType_Bar_Casting>
{
    protected override void Awake()
    {
        base.Awake();
        SetStates(new Bar_Casting_Idle(this)
            , new Bar_Casting_Active(this)
            , new Bar_Casting_Deactivate(this));
    }
}
