﻿using UnityEngine;
using System.Collections;

public class Bar_Life_Idle : State<EStateType_Bar_Life>
{
    private Bar_Life mBar;

    public Bar_Life_Idle(Bar_Life bar) : base(EStateType_Bar_Life.Idle)
    {
        mBar = bar;
    }

    public override void Start()
    {
        mBar.IsActive = false;
    }

    public override void Update()
    {
        float targetAmount = mBar.TargetAmount;
        if (Mathf.Approximately(targetAmount, 0.0f))
        {
            mBar.State = EStateType_Bar_Life.Idle;
        }
        else if (!Mathf.Approximately(mBar.FillAmount, targetAmount))
        {
            mBar.State = EStateType_Bar_Life.Active;
        }
    }
}
