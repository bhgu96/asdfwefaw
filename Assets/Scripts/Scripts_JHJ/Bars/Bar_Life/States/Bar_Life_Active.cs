﻿using UnityEngine;
using System.Collections;

public class Bar_Life_Active : State<EStateType_Bar_Life>
{
    private Bar_Life mBar;
    private const float ACTIVE_TIME = 3.0F;
    private float mRemainTime;

    public Bar_Life_Active(Bar_Life bar) : base(EStateType_Bar_Life.Active)
    {
        mBar = bar;
    }

    public override void Start()
    {
        mBar.IsActive = true;
        mBar.FillAmount = mBar.TargetAmount;
        mRemainTime = ACTIVE_TIME;
    }

    public override void Update()
    {
        float targetAmount = mBar.TargetAmount;
        if(Mathf.Approximately(targetAmount, 0.0f))
        {
            mBar.State = EStateType_Bar_Life.Idle;
        }

        if(!Mathf.Approximately(mBar.FillAmount, targetAmount))
        {
            mBar.FillAmount = targetAmount;
            mRemainTime = ACTIVE_TIME;
        }
        else if(mRemainTime > 0.0f)
        {
            mRemainTime -= Time.deltaTime;
        }
        else
        {
            mBar.State = EStateType_Bar_Life.Idle;
        }
    }
}
