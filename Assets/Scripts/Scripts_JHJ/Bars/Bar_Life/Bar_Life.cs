﻿using UnityEngine;
using System.Collections;

public enum EStateType_Bar_Life
{
    Idle, 
    Active, 
}

public class Bar_Life : ABar<_IActor_Damagable, EStateType_Bar_Life>
{
    public float TargetAmount { get { return (float)Target.Shield / Target.MaxShield; } }

    protected override void Awake()
    {
        base.Awake();
        SetStates(new Bar_Life_Idle(this)
            , new Bar_Life_Active(this));
    }

    private void Start()
    {
        FillAmount = TargetAmount;
    }
}
