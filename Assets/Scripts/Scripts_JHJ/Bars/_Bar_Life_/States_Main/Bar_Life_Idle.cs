﻿//using UnityEngine;
//using System.Collections;

//public class Bar_Life_Idle : State<EStateType_Bar_Life>
//{
//    private Bar_Life mBar;
//    public Bar_Life_Idle(Bar_Life bar) : base(EStateType_Bar_Life.Idle)
//    {
//        mBar = bar;
//    }

//    public override void Start()
//    {
//        mBar.IsActive = false;
//    }

//    public override void Update()
//    {
//        // Target의 Life 기준 Amount와 Bar가 현재 가리키고 있는 Amount 값이 다르다면
//        if(!Mathf.Approximately(mBar.CurAmount, mBar.FillAmount))
//        {
//            // Active 상태로 전이
//            mBar.State = EStateType_Bar_Life.Active;
//        }
//    }
//}
