﻿//using UnityEngine;
//using System.Collections;

//public class Bar_Life_Deactivate : State<EStateType_Bar_Life>
//{
//    private Bar_Life mBar;
//    private float mFadingTime;
//    private const float FADE_TIME = 0.5F;

//    public Bar_Life_Deactivate(Bar_Life bar) : base(EStateType_Bar_Life.Deactivate)
//    {
//        mBar = bar;
//    }

//    public override void Start()
//    {
//        mFadingTime = FADE_TIME;
//    }

//    public override void Update()
//    {
//        float mainAmount = mBar.FillAmount;
//        float curAmount = mBar.CurAmount;
//        // Main Amount와 Cur Amount가 일치하지 않는 경우
//        if(!Mathf.Approximately(mainAmount, curAmount))
//        {
//            // Active  상태로 전이
//            mBar.State = EStateType_Bar_Life.Active;
//            return;
//        }

//        mFadingTime -= Time.deltaTime;
//        if(mFadingTime > 0.0f)
//        {
//            mBar.SetAlpha(Mathf.Lerp(0.0f, 1.0f, mFadingTime / FADE_TIME));
//        }
//        else
//        {
//            mBar.State = EStateType_Bar_Life.Idle;
//        }
//    }
//}
