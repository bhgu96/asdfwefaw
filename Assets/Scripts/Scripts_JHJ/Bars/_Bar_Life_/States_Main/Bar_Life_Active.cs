﻿//using UnityEngine;
//using System.Collections;

//public class Bar_Life_Active : State<EStateType_Bar_Life>
//{
//    private Bar_Life mBar;

//    private float mSubContentTime;

//    private const float SUB_CONTENT_TIME = 0.1f;

//    private float mSubAmount_Target;

//    public Bar_Life_Active(Bar_Life bar) : base(EStateType_Bar_Life.Active)
//    {
//        mBar = bar;
//    }

//    public override void Start()
//    {
//        // Frame, Main, Sub Content를 활성화
//        mBar.IsActive = true;
//        mBar.SetAlpha(1.0f);
//        mSubAmount_Target = mBar.SubFillAmount;
//        mSubContentTime = SUB_CONTENT_TIME;
//    }

//    public override void Update()
//    {
//        // Main Content의 경우...
//        float curAmount = mBar.CurAmount;
//        float mainAmount = mBar.FillAmount;
//        float subAmount = mBar.SubFillAmount;

//        if(!Mathf.Approximately(mainAmount, curAmount))
//        {
//            mainAmount = Mathf.MoveTowards(mainAmount, curAmount, Bar_Life.MAIN_BAR_SPEED * Time.deltaTime);
//            mBar.FillAmount = mainAmount;
//        }

//        // Sub Content의 경우...
//        // Main Amount가 Sub Amount보다 큰경우
//        if(mainAmount > subAmount)
//        {
//            subAmount = mainAmount;
//            mBar.SubFillAmount = subAmount;
//        }
//        // Main Amount보다 Sub Amount가 큰 경우
//        else if(mainAmount < subAmount)
//        {
//            // Main Amount와 Sub Target Amount가 일치하지 않는 경우
//            if(!Mathf.Approximately(mSubAmount_Target, mainAmount))
//            {
//                mSubAmount_Target = mainAmount;
//                mSubContentTime = SUB_CONTENT_TIME;
//            }
//            // Sub Target Amount와 Main Amount가 일치하는 경우
//            else
//            {
//                // Sub Content 대기 시간이 남은 경우
//                if(mSubContentTime > 0.0f)
//                {
//                    mSubContentTime -= Time.deltaTime;
//                }
//                // 대기시간이 남지 않은 경우
//                else
//                {
//                    subAmount = Mathf.MoveTowards(subAmount, mSubAmount_Target, Bar_Life.SUB_BAR_SPEED * Time.deltaTime);
//                    mBar.SubFillAmount = subAmount;
//                }
//            }
//        }
//        // Main Amount와 Sub Amount 가 일치하는 경우
//        else
//        {
//            // Deactivate 상태로 전이
//            mBar.State = EStateType_Bar_Life.Deactivate;
//        }
//    }
//}
