﻿//using UnityEngine;
//using UnityEngine.UI;
//using System.Collections;

//public enum EStateType_Bar_Life
//{
//    Idle, 
//    Active, 
//    Deactivate,
//}

//public class Bar_Life : ABar<IActor_Damagable, EStateType_Bar_Life>
//{
//    public const float MAIN_BAR_SPEED = 5.0f;
//    public const float SUB_BAR_SPEED = 2.0F;

//    [SerializeField]
//    private Image mSubContent;

//    /// <summary>
//    /// Sub Content의 양
//    /// </summary>
//    public float SubFillAmount
//    {
//        get { return mSubContent.fillAmount; }
//        set { mSubContent.fillAmount = value; }
//    }

//    /// <summary>
//    /// 프레임, 콘텐트의 알파값 설정 메소드
//    /// </summary>
//    /// <param name="alpha"></param>
//    public override void SetAlpha(float alpha)
//    {
//        base.SetAlpha(alpha);
//        Color prevColor = mSubContent.color;
//        if (alpha >= 1.0f)
//        {
//            mSubContent.color = new Color(prevColor.r, prevColor.g, prevColor.b, 1.0f);
//        }
//        else
//        {
//            mSubContent.color = new Color(prevColor.r, prevColor.g, prevColor.b, 0.0f);
//        }
//    }

//    /// <summary>
//    /// Target의 Life 기준 Fill Amount
//    /// </summary>
//    public float CurAmount { get { return Target.Status.Life / Target.Status.MaxLife; } }

//    protected override void Awake()
//    {
//        base.Awake();
//        SetStates(new Bar_Life_Idle(this)
//            , new Bar_Life_Active(this)
//            , new Bar_Life_Deactivate(this));
//    }

//    private void Start()
//    {
//        FillAmount = CurAmount;
//        SubFillAmount = FillAmount;
//    }
//}
