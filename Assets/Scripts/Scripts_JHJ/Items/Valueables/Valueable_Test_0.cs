﻿using UnityEngine;
using System.Collections;

public class Valueable_Test_0 : AValueable
{
    public override int MaxCount => 5;
    public override string Name => "Valueable_0";
    public override string Description => "Valueable_0's Description";
}
