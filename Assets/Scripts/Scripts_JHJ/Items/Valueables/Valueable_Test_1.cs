﻿using UnityEngine;
using System.Collections;

public class Valueable_Test_1 : AValueable
{

    public override string Name => "Valueable_1";
    public override string Description => "Valueable_1's Description";
    public override int MaxCount => 3;
}
