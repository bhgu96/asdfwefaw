﻿using UnityEngine;
using System.Collections;

public class Material_Test_0 : AMaterial
{
    public override int MaxCount => 20;
    public override string Name => "Material_0";
    public override string Description => "Material_0's Description";
}
