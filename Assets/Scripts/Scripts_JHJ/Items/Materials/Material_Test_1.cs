﻿using UnityEngine;
using System.Collections;

public class Material_Test_1 : AMaterial
{
    public override int MaxCount => 30;
    public override string Name => "Material_1";
    public override string Description => "Material_1's Description";
}
