﻿//using System.Collections;
//using System.Collections.Generic;
//using UnityEngine;

//public class Tutorial_Monster : MonoBehaviour
//{
//    private IActor mActor;

//    [SerializeField]
//    private GameObject mGuide;
//    private bool mIsInactive = false;
//    private void Awake()
//    {
//        mActor = GetComponentInParent<IActor>();
//    }

//    private void Update()
//    {
//        Vector3 curLocalScale = transform.localScale;
//        if (transform.lossyScale.x < 0.0f)
//        {
//            transform.localScale = new Vector3(curLocalScale.x * -1.0f, curLocalScale.y, curLocalScale.z);
//        }
//        BushStackDebuff debuff = mActor.State.DebuffDict[EDebuffFlags.BUSH_BOUND] as BushStackDebuff;
//        if (!mIsInactive)
//        {
//            if (debuff != null && debuff.IsFullLevel)
//            {
//                mGuide.SetActive(false);
//                mIsInactive = true;
//            }
//            else if (mActor.State.BehaviorFlag == EBehaviorFlags.DIE)
//            {
//                mGuide.SetActive(false);
//                mIsInactive = true;
//            }
//        }
//        else
//        {
//            if ((debuff == null || !debuff.IsFullLevel) && (mActor.State.BehaviorFlag != EBehaviorFlags.DIE))
//            {
//                mGuide.SetActive(true);
//                mIsInactive = false;
//            }
//        }
//    }
//}
