﻿using System;
using UnityEngine;

[RequireComponent(typeof(Rigidbody2D))]
public abstract class AInteractable<E, ETriggerType> : StateBase<E> where E : Enum where ETriggerType : Enum
{
    public SpriteRenderer MainRenderer { get { return mMainRenderer; } }
    [SerializeField]
    private SpriteRenderer mMainRenderer;

    public Rigidbody2D Rigidbody { get; private set; }
    /// <summary>
    /// 상호작용 가능한 물체가 트리거 됐는지 검사하기 위해 필요한 트리거 핸들러
    /// </summary>
    public TriggerHandler_AInteractable InteractableTrigger { get; private set; }

    private OutlineController mOutline;

    /// <summary>
    /// 상호작용 호출 메소드
    /// </summary>
    /// <param name="actor"></param>
    public abstract void Interact(IActor actor);
    /// <summary>
    /// 상호작용 가능한가를 반환하는 메소드
    /// </summary>
    /// <param name="actor"></param>
    /// <returns></returns>
    protected abstract bool CanInteract(IActor actor);

    protected override void Update()
    {
        base.Update();

        // Outline이 꺼져있다면
        if (mOutline.Toggle == 0)
        {
            // 모든 감지된 콜라이더에 대해
            Collider2D[] colliders = InteractableTrigger.GetColliders(ETriggerType_AInteractable.ACTOR);
            for (int i = 0; i < colliders.Length && colliders[i] != null; i++)
            {
                IActor actor = colliders[i].GetComponentInParent<IActor>();
                // 감지된 액터가 상호작용 가능하다면
                if (actor != null && CanInteract(actor))
                {
                    // Outline을 킨다.
                    mOutline.Toggle = 1;
                    break;
                }
            }
        } // Outline이 켜져있다면
        else
        {
            bool canInteract = false;
            // 모든 감지된 콜라이더에 대해
            Collider2D[] colliders = InteractableTrigger.GetColliders(ETriggerType_AInteractable.ACTOR);
            for (int i = 0; i < colliders.Length && colliders[i] != null; i++)
            {
                IActor actor = colliders[i].GetComponentInParent<IActor>();
                // 감지된 액터가 상호작용 가능하다면
                if (actor != null && CanInteract(actor))
                {
                    // 상호작용이 아직 가능하다는 표시를 남긴다
                    canInteract = true;
                    break;
                }
            }
            // 상호작용 가능한 액터가 존재하지 않는다면
            if(!canInteract)
            {
                // Outline을 끈다
                mOutline.Toggle = 0;
            }
        }
    }

    protected override void Awake()
    {
        base.Awake();
        if(mMainRenderer != null)
        {
            mOutline = mMainRenderer.GetComponent<OutlineController>();
        }

        InteractableTrigger = GetComponentInChildren<TriggerHandler_AInteractable>(true);
        Rigidbody = GetComponent<Rigidbody2D>();
    }
}
