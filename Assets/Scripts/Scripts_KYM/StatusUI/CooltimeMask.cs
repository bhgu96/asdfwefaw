﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CooltimeMask : MonoBehaviour
{
    [SerializeField]
    private ECharacterType mCharacterType;
    [SerializeField]
    private int mIndex;
    [SerializeField]
    private Text mCooltimeText;

    private Image mImage;

    private void Awake()
    {
        mImage = this.GetComponent<Image>();
    }

    private void Start()
    {
        this.gameObject.SetActive(false);
    }

    private void OnEnable()
    {
        if(mCharacterType == ECharacterType.Fairy && Fairy_Player.Main != null)
        {
            if(Fairy_Player.Main.SkillFrame.Peek(mIndex) != null)
            {
                StartCoroutine(ActivateCooltimeMask());
            }
        }
        else if(mCharacterType == ECharacterType.Giant && Giant_Player.Main != null)
        {
            if (Giant_Player.Main.SkillFrame.Peek(mIndex) != null)
            {
                StartCoroutine(ActivateCooltimeMask());
            }
        }
    }

    private IEnumerator ActivateCooltimeMask()
    {
        mImage.fillAmount = 1;

        if (mCharacterType == ECharacterType.Fairy)
        {
            while(Fairy_Player.Main.SkillFrame.Peek(mIndex).CoolRemainTime != 0)
            {
                mImage.fillAmount = Fairy_Player.Main.SkillFrame.Peek(mIndex).CoolRemainTime / Fairy_Player.Main.SkillFrame.Peek(mIndex).CoolTime;
                mCooltimeText.text = (Mathf.CeilToInt(Fairy_Player.Main.SkillFrame.Peek(mIndex).CoolRemainTime)).ToString();
                yield return null;
            }
        }
        else if (mCharacterType == ECharacterType.Giant)
        {
            while (Giant_Player.Main.SkillFrame.Peek(mIndex).CoolRemainTime != 0)
            {
                mImage.fillAmount = Giant_Player.Main.SkillFrame.Peek(mIndex).CoolRemainTime / Giant_Player.Main.SkillFrame.Peek(mIndex).CoolTime;
                mCooltimeText.text = (Mathf.CeilToInt(Giant_Player.Main.SkillFrame.Peek(mIndex).CoolRemainTime)).ToString();
                yield return null;
            }
        }

        mImage.fillAmount = 0;
        this.gameObject.SetActive(false);
    }
}
