﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class _FontManager : MonoBehaviour
{
    public static Font CurrentFont { get { return mCurrentFont; } private set { } }
    public static ELanguageType LanguageType { get { return mLanguageType; } private set { } }
    public static EFont FontType { get { return mFontType; } private set { } }

    [SerializeField]
    private static Font mPixelOperator8;
    [SerializeField]
    private static Font mDOSSaemmul;

    private static Font mCurrentFont;
    private static EFont mFontType;
    private static ELanguageType mLanguageType;

    private void Awake()
    {
        if(mPixelOperator8 == null)
        {
            mPixelOperator8 = Resources.Load("Fonts/PixelOperator8") as Font;
        }

        if(mDOSSaemmul == null)
        {
            mDOSSaemmul = Resources.Load("Fonts/DOSSaemmul") as Font;
        }

        if (mFontType == EFont.None)
        {
            mFontType = EFont.PixelOperator8;
        }


        if (mFontType == EFont.PixelOperator8)
        {
            mCurrentFont = mPixelOperator8;
        }
        else if (mFontType == EFont.DOSSaemmul)
        {
            mCurrentFont = mDOSSaemmul;
        }

        ChangeFont(mFontType);
    }

    public static void ChangeFont(EFont font)
    {
        if(font == EFont.PixelOperator)
        {
            mFontType = EFont.PixelOperator;
            mLanguageType = ELanguageType.English;
        }
        else if(font == EFont.PixelOperator8)
        {
            mCurrentFont = mPixelOperator8;
            mFontType = EFont.PixelOperator8;
            mLanguageType = ELanguageType.English;
        }
        else if(font == EFont.DOSSaemmul)
        {
            mCurrentFont = mDOSSaemmul;
            mFontType = EFont.DOSSaemmul;
            mLanguageType = ELanguageType.Korean;
        }
    }
}

public enum EFont
{
    None = 0,
    PixelOperator,
    PixelOperator_Bold,
    PixelOperator8,
    PixelOperator8_Bold,
    DOSSaemmul,
    DungGeunMo_Kor,
    DungGeunMo_Eng,
    Count
}
