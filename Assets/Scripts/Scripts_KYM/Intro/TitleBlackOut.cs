﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TitleBlackOut : MonoBehaviour
{
    [SerializeField]
    private float mSanc;
    [SerializeField]
    private Animator mStartAnimator;

    private Image mImage;
    private static bool mIsActivate;

    private void Awake()
    {
        mImage = this.GetComponent<Image>();
    }

    private void Start()
    {
        StartCoroutine(BlackOut());
    }

    private void OnDestroy()
    {
        mIsActivate = false;
    }

    private void Update()
    {
        if (mStartAnimator != null)
        {
            mStartAnimator.SetBool("IsMoving", mIsActivate);
        }
    }

    private IEnumerator BlackOut()
    {
        for(float a = 1; a >= 0; a -= Time.deltaTime * mSanc)
        {
            mImage.color = new Color(1, 1, 1, a);
            yield return null;
        }

        mImage.color = new Color(1, 1, 1, 0);

        if(mStartAnimator != null)
        {
            mIsActivate = true;
            mStartAnimator.SetBool("IsStart", true);
            mStartAnimator.SetBool("IsMoving", true);
        }
    }
}
