﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ButtonEvent_SetFont : MonoBehaviour
{
    [SerializeField]
    private EFont mFont;
    [SerializeField]
    private RectTransform mCheckTransform;

    private UIButton mButton;

    private void Awake()
    {
        mButton = this.GetComponent<UIButton>();
        mButton.onConfirm += ConfirmButton;
    }

    private void ConfirmButton()
    {
        mCheckTransform.SetParent(this.transform);
        mCheckTransform.anchoredPosition = new Vector2(0f, 0f);
        _FontManager.ChangeFont(mFont);
    }
}
