﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class IntroSceneController : MonoBehaviour
{
    [SerializeField]
    private CameraController mCamera;
    [SerializeField]
    private float mSmoother;
    [SerializeField]
    private EGameScene mNextScene;

    private RectTransform mRectTransform;

    private void Awake()
    {
        mRectTransform = this.GetComponent<RectTransform>();
        mCamera.BlackOut(0.3f, 0f);
    }

    private void Start()
    {
        StartCoroutine(ShakeLogo());
    }

    private IEnumerator ShakeLogo()
    {
        yield return new WaitUntil(() => !mCamera.IsBlackOutTriggered);

        yield return new WaitForSeconds(1f);

        int signValue = 1;
        float zAngle = 0;

        for(int shakeCount = 0; shakeCount < 4; shakeCount++)
        {
            if(signValue > 0)
            {
                while (zAngle <= 3f)
                {
                    zAngle += Time.deltaTime * mSmoother;
                    mRectTransform.eulerAngles = new Vector3(mRectTransform.eulerAngles.x, mRectTransform.eulerAngles.y, zAngle);
                    yield return null;
                }

                mRectTransform.eulerAngles = new Vector3(mRectTransform.eulerAngles.x, mRectTransform.eulerAngles.y, +3f);
            }
            else if(signValue < 0)
            {
                while (zAngle >= -3f)
                {
                    zAngle -= Time.deltaTime * mSmoother;
                    mRectTransform.eulerAngles = new Vector3(mRectTransform.eulerAngles.x, mRectTransform.eulerAngles.y, zAngle);
                    yield return null;
                }

                mRectTransform.eulerAngles = new Vector3(mRectTransform.eulerAngles.x, mRectTransform.eulerAngles.y, -3f);
            }

            signValue *= -1;
            yield return null;
        }

        mRectTransform.eulerAngles = new Vector3(mRectTransform.eulerAngles.x, mRectTransform.eulerAngles.y, 0f);

        yield return new WaitForSeconds(1f);
        mCamera.BlackOut(0.3f, 1f);
        yield return new WaitUntil(() => !mCamera.IsBlackOutTriggered);

        yield return new WaitForSeconds(0.1f);

        LoadingController.LoadScene(mNextScene.ToString());
    }
}
