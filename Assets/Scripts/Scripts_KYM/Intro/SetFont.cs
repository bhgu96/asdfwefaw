﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SetFont : MonoBehaviour
{
    public string Korean { get { return mKorean; } set { mKorean = value; } }
    public string English { get { return mEnglish; } set { mEnglish = value; } }

    [SerializeField]
    private string mKorean;
    [SerializeField]
    private string mEnglish;
    [SerializeField]
    private bool mIsUnchanged;

    private Text mText;
    private int mDefaultFontSize;

    private void Awake()
    {
        mText = this.GetComponent<Text>();
        mDefaultFontSize = this.GetComponent<Text>().fontSize;
    }

    private void Update()
    {
        if (mText != null && _FontManager.CurrentFont != null)
        {
            mText.font = _FontManager.CurrentFont;

            if (!mIsUnchanged)
            {
                if (_FontManager.LanguageType == ELanguageType.Korean)
                {
                    mText.text = mKorean;
                }
                else if (_FontManager.LanguageType == ELanguageType.English)
                {
                    mText.text = mEnglish;
                }
            }

            if (_FontManager.FontType == EFont.PixelOperator8)
            {
                if (mDefaultFontSize >= 70)
                {
                    mText.fontSize = mDefaultFontSize - 25;
                }
                else if (mDefaultFontSize >= 50)
                {
                    mText.fontSize = mDefaultFontSize - 25;
                }
                else if (mDefaultFontSize >= 45)
                {
                    mText.fontSize = mDefaultFontSize - 15;
                }
                else
                {
                    mText.fontSize = mDefaultFontSize - 5;
                }
            }
            else
            {
                mText.fontSize = mDefaultFontSize;
            }
        }
    }
}
