﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ButtonEvent_BackToTitle : MonoBehaviour
{
    private UIButton mButton;

    private void Awake()
    {
        mButton = this.GetComponent<UIButton>();
        mButton.onConfirm += ConfirmButton;
    }

    private void ConfirmButton()
    {
        StateManager.Pause.State = false;
        StateManager.GameOver.State = false;
        Time.timeScale = 1.0f;
        ScoreData.Save();
        LoadingController.LoadScene("Scene_NewTitle");
    }
}
