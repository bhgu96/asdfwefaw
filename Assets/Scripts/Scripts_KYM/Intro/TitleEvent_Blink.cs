﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TitleEvent_Blink : MonoBehaviour
{
    [SerializeField]
    private GameObject mTitleObject;
    private Animator mAnimator;

    private void Awake()
    {
        mAnimator = this.GetComponent<Animator>();
    }

    private void EnableTitle()
    {
        this.gameObject.SetActive(false);
    }

    private void Activate()
    {
        mTitleObject.SetActive(true);
    }
}
