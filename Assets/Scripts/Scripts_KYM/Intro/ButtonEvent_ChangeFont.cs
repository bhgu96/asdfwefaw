﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ButtonEvent_ChangeFont : MonoBehaviour
{
    [SerializeField]
    private EFont mFont;

    private UIButton mButton;

    private void Awake()
    {
        mButton = this.GetComponent<UIButton>();
        mButton.onConfirm += ConfirmButton;
    }

    private void ConfirmButton()
    {
        _FontManager.ChangeFont(mFont);
    }
}
