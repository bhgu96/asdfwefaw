﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ButtonEvent_Credit : MonoBehaviour
{
    [SerializeField]
    private UIPanel mCreditPanel;

    private UIPanel mPanel;
    private UIButton mButton;
    private UIController mUIController;

    private void Awake()
    {
        mButton = this.GetComponent<UIButton>();
        mPanel = this.GetComponentInParent<UIPanel>();
        mButton.onConfirm += ConfirmButton;
    }

    private void ConfirmButton()
    {
        mPanel.UIController.OpenPanel(mCreditPanel);
    }
}
