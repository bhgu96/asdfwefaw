﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PredatorStateHud : AHud
{
    [SerializeField]
    private GameObject mActorObject;

    private IActor mActor;
    //private NewGiant mPredetor;
    private Animator mAnimator;
    private RectTransform mRectTransform;

    private void Awake()
    {
        if(mActorObject.GetComponent<IActor>() != null)
        {
            mActor = mActorObject.GetComponent<IActor>();
        }

        //if(mActorObject.GetComponent<NewGiant>() != null)
        //{
        //    mPredetor = mActorObject.GetComponent<NewGiant>();
        //}

        mAnimator = this.GetComponent<Animator>();
        mRectTransform = this.GetComponent<RectTransform>();

        //StateManager.GamePauseHandler.AddEnterEvent(EPauseFlags.PAUSE, Deactivate);
        //StateManager.GamePauseHandler.AddEnterEvent(EPauseFlags.Play, Activate);
        //StateManager.GameOverHandler.AddEnterEvent(EGameOverFlags.GameOver, Deactivate);
        //StateManager.GameOverHandler.AddEnterEvent(EGameOverFlags.Play, Activate);
        //StateManager.GameStateHandler.AddEnterEvent(EGameStateFlags.Normal, Activate);
        //StateManager.GameStateHandler.AddExitEvent(EGameStateFlags.Normal, Deactivate);
        //StateManager.GameStateHandler.AddEnterEvent(EGameStateFlags.TUTORIAL_CURSE, Activate);
    }

    public override void Activate()
    {
        //if (StateManager.GameStateHandler.State == EGameStateFlags.Normal || StateManager.GameStateHandler.State == EGameStateFlags.TUTORIAL_CURSE)
        //{
        //    this.GetComponent<Image>().enabled = true;
        //}
    }

    public override void Deactivate()
    {
        this.GetComponent<Image>().enabled = false;
    }

    public override void Express()
    {
        ////피격
        //mAnimator.SetBool("isHit",mActor.State.IsHit);
        ////탈진
        //mAnimator.SetBool("isExhausted", StateManager.GiantStateHandler.State == EGiantStateFlags.DEAD || mActor.State.BehaviorFlag == EBehaviorFlags.DIE);
        ////초월
        //mAnimator.SetBool("isTransandense", mPredetor.Transcendence != NewGiant.ETranscendenceFlags.None);
    }

    public override void GetInformation()
    {

    }

    private void Animation_TransandenseStart()
    {
        mRectTransform.anchoredPosition = new Vector2(-554f, 384f);
        mRectTransform.sizeDelta = new Vector2(900f, 900f);
    }

    private void Animation_Transandenseing01()
    {
        mRectTransform.anchoredPosition = new Vector2(-554f, 339f);
    }

    private void Animation_Transandenseing02()
    {
        mRectTransform.anchoredPosition = new Vector2(-554f, 300f);
    }

    private void Animation_Transandenseing03()
    {
        mRectTransform.anchoredPosition = new Vector2(-554f, 274f);
    }

    private void Animation_Transandenseing04()
    {
        mRectTransform.anchoredPosition = new Vector2(-574f, 215f);
    }

    private void Animation_TransandenseEnd()
    {
        mRectTransform.anchoredPosition = new Vector2(-731f, 201f);
        mRectTransform.sizeDelta = new Vector2(500f, 500f);
    }

    private void OnDestroy()
    {
        //StateManager.GamePauseHandler.RemoveEnterEvent(EPauseFlags.PAUSE, Deactivate);
        //StateManager.GamePauseHandler.RemoveEnterEvent(EPauseFlags.Play, Activate);
        //StateManager.GameOverHandler.RemoveEnterEvent(EGameOverFlags.GameOver, Deactivate);
        //StateManager.GameOverHandler.RemoveEnterEvent(EGameOverFlags.Play, Activate);
        //StateManager.GameStateHandler.RemoveEnterEvent(EGameStateFlags.Normal, Activate);
        //StateManager.GameStateHandler.RemoveExitEvent(EGameStateFlags.Normal, Deactivate);
        //StateManager.GameStateHandler.RemoveEnterEvent(EGameStateFlags.TUTORIAL_CURSE, Activate);
    }
}
