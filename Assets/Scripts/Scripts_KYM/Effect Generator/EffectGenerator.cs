﻿//using System.Collections;
//using System.Collections.Generic;
//using UnityEngine;

//public class EffectGenerator : AEffectGenerator
//{
//    private EffectTriggerTable m_effectTable;
//    private Animator m_animator;

//    [SerializeField]
//    private bool m_isStartContinuousEffect;
//    [SerializeField]
//    private bool m_isFixed;

//    private void Awake()
//    {
//        m_effectTable = this.GetComponentInParent<EffectTriggerTable>();
//        m_animator = this.GetComponent<Animator>();
//    }

//    private void Start()
//    {
//        this.gameObject.SetActive(false);

//        if(m_isFixed)
//        {
//            this.transform.SetParent(null);
//        }
//    }

//    public override void GenerateEffect(Vector3 position)
//    {
//        this.gameObject.SetActive(true);
//        this.transform.position = position;
//        m_animator.SetBool("isEndAnimation", false);
//        m_animator.SetBool(m_effectTable.GetEffectName(Effect), true);
//    }

//    private void endEffect()
//    {
//        m_animator.SetBool("isEndAnimation", true);
//        m_animator.SetBool(m_effectTable.GetEffectName(Effect), false);
//        this.gameObject.SetActive(false);
//    }

//    private IEnumerator startContinuousEffect(float time)
//    {
//        if (!m_isStartContinuousEffect)
//        {
//            m_isStartContinuousEffect = true;
//            float continuousTime = 0.0f;

//            while (continuousTime <= time)
//            {
//                continuousTime += Time.deltaTime;
//                yield return null;
//            }

//            m_isStartContinuousEffect = false;
//            endEffect();
//        }
//    }
//}
