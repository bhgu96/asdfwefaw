﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// 발생할 Effect의 위치를 반환시켜주는 Table Class
/// </summary>
public class EffectPositionTable
{
    private float m_damageObject_Max_Bound_X;
    private float m_damageObject_Min_Bound_X;
    private float m_damageObject_Center_Bound_X;
    private float m_damageObject_Center_Bound_Y;

    private float m_hitObject_Max_Bound_X;
    private float m_hitObject_Min_Bound_X;
    private float m_hitObject_Center_Bound_X;
    private float m_hitObject_Center_Bound_Y;

    public EffectPositionTable(Collider2D damageObjectCollider, Collider2D hitObjectCollider)
    {
        m_damageObject_Max_Bound_X = damageObjectCollider.bounds.max.x;
        m_damageObject_Min_Bound_X = damageObjectCollider.bounds.min.x;
        m_damageObject_Center_Bound_X = damageObjectCollider.bounds.center.x;
        m_damageObject_Center_Bound_Y = damageObjectCollider.bounds.center.y;

        m_hitObject_Max_Bound_X = hitObjectCollider.bounds.max.x;
        m_hitObject_Min_Bound_X = hitObjectCollider.bounds.min.x;
        m_hitObject_Center_Bound_X = hitObjectCollider.bounds.center.x;
        m_hitObject_Center_Bound_Y = hitObjectCollider.bounds.center.y;
    }

    public Vector3 GetHitEffectPosition()
    {
        if (m_damageObject_Center_Bound_Y > m_hitObject_Center_Bound_Y)
        {
            return new Vector2(m_damageObject_Max_Bound_X - (m_damageObject_Max_Bound_X - m_hitObject_Min_Bound_X) * 0.5f,
                m_damageObject_Center_Bound_Y - (m_damageObject_Center_Bound_Y - m_hitObject_Center_Bound_Y) * 0.5f);
        }
        else
        {
            return new Vector2(m_damageObject_Max_Bound_X - (m_damageObject_Max_Bound_X - m_hitObject_Min_Bound_X) * 0.5f,
                m_damageObject_Center_Bound_Y + (m_damageObject_Center_Bound_Y - m_hitObject_Center_Bound_Y) * 0.5f);
        }
    }
}
