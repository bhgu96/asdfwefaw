﻿//using System.Collections;
//using System.Collections.Generic;
//using UnityEngine;

//public class EffectTriggerTable : MonoBehaviour
//{
//    private Dictionary<ETriggerFlags, AEffectGenerator> m_generateDict;
//    private Dictionary<EEffectObject, string> m_effectDict;

//    private void Awake()
//    {
//        m_generateDict = new Dictionary<ETriggerFlags, AEffectGenerator>();
//        m_effectDict = new Dictionary<EEffectObject, string>();

//        AEffectGenerator[] effectObjects = this.GetComponentsInChildren<AEffectGenerator>();

//        foreach(AEffectGenerator effectObject in effectObjects)
//        {
//            if(!m_generateDict.ContainsKey(effectObject.Trigger))
//            {
//                m_generateDict.Add(effectObject.Trigger, effectObject);
//            }

//            if(!m_effectDict.ContainsKey(effectObject.Effect))
//            {
//                m_effectDict.Add(effectObject.Effect, effectObject.Effect.ToString());
//            }
//        }
//    }

//    public AEffectGenerator GetEffectObject(ETriggerFlags trigger)
//    {
//        return m_generateDict[trigger];
//    }

//    public string GetEffectName(EEffectObject effect)
//    {
//        return m_effectDict[effect];
//    }
//}
