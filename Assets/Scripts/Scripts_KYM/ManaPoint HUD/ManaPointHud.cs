﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ManaPointHud : AHud
{
    private RectTransform mManaRectTransform;
    private Status_Fairy_Player mActorStatus;
    private float mMaxManaLength;
    private int mMaxMana;
    private int mCurMana;


    private void Awake()
    {
        mManaRectTransform = this.GetComponent<RectTransform>();
        mMaxManaLength = mManaRectTransform.sizeDelta.x;
    }

    private void Start()
    {
        if (Fairy_Player.Main != null)
        {
            mActorStatus = Fairy_Player.Main.Status;
            mMaxMana = (int)mActorStatus.MaxMana;
            mCurMana = (int)mActorStatus.Mana;
        }
        else
        {
            mMaxMana = (int)PrototypeFairy.Fairy.MaxMana;
            mCurMana = (int)PrototypeFairy.Fairy.CurMana;
        }
    }

    public override void Activate()
    {

    }

    public override void Deactivate()
    {

    }

    public override void Express()
    {
        mManaRectTransform.anchoredPosition = new Vector2(-(mMaxManaLength - mMaxManaLength * ((float)mCurMana / mMaxMana)), mManaRectTransform.anchoredPosition.y);
    }

    public override void GetInformation()
    {
        if(mActorStatus != null)
        {
            mMaxMana = (int)mActorStatus.MaxMana;
            mCurMana = (int)mActorStatus.Mana;
        }
        else
        {
            mMaxMana = (int)PrototypeFairy.Fairy.MaxMana;
            mCurMana = (int)PrototypeFairy.Fairy.CurMana;
        }

    }

    private void OnDestroy()
    {
        mActorStatus = null;
    }
}
