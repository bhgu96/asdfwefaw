﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ManaPointTextHud : AHud
{
    private Text mText;
    private Status_Fairy_Player mActorStatus;
    private float mMaxMana;
    private float mCurMana;


    private void Awake()
    {
        mText = this.GetComponent<Text>();
    }

    private void Start()
    {
        if (Fairy_Player.Main != null)
        {
            mActorStatus = Fairy_Player.Main.Status;
            mMaxMana = (int)mActorStatus.MaxMana;
            mCurMana = (int)mActorStatus.Mana;
        }
        else
        {
            mMaxMana = (int)PrototypeFairy.Fairy.MaxMana;
            mCurMana = (int)PrototypeFairy.Fairy.CurMana;
        }
    }

    public override void Activate()
    {

    }

    public override void Deactivate()
    {

    }

    public override void Express()
    {
        mText.text = mCurMana.ToString() + "/" + mMaxMana.ToString() + " " + "(" + ((mCurMana / mMaxMana) * 100).ToString() + "%" + ")";
    }

    public override void GetInformation()
    {
        if (mActorStatus != null)
        {
            mMaxMana = (int)mActorStatus.MaxMana;
            mCurMana = (int)mActorStatus.Mana;
        }
        else
        {
            mMaxMana = (int)PrototypeFairy.Fairy.MaxMana;
            mCurMana = (int)PrototypeFairy.Fairy.CurMana;
        }
    }

    private void OnDestroy()
    {
        mActorStatus = null;
    }
}
