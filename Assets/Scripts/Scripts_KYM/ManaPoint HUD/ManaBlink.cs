﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ManaBlink : MonoBehaviour
{
    public float ManaAlpha { get { return mManaAlpha; } set { mManaAlpha = value; } }
    public float BlinkTime { get { return mBlinkTime; } private set { } }
    public float BlinkSpeed { get { return mBlinkSpeed; } private set { } }
    private float mManaAlpha;
    [SerializeField]
    private float mBlinkTime;
    [SerializeField]
    private float mBlinkSpeed;
    [SerializeField]
    private List<Image> mManaMaskList;
    [SerializeField]
    private List<ManaPointMaskHud> mManaPointMaskList;

    private void Start()
    {
        StartCoroutine(Blink());
    }

    private IEnumerator Blink()
    {
        while(true)
        {
            for (float i = 0.1f; i <= 1; i += Time.deltaTime * mBlinkSpeed)
            {
                if (mManaPointMaskList[0].IsMasking)
                {
                    mManaMaskList[0].color = new Color(1, 1, 1, i);
                }

                if (mManaPointMaskList[1].IsMasking)
                {
                    mManaMaskList[1].color = mManaMaskList[0].color;
                }

                if (mManaPointMaskList[2].IsMasking)
                {
                    mManaMaskList[2].color = mManaMaskList[1].color;
                }

                if (mManaPointMaskList[3].IsMasking)
                {
                    mManaMaskList[3].color = mManaMaskList[2].color;
                }

                if (mManaPointMaskList[4].IsMasking)
                {
                    mManaMaskList[4].color = mManaMaskList[3].color;
                }

                yield return null;
            }

            yield return new WaitForSeconds(mBlinkTime);

            for (float i = 1f; i >= 0.1f; i -= Time.deltaTime * mBlinkSpeed)
            {
                if (mManaPointMaskList[0].IsMasking)
                {
                    mManaMaskList[0].color = new Color(1, 1, 1, i);
                }

                if (mManaPointMaskList[1].IsMasking)
                {
                    mManaMaskList[1].color = mManaMaskList[0].color;
                }

                if (mManaPointMaskList[2].IsMasking)
                {
                    mManaMaskList[2].color = mManaMaskList[1].color;
                }

                if (mManaPointMaskList[3].IsMasking)
                {
                    mManaMaskList[3].color = mManaMaskList[2].color;
                }

                if (mManaPointMaskList[4].IsMasking)
                {
                    mManaMaskList[4].color = mManaMaskList[3].color;
                }

                yield return null;
            }

            yield return new WaitForSeconds(mBlinkTime);
        }
    }
}
