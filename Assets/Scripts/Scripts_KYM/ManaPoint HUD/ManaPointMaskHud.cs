﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ManaPointMaskHud : AHud
{
    public bool IsMasking { get { return mIsMasking; } private set { } }

    [SerializeField]
    private int mMaskIndex;

    private ManaBlink mManaBlink;
    private RectTransform mMaskRectTransform;
    private Image mMaskImage;
    private Status_Fairy_Player mActorStatus;
    private float mMaxMana;
    private float mCurMana;
    private bool mIsMasking;


    private void Awake()
    {
        mManaBlink = this.GetComponentInParent<ManaBlink>();
        mMaskRectTransform = this.GetComponent<RectTransform>();
        mMaskImage = this.GetComponent<Image>();
    }

    private void Start()
    {
        //if (NewGiant.Instance != null)
        //{
        //    mActorStatus = NewGiant.Instance.Status;
        //}

        //mMaxMana = mActorStatus.MaximumManaPoint;
        //mCurMana = mActorStatus.CurrentManaPoint;
    }

    public override void Activate()
    {

    }

    public override void Deactivate()
    {

    }

    public override void Express()
    {
        if(mCurMana < mMaxMana - mMaxMana * 0.2f * mMaskIndex)
        {
            //마스크 사라짐
            mMaskImage.color = new Color(1, 1, 1, 0);
            mIsMasking = false;
        }
        else
        {
            //마스크 채워짐
            if (!mIsMasking)
            {
                mIsMasking = true;
            }
        }
    }

    public override void GetInformation()
    {
        //mMaxMana = mActorStatus.MaximumManaPoint;
        //mCurMana = mActorStatus.CurrentManaPoint;
    }

    private void OnDestroy()
    {
        mActorStatus = null;
    }
}
