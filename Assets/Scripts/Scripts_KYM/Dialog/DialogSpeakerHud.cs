﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DialogSpeakerHud : AHud
{
    private Text mSpeakerLabel;
    private string mCurrentSpeaker;

    private void Awake()
    {
        mSpeakerLabel = this.GetComponent<Text>();
    }

    public override void Activate()
    {

    }

    public override void Deactivate()
    {

    }

    public override void Express()
    {
        if(mCurrentSpeaker != null)
        {
            mSpeakerLabel.text = mCurrentSpeaker;
        }
    }

    public override void GetInformation()
    {
        mCurrentSpeaker = DialogText.SpeakerText;
    }
}
