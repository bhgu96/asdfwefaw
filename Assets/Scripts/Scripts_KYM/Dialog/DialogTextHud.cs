﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DialogTextHud : AHud
{
    [SerializeField]
    private GameObject mTextIndicator;
    [SerializeField]
    private string[] mCurrentText;

    private bool mIsReadText;
    private Text mTextLabel;

    private void Awake()
    {
        mTextLabel = this.GetComponent<Text>();
    }

    public override void Activate()
    {

    }

    public override void Deactivate()
    {

    }

    public override void Express()
    {
        if (mTextLabel.enabled)
        {
            StartCoroutine(ExpressText());
        }
    }

    public override void GetInformation()
    {
        if(DialogText.ReadText != null)
        {
            mCurrentText = DialogText.ReadText.Split('/', '\n');
        }
    }

    private IEnumerator ExpressText()
    {
        if(mIsReadText)
        {
            yield break;
        }

        if(mCurrentText.Length == 0)
        {
            mTextLabel.text = null;
            mTextIndicator.SetActive(true);
            yield return new WaitUntil(() => InputManager.GetBool(EInputType.Confirm));
            mTextIndicator.SetActive(false);
            yield break;
        }

        mIsReadText = true;

        string tempString = mCurrentText[0];
        mTextLabel.text = null;
        yield return null;

        for (int i = 0; i < mCurrentText.Length; i++)
        {
            for(int j = 0; j < mCurrentText[i].Length; j++)
            {
                //갑자기 다른 대사가 치고 들어올때 대사 다시 초기화
                if(tempString != mCurrentText[0])
                {
                    j = 0;
                    tempString = mCurrentText[0];
                    mTextLabel.text = null;
                }

                mTextLabel.text += mCurrentText[i][j];
                yield return new WaitForSecondsRealtime(DialogText.DialogTime);
            }

            yield return null;
            mTextIndicator.SetActive(true);
            yield return new WaitUntil(() => InputManager.GetBool(EInputType.Confirm));
            mTextIndicator.SetActive(false);
            mTextLabel.text = null;
        }

        mIsReadText = false;

        DialogText.EndDialogue();
    }
}
