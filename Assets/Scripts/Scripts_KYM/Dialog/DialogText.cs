﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DialogText : MonoBehaviour
{
    private const float DEFAULT_DIALOGUE_TIME = 0.05f;

    public static string ReadText { get { return mReadText; } private set { } }
    public static string SpeakerText { get { return mSpeakerText; } private set { } }
    public static float DialogTime { get { return mDialogTime; } private set { } }
    public static bool IsEndDialogue { get { return mIsEndDialogue; } private set { } }

    private static string mReadText;
    private static string mSpeakerText;
    private static float mDialogTime;

    private static bool mIsEndDialogue;

    public static void GetCurrentText(string speakerText, string currentDialogue)
    {
        //다이얼로그 시작 이벤트 State로 변경
        mIsEndDialogue = false;

        mSpeakerText = speakerText;
        mReadText = currentDialogue;
        mDialogTime = DEFAULT_DIALOGUE_TIME;
    }

    public static void GetCurrentText(string speakerText, string currentDialogue, float dialogTime)
    {
        //다이얼로그 시작 이벤트 State로 변경
        mIsEndDialogue = false;

        mSpeakerText = speakerText;
        mReadText = currentDialogue;
        mDialogTime = dialogTime;
    }

    public static void EndDialogue()
    {
        //다이얼로그 종료 이벤트 State로 변경
        mIsEndDialogue = true;
    }
}
