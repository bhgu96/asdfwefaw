﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class SetCameraTransform : MonoBehaviour
{
    [SerializeField]
    private Transform mCameraTransform;
    [SerializeField]
    private GameObject mWallLightCamera;

    private void Start()
    {
        this.transform.parent = mCameraTransform;
        this.transform.localPosition = Vector3.zero;

        StartCoroutine(SyncCamera());
    }

    IEnumerator SyncCamera()
    {
        mWallLightCamera.SetActive(false);
        yield return null;
        mWallLightCamera.SetActive(true);
    }
}
