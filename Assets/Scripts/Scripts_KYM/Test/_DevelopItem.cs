﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class _DevelopItem : MonoBehaviour
{
    public CameraController mCameraController;

    private void Start()
    {
        mCameraController.BlackOut(5.5f, 0);
    }

    void Update()
    {
        if(Input.GetKeyDown(KeyCode.F1))
        {
            //머터리얼 0 30개씩 삽입
            AItem item = ItemPool.Get(EItemType.TestMaterial_0);
            int count = Fairy_Player.Main.Inventory.PutIn(item, 30, out item);
            SlotController.CheckInventorySlotCount(EItemCategory.Material);
        }
        else if(Input.GetKeyDown(KeyCode.F2))
        {
            //머터리얼 1 40개씩 삽입
            AItem item = ItemPool.Get(EItemType.TestMaterial_1);
            int count = Fairy_Player.Main.Inventory.PutIn(item, 40, out item);
            SlotController.CheckInventorySlotCount(EItemCategory.Material);
        }
        else if(Input.GetKeyDown(KeyCode.F3))
        {
            AItem item = ItemPool.Get(EItemType.TestSoulLight_0);
            int count = Fairy_Player.Main.Inventory.PutIn(item, 1, out item);
            SlotController.CheckInventorySlotCount(EItemCategory.Soulite);
        }
        else if(Input.GetKeyDown(KeyCode.F4))
        {
            AItem item = ItemPool.Get(EItemType.TestSoulLight_1);
            int count = Fairy_Player.Main.Inventory.PutIn(item, 1, out item);
            SlotController.CheckInventorySlotCount(EItemCategory.Soulite);
        }

    }
}
