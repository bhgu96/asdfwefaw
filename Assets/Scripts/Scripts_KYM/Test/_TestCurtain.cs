﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class _TestCurtain : MonoBehaviour
{
    public GameObject curtain;
    public Vector3 position;
    public bool isCutain;
    public List<GameObject> testList;

    private void Awake()
    {
        testList = new List<GameObject>();
        position = this.transform.position;
    }

    private void Update()
    {
        if(this.transform.position != position)
        {
            StartCoroutine(Test());
        }

        position = this.transform.position;
    }

    IEnumerator Test()
    {
        if(isCutain)
        {
            yield break;
        }

        testList.Add(Instantiate(curtain));

        if(testList.Count >= 2)
        {
            Destroy(testList[testList.Count - 2]);
        }

        testList[testList.Count -1].transform.position = this.transform.position;
        yield return null;
        isCutain = true;
        yield return new WaitForSeconds(0.2f);

        //if (this.transform.position == position)
        //{
        //    for (int i = 0; i < testList.Count; i++)
        //    {
        //        Destroy(testList[i]);
        //    }
        //    testList.Clear();
        //}

        //if (testList.Count >= 3)
        //{
        //    testList.Clear();
        //}

        isCutain = false;
    }
}
