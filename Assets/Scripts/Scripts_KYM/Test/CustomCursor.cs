﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CustomCursor : MonoBehaviour
{
    //[SerializeField]
    //private bool isTitle;

    //public static bool misChangedResolution = false;
    //public static float xCorrection;
    //public static float yCorrection;
    //public static float mDefaultWidth;
    //public static float mDefaultHeight;

    //private static float xStoreCorrection;
    //private static float yStoreCorrection;
    //private static bool mIsTitle;

    //private void Awake()
    //{
    //    mDefaultWidth = Screen.width;
    //    mDefaultHeight = Screen.height;

    //    xCorrection = mDefaultWidth;
    //    yCorrection = mDefaultHeight;

    //    xStoreCorrection = xCorrection;
    //    yStoreCorrection = yCorrection;

    //    mIsTitle = isTitle;
    //}

    [SerializeField]
    //커서를 일반 윈도우 마우스 커서로 바꾸고 싶을때(에디터용)
    private bool mIsDefaultCursor = false;
    [SerializeField]
    private Texture2D[] mMouseTextures;

    private float mCurrentTimes = 0.0f;
    private int mIndex = 0;

    private void Update()
    {
        mCurrentTimes += Time.unscaledDeltaTime;

        if(mCurrentTimes >= 0.1f)
        {
            mIndex++;

            if (mIndex >= mMouseTextures.Length)
            {
                mIndex = 0;
            }

            Cursor.SetCursor(mMouseTextures[mIndex], new Vector2(16, 16), CursorMode.Auto);
            mCurrentTimes = 0.0f;
        }

        if(mIsDefaultCursor)
        {
            Cursor.SetCursor(null, new Vector2(16, 16), CursorMode.Auto);
        }

        //if (misChangedResolution)
        //{
        //    this.transform.position = Camera.main.ScreenToWorldPoint(new Vector3(Input.mousePosition.x * (mDefaultWidth / xCorrection), Input.mousePosition.y * (mDefaultHeight / yCorrection), +10f));
        //}
        //else
        //{
        //    this.transform.position = Camera.main.ScreenToWorldPoint(new Vector3(Input.mousePosition.x, Input.mousePosition.y, +10f));
        //}
    }

    public static void VisibleCursor()
    {
        Cursor.visible = true;
    }

    public static void InvisibleCursor()
    {
        Cursor.visible = false;
    }

    //public static void SetMousePosition(float x, float y)
    //{
    //    if(!mIsTitle)
    //    {
    //        xCorrection = x;
    //        yCorrection = y;
    //    }

    //    misChangedResolution = true;

    //    xStoreCorrection = x;
    //    yStoreCorrection = y;
    //}

    //private void OnDestroy()
    //{
    //    misChangedResolution = false;
    //}
}
