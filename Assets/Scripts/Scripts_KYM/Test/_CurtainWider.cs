﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class _CurtainWider : MonoBehaviour
{
    [SerializeField]
    private GameObject spirit;
    [SerializeField]
    private Transform curtainSmaller;
    [SerializeField]
    private float smoother;

    private SpriteRenderer spriteRenderer;

    private void Awake()
    {
        spriteRenderer = this.GetComponent<SpriteRenderer>();
    }

    private void Start()
    {
        this.transform.SetParent(null);
    }

    private void LateUpdate()
    {
        if(spirit.activeSelf)
        {
            spriteRenderer.color = new Color(1, 1, 1, 1);
            this.transform.position = Vector3.Lerp(this.transform.position, curtainSmaller.position, smoother * Time.deltaTime);
        }
        else
        {
            spriteRenderer.color = new Color(1, 1, 1, 0);
        }
    }
}
