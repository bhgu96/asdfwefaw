﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class _TestTextr : MonoBehaviour
{
    public string speaker;
    public string text;

    private void Start()
    {
        DialogText.GetCurrentText(speaker, text, 0.1f);
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Alpha1))
        {
            DialogText.GetCurrentText("주희진", "밥을 먹여 달랴라라라라라라라라라라라라라라라라라라라라라라라라라라/" +
                "배가 고프구나/" +
                "냄새 씁하씁하/" +
                "떼뜨");
        }

        if (Input.GetKeyDown(KeyCode.Alpha2))
        {
            DialogText.GetCurrentText(speaker, text, 0.1f);
        }
    }
}
