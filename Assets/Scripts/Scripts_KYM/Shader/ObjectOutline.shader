﻿Shader "Custom/ObjectOutline"
{
	Properties
	{
		_MainTex("DarkShadow", 2D) = "white" {}
		_OutlineColor("OutlineColor", Color) = (1,0,0,1)
		_OutlineThreadhold("OutlineThreadhold", Int) = 1
	}
		SubShader
	{
		Tags{ "Queue" = "Transparent" "RenderType" = "Transparent" }
		LOD 100

		Blend SrcAlpha OneMinusSrcAlpha

		Pass
		{
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag

			#include "UnityCG.cginc"

			struct appdata
			{
				float4 vertex : POSITION;
				float2 uv : TEXCOORD0;
			};

			struct v2f
			{
				float2 uv : TEXCOORD0;
				UNITY_FOG_COORDS(1)
				float4 vertex : SV_POSITION;
			};

			sampler2D _MainTex;
			float4 _MainTex_ST;
			float4 _MainTex_TexelSize;
			fixed4 _OutlineColor;
			int _OutlineThreadhold;

			v2f vert(appdata v)
			{
				v2f o;
				o.vertex = UnityObjectToClipPos(v.vertex);
				o.uv = TRANSFORM_TEX(v.uv, _MainTex);
				return o;
			}

			fixed4 frag(v2f i) : SV_Target
			{
				float2 t_uv = _MainTex_TexelSize.xy;
				fixed2 offsetUV;
				fixed4 mainTexColor = tex2D(_MainTex, i.uv);
				fixed addAlpha = 0;
				int alpha = 0;

				if (_OutlineThreadhold > 0 && mainTexColor.a == 0)
				{
					// Get the neighbouring four pixels.
					for (int k = -1; k <= 1; k++)
					{
						for (int j = -1; j <= 1; j++)
						{
							offsetUV = fixed2(t_uv.x * k, t_uv.x * j);
							addAlpha += tex2D(_MainTex, (i.uv + offsetUV)).a;
						}
					}

					// If one of the neighbouring pixels is invisible, we render an outline.
					// 0 <= addAlpha : 1
					alpha = step(0.9f, addAlpha);
				}

				_OutlineColor.a *= alpha;
				int threadhold = alpha;
				fixed4 col = mainTexColor + _OutlineColor * threadhold;
				return col;
			}
			ENDCG
		}
	}
}
