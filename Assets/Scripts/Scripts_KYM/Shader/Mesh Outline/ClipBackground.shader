﻿Shader "Custom/ClipBackground"
{
	Properties
	{
		_MainTex("Albedo (RGB)", 2D) = "white" {}
	}

		SubShader
	{
		Tags{ "Queue" = "Transparent" "RenderType" = "Transparent" }
		LOD 100
		ZWrite Off
		Blend SrcAlpha OneMinusDstAlpha

		Pass
	{
		CGPROGRAM
		#pragma vertex vert
		#pragma fragment frag

		sampler2D _MainTex, _SourceTex;

	struct appdata
	{
		float4 vertex : POSITION;
		float2 uv : TEXCOORD0;
	};

	struct v2f
	{
		float2 uv : TEXCOORD0;
		float4 vertex : SV_POSITION;
	};

	v2f vert(appdata v)
	{
		v2f o;
		o.vertex = UnityObjectToClipPos(v.vertex);
		o.uv = v.uv;
		return o;
	}


	float4 frag(v2f i) : SV_TARGET
	{
		float4 lightTexture = tex2D(_MainTex, i.uv);

		//if (lightTexture.a < 0.1)
		//{
		//	float4 bloom = lightTexture;
		//	return bloom;
		//}
		//else
		//{
		//	discard;
		//}

		return lightTexture;
	}
		ENDCG
	}
	}
}
