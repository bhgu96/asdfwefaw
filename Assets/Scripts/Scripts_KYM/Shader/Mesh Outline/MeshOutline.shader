﻿Shader "Custom/MeshOutline"
{
	Properties
	{
		_MainTex("Main Texture", 2D) = "white"{}
		_BackTex("Back Texture", 2D) = "white" {}
		_Color("Color", Color) = (1,1,1,1)
		_Thickness("Thickness", float) = 1
	}

		SubShader
		{
			Tags{ "Queue" = "Transparent" "RenderType" = "Transparent" }

			Cull Off
			Blend SrcAlpha OneMinusSrcAlpha

			Pass
			{
				CGPROGRAM
				#pragma vertex vert
				#pragma fragment frag
				#include "UnityCG.cginc"

				sampler2D _MainTex;
				sampler2D _BackTex;
				fixed4 _Color;
				float4 _MainTex_TexelSize;
				float _Thickness;
				
				
				struct v2f
				{
					float4 pos : SV_POSITION;
					half2 uv : TEXCOORD0;
				};

				v2f vert(appdata_base v)
				{
					v2f o;
					o.pos = UnityObjectToClipPos(v.vertex);
					o.uv = v.texcoord;
					return o;
				}

				fixed4 frag(v2f i) : COLOR
				{
					float4 t = tex2D(_MainTex, i.uv);
					float4 b = tex2D(_BackTex, i.uv);


					if (t.a > 0)
					{
						discard;
					}

					float2 t_uv = _MainTex_TexelSize.xy;
					fixed2 offsetUV;
					fixed4 mainTexColor = tex2D(_MainTex, i.uv);
					fixed addAlpha = 0;
					int alpha = 0;

					if (mainTexColor.a == 0)
					{
						for (int k = -1; k <= 1; k++)
						{
							for (int j = -1; j <= 1; j++)
							{
								offsetUV = fixed2(t_uv.x * k * _Thickness, t_uv.x * j * _Thickness);
								addAlpha += tex2D(_MainTex, (i.uv + offsetUV)).a;
							}
						}

						alpha = step(1, addAlpha);
					}

					_Color.a *= alpha;
					fixed4 col = mainTexColor + _Color * alpha;
					return col;


					return b;

					//half4 c = tex2D(_MainTex, i.uv);
					//c.rgb *= c.a;

					//half4 outlineColor = _Color;
					//outlineColor.a *= ceil(c.a);

					//outlineColor.rgb *= outlineColor.a;


					//fixed upAlpha = tex2D(_MainTex, i.uv + fixed2(0, _MainTex_TexelSize.y * _Thickness)).a;
					//fixed downAlpha = tex2D(_MainTex, i.uv - fixed2(0, _MainTex_TexelSize.y * _Thickness)).a;
					//fixed rightAlpha = tex2D(_MainTex, i.uv + fixed2(_MainTex_TexelSize.x * _Thickness, 0)).a;
					//fixed leftAlpha = tex2D(_MainTex, i.uv - fixed2(_MainTex_TexelSize.x * _Thickness, 0)).a;
					//
					//return lerp(outlineColor, c, ceil(upAlpha * downAlpha * rightAlpha * leftAlpha));
				}
				ENDCG
		}
	}
}
