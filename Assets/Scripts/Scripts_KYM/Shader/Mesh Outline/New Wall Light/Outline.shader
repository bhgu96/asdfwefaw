﻿Shader "Custom/Outline"
{
	Properties
	{
		_MainTex("Main Texture", 2D) = "white" {}
		_Color("Color", Color) = (1,1,1,1)
		
		_OutlineTex("Outline Texture", 2D) = "white"{}
		_OutlineColor("Outline Color", Color) = (1,1,1,1)
		_OutlineWidth("Outline Width", Range(1.0, 10.0)) = 1.0
	}

	SubShader
	{
		Tags {"Queue" = "Transparent"}

		Pass
		{
			Name "Outline"

			ZWrite Off

			CGPROGRAM

			#pragma vertex vert
			#pragma fragment frag
			
			#include "UnityCG.cginc"

			struct appdata
			{
				float4 vertex : POSITION;
				float2 uv : TEXCOORD0;
			};

			struct v2f
			{
				float4 pos : SV_POSITION;
				float uv : TEXCOORD0;
			};

			sampler2D _OutlineTex;
			float4 _OutlineColor;
			float _OutlineWidth;

			v2f vert(appdata IN)
			{
				IN.vertex.xyz *= _OutlineWidth;
				v2f o;
				o.pos = UnityObjectToClipPos(IN.vertex);
				o.uv = IN.uv;

				return o;
			}

			fixed4 frag(v2f IN) : SV_Target
			{
				float4 texColor = tex2D(_OutlineTex, IN.uv);
				return texColor * _OutlineColor;
			}

				ENDCG
		}

		Pass
		{
			Name "Object"

			CGPROGRAM

			#pragma vertex vert
			#pragma fragment frag

			#include "UnityCG.cginc"

			struct appdata
			{
				float4 vertex : POSITION;
				float2 uv : TEXCOORD0;
			};

			struct v2f
			{
				float4 pos : SV_POSITION;
				float uv : TEXCOORD0;
			};

			sampler2D _Maintex;
			float4 _Color;

			v2f vert(appdata IN)
			{
				v2f o;
				o.pos = UnityObjectToClipPos(IN.vertex);
				o.uv = IN.uv;

				return o;
			}

			fixed4 frag(v2f IN) : SV_Target
			{
				float4 texColor = tex2D(_Maintex, IN.uv);
				return texColor * _Color;
			}

			ENDCG
		}
	}
}
