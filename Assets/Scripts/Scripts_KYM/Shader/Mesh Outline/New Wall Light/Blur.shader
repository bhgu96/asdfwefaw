﻿Shader "Custom/Blur"
{
	Properties
	{
		_TerrainTex("Terrain Texture", 2D) = "white"{}
		_MainTex("Main Texture", 2D)= "white"{}
		_BlurRadius("Blur Radius", Range(0.0, 20.0)) = 1
		_Intensity("Blur Intensity", Range(0.0, 1.0)) = 0.0
	}

	SubShader
	{
		Tags { "Queue" = "Transparent+4" "RenderType" = "Transparent" }
		Blend SrcAlpha OneMinusSrcAlpha
		GrabPass {}

		Pass
		{
			Name "Horizontal Blur"

			CGPROGRAM

			#pragma vertex vert
			#pragma fragment frag
			
			#include "UnityCG.cginc"

			struct v2f
			{
				float4 vertex : SV_POSITION;
				float4 uvgrab : TEXCOORD0;
			};

			sampler2D _TerrainTex;
			sampler2D _MainTex;
			sampler2D _GrabTexture;
			float4 _GrabTexture_TexelSize;
			float _Intensity;
			float _BlurRadius;

			v2f vert(appdata_base IN)
			{
				v2f o;
				o.vertex = UnityObjectToClipPos(IN.vertex);

				#if UNITY_UV_BeginningS_AT_TOP
					float scale = -1.0;
				#else
					float scale = 1.0;
				#endif

				o.uvgrab.xy = (float2(o.vertex.x, o.vertex.y * scale) + o.vertex.w) * 0.5;

				return o;
			}

			half4 GrabPixel(float weight, float kernalx, v2f IN)
			{
				return tex2D(_GrabTexture, float4(IN.uvgrab.x + _GrabTexture_TexelSize.x * kernalx * _BlurRadius, IN.uvgrab.y, IN.uvgrab.z, IN.uvgrab.w)) * weight;
			}

			half4 frag(v2f IN) :COLOR
			{
				half4 mainTex = tex2D(_MainTex, IN.uvgrab);
				half4 terrainTex = tex2D(_TerrainTex, IN.uvgrab);

				half4 texcol = tex2D(_GrabTexture, IN.uvgrab);
				half4 texsum = half4(0, 0, 0, 0);

				//if (terrainTex.a < 0.1)
				//{
				//	texsum += GrabPixel(0.06, -4.0, IN);
				//	texsum += GrabPixel(0.09, -3.0, IN);
				//	texsum += GrabPixel(0.12, -2.0, IN);
				//	texsum += GrabPixel(0.15, -1.0, IN);
				//	texsum += GrabPixel(0.18, 0.0, IN);
				//	texsum += GrabPixel(0.15, +1.0, IN);
				//	texsum += GrabPixel(0.12, +2.0, IN);
				//	texsum += GrabPixel(0.09, +3.0, IN);
				//	texsum += GrabPixel(0.06, +4.0, IN);

				//	texcol = lerp(texcol, texsum, _Intensity);

				//	return texcol;
				//}
				//else
				//{
				//	return terrainTex;
				//}

				texsum += GrabPixel(0.06, -4.0, IN);
				texsum += GrabPixel(0.09, -3.0, IN);
				texsum += GrabPixel(0.12, -2.0, IN);
				texsum += GrabPixel(0.15, -1.0, IN);
				texsum += GrabPixel(0.18, 0.0, IN);
				texsum += GrabPixel(0.15, +1.0, IN);
				texsum += GrabPixel(0.12, +2.0, IN);
				texsum += GrabPixel(0.09, +3.0, IN);
				texsum += GrabPixel(0.06, +4.0, IN);

				texcol = lerp(texcol, texsum, _Intensity);

				return texcol;
			}

			ENDCG
		}

		GrabPass{}

		Pass
		{
			Name "Vertical Blur"

			CGPROGRAM

			#pragma vertex vert
			#pragma fragment frag

			#include "UnityCG.cginc"

			struct v2f
			{
				float4 vertex : SV_POSITION;
				float4 uvgrab : TEXCOORD0;
			};

			sampler2D _TerrainTex;
			sampler2D _MainTex;
			sampler2D _GrabTexture;
			float4 _GrabTexture_TexelSize;
			float _Intensity;
			float _BlurRadius;

			v2f vert(appdata_base IN)
			{
				v2f o;
				o.vertex = UnityObjectToClipPos(IN.vertex);

				#if UNITY_UV_BeginningS_AT_TOP
					float scale = -1.0;
				#else
					float scale = 1.0;
				#endif

				o.uvgrab.xy = (float2(o.vertex.x, o.vertex.y * scale) + o.vertex.w) * 0.5;
				return o;
			}

			half4 GrabPixel(float weight, float kernaly, v2f IN)
			{
				return tex2D(_GrabTexture, float4(IN.uvgrab.x, IN.uvgrab.y + _GrabTexture_TexelSize.y * kernaly * _BlurRadius, IN.uvgrab.z, IN.uvgrab.w)) * weight;
			}

			half4 frag(v2f IN) :COLOR
			{
				half4 mainTex = tex2D(_MainTex, IN.uvgrab);
				half4 terrainTex = tex2D(_TerrainTex, IN.uvgrab);

				half4 texcol = tex2D(_GrabTexture, IN.uvgrab);
				half4 texsum = half4(0, 0, 0, 0);

				//if (terrainTex.a < 0.1)
				//{
				//	texsum += GrabPixel(0.06, -4.0, IN);
				//	texsum += GrabPixel(0.09, -3.0, IN);
				//	texsum += GrabPixel(0.12, -2.0, IN);
				//	texsum += GrabPixel(0.15, -1.0, IN);
				//	texsum += GrabPixel(0.18, 0.0, IN);
				//	texsum += GrabPixel(0.15, +1.0, IN);
				//	texsum += GrabPixel(0.12, +2.0, IN);
				//	texsum += GrabPixel(0.09, +3.0, IN);
				//	texsum += GrabPixel(0.06, +4.0, IN);

				//	texcol = lerp(texcol, texsum, _Intensity);

				//	return texcol;
				//}
				//else
				//{
				//	return terrainTex;
				//}

				texsum += GrabPixel(0.06, -4.0, IN);
				texsum += GrabPixel(0.09, -3.0, IN);
				texsum += GrabPixel(0.12, -2.0, IN);
				texsum += GrabPixel(0.15, -1.0, IN);
				texsum += GrabPixel(0.18, 0.0, IN);
				texsum += GrabPixel(0.15, +1.0, IN);
				texsum += GrabPixel(0.12, +2.0, IN);
				texsum += GrabPixel(0.09, +3.0, IN);
				texsum += GrabPixel(0.06, +4.0, IN);

				texcol = lerp(texcol, texsum, _Intensity);

				return texcol;
			}

			ENDCG
		}
	}
}
