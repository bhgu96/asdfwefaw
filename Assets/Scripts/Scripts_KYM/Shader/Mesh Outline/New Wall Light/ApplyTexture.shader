﻿Shader "Custom/ApplyTexture"
{
	Properties
	{
		_MainTex("Main Texture", 2D) = "white" {}
		_Color("Color", Color) = (1,1,1,1)
	}

	SubShader
	{
		Pass
		{
			CGPROGRAM

			#pragma vertex vert
			#pragma fragment frag

			#include "UnityCG.cginc"

			struct appdata
			{
				float4 vertex : POSITION;
				float2 uv : TEXCOORD0;
			};

			struct v2f
			{
				float4 pos : SV_POSITION;
				float uv : TEXCOORD0;
			};

			sampler2D _Maintex;
			float4 _Color;

			v2f vert(appdata IN)
			{
				v2f o;
				o.pos = UnityObjectToClipPos(IN.vertex);
				o.uv = IN.uv;

				return o;
			}

			fixed4 frag(v2f IN) : SV_Target
			{
				float4 texColor = tex2D(_Maintex, IN.uv);
				return texColor * _Color;
			}

			ENDCG
		}
	}
}
