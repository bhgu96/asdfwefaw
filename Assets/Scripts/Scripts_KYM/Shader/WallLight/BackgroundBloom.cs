﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BackgroundBloom : MonoBehaviour
{
    private const int BOX_Down_PREFILTER_PASS = 0;
    private const int BOX_Down_PASS = 1;
    private const int APPLY_BLOOM_PASS = 2;

    [SerializeField]
    private Shader mBloomShader;
    [SerializeField]
    private Material mOriginLightMaterial;

    private Material mBloomMaterial;

    [SerializeField]
    [Range(1, 16)]
    private int mIteration = 1;

    [SerializeField]
    [Range(0, 10)]
    private float mIntensity = 1;

    [SerializeField]
    private Color mColor;

    //[SerializeField]
    [Range(1, 10)]
    private int mThreshold = 1;

    //[SerializeField]
    [Range(0, 1)]
    private float mSoftThreshold = 0.5f;

    //3번째 호출
    private void OnRenderImage(RenderTexture source, RenderTexture destination)
    {
        if (mBloomMaterial == null)
        {
            mBloomMaterial = new Material(mBloomShader);
            mBloomMaterial.hideFlags = HideFlags.HideAndDontSave;
        }

        float knee = mThreshold * mSoftThreshold;
        Vector4 filter;
        filter.x = mThreshold;
        filter.y = filter.x - knee;
        filter.z = 2f * knee;
        filter.w = 0.25f / (knee + 0.0001f);

        mBloomMaterial.SetVector("_Fileter", filter);
        mBloomMaterial.SetFloat("_Intensity", Mathf.GammaToLinearSpace(mIntensity));
        mOriginLightMaterial.SetColor("_Color", mColor);

        int width = source.width / 2;
        int height = source.height / 2;
        RenderTextureFormat format = source.format;

        RenderTexture currentDestination = RenderTexture.GetTemporary(width, height, 0, format);

        Graphics.Blit(source, currentDestination, mBloomMaterial, BOX_Down_PREFILTER_PASS);
        RenderTexture currentSource = currentDestination;
        RenderTexture.ReleaseTemporary(currentDestination);

        int i = 1;

        for (; i < mIteration; i++)
        {
            width /= 2;
            height /= 2;

            if (height < 2)
            {
                break;
            }

            currentDestination = RenderTexture.GetTemporary(width, height, 0, format);

            Graphics.Blit(currentSource, currentDestination, mBloomMaterial, BOX_Down_PASS);
            RenderTexture.ReleaseTemporary(currentDestination);
            currentSource = currentDestination;
        }

        mBloomMaterial.SetTexture("_SourceTex", currentSource);
        Graphics.Blit(currentSource, destination, mBloomMaterial, APPLY_BLOOM_PASS);
        RenderTexture.ReleaseTemporary(currentSource);
    }
}
