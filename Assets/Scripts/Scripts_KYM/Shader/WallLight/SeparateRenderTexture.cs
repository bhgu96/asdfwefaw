﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SeparateRenderTexture : MonoBehaviour
{
    [SerializeField]
    private RenderTexture backgroundTexture;
    [SerializeField]
    private RenderTexture terrainTex;
    [SerializeField]
    private bool isBackground;

    private void OnRenderImage(RenderTexture source, RenderTexture destination)
    {
        if(isBackground)
        {
            Graphics.Blit(source, backgroundTexture);
        }
        else
        {
            Graphics.Blit(source, terrainTex);
            Graphics.Blit(source, destination);
        }
    }
}
