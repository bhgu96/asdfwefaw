﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LightRenderTexture : MonoBehaviour
{
    [SerializeField]
    private RenderTexture mRenderTexture;

    private Camera mCamera;

    private void Awake()
    {
        mCamera = this.GetComponent<Camera>();
    }

    private void Update()
    {
        mCamera.Render();

    }
}
