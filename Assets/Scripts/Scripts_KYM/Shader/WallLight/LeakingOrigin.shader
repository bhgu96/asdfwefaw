﻿Shader "Custom/LeakingOrigin"
{
	Properties
	{
		_Color("Color", Color) = (1,1,1,1)
		_MainTex("Main Texture", 2D) = "white" {}
		_BackTex("Back Texture", 2D) = "white" {}
	}

	SubShader
	{
		Tags{ "Queue" = "Transparent+1001" "RenderType" = "Transparent" }
		LOD 100
		ZWrite Off
		Blend SrcAlpha OneMinusSrcAlpha

		Pass
		{
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag

			sampler2D _MainTex;
			sampler2D _BackTex;
			float4 _Color;

			struct appdata
			{
				float4 vertex : POSITION;
				float2 uv : TEXCOORD0;
			};

			struct v2f
			{
				float2 uv : TEXCOORD0;
				float4 vertex : SV_POSITION;
			};

			v2f vert(appdata v)
			{
				v2f o;
				o.vertex = UnityObjectToClipPos(v.vertex);
				o.uv = v.uv;
				return o;
			}

			float4 frag(v2f i) : COLOR
			{
				float4 t = tex2D(_MainTex, i.uv);
				float4 b = tex2D(_BackTex, i.uv);

				//t.a = 1 - t.a - .1f;
				//clip(t.a);

				if (t.a < 0.1)
				{
					return b;
				}
				else
				{
					discard;
				}

				return b;
			}
			ENDCG
	    }
	}
}
