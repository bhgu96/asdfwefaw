﻿Shader "Custom/MergeTextures"
{
    Properties
    {
		_EmptyTex("EmptyTex", 2D) = "white"{}
		_BackgroundTex("BackgroundTex", 2D) = "white"{}
		_TerrainTex("TerrainTex", 2D) = "white"{}
		_ObjectTex("ObjectTex", 2D) = "white"{}
    }
    SubShader
    {
        Tags { "RenderType"="Opaque" }
		 
		Pass
		{
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag

			sampler2D _EmptyTex;
			sampler2D _BackgroundTex;
			sampler2D _TerrainTex;
			sampler2D _ObjectTex;

			struct appdata
			{
				float4 vertex : POSITION;
				float2 uv : TEXCOORD0;
			};

			struct v2f
			{
				float2 uv : TEXCOORD0;
				float4 vertex : SV_POSITION;
			};

			v2f vert(appdata v)
			{
				v2f o;
				o.vertex = UnityObjectToClipPos(v.vertex);
				o.uv = v.uv;
				return o;
			}

			float4 frag(v2f i) : COLOR
			{
				float4 empty = tex2D(_EmptyTex, i.uv);
				float4 background = tex2D(_BackgroundTex, i.uv);
				float4 terrain = tex2D(_TerrainTex, i.uv);
				float4 object = tex2D(_ObjectTex, i.uv);

				float3 merge;

				merge = background.rgb * (1 - terrain.a) + terrain.rgb*(terrain.a);
				merge = merge * (1 - object.a) + object.rgb*(object.a);

				return float4(merge.rgb, 1);
			}
			ENDCG
		}
    }
}
