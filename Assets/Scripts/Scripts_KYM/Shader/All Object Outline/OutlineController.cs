﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//[ExecuteInEditMode]
public class OutlineController : MonoBehaviour
{
    public int Toggle { get { return mToggle; } set { mToggle = value; } }

    [SerializeField]
    private Color mOutlineColor; //외곽선 색깔
    [SerializeField]
    [Range(0, 1)]
    private int mToggle = 0; //외곽선 활성/비활성화(0 : 비활성, 1 : 활성)

    private SpriteRenderer mSpriteRenderer;
    private Material mOutlineMaterial;

    private void Awake()
    {
        mSpriteRenderer = this.GetComponent<SpriteRenderer>();
        mOutlineMaterial = mSpriteRenderer.material;
    }

    private void Update()
    {
        mOutlineMaterial.SetColor("_OutlineColor", mOutlineColor);
        mOutlineMaterial.SetInt("_IsActiveOutline", mToggle);
    }

    //마우스가 해당 객체의 콜라이더 안에 들어가 있을때(1번 호출)
    private void OnMouseEnter()
    {
        mToggle = 1;
    }

    //마우스가 해당 객체의 콜라이더 밖으로 나갔을때(1번 호출)
    private void OnMouseExit()
    {
        mToggle = 0;
    }
}
