﻿Shader "Custom/SpriteOutlineShader"
{
	Properties
	{
		_MainTex("Main Texture", 2D) = "white" {}
		_OutlineColor("Outline Color", Color) = (1,1,1,1)
		[MaterialToggle]_IsActiveOutline("Active Outline", int) = 0
	}

	SubShader
	{
		Tags{ "Queue" = "Transparent""RenderType" = "Transparent" }

		Blend SrcAlpha OneMinusSrcAlpha

		CGPROGRAM
		#pragma surface surf Lambert alpha

		struct Input
		{
			float2 uv_MainTex;
			fixed4 color : COLOR;
		};

		sampler2D _MainTex;
		float4 _OutlineColor;
		int _IsActiveOutline;
		float4 _MainTex_TexelSize;


		void surf(Input IN, inout SurfaceOutput o)
		{
			if (_IsActiveOutline == 1)
			{
				fixed4 tempColor = tex2D(_MainTex, IN.uv_MainTex + float2(_MainTex_TexelSize.x, 0.0)) + tex2D(_MainTex, IN.uv_MainTex - float2(_MainTex_TexelSize.x, 0.0));

				tempColor = tempColor + tex2D(_MainTex, IN.uv_MainTex + float2(0.0, _MainTex_TexelSize.y)) + tex2D(_MainTex, IN.uv_MainTex - float2(0.0, _MainTex_TexelSize.y));

				fixed4 alphaColor = (0, 0, 0, tempColor.a);
				fixed4 mainColor = alphaColor * _OutlineColor.rgba;
				fixed4 addcolor = tex2D(_MainTex, IN.uv_MainTex) * IN.color;

				if (addcolor.a > 0.95)
				{
					mainColor = addcolor;
				}

				o.Albedo = mainColor.rgb;
				o.Alpha = mainColor.a;
			}
			else
			{
				fixed4 mainColor = tex2D(_MainTex, IN.uv_MainTex);

				o.Albedo = mainColor.rgb;
				o.Alpha = mainColor.a;
			}
		}
		ENDCG
	}
}

