﻿Shader "Custom/StencilObject"
{
    Properties
    {
		_SpiritPosition("SpiritPosition", Vector) = (0,0,0,0)
        _Color ("Color", Color) = (1,1,1,1)
        _MainTex ("Albedo (RGB)", 2D) = "white" {}
		_MinDistance("MinDistance", Range(0,30)) = 0 //최적값 : 0
		_MaxDistance("MaxDistance", Range(0,30)) = 0 //최적값 : 18.4
		_MinAlpha("MinAlpha", Range(0,1)) = 0 //최적값 : 0.64
		_MaxAlpha("MaxAlpha", Range(0,1)) = 0 //최적값 : 0
    }
    SubShader
    {	
		Tags{ "Queue" = "Transparent+1" "RenderType" = "Transparent" }
        LOD 200

		Stencil
		{
			Ref 2
			Comp NotEqual
			Pass Keep
		}

        CGPROGRAM
		#pragma surface surf Lambert alpha

        sampler2D _MainTex;
		fixed4 _Color;
		float4 _SpiritPosition;
		float _distanceAlpha;
		float _MinDistance;
		float _MaxDistance;
		float _MinAlpha;
		float _MaxAlpha;

        struct Input
        {
			float2 uv_MainTex;
			float3 worldPos;
        };

        void surf (Input IN, inout SurfaceOutput o)
        {
			float4 c = tex2D(_MainTex, IN.uv_MainTex) * _Color;
			float dist = distance(_SpiritPosition, IN.worldPos);
	
			dist = saturate((dist - _MinDistance) / (_MaxDistance - _MinDistance));
			o.Alpha = _MinAlpha + (_MaxAlpha - _MinAlpha) * dist;
			o.Albedo = c.rgb;
        }
        ENDCG
    }
}
