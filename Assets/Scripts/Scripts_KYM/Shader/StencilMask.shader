﻿Shader "Custom/StencilMask"
{
	Properties
	{
		_MainTex("MainTex", 2D) = "white"{}
	}

	SubShader
	{
		Tags { "Queue" = "Transparent+2" "RenderType" = "Opaque"}

		ColorMask 0
		ZWrite off

		Stencil
		{
			Ref 2
			Comp Always
			Pass Replace
		}

		Pass
		{
			Cull Back
			ZTest Less

		
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag

			sampler2D _MainTex;

			struct appdata
			{
				float4 vertex : POSITION;
				float2 uv : TEXCOORD0;
			};

			struct v2f
			{
				float2 uv : TEXCOORD0;
				float4 vertex : SV_POSITION;
			};

			v2f vert(appdata v)
			{
				v2f o;
				o.vertex = UnityObjectToClipPos(v.vertex);
				o.uv = v.uv;
				return o;
			}

			half4 frag(v2f i) : COLOR
			{				
				half t = tex2D(_MainTex, i.uv).g;
				clip(t - 0.01);
				return half4(t,0,0,0);
			}
			ENDCG
		}

	}
}
