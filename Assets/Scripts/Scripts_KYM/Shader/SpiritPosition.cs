﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpiritPosition : MonoBehaviour
{
    [SerializeField]
    private Transform mSpiritTransform;

    Material mShadowMaterial;

    private void Awake()
    {
        mShadowMaterial = this.GetComponent<MeshRenderer>().material;
    }

    private void LateUpdate()
    {
        mShadowMaterial.SetVector("_SpiritPosition", mSpiritTransform.position);

    }
}
