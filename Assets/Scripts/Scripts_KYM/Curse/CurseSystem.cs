﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CurseSystem
{
    private bool m_isTerm;
    public bool IsTerm { get { return m_isTerm; } private set { } }

    private float m_termTime;

    private float m_maxTermTime = 1f; //텀을 주는 시간
    public float MaxTermTime { get { return m_maxTermTime; } private set{ } }

    private Status_Giant_Player mActorStatus;
    private IActor mActor;

    /// <summary>
    /// 대기 시간. 저주가 발동하고 대기시간을 갖는다.
    /// </summary>
    public float TermTime
    {
        get
        {
            return m_termTime;
        }
        set
        {
            m_termTime = value;

            if(m_termTime >= m_maxTermTime)
            {
                m_isTerm = false;
                m_curCurseCount = 0;
            }
        }
    }

    /// <summary>
    /// 스택 1개당 활성화되는 시간(ex 5초) 5초가 지나면 스택이 +1 된다.
    /// </summary>
    [SerializeField]
    private float m_curseTime;
    public float CurseTime {get { return m_curseTime; } set { m_curseTime = value; } }

    /// <summary>
    /// 최대로 쌓을 수 있는 저주 스택 개수
    /// </summary>
    [SerializeField]
    private int m_maxCurseCount;
    public int MaxCurseCount { get { return m_maxCurseCount; } set { m_maxCurseCount = value;  m_curseTime = 0; } }

    /// <summary>
    /// 저주 시스템을 활성화/비활성화 할것인지 체크함
    /// </summary>
    [SerializeField]
    private bool m_isActive = false;
    public bool IsActive
    {
        get
        {
            return m_isActive;
        }
        set
        {
            m_isActive = value;

            if (!m_isActive)
            {
                m_curCurseTime = 0;
                m_curCurseCount = 0;
                m_termTime = 0;
                m_isTerm = false;
            }
        }
    }

    /// <summary>
    /// 현재 경과된 저주 스택 활성화 시간
    /// </summary>
    [SerializeField]
    private float m_curCurseTime;
    public float CurCurseTime
    {
        get
        {
            return m_curCurseTime;
        }
        set
        {
            m_termTime = 0.0f;
            m_curCurseTime = value;

            if(m_curCurseTime >= m_curseTime)
            {
                CurCurseCount++;
            }
        }
    }

    /// <summary>
    /// 현재 저주 스택 개수
    /// </summary>
    [SerializeField]
    private int m_curCurseCount;
    public int CurCurseCount
    {
        get
        {
            return m_curCurseCount;
        }
        set
        {
            m_curCurseTime = 0;
            m_curCurseCount = value;
            m_curCurseCount = Mathf.Clamp(m_curCurseCount, 0, m_maxCurseCount);

            if(m_curCurseCount == m_maxCurseCount)
            {
                m_isTerm = true;
                //저주 데미지를 받고 일정 시간의 텀을 갖는다.
            }
        }
    }

    public CurseSystem(IActor actor)
    {
        mActor = actor;
        //mActorStatus = actor.Status;

        //m_curseTime = mActorStatus.TimeOfActivatingCurse;
        //m_maxCurseCount = mActorStatus.MaximumNumberOfCurseStack;
    }

    /// <summary>
    /// 저주 활성화하는 함수.
    /// </summary>
    public void Curse()
    {
        if(IsActive)
        {
            if (m_isTerm)
            {
                TermTime += Time.deltaTime;
            }
            else
            {
                CurCurseTime += Time.deltaTime;
            }
        }
    }

    /// <summary>
    /// 일시적인 버프, 또는 디버프가 끝나거나 그 외의 경우에 있어서, 원래 Actor에 할당된 Status값으로 초기화할때 사용
    /// </summary>
    public void InitField()
    {
        //m_curseTime = mActorStatus.TimeOfActivatingCurse;
        //m_maxCurseCount = mActorStatus.MaximumNumberOfCurseStack;
    }

    /// <summary>
    /// 저주 시스템에 의해 데미지를 줌. HUD 애니메이션에서 터지고 채워질때 호출됨
    /// IDamagable을 통해서 감소가 아닌 이 클래스 기능으로 현재 생명력을 감소시킨다.
    /// </summary>
    public void Damaged()
    {
        //최대 체력의 25% 감소(health scaler에 맞춤)
        //mActor.Damage.Value = mActorStatus.MaximumHealthPoint * 0.25f;
        //mActor.Damage.Stiffness = 0;
        //mActor.Damage.SrcPosition = Vector3.zero;

        //mActorStatus.CurrentHealthPoint -= (int)mActor.Damage.Value;

        //if (m_actorStatus.CurrentHealthPoint <= 0)
        //{
        //    m_actorState.BehaviorFlag = EBehaviorFlags.DIED;
        //    m_actor.Damage.IsCurseDied = true;
        //}
        //else
        //{
        //    m_actor.Damage.IsCurseDied = false;
        //}
    }
}
