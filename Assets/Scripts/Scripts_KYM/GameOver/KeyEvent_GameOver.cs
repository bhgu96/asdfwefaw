﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KeyEvent_GameOver : MonoBehaviour
{
    [SerializeField]
    private UIController mUIController;
    //[SerializeField]
    //private NewGiant mPredetor;

    private void Update()
    {
        if(StateManager.GameOver.State)
        {
            if (Input.GetKeyDown(KeyCode.Return))
            {
                ///게임 다시 시작
                GameOverHud.mIsGameOver = false;
                //게임 다시 시작 호출
                //mPredetor.Revive();
                //메뉴 닫기
                mUIController.ClosePanel();

            }
            else if (Input.GetKeyDown(KeyCode.Escape))
            {
                //타이틀로 돌아가기
                StateManager.Pause.State = false;
                StateManager.GameOver.State = false;

                //StateManager.GamePauseHandler.State = EPauseFlags.Play;
                //StateManager.GameOverHandler.State = EGameOverFlags.Play;
                GameOverHud.mIsGameOver = false;
                ScoreData.Save();
                LoadingController.LoadScene("Scene_Title");
            }
        }


        //if(StateManager.GameOverHandler.State == EGameOverFlags.GameOver)
        //{
        //    if(Input.GetKeyDown(KeyCode.Return))
        //    {
        //        ///게임 다시 시작
        //        GameOverHud.mIsGameOver = false;
        //        //게임 다시 시작 호출
        //        //mPredetor.Revive();
        //        //메뉴 닫기
        //        mUIController.ClosePanel();
                
        //    }
        //    else if(Input.GetKeyDown(KeyCode.Escape))
        //    {
        //        //타이틀로 돌아가기
        //        StateManager.GamePauseHandler.State = EPauseFlags.Play;
        //        StateManager.GameOverHandler.State = EGameOverFlags.Play;
        //        GameOverHud.mIsGameOver = false;
        //        ScoreData.Save();
        //        LoadingController.LoadScene("Scene_Title");
        //    }
        //}
    }
}
