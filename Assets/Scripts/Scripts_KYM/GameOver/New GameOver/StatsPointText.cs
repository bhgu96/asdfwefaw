﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class StatsPointText : MonoBehaviour
{
    public ECharacterType CharacterType { get { return mCharacterType; } set { mCharacterType = value; } }

    private ECharacterType mCharacterType;
    private Text mText;

    private void Awake()
    {
        mText = this.GetComponent<Text>();
    }

    private void Update()
    {
        mText.text = "남은 스탯 : " + PlayerData.Promotion;
    }
}
