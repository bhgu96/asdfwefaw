﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ShopCurrencyText : MonoBehaviour
{
    // 현재 인벤토리를 참조

    // 만약 인벤토리의 소재의 개수 이상을 갖고 있으면 소재 글씨 파란색(?) -> 사짐
    // 만약 인벤토리의 소재의 개수 미만을 갖고 있으면 소재 글씨 빨간색(?) -> 안사짐

    [SerializeField]
    private EItemType mType;
    [SerializeField]
    private int mCurrency00;
    [SerializeField]
    private int mCurrency01;

    private Text mText;
    private AItem mItem;

    private void Awake()
    {
        mText = this.GetComponent<Text>();
    }

    private void Update()
    {
         
    }
}
