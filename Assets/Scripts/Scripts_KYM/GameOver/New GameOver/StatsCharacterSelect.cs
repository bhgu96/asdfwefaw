﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class StatsCharacterSelect : MonoBehaviour
{
    [SerializeField]
    private GameObject mGiantStats;
    [SerializeField]
    private GameObject mFairyStats;

    [SerializeField]
    private ECharacterType mCharacterType;
    [SerializeField]
    private Animator mStatsAnimator;

    private UIButton mButton;

    private void Awake()
    {
        mButton = this.GetComponent<UIButton>();
        mButton.onConfirm += ChangeCharacter;
    }

    private void ChangeCharacter()
    {      
        if(mCharacterType == ECharacterType.Giant)
        {
            mGiantStats.SetActive(true);
            mFairyStats.SetActive(false);
            mStatsAnimator.SetBool("IsFairyStats", false);
        }
        else if(mCharacterType == ECharacterType.Fairy)
        {
            mFairyStats.SetActive(true);
            mGiantStats.SetActive(false);
            mStatsAnimator.SetBool("IsFairyStats", true);
        }
    }
}
