﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class NewGameOverHud : AHud
{
    [SerializeField]
    private RectTransform mFlashEffectTransform;
    [SerializeField]
    private Text mGameOverText;
    [SerializeField]
    private Text mBlinkText;
    [SerializeField]
    private GameObject mStatsMenu;

    private bool mIsGameOver = false;
    private bool mIsFinishFadeOut = false;

    public override void Activate()
    {

    }

    public override void Deactivate()
    {

    }

    public override void Express()
    {
        if(!mIsGameOver && StateManager.GameOver.State)
        {
            Time.timeScale = 0.0f;
            mGameOverText.text = "Game Over";
            mIsGameOver = true;
            StartCoroutine(ActivateGameOver());
        }
        else if(!mIsGameOver && StateManager.GameClear.State)
        {
            Time.timeScale = 0.0f;
            mGameOverText.text = "Game Clear";
            mIsGameOver = true;
            StartCoroutine(ActivateGameOver());
        }
    }

    public override void GetInformation()
    {

    }

    private IEnumerator ActivateGameOver()
    {
        mFlashEffectTransform.gameObject.SetActive(true);

        yield return new WaitForSecondsRealtime(0.05f);

        mFlashEffectTransform.sizeDelta = new Vector2(1500f, 100f);

        yield return new WaitForSecondsRealtime(0.05f);

        mFlashEffectTransform.sizeDelta = new Vector2(1920, 300f);

        yield return new WaitForSecondsRealtime(0.05f);

        mFlashEffectTransform.gameObject.SetActive(false);
        mGameOverText.gameObject.SetActive(true);

        StartCoroutine(FadeOut());
        yield return new WaitUntil(() => mIsFinishFadeOut);

        mBlinkText.gameObject.SetActive(true);

        yield return new WaitUntil(() => Input.anyKeyDown);

        mStatsMenu.SetActive(true);
    }

    private IEnumerator FadeOut()
    {
        for(float a = 0; a <= 1; a += Time.unscaledDeltaTime)
        {
            this.GetComponent<Image>().color = new Color(0, 0, 0, a);
            yield return null;
        }

        mIsFinishFadeOut = true;
    }
}
