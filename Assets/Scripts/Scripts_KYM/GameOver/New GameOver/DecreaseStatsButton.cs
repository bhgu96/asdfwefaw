﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DecreaseStatsButton : MonoBehaviour
{
    private UIButton mButton;
    private StatsTextMenu mStatsTextMenu;

    private void Awake()
    {
        mButton = this.GetComponent<UIButton>();
        mStatsTextMenu = this.GetComponentInParent<StatsTextMenu>();
        mButton.onConfirm += DecreaseStats;
    }

    private void DecreaseStats()
    {
        if (mStatsTextMenu.CharacterType == ECharacterType.Giant)
        {
            switch (mStatsTextMenu.IncreaseStats)
            {
                case EIncreaseStats.MaxHP:
                    PlayerData.SetAbility_Promotion(EFloatAbility.MaxLife, PlayerData.GetAbility_Promotion(EFloatAbility.MaxLife) - 1);
                    break;
                case EIncreaseStats.HPRecovery:
                    PlayerData.SetAbility_Promotion(EFloatAbility.LifeRps, PlayerData.GetAbility_Promotion(EFloatAbility.LifeRps) - 1);
                    break;
                case EIncreaseStats.Movement:
                    PlayerData.SetAbility_Promotion(EFloatAbility.MoveSpeed_Giant, PlayerData.GetAbility_Promotion(EFloatAbility.MoveSpeed_Giant) - 1);
                    break;
                case EIncreaseStats.Casting:
                    PlayerData.SetAbility_Promotion(EFloatAbility.CastSpeed_Giant, PlayerData.GetAbility_Promotion(EFloatAbility.CastSpeed_Giant) - 1);
                    break;
                case EIncreaseStats.Exocism:
                    PlayerData.SetAbility_Promotion(EIntAbility.Exorcism, PlayerData.GetAbility_Promotion(EIntAbility.Exorcism) - 1);
                    break;
                case EIncreaseStats.CriticalChance:
                    PlayerData.SetAbility_Promotion(EFloatAbility.CriticalChance_Giant, PlayerData.GetAbility_Promotion(EFloatAbility.CriticalChance_Giant) - 1);
                    break;
                case EIncreaseStats.CriticalMag:
                    PlayerData.SetAbility_Promotion(EFloatAbility.CriticalMag_Giant, PlayerData.GetAbility_Promotion(EFloatAbility.CriticalMag_Giant) - 1);
                    break;
                case EIncreaseStats.Kinetic:
                    PlayerData.SetEA_Giant_Promotion(EElementalType.Kinematic, PlayerData.GetEA_Giant_Promotion(EElementalType.Kinematic) - 1);
                    break;
                case EIncreaseStats.Arcane:
                    PlayerData.SetEA_Giant_Promotion(EElementalType.Arcane, PlayerData.GetEA_Giant_Promotion(EElementalType.Arcane) - 1);
                    break;
                case EIncreaseStats.Fire:
                    PlayerData.SetEA_Giant_Promotion(EElementalType.Fire, PlayerData.GetEA_Giant_Promotion(EElementalType.Fire) - 1);
                    break;
                case EIncreaseStats.Water:
                    PlayerData.SetEA_Giant_Promotion(EElementalType.Water, PlayerData.GetEA_Giant_Promotion(EElementalType.Water) - 1);
                    break;
                case EIncreaseStats.Wind:
                    PlayerData.SetEA_Giant_Promotion(EElementalType.Wind, PlayerData.GetEA_Giant_Promotion(EElementalType.Wind) - 1);
                    break;
                case EIncreaseStats.Earth:
                    PlayerData.SetEA_Giant_Promotion(EElementalType.Earth, PlayerData.GetEA_Giant_Promotion(EElementalType.Earth) - 1);
                    break;
                case EIncreaseStats.Light:
                    PlayerData.SetEA_Giant_Promotion(EElementalType.Light, PlayerData.GetEA_Giant_Promotion(EElementalType.Light) - 1);
                    break;
                case EIncreaseStats.Darkness:
                    PlayerData.SetEA_Giant_Promotion(EElementalType.Darkness, PlayerData.GetEA_Giant_Promotion(EElementalType.Darkness) - 1);
                    break;
            }
        }
        else if (mStatsTextMenu.CharacterType == ECharacterType.Fairy)
        {
            switch (mStatsTextMenu.IncreaseStats)
            {
                case EIncreaseStats.MaxMana:
                    PlayerData.SetAbility_Promotion(EFloatAbility.MaxMana, PlayerData.GetAbility_Promotion(EFloatAbility.MaxMana) - 1);
                    break;
                case EIncreaseStats.ManaRecovery:
                    PlayerData.SetAbility_Promotion(EFloatAbility.ManaRps, PlayerData.GetAbility_Promotion(EFloatAbility.ManaRps) - 1);
                    break;
                case EIncreaseStats.Movement:
                    PlayerData.SetAbility_Promotion(EFloatAbility.MoveSpeed_Fairy, PlayerData.GetAbility_Promotion(EFloatAbility.MoveSpeed_Fairy) - 1);
                    break;
                case EIncreaseStats.Casting:
                    PlayerData.SetAbility_Promotion(EFloatAbility.CastSpeed_Fairy, PlayerData.GetAbility_Promotion(EFloatAbility.CastSpeed_Fairy) - 1);
                    break;
                case EIncreaseStats.Purification:
                    PlayerData.SetAbility_Promotion(EIntAbility.Purification, PlayerData.GetAbility_Promotion(EIntAbility.Purification) - 1);
                    break;
                case EIncreaseStats.CriticalMag:
                    PlayerData.SetAbility_Promotion(EFloatAbility.CriticalMag_Fairy, PlayerData.GetAbility_Promotion(EFloatAbility.CriticalMag_Fairy) - 1);
                    break;
                case EIncreaseStats.CriticalChance:
                    PlayerData.SetAbility_Promotion(EFloatAbility.CriticalChance_Fairy, PlayerData.GetAbility_Promotion(EFloatAbility.CriticalChance_Fairy) - 1);
                    break;
                case EIncreaseStats.Kinetic:
                    PlayerData.SetEA_Fairy_Promotion(EElementalType.Kinematic, PlayerData.GetEA_Fairy_Promotion(EElementalType.Kinematic) - 1);
                    break;
                case EIncreaseStats.Arcane:
                    PlayerData.SetEA_Fairy_Promotion(EElementalType.Arcane, PlayerData.GetEA_Fairy_Promotion(EElementalType.Arcane) - 1);
                    break;
                case EIncreaseStats.Fire:
                    PlayerData.SetEA_Fairy_Promotion(EElementalType.Fire, PlayerData.GetEA_Fairy_Promotion(EElementalType.Fire) - 1);
                    break;
                case EIncreaseStats.Water:
                    PlayerData.SetEA_Fairy_Promotion(EElementalType.Water, PlayerData.GetEA_Fairy_Promotion(EElementalType.Water) - 1);
                    break;
                case EIncreaseStats.Wind:
                    PlayerData.SetEA_Fairy_Promotion(EElementalType.Wind, PlayerData.GetEA_Fairy_Promotion(EElementalType.Wind) - 1);
                    break;
                case EIncreaseStats.Earth:
                    PlayerData.SetEA_Fairy_Promotion(EElementalType.Earth, PlayerData.GetEA_Fairy_Promotion(EElementalType.Earth) - 1);
                    break;
                case EIncreaseStats.Light:
                    PlayerData.SetEA_Fairy_Promotion(EElementalType.Light, PlayerData.GetEA_Fairy_Promotion(EElementalType.Light) - 1);
                    break;
                case EIncreaseStats.Darkness:
                    PlayerData.SetEA_Fairy_Promotion(EElementalType.Darkness, PlayerData.GetEA_Fairy_Promotion(EElementalType.Darkness) - 1);
                    break;
            }
        }
    }
}
