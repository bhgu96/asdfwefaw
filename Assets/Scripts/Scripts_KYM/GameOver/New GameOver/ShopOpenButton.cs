﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ShopOpenButton : MonoBehaviour
{
    [SerializeField]
    private GameObject mShopObject;
    [SerializeField]
    private GameObject mInventoryObject;
    [SerializeField]
    private GameObject mStatsObject;
    [SerializeField]
    private RectTransform mShopTransform;

    private UIButton mButton;

    private void Awake()
    {
        mButton = this.GetComponent<UIButton>();

        mButton.onConfirm += OpenShop;
    }

    private void OpenShop()
    {
        mInventoryObject.transform.SetParent(mShopTransform);
        mInventoryObject.GetComponent<RectTransform>().localScale = new Vector3(1, 1, 1);
        mInventoryObject.GetComponent<RectTransform>().anchoredPosition = new Vector2(0, 0);
        mInventoryObject.GetComponent<InventoryChildObjectHandler>().Activate();

        mShopObject.SetActive(true);
        mInventoryObject.SetActive(true);
        mStatsObject.SetActive(false);
    }
}
