﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShopExitButton : MonoBehaviour
{
    [SerializeField]
    private GameObject mShopObject;
    [SerializeField]
    private GameObject mInventoryObject;
    [SerializeField]
    private GameObject mStatsObject;

    private UIButton mButton;

    private void Awake()
    {
        mButton = this.GetComponent<UIButton>();

        mButton.onConfirm += CloseShop;
    }

    private void CloseShop()
    {
        mShopObject.SetActive(false);
        mInventoryObject.SetActive(false);
        mStatsObject.SetActive(true);
    }
}
