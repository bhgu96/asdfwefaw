﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class StatsTextMenu : MonoBehaviour
{
    public EIncreaseStats IncreaseStats { get { return mIncreaseStats; } private set { } }
    public ECharacterType CharacterType { get { return mCharacterType; } set { mCharacterType = value; } }

    [SerializeField]
    private EIncreaseStats mIncreaseStats;
    [SerializeField]
    private ECharacterType mCharacterType;
    private Text mText;
    private GameObject mIncreaseButton;

    private void Awake()
    {
        mText = this.GetComponent<Text>();
        mIncreaseButton = this.transform.GetChild(0).gameObject;
    }

    private void Update()
    {
        int promotion = PlayerData.Promotion;
        
        if (promotion <= 0)
        {
            mIncreaseButton.SetActive(false);
        }
        else if (promotion > 0)
        {
            mIncreaseButton.SetActive(true);
        }

        ExpressStats(mCharacterType);
    }

    private void ExpressStats(ECharacterType characterType)
    {
        SetStatsText(characterType);
    }

    private void SetStatsText(ECharacterType type)
    {
        switch (mIncreaseStats)
        {
            case EIncreaseStats.MaxMana:
                if (type == ECharacterType.Fairy)
                {
                    mText.text = "최대 마력 : " + PlayerData.GetAbility_Promotion(EFloatAbility.MaxMana) + " => " + PlayerData.GetAbility(EFloatAbility.MaxMana);
                }
                break;
            case EIncreaseStats.ManaRecovery:
                if (type == ECharacterType.Fairy)
                {
                    mText.text = "초당 마나 회복량 : " + PlayerData.GetAbility_Promotion(EFloatAbility.ManaRps) + " => " + PlayerData.GetAbility(EFloatAbility.ManaRps);
                }
                break;
            case EIncreaseStats.MaxHP:
                if (type == ECharacterType.Giant)
                {
                    mText.text = "최대 생명력 : " + PlayerData.GetAbility_Promotion(EFloatAbility.MaxLife) + " => " + PlayerData.GetAbility(EFloatAbility.MaxLife);
                }
                break;
            case EIncreaseStats.HPRecovery:
                if (type == ECharacterType.Giant)
                {
                    mText.text = "초당 생명력 회복량 : " + PlayerData.GetAbility_Promotion(EFloatAbility.LifeRps) + " => " + PlayerData.GetAbility(EFloatAbility.LifeRps);
                }
                break;
            case EIncreaseStats.Movement:
                if (type == ECharacterType.Fairy)
                {
                    mText.text = "이동속도 증가율(%) : " + PlayerData.GetAbility_Promotion(EFloatAbility.MoveSpeed_Fairy) + " => " + Mathf.Round(PlayerData.GetAbility(EFloatAbility.MoveSpeed_Fairy) * 100.0f);
                }
                else if (type == ECharacterType.Giant)
                {
                    mText.text = "이동속도 증가율(%) : " + PlayerData.GetAbility_Promotion(EFloatAbility.MoveSpeed_Giant) + " => " + Mathf.Round(PlayerData.GetAbility(EFloatAbility.MoveSpeed_Giant) * 100.0f);
                }
                break;
            case EIncreaseStats.Casting:
                if (type == ECharacterType.Fairy)
                {
                    mText.text = "시전속도 증가율(%) : " + PlayerData.GetAbility_Promotion(EFloatAbility.CastSpeed_Fairy) + " => " + Mathf.Round(PlayerData.GetAbility(EFloatAbility.CastSpeed_Fairy) * 100.0f);
                }
                else if (type == ECharacterType.Giant)
                {
                    mText.text = "시전속도 증가율(%) : " + PlayerData.GetAbility_Promotion(EFloatAbility.CastSpeed_Giant) + " => " + Mathf.Round(PlayerData.GetAbility(EFloatAbility.CastSpeed_Giant) * 100.0f);
                }
                break;
            case EIncreaseStats.Purification:
                mText.text = "정화력 : " + PlayerData.GetAbility_Promotion(EIntAbility.Purification);
                break;
            case EIncreaseStats.Exocism:
                mText.text = "항마력 : " + PlayerData.GetAbility_Promotion(EIntAbility.Exorcism);
                break;
            case EIncreaseStats.CriticalChance:
                if (type == ECharacterType.Fairy)
                {
                    mText.text = "치명타 확률(%) : " + PlayerData.GetAbility_Promotion(EFloatAbility.CriticalChance_Fairy) + "=>" + Mathf.Round(PlayerData.GetAbility(EFloatAbility.CriticalChance_Fairy) * 100.0f);
                }
                else if(type == ECharacterType.Giant)
                {
                    mText.text = "치명타 확률(%) : " + PlayerData.GetAbility_Promotion(EFloatAbility.CriticalChance_Giant) + "=>" + Mathf.Round(PlayerData.GetAbility(EFloatAbility.CriticalChance_Giant) * 100.0f);
                }
                break;
            case EIncreaseStats.CriticalMag:
                if (type == ECharacterType.Fairy)
                {
                    mText.text = "치명타 확률(%) : " + PlayerData.GetAbility_Promotion(EFloatAbility.CriticalMag_Fairy) + "=>" + Mathf.Round(PlayerData.GetAbility(EFloatAbility.CriticalMag_Fairy) * 100.0f);
                }
                else if (type == ECharacterType.Giant)
                {
                    mText.text = "치명타 확률(%) : " + PlayerData.GetAbility_Promotion(EFloatAbility.CriticalMag_Giant) + "=>" + Mathf.Round(PlayerData.GetAbility(EFloatAbility.CriticalMag_Giant) * 100.0f);
                }
                break;
            case EIncreaseStats.Kinetic:
                if (type == ECharacterType.Fairy)
                {
                    mText.text = "물리 속성 친화도 : " + PlayerData.GetEA_Fairy_Promotion(EElementalType.Kinematic);
                }
                else if (type == ECharacterType.Giant)
                {
                    mText.text = "물리 속성 친화도 : " + PlayerData.GetEA_Giant_Promotion(EElementalType.Kinematic);
                }
                break;
            case EIncreaseStats.Arcane:
                if (type == ECharacterType.Fairy)
                {
                    mText.text = "신비 속성 친화도 : " + PlayerData.GetEA_Fairy_Promotion(EElementalType.Arcane);
                }
                else if (type == ECharacterType.Giant)
                {
                    mText.text = "신비 속성 친화도 : " + PlayerData.GetEA_Giant_Promotion(EElementalType.Arcane);
                }
                break;
            case EIncreaseStats.Fire:
                if (type == ECharacterType.Fairy)
                {
                    mText.text = "화 속성 친화도 : " + PlayerData.GetEA_Fairy_Promotion(EElementalType.Fire);
                }
                else if (type == ECharacterType.Giant)
                {
                    mText.text = "화 속성 친화도 : " + PlayerData.GetEA_Giant_Promotion(EElementalType.Fire);
                }
                break;
            case EIncreaseStats.Water:
                if (type == ECharacterType.Fairy)
                {
                    mText.text = "수 속성 친화도 : " + PlayerData.GetEA_Fairy_Promotion(EElementalType.Water);
                }
                else if (type == ECharacterType.Giant)
                {
                    mText.text = "수 속성 친화도 : " + PlayerData.GetEA_Giant_Promotion(EElementalType.Water);
                }
                break;
            case EIncreaseStats.Wind:
                if (type == ECharacterType.Fairy)
                {
                    mText.text = "풍 속성 친화도 : " + PlayerData.GetEA_Fairy_Promotion(EElementalType.Wind);
                }
                else if (type == ECharacterType.Giant)
                {
                    mText.text = "풍 속성 친화도 : " + PlayerData.GetEA_Giant_Promotion(EElementalType.Wind);
                }
                break;
            case EIncreaseStats.Earth:
                if (type == ECharacterType.Fairy)
                {
                    mText.text = "지 속성 친화도 : " + PlayerData.GetEA_Fairy_Promotion(EElementalType.Earth);
                }
                else if (type == ECharacterType.Giant)
                {
                    mText.text = "지 속성 친화도 : " + PlayerData.GetEA_Giant_Promotion(EElementalType.Earth);
                }
                break;
            case EIncreaseStats.Light:
                if (type == ECharacterType.Fairy)
                {
                    mText.text = "광 속성 친화도 : " + PlayerData.GetEA_Fairy_Promotion(EElementalType.Light);
                }
                else if (type == ECharacterType.Giant)
                {
                    mText.text = "광 속성 친화도 : " + PlayerData.GetEA_Giant_Promotion(EElementalType.Light);
                }
                break;
            case EIncreaseStats.Darkness:
                if (type == ECharacterType.Fairy)
                {
                    mText.text = "암 속성 친화도 : " + PlayerData.GetEA_Fairy_Promotion(EElementalType.Darkness);
                }
                else if (type == ECharacterType.Giant)
                {
                    mText.text = "암 속성 친화도 : " + PlayerData.GetEA_Giant_Promotion(EElementalType.Darkness);
                }
                break;
        }
    }
}

public enum EIncreaseStats
{
    None = -1,
    MaxMana,
    ManaRecovery,
    MaxHP,
    HPRecovery,
    Movement,
    Casting,
    Purification,
    Exocism,
    CriticalChance,
    Kinetic,
    Arcane,
    Fire,
    Water,
    Wind,
    Earth,
    Light,
    Darkness,
    CriticalMag,
    Count
}

