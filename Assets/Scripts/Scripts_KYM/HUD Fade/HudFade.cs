﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HudFade : MonoBehaviour
{
    private const float FADE_SPEED = 2f;

    private static bool mIsFadeIn;
    private static bool mIsFadeOut;

    private void Awake()
    {
        mIsFadeIn = false;
        mIsFadeOut = false;
    }

    //HUD 나타나기
    public static IEnumerator FadeIn(Image image)
    {
        mIsFadeOut = false;
        mIsFadeIn = true;
        image.enabled = true;
        float alpha = image.color.a;

        for (; alpha <= 1; alpha += Time.unscaledDeltaTime * FADE_SPEED)
        {
            if (mIsFadeOut)
            {
                yield break;
            }
            image.color = new Color(1, 1, 1, alpha);
            yield return null;
        }

        image.color = new Color(1, 1, 1, 1);
    }

    public static IEnumerator FadeIn(Text text)
    {
        mIsFadeOut = false;
        mIsFadeIn = true;
        text.enabled = true;
        float alpha = text.color.a;

        for ( ; alpha <= 1; alpha += Time.unscaledDeltaTime * FADE_SPEED)
        {
            if (mIsFadeOut)
            {
                yield break;
            }
            text.color = new Color(1, 1, 1, alpha);
            yield return null;
        }

        text.color = new Color(1, 1, 1, 1);
    }

    public static IEnumerator FadeIn(Image image, Text text)
    {
        mIsFadeOut = false;
        mIsFadeIn = true;
        image.enabled = true;
        text.enabled = true;

        float alpha = image.color.a;

        for (; alpha <= 1; alpha += Time.unscaledDeltaTime * FADE_SPEED)
        {
            if (mIsFadeOut)
            {
                yield break;
            }
            image.color = new Color(1, 1, 1, alpha);
            text.color = new Color(1, 1, 1, alpha);
            yield return null;
        }

        image.color = new Color(1, 1, 1, 1);
        text.color = new Color(1, 1, 1, 1);
    }

    public static IEnumerator FadeIn(Image image, bool isDeactivate)
    {
        if(isDeactivate)
        {
            yield break;
        }

        mIsFadeOut = false;
        mIsFadeIn = true;
        image.enabled = true;
        float alpha = image.color.a;

        for (; alpha <= 1; alpha += Time.unscaledDeltaTime * FADE_SPEED)
        {
            if (mIsFadeOut)
            {
                yield break;
            }
            image.color = new Color(1, 1, 1, alpha);
            yield return null;
        }

        image.color = new Color(1, 1, 1, 1);
    }

    //HUD 사라지기
    public static IEnumerator FadeOut(Image image)
    {
        mIsFadeIn = false;
        mIsFadeOut = true;
        float alpha = image.color.a;

        for (; alpha >= 0; alpha -= Time.unscaledDeltaTime * FADE_SPEED)
        {
            if (mIsFadeIn)
            {
                yield break;
            }
            image.color = new Color(1, 1, 1, alpha);
            yield return null;
        }

        image.color = new Color(1, 1, 1, 0);
        image.enabled = false;
    }

    public static IEnumerator FadeOut(Text text)
    {
        mIsFadeIn = false;
        mIsFadeOut = true;
        float alpha = text.color.a;

        for (; alpha >= 0; alpha -= Time.unscaledDeltaTime * FADE_SPEED)
        {
            if (mIsFadeIn)
            {
                yield break;
            }
            text.color = new Color(1, 1, 1, alpha);
            yield return null;
        }

        text.color = new Color(1, 1, 1, 0);
        text.enabled = false;
    }

    public static IEnumerator FadeOut(Image image, Text text)
    {
        mIsFadeIn = false;
        mIsFadeOut = true;
        float alpha = image.color.a;

        for (; alpha >= 0; alpha -= Time.unscaledDeltaTime * FADE_SPEED)
        {
            if(mIsFadeIn)
            {
                yield break;
            }
            image.color = new Color(1, 1, 1, alpha);
            text.color = new Color(1, 1, 1, alpha);
            yield return null;
        }

        image.color = new Color(1, 1, 1, 0);
        text.color = new Color(1, 1, 1, 0);

        image.enabled = false;
        text.enabled = false;
    }

    public static IEnumerator FadeOut(Image image, bool isDeactivate)
    {
        if(isDeactivate)
        {
            yield break;
        }

        mIsFadeIn = false;
        mIsFadeOut = true;
        float alpha = image.color.a;

        for (; alpha >= 0; alpha -= Time.unscaledDeltaTime * FADE_SPEED)
        {
            if (mIsFadeIn)
            {
                yield break;
            }
            image.color = new Color(1, 1, 1, alpha);
            yield return null;
        }

        image.color = new Color(1, 1, 1, 0);
        image.enabled = false;
    }

    private void OnDestroy()
    {
        mIsFadeIn = false;
        mIsFadeOut = false;
    }
}
