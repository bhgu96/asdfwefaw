﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MosterHitParticleSystem : MonoBehaviour
{
    [SerializeField]
    private ParticleSystem mParticleSystem;

    public void PlayParticleSystem()
    {
        float prevAngle = mParticleSystem.transform.eulerAngles.z;
        if (transform.lossyScale.x > 0.0f)
        {
            mParticleSystem.transform.eulerAngles = new Vector3(0.0f, 180.0f, prevAngle);
        }
        else
        {
            mParticleSystem.transform.eulerAngles = new Vector3(180.0f, 0.0f, prevAngle);
        }
        mParticleSystem.Play();
    }
    public void Play(EHorizontalDirection direction)
    {
        float prevAngle = mParticleSystem.transform.eulerAngles.z;
        if (direction == EHorizontalDirection.Left)
        {
            mParticleSystem.transform.eulerAngles = new Vector3(0.0f, 180.0f, prevAngle);
        }
        else if(direction == EHorizontalDirection.Right)
        {
            mParticleSystem.transform.eulerAngles = new Vector3(180.0f, 0.0f, prevAngle);
        }
        mParticleSystem.Play();
    }
}
