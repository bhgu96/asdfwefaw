﻿using UnityEngine;

public class SelectCommunicateObject : MonoBehaviour
{
    private Material mMaterial;

    private void Awake()
    {
        mMaterial = this.transform.parent.GetComponent<SpriteRenderer>().material;
        mMaterial.SetInt("_OutlineThreadhold", 0);
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.layer == 30)
        {
            mMaterial.SetInt("_OutlineThreadhold", 1);
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.gameObject.layer == 30)
        {
            mMaterial.SetInt("_OutlineThreadhold", 0);
        }
    }
}
