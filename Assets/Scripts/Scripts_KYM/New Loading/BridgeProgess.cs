﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BridgeProgess : MonoBehaviour
{
    [SerializeField]
    private float mMaxProgressLength;

    private RectTransform mRectTransform;

    private void Awake()
    {
        mRectTransform = this.GetComponent<RectTransform>();
    }

    private void Update()
    {
        mRectTransform.anchoredPosition = new Vector2(-(mMaxProgressLength - mMaxProgressLength * LoadingController.AsyncOperation.progress), mRectTransform.anchoredPosition.y);

        if(LoadingController.IsCompleted)
        {
            mRectTransform.anchoredPosition = new Vector2(0f, mRectTransform.anchoredPosition.y);
        }
    }
}
