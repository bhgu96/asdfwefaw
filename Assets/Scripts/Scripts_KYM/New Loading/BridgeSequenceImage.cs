﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BridgeSequenceImage : MonoBehaviour
{
    [SerializeField]
    private float mTime;
    [SerializeField]
    private Sprite[] mSequenceImages;
    [SerializeField]
    private Text mText;

    private Image mImage;
    private Dictionary<Sprite, string> mSequenceDict; //Key : sprite, Value : string
    private Dictionary<string, Sprite> mSceneSequenceDict; //Key : string, Value : sprite 
    private Dictionary<string, int> mSceneIndexDict; //Key : string, Value : int
    

    private void Awake()
    {
        mImage = this.GetComponent<Image>();
        mSequenceDict = new Dictionary<Sprite, string>();
        mSceneSequenceDict = new Dictionary<string, Sprite>();
        mSceneIndexDict = new Dictionary<string, int>();

        AddDict();

        mText.text = mSequenceDict[mSequenceImages[mSceneIndexDict[LoadingController.NextScene]]];
        mImage.sprite = mSequenceImages[mSceneIndexDict[LoadingController.NextScene]];
        //StartCoroutine(SequenceBridgeImage());
    }

    private void AddDict()
    {
        mSequenceDict.Add(mSequenceImages[0], "성역");
        mSequenceDict.Add(mSequenceImages[1], "아스가르드");
        mSequenceDict.Add(mSequenceImages[2], "미드가르드");
        mSequenceDict.Add(mSequenceImages[3], "알프헤임");
        mSequenceDict.Add(mSequenceImages[4], "니플헤임");
        mSequenceDict.Add(mSequenceImages[5], "헬");

        mSceneSequenceDict.Add("성역", mSequenceImages[0]);
        mSceneSequenceDict.Add("아스가르드", mSequenceImages[1]);
        mSceneSequenceDict.Add("미드가르드", mSequenceImages[2]);
        mSceneSequenceDict.Add("알프헤임", mSequenceImages[3]);
        mSceneSequenceDict.Add("니플헤임", mSequenceImages[4]);
        mSceneSequenceDict.Add("헬", mSequenceImages[5]);

        mSceneIndexDict.Add(EGameScene.Scene_Tutorial.ToString(), 0);
        mSceneIndexDict.Add(EGameScene.Scene_Abyss_MemoryOfAstral.ToString(), 1);
        mSceneIndexDict.Add(EGameScene._Scene_Abyss_Santuary_KYM.ToString(), 0);
        mSceneIndexDict.Add(EGameScene.Scene_Abyss_Asgard.ToString(), 1);
        mSceneIndexDict.Add(EGameScene.Scene_Title.ToString(), 0);
        //mSceneIndexDict.Add("아스가르드", 1);
        //mSceneIndexDict.Add("미드가르드", 2);
        //mSceneIndexDict.Add("알프헤임", 3);
        //mSceneIndexDict.Add("니플헤임", 4);
        //mSceneIndexDict.Add("헬", 5);
    }

    private IEnumerator SequenceBridgeImage()
    {
        int index = 0;

        while(true)
        {
            if(index >= mSequenceImages.Length)
            {
                index = 0;
            }

            mText.text = mSequenceDict[mSequenceImages[index]];
            mImage.sprite = mSequenceImages[index];
            yield return new WaitForSeconds(mTime);
            yield return null;
            index++;
        }
    }
}
