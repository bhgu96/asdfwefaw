﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BridgeSpiritAnimation : MonoBehaviour
{
    public bool IsEndLoading { get { return mIsEndLoading; } private set { } }

    [SerializeField]
    private BridgePredetorAnimation mPredetorAnimation;

    private Animator mAnimatior;

    private bool mIsEndLoading = false;

    private void Awake()
    {
        mAnimatior = this.GetComponent<Animator>();
    }

    private void Update()
    {
        mAnimatior.SetBool("IsComplete", LoadingController.IsCompleted);
        mAnimatior.SetBool("IsGameStart", mPredetorAnimation.EndAnimation);
    }

    private void EndLoading()
    {
        mIsEndLoading = true;
    }
}
