﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BridgePredetorAnimation : MonoBehaviour
{
    public bool EndAnimation { get { return mEndAnimation; } private set { } }

    private Animator mAnimatior;
    private bool mEndAnimation = false;

    private void Awake()
    {
        mAnimatior = this.GetComponent<Animator>();
    }

    private void Update()
    {
        mAnimatior.SetBool("IsComplete", LoadingController.IsCompleted);
        mAnimatior.SetBool("IsGameStart", LoadingController.IsGameStart);
    }

    private void EndPredetorAnimation()
    {
        mEndAnimation = true;
    }
}
