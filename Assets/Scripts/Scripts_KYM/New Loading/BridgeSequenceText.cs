﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BridgeSequenceText : MonoBehaviour
{
    private Text mText;

    private void Awake()
    {
        mText = this.GetComponent<Text>();
    }

    private void Update()
    {
        if(!LoadingController.IsCompleted)
        {
            mText.text = (LoadingController.AsyncOperation.progress * 100f).ToString()+"%";
        }
    }
}
