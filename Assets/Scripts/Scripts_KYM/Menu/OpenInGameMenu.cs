﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OpenInGameMenu : MonoBehaviour
{
    [SerializeField]
    private UIPanel mOpenPanel;
    [SerializeField]
    private GameObject mSelectObject;
    [SerializeField]
    private EMenuType mMenuType;

    private UIButton mButton;

    private void Awake()
    {
        mButton = this.GetComponent<UIButton>();

        mButton.onConfirm += OpenMenu;
    }

    private void OnEnable()
    {
        mSelectObject.SetActive(true);
    }

    private void OpenMenu()
    {
        if(mMenuType == EMenuType.System)
        {
            mSelectObject.SetActive(false);
        }
        else if(mMenuType == EMenuType.Equipment)
        {
            mSelectObject.SetActive(true);
        }
        else if(mMenuType == EMenuType.Inventory)
        {
            mSelectObject.SetActive(true);
        }

        mButton.UIController.OpenPanel(mOpenPanel);
    }
}

public enum EMenuType
{
    None = -1,
    Equipment,
    Inventory,
    System,
    Count
}
