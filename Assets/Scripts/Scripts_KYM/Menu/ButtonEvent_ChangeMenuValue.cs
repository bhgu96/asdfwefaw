﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ButtonEvent_ChangeMenuValue : MonoBehaviour
{
    [SerializeField]
    private ButtonEvent_SettingControl mSettingControl;
    [SerializeField]
    private bool mIsPositive;

    private UIButton mButton;

    private void Awake()
    {
        mButton = this.GetComponent<UIButton>();
        mButton.onConfirm += ConfirmButton;
    }

    private void ConfirmButton()
    {
        mSettingControl.ChangeMenuValue(mIsPositive);
    }
}
