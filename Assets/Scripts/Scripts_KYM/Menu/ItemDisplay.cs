﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ItemDisplay : MonoBehaviour
{
    [SerializeField]
    private EItemDisplayCategory mDisplayCategory;
    [SerializeField]
    private Sprite mTransparentSprite;

    private UIController mUIController;

    private Image mImage;
    private Text mText;

    private void Awake()
    {
        if(this.GetComponent<Image>() != null)
        {
            mImage = this.GetComponent<Image>();
        }

        if(this.GetComponent<Text>() != null)
        {
            mText = this.GetComponent<Text>();
        }

        mUIController = this.GetComponentInParent<UIController>();
    }

    private void Update()
    {
        if(mUIController.CurButton != null)
        {
            switch (mDisplayCategory)
            {
                case EItemDisplayCategory.Icon:
                    if(mUIController.CurButton.GetComponent<CharacterSkillSlotHud>() != null)
                    {
                        if(mUIController.CurButton.GetComponent<CharacterSkillSlotHud>().CharacterType == ECharacterType.Fairy)
                        {
                            if(Fairy_Player.Main.SkillFrame.Peek(mUIController.CurButton.GetComponent<CharacterSkillSlotHud>().EquipIndex) == null)
                            {
                                mImage.sprite = mTransparentSprite;
                            }
                            else
                            {
                                mImage.sprite = Fairy_Player.Main.SkillFrame.Peek(mUIController.CurButton.GetComponent<CharacterSkillSlotHud>().EquipIndex).Icon;
                            }
                        }
                        else if(mUIController.CurButton.GetComponent<CharacterSkillSlotHud>().CharacterType == ECharacterType.Giant)
                        {
                            if (Giant_Player.Main.SkillFrame.Peek(mUIController.CurButton.GetComponent<CharacterSkillSlotHud>().EquipIndex) == null)
                            {
                                mImage.sprite = mTransparentSprite;
                            }
                            else
                            {
                                mImage.sprite = Giant_Player.Main.SkillFrame.Peek(mUIController.CurButton.GetComponent<CharacterSkillSlotHud>().EquipIndex).Icon;
                            }
                        }
                    }
                    else if(mUIController.CurButton.GetComponent<NewItemSlot>() != null)
                    {
                        if(mUIController.CurButton.GetComponent<NewItemSlot>().SlotCategory == EItemSlotCategory.Skill)
                        {
                            if (mUIController.CurButton.GetComponent<NewItemSlot>().CharacterType == ECharacterType.Fairy)
                            {
                                if (Fairy_Player.Main.SkillHandler.Get(mUIController.CurButton.GetComponent<NewItemSlot>().FairySkill) == null)
                                {
                                    mImage.sprite = mTransparentSprite;
                                }
                                else
                                {
                                    mImage.sprite = Fairy_Player.Main.SkillHandler.Get(mUIController.CurButton.GetComponent<NewItemSlot>().FairySkill).Icon;
                                }
                            }
                            else if (mUIController.CurButton.GetComponent<NewItemSlot>().CharacterType == ECharacterType.Giant)
                            {
                                if (Giant_Player.Main.SkillHandler.Get(mUIController.CurButton.GetComponent<NewItemSlot>().GiantSkill) == null)
                                {
                                    mImage.sprite = mTransparentSprite;
                                }
                                else
                                {
                                    mImage.sprite = Giant_Player.Main.SkillHandler.Get(mUIController.CurButton.GetComponent<NewItemSlot>().GiantSkill).Icon;
                                }
                            }
                        }
                        else
                        {
                            mImage.sprite = mTransparentSprite;
                        }
                    }
                    else
                    {
                        mImage.sprite = mTransparentSprite;
                    }
                    break;
                case EItemDisplayCategory.Name:
                    if (mUIController.CurButton.GetComponent<CharacterSkillSlotHud>() != null)
                    {
                        if (mUIController.CurButton.GetComponent<CharacterSkillSlotHud>().CharacterType == ECharacterType.Fairy)
                        {
                            if (Fairy_Player.Main.SkillFrame.Peek(mUIController.CurButton.GetComponent<CharacterSkillSlotHud>().EquipIndex) == null)
                            {
                                mText.text = null;
                            }
                            else
                            {
                                mText.text = Fairy_Player.Main.SkillFrame.Peek(mUIController.CurButton.GetComponent<CharacterSkillSlotHud>().EquipIndex).Name;
                            }
                        }
                        else if (mUIController.CurButton.GetComponent<CharacterSkillSlotHud>().CharacterType == ECharacterType.Giant)
                        {
                            if (Giant_Player.Main.SkillFrame.Peek(mUIController.CurButton.GetComponent<CharacterSkillSlotHud>().EquipIndex) == null)
                            {
                                mText.text = null;
                            }
                            else
                            {
                                mText.text = Giant_Player.Main.SkillFrame.Peek(mUIController.CurButton.GetComponent<CharacterSkillSlotHud>().EquipIndex).Name;
                            }
                        }
                    }
                    else if (mUIController.CurButton.GetComponent<NewItemSlot>() != null)
                    {
                        if(mUIController.CurButton.GetComponent<NewItemSlot>().SlotCategory == EItemSlotCategory.Skill)
                        {
                            if (mUIController.CurButton.GetComponent<NewItemSlot>().CharacterType == ECharacterType.Fairy)
                            {
                                if (Fairy_Player.Main.SkillHandler.Get(mUIController.CurButton.GetComponent<NewItemSlot>().FairySkill) == null)
                                {
                                    mText.text = null;
                                }
                                else
                                {
                                    mText.text = Fairy_Player.Main.SkillHandler.Get(mUIController.CurButton.GetComponent<NewItemSlot>().FairySkill).Name;
                                }
                            }
                            else if (mUIController.CurButton.GetComponent<NewItemSlot>().CharacterType == ECharacterType.Giant)
                            {
                                if (Giant_Player.Main.SkillHandler.Get(mUIController.CurButton.GetComponent<NewItemSlot>().GiantSkill) == null)
                                {
                                    mText.text = null;
                                }
                                else
                                {
                                    mText.text = Giant_Player.Main.SkillHandler.Get(mUIController.CurButton.GetComponent<NewItemSlot>().GiantSkill).Name;
                                }
                            }
                        }
                        else
                        {
                            mText.text = null;
                        }
                    }
                    else
                    {
                        mText.text = null;
                    }
                    break;
                case EItemDisplayCategory.Price:
                    if (mUIController.CurButton.GetComponent<CharacterSkillSlotHud>() != null)
                    {
                        if (mUIController.CurButton.GetComponent<CharacterSkillSlotHud>().CharacterType == ECharacterType.Fairy)
                        {
                            if (Fairy_Player.Main.SkillFrame.Peek(mUIController.CurButton.GetComponent<CharacterSkillSlotHud>().EquipIndex) == null)
                            {
                                mText.text = null;
                            }
                            else
                            {
                                mText.text = "100";//Fairy_Player.Main.SkillFrame.Peek(mUIController.CurButton.GetComponent<CharacterSkillSlotHud>().EquipIndex).Price;
                            }
                        }
                        else if (mUIController.CurButton.GetComponent<CharacterSkillSlotHud>().CharacterType == ECharacterType.Giant)
                        {
                            if (Giant_Player.Main.SkillFrame.Peek(mUIController.CurButton.GetComponent<CharacterSkillSlotHud>().EquipIndex) == null)
                            {
                                mText.text = null;
                            }
                            else
                            {
                                mText.text = "900";//Giant_Player.Main.SkillFrame.Peek(mUIController.CurButton.GetComponent<CharacterSkillSlotHud>().EquipIndex).Price;
                            }
                        }
                    }
                    else if (mUIController.CurButton.GetComponent<NewItemSlot>() != null)
                    {
                        if (mUIController.CurButton.GetComponent<NewItemSlot>().SlotCategory == EItemSlotCategory.Skill)
                        {
                            if (mUIController.CurButton.GetComponent<NewItemSlot>().CharacterType == ECharacterType.Fairy)
                            {
                                if (Fairy_Player.Main.SkillHandler.Get(mUIController.CurButton.GetComponent<NewItemSlot>().FairySkill) == null)
                                {
                                    mText.text = null;
                                }
                                else
                                {
                                    mText.text = "50";//Fairy_Player.Main.SkillHandler.Get(mUIController.CurButton.GetComponent<NewItemSlot>().FairySkill).Name;
                                }
                            }
                            else if (mUIController.CurButton.GetComponent<NewItemSlot>().CharacterType == ECharacterType.Giant)
                            {
                                if (Giant_Player.Main.SkillHandler.Get(mUIController.CurButton.GetComponent<NewItemSlot>().GiantSkill) == null)
                                {
                                    mText.text = null;
                                }
                                else
                                {
                                    mText.text = "100";//Giant_Player.Main.SkillHandler.Get(mUIController.CurButton.GetComponent<NewItemSlot>().GiantSkill).Name;
                                }
                            }
                        }
                        else
                        {
                            mText.text = null;
                        }
                    }
                    else
                    {
                        mText.text = null;
                    }
                    break;
                case EItemDisplayCategory.Stats:
                    if (mUIController.CurButton.GetComponent<CharacterSkillSlotHud>() != null)
                    {
                        if (mUIController.CurButton.GetComponent<CharacterSkillSlotHud>().CharacterType == ECharacterType.Fairy)
                        {
                            if (Fairy_Player.Main.SkillFrame.Peek(mUIController.CurButton.GetComponent<CharacterSkillSlotHud>().EquipIndex) == null)
                            {
                                mText.text = null;
                            }
                            else
                            {
                                mText.text = "Ground	CoolDown:0(sec)	Mana Generate:45";
                            }
                        }
                        else if (mUIController.CurButton.GetComponent<CharacterSkillSlotHud>().CharacterType == ECharacterType.Giant)
                        {
                            if (Giant_Player.Main.SkillFrame.Peek(mUIController.CurButton.GetComponent<CharacterSkillSlotHud>().EquipIndex) == null)
                            {
                                mText.text = null;
                            }
                            else
                            {
                                mText.text = "Ground	CoolDown:0(sec)	Mana Generate:25";
                            }
                        }
                    }
                    else if (mUIController.CurButton.GetComponent<NewItemSlot>() != null)
                    {
                        if (mUIController.CurButton.GetComponent<NewItemSlot>().SlotCategory == EItemSlotCategory.Skill)
                        {
                            if (mUIController.CurButton.GetComponent<NewItemSlot>().CharacterType == ECharacterType.Fairy)
                            {
                                if (Fairy_Player.Main.SkillHandler.Get(mUIController.CurButton.GetComponent<NewItemSlot>().FairySkill) == null)
                                {
                                    mText.text = null;
                                }
                                else
                                {
                                    mText.text = "Ground	CoolDown:0(sec)	Mana Generate:45";//Fairy_Player.Main.SkillHandler.Get(mUIController.CurButton.GetComponent<NewItemSlot>().FairySkill).Name;
                                }
                            }
                            else if (mUIController.CurButton.GetComponent<NewItemSlot>().CharacterType == ECharacterType.Giant)
                            {
                                if (Giant_Player.Main.SkillHandler.Get(mUIController.CurButton.GetComponent<NewItemSlot>().GiantSkill) == null)
                                {
                                    mText.text = null;
                                }
                                else
                                {
                                    mText.text = mText.text = "Ground	CoolDown:0(sec)	Mana Generate:25";//Giant_Player.Main.SkillHandler.Get(mUIController.CurButton.GetComponent<NewItemSlot>().GiantSkill).Name;
                                }
                            }
                        }
                        else
                        {
                            mText.text = null;
                        }
                    }
                    else
                    {
                        mText.text = null;
                    }
                    break;
                case EItemDisplayCategory.Level:
                    if (mUIController.CurButton.GetComponent<CharacterSkillSlotHud>() != null)
                    {
                        if (mUIController.CurButton.GetComponent<CharacterSkillSlotHud>().CharacterType == ECharacterType.Fairy)
                        {
                            if (Fairy_Player.Main.SkillFrame.Peek(mUIController.CurButton.GetComponent<CharacterSkillSlotHud>().EquipIndex) == null)
                            {
                                mText.text = null;
                            }
                            else
                            {
                                mText.text = "LV." + "4";//Fairy_Player.Main.SkillFrame.Peek(mUIController.CurButton.GetComponent<CharacterSkillSlotHud>().EquipIndex).Level;
                            }
                        }
                        else if (mUIController.CurButton.GetComponent<CharacterSkillSlotHud>().CharacterType == ECharacterType.Giant)
                        {
                            if (Giant_Player.Main.SkillFrame.Peek(mUIController.CurButton.GetComponent<CharacterSkillSlotHud>().EquipIndex) == null)
                            {
                                mText.text = null;
                            }
                            else
                            {
                                mText.text = "LV." + "2";//Fairy_Player.Main.SkillFrame.Peek(mUIController.CurButton.GetComponent<CharacterSkillSlotHud>().EquipIndex).Level;
                            }
                        }
                    }
                    else if (mUIController.CurButton.GetComponent<NewItemSlot>() != null)
                    {
                        if (mUIController.CurButton.GetComponent<NewItemSlot>().SlotCategory == EItemSlotCategory.Skill)
                        {
                            if (mUIController.CurButton.GetComponent<NewItemSlot>().CharacterType == ECharacterType.Fairy)
                            {
                                if (Fairy_Player.Main.SkillHandler.Get(mUIController.CurButton.GetComponent<NewItemSlot>().FairySkill) == null)
                                {
                                    mText.text = null;
                                }
                                else
                                {
                                    mText.text = "LV."+"5";//Fairy_Player.Main.SkillHandler.Get(mUIController.CurButton.GetComponent<NewItemSlot>().FairySkill).Name;
                                }
                            }
                            else if (mUIController.CurButton.GetComponent<NewItemSlot>().CharacterType == ECharacterType.Giant)
                            {
                                if (Giant_Player.Main.SkillHandler.Get(mUIController.CurButton.GetComponent<NewItemSlot>().GiantSkill) == null)
                                {
                                    mText.text = null;
                                }
                                else
                                {
                                    mText.text = mText.text = "LV."+"3";//Giant_Player.Main.SkillHandler.Get(mUIController.CurButton.GetComponent<NewItemSlot>().GiantSkill).Name;
                                }
                            }
                        }
                        else
                        {
                            mText.text = null;
                        }
                    }
                    else
                    {
                        mText.text = null;
                    }
                    break;
                case EItemDisplayCategory.Description:
                    if (mUIController.CurButton.GetComponent<CharacterSkillSlotHud>() != null)
                    {
                        if (mUIController.CurButton.GetComponent<CharacterSkillSlotHud>().CharacterType == ECharacterType.Fairy)
                        {
                            if (Fairy_Player.Main.SkillFrame.Peek(mUIController.CurButton.GetComponent<CharacterSkillSlotHud>().EquipIndex) == null)
                            {
                                mText.text = null;
                            }
                            else
                            {
                                mText.text = Fairy_Player.Main.SkillFrame.Peek(mUIController.CurButton.GetComponent<CharacterSkillSlotHud>().EquipIndex).Description;
                            }
                        }
                        else if (mUIController.CurButton.GetComponent<CharacterSkillSlotHud>().CharacterType == ECharacterType.Giant)
                        {
                            if (Giant_Player.Main.SkillFrame.Peek(mUIController.CurButton.GetComponent<CharacterSkillSlotHud>().EquipIndex) == null)
                            {
                                mText.text = null;
                            }
                            else
                            {
                                mText.text = Giant_Player.Main.SkillFrame.Peek(mUIController.CurButton.GetComponent<CharacterSkillSlotHud>().EquipIndex).Description;
                            }
                        }
                    }
                    else if (mUIController.CurButton.GetComponent<NewItemSlot>() != null)
                    {
                        if (mUIController.CurButton.GetComponent<NewItemSlot>().SlotCategory == EItemSlotCategory.Skill)
                        {
                            if (mUIController.CurButton.GetComponent<NewItemSlot>().CharacterType == ECharacterType.Fairy)
                            {
                                if (Fairy_Player.Main.SkillHandler.Get(mUIController.CurButton.GetComponent<NewItemSlot>().FairySkill) == null)
                                {
                                    mText.text = null;
                                }
                                else
                                {
                                    mText.text = Fairy_Player.Main.SkillHandler.Get(mUIController.CurButton.GetComponent<NewItemSlot>().FairySkill).Description;
                                }
                            }
                            else if (mUIController.CurButton.GetComponent<NewItemSlot>().CharacterType == ECharacterType.Giant)
                            {
                                if (Giant_Player.Main.SkillHandler.Get(mUIController.CurButton.GetComponent<NewItemSlot>().GiantSkill) == null)
                                {
                                    mText.text = null;
                                }
                                else
                                {
                                    mText.text = mText.text = Giant_Player.Main.SkillHandler.Get(mUIController.CurButton.GetComponent<NewItemSlot>().GiantSkill).Description;
                                }
                            }
                        }
                        else
                        {
                            mText.text = null;
                        }
                    }
                    else
                    {
                        mText.text = null;
                    }
                    break;
            }
        }
    }
}

public enum EItemDisplayCategory
{
    None = -1,
    Icon,
    Name,
    Level,
    Price,
    Description,
    Stats,
    Count
}

