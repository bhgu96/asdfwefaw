﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CheckInteract : MonoBehaviour
{
    [SerializeField]
    private GameObject mFairyEquip;
    [SerializeField]
    private GameObject mGiantEquip;
    [SerializeField]
    private GameObject mFairyButton;
    [SerializeField]
    private GameObject mGiantButton;

    private UIButton[] mFairyEquipButtons;
    private UIButton[] mGiantEquipButtons;
    private UIButton mFairyCharacterButton;
    private UIButton mGiantCharacterButton;

    private void Awake()
    {
        mFairyEquipButtons = mFairyEquip.GetComponentsInChildren<UIButton>();
        mGiantEquipButtons = mGiantEquip.GetComponentsInChildren<UIButton>();

        mFairyCharacterButton = mFairyButton.GetComponentInChildren<UIButton>();
        mGiantCharacterButton = mGiantButton.GetComponentInChildren<UIButton>();
    }

    private void OnEnable()
    {
        foreach(UIButton button in mFairyEquipButtons)
        {
            button.interactable = false;
        }

        foreach (UIButton button in mGiantEquipButtons)
        {
            button.interactable = false;
        }

        mFairyCharacterButton.interactable = false;
        mGiantCharacterButton.interactable = false;
    }

    private void OnDisable()
    {
        foreach (UIButton button in mFairyEquipButtons)
        {
            button.interactable = true;
        }

        foreach (UIButton button in mGiantEquipButtons)
        {
            button.interactable = true;
        }

        mFairyCharacterButton.interactable = true;
        mGiantCharacterButton.interactable = true;
    }
}
