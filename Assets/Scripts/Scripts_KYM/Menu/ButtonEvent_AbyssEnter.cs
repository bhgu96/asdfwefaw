﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ButtonEvent_AbyssEnter : MonoBehaviour
{
    [SerializeField]
    private SanctuaryEnterPotal mPortal;
    [SerializeField]
    private bool mIsYes;

    private UIButton mButton;

    private void Awake()
    {
        mButton = this.GetComponent<UIButton>();
        mButton.onConfirm += ConfirmButton;
    }

    private void ConfirmButton()
    {
        if (mIsYes)
        {
            mPortal.EnterPotalEvent();
        }

        if(!mIsYes)
        {
            mPortal.IsEnter = false;
        }

        mPortal.AlterPanel.gameObject.SetActive(false);
    }
}
