﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ActivateSelectButton : MonoBehaviour
{
    [SerializeField]
    private GameObject mSelectButton;

    private void OnEnable()
    {
        mSelectButton.SetActive(true);
    }
}
