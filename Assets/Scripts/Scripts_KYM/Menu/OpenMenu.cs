﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class OpenMenu : MonoBehaviour
{
    private const float DEFAULT_X_POSITION = 472;

    [SerializeField]
    private GameObject mSelectObject;

    private RectTransform mRectTransform;

    private void Awake()
    {
        mRectTransform = this.GetComponent<RectTransform>();
    }

    private void OnEnable()
    {
        mSelectObject.SetActive(true);
        StartCoroutine(MoveMenuPostion());
    }

    private void OnDisable()
    {
        mSelectObject.SetActive(false);
        mRectTransform.anchoredPosition = new Vector2(DEFAULT_X_POSITION, mRectTransform.anchoredPosition.y);
    }

    private IEnumerator MoveMenuPostion()
    {
        mRectTransform.anchoredPosition = new Vector2(DEFAULT_X_POSITION, mRectTransform.anchoredPosition.y);

        for(float x = DEFAULT_X_POSITION; x >= 0.0f; x -= Time.unscaledDeltaTime * 1000f)
        {
            mRectTransform.anchoredPosition = new Vector2(x, mRectTransform.anchoredPosition.y);
            yield return null;
        }

        mRectTransform.anchoredPosition = new Vector2(0f, mRectTransform.anchoredPosition.y);
    }
}
