﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class NewItemSlot : MonoBehaviour
{
    public ECharacterType CharacterType { get { return mCharacterType; } private set { } }
    public EItemSlotCategory SlotCategory { get { return mSlotCategory; } private set { } }
    public ESkillType_Fairy_Player FairySkill { get { return mFairySkill; } private set { } }
    public ESkillType_Giant_Player GiantSkill { get { return mGiantSkill; } private set { } }
    public int ItemIndex { get { return mItemIndex; } private set { } }

    [SerializeField]
    private ECharacterType mCharacterType;
    [SerializeField]
    private EItemSlotCategory mSlotCategory;
    [SerializeField]
    private ESkillType_Fairy_Player mFairySkill;
    [SerializeField]
    private ESkillType_Giant_Player mGiantSkill;
    [SerializeField]
    private int mItemIndex;

    private Image mImage;
    private Text mText;

    private void Awake()
    {
        mImage = this.GetComponent<Image>();
        mText = this.GetComponent<Text>();
    }

    private void Update()
    {
        if(mSlotCategory == EItemSlotCategory.Skill)
        {
            if(mCharacterType == ECharacterType.Fairy)
            {
                if(Fairy_Player.Main.SkillHandler.Get(mFairySkill) != null)
                {
                    mImage.sprite = Fairy_Player.Main.SkillHandler.Get(mFairySkill).Icon;
                }
            }
            else if(mCharacterType == ECharacterType.Giant)
            {
                if(Giant_Player.Main.SkillHandler.Get(mGiantSkill) != null)
                {
                    mImage.sprite = Giant_Player.Main.SkillHandler.Get(mGiantSkill).Icon;
                }
            }
        }
        else if(mSlotCategory == EItemSlotCategory.Power)
        {

        }
    }
}

public enum EItemSlotCategory
{
    None = -1,
    Skill,
    Power,
    Count
}

