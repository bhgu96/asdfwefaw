﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CharacterEquipment : MonoBehaviour
{
    [SerializeField]
    private ECharacterType mCharacterType;
    [SerializeField]
    private GameObject mFairyEquipSlotObject;
    [SerializeField]
    private GameObject mGiantEquipSlotObject;

    private UIButton mButton;


    private void Awake()
    {
        mButton = this.GetComponent<UIButton>();

        mButton.onConfirm += SelectCharacterButton;
    }

    private void SelectCharacterButton()
    {
        if(mCharacterType == ECharacterType.Fairy)
        {
            mFairyEquipSlotObject.SetActive(true);
            mGiantEquipSlotObject.SetActive(false);
        }
        else if(mCharacterType == ECharacterType.Giant)
        {
            mGiantEquipSlotObject.SetActive(true);
            mFairyEquipSlotObject.SetActive(false);
        }
    }
}
