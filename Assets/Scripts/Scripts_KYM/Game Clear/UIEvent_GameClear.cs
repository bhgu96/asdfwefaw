﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIEvent_GameClear : MonoBehaviour
{
    [SerializeField]
    private Text mText;
    //[SerializeField]
    //private string mReloadScene;
    [SerializeField]
    private EGameScene mReloadScene;

    [SerializeField]
    private RectTransform mBlinkTransform;
    [SerializeField]
    private Image mStageClearImage;

    private void OnEnable()
    {
        StartCoroutine(BlinkGameClearText());
    }

    private IEnumerator BlinkGameClearText()
    {
        mBlinkTransform.gameObject.SetActive(true);
        mStageClearImage.gameObject.SetActive(false);

        yield return new WaitForSeconds(0.1f);

        mBlinkTransform.sizeDelta = new Vector2(mBlinkTransform.sizeDelta.x, 50f);

        yield return new WaitForSeconds(0.05f);

        mBlinkTransform.sizeDelta = new Vector2(mBlinkTransform.sizeDelta.x, 200f);

        yield return new WaitForSeconds(0.1f);

        mBlinkTransform.gameObject.SetActive(false);
        mStageClearImage.gameObject.SetActive(true);

        yield return new WaitForSeconds(2.0f);
        mText.gameObject.SetActive(true);

        while (true)
        {
            for (float i = 0; i <= 1; i += Time.deltaTime)
            {
                if (Input.GetKeyDown(KeyCode.Return))
                {
                    //StateManager.GameClearHandler.State = EGameClearFlags.Normal;
                    LoadingController.LoadScene(mReloadScene.ToString());
                }
                else if (Input.GetKeyDown(KeyCode.Escape))
                {
                    //StateManager.GameClearHandler.State = EGameClearFlags.Normal;
                    LoadingController.LoadScene("Scene_Title");
                }

                mText.color = new Color(1, 1, 1, i);
                yield return null;
            }

            mText.color = new Color(1, 1, 1, 1);

            for (float i = 1; i >= 0; i -= Time.deltaTime)
            {
                if (Input.GetKeyDown(KeyCode.Return))
                {
                    //StateManager.GameClearHandler.State = EGameClearFlags.Normal;
                    LoadingController.LoadScene(mReloadScene.ToString());
                }
                else if (Input.GetKeyDown(KeyCode.Escape))
                {
                    //StateManager.GameClearHandler.State = EGameClearFlags.Normal;
                    LoadingController.LoadScene("Scene_Title");
                }

                mText.color = new Color(1, 1, 1, i);
                yield return null;
            }

            mText.color = new Color(1, 1, 1, 0);
        }
    }
}
