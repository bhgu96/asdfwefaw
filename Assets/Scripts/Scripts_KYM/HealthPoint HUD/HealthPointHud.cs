﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HealthPointHud : AHud
{
    [SerializeField]
    Material mPostProcessMaterial;

    private Image mImage;
    private RectTransform mHPRectTransform;
    private Status_Giant_Player mActorStatus;
    private float mMaxHPLength;
    private int mMaxHP;
    private int mCurHP;


    private void Awake()
    {
        mHPRectTransform = this.GetComponent<RectTransform>();
        mMaxHPLength = mHPRectTransform.sizeDelta.x;
        mImage = this.GetComponent<Image>();
    }

    private void Start()
    {
        if (Giant_Player.Main != null)
        {
            mActorStatus = Giant_Player.Main.Status;
            mMaxHP = (int)mActorStatus.MaxLife;
            mCurHP = (int)mActorStatus.Life;
        }
        else
        {
            mMaxHP = (int)PrototypeGiant.Giant.MaxHP;
            mCurHP = (int)PrototypeGiant.Giant.CurHP;
        }
    }

    public override void Activate()
    {

    }

    public override void Deactivate()
    {

    }

    public override void Express()
    {
        //float resultXPosition = mMaxHPLength - mMaxHPLength * ((float)mCurHP / mMaxHP);
        //float curXPosition = mHPRectTransform.anchoredPosition.x;

        mHPRectTransform.anchoredPosition = new Vector2(-(mMaxHPLength - mMaxHPLength * ((float)mCurHP / mMaxHP)), mHPRectTransform.anchoredPosition.y);


        //HP PostProcessing
        if (mPostProcessMaterial != null)
        {
            //현재 생명력이 최대 생명력의 25%이하가 되면 효과가 발동되도록 구현함
            if (mCurHP <= mMaxHP * 0.25f)
            {
                SetPostProcess.LowHealthPointPostProcess(mPostProcessMaterial);
            }
            else
            {
                SetPostProcess.NormalHealthPointPostProcess(mPostProcessMaterial);
            }

            if (StateManager.GameOver.State)
            {
                SetPostProcess.GameOverHealthPointPostProcess(mPostProcessMaterial);
            }
        }
    }

    public override void GetInformation()
    {
        if(mActorStatus != null)
        {
            mMaxHP = (int)mActorStatus.MaxLife;
            mCurHP = (int)mActorStatus.Life;
        }
        else
        {
            mMaxHP = (int)PrototypeGiant.Giant.MaxHP;
            mCurHP = (int)PrototypeGiant.Giant.CurHP;
        }
    }

    private void OnDestroy()
    {
        mActorStatus = null;
    }
}
