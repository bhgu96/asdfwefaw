﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ChestImageHud : AHud
{
    private void Awake()
    {
        //StateManager.GamePauseHandler.AddEnterEvent(EPauseFlags.PAUSE, Deactivate);
        //StateManager.GamePauseHandler.AddEnterEvent(EPauseFlags.Play, Activate);
        //StateManager.GameOverHandler.AddEnterEvent(EGameOverFlags.GameOver, Deactivate);
        //StateManager.GameOverHandler.AddEnterEvent(EGameOverFlags.Play, Activate);
        //StateManager.GameStateHandler.AddEnterEvent(EGameStateFlags.Normal, Activate);
        //StateManager.GameStateHandler.AddExitEvent(EGameStateFlags.Normal, Deactivate);
        //StateManager.GameStateHandler.AddEnterEvent(EGameStateFlags.TUTORIAL_CURSE, Activate);
    }

    public override void Activate()
    {
        //if (StateManager.GameStateHandler.State == EGameStateFlags.Normal || StateManager.GameStateHandler.State == EGameStateFlags.TUTORIAL_CURSE)
        //{
        //    this.gameObject.SetActive(true);
        //}
    }

    public override void Deactivate()
    {
        this.gameObject.SetActive(false);
    }

    public override void Express()
    {

    }

    public override void GetInformation()
    {

    }

    private void OnDestroy()
    {
        //StateManager.GamePauseHandler.RemoveEnterEvent(EPauseFlags.PAUSE, Deactivate);
        //StateManager.GamePauseHandler.RemoveEnterEvent(EPauseFlags.Play, Activate);
        //StateManager.GameOverHandler.RemoveEnterEvent(EGameOverFlags.GameOver, Deactivate);
        //StateManager.GameOverHandler.RemoveEnterEvent(EGameOverFlags.Play, Activate);
        //StateManager.GameStateHandler.RemoveEnterEvent(EGameStateFlags.Normal, Activate);
        //StateManager.GameStateHandler.RemoveExitEvent(EGameStateFlags.Normal, Deactivate);
        //StateManager.GameStateHandler.RemoveEnterEvent(EGameStateFlags.TUTORIAL_CURSE, Activate);
    }
}
