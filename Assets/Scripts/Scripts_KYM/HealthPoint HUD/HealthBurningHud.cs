﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HealthBurningHud : AHud
{
    private const float MAX_BURNING_Y_POSITION = 41;
    private const float MIN_BURNING_Y_POSITION = 0;

    [SerializeField]
    /// 인스펙터창에서 포식귀 게임 오브젝트(작업했을때는 Prefab_Guardian으로 되어있었음)를 드래그 앤 드랍 시키면 됩니다.
    private GameObject mActorObject;
    [SerializeField]
    private float mMaxHPLength;

    private float mBurningYPosition;
    private Vector2 mMaxSize;
    private Vector2 mMinSize;
    private RectTransform mBurningRectTransform;
    private IActor mActor;
    private float mMaxHP;
    private float mCurHP;

    private void Awake()
    {
        if (mActorObject.GetComponent<IActor>() != null)
        {
            mActor = mActorObject.GetComponent<IActor>();
        }

        mBurningRectTransform = this.GetComponent<RectTransform>();

        mMaxSize = mBurningRectTransform.sizeDelta;
        mMinSize = new Vector2(mBurningRectTransform.sizeDelta.x / 3f, mBurningRectTransform.sizeDelta.y / 3f);

        //StateManager.GamePauseHandler.AddEnterEvent(EPauseFlags.PAUSE, Deactivate);
        //StateManager.GamePauseHandler.AddEnterEvent(EPauseFlags.Play, Activate);
        //StateManager.GameOverHandler.AddEnterEvent(EGameOverFlags.GameOver, Deactivate);
        //StateManager.GameOverHandler.AddEnterEvent(EGameOverFlags.Play, Activate);
        //StateManager.GameStateHandler.AddEnterEvent(EGameStateFlags.Normal, Activate);
        //StateManager.GameStateHandler.AddExitEvent(EGameStateFlags.Normal, Deactivate);
        //StateManager.GameStateHandler.AddEnterEvent(EGameStateFlags.TUTORIAL_CURSE, Activate);
    }

    private void Start()
    {
        //mMaxHP = mActor.Status.MaximumHealthPoint;
        //mCurHP = mActor.Status.CurrentHealthPoint;
    }

    public override void Activate()
    {
        //if (StateManager.GameStateHandler.State == EGameStateFlags.Normal || StateManager.GameStateHandler.State == EGameStateFlags.TUTORIAL_CURSE)
        //{
        //    this.gameObject.SetActive(true);
        //}
    }

    public override void Deactivate()
    {
        this.gameObject.SetActive(false);
    }

    public override void Express()
    {
        if(mCurHP <= 0)
        {
            this.gameObject.GetComponent<Image>().enabled = false;
        }
        else
        {
            this.gameObject.GetComponent<Image>().enabled = true;
        }

        float resultXPosition = mMaxHPLength - mMaxHPLength * (mCurHP / mMaxHP);
        float curXPosition = mBurningRectTransform.anchoredPosition.x;

        //if (Mathf.Abs(curXPosition) > resultXPosition)
        //{
        //    curXPosition += Time.deltaTime * 500f;
        //    curXPosition = Mathf.Clamp(curXPosition, curXPosition, 0);
        //    mBurningRectTransform.anchoredPosition = new Vector2(curXPosition, mBurningYPosition);
        //}
        //else
        //{
        //    mBurningRectTransform.anchoredPosition = new Vector2(-(mMaxHPLength - mMaxHPLength * (mCurHP / mMaxHP)), mBurningYPosition);
        //}

        mBurningRectTransform.anchoredPosition = new Vector2(-(mMaxHPLength - mMaxHPLength * (mCurHP / mMaxHP)), mBurningYPosition);
    }

    public override void GetInformation()
    {
        //mMaxHP = mActor.Status.MaximumHealthPoint;
        //mCurHP = mActor.Status.CurrentHealthPoint;

        if (mCurHP <= mMaxHP / 4f)
        {
            mBurningYPosition = MAX_BURNING_Y_POSITION;
            mBurningRectTransform.sizeDelta = mMinSize;
        }
        else
        {
            mBurningYPosition = MIN_BURNING_Y_POSITION;
            mBurningRectTransform.sizeDelta = mMaxSize;
        }
    }

    private void OnDestroy()
    {
        //StateManager.GamePauseHandler.RemoveEnterEvent(EPauseFlags.PAUSE, Deactivate);
        //StateManager.GamePauseHandler.RemoveEnterEvent(EPauseFlags.Play, Activate);
        //StateManager.GameOverHandler.RemoveEnterEvent(EGameOverFlags.GameOver, Deactivate);
        //StateManager.GameOverHandler.RemoveEnterEvent(EGameOverFlags.Play, Activate);
        //StateManager.GameStateHandler.RemoveEnterEvent(EGameStateFlags.Normal, Activate);
        //StateManager.GameStateHandler.RemoveExitEvent(EGameStateFlags.Normal, Deactivate);
        //StateManager.GameStateHandler.RemoveEnterEvent(EGameStateFlags.TUTORIAL_CURSE, Activate);
    }
}
