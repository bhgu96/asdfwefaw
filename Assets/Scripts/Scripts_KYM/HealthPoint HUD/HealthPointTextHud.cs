﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HealthPointTextHud : AHud
{
    private Text mText;
    private Status_Giant_Player mActorStatus;
    private float mMaxHP;
    private float mCurHP;


    private void Awake()
    {
        mText = this.GetComponent<Text>();
    }

    private void Start()
    {
        if (Giant_Player.Main != null)
        {
            mActorStatus = Giant_Player.Main.Status;
            mMaxHP = mActorStatus.MaxLife;
            mCurHP = mActorStatus.Life;
        }
        else
        {
            mMaxHP = (int)PrototypeGiant.Giant.MaxHP;
            mCurHP = (int)PrototypeGiant.Giant.CurHP;
        }
    }

    public override void Activate()
    {

    }

    public override void Deactivate()
    {

    }

    public override void Express()
    {
        mText.text = mCurHP.ToString() + "/" + mMaxHP.ToString() + " " + "(" + ((mCurHP / mMaxHP) * 100).ToString() + "%" + ")";
    }

    public override void GetInformation()
    {
        if (mActorStatus != null)
        {
            mMaxHP = (int)mActorStatus.MaxLife;
            mCurHP = (int)mActorStatus.Life;
        }
        else
        {
            mMaxHP = (int)PrototypeGiant.Giant.MaxHP;
            mCurHP = (int)PrototypeGiant.Giant.CurHP;
        }
    }


    private void OnDestroy()
    {
        mActorStatus = null;
    }

}
