﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BackHealthPointHud : AHud
{
    [SerializeField]
    private float mMaxHPLength;

    private Image mImage;
    private RectTransform mHPRectTransform;

    //private ActorStatus mActorStatus;
    private IActor mActorInterface;
    private float mMaxHP;
    private float mCurHP;

    private void Awake()
    {
        mHPRectTransform = this.GetComponent<RectTransform>();
        mImage = this.GetComponent<Image>();
    }

    private void Start()
    {
        //if (NewGiant.Instance != null)
        //{
        //    mActorStatus = NewGiant.Instance.Status;
        //    mActorInterface = NewGiant.Instance.GetComponent<IActor>();
        //}

        //mMaxHP = mActorStatus.MaximumHealthPoint;
        //mCurHP = mActorStatus.CurrentHealthPoint;
    }

    public override void Activate()
    {

    }

    public override void Deactivate()
    {

    }

    public override void Express()
    {
        float resultXPosition = mMaxHPLength - mMaxHPLength * (mCurHP / mMaxHP);
        float curXPosition = Mathf.Abs(mHPRectTransform.anchoredPosition.x);
        float curAlpha = mImage.color.a;
        
        if(curXPosition - resultXPosition < -15)
        {
            //if(mActorInterface.State.IsHit)
            //{
            //    curAlpha = 1;
            //}

            curXPosition += Time.deltaTime * 150f;
            mImage.color = new Color(1, 1, 1, curAlpha = Mathf.Clamp(curAlpha -= Time.deltaTime, 0, 1));
            mHPRectTransform.anchoredPosition = new Vector2(-(curXPosition), mHPRectTransform.anchoredPosition.y);

            if(mImage.color.a == 0)
            {
                mHPRectTransform.anchoredPosition = new Vector2(-(mMaxHPLength - mMaxHPLength * (mCurHP / mMaxHP)), mHPRectTransform.anchoredPosition.y);
            }
        }
        else
        {
            mHPRectTransform.anchoredPosition = new Vector2(-(mMaxHPLength - mMaxHPLength * (mCurHP / mMaxHP)), mHPRectTransform.anchoredPosition.y);
        }
    }

    public override void GetInformation()
    {
        //mMaxHP = mActorStatus.MaximumHealthPoint;
        //mCurHP = mActorStatus.CurrentHealthPoint;
    }

    private void OnDestroy()
    {
        //mActorStatus = null;
        mActorInterface = null;
    }
}
