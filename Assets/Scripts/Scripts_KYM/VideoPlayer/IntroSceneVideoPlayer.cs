﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Video;
using UnityEngine.SceneManagement;

public class IntroSceneVideoPlayer : MonoBehaviour
{
    [SerializeField]
    private CameraController mCamera;
    [SerializeField]
    private EGameScene mNextScene;
    [SerializeField]
    private GameObject mVideoRawObject;

    private VideoPlayer mVideoPlayer;
    private bool mIsEndVideo;

    private void Awake()
    {
        mVideoPlayer = this.GetComponent<VideoPlayer>();
        mVideoRawObject.SetActive(true);

        Application.targetFrameRate = 120;
        Screen.SetResolution(1920, 1080, true);
        CustomCursor.InvisibleCursor();
    }

    private void Update()
    {
        if(Input.anyKeyDown)
        {
            mVideoPlayer.Stop();
            mIsEndVideo = false;
        }

        if(!mVideoPlayer.isPlaying && !mIsEndVideo)
        {
            mIsEndVideo = true;
            StartCoroutine(CameraFade());
        }
    }

    private IEnumerator CameraFade()
    {
        mCamera.BlackOut(0.5f, 1.0f);
        yield return new WaitUntil(() => !mCamera.IsBlackOutTriggered);
        CustomCursor.VisibleCursor();
        SceneManager.LoadScene(mNextScene.ToString());
    }
}
