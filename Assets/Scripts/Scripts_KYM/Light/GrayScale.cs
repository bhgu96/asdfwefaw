﻿using UnityEngine;
using System.Collections;


public class GrayScale : MonoBehaviour
{
    [Range(0,1)]
    [SerializeField]
    private float mIntensity;
    [SerializeField]
    private Material mGrayMaterial;

    public float Intensity { get { return mIntensity; } set { mIntensity = value; } }

    void OnRenderImage(RenderTexture source, RenderTexture destination)
    {
        if (mIntensity == 0)
        {
            Graphics.Blit(source, destination);
            return;
        }
        else
        {
            mGrayMaterial.SetFloat("_bwBlend", mIntensity);
            Graphics.Blit(source, destination, mGrayMaterial);
            return;
        }
    }
}
