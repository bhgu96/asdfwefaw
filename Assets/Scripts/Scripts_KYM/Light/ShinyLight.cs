﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum EBlinkLight
{
    None = -1,
    RANGE,
    INTENSITY,
    Count
}

public class ShinyLight : MonoBehaviour
{
    private const float MIN_LIGHT_RANGE = 1;
    private const float MIN_LIGHT_INTENSITY = 30;
    private const float COEEFICIENT = 10;

    [SerializeField]
    private float mMaxLightRange;
    [SerializeField]
    private float mMaxLightIntensity;
    [SerializeField]
    private EBlinkLight mBlinkKind;

    private Light mLight;
    private bool mIsShiny;

    private void Awake()
    {
        mLight = this.GetComponent<Light>();
    }

    private void OnEnable()
    {
        if(mBlinkKind == EBlinkLight.RANGE)
        {
            StartCoroutine(BlinkLightRange());
        }
        else if(mBlinkKind == EBlinkLight.INTENSITY)
        {
            StartCoroutine(BlinkLightIntensity());
        }
    }

    private IEnumerator BlinkLightRange()
    {
        while(true)
        {
            while (mLight.range <= mMaxLightRange)
            {
                mLight.range += Time.deltaTime * COEEFICIENT;
                yield return null;
            }

            mLight.range = mMaxLightRange;

            while (mLight.range >= MIN_LIGHT_RANGE)
            {
                mLight.range -= Time.deltaTime * COEEFICIENT;
                yield return null;
            }

            mLight.range = MIN_LIGHT_RANGE;

            yield return null;
        }
    }

    private IEnumerator BlinkLightIntensity()
    {
        while (true)
        {
            while (mLight.intensity <= mMaxLightIntensity)
            {
                mLight.intensity += Time.deltaTime * COEEFICIENT * 10;
                yield return null;
            }

            mLight.intensity = mMaxLightIntensity;

            while (mLight.intensity >= MIN_LIGHT_INTENSITY)
            {
                mLight.intensity -= Time.deltaTime * COEEFICIENT * 10;
                yield return null;
            }

            mLight.intensity = MIN_LIGHT_INTENSITY;

            yield return null;
        }
    }
}
