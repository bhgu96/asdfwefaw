﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OutsideOfView : MonoBehaviour
{
    [Range(0, 360)]
    [SerializeField]
    private float mViewAngle;
    [SerializeField]
    private float mViewRadius;
    [SerializeField]
    private LayerMask mObstacleMask;
    [SerializeField]
    private float mMeshResolution; //삼각형 메쉬의 그릴 간격
    [SerializeField]
    private int mEdgeResolveIteration; //가장자리 처리 이터레이션
    [SerializeField]
    private float mEdgeDstThreshold; //가장자리에 그릴 메쉬의 최대 도착지점

    private MeshFilter mViewMeshFilter;
    private Mesh viewMesh;

    public float ViewRadius { get { return mViewRadius; } private set { } }
    public float ViewAngle { get { return mViewAngle; } private set { } }

    public struct ViewCastInfo
    {
        public bool hit;
        public Vector3 point;
        public float dst;
        public float angle;

        public ViewCastInfo(bool _hit, Vector3 _point, float _dst, float _angle)
        {
            hit = _hit;
            point = _point;
            dst = _dst;
            angle = _angle;
        }
    }

    public struct EdgeInfo
    {
        public Vector3 pointA;
        public Vector3 pointB;

        public EdgeInfo(Vector3 _pointA, Vector3 _PointB)
        {
            pointA = _pointA;
            pointB = _PointB;
        }
    }

    private void Awake()
    {
        mViewMeshFilter = this.GetComponent<MeshFilter>();

        viewMesh = new Mesh();
        viewMesh.name = "View Mesh";
        mViewMeshFilter.mesh = viewMesh;

        DrawFieldOfView();
    }

    public Vector3 DirFromAngle(float angleInDegrees, bool angleIsGlobal)
    {
        if (!angleIsGlobal)
        {
            angleInDegrees += transform.eulerAngles.y;
        }

        return new Vector3(Mathf.Sin(angleInDegrees * Mathf.Deg2Rad), Mathf.Cos(angleInDegrees * Mathf.Deg2Rad), 0);
    }

    private void DrawFieldOfView()
    {
        int stepCount = Mathf.RoundToInt(mViewAngle * mMeshResolution);

        float stepAngleSize = mViewAngle / stepCount;

        List<Vector3> viewPoints = new List<Vector3>();

        ViewCastInfo oldViewCast = new ViewCastInfo();

        for (int i = 0; i <= stepCount; i++)
        {
            float angle = transform.eulerAngles.y - mViewAngle / 2 + stepAngleSize * i;
            ViewCastInfo newViewCast = ViewCast(angle);

            if (i > 0)
            {
                bool edgeDstThresholdExceded = Mathf.Abs(oldViewCast.dst - newViewCast.dst) > mEdgeDstThreshold;

                if (oldViewCast.hit != newViewCast.hit || (oldViewCast.hit && newViewCast.hit && edgeDstThresholdExceded))
                {
                    EdgeInfo edge = FindEdge(oldViewCast, newViewCast);
                    if (edge.pointA != Vector3.zero)
                    {
                        viewPoints.Add(edge.pointA);
                    }
                    if (edge.pointB != Vector3.zero)
                    {
                        viewPoints.Add(edge.pointB);
                    }
                }
            }

            viewPoints.Add(newViewCast.point);
            oldViewCast = newViewCast;
        }

        int vertexCount = viewPoints.Count + 1;

        Vector3[] vertices = new Vector3[vertexCount];
        int[] triangles = new int[(vertexCount - 2) * 3];

        vertices[0] = Vector3.zero;
        for (int i = 0; i < vertexCount - 1; i++)
        {
            vertices[i + 1] = this.transform.InverseTransformPoint(viewPoints[i]);

            if (i < vertexCount - 2)
            {
                triangles[i * 3] = 0;
                triangles[i * 3 + 1] = i + 1;
                triangles[i * 3 + 2] = i + 2;
            }
        }

        viewMesh.Clear();
        viewMesh.vertices = vertices;
        viewMesh.triangles = triangles;
        viewMesh.RecalculateNormals();
    }

    private EdgeInfo FindEdge(ViewCastInfo minViewCast, ViewCastInfo maxViewCast)
    {
        float minAngle = minViewCast.angle;
        float maxAngle = maxViewCast.angle;
        Vector3 minPoint = Vector3.zero;
        Vector3 maxPoint = Vector3.zero;

        for (int i = 0; i < mEdgeResolveIteration; i++)
        {
            float angle = (minAngle + maxAngle) / 2;
            ViewCastInfo newViewCast = ViewCast(angle);
            bool edgeDstThresholdExceded = Mathf.Abs(minViewCast.dst - newViewCast.dst) > mEdgeDstThreshold;

            if (newViewCast.hit == minViewCast.hit && !edgeDstThresholdExceded)
            {
                minAngle = angle;
                minPoint = newViewCast.point;
            }
            else
            {
                maxAngle = angle;
                maxPoint = newViewCast.point;
            }
        }

        return new EdgeInfo(minPoint, maxPoint);
    }

    private ViewCastInfo ViewCast(float globalAngle)
    {
        Vector3 dir = DirFromAngle(globalAngle, true);
        RaycastHit2D hit2D;

        if (Physics2D.Raycast(this.transform.position, dir, mViewRadius, mObstacleMask))
        {
            hit2D = Physics2D.Raycast(this.transform.position, dir, mViewRadius, mObstacleMask);
            return new ViewCastInfo(true, hit2D.point, hit2D.distance, globalAngle);
        }
        else
        {
            return new ViewCastInfo(false, transform.position + dir * mViewRadius, mViewRadius, globalAngle);
        }
    }
}
