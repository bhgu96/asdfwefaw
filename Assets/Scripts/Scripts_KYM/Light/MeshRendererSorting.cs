﻿using UnityEngine;

public class MeshRendererSorting : MonoBehaviour
{
    [SerializeField]
    private string mLayerName;
    [SerializeField]
    private int mSortingOrder;

    private MeshRenderer mMeshRenderer;

    private void Awake()
    {
        mMeshRenderer = this.GetComponent<MeshRenderer>();
        mMeshRenderer.sortingLayerName = mLayerName;
        mMeshRenderer.sortingOrder = mSortingOrder;
    }
}
