﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TerrainShadows : MonoBehaviour
{
    [Range(0, 360)]
    [SerializeField]
    private float mViewAngle;
    [SerializeField]
    private float mViewRadius;
    [SerializeField]
    private LayerMask mObstacleMask;
    [SerializeField]
    private float mMeshResolution; //삼각형 메쉬의 그릴 간격
    [SerializeField]
    private int mEdgeResolveIteration; //가장자리 처리 이터레이션
    [SerializeField]
    private float mEdgeDstThreshold; //가장자리에 그릴 메쉬의 최대 도착지점
    [SerializeField]
    private TerrainShadows mInsideShadow; //터레인 안쪽 mesh

    private MeshFilter mViewMeshFilter;
    private Mesh viewMesh;
    private Vector3 mPrevPosition;

    public List<Vector2> mViewPointList;
    public float ViewRadius { get { return mViewRadius; } private set { } }
    public float ViewAngle { get { return mViewAngle; } private set { } }

    public class ViewCastInfo
    {
        public bool hit;
        public Vector3 point;
        public float dst;
        public float angle;

        public ViewCastInfo()
        {

        }

        public ViewCastInfo(bool _hit, Vector3 _point, float _dst, float _angle)
        {
            hit = _hit;
            point = _point;
            dst = _dst;
            angle = _angle;
        }
    }

    public struct EdgeInfo
    {
        public Vector2 pointA;
        public Vector2 pointB;

        public EdgeInfo(Vector2 _pointA, Vector2 _PointB)
        {
            pointA = _pointA;
            pointB = _PointB;
        }
    }

    private void Awake()
    {
        mViewPointList = new List<Vector2>();
        mViewMeshFilter = this.GetComponent<MeshFilter>();

        viewMesh = new Mesh();
        viewMesh.name = "Terrain Shadow Mesh";
        mViewMeshFilter.mesh = viewMesh;

        GetVertexMesh();
    }

    private void Start()
    {
        DrawOutsideShadow();
        mViewPointList.Clear();
    }

    public Vector3 DirFromAngle(float angleInDegrees, bool angleIsGlobal)
    {
        if (!angleIsGlobal)
        {
            angleInDegrees += transform.eulerAngles.y;
        }

        return new Vector3(Mathf.Sin(angleInDegrees * Mathf.Deg2Rad), Mathf.Cos(angleInDegrees * Mathf.Deg2Rad));
    }

    private void DrawOutsideShadow()
    {
        if(mInsideShadow != null)
        {
            for (int i = 0; i < mInsideShadow.mViewPointList.Count; i++)
            {
                mViewPointList.Remove(mInsideShadow.mViewPointList[i]);
            }
        }

        int vertexCount = mViewPointList.Count + 1;

        Vector3[] vertices = new Vector3[vertexCount];
        int[] triangles = new int[(vertexCount - 2) * 3];

        vertices[0] = Vector3.zero;
        for (int i = 0; i < vertexCount - 1; i++)
        {
            vertices[i + 1] = this.transform.InverseTransformPoint(mViewPointList[i]);

            if (i < vertexCount - 2)
            {
                triangles[i * 3] = 0;
                triangles[i * 3 + 1] = i + 1;
                triangles[i * 3 + 2] = i + 2;
            }
        }

        viewMesh.Clear();
        viewMesh.vertices = vertices;
        viewMesh.triangles = triangles;
        viewMesh.RecalculateNormals();
        //if(mInsideShadow != null)
        //{
        //    for (int i = 0; i < mInsideShadow.mViewPointList.Count; i++)
        //    {
        //        mViewPointList.Remove(mInsideShadow.mViewPointList[i]);
        //    }

        //    int vertexCount = mViewPointList.Count + 1;

        //    Vector3[] vertices = new Vector3[vertexCount];
        //    int[] triangles = new int[(vertexCount - 2) * 3];

        //    vertices[0] = Vector3.zero;
        //    for (int i = 0; i < vertexCount - 1; i++)
        //    {
        //        vertices[i + 1] = this.transform.InverseTransformPoint(mViewPointList[i]);

        //        if (i < vertexCount - 2)
        //        {
        //            triangles[i * 3] = 0;
        //            triangles[i * 3 + 1] = i + 1;
        //            triangles[i * 3 + 2] = i + 2;
        //        }
        //    }

        //    viewMesh.Clear();
        //    viewMesh.vertices = vertices;
        //    viewMesh.triangles = triangles;
        //    viewMesh.RecalculateNormals();
        //}
    }

    private void GetVertexMesh()
    {
        int stepCount = Mathf.RoundToInt(mViewAngle * mMeshResolution);
        float stepAngleSize = mViewAngle / stepCount;
        ViewCastInfo oldViewCast = new ViewCastInfo();

        for (int i = 0; i <= stepCount; i++)
        {
            float angle = transform.eulerAngles.y - mViewAngle / 2 + stepAngleSize * i;
            ViewCastInfo newViewCast = ViewCast(angle);

            if (i > 0)
            {
                bool isEdgeDstThresholdExceded = Mathf.Abs(oldViewCast.dst - newViewCast.dst) > mEdgeDstThreshold;

                if (oldViewCast.hit != newViewCast.hit || (oldViewCast.hit && newViewCast.hit && isEdgeDstThresholdExceded))
                {
                    EdgeInfo edge = FindEdge(oldViewCast, newViewCast);
                    if (edge.pointA != Vector2.zero)
                    {
                        mViewPointList.Add(edge.pointA);
                    }
                    if (edge.pointB != Vector2.zero)
                    {
                        mViewPointList.Add(edge.pointB);
                    }
                }
            }

            mViewPointList.Add(newViewCast.point);
            oldViewCast = newViewCast;
        }
    }

    private EdgeInfo FindEdge(ViewCastInfo minViewCast, ViewCastInfo maxViewCast)
    {
        float minAngle = minViewCast.angle;
        float maxAngle = maxViewCast.angle;
        Vector2 minPoint = Vector2.zero;
        Vector2 maxPoint = Vector2.zero;

        for (int i = 0; i < mEdgeResolveIteration; i++)
        {
            float angle = (minAngle + maxAngle) / 2;
            ViewCastInfo newViewCast = ViewCast(angle);
            bool edgeDstThresholdExceded = Mathf.Abs(minViewCast.dst - newViewCast.dst) > mEdgeDstThreshold;

            if (newViewCast.hit == minViewCast.hit && !edgeDstThresholdExceded)
            {
                minAngle = angle;
                minPoint = newViewCast.point;
            }
            else
            {
                maxAngle = angle;
                maxPoint = newViewCast.point;
            }
        }

        return new EdgeInfo(minPoint, maxPoint);
    }

    private ViewCastInfo ViewCast(float globalAngle)
    {
        Vector3 dir = DirFromAngle(globalAngle, true);
        RaycastHit2D hit2D;

        if (Physics2D.Raycast(this.transform.position, dir, mViewRadius, mObstacleMask))
        {
            hit2D = Physics2D.Raycast(this.transform.position, dir, mViewRadius, mObstacleMask);
            return new ViewCastInfo(true, hit2D.point, hit2D.distance, globalAngle);
        }
        else
        {
            return new ViewCastInfo(false, transform.position + dir * mViewRadius, mViewRadius, globalAngle);
        }
    }
}
