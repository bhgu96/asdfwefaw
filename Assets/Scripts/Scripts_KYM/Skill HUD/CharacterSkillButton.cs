﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CharacterSkillButton : MonoBehaviour
{
    [SerializeField]
    private ECharacterType mCharacterType;
    [SerializeField]
    private ESkillsType mSkillsType;
    [SerializeField]
    private GameObject mSkillSlots;

    private UIButton mButton;
    private SkillSlotInfo[] mSkillSlotInfos;

    private void Awake()
    {
        mButton = this.GetComponent<UIButton>();
        mButton.onConfirm += ChangeCharacterSkill;
        mSkillSlotInfos = mSkillSlots.GetComponentsInChildren<SkillSlotInfo>();
    }

    private void ChangeCharacterSkill()
    {
        foreach(SkillSlotInfo skillslot in mSkillSlotInfos)
        {
            skillslot.CharacterType = mCharacterType;
            skillslot.SkillType = mSkillsType;
            skillslot.ChangeSkillType(mCharacterType);
        }
    }
}
