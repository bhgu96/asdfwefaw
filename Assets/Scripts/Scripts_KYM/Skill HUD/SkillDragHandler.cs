﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class SkillDragHandler : MonoBehaviour, IDragHandler, IEndDragHandler, IBeginDragHandler, IDropHandler
{
    [SerializeField]
    private ESkillSlotType mSkillSlotType;
    [SerializeField]
    private Transform mMouseTransform;
    [SerializeField]
    private RectTransform mOriginParentTransform;
    [SerializeField]
    private int mEquipIndex;

    private RectTransform mRectTransform;
    private SkillSlotInfo mSkillSlotInfo;

    private void Awake()
    {
        mOriginParentTransform = this.transform.parent.GetComponent<RectTransform>();
        mRectTransform = this.GetComponent<RectTransform>();
        mSkillSlotInfo = this.GetComponent<SkillSlotInfo>();
    }

    public void OnBeginDrag(PointerEventData eventData)
    {
        if(mSkillSlotType == ESkillSlotType.Equip)
        {
            return;
        }

        if(mSkillSlotInfo.SkillType == ESkillsType.Active)
        {
            this.GetComponent<CanvasGroup>().blocksRaycasts = false;
        }
    }

    public void OnDrag(PointerEventData eventData)
    {
        if (mSkillSlotType == ESkillSlotType.Equip)
        {
            return;
        }


        if (mSkillSlotInfo.IsLearnSkill(eventData.selectedObject.GetComponent<SkillSlotInfo>().CharacterType))
        {
            if (mSkillSlotInfo.IsSkillTypeActive(eventData.selectedObject.GetComponent<SkillSlotInfo>().CharacterType))
            {
                this.transform.SetParent(mMouseTransform);
                Vector3 tempVector = Camera.main.ScreenToWorldPoint(Input.mousePosition);
                mRectTransform.position = new Vector3(tempVector.x, tempVector.y, 10f);
            }
        }
    }

    public void OnEndDrag(PointerEventData eventData)
    {
        if (mSkillSlotType == ESkillSlotType.Equip)
        {
            return;
        }

        if (mSkillSlotInfo.SkillType == ESkillsType.Active)
        {
            this.transform.SetParent(mOriginParentTransform);
            mRectTransform.anchoredPosition = Vector2.zero;
            mRectTransform.localPosition = new Vector3(mRectTransform.localPosition.x, mRectTransform.localPosition.y, 0f);

            this.GetComponent<CanvasGroup>().blocksRaycasts = true;
        }
    }

    public void OnDrop(PointerEventData eventData)
    {
        if (mSkillSlotType == ESkillSlotType.None)
        {
            return;
        }

        if (this.GetComponent<SkillSlotType>() != null)
        {
            if (eventData.selectedObject.GetComponent<SkillSlotInfo>().IsLearnSkill(eventData.selectedObject.GetComponent<SkillSlotInfo>().CharacterType))
            {
                if (eventData.selectedObject.GetComponent<SkillSlotInfo>().SkillType == ESkillsType.Active)
                {
                    if (!eventData.selectedObject.GetComponent<SkillSlotInfo>().IsEquipSkill(eventData.selectedObject.GetComponent<SkillSlotInfo>().CharacterType))
                    {
                        if (eventData.selectedObject.GetComponent<SkillSlotInfo>().CharacterType == this.GetComponent<SkillSlotType>().CharacterType)
                        {
                            //만약 이미 장착중이라면
                            if(this.GetComponent<SkillSlotType>().ConnectSkill != null)
                            {
                                this.GetComponent<SkillSlotType>().ConnectSkill.EquipSkill(this.gameObject.GetComponent<SkillSlotType>().CharacterType, false, this.gameObject.GetComponent<SkillSlotType>().EquipIndex);
                                eventData.selectedObject.GetComponent<SkillSlotInfo>().EquipSkill(eventData.selectedObject.GetComponent<SkillSlotInfo>().CharacterType, true, mEquipIndex);
                                this.GetComponent<SkillSlotType>().ConnectSkill = eventData.selectedObject.GetComponent<SkillSlotInfo>();
                                this.GetComponent<Image>().sprite = eventData.selectedObject.GetComponent<Image>().sprite;
                            }
                            else
                            {
                                eventData.selectedObject.GetComponent<SkillSlotInfo>().EquipSkill(eventData.selectedObject.GetComponent<SkillSlotInfo>().CharacterType, true, mEquipIndex);
                                this.GetComponent<Image>().sprite = eventData.selectedObject.GetComponent<Image>().sprite;
                                this.GetComponent<SkillSlotType>().ConnectSkill = eventData.selectedObject.GetComponent<SkillSlotInfo>();
                            }
                        }
                    }
                }
            }
        }
    }
}

public enum ESkillSlotType
{
    None = -1,
    Equip,
    Count
}

