﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SkillSlotType : MonoBehaviour
{
    public ECharacterType CharacterType { get { return mCharacterType; } private set { } }
    public Sprite DefaultSprite { get { return mDefaultSprite; } private set { } }
    public SkillSlotInfo ConnectSkill { get { return mConnectSkill; } set { mConnectSkill = value; } }
    public int EquipIndex { get { return mEquipIndex; } private set { } }

    [SerializeField]
    private ECharacterType mCharacterType;
    [SerializeField]
    private Sprite mDefaultSprite;
    [SerializeField]
    private GameObject mSkillTooltip;
    [SerializeField]
    private int mNormalizeIndex;
    [SerializeField]
    private int mEquipIndex;
    [SerializeField]
    private GameObject mSkillSlots;

    private SkillSlotInfo mConnectSkill;

    private void OnEnable()
    {
        SkillSlotInfo[] skillslotInfos = mSkillSlots.GetComponentsInChildren<SkillSlotInfo>();

        if (mCharacterType == ECharacterType.Fairy)
        {
            foreach(SkillSlotInfo skillSlot in skillslotInfos)
            {
                skillSlot.CharacterType = ECharacterType.Fairy;
                skillSlot.ChangeSkillType(mCharacterType);
            }
        }
        else if(mCharacterType == ECharacterType.Giant)
        {
            foreach (SkillSlotInfo skillSlot in skillslotInfos)
            {
                skillSlot.CharacterType = ECharacterType.Giant;
                skillSlot.ChangeSkillType(mCharacterType);
            }
        }
    }

    private void Start()
    {
        if(mCharacterType == ECharacterType.Fairy)
        {
            if (Fairy_Player.Main.SkillFrame.Peek(mEquipIndex) != null)
            {
                this.GetComponent<Image>().sprite = Fairy_Player.Main.SkillFrame.Peek(mEquipIndex).Icon;

                //해제를 위한 ConnectSkill
                SkillSlotInfo[] tempSkillSlots = mSkillSlots.GetComponentsInChildren<SkillSlotInfo>();
                for (int i = 0; i < tempSkillSlots.Length; i++)
                {
                    if(Fairy_Player.Main.SkillFrame.Peek(mEquipIndex).Type == tempSkillSlots[i].FairySkillType)
                    {
                        mConnectSkill = tempSkillSlots[i];
                        mConnectSkill.SetInformation();
                        break;
                    }
                }
            }
        }
        else if(mCharacterType == ECharacterType.Giant)
        {
            if (Giant_Player.Main.SkillFrame.Peek(mEquipIndex) != null)
            {
                this.GetComponent<Image>().sprite = Giant_Player.Main.SkillFrame.Peek(mEquipIndex).Icon;

                //해제를 위한 ConnectSkill
                SkillSlotInfo[] tempSkillSlots = mSkillSlots.GetComponentsInChildren<SkillSlotInfo>();
                for (int i = 0; i < tempSkillSlots.Length; i++)
                {
                    if (Giant_Player.Main.SkillFrame.Peek(mEquipIndex).Type == tempSkillSlots[i].GiantSkillType)
                    {
                        mConnectSkill = tempSkillSlots[i];
                        mConnectSkill.SetInformation();
                        break;
                    }
                }
            }
        }
    }

    //장착한 스킬 팝업창
    public void PopupSkillInfo(bool isPopup, Vector2 position)
    {
        if(mConnectSkill.FairySkill == null)
        {
            return;
        }

        if (isPopup)
        {
            mSkillTooltip.GetComponent<TooltipHud>().IsContainItem = true;

            mSkillTooltip.SetActive(true);

            Vector2 tempPosition = Vector2.zero;

            if (mNormalizeIndex >= 3)
            {
                tempPosition = new Vector2(position.x + 240f, position.y - 120f);
            }
            else
            {
                tempPosition = new Vector2(position.x - 400f, position.y - 120f);
            }

            if(mCharacterType == ECharacterType.Fairy)
            {
                mSkillTooltip.GetComponent<TooltipHud>().SetTooltip(mConnectSkill.FairySkill.Name, mConnectSkill.FairySkill.Description);
            }
            else if(mCharacterType == ECharacterType.Giant)
            {
                mSkillTooltip.GetComponent<TooltipHud>().SetTooltip(mConnectSkill.GiantSkill.Name, mConnectSkill.GiantSkill.Description);
            }

            mSkillTooltip.GetComponent<RectTransform>().anchoredPosition = tempPosition;
        }
        else if (!isPopup)
        {
            mSkillTooltip.GetComponent<TooltipHud>().IsContainItem = false;
            mSkillTooltip.SetActive(false);
        }
    }

    public void PopupSkillInfo(bool isPopup)
    {
        if (!isPopup)
        {
            mSkillTooltip.GetComponent<TooltipHud>().IsContainItem = false;
            mSkillTooltip.SetActive(false);
        }
    }
}
