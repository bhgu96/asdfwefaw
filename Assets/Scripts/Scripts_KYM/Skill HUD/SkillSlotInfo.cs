﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SkillSlotInfo : MonoBehaviour
{
    public ISkill_Fairy_Player FairySkill { get { return mFairySkill; } private set { } }
    public ISkill_Giant_Player GiantSkill { get { return mGiantSkill; } private set { } }
    public ESkillType_Fairy_Player FairySkillType { get { return mFairySkillType; } private set { } }
    public ESkillType_Giant_Player GiantSkillType { get { return mGiantSkillType; } private set { } }
    public ECharacterType CharacterType { get { return mCharacterType; } set { mCharacterType = value; } }
    public ESkillsType SkillType { get { return mSkillType; }  set { mSkillType = value; } }

    [SerializeField]
    private ESkillType_Fairy_Player mFairySkillType;
    [SerializeField]
    private ESkillType_Giant_Player mGiantSkillType;
    [SerializeField]
    private ESkillsType mSkillType;
    [SerializeField]
    private ECharacterType mCharacterType;
    [SerializeField]
    private Sprite mNullImage;
    [SerializeField]
    private GameObject mEquipMask;
    [SerializeField]
    private GameObject mLearnMask;
    [SerializeField]
    private GameObject mSkillTooltip;
    [SerializeField]
    private int mNormalizeIndex;

    private ISkill_Fairy_Player mFairySkill;
    private ISkill_Giant_Player mGiantSkill;

    private Image mImage;
    private Dictionary<ECharacterType, SkillInfo> mCharacterSkillDict;

    private void Awake()
    {
        if(mCharacterSkillDict == null)
        {
            mCharacterSkillDict = new Dictionary<ECharacterType, SkillInfo>();

            mFairySkill = Fairy_Player.Main.SkillHandler.Get(mFairySkillType);
            mGiantSkill = Giant_Player.Main.SkillHandler.Get(mGiantSkillType);

            if (mFairySkill != null)
            {
                mCharacterSkillDict.Add(ECharacterType.Fairy, new SkillInfo(mFairySkill.Icon, mEquipMask, mLearnMask, ESkillsType.Active));
            }
            else if(mFairySkill == null)
            {
                mCharacterSkillDict.Add(ECharacterType.Fairy, new SkillInfo(mNullImage, mEquipMask, mLearnMask, ESkillsType.Active));
            }

            if (mGiantSkill != null)
            {
                mCharacterSkillDict.Add(ECharacterType.Giant, new SkillInfo(mGiantSkill.Icon, mEquipMask, mLearnMask, ESkillsType.Active));
            }
            else if(mGiantSkill == null)
            {
                mCharacterSkillDict.Add(ECharacterType.Giant, new SkillInfo(mNullImage, mEquipMask, mLearnMask, ESkillsType.Active));
            }


            if (mCharacterType == ECharacterType.Fairy)
            {
                if (mCharacterSkillDict[ECharacterType.Fairy].IsLearn)
                {
                    mCharacterSkillDict[ECharacterType.Fairy].LearnMask.SetActive(false);
                }
                else if (!mCharacterSkillDict[ECharacterType.Fairy].IsLearn)
                {
                    mCharacterSkillDict[ECharacterType.Fairy].LearnMask.SetActive(true);
                }

                if (mCharacterSkillDict[ECharacterType.Fairy].IsEquip)
                {
                    mCharacterSkillDict[ECharacterType.Fairy].EquipMask.SetActive(true);
                }
                else if (!mCharacterSkillDict[ECharacterType.Giant].IsEquip)
                {
                    mCharacterSkillDict[ECharacterType.Fairy].EquipMask.SetActive(false);
                }
            }
            else if (mCharacterType == ECharacterType.Giant)
            {
                if (mCharacterSkillDict[ECharacterType.Giant].IsLearn)
                {
                    mCharacterSkillDict[ECharacterType.Giant].LearnMask.SetActive(false);
                }
                else if (!mCharacterSkillDict[ECharacterType.Giant].IsLearn)
                {
                    mCharacterSkillDict[ECharacterType.Giant].LearnMask.SetActive(true);
                }

                if (mCharacterSkillDict[ECharacterType.Giant].IsEquip)
                {
                    mCharacterSkillDict[ECharacterType.Giant].EquipMask.SetActive(true);
                }
                else if (!mCharacterSkillDict[ECharacterType.Giant].IsEquip)
                {
                    mCharacterSkillDict[ECharacterType.Giant].EquipMask.SetActive(false);
                }
            }
        }

        mImage = this.GetComponent<Image>();
    }

    private void Start()
    {
        if (mCharacterType == ECharacterType.Fairy)
        {
            for (ESkillType_Fairy_Player e = ESkillType_Fairy_Player.None + 1; e < ESkillType_Fairy_Player.Count; e++)
            {
                if (e == mFairySkillType)
                {
                    ISkill_Fairy_Player skill = Fairy_Player.Main.SkillHandler.Get(e);

                    for (int i = 0; i < 3; i++)
                    {
                        if (Fairy_Player.Main.SkillFrame.Peek(i) == skill)
                        {
                            if (skill.IsMounted)
                            {
                                // 장착된 상태다
                                EquipSkill(mCharacterType, true, i);
                                break;
                            }
                        }
                    }
                }
            }
        }
        else if (mCharacterType == ECharacterType.Giant)
        {
            for (ESkillType_Giant_Player e = ESkillType_Giant_Player.None + 1; e < ESkillType_Giant_Player.Count; e++)
            {
                if (e == mGiantSkillType)
                {
                    ISkill_Giant_Player skill = Giant_Player.Main.SkillHandler.Get(e);

                    for (int i = 0; i < 3; i++)
                    {
                        if (Giant_Player.Main.SkillFrame.Peek(i) == skill)
                        {
                            if (skill.IsMounted)
                            {
                                // 장착된 상태다
                                EquipSkill(mCharacterType, true, i);
                                break;
                            }
                        }
                    }
                }
            }
        }
    }

    private void Update()
    {
        if(mCharacterSkillDict[mCharacterType] != null)
        {
            mImage.sprite = mCharacterSkillDict[mCharacterType].SkillIcon;

            if(mCharacterType == ECharacterType.Fairy && mFairySkillType != ESkillType_Fairy_Player.None)
            {
                ISkill_Fairy_Player fairySkill = Fairy_Player.Main.SkillHandler.Get(mFairySkillType);

                for(int i = 0; i < 3; i++)
                {
                    if(Fairy_Player.Main.SkillFrame.Peek(i) == fairySkill)
                    {
                        mCharacterSkillDict[mCharacterType].EquipMask.SetActive(true);
                        break;
                    }
                }
            }
            else if(mCharacterType == ECharacterType.Giant && mGiantSkillType != ESkillType_Giant_Player.None)
            {
                ISkill_Giant_Player giantSkill = Giant_Player.Main.SkillHandler.Get(mGiantSkillType);

                for (int i = 0; i < 3; i++)
                {
                    if (Giant_Player.Main.SkillFrame.Peek(i) == giantSkill)
                    {
                        mCharacterSkillDict[mCharacterType].EquipMask.SetActive(true);
                        break;
                    }
                }
            }
        }
    }

    public void ChangeSkillType(ECharacterType characterType)
    {
        if (mCharacterSkillDict == null)
        {
            return;
        }

        if (characterType == ECharacterType.Fairy)
        {
            if (mCharacterSkillDict[ECharacterType.Fairy].IsLearn)
            {
                mCharacterSkillDict[ECharacterType.Fairy].LearnMask.SetActive(false);
            }
            else if (!mCharacterSkillDict[ECharacterType.Fairy].IsLearn)
            {
                mCharacterSkillDict[ECharacterType.Fairy].LearnMask.SetActive(true);
            }

            if (mCharacterSkillDict[ECharacterType.Fairy].IsEquip)
            {
                mCharacterSkillDict[ECharacterType.Fairy].EquipMask.SetActive(true);
            }
            else if (!mCharacterSkillDict[ECharacterType.Fairy].IsEquip)
            {
                mCharacterSkillDict[ECharacterType.Fairy].EquipMask.SetActive(false);
            }
        }
        else if (characterType == ECharacterType.Giant)
        {
            if (mCharacterSkillDict[ECharacterType.Giant].IsLearn)
            {
                mCharacterSkillDict[ECharacterType.Giant].LearnMask.SetActive(false);
            }
            else if (!mCharacterSkillDict[ECharacterType.Giant].IsLearn)
            {
                mCharacterSkillDict[ECharacterType.Giant].LearnMask.SetActive(true);
            }

            if (mCharacterSkillDict[ECharacterType.Giant].IsEquip)
            {
                mCharacterSkillDict[ECharacterType.Giant].EquipMask.SetActive(true);
            }
            else if (!mCharacterSkillDict[ECharacterType.Giant].IsEquip)
            {
                mCharacterSkillDict[ECharacterType.Giant].EquipMask.SetActive(false);
            }
        }
    }

    public bool IsLearnSkill(ECharacterType characterType)
    {
        return mCharacterSkillDict[characterType].IsLearn;
    }

    public void LearnSkill(ECharacterType characterType, bool isLearn, int index)
    {
        mCharacterSkillDict[characterType].IsLearn = isLearn;

        //스킬 배움(봉인 해방) 코드 삽입
        if(characterType == ECharacterType.Fairy)
        {
            mFairySkill.UnSeal();
        }
        else if(characterType == ECharacterType.Giant)
        {
            mGiantSkill.UnSeal();
        }
    }

    public bool IsEquipSkill(ECharacterType characterType)
    {
        return mCharacterSkillDict[characterType].IsEquip;
    }

    public void EquipSkill(ECharacterType characterType, bool isEquip, int index)
    {
        mCharacterSkillDict[characterType].IsEquip = isEquip;

        //스킬 장착 코드 삽입
        if(isEquip)
        {
            if (characterType == ECharacterType.Fairy)
            {
                Fairy_Player.Main.SkillFrame.Mount(mFairySkill, index);
            }
            else if(characterType == ECharacterType.Giant)
            {
                Giant_Player.Main.SkillFrame.Mount(mGiantSkill, index);
            }
        }
        //스킬 해제 코드 삽입
        else if(!isEquip)
        {
            if (characterType == ECharacterType.Fairy)
            {
                Fairy_Player.Main.SkillFrame.UnMount(index);
            }
            else if (characterType == ECharacterType.Giant)
            {
                Giant_Player.Main.SkillFrame.UnMount(index);
            }
        }
    }

    public bool IsSkillTypeActive(ECharacterType characterType)
    {
        return mCharacterSkillDict[characterType].SkillType == ESkillsType.Active;
    }

    //스킬 팝업 나오기
    public void PopupSkillInfo(bool isPopup, Vector2 position)
    {
        if(mFairySkill == null && mGiantSkill == null)
        {
            return;
        }

        if(isPopup)
        {
            mSkillTooltip.GetComponent<TooltipHud>().IsContainItem = true;
            mSkillTooltip.SetActive(true);
            Vector2 tempPosition = Vector2.zero;

            if (mNormalizeIndex > 3)
            {
                if(position.y > 0)
                {
                    tempPosition = new Vector2(position.x - 240f, position.y - 120f);
                }
                else if(position.y <= 0)
                {
                    tempPosition = new Vector2(position.x - 240f, position.y + 120f);
                }
            }
            else
            {
                if (position.y > 0)
                {
                    tempPosition = new Vector2(position.x + 360f, position.y - 120f);
                }
                else if (position.y <= 0)
                {
                    tempPosition = new Vector2(position.x + 360f, position.y + 120f);
                }
            }

            if (mCharacterType == ECharacterType.Fairy)
            {
                mSkillTooltip.GetComponent<TooltipHud>().SetTooltip(mFairySkill.Name, mFairySkill.Description);
            }
            else if (mCharacterType == ECharacterType.Giant)
            {
                mSkillTooltip.GetComponent<TooltipHud>().SetTooltip(mGiantSkill.Name, mGiantSkill.Description);
            }

            mSkillTooltip.GetComponent<RectTransform>().anchoredPosition = tempPosition;
        }
        else if(!isPopup)
        {
            mSkillTooltip.GetComponent<TooltipHud>().IsContainItem = false;
            mSkillTooltip.SetActive(false);
        }
    }

    public void PopupSkillInfo(bool isPopup)
    {
        if (!isPopup)
        {
            mSkillTooltip.GetComponent<TooltipHud>().IsContainItem = false;
            mSkillTooltip.SetActive(false);
        }
    }

    //미리 할당
    public void SetInformation()
    {
        mCharacterSkillDict = new Dictionary<ECharacterType, SkillInfo>();

        mFairySkill = Fairy_Player.Main.SkillHandler.Get(mFairySkillType);
        mGiantSkill = Giant_Player.Main.SkillHandler.Get(mGiantSkillType);

        if (mFairySkill != null)
        {
            mCharacterSkillDict.Add(ECharacterType.Fairy, new SkillInfo(mFairySkill.Icon, mEquipMask, mLearnMask, ESkillsType.Active));
        }
        else if (mFairySkill == null)
        {
            mCharacterSkillDict.Add(ECharacterType.Fairy, new SkillInfo(mNullImage, mEquipMask, mLearnMask, ESkillsType.Active));
        }

        if (mGiantSkill != null)
        {
            mCharacterSkillDict.Add(ECharacterType.Giant, new SkillInfo(mGiantSkill.Icon, mEquipMask, mLearnMask, ESkillsType.Active));
        }
        else if (mGiantSkill == null)
        {
            mCharacterSkillDict.Add(ECharacterType.Giant, new SkillInfo(mNullImage, mEquipMask, mLearnMask, ESkillsType.Active));
        }


        if (mCharacterType == ECharacterType.Fairy)
        {
            if (mCharacterSkillDict[ECharacterType.Fairy] != null)
            {
                if (mCharacterSkillDict[ECharacterType.Fairy].IsLearn)
                {
                    mCharacterSkillDict[ECharacterType.Fairy].LearnMask.SetActive(false);
                }
                else if (!mCharacterSkillDict[ECharacterType.Fairy].IsLearn)
                {
                    mCharacterSkillDict[ECharacterType.Fairy].LearnMask.SetActive(true);
                }

                if (mCharacterSkillDict[ECharacterType.Fairy].IsEquip)
                {
                    mCharacterSkillDict[ECharacterType.Fairy].EquipMask.SetActive(true);
                }
                else if (!mCharacterSkillDict[ECharacterType.Fairy].IsEquip)
                {
                    mCharacterSkillDict[ECharacterType.Fairy].EquipMask.SetActive(false);
                }
            }
        }
        else if (mCharacterType == ECharacterType.Giant)
        {
            if(mCharacterSkillDict[ECharacterType.Giant] != null)
            {
                if (mCharacterSkillDict[ECharacterType.Giant].IsLearn)
                {
                    mCharacterSkillDict[ECharacterType.Giant].LearnMask.SetActive(false);
                }
                else if (!mCharacterSkillDict[ECharacterType.Giant].IsLearn)
                {
                    mCharacterSkillDict[ECharacterType.Giant].LearnMask.SetActive(true);
                }

                if (mCharacterSkillDict[ECharacterType.Giant].IsEquip)
                {
                    mCharacterSkillDict[ECharacterType.Giant].EquipMask.SetActive(true);
                }
                else if (!mCharacterSkillDict[ECharacterType.Giant].IsEquip)
                {
                    mCharacterSkillDict[ECharacterType.Giant].EquipMask.SetActive(false);
                }
            }
        }

        if (mCharacterType == ECharacterType.Fairy)
        {
            for (ESkillType_Fairy_Player e = ESkillType_Fairy_Player.None + 1; e < ESkillType_Fairy_Player.Count; e++)
            {
                if (e == mFairySkillType)
                {
                    ISkill_Fairy_Player skill = Fairy_Player.Main.SkillHandler.Get(e);

                    for (int i = 0; i < 3; i++)
                    {
                        if (Fairy_Player.Main.SkillFrame.Peek(i) == skill)
                        {
                            if (skill.IsMounted)
                            {
                                // 장착된 상태다
                                EquipSkill(mCharacterType, true, i);
                                break;
                            }
                        }
                    }
                }
            }
        }
        else if (mCharacterType == ECharacterType.Giant)
        {
            for (ESkillType_Giant_Player e = ESkillType_Giant_Player.None + 1; e < ESkillType_Giant_Player.Count; e++)
            {
                if (e == mGiantSkillType)
                {
                    ISkill_Giant_Player skill = Giant_Player.Main.SkillHandler.Get(e);

                    for (int i = 0; i < 3; i++)
                    {
                        if (Giant_Player.Main.SkillFrame.Peek(i) == skill)
                        {
                            if (skill.IsMounted)
                            {
                                // 장착된 상태다
                                EquipSkill(mCharacterType, true, i);
                                break;
                            }
                        }
                    }
                }
            }
        }
    }
}

public enum ESkillsType
{
    None = -1,
    Active,
    Count
}

