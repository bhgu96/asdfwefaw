﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CharacterSkillSlotHud : AHud
{
    public int EquipIndex { get { return mIndex; } private set { } }
    public ECharacterType CharacterType { get { return mCharacterType; } private set { } }

    [SerializeField]
    private Sprite mTransparentImage;
    [SerializeField]
    private ECharacterType mCharacterType;
    [SerializeField]
    private int mIndex;
    [SerializeField]
    private GameObject mCooltimeObject;

    private Image mImage;

    private void Awake()
    {
        mImage = this.GetComponent<Image>();
    }

    public override void Activate()
    {

    }

    public override void Deactivate()
    {

    }

    public override void Express()
    {
        if(mCharacterType == ECharacterType.Fairy)
        {
            if(Fairy_Player.Main.SkillFrame.Peek(mIndex) == null)
            {
                mImage.sprite = mTransparentImage;
            }
            else
            {
                mImage.sprite = Fairy_Player.Main.SkillFrame.Peek(mIndex).Icon;

                if(Fairy_Player.Main.SkillFrame.Peek(mIndex).IsActivated && mCooltimeObject != null)
                {
                    mCooltimeObject.SetActive(true);
                }
            }
        }
        else if(mCharacterType == ECharacterType.Giant)
        {
            if (Giant_Player.Main.SkillFrame.Peek(mIndex) == null)
            {
                mImage.sprite = mTransparentImage;
            }
            else
            {
                mImage.sprite = Giant_Player.Main.SkillFrame.Peek(mIndex).Icon;

                if (Giant_Player.Main.SkillFrame.Peek(mIndex).IsActivated && mCooltimeObject != null)
                {
                    mCooltimeObject.SetActive(true);
                }
            }
        }
    }

    public override void GetInformation()
    {

    }
}
