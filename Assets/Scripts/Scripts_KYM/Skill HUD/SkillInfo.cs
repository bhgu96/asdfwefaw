﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SkillInfo
{
    public bool IsEquip
    {
        get
        {
            return mIsEquip;
        }
        set
        {
            if (mSkillType == ESkillsType.Active)
            {
                mIsEquip = value;

                if (mIsEquip)
                {
                    mEquipMask.SetActive(true);
                }
                else
                {
                    mEquipMask.SetActive(false);
                }
            }
        }
    }
    public bool IsLearn
    {
        get
        {
            return mIsLearn;
        }
        set
        {
            mIsLearn = true;
            mLearnMask.SetActive(false);
        }
    }

    public Sprite SkillIcon { get { return mSkillIcon; } set { mSkillIcon = value; } }
    public GameObject EquipMask { get { return mEquipMask; } set { mEquipMask = value; } }
    public GameObject LearnMask { get { return mLearnMask; } set { mLearnMask = value; } }
    public ESkillsType SkillType { get { return mSkillType; } set { mSkillType = value; } }

    private bool mIsEquip;
    private bool mIsLearn;
    private Sprite mSkillIcon;
    private GameObject mEquipMask;
    private GameObject mLearnMask;
    private ESkillsType mSkillType;


    public SkillInfo(Sprite icon, GameObject equipMask, GameObject learnMask, ESkillsType skillType)
    {
        mSkillIcon = icon;
        mEquipMask = equipMask;
        mLearnMask = learnMask;
        mSkillType = skillType;

        //스킬 구현이 다 됬다면 밑의 코드는 삭제한다.
        mIsLearn = true;
    }
}
