﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Prototype_SlopeScrolling : MonoBehaviour
{
    [SerializeField]
    private Transform mTopTransform;
    [SerializeField]
    private Transform mBottomTransform;

    private Rigidbody2D mRigidbody2D;
    private Vector3 mTopVector;
    private Vector3 mBottomVector;


    private void Awake()
    {
        mRigidbody2D = this.GetComponent<Rigidbody2D>();

        mTopVector = mTopTransform.position;
    }

    private void FixedUpdate()
    {
        mRigidbody2D.velocity = new Vector2(-10f, +10f);

        if(this.transform.position.x <= mTopVector.x)
        {
            this.transform.position = mBottomTransform.parent.transform.position + mBottomTransform.localPosition;
        }
    }
}
