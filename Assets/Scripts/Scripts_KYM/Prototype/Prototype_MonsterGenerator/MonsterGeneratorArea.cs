﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MonsterGeneratorArea : MonoBehaviour
{
    public static Vector3 TopDefensePosition { get { return mTopDefensePosition; } private set { } }
    public static Vector3 MiddleDefensePosition { get { return mMiddleDefensePosition; } private set { } }
    public static Vector3 BottomDefensePosition { get { return mBottomDefensePosition; } private set { } }

    public int MonsterCount
    {
        get
        {
            return mMonsterCount;
        }
        set
        {
            mMonsterCount = Mathf.Clamp(mMonsterCount, 1, 3);
            mMonsterCount = value;
        }
    }

    [SerializeField]
    private Transform mLeftBottom;
    [SerializeField]
    private Transform mRightTop;
    [SerializeField]
    private EMonsterPositionType mPositionType;
    [SerializeField]
    private int mMonsterCount;
    [SerializeField]
    private int mMonsterRatio;
    [SerializeField]
    private int mMaxMounterCount;

    private Vector3[] mMonsterPositionArray;

    private static Vector3 mTopDefensePosition;
    private static Vector3 mMiddleDefensePosition;
    private static Vector3 mBottomDefensePosition;

    private void Awake()
    {
        if(mPositionType == EMonsterPositionType.Top)
        {
            mTopDefensePosition = this.transform.GetChild(2).transform.position;
        }
        else if(mPositionType == EMonsterPositionType.Middle)
        {
            mMiddleDefensePosition = this.transform.GetChild(2).transform.position;
        }
        else if(mPositionType == EMonsterPositionType.Bottom)
        {
            mBottomDefensePosition = this.transform.GetChild(2).transform.position;
        }


        mMonsterCount = Mathf.Clamp(mMonsterCount, 1, 3);
        mMonsterPositionArray = new Vector3[mMonsterCount * mMonsterRatio];

        float xInterval = (mRightTop.position.x - mLeftBottom.position.x) / (mMonsterCount * mMonsterRatio);
        float yPosition = (mRightTop.position.y + mLeftBottom.position.y) * 0.5f;

        for(int i = 0; i < mMonsterCount * mMonsterRatio; i++)
        {
            mMonsterPositionArray[i] = new Vector3(mLeftBottom.position.x + (xInterval * (i + 1)) - xInterval * 0.5f, yPosition);
        }
    }

    public void StartGenerate(int waveCount)
    {
        int difficulty = 0;

        for (int i = 0; i < mMonsterPositionArray.Length; i++)
        {
            difficulty = Random.Range((int)(waveCount * 0.1f), /*(int)Mathf.Log(waveCount, 2f)*/8);
            difficulty = Mathf.Clamp(difficulty, 0, 8);

            PrototypeMonsterPool.PopMonster(mMonsterPositionArray[i], mPositionType, difficulty);
        }
    }

    public void ResizeArea()
    {
        if (mPositionType == EMonsterPositionType.Top)
        {
            mTopDefensePosition = this.transform.GetChild(2).transform.position;
        }
        else if (mPositionType == EMonsterPositionType.Middle)
        {
            mMiddleDefensePosition = this.transform.GetChild(2).transform.position;
        }
        else if (mPositionType == EMonsterPositionType.Bottom)
        {
            mBottomDefensePosition = this.transform.GetChild(2).transform.position;
        }

        mMonsterCount = Mathf.Clamp(mMonsterCount, 1, 3);
        mMonsterPositionArray = new Vector3[mMonsterCount * mMonsterRatio];

        float xInterval = (mRightTop.position.x - mLeftBottom.position.x) / (mMonsterCount * mMonsterRatio);
        float yPosition = (mRightTop.position.y + mLeftBottom.position.y) * 0.5f;

        for (int i = 0; i < mMonsterCount * mMonsterRatio; i++)
        {
            mMonsterPositionArray[i] = new Vector3(mLeftBottom.position.x + (xInterval * (i + 1)) - xInterval * 0.5f, yPosition);
        }
    }

    private void OnDrawGizmos()
    {
        Gizmos.DrawCube(new Vector3((mRightTop.position.x + mLeftBottom.position.x) * 0.5f, (mRightTop.position.y + mLeftBottom.position.y) * 0.5f)
            , new Vector3(mRightTop.position.x - mLeftBottom.position.x, mRightTop.position.y - mLeftBottom.position.y));

        Gizmos.color = new Color(0.5f,1f,0.3f,0.5f);
    }
}
