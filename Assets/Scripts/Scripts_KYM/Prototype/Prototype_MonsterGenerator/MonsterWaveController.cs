﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MonsterWaveController : MonoBehaviour
{
    [SerializeField]
    private Text mWaveText;

    public static int mWaveCount;

    private bool mIsStartWave;
    private MonsterGeneratorArea[] mGenerators;

    private void Awake()
    {
        mWaveCount = 0;
        mGenerators = this.GetComponentsInChildren<MonsterGeneratorArea>();
    }

    private void Update()
    {
        if (PrototypeMonsterPool.mTopMonsterCount <= 0 
            && PrototypeMonsterPool.mMiddleMonsterCount <= 0 
            && PrototypeMonsterPool.mBottomMonsterCount <= 0 
            && !mIsStartWave)
        {
            mWaveCount++;
            StartCoroutine(StartWave());
        }
    }

    private int mGenerateCount = 0;

    private IEnumerator StartWave()
    {
        mIsStartWave = true;

        //몬스터 카운트 늘리기
        if(Mathf.Log(mWaveCount, 4) == mGenerateCount  && mWaveCount != 1)
        {
            for (int i = 0; i < mGenerators.Length; i++)
            {
                mGenerators[i].MonsterCount++;
                mGenerators[i].ResizeArea();
            }
            mGenerateCount++;
        }


        mWaveText.text = "Wave : " + mWaveCount.ToString();

        for(float a = 0; a <=1; a += Time.deltaTime)
        {
            mWaveText.color = new Color(1, 1, 1, a);
            yield return null;
        }

        for (float a = 1; a >= 0; a -= Time.deltaTime)
        {
            mWaveText.color = new Color(1, 1, 1, a);
            yield return null;
        }

        mWaveText.color = new Color(1, 1, 1, 0);

        yield return new WaitForSeconds(0.5f);


        for(int i = 0; i < mGenerators.Length; i++)
        {
            mGenerators[i].StartGenerate(mWaveCount);
        }

        yield return null;

        mIsStartWave = false;
    }
}
