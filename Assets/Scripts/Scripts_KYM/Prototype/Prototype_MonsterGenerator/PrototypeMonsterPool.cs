﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PrototypeMonsterPool : MonoBehaviour
{
    [SerializeField]
    private int mMonsterCount;
    [SerializeField]
    private GameObject mMonster00;
    [SerializeField]
    private GameObject mMonster01;
    [SerializeField]
    private GameObject mMonster02;
    [SerializeField]
    private GameObject mMonster03;
    [SerializeField]
    private GameObject mMonster04;
    [SerializeField]
    private GameObject mMonster05;
    [SerializeField]
    private GameObject mMonster06;
    [SerializeField]
    private GameObject mMonster07;

    private static Dictionary<int, GameObject[]> mMonsterDIct;

    private static GameObject[] mMonster00Array;
    private static GameObject[] mMonster01Array;
    private static GameObject[] mMonster02Array;
    private static GameObject[] mMonster03Array;
    private static GameObject[] mMonster04Array;
    private static GameObject[] mMonster05Array;
    private static GameObject[] mMonster06Array;
    private static GameObject[] mMonster07Array;
    private static int mCurrentMonster00Index;
    private static int mCurrentMonster01Index;
    private static int mCurrentMonster02Index;
    private static int mCurrentMonster03Index;
    private static int mCurrentMonster04Index;
    private static int mCurrentMonster05Index;
    private static int mCurrentMonster06Index;
    private static int mCurrentMonster07Index;

    public static int mTopMonsterCount;
    public static int mMiddleMonsterCount;
    public static int mBottomMonsterCount;

    private void Awake()
    {
        mTopMonsterCount = 0;
        mMiddleMonsterCount = 0;
        mBottomMonsterCount = 0;

        mCurrentMonster00Index = 0;
        mCurrentMonster01Index = 0;
        mCurrentMonster02Index = 0;
        mCurrentMonster03Index = 0;
        mCurrentMonster04Index = 0;
        mCurrentMonster05Index = 0;
        mCurrentMonster06Index = 0;
        mCurrentMonster07Index = 0;
        mMonster00Array = new GameObject[mMonsterCount];
        mMonster01Array = new GameObject[mMonsterCount];
        mMonster02Array = new GameObject[mMonsterCount];
        mMonster03Array = new GameObject[mMonsterCount];
        mMonster04Array = new GameObject[mMonsterCount];
        mMonster05Array = new GameObject[mMonsterCount];
        mMonster06Array = new GameObject[mMonsterCount];
        mMonster07Array = new GameObject[mMonsterCount];

        mMonsterDIct = new Dictionary<int, GameObject[]>();

        for(int i = 0; i < mMonsterCount; i++)
        {
            mMonster00Array[i] = Instantiate(mMonster00, this.transform);
            mMonster00Array[i].SetActive(false);

            mMonster01Array[i] = Instantiate(mMonster01, this.transform);
            mMonster01Array[i].SetActive(false);

            mMonster02Array[i] = Instantiate(mMonster02, this.transform);
            mMonster02Array[i].SetActive(false);

            mMonster03Array[i] = Instantiate(mMonster03, this.transform);
            mMonster03Array[i].SetActive(false);

            mMonster04Array[i] = Instantiate(mMonster04, this.transform);
            mMonster04Array[i].SetActive(false);

            mMonster05Array[i] = Instantiate(mMonster05, this.transform);
            mMonster05Array[i].SetActive(false);

            mMonster06Array[i] = Instantiate(mMonster06, this.transform);
            mMonster06Array[i].SetActive(false);

            mMonster07Array[i] = Instantiate(mMonster07, this.transform);
            mMonster07Array[i].SetActive(false);
        }

        mMonsterDIct.Add(0, mMonster00Array);
        mMonsterDIct.Add(1, mMonster01Array);
        mMonsterDIct.Add(2, mMonster02Array);
        mMonsterDIct.Add(3, mMonster03Array);
        mMonsterDIct.Add(4, mMonster04Array);
        mMonsterDIct.Add(5, mMonster05Array);
        mMonsterDIct.Add(6, mMonster06Array);
        mMonsterDIct.Add(7, mMonster07Array);
    }

    public static void PopMonster(Vector3 monsterPosition, EMonsterPositionType positionType, int difficulty)
    {
        if(mCurrentMonster00Index == mMonster00Array.Length)
        {
            mCurrentMonster00Index = 0;
        }

        if (mCurrentMonster01Index == mMonster01Array.Length)
        {
            mCurrentMonster01Index = 0;
        }

        if (mCurrentMonster02Index == mMonster02Array.Length)
        {
            mCurrentMonster02Index = 0;
        }

        if (mCurrentMonster03Index == mMonster03Array.Length)
        {
            mCurrentMonster03Index = 0;
        }

        if (mCurrentMonster04Index == mMonster04Array.Length)
        {
            mCurrentMonster04Index = 0;
        }

        if (mCurrentMonster05Index == mMonster05Array.Length)
        {
            mCurrentMonster05Index = 0;
        }

        if (mCurrentMonster06Index == mMonster06Array.Length)
        {
            mCurrentMonster06Index = 0;
        }

        if (mCurrentMonster07Index == mMonster07Array.Length)
        {
            mCurrentMonster07Index = 0;
        }

        if (difficulty == 0)
        {
            mMonsterDIct[difficulty][mCurrentMonster00Index].GetComponent<PrototypeMonster>().PositionType = positionType;
            mMonsterDIct[difficulty][mCurrentMonster00Index].transform.position = monsterPosition;
            mMonsterDIct[difficulty][mCurrentMonster00Index].SetActive(true);
            mCurrentMonster00Index++;
        }
        else if(difficulty == 1)
        {
            mMonsterDIct[difficulty][mCurrentMonster01Index].GetComponent<PrototypeMonster>().PositionType = positionType;
            mMonsterDIct[difficulty][mCurrentMonster01Index].transform.position = monsterPosition;
            mMonsterDIct[difficulty][mCurrentMonster01Index].SetActive(true);
            mCurrentMonster01Index++;
        }
        else if(difficulty == 2)
        {
            mMonsterDIct[difficulty][mCurrentMonster02Index].GetComponent<PrototypeMonster>().PositionType = positionType;
            mMonsterDIct[difficulty][mCurrentMonster02Index].transform.position = monsterPosition;
            mMonsterDIct[difficulty][mCurrentMonster02Index].SetActive(true);
            mCurrentMonster02Index++;
        }
        else if (difficulty == 3)
        {
            mMonsterDIct[difficulty][mCurrentMonster03Index].GetComponent<PrototypeMonster>().PositionType = positionType;
            mMonsterDIct[difficulty][mCurrentMonster03Index].transform.position = monsterPosition;
            mMonsterDIct[difficulty][mCurrentMonster03Index].SetActive(true);
            mCurrentMonster03Index++;
        }
        else if (difficulty == 4)
        {
            mMonsterDIct[difficulty][mCurrentMonster04Index].GetComponent<PrototypeMonster>().PositionType = positionType;
            mMonsterDIct[difficulty][mCurrentMonster04Index].transform.position = monsterPosition;
            mMonsterDIct[difficulty][mCurrentMonster04Index].SetActive(true);
            mCurrentMonster04Index++;
        }
        else if (difficulty == 5)
        {
            mMonsterDIct[difficulty][mCurrentMonster05Index].GetComponent<PrototypeMonster>().PositionType = positionType;
            mMonsterDIct[difficulty][mCurrentMonster05Index].transform.position = monsterPosition;
            mMonsterDIct[difficulty][mCurrentMonster05Index].SetActive(true);
            mCurrentMonster05Index++;
        }
        else if (difficulty == 6)
        {
            mMonsterDIct[difficulty][mCurrentMonster06Index].GetComponent<PrototypeMonster>().PositionType = positionType;
            mMonsterDIct[difficulty][mCurrentMonster06Index].transform.position = monsterPosition;
            mMonsterDIct[difficulty][mCurrentMonster06Index].SetActive(true);
            mCurrentMonster06Index++;
        }
        else if (difficulty == 7)
        {
            mMonsterDIct[difficulty][mCurrentMonster07Index].GetComponent<PrototypeMonster>().PositionType = positionType;
            mMonsterDIct[difficulty][mCurrentMonster07Index].transform.position = monsterPosition;
            mMonsterDIct[difficulty][mCurrentMonster07Index].SetActive(true);
            mCurrentMonster07Index++;
        }


        if (positionType == EMonsterPositionType.Top)
        {
            mTopMonsterCount++;
        }
        else if(positionType == EMonsterPositionType.Middle)
        {
            mMiddleMonsterCount++;
        }
        else
        {
            mBottomMonsterCount++;
        }
    }
}

public enum EMonsterPositionType
{
    None = -1,
    Top,
    Middle,
    Bottom,
    Count
}

