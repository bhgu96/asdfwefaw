﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Prototype_ShielderDefense : MonoBehaviour
{
    public Vector3 DefensePosition { get { return mDefensePosition; } set { mDefensePosition = value; } }

    private Vector3 mInitPosition;
    private PrototypeMonster mMonster;
    private Vector3 mDefensePosition;

    private Vector3 mInitOffset;
    private Vector3 mInitSize;

    private void Awake()
    {
        mMonster = this.GetComponent<PrototypeMonster>();
        mInitOffset = this.GetComponent<BoxCollider2D>().offset;
        mInitSize = this.GetComponent<BoxCollider2D>().size;
    }

    private void OnEnable()
    {
        mInitPosition = this.transform.position;
    }

    private void OnDisable()
    {
        mDefensePosition = Vector3.zero;
    }

    private void StartDefense()
    {
        //쉴드 범위 넓게?
        this.gameObject.GetComponent<BoxCollider2D>().offset = new Vector2(21.43838f, -0.04502487f);
        this.gameObject.GetComponent<BoxCollider2D>().size = new Vector2(50.66338f, 6.753284f);

        StartCoroutine(SetDefenseMode());
    }

    private IEnumerator SetDefenseMode()
    {
        this.transform.position = mDefensePosition;

        for (float t = 0; t <= 3f; t += Time.deltaTime)
        {
            if(mMonster.IsStuck)
            {
                break;
            }
            yield return null;
        }

        if(!mMonster.IsStuck)
        {
            this.transform.position = mInitPosition;
        }

        yield return null;

        mMonster.IsDefense = false;

        this.gameObject.GetComponent<BoxCollider2D>().offset = mInitOffset;
        this.gameObject.GetComponent<BoxCollider2D>().size = mInitSize;
    }
}
