﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PrototypeMonster : MonoBehaviour
{
    public EMonsterPositionType PositionType { get { return mPositionType; } set { mPositionType = value; } }
    public bool IsStuck { get { return mIsStuck; } private set { } }
    public bool IsInvincible { get { return mIsInvincible; } set { mIsInvincible = value; } }
    public bool IsDefense { get { return mIsDefense; } set { mIsDefense = value; } }
    public float MonsterDamage { get { return mMonsterDamage; } private set { } }

    [SerializeField]
    private EMonsterCategorys mMonsterCategory;
    [SerializeField]
    private EMonsterAttackCategorys mMonsterAttackCategory;

    [SerializeField]
    private int mShieldCount;
    [SerializeField]
    private Transform mFirePosition;
    [SerializeField]
    private int mMonsterIndex;
    [SerializeField]
    private float mMonsterDamage;
    [SerializeField]
    private Animator mShieldAnimator;

    private float mAttackWaitTime;
    private float mAttackInterval;
    private Rigidbody2D mRigidbody2D;
    private Animator mAnimator;
    private int mDefalutShieldCount;
    private bool mIsStuck;
    private bool mIsDie;
    private bool mIsInvincible;
    private bool mIsDefense;
    private EMonsterPositionType mPositionType;

    private void Awake()
    {
        mDefalutShieldCount = mShieldCount;
        mRigidbody2D = this.GetComponent<Rigidbody2D>();
        mAnimator = this.GetComponent<Animator>();
    }

    private void Start()
    {
        mAttackWaitTime = Random.Range(1, 6);
        mAttackInterval = Random.Range(0.2f, 2f);
    }

    private void OnEnable()
    {
        if(mMonsterAttackCategory == EMonsterAttackCategorys.Range)
        {
            StartCoroutine(RangeAttack());
        }
        else if(mMonsterAttackCategory == EMonsterAttackCategorys.Melee)
        {
            StartCoroutine(RushAttack());
        }
        else if(mMonsterAttackCategory == EMonsterAttackCategorys.Defense)
        {
            StartCoroutine(DefenseAttack());
        }
    }

    private void OnDisable()
    {
        mIsDie = false;
        mIsStuck = false;
        mIsInvincible = false;
        mShieldCount = mDefalutShieldCount;
        mRigidbody2D.velocity = Vector2.zero;
        this.gameObject.GetComponent<BoxCollider2D>().isTrigger = false;
        mRigidbody2D.isKinematic = true;

        mAnimator.SetBool("IsDie", false);
    }

    private void OnMouseDown()
    {
        if(!mIsInvincible && !PrototypeFairy.Fairy.IsHit)
        {
            mShieldCount--;
            mShieldAnimator.SetTrigger("Hit");

            PrototypeFairy.Fairy.IsHit = true;

            //정령이 쏜 레이저 맞는다
            float deltaX = Mathf.Abs(PrototypeFairy.Fairy.transform.position.x - this.transform.position.x);
            float deltaY = Mathf.Abs(PrototypeFairy.Fairy.transform.position.y - this.transform.position.y);
            float radian = Mathf.Atan2(deltaY, deltaX);

            PrototypeFairy.Fairy.ProjectileLine.localRotation = Quaternion.Euler(0, 0, radian * 180 / Mathf.PI);

            float x = this.transform.position.x - PrototypeFairy.Fairy.transform.position.x;

            PrototypeFairy.Fairy.ProjectileLine.localScale = new Vector3(1f * (x / PrototypeFairy.Fairy.ProjectileLineLength), 1f);
            PrototypeFairy.Fairy.ProjectileHead.localScale = new Vector3(1f / (x / PrototypeFairy.Fairy.ProjectileLineLength), 1f);
            PrototypeFairy.Fairy.ProjectileExplosition.localScale = new Vector3(1f / (x / PrototypeFairy.Fairy.ProjectileLineLength), 1f);

            if (mShieldCount == 0)
            {
                mIsStuck = true;
                mRigidbody2D.isKinematic = false;
            }
        }
    }

    private void FixedUpdate()
    {
        if (mIsStuck)
        {
            mRigidbody2D.velocity = new Vector2(-10.0f, mRigidbody2D.velocity.y);

            if(mShieldAnimator != null)
            {
                mShieldAnimator.SetBool("IsStuck", true);
            }
        }
    }

    public void Die()
    {
        mIsDie = true;
        mIsStuck = false;
        mIsInvincible = false;
        mAnimator.SetBool("IsDie", mIsDie);

        if (mShieldAnimator != null)
        {
            mShieldAnimator.SetBool("IsStuck", false);
        }

        StartCoroutine(KnockBack());
    }

    private IEnumerator KnockBack()
    {
        mRigidbody2D.velocity = new Vector2(200f, 150f);

        yield return new WaitUntil(() => mRigidbody2D.velocity.x <= 0);

        mRigidbody2D.velocity = new Vector2(-10f, mRigidbody2D.velocity.y);

        yield return new WaitUntil(() => mRigidbody2D.velocity.x == 0);

        if (mPositionType == EMonsterPositionType.Top)
        {
            PrototypeMonsterPool.mTopMonsterCount--;
        }
        else if(mPositionType == EMonsterPositionType.Middle)
        {
            PrototypeMonsterPool.mMiddleMonsterCount--;
        }
        else if(mPositionType == EMonsterPositionType.Bottom)
        {
            PrototypeMonsterPool.mBottomMonsterCount--;
        }

        this.gameObject.SetActive(false);
    }

    private IEnumerator RangeAttack()
    {
        yield return null;

        if (mIsStuck || mIsDie || PrototypeGiant.IsDie)
        {
            yield break;
        }

        yield return new WaitForSeconds(mAttackWaitTime);

        if (mIsStuck || mIsDie || PrototypeGiant.IsDie)
        {
            yield break;
        }

        GameObject curProjectile = null;

        while (true)
        {
            if(mIsStuck || mIsDie || PrototypeGiant.IsDie)
            {
                yield break;
            }

            curProjectile = PrototypeProjectilePool.PopProjectile(mFirePosition.position, mMonsterIndex);

            mAnimator.SetBool("IsAttack", true);

            yield return new WaitUntil(() => !curProjectile.activeSelf);

            mAnimator.SetBool("IsAttack", false);

            if (mIsStuck || mIsDie || PrototypeGiant.IsDie)
            {
                yield break;
            }

            yield return new WaitForSeconds(mAttackInterval);
        }
    }

    private IEnumerator RushAttack()
    {
        yield return null;

        if (this.gameObject.GetComponent<Prototype_FlaskRushAttack>() != null)
        {
            this.gameObject.GetComponent<Prototype_FlaskRushAttack>().InitPosition = this.transform.position;
        }

        if (mIsStuck || mIsDie || PrototypeGiant.IsDie)
        {
            this.gameObject.GetComponent<BoxCollider2D>().isTrigger = false;
            yield break;
        }

        yield return new WaitForSeconds(mAttackWaitTime);

        if (mIsStuck || mIsDie || PrototypeGiant.IsDie)
        {
            this.gameObject.GetComponent<BoxCollider2D>().isTrigger = false;
            yield break;
        }

        while (true)
        {
            if (mIsStuck || mIsDie || PrototypeGiant.IsDie)
            {
                this.gameObject.GetComponent<BoxCollider2D>().isTrigger = false;
                yield break;
            }


            mAnimator.SetBool("IsAttack", true);
            mIsInvincible = true;

            yield return new WaitUntil(() => !mIsInvincible);

            mAnimator.SetBool("IsAttack", false);

            if (mIsStuck || mIsDie || PrototypeGiant.IsDie)
            {
                this.gameObject.GetComponent<BoxCollider2D>().isTrigger = false;
                yield break;
            }

            yield return new WaitForSeconds(mAttackInterval);
        }
    }

    private IEnumerator DefenseAttack()
    {
        if(mPositionType == EMonsterPositionType.Top)
        {
            this.GetComponent<Prototype_ShielderDefense>().DefensePosition = new Vector3(MonsterGeneratorArea.TopDefensePosition.x, MonsterGeneratorArea.TopDefensePosition.y);
        }
        else if(mPositionType == EMonsterPositionType.Middle)
        {
            this.GetComponent<Prototype_ShielderDefense>().DefensePosition = new Vector3(MonsterGeneratorArea.MiddleDefensePosition.x, MonsterGeneratorArea.MiddleDefensePosition.y);
        }
        else if(mPositionType == EMonsterPositionType.Bottom)
        {
            this.GetComponent<Prototype_ShielderDefense>().DefensePosition = new Vector3(MonsterGeneratorArea.BottomDefensePosition.x, MonsterGeneratorArea.BottomDefensePosition.y);
        }

        yield return null;

        if (mIsStuck || mIsDie || PrototypeGiant.IsDie)
        {
            yield break;
        }

        yield return new WaitForSeconds(mAttackWaitTime);

        if (mIsStuck || mIsDie || PrototypeGiant.IsDie)
        {
            yield break;
        }

        while (true)
        {
            if (mIsStuck || mIsDie || PrototypeGiant.IsDie)
            {
                yield break;
            }


            mAnimator.SetBool("IsAttack", true);
            mIsDefense = true;

            yield return new WaitUntil(() => !mIsDefense);

            mAnimator.SetBool("IsAttack", false);

            if (mIsStuck || mIsDie || PrototypeGiant.IsDie)
            {
                yield break;
            }

            yield return new WaitForSeconds(mAttackInterval);
        }
    }
}

public enum EMonsterCategorys
{
    None = -1,
    Tank,
    Dealer,
    Support,
    Count
}

public enum EMonsterAttackCategorys
{
    None = -1,
    Melee,
    Range,
    Defense,
    Count
}


