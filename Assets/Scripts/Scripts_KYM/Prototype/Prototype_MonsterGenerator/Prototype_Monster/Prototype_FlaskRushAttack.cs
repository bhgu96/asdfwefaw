﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Prototype_FlaskRushAttack : MonoBehaviour
{
    public Vector3 InitPosition { get { return mInitPosition; } set { mInitPosition = value; } }

    private Vector3 mInitPosition;
    private bool mIsAttackGiant;
    private PrototypeMonster mMonster;

    private void Awake()
    {
        mMonster = this.GetComponent<PrototypeMonster>();
    }

    private void OnEnable()
    {
        mIsAttackGiant = false;
    }

    private void OnDisable()
    {
        this.gameObject.layer = (int)ELayerFlags.Monster;
        this.gameObject.GetComponent<BoxCollider2D>().isTrigger = false;
        mIsAttackGiant = false;
    }

    private void StartRushAttack()
    {
        this.gameObject.layer = (int)ELayerFlags.MonsterAttack;
        this.gameObject.GetComponent<BoxCollider2D>().isTrigger = true;
        StartCoroutine(RushAttack());

    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.gameObject.layer == (int)ELayerFlags.Giant)
        {
            mIsAttackGiant = true;
        }
    }

    private IEnumerator RushAttack()
    {
        while(!mIsAttackGiant)
        {
            if(mMonster.IsStuck)
            {
                this.gameObject.layer = (int)ELayerFlags.Monster;
                this.gameObject.GetComponent<BoxCollider2D>().isTrigger = false;
                break;
            }

            this.transform.position = Vector3.Lerp(this.transform.position, PrototypeGiant.Giant.transform.position, 5f * Time.deltaTime);
            yield return null;
        }

        yield return new WaitForSeconds(0.1f);

        this.gameObject.layer = (int)ELayerFlags.Monster;
        this.gameObject.GetComponent<BoxCollider2D>().isTrigger = false;

        this.transform.position = mInitPosition;

        yield return null;

        mMonster.IsInvincible = false;
        mIsAttackGiant = false;
    }
}
