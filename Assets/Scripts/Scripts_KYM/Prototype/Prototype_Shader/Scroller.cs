﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Scroller : MonoBehaviour
{
    [SerializeField]
    private Material mScrollMaterial;
    [SerializeField]
    private float mScrollSpeed;
    [SerializeField]
    private Texture[] mTextures;

    private float texSizeRatio = 1;
    private float mScrollAmount;

    private void Start()
    {
        for(int i = 0; i < mTextures.Length; i++)
        {
            SetTexture(mTextures[i]);
        }
    }

    public void SetTexture(Texture tex)
    {
        var quadRatio = 42.77821f / 23.89387f;
        var texRatio = (float)tex.width / tex.height;
        texSizeRatio = quadRatio / texRatio;
        mScrollMaterial.SetTextureScale("_MainTex", new Vector2(texSizeRatio, 1));
        mScrollMaterial.SetTexture("_MainTex", tex);
    }

    public void Update()
    {
        if(!PrototypeGiant.IsDie)
        {
            mScrollAmount += Time.deltaTime * mScrollSpeed * (texSizeRatio);
            mScrollMaterial.SetFloat("_scrollAmount", mScrollAmount);
        }
    }
}
