﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PrototypeProjectile : MonoBehaviour
{
    public float Damage { get { return mDamage; } private set { } }
    public Animator Animator { get { return mAnimator; } private set { } }

    [SerializeField]
    private EPrototypeProjectileType mType;
    [SerializeField]
    private float mSpeed;
    [SerializeField]
    private float mDamage;

    private Rigidbody2D mRigidbody2D;
    private Animator mAnimator;

    private void Awake()
    {
        mRigidbody2D = this.GetComponent<Rigidbody2D>();
        mAnimator = this.GetComponent<Animator>();
    }

    private void OnEnable()
    {
        if (mType == EPrototypeProjectileType.Projectile)
        {
            float deltaX = Mathf.Abs(PrototypeGiant.Giant.transform.position.x - this.transform.position.x);
            float deltaY = Mathf.Abs(PrototypeGiant.Giant.transform.position.y - this.transform.position.y);
            float radian = Mathf.Atan2(deltaY, deltaX);

            this.transform.rotation = Quaternion.Euler(0, 0, radian * 180 / Mathf.PI);
            mRigidbody2D.velocity = (PrototypeGiant.Giant.transform.position - this.transform.position).normalized * mSpeed;
        }
        else if (mType == EPrototypeProjectileType.Summon)
        {

        }
    }

    private void OnDisable()
    {
        if(mType == EPrototypeProjectileType.Projectile)
        {
            mRigidbody2D.velocity = Vector2.zero;
            mAnimator.SetBool("IsHit", false);
        }
    }

    private void StopMove()
    {
        mRigidbody2D.velocity = Vector2.zero;
    }

    private void FinishEffect()
    {
        this.gameObject.SetActive(false);
    }
}

public enum EPrototypeProjectileType
{
    None = -1,
    Projectile,
    Summon,
    Count
}
