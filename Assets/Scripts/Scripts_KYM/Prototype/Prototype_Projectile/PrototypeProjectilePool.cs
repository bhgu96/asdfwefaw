﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PrototypeProjectilePool : MonoBehaviour
{
    [SerializeField]
    private int mProjectileCount;
    [SerializeField]
    private GameObject mMonsterProjectile00Object;
    [SerializeField]
    private GameObject mMonsterProjectile01Object;
    [SerializeField]
    private GameObject mMonsterProjectile02Object;
    [SerializeField]
    private GameObject mMonsterProjectile03Object;
    [SerializeField]
    private GameObject mMonsterProjectile04Object;
    [SerializeField]
    private GameObject mMonsterProjectile05Object;

    private static GameObject[] mMonsterProjectile00Array;
    private static GameObject[] mMonsterProjectile01Array;
    private static GameObject[] mMonsterProjectile02Array;
    private static GameObject[] mMonsterProjectile03Array;
    private static GameObject[] mMonsterProjectile04Array;
    private static GameObject[] mMonsterProjectile05Array;
    private static int mCurrentProjectile00Index;
    private static int mCurrentProjectile01Index;
    private static int mCurrentProjectile02Index;
    private static int mCurrentProjectile03Index;
    private static int mCurrentProjectile04Index;
    private static int mCurrentProjectile05Index;

    private static Dictionary<int, GameObject[]> mProjectileDict;

    private void Awake()
    {
        mMonsterProjectile00Array = new GameObject[mProjectileCount];
        mCurrentProjectile00Index = 0;

        mMonsterProjectile01Array = new GameObject[mProjectileCount];
        mCurrentProjectile01Index = 0;

        mMonsterProjectile02Array = new GameObject[mProjectileCount];
        mCurrentProjectile02Index = 0;

        mMonsterProjectile03Array = new GameObject[mProjectileCount];
        mCurrentProjectile03Index = 0;

        mMonsterProjectile04Array = new GameObject[mProjectileCount];
        mCurrentProjectile04Index = 0;

        mMonsterProjectile05Array = new GameObject[mProjectileCount];
        mCurrentProjectile05Index = 0;

        mProjectileDict = new Dictionary<int, GameObject[]>();

        for (int i = 0; i < mMonsterProjectile00Array.Length; i++)
        {
            mMonsterProjectile00Array[i] = Instantiate(mMonsterProjectile00Object, this.transform);
            mMonsterProjectile00Array[i].SetActive(false);

            mMonsterProjectile01Array[i] = Instantiate(mMonsterProjectile01Object, this.transform);
            mMonsterProjectile01Array[i].SetActive(false);

            mMonsterProjectile02Array[i] = Instantiate(mMonsterProjectile02Object, this.transform);
            mMonsterProjectile02Array[i].SetActive(false);

            mMonsterProjectile03Array[i] = Instantiate(mMonsterProjectile03Object, this.transform);
            mMonsterProjectile03Array[i].SetActive(false);

            mMonsterProjectile04Array[i] = Instantiate(mMonsterProjectile04Object, this.transform);
            mMonsterProjectile04Array[i].SetActive(false);

            mMonsterProjectile05Array[i] = Instantiate(mMonsterProjectile05Object, this.transform);
            mMonsterProjectile05Array[i].SetActive(false);
        }

        mProjectileDict.Add(0, mMonsterProjectile00Array);
        mProjectileDict.Add(1, mMonsterProjectile01Array);
        mProjectileDict.Add(2, mMonsterProjectile02Array);
        mProjectileDict.Add(3, mMonsterProjectile03Array);
        mProjectileDict.Add(4, mMonsterProjectile04Array);
        mProjectileDict.Add(7, mMonsterProjectile05Array);
    }

    public static GameObject PopProjectile(Vector3 projectilePosition, int monsterIndex)
    {
        if (mCurrentProjectile00Index == mMonsterProjectile00Array.Length)
        {
            mCurrentProjectile00Index = 0;
        }

        if(mCurrentProjectile01Index == mMonsterProjectile01Array.Length)
        {
            mCurrentProjectile01Index = 0;
        }

        if (mCurrentProjectile02Index == mMonsterProjectile02Array.Length)
        {
            mCurrentProjectile02Index = 0;
        }

        if (mCurrentProjectile03Index == mMonsterProjectile03Array.Length)
        {
            mCurrentProjectile03Index = 0;
        }

        if (mCurrentProjectile04Index == mMonsterProjectile04Array.Length)
        {
            mCurrentProjectile04Index = 0;
        }

        if (mCurrentProjectile05Index == mMonsterProjectile05Array.Length)
        {
            mCurrentProjectile05Index = 0;
        }

        if (monsterIndex == 0)
        {
            mMonsterProjectile00Array[mCurrentProjectile00Index].transform.position = projectilePosition;
            mMonsterProjectile00Array[mCurrentProjectile00Index].SetActive(true);
            mCurrentProjectile00Index++;

            int tmpIndex = mCurrentProjectile00Index - 1;
            tmpIndex = Mathf.Clamp(tmpIndex, 0, mMonsterProjectile00Array.Length);

            return mMonsterProjectile00Array[tmpIndex];
        }
        else if(monsterIndex == 1)
        {
            mMonsterProjectile01Array[mCurrentProjectile01Index].transform.position = projectilePosition;
            mMonsterProjectile01Array[mCurrentProjectile01Index].SetActive(true);
            mCurrentProjectile01Index++;

            int tmpIndex = mCurrentProjectile01Index - 1;
            tmpIndex = Mathf.Clamp(tmpIndex, 0, mMonsterProjectile01Array.Length);

            return mMonsterProjectile01Array[tmpIndex];
        }
        else if(monsterIndex == 2)
        {
            mMonsterProjectile02Array[mCurrentProjectile02Index].transform.position = projectilePosition;
            mMonsterProjectile02Array[mCurrentProjectile02Index].SetActive(true);
            mCurrentProjectile02Index++;

            int tmpIndex = mCurrentProjectile02Index - 1;
            tmpIndex = Mathf.Clamp(tmpIndex, 0, mMonsterProjectile02Array.Length);

            return mMonsterProjectile02Array[tmpIndex];
        }
        else if (monsterIndex == 3)
        {
            mMonsterProjectile03Array[mCurrentProjectile03Index].transform.position = projectilePosition;
            mMonsterProjectile03Array[mCurrentProjectile03Index].SetActive(true);
            mCurrentProjectile03Index++;

            int tmpIndex = mCurrentProjectile03Index - 1;
            tmpIndex = Mathf.Clamp(tmpIndex, 0, mMonsterProjectile03Array.Length);

            return mMonsterProjectile03Array[tmpIndex];
        }
        else if (monsterIndex == 4)
        {
            mMonsterProjectile04Array[mCurrentProjectile04Index].transform.position = projectilePosition;
            mMonsterProjectile04Array[mCurrentProjectile04Index].SetActive(true);
            mCurrentProjectile04Index++;

            int tmpIndex = mCurrentProjectile04Index - 1;
            tmpIndex = Mathf.Clamp(tmpIndex, 0, mMonsterProjectile04Array.Length);

            return mMonsterProjectile04Array[tmpIndex];
        }
        else if (monsterIndex == 5)
        {
            mMonsterProjectile04Array[mCurrentProjectile04Index].transform.position = projectilePosition;
            mMonsterProjectile04Array[mCurrentProjectile04Index].SetActive(true);
            mCurrentProjectile04Index++;

            int tmpIndex = mCurrentProjectile04Index - 1;
            tmpIndex = Mathf.Clamp(tmpIndex, 0, mMonsterProjectile04Array.Length);

            return mMonsterProjectile04Array[tmpIndex];
        }
        else if (monsterIndex == 7)
        {
            mMonsterProjectile05Array[mCurrentProjectile05Index].transform.position = projectilePosition;
            mMonsterProjectile05Array[mCurrentProjectile05Index].SetActive(true);
            mCurrentProjectile05Index++;

            int tmpIndex = mCurrentProjectile05Index - 1;
            tmpIndex = Mathf.Clamp(tmpIndex, 0, mMonsterProjectile05Array.Length);

            return mMonsterProjectile05Array[tmpIndex];
        }
        else
        {
            return null;
        }
    }
}
