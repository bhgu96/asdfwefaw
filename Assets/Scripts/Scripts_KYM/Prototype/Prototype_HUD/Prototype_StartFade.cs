﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Prototype_StartFade : MonoBehaviour
{
    private Image mImage;

    private void Awake()
    {
        mImage = this.GetComponent<Image>();
    }

    private void Start()
    {
        StartCoroutine(Fade());
    }

    private IEnumerator Fade()
    {
        for(float a = 1; a >= 0; a -=Time.deltaTime * 1.5f)
        {
            mImage.color = new Color(0, 0, 0, a);
            yield return null;
        }
    }
}
