﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Prototype_GameOver : MonoBehaviour
{
    [SerializeField]
    private GameObject mGameOverText;
    [SerializeField]
    private GameObject mRestartText;

    private Image mImage;
    private bool mIsGameOver;

    private void Awake()
    {
        mImage = this.GetComponent<Image>();
    }

    private void Update()
    {
        if(PrototypeGiant.IsDie && !mIsGameOver)
        {
            mIsGameOver = true;
            StartCoroutine(GameOver());
        }
    }

    private IEnumerator GameOver()
    {
        for(float a = 0; a <= 1; a += Time.deltaTime)
        {
            mImage.color = new Color(0, 0, 0, a);
            yield return null;
        }

        mGameOverText.SetActive(true);

        yield return new WaitForSeconds(1.0f);

        mRestartText.SetActive(true);

        yield return new WaitUntil(() => Input.GetKeyDown(KeyCode.R));

        SceneManager.LoadScene("_Scene_Abyss_KYM");
    }
}
