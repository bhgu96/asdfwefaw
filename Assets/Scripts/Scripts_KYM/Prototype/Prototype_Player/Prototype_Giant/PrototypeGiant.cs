﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PrototypeGiant : MonoBehaviour
{
    public static PrototypeGiant Giant { get { return mGiant; } }
    public float MaxHP
    {
        get
        {
            mMaxHP =Mathf.Clamp(mMaxHP, 0, mMaxHP);
            return mMaxHP;
        }
        private set
        {

        }
    }
    public float CurHP
    {
        get
        {
            mCurHP =Mathf.Clamp(mCurHP, 0, mMaxHP);
            return mCurHP;
        }
        set
        {
            mCurHP = value;

            if (mCurHP <= 0)
            {
                Die();
            }
        }
    }
    public static bool IsDie { get { return mIsDie; } set { mIsDie = value; } }

    private static PrototypeGiant mGiant;
    private static bool mIsDie;

    [SerializeField]
    private float mMaxHP;
    [SerializeField]
    private float mCurHP;
    [SerializeField]
    private Color mHitColor;

    private Animator mAnimator;
    private int mAttackIndex;
    private List<GameObject> mAttackedMonster;
    private bool mIsAttack;
    private SpriteRenderer mSpriteRenderer;

    private void Awake()
    {
        mCurHP = mMaxHP;

        mIsDie = false;

        mGiant = this;
        mAnimator = this.GetComponent<Animator>();
        mSpriteRenderer = this.GetComponent<SpriteRenderer>();
        mAttackedMonster = new List<GameObject>();
    }

    private void OnTriggerStay2D(Collider2D collision)
    {
        if (collision.gameObject.layer == (int)ELayerFlags.Monster)
        {
            mIsAttack = true;
            mAnimator.SetBool("IsAttack", mIsAttack);
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.gameObject.layer == (int)ELayerFlags.Monster)
        {
            mAttackedMonster.Add(collision.gameObject);

            mIsAttack = true;
            mAnimator.SetBool("IsAttack", mIsAttack);
            mAnimator.SetInteger("AttackIndex", GiantAttackIndex());
        }
        else if(collision.gameObject.layer == (int)ELayerFlags.MonsterAttack)
        {
            //체력 감소
            if(collision.gameObject.GetComponent<PrototypeProjectile>() != null)
            {
                //피격 애니메이션 재생
                mAnimator.SetTrigger("Hit");
                collision.gameObject.GetComponent<PrototypeProjectile>().Animator.SetBool("IsHit", true);

                if(!Prototype_DevelopKey.IsInvincible)
                {
                    mCurHP -= collision.gameObject.GetComponent<PrototypeProjectile>().Damage;
                    mSpriteRenderer.color = mHitColor;

                    if (mCurHP <= 0)
                    {
                        Die();
                    }
                }
            }
            else if(collision.gameObject.GetComponent<PrototypeProjectile>() == null )
            {
                //피격 애니메이션 재생
                mAnimator.SetTrigger("Hit");
                
                if(!Prototype_DevelopKey.IsInvincible)
                {
                    mCurHP -= collision.gameObject.GetComponent<PrototypeMonster>().MonsterDamage;
                    mSpriteRenderer.color = mHitColor;

                    if (mCurHP <= 0)
                    {
                        Die();
                    }
                }
            }
        }
    }

    private void Die()
    {
        //사망 애니메이션 재생
        mIsDie = true;
        mAnimator.SetBool("IsDie", mIsDie);
    }

    private int GiantAttackIndex()
    {
        mAttackIndex++;

        if(mAttackIndex > 1)
        {
            mAttackIndex = 0;
        }

        return mAttackIndex;
    }

    private void GiantStartAttack()
    {
        mIsAttack = false;
        mAnimator.SetBool("IsAttack", mIsAttack);
    }

    private void GiantAttackMonster()
    {
        GameObject curMonster = null;

        for(int i = 0; i < mAttackedMonster.Count; i++)
        {
            if(mAttackedMonster[i].activeSelf)
            {
                curMonster = mAttackedMonster[i];
                mAttackedMonster[i].GetComponent<PrototypeMonster>().Die();
                mAttackedMonster.Remove(curMonster);
            }
        }
    }

    private void EndHit()
    {
        mSpriteRenderer.color = new Color(1, 1, 1, 1);
    }
}
