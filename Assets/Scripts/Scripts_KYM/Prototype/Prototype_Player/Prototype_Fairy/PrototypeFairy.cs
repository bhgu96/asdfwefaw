﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PrototypeFairy : MonoBehaviour
{
    public static PrototypeFairy Fairy { get { return mFairy; } private set { } }
    public float MaxMana
    {
        get
        {
            Mathf.Clamp(mMaxMana, 0, mMaxMana);
            return mMaxMana;
        }
        private set
        {

        }
    }
    public float CurMana
    {
        get
        {
            Mathf.Clamp(mCurMana, 0, mCurMana);
            return mCurMana;
        }
        private set
        {

        }
    }
    public Animator Animator { get { return mAnimator; } private set { } }
    public Transform ProjectileLine { get { return mProjectileLine; } private set { } }
    public Transform ProjectileHead { get { return mProjectileHead; } private set { } }
    public Transform ProjectileExplosition { get { return mProjectileExplosition; } private set { } }
    public float ProjectileLineLength { get { return mProjectileLength; } private set { } }
    public bool IsHit { get { return mIsHit; } set { mIsHit = value; } }

    [SerializeField]
    private float mMaxMana;
    [SerializeField]
    private float mCurMana;
    [SerializeField]
    private Transform mProjectileLine;
    [SerializeField]
    private Transform mProjectileHead;
    [SerializeField]
    private Transform mProjectileExplosition;
    [SerializeField]
    private Transform mProjectileLeft;
    [SerializeField]
    private Transform mProjectileRight;

    private Animator mAnimator;
    private float mProjectileLength;
    private bool mIsHit;
    private static PrototypeFairy mFairy;

    private void Awake()
    {
        mProjectileLength = mProjectileRight.position.x - mProjectileLeft.position.x;
        mCurMana = mMaxMana;
        mFairy = this;
        mAnimator = this.GetComponent<Animator>();
    }

    private void Update()
    {
        if(Input.GetKeyDown(KeyCode.Mouse0) && mIsHit)
        {
            mAnimator.SetTrigger("Attack");
        }
    }

    private void Fire()
    {
        mProjectileLine.gameObject.SetActive(true);
    }

    private void EndFire()
    {
        mIsHit = false;
        mProjectileLine.gameObject.SetActive(false);
    }
}
