﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Prototype_DevelopKey : MonoBehaviour
{
    public static bool IsInvincible;

    private void Update()
    {
        if(Input.GetKey(KeyCode.LeftControl))
        {
            if(Input.GetKeyDown(KeyCode.H))
            {
                PrototypeGiant.Giant.CurHP += 100;
            }
            else if(Input.GetKeyDown(KeyCode.I) && !IsInvincible)
            {
                IsInvincible = true;
            }
            else if (Input.GetKeyDown(KeyCode.I) && IsInvincible)
            {
                IsInvincible = false;
            }
            else if(Input.GetKeyDown(KeyCode.R))
            {
                SceneManager.LoadScene("_Scene_Abyss_KYM");
            }
            else if(Input.GetKeyDown(KeyCode.D))
            {
                PrototypeGiant.Giant.CurHP -= 10000;
            }
            else if(Input.GetKeyDown(KeyCode.Escape))
            {
                Application.Quit();
            }
        }
    }
}
