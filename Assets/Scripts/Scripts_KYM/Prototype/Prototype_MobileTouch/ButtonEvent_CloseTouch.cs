﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ButtonEvent_CloseTouch : MonoBehaviour
{
    private UIButton mButton;

    private void Awake()
    {
        mButton = this.GetComponent<UIButton>();
        mButton.onConfirm += ConfirmButton;
    }

    private void ConfirmButton()
    {
        mButton.UIController.ClosePanel();
    }
}
