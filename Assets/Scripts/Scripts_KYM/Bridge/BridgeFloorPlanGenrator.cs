﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BridgeFloorPlanGenrator : MonoBehaviour
{
    [SerializeField]
    private RectTransform mSanctuaryTransform;
    [SerializeField]
    private RectTransform mEndAbyssTransform;
    [SerializeField]
    private GameObject mFloorObject;
    [SerializeField]
    private Color[] mStageColor;
    [SerializeField]
    private int mStageCount; //스테이지 하나당 방 개수

    private List<GameObject> mStageList = new List<GameObject>();
    private int mColumn = 5; //↓, 아마 5층이 될듯
    private int mRow; // → , 총 방 개수 - (Column - 1)]
    private float mFloorPlanSizeX;
    private float mFloorPlanSizeY;

    private void Awake()
    {
        mRow = mStageCount * mColumn - (mColumn - 1);
        mFloorPlanSizeX = mEndAbyssTransform.anchoredPosition.x - mSanctuaryTransform.anchoredPosition.x;
        mFloorPlanSizeY = mSanctuaryTransform.anchoredPosition.y - mEndAbyssTransform.anchoredPosition.y;

        GenerateFloorPlan();
        SetPlayerStage();
    }

    private void GenerateFloorPlan()
    {
        GameObject tempObject = null;
        int tempIndex = 0;

        for (int i = 0; i < mColumn * mStageCount; i++)
        {
            tempObject = Instantiate(mFloorObject, this.transform);
            tempObject.GetComponent<Image>().color = mStageColor[tempIndex];
            mStageList.Add(tempObject);

            if (i > 0)
            {
                if ((i + 1) % mStageCount == 0)
                {
                    tempIndex++;
                }
            }
        }

        mFloorObject.SetActive(false);

        //배치 시작
        float xInterval = (mFloorObject.GetComponent<RectTransform>().sizeDelta.x / 8f) + (mFloorPlanSizeX / mRow);
        float yInterval = (mFloorPlanSizeY / mColumn) - (mFloorObject.GetComponent<RectTransform>().sizeDelta.y / 3f);

        float startXPosition = mFloorObject.GetComponent<RectTransform>().anchoredPosition.x;
        float startYPosition = mFloorObject.GetComponent<RectTransform>().anchoredPosition.y;

        int tmpIndex = 0;
        Vector2 prevPosition = new Vector2(startXPosition, startYPosition);

        for(int i = 0; i < mColumn; i++)
        {
            for (int j = 0; j < mStageCount; j++)
            {
                if(i != 0)
                {
                    if(j == 0)
                    {
                        mStageList[tmpIndex].GetComponent<RectTransform>().anchoredPosition = new Vector2(prevPosition.x, startYPosition - i * yInterval);
                    }
                    else
                    {
                        mStageList[tmpIndex].GetComponent<RectTransform>().anchoredPosition = new Vector2(startXPosition + j * xInterval, startYPosition - i * yInterval);
                    }
                }
                else
                {
                    mStageList[tmpIndex].GetComponent<RectTransform>().anchoredPosition = new Vector2(startXPosition + j * xInterval, startYPosition - i * yInterval);
                }

                
                prevPosition = mStageList[tmpIndex].GetComponent<RectTransform>().anchoredPosition;
                tmpIndex++;
            }

            startXPosition = prevPosition.x;
        }


    }

    private void SetPlayerStage()
    {
        //0번 자식오브젝트 : clear
        //1번 자식오브젝트 : 플레이어 있음
        //2번 자식오브젝트 : 아직 안간 지역
        mStageList[0].transform.GetChild(1).gameObject.SetActive(true);
        mStageList[0].transform.GetChild(2).gameObject.SetActive(false);
    }
}
