﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChamberInfo : MonoBehaviour
{
    public Transform RightTop { get { return mRightTop; } private set { } }
    public Transform LeftBottom { get { return mLeftBottom; } private set { } }
    public EChamberFlag ChamberFlag { get { return mChamberFlag; } private set { } }


    [SerializeField]
    private Transform mRightTop;
    [SerializeField]
    private Transform mLeftBottom;
    [SerializeField]
    private EChamberFlag mChamberFlag;
}

public enum EChamberFlag
{
    None = -1,
    Normal,
    Shop,
    Boss,
    Count
}
