﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MinimapHud2 : AHud
{
    private const float EDGE_COEFFICIENT = 13.5f;
    private const float DOOR_COEEFICIENT = 10f;
    private const float SHOP_COEFFICIENT = 30f;

    #region State Events
    //[SerializeField]
    //private EPauseFlags[] mGamePauseEnterActivate;
    //[SerializeField]
    //private EPauseFlags[] mGamePauseEnterDeactivate;
    //[SerializeField]
    //private EPauseFlags[] mGamePauseExitActivate;
    //[SerializeField]
    //private EPauseFlags[] mGamePauseExitDeactivate;

    //[SerializeField]
    //private EGameOverFlags[] mGameOverEnterActivate;
    //[SerializeField]
    //private EGameOverFlags[] mGameOverEnterDeactivate;
    //[SerializeField]
    //private EGameOverFlags[] mGameOverExitActivate;
    //[SerializeField]
    //private EGameOverFlags[] mGameOverExitDeactivate;

    //[SerializeField]
    //private EGameStateFlags[] mGameStateEnterActivate;
    //[SerializeField]
    //private EGameStateFlags[] mGameStateEnterDeactivate;
    //[SerializeField]
    //private EGameStateFlags[] mGameStateExitActivate;
    //[SerializeField]
    //private EGameStateFlags[] mGameStateExitDeactivate;
    #endregion


    [SerializeField]
    private Transform mChamberHolder;
    [SerializeField]
    private RectTransform mFairyMarker;
    [SerializeField]
    private RectTransform mPredetorMarker;
    [SerializeField]
    private RectTransform mShopMarker;
    [SerializeField]
    private RectTransform mWallMarker;
    [SerializeField]
    private RectTransform mWallHolder;
    [SerializeField]
    private RectTransform mPlaceObjectHolder;
    [SerializeField]
    private float mMinimapWidth;

    private Image[] mImages;

    private Transform mFairyTransform;

    private List<RectTransform> mDoorPositionList;
    private List<RectTransform> mRoomTransformList;
    private List<Vector2> roomTransformList;
    private List<GameObject> shopList = new List<GameObject>();

    private List<float> mRoomLeftX;
    private List<float> mRoomRightX;
    private List<float> mSecretChamberRightX;
    private List<float> mSecretChamberLeftX;

    private void Awake()
    {
        AddStateEvent();

        mDoorPositionList = new List<RectTransform>();
        mRoomTransformList = new List<RectTransform>();
        roomTransformList = new List<Vector2>();

        shopList = new List<GameObject>();

        mRoomLeftX = new List<float>();
        mRoomRightX = new List<float>();
        mSecretChamberRightX = new List<float>();
        mSecretChamberLeftX = new List<float>();
    }

    private void Start()
    {
        if (mChamberHolder != null)
        {
            PlaceMinimapObject();
        }
        else if(mChamberHolder == null)
        {
            this.gameObject.SetActive(false);
        }

        mImages = this.GetComponentsInChildren<Image>();
    }

    public override void Activate()
    {
        foreach (Image image in mImages)
        {
            if (image.GetComponent<CheckHudMask>() != null)
            {
                StartCoroutine(HudFade.FadeIn(image, image.GetComponent<CheckHudMask>().IsMask));
            }
            else
            {
                //StartCoroutine(HudFade.FadeIn(image));
            }
        }
    }

    public override void Deactivate()
    {
        foreach (Image image in mImages)
        {
            if (image.GetComponent<CheckHudMask>() != null)
            {
                StartCoroutine(HudFade.FadeOut(image, image.GetComponent<CheckHudMask>().IsMask));
            }
            else
            {
                //StartCoroutine(HudFade.FadeOut(image));
            }
        }
    }

    public override void Express()
    {
        for(int i =0; i < mRoomLeftX.Count; i++)
        {
            if(Giant_Player.Main.transform.position.x >= mRoomLeftX[i] && Giant_Player.Main.transform.position.x <= mRoomRightX[i])
            {
                mPredetorMarker.anchoredPosition = new Vector2(roomTransformList[i].x, mPredetorMarker.anchoredPosition.y);
                mFairyMarker.anchoredPosition = new Vector2(mPredetorMarker.anchoredPosition.x, mFairyMarker.anchoredPosition.y);
            }
        }

        for(int i = 0; i < mSecretChamberLeftX.Count; i++)
        {
            if (Giant_Player.Main.transform.position.x >= mSecretChamberLeftX[i] && Giant_Player.Main.transform.position.y <= mSecretChamberRightX[i])
            {
                shopList[i].GetComponent<Animator>().SetBool("IsShop", true);
            }
        }
    }

    public override void GetInformation()
    {

    }
   

    private void PlaceMinimapObject()
    {
        int chamberCount = mChamberHolder.childCount;

        List<RectTransform> restChamberTransformList = new List<RectTransform>();
        List<RectTransform> wallChamberTransformList = new List<RectTransform>();

        int doorCount = 0;
        for(int i = 1; i < chamberCount; i++)
        {
            if(mChamberHolder.GetChild(i).GetComponent<ChamberInfo>() != null)
            {
                doorCount++;

                //각 챔버의 x Position
                mRoomLeftX.Add(mChamberHolder.GetChild(i).GetComponent<ChamberInfo>().LeftBottom.position.x);
                mRoomRightX.Add(mChamberHolder.GetChild(i).GetComponent<ChamberInfo>().RightTop.position.x);

                mRoomTransformList.Add(mChamberHolder.GetChild(i).GetComponent<RectTransform>());

                if(mChamberHolder.GetChild(i).GetComponent<ChamberInfo>().ChamberFlag == EChamberFlag.Shop)
                {
                    restChamberTransformList.Add(Instantiate(mShopMarker, mPlaceObjectHolder));
                    mSecretChamberLeftX.Add(mChamberHolder.GetChild(i).GetComponent<ChamberInfo>().LeftBottom.position.x);
                    mSecretChamberRightX.Add(mChamberHolder.GetChild(i).GetComponent<ChamberInfo>().RightTop.position.x);
                }
            }
        }

        //doorCount = 문 생성 갯수
        for(int i = 0; i < doorCount; i++)
        {
            wallChamberTransformList.Add(Instantiate(mWallMarker, mWallHolder));
        }

        //문 배치
        //전체 맵의 길이 / 문 갯수
        //시작 지점 포지션부터 갯수만큼 더한다
        float wallPosition = 0;
        for(int i = 0; i < wallChamberTransformList.Count; i++)
        {
            wallPosition += (mMinimapWidth / wallChamberTransformList.Count + 1);
            wallChamberTransformList[i].anchoredPosition = new Vector2(wallPosition, wallChamberTransformList[i].anchoredPosition.y);
        }

        //챔버 가운데 위치
        roomTransformList.Add(new Vector2(0f + wallChamberTransformList[0].anchoredPosition.x / 2f, 0f));
        for(int i = 1; i < wallChamberTransformList.Count; i++)
        {
            roomTransformList.Add(new Vector2(roomTransformList[i - 1].x + roomTransformList[0].x * 2, 0f));
        }

        //상점 배치
        int shopCount = 0;
        int shopIndex = 0;
        for(int i = 1; i < chamberCount; i++)
        {
            if(mChamberHolder.GetChild(i).GetComponent<ChamberInfo>() != null)
            {
                shopCount++;

                if(mChamberHolder.GetChild(i).GetComponent<ChamberInfo>().ChamberFlag == EChamberFlag.Shop)
                {
                    restChamberTransformList[shopIndex].anchoredPosition = new Vector2(roomTransformList[shopCount - 1].x,
                        restChamberTransformList[shopIndex].anchoredPosition.y);
                    shopIndex++;
                }
            }
        }

        //그 외 특수 챔버 배치


        //상점 오브젝트 가져오기
        for (int i = 0; i < restChamberTransformList.Count; i++)
        {
            shopList.Add(restChamberTransformList[i].gameObject);
        }

        //그 외 특수 오브젝트 가져오기



        mWallMarker.gameObject.SetActive(false);
        mShopMarker.gameObject.SetActive(false);

        restChamberTransformList.Clear();
        wallChamberTransformList.Clear();
    }

    private void AddStateEvent()
    {
        StateManager.Pause.AddEnterEvent(false, Activate);
        StateManager.Pause.AddEnterEvent(true, Deactivate);
        StateManager.Pause.AddExitEvent(false, Activate);
        StateManager.Pause.AddExitEvent(true, Deactivate);

        StateManager.GameOver.AddEnterEvent(false, Activate);
        StateManager.GameOver.AddEnterEvent(true, Deactivate);
        StateManager.GameOver.AddExitEvent(false, Activate);
        StateManager.GameOver.AddExitEvent(true, Deactivate);


        //if (mGamePauseEnterActivate.Length != 0)
        //{
        //    for (int i = 0; i < mGamePauseEnterActivate.Length; i++)
        //    {
        //        StateManager.GamePauseHandler.AddEnterEvent(mGamePauseEnterActivate[i], Activate);
        //    }
        //}
        //if (mGamePauseEnterDeactivate.Length != 0)
        //{
        //    for (int i = 0; i < mGamePauseEnterDeactivate.Length; i++)
        //    {
        //        StateManager.GamePauseHandler.AddEnterEvent(mGamePauseEnterDeactivate[i], Deactivate);
        //    }
        //}
        //if (mGamePauseExitActivate.Length != 0)
        //{
        //    for (int i = 0; i < mGamePauseExitActivate.Length; i++)
        //    {
        //        StateManager.GamePauseHandler.AddExitEvent(mGamePauseExitActivate[i], Activate);
        //    }
        //}
        //if (mGamePauseExitDeactivate.Length != 0)
        //{
        //    for (int i = 0; i < mGamePauseExitDeactivate.Length; i++)
        //    {
        //        StateManager.GamePauseHandler.AddExitEvent(mGamePauseExitDeactivate[i], Deactivate);
        //    }
        //}

        //if (mGameOverEnterActivate.Length != 0)
        //{
        //    for (int i = 0; i < mGameOverEnterActivate.Length; i++)
        //    {
        //        StateManager.GameOverHandler.AddEnterEvent(mGameOverEnterActivate[i], Activate);
        //    }
        //}
        //if (mGameOverEnterDeactivate.Length != 0)
        //{
        //    for (int i = 0; i < mGameOverEnterDeactivate.Length; i++)
        //    {
        //        StateManager.GameOverHandler.AddEnterEvent(mGameOverEnterDeactivate[i], Deactivate);
        //    }
        //}
        //if (mGameOverExitActivate.Length != 0)
        //{
        //    for (int i = 0; i < mGameOverExitActivate.Length; i++)
        //    {
        //        StateManager.GameOverHandler.AddExitEvent(mGameOverExitActivate[i], Activate);
        //    }
        //}
        //if (mGameOverExitDeactivate.Length != 0)
        //{
        //    for (int i = 0; i < mGameOverExitDeactivate.Length; i++)
        //    {
        //        StateManager.GameOverHandler.AddExitEvent(mGameOverExitDeactivate[i], Deactivate);
        //    }
        //}

        //if (mGameStateEnterActivate.Length != 0)
        //{
        //    for (int i = 0; i < mGameStateEnterActivate.Length; i++)
        //    {
        //        StateManager.GameStateHandler.AddEnterEvent(mGameStateEnterActivate[i], Activate);
        //    }
        //}
        //if (mGameStateEnterDeactivate.Length != 0)
        //{
        //    for (int i = 0; i < mGameStateEnterDeactivate.Length; i++)
        //    {
        //        StateManager.GameStateHandler.AddEnterEvent(mGameStateEnterDeactivate[i], Deactivate);
        //    }
        //}
        //if (mGameStateExitActivate.Length != 0)
        //{
        //    for (int i = 0; i < mGameStateExitActivate.Length; i++)
        //    {
        //        StateManager.GameStateHandler.AddExitEvent(mGameStateExitActivate[i], Activate);
        //    }
        //}
        //if (mGameStateExitDeactivate.Length != 0)
        //{
        //    for (int i = 0; i < mGameStateExitDeactivate.Length; i++)
        //    {
        //        StateManager.GameStateHandler.AddExitEvent(mGameStateExitDeactivate[i], Deactivate);
        //    }
        //}
    }

    private void OnDestroy()
    {
        RemoveStateEvent();
    }

    private void RemoveStateEvent()
    {
        StateManager.Pause.RemoveEnterEvent(false, Activate);
        StateManager.Pause.RemoveEnterEvent(true, Deactivate);
        StateManager.Pause.RemoveExitEvent(false, Activate);
        StateManager.Pause.RemoveExitEvent(true, Deactivate);

        StateManager.GameOver.RemoveEnterEvent(false, Activate);
        StateManager.GameOver.RemoveEnterEvent(true, Deactivate);
        StateManager.GameOver.RemoveExitEvent(false, Activate);
        StateManager.GameOver.RemoveExitEvent(true, Deactivate);


        //if (mGamePauseEnterActivate.Length != 0)
        //{
        //    for (int i = 0; i < mGamePauseEnterActivate.Length; i++)
        //    {
        //        StateManager.GamePauseHandler.RemoveEnterEvent(mGamePauseEnterActivate[i], Activate);
        //    }
        //}
        //if (mGamePauseEnterDeactivate.Length != 0)
        //{
        //    for (int i = 0; i < mGamePauseEnterDeactivate.Length; i++)
        //    {
        //        StateManager.GamePauseHandler.RemoveEnterEvent(mGamePauseEnterDeactivate[i], Deactivate);
        //    }
        //}
        //if (mGamePauseExitActivate.Length != 0)
        //{
        //    for (int i = 0; i < mGamePauseExitActivate.Length; i++)
        //    {
        //        StateManager.GamePauseHandler.RemoveExitEvent(mGamePauseExitActivate[i], Activate);
        //    }
        //}
        //if (mGamePauseExitDeactivate.Length != 0)
        //{
        //    for (int i = 0; i < mGamePauseExitDeactivate.Length; i++)
        //    {
        //        StateManager.GamePauseHandler.RemoveExitEvent(mGamePauseExitDeactivate[i], Deactivate);
        //    }
        //}

        //if (mGameOverEnterActivate.Length != 0)
        //{
        //    for (int i = 0; i < mGameOverEnterActivate.Length; i++)
        //    {
        //        StateManager.GameOverHandler.RemoveEnterEvent(mGameOverEnterActivate[i], Activate);
        //    }
        //}
        //if (mGameOverEnterDeactivate.Length != 0)
        //{
        //    for (int i = 0; i < mGameOverEnterDeactivate.Length; i++)
        //    {
        //        StateManager.GameOverHandler.RemoveEnterEvent(mGameOverEnterDeactivate[i], Deactivate);
        //    }
        //}
        //if (mGameOverExitActivate.Length != 0)
        //{
        //    for (int i = 0; i < mGameOverExitActivate.Length; i++)
        //    {
        //        StateManager.GameOverHandler.RemoveExitEvent(mGameOverExitActivate[i], Activate);
        //    }
        //}
        //if (mGameOverExitDeactivate.Length != 0)
        //{
        //    for (int i = 0; i < mGameOverExitDeactivate.Length; i++)
        //    {
        //        StateManager.GameOverHandler.RemoveExitEvent(mGameOverExitDeactivate[i], Deactivate);
        //    }
        //}

        //if (mGameStateEnterActivate.Length != 0)
        //{
        //    for (int i = 0; i < mGameStateEnterActivate.Length; i++)
        //    {
        //        StateManager.GameStateHandler.RemoveEnterEvent(mGameStateEnterActivate[i], Activate);
        //    }
        //}
        //if (mGameStateEnterDeactivate.Length != 0)
        //{
        //    for (int i = 0; i < mGameStateEnterDeactivate.Length; i++)
        //    {
        //        StateManager.GameStateHandler.RemoveEnterEvent(mGameStateEnterDeactivate[i], Deactivate);
        //    }
        //}
        //if (mGameStateExitActivate.Length != 0)
        //{
        //    for (int i = 0; i < mGameStateExitActivate.Length; i++)
        //    {
        //        StateManager.GameStateHandler.RemoveExitEvent(mGameStateExitActivate[i], Activate);
        //    }
        //}
        //if (mGameStateExitDeactivate.Length != 0)
        //{
        //    for (int i = 0; i < mGameStateExitDeactivate.Length; i++)
        //    {
        //        StateManager.GameStateHandler.RemoveExitEvent(mGameStateExitDeactivate[i], Deactivate);
        //    }
        //}
    }
}

