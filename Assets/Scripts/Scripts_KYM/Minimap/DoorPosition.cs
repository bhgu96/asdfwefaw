﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DoorPosition : MonoBehaviour
{
    [SerializeField]
    private List<Transform> mDoorTransform;
    public List<Transform> DoorTransform { get { return mDoorTransform; } private set { } }
}
