﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MinimapHud : AHud
{
    private const float EDGE_COEFFICIENT = 13.5f;
    private const float DOOR_COEEFICIENT = 10f;
    private const float SHOP_COEFFICIENT = 30f;

    #region State Events
    //[SerializeField]
    //private EPauseFlags[] mGamePauseEnterActivate;
    //[SerializeField]
    //private EPauseFlags[] mGamePauseEnterDeactivate;
    //[SerializeField]
    //private EPauseFlags[] mGamePauseExitActivate;
    //[SerializeField]
    //private EPauseFlags[] mGamePauseExitDeactivate;

    //[SerializeField]
    //private EGameOverFlags[] mGameOverEnterActivate;
    //[SerializeField]
    //private EGameOverFlags[] mGameOverEnterDeactivate;
    //[SerializeField]
    //private EGameOverFlags[] mGameOverExitActivate;
    //[SerializeField]
    //private EGameOverFlags[] mGameOverExitDeactivate;

    //[SerializeField]
    //private EGameStateFlags[] mGameStateEnterActivate;
    //[SerializeField]
    //private EGameStateFlags[] mGameStateEnterDeactivate;
    //[SerializeField]
    //private EGameStateFlags[] mGameStateExitActivate;
    //[SerializeField]
    //private EGameStateFlags[] mGameStateExitDeactivate;
    #endregion


    [SerializeField]
    private Transform mChamberHolder;
    [SerializeField]
    private RectTransform mFairyMarker;
    [SerializeField]
    private RectTransform mPredetorMarker;
    [SerializeField]
    private RectTransform mShopMarker;
    [SerializeField]
    private RectTransform mWallMarker;
    [SerializeField]
    private RectTransform mPredetorLine;
    [SerializeField]
    private RectTransform mWallHolder;
    [SerializeField]
    private RectTransform mPlaceObjectHolder;

    private Image[] mImages;

    [SerializeField]
    private Transform mFairyTransform;
    [SerializeField]
    private Transform mPredetorTransform;

    private float mMaxWorldLength;
    private float mMinimapWidth;

    private List<RectTransform> mDoorPositionList;
    private List<RectTransform> mShopPositionList;

    private void Awake()
    {
        AddStateEvent();

        mDoorPositionList = new List<RectTransform>();
        mShopPositionList = new List<RectTransform>();
    }

    private void Start()
    {
        //if(NewGiant.Instance != null)
        //{
        //    mPredetorTransform = NewGiant.Instance.gameObject.transform;
        //}

        //if(Fairy_V2.Instance != null)
        //{
        //    mFairyTransform = Fairy_V2.Instance.gameObject.transform;
        //}

        if(mChamberHolder != null)
        {
            int chamberCount = mChamberHolder.childCount;
                                                         
            mMaxWorldLength = mChamberHolder.GetChild(chamberCount - 1).GetComponent<ChamberInfo>().RightTop.position.x
                - mChamberHolder.GetChild(1).GetComponent<ChamberInfo>().LeftBottom.position.x;

            //mMaxWorldLength = mChamberHolder.GetChild(chamberCount - 1).GetComponentInChildren<RightTopMarker>().transform.position.x
            //    - mChamberHolder.GetChild(1).GetComponentInChildren<LeftBottomMarker>().transform.position.x;

            mMinimapWidth = this.GetComponent<RectTransform>().sizeDelta.x;

            PlaceMinimapObject();
        }

        mImages = this.GetComponentsInChildren<Image>();
    }

    public override void Activate()
    {
        foreach (Image image in mImages)
        {
            if(image.GetComponent<CheckHudMask>() != null)
            {
                StartCoroutine(HudFade.FadeIn(image, image.GetComponent<CheckHudMask>().IsMask));
            }
            else
            {
                StartCoroutine(HudFade.FadeIn(image));
            }
        }
    }

    public override void Deactivate()
    {
        foreach (Image image in mImages)
        {
            if (image.GetComponent<CheckHudMask>() != null)
            {
                StartCoroutine(HudFade.FadeOut(image, image.GetComponent<CheckHudMask>().IsMask));
            }
            else
            {
                StartCoroutine(HudFade.FadeOut(image));
            }
        }
    }

    public override void Express()
    {
        mFairyMarker.anchoredPosition = new Vector2(SetMarkerPositionX(mFairyTransform.position.x), mFairyMarker.anchoredPosition.y);
        mPredetorMarker.anchoredPosition = new Vector2(SetMarkerPositionX(mPredetorTransform.position.x), mPredetorMarker.anchoredPosition.y);
        mPredetorLine.anchoredPosition = new Vector2(SetMarkerPositionX(mPredetorTransform.position.x - EDGE_COEFFICIENT), mPredetorLine.anchoredPosition.y);

        for(int i = 0; i < mDoorPositionList.Count; i++)
        {
            if(mPredetorMarker.anchoredPosition.x + DOOR_COEEFICIENT > mDoorPositionList[i].anchoredPosition.x)
            {
                mDoorPositionList[i].GetComponent<CheckBreakDoor>().Animator.SetBool("isBroken", true);
            }
        }

        for (int i = 0; i < mShopPositionList.Count; i++)
        {
            if (mShopPositionList[i].anchoredPosition.x - mPredetorMarker.anchoredPosition.x <= SHOP_COEFFICIENT)
            {
                mShopPositionList[i].GetComponent<CheckShop>().Animator.SetBool("isShop", true);
            }
        }
    }

    public override void GetInformation()
    {

    }

    private void PlaceMinimapObject()
    {
        int chamberCount = mChamberHolder.childCount;

        List<RectTransform> restChamberTransformList = new List<RectTransform>();
        List<GameObject> restChamberObjectList = new List<GameObject>();

        List<RectTransform> wallChamberTransformList = new List<RectTransform>();
        List<GameObject> doorObjectList = new List<GameObject>();

        for (int i = 1; i < chamberCount; i++)
        {
            //특정 구역이 될 수 있음

            if(mChamberHolder.GetChild(i).GetComponent<ChamberInfo>() != null)
            {
                if(mChamberHolder.GetChild(i).GetComponent<ChamberInfo>().ChamberFlag == EChamberFlag.Shop)
                {
                    restChamberObjectList.Add(mChamberHolder.GetChild(i).gameObject);
                    restChamberTransformList.Add(Instantiate(mShopMarker, mPlaceObjectHolder));
                }
            }

            //if(mChamberHolder.GetChild(i).GetComponent<Chamber>().ChamberType == EChamberType.REST_POINT)
            //{
            //    restChamberObjectList.Add(mChamberHolder.GetChild(i).gameObject);
            //    restChamberTransformList.Add(Instantiate(mShopMarker, mPlaceObjectHolder));
            //}

            if(mChamberHolder.GetChild(i).GetComponent<DoorPosition>() != null)
            {
                for(int count = 0; count < mChamberHolder.GetChild(i).GetComponent<DoorPosition>().DoorTransform.Count; count++)
                {
                    doorObjectList.Add(mChamberHolder.GetChild(i).GetComponent<DoorPosition>().DoorTransform[count].gameObject);
                    wallChamberTransformList.Add(Instantiate(mWallMarker, mWallHolder));
                }

                for(int count = 0; count < wallChamberTransformList.Count; count++)
                {
                    if(!mDoorPositionList.Contains(wallChamberTransformList[count]))
                    {
                        mDoorPositionList.Add(wallChamberTransformList[count]);
                    }
                }
            }
        }

        for (int count = 0; count < mDoorPositionList.Count; count++)
        {
            mDoorPositionList[count].GetComponent<CheckBreakDoor>().Index = count;
        }

        for (int count = 0; count < restChamberTransformList.Count; count++)
        {
            mShopPositionList.Add(restChamberTransformList[count]);
            mShopPositionList[count].GetComponent<CheckShop>().Index = count;
        }

        for (int i = 0; i < restChamberTransformList.Count; i++)
        {
            restChamberTransformList[i].gameObject.SetActive(true);


            if(restChamberObjectList[i].GetComponent<ChamberInfo>() != null)
            {
                restChamberTransformList[i].anchoredPosition = new Vector2(SetMarkerPositionX(restChamberObjectList[i].GetComponent<ChamberInfo>().RightTop.position.x
                    - (restChamberObjectList[i].GetComponent<ChamberInfo>().RightTop.position.x - restChamberObjectList[i].GetComponent<ChamberInfo>().LeftBottom.position.x) / 2f)
                    , restChamberTransformList[i].anchoredPosition.y);
            }


            //restChamberTransformList[i].anchoredPosition = new Vector2(SetMarkerPositionX(restChamberObjectList[i].GetComponentInChildren<RightTopMarker>().transform.position.x 
            //    - (restChamberObjectList[i].GetComponentInChildren<RightTopMarker>().transform.position.x - restChamberObjectList[i].GetComponentInChildren<LeftBottomMarker>().transform.position.x) / 2f) 
            //    , restChamberTransformList[i].anchoredPosition.y);
        }

        for (int i = 0; i < wallChamberTransformList.Count; i++)
        {
            wallChamberTransformList[i].gameObject.SetActive(true);
            wallChamberTransformList[i].anchoredPosition = new Vector2(SetMarkerPositionX(doorObjectList[i].transform.position.x)
                , wallChamberTransformList[i].anchoredPosition.y);
        }

        restChamberTransformList.Clear();
        restChamberObjectList.Clear();
        doorObjectList.Clear();
        wallChamberTransformList.Clear();
    }

    private float SetMarkerPositionX(float worldPositionX)
    {
        return mMinimapWidth * worldPositionX / mMaxWorldLength;
    }

    private void AddStateEvent()
    {
        StateManager.Pause.AddEnterEvent(false, Activate);
        StateManager.Pause.AddEnterEvent(true, Deactivate);
        StateManager.Pause.AddExitEvent(false, Activate);
        StateManager.Pause.AddExitEvent(true, Deactivate);

        StateManager.GameOver.AddEnterEvent(false, Activate);
        StateManager.GameOver.AddEnterEvent(true, Deactivate);
        StateManager.GameOver.AddExitEvent(false, Activate);
        StateManager.GameOver.AddExitEvent(true, Deactivate);

        //if (mGamePauseEnterActivate.Length != 0)
        //{
        //    for (int i = 0; i < mGamePauseEnterActivate.Length; i++)
        //    {
        //        StateManager.GamePauseHandler.AddEnterEvent(mGamePauseEnterActivate[i], Activate);
        //    }
        //}
        //if (mGamePauseEnterDeactivate.Length != 0)
        //{
        //    for (int i = 0; i < mGamePauseEnterDeactivate.Length; i++)
        //    {
        //        StateManager.GamePauseHandler.AddEnterEvent(mGamePauseEnterDeactivate[i], Deactivate);
        //    }
        //}
        //if (mGamePauseExitActivate.Length != 0)
        //{
        //    for (int i = 0; i < mGamePauseExitActivate.Length; i++)
        //    {
        //        StateManager.GamePauseHandler.AddExitEvent(mGamePauseExitActivate[i], Activate);
        //    }
        //}
        //if (mGamePauseExitDeactivate.Length != 0)
        //{
        //    for (int i = 0; i < mGamePauseExitDeactivate.Length; i++)
        //    {
        //        StateManager.GamePauseHandler.AddExitEvent(mGamePauseExitDeactivate[i], Deactivate);
        //    }
        //}

        //if (mGameOverEnterActivate.Length != 0)
        //{
        //    for (int i = 0; i < mGameOverEnterActivate.Length; i++)
        //    {
        //        StateManager.GameOverHandler.AddEnterEvent(mGameOverEnterActivate[i], Activate);
        //    }
        //}
        //if (mGameOverEnterDeactivate.Length != 0)
        //{
        //    for (int i = 0; i < mGameOverEnterDeactivate.Length; i++)
        //    {
        //        StateManager.GameOverHandler.AddEnterEvent(mGameOverEnterDeactivate[i], Deactivate);
        //    }
        //}
        //if (mGameOverExitActivate.Length != 0)
        //{
        //    for (int i = 0; i < mGameOverExitActivate.Length; i++)
        //    {
        //        StateManager.GameOverHandler.AddExitEvent(mGameOverExitActivate[i], Activate);
        //    }
        //}
        //if (mGameOverExitDeactivate.Length != 0)
        //{
        //    for (int i = 0; i < mGameOverExitDeactivate.Length; i++)
        //    {
        //        StateManager.GameOverHandler.AddExitEvent(mGameOverExitDeactivate[i], Deactivate);
        //    }
        //}

        //if (mGameStateEnterActivate.Length != 0)
        //{
        //    for (int i = 0; i < mGameStateEnterActivate.Length; i++)
        //    {
        //        StateManager.GameStateHandler.AddEnterEvent(mGameStateEnterActivate[i], Activate);
        //    }
        //}
        //if (mGameStateEnterDeactivate.Length != 0)
        //{
        //    for (int i = 0; i < mGameStateEnterDeactivate.Length; i++)
        //    {
        //        StateManager.GameStateHandler.AddEnterEvent(mGameStateEnterDeactivate[i], Deactivate);
        //    }
        //}
        //if (mGameStateExitActivate.Length != 0)
        //{
        //    for (int i = 0; i < mGameStateExitActivate.Length; i++)
        //    {
        //        StateManager.GameStateHandler.AddExitEvent(mGameStateExitActivate[i], Activate);
        //    }
        //}
        //if (mGameStateExitDeactivate.Length != 0)
        //{
        //    for (int i = 0; i < mGameStateExitDeactivate.Length; i++)
        //    {
        //        StateManager.GameStateHandler.AddExitEvent(mGameStateExitDeactivate[i], Deactivate);
        //    }
        //}
    }

    private void OnDestroy()
    {
        RemoveStateEvent();
    }

    private void RemoveStateEvent()
    {
        StateManager.Pause.RemoveEnterEvent(false, Activate);
        StateManager.Pause.RemoveEnterEvent(true, Deactivate);
        StateManager.Pause.RemoveExitEvent(false, Activate);
        StateManager.Pause.RemoveExitEvent(true, Deactivate);

        StateManager.GameOver.RemoveEnterEvent(false, Activate);
        StateManager.GameOver.RemoveEnterEvent(true, Deactivate);
        StateManager.GameOver.RemoveExitEvent(false, Activate);
        StateManager.GameOver.RemoveExitEvent(true, Deactivate);



        //if (mGamePauseEnterActivate.Length != 0)
        //{
        //    for (int i = 0; i < mGamePauseEnterActivate.Length; i++)
        //    {
        //        StateManager.GamePauseHandler.RemoveEnterEvent(mGamePauseEnterActivate[i], Activate);
        //    }
        //}
        //if (mGamePauseEnterDeactivate.Length != 0)
        //{
        //    for (int i = 0; i < mGamePauseEnterDeactivate.Length; i++)
        //    {
        //        StateManager.GamePauseHandler.RemoveEnterEvent(mGamePauseEnterDeactivate[i], Deactivate);
        //    }
        //}
        //if (mGamePauseExitActivate.Length != 0)
        //{
        //    for (int i = 0; i < mGamePauseExitActivate.Length; i++)
        //    {
        //        StateManager.GamePauseHandler.RemoveExitEvent(mGamePauseExitActivate[i], Activate);
        //    }
        //}
        //if (mGamePauseExitDeactivate.Length != 0)
        //{
        //    for (int i = 0; i < mGamePauseExitDeactivate.Length; i++)
        //    {
        //        StateManager.GamePauseHandler.RemoveExitEvent(mGamePauseExitDeactivate[i], Deactivate);
        //    }
        //}

        //if (mGameOverEnterActivate.Length != 0)
        //{
        //    for (int i = 0; i < mGameOverEnterActivate.Length; i++)
        //    {
        //        StateManager.GameOverHandler.RemoveEnterEvent(mGameOverEnterActivate[i], Activate);
        //    }
        //}
        //if (mGameOverEnterDeactivate.Length != 0)
        //{
        //    for (int i = 0; i < mGameOverEnterDeactivate.Length; i++)
        //    {
        //        StateManager.GameOverHandler.RemoveEnterEvent(mGameOverEnterDeactivate[i], Deactivate);
        //    }
        //}
        //if (mGameOverExitActivate.Length != 0)
        //{
        //    for (int i = 0; i < mGameOverExitActivate.Length; i++)
        //    {
        //        StateManager.GameOverHandler.RemoveExitEvent(mGameOverExitActivate[i], Activate);
        //    }
        //}
        //if (mGameOverExitDeactivate.Length != 0)
        //{
        //    for (int i = 0; i < mGameOverExitDeactivate.Length; i++)
        //    {
        //        StateManager.GameOverHandler.RemoveExitEvent(mGameOverExitDeactivate[i], Deactivate);
        //    }
        //}

        //if (mGameStateEnterActivate.Length != 0)
        //{
        //    for (int i = 0; i < mGameStateEnterActivate.Length; i++)
        //    {
        //        StateManager.GameStateHandler.RemoveEnterEvent(mGameStateEnterActivate[i], Activate);
        //    }
        //}
        //if (mGameStateEnterDeactivate.Length != 0)
        //{
        //    for (int i = 0; i < mGameStateEnterDeactivate.Length; i++)
        //    {
        //        StateManager.GameStateHandler.RemoveEnterEvent(mGameStateEnterDeactivate[i], Deactivate);
        //    }
        //}
        //if (mGameStateExitActivate.Length != 0)
        //{
        //    for (int i = 0; i < mGameStateExitActivate.Length; i++)
        //    {
        //        StateManager.GameStateHandler.RemoveExitEvent(mGameStateExitActivate[i], Activate);
        //    }
        //}
        //if (mGameStateExitDeactivate.Length != 0)
        //{
        //    for (int i = 0; i < mGameStateExitDeactivate.Length; i++)
        //    {
        //        StateManager.GameStateHandler.RemoveExitEvent(mGameStateExitDeactivate[i], Deactivate);
        //    }
        //}
    }
}
