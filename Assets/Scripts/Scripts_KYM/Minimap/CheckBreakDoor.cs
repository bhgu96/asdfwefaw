﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CheckBreakDoor : MonoBehaviour
{
    public float Index { get { return mIndex; } set { mIndex = value; } }
    public Animator Animator { get { return mAnimator; } private set { } }
    private float mIndex;
    private Animator mAnimator;

    private void Awake()
    {
        mAnimator = this.GetComponent<Animator>();
    }
}
