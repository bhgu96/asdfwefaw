﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TooltipHud : MonoBehaviour
{
    public bool IsContainItem { get { return mIsContainItem; } set { mIsContainItem = value;} }

    [SerializeField]
    private ETooltipType mTooltipType;
    [SerializeField]
    private Text mItemNameText;
    [SerializeField]
    private Text mItemDescriptionText;

    private bool mIsContainItem;

    private void OnEnable()
    {
        if (!mIsContainItem)
        {
            this.gameObject.SetActive(false);
        }
    }

    private void OnDisable()
    {
        mIsContainItem = false;
    }

    public void SetTooltip(string itemName, string itemDescription)
    {
        mItemNameText.text = itemName;
        mItemDescriptionText.text = itemDescription;
    }
}

public enum ETooltipType
{
    None = -1,
    Item,
    Skill,
    Count
}
