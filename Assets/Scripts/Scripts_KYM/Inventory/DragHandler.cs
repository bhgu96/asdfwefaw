﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class DragHandler : MonoBehaviour, IDragHandler, IEndDragHandler, IBeginDragHandler, IDropHandler
{
    public ESlotType SlotType { get { return mSlotType; } private set { } }
    public Transform PrevTransform { get { return mPrevTransform; } private set { } }
    public GameObject EquipObject {  get { return mEquipObject; } private set { } }
    public RectTransform RectTransform {  get { return mRectTransform; } private set { } }

    [SerializeField]
    private ESlotType mSlotType;
    [SerializeField]
    private GameObject mEquipObject;
    [SerializeField]
    private ECharacterType mCharacterType;

    private ItemSlotInfo mItemInfo;
    private SelectableInventorySlot mSelectableInventorySlot;
    private RectTransform mRectTransform;
    private Transform mPrevTransform;

    private void Awake()
    {
        mSelectableInventorySlot = this.GetComponent<SelectableInventorySlot>();
        mRectTransform = this.GetComponent<RectTransform>();
        mPrevTransform = this.GetComponent<Transform>().parent;
        mItemInfo = this.GetComponent<ItemSlotInfo>();
    }

    private void OnEnable()
    {
        if(mEquipObject != null)
        {
            if (!mItemInfo.IsEquipItem)
            {
                mEquipObject.SetActive(false);
            }
            else if(mItemInfo.IsEquipItem && mItemInfo.ItemCategory == EItemCategory.Soulite)
            {
                mEquipObject.SetActive(true);
            }
        }
    }

    public void OnDrag(PointerEventData eventData)
    {
        if (mSelectableInventorySlot != null)
        {
            if (mSelectableInventorySlot.IsSlot)
            {
                this.transform.SetParent(mSelectableInventorySlot.MouseTransform);
                Vector3 tempVector = Camera.main.ScreenToWorldPoint(Input.mousePosition);
                mRectTransform.position = new Vector3(tempVector.x, tempVector.y, 10f);
            }
        }
    }

    public void OnBeginDrag(PointerEventData eventData)
    {
        if (mSelectableInventorySlot != null)
        {
            if (mSelectableInventorySlot.IsSlot)
            {
                this.GetComponent<CanvasGroup>().blocksRaycasts = false;
            }
        }
    }

    public void OnEndDrag(PointerEventData eventData)
    {
        if (mSelectableInventorySlot != null)
        {
            if (mSelectableInventorySlot.IsSlot)
            {     
                //슬롯이 안옮겨 졌을때
                if(this.transform.parent == this.GetComponent<SelectableInventorySlot>().MouseTransform)
                {
                    this.transform.SetParent(mPrevTransform);
                    mRectTransform.anchoredPosition = Vector2.zero;
                    mRectTransform.localPosition = new Vector3(mRectTransform.localPosition.x, mRectTransform.localPosition.y, 0f);
                }
                else
                {
                    mRectTransform.anchoredPosition = Vector2.zero;
                    mPrevTransform = this.GetComponent<Transform>().parent;
                }

                this.GetComponent<CanvasGroup>().blocksRaycasts = true;
            }
        }
    }

    public void OnDrop(PointerEventData eventData)
    {
        if(mSelectableInventorySlot != null)
        {
            if (mSelectableInventorySlot.IsSlot)
            {
                if (mSlotType == ESlotType.EquipmentSlot)
                {
                    if(this.GetComponent<ItemSlotInfo>().ConnectSlot != null)
                    {
                        //서로 변경
                        if(mSlotType == eventData.selectedObject.GetComponent<DragHandler>().SlotType)
                        {
                            this.GetComponent<ItemSlotInfo>().ExchangeEquipItem(this.GetComponent<ItemSlotInfo>(), eventData.selectedObject.GetComponent<ItemSlotInfo>(), mCharacterType);
                        }
                        else
                        {
                            //다시 되돌아간다
                            eventData.selectedObject.transform.SetParent(eventData.selectedObject.GetComponent<DragHandler>().PrevTransform);
                            eventData.selectedObject.GetComponent<DragHandler>().RectTransform.anchoredPosition = Vector2.zero;
                            eventData.selectedObject.GetComponent<DragHandler>().RectTransform.localPosition = new Vector3(
                                eventData.selectedObject.GetComponent<DragHandler>().RectTransform.localPosition.x,
                                eventData.selectedObject.GetComponent<DragHandler>().RectTransform.localPosition.y,
                                0f);
                        }
                    }
                    else
                    {
                        //장착 슬롯 내에 아이템 교환
                        if (eventData.selectedObject.GetComponent<DragHandler>().SlotType == ESlotType.EquipmentSlot)
                        {
                            this.GetComponent<ItemSlotInfo>().ExchangeEquipItem(this.GetComponent<ItemSlotInfo>(), eventData.selectedObject.GetComponent<ItemSlotInfo>(), mCharacterType);
                        }
                        else
                        {
                            //아이템 장착
                            if(!eventData.selectedObject.GetComponent<ItemSlotInfo>().IsEquipItem)
                            {
                                if (this.GetComponent<SelectableInventorySlot>().ItemCategory == eventData.selectedObject.GetComponent<ItemSlotInfo>().ItemCategory)
                                {
                                    if(this.GetComponent<ItemSlotInfo>().ConnectEquipSlot(eventData.selectedObject.GetComponent<ItemSlotInfo>(), mCharacterType))
                                    {
                                        Transform selectParent = eventData.selectedObject.GetComponent<DragHandler>().PrevTransform;
                                        this.gameObject.GetComponent<Image>().sprite = eventData.selectedObject.GetComponent<Image>().sprite;
                                        eventData.selectedObject.transform.SetParent(selectParent);
                                        eventData.selectedObject.GetComponent<ItemSlotInfo>().IsEquipItem = true;
                                        //this.gameObject.GetComponent<ItemSlotInfo>().mItem = eventData.selectedObject.GetComponent<ItemSlotInfo>().mItem;

                                        //장착 효과 발생
                                        //

                                    }
                                    else
                                    {
                                        this.gameObject.GetComponent<ItemSlotInfo>().ConnectSlot = null;
                                    }
                                }
                            }
                        }
                    }
                }
                else if (mSlotType == ESlotType.ItemSlot)
                {
                    //아이템 해제
                    if(eventData.selectedObject.GetComponent<DragHandler>().SlotType == ESlotType.EquipmentSlot)
                    {
                        //DeselectItem(eventData.selectedObject);
                    }
                    else //인벤토리 내에 있는 슬롯끼리 아이템 변경
                    {
                        Transform selectParent = eventData.selectedObject.GetComponent<DragHandler>().PrevTransform;
                        eventData.selectedObject.transform.SetParent(this.transform.parent);
                        this.gameObject.transform.SetParent(selectParent);
                    }

                    mRectTransform.anchoredPosition = Vector2.zero;
                    mRectTransform.localPosition = Vector3.zero;

                    eventData.selectedObject.GetComponent<RectTransform>().anchoredPosition = Vector2.zero;
                    eventData.selectedObject.GetComponent<RectTransform>().localPosition = Vector3.zero;
                }
            }
        }
    }

    public void DeselectItem(GameObject deselectSlot)
    {
        if(deselectSlot.GetComponent<ItemSlotInfo>().ConnectSlot != null)
        {
            deselectSlot.GetComponent<ItemSlotInfo>().ConnectSlot.IsEquipItem = false;
            deselectSlot.GetComponent<ItemSlotInfo>().IsEquipItem = false;
            deselectSlot.GetComponent<ItemSlotInfo>().UnEquip(mCharacterType);

            //장착 효과 해제
            //

        }
    }
}

public enum ESlotType
{
    None = -1,
    EquipmentSlot,
    ItemSlot,
    Count
}

public enum ECharacterType
{
    None = -1,
    Giant,
    Fairy,
    Common,
    Count
}

