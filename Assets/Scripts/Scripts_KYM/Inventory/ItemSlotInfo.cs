﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ItemSlotInfo : MonoBehaviour
{
    public int Row { get { return mRow; } set { mRow = value; } }
    public int Column { get { return mColumn; } set { mColumn = value; } }
    public int NormalizeSlotIndex { get { return mNormalizeSlotIndex; } set { mNormalizeSlotIndex = value; } }
    public EItemCategory ItemCategory { get { return mItemType; }  set { mItemType = value; } }
    public ECharacterType CharacterType { get { return mCharacterType; } set { mCharacterType = value; } }
    public ItemSlotInfo ConnectSlot {  get { return mConnectSlot; } set { mConnectSlot = value; } }
    public bool IsEquipItem
    {
        get
        {
            return mIsEquipItem;
        }
        set
        {
            mIsEquipItem = value;

            if(mIsEquipItem)
            {
                mDragHandler.EquipObject.SetActive(true);
            }
            else
            {
                mConnectSlot = null;

                if(mDragHandler.EquipObject != null)
                {
                    mDragHandler.EquipObject.SetActive(false);
                }
            }
        }
    } 

    [SerializeField]
    private bool mIsEquipItem;
    [SerializeField]
    private Sprite mDefaultSprite;
    [SerializeField]
    private EItemCategory mItemType;
    [SerializeField]
    private ECharacterType mCharacterType;
    [SerializeField]
    private GameObject mMaterialCountObject;
    [SerializeField]
    private GameObject mTooltip;
    [SerializeField]
    private int mEquipIndex;

    private Image mItemImage;
    private DragHandler mDragHandler;
    private ItemSlotInfo mConnectSlot;
    private EInventoryType mInventoryType;
    private ESlotType mSlotType;
    private int mNormalizeSlotIndex;

    //아이템 등록
    [HideInInspector]
    public AItem mItem;
    //아이템 행렬(행 : 밑, 렬 : 오)
    private int mRow;
    private int mColumn;

    private void Awake()
    {
        if(this.GetComponent<DragHandler>() != null)
        {
            mDragHandler = this.GetComponent<DragHandler>();
        }

        mItemImage = this.GetComponent<Image>();
        mInventoryType = this.GetComponent<SelectableInventorySlot>().InventoryType;
        mSlotType = this.GetComponent<DragHandler>().SlotType;
    }

    private void Update()
    {
        if(mSlotType == ESlotType.ItemSlot)
        {
            if(mItemType == EItemCategory.Material)
            {
                int materialCount = Fairy_Player.Main.Inventory.Peek(EItemCategory.Material, mRow, mColumn, out mItem);

                if (materialCount <= 0)
                {
                    mMaterialCountObject.SetActive(false);
                    this.GetComponent<Image>().sprite = mDefaultSprite;
                }
                else
                {
                    mMaterialCountObject.SetActive(true);
                    mItemImage.sprite = mItem.Icon;
                }
            }
            else if(mItemType == EItemCategory.Soulite)
            {
                int materialCount = Fairy_Player.Main.Inventory.Peek(EItemCategory.Soulite, mRow, mColumn, out mItem);

                if (materialCount <= 0)
                {
                    this.GetComponent<Image>().sprite = mDefaultSprite;
                }
                else
                {
                    mItemImage.sprite = mItem.Icon;
                }
            }
            else if(mItemType == EItemCategory.Valueable)
            {
                int materialCount = Fairy_Player.Main.Inventory.Peek(EItemCategory.Valueable, mRow, mColumn, out mItem);

                if (materialCount <= 0)
                {
                    this.GetComponent<Image>().sprite = mDefaultSprite;
                }
                else
                {
                    mItemImage.sprite = mItem.Icon;
                }
            }
        }
    }

    public void ChangeCategory(EItemCategory mItemCagegory)
    {
        changeCategory(mItemCagegory);

        switch (mItemCagegory)
        {
            case EItemCategory.Soulite:
                mMaterialCountObject.SetActive(false);
                int soulLightCount = Fairy_Player.Main.Inventory.Peek(EItemCategory.Soulite, mRow, mColumn, out mItem);
                if (soulLightCount <= 0)
                {
                    this.GetComponent<Image>().sprite = mDefaultSprite;
                }
                else
                {
                    mItemImage.sprite = mItem.Icon;
                }
                break;
            case EItemCategory.Material:
                int materialCount = Fairy_Player.Main.Inventory.Peek(EItemCategory.Material, mRow, mColumn, out mItem);
                if (materialCount <= 0)
                {
                    mMaterialCountObject.SetActive(false);
                    this.GetComponent<Image>().sprite = mDefaultSprite;
                }
                else
                {
                    mMaterialCountObject.SetActive(true);
                    mItemImage.sprite = mItem.Icon;
                }
                break;
            case EItemCategory.Valueable:
                mMaterialCountObject.SetActive(false);
                int valueableCount = Fairy_Player.Main.Inventory.Peek(EItemCategory.Valueable, mRow, mColumn, out mItem);
                if (valueableCount <= 0)
                {
                    this.GetComponent<Image>().sprite = mDefaultSprite;
                }
                else
                {
                    mItemImage.sprite = mItem.Icon;
                }
                break;
        }
    }

    public void UnEquip(ECharacterType characterType)
    {
        mItemImage.sprite = mDefaultSprite;

        if(characterType != ECharacterType.None)
        {
            if (characterType == ECharacterType.Giant)
            {
                Giant_Player.Main.SouliteFrame.Mount(null, mEquipIndex);
            }
            else if (characterType == ECharacterType.Fairy)
            {
                Fairy_Player.Main.SouliteFrame.Mount(null, mEquipIndex);
            }
        }
    }

    public bool ConnectEquipSlot(ItemSlotInfo connectSlot, ECharacterType characterType)
    {
        mConnectSlot = connectSlot;

        if(characterType != ECharacterType.None)
        {
            int count = Fairy_Player.Main.Inventory.Peek(EItemCategory.Soulite, mConnectSlot.Row, mConnectSlot.Column, out mConnectSlot.mItem);
            ASoulite soulLight = mConnectSlot.mItem as ASoulite;

            if (characterType == ECharacterType.Giant && Giant_Player.Main.SouliteFrame.Mount(soulLight, mEquipIndex))
            {
                return true;
            }
            else if (characterType == ECharacterType.Fairy && Fairy_Player.Main.SouliteFrame.Mount(soulLight, mEquipIndex))
            {
                return true;
            }
        }

        return false;
    }

    public void ExchangeEquipItem(ItemSlotInfo selectingItem, ItemSlotInfo selectedItem, ECharacterType characterType)
    {
        //둘다 아이템 있을때
        if(selectingItem.ConnectSlot != null)
        {
            ItemSlotInfo tempSelectingItem = selectingItem;
            ItemSlotInfo tempConnectingItem = selectingItem.ConnectSlot;
            Sprite tempSelectingIcon = tempSelectingItem.gameObject.GetComponent<Image>().sprite;
            AItem tempItem = selectingItem.mItem;

            selectingItem.ConnectEquipSlot(selectedItem.ConnectSlot, characterType);
            selectingItem.mItem = selectedItem.mItem;
            selectingItem.GetComponent<Image>().sprite = selectedItem.gameObject.GetComponent<Image>().sprite;

            selectedItem.ConnectEquipSlot(tempConnectingItem, characterType);
            selectedItem.mItem = tempItem;
            selectedItem.GetComponent<Image>().sprite = tempSelectingIcon;
        }
        //옮겨질 곳에 아이템이 없을때
        else
        {
            selectingItem.mItem = selectedItem.ConnectSlot.mItem;
            selectingItem.ConnectEquipSlot(selectedItem.ConnectSlot, ECharacterType.Giant);

            selectedItem.UnEquip(ECharacterType.Giant);
            selectedItem.ConnectSlot.mItem = null;
            selectedItem.ConnectSlot = null;

            selectingItem.gameObject.GetComponent<Image>().sprite = selectingItem.ConnectSlot.gameObject.GetComponent<Image>().sprite;
            selectedItem.gameObject.GetComponent<Image>().sprite = mDefaultSprite;
        }
    }

    public void PopupTooltip(bool isPopup)
    {
        if (isPopup)
        {
            mTooltip.GetComponent<TooltipHud>().IsContainItem = true;
            mTooltip.SetActive(true);
            mTooltip.GetComponent<TooltipHud>().SetTooltip(mItem.Name, mItem.Description);
        }
        else if (!isPopup)
        {
            mTooltip.GetComponent<TooltipHud>().IsContainItem = false;
            mTooltip.SetActive(false);
        }
    }

    public void PopupTooltip(bool isPopup, Vector2 position)
    {
        if (isPopup)
        {
            Vector2 tempPosition = Vector2.zero;

            if (mNormalizeSlotIndex > 3)
            {
                tempPosition = new Vector2(position.x - 240f, position.y);
            }
            else
            {
                tempPosition = new Vector2(position.x + 240f, position.y);
            }

            mTooltip.GetComponent<TooltipHud>().IsContainItem = true;
            mTooltip.SetActive(true);
            mTooltip.GetComponent<TooltipHud>().SetTooltip(mItem.Name, mItem.Description);
            mTooltip.GetComponent<RectTransform>().anchoredPosition = tempPosition;
        }
        else if (!isPopup)
        {
            mTooltip.GetComponent<TooltipHud>().IsContainItem = false;
            mTooltip.SetActive(false);
        }
    }

    public void PopupTooltip(bool isPopup, Vector2 position, bool isEquipmentSlot)
    {
        if (isPopup)
        {
            if(isEquipmentSlot)
            {
                Vector2 tempPosition = Vector2.zero;

                if (mNormalizeSlotIndex > 3)
                {
                    tempPosition = new Vector2(position.x - 240f, position.y);
                }
                else
                {
                    tempPosition = new Vector2(position.x + 240f, position.y);
                }

                mTooltip.GetComponent<TooltipHud>().IsContainItem = true;
                mTooltip.SetActive(true);
                mTooltip.GetComponent<TooltipHud>().SetTooltip(ConnectSlot.mItem.Name, ConnectSlot.mItem.Description);
                mTooltip.GetComponent<RectTransform>().anchoredPosition = tempPosition;
            }
            else
            {
                Vector2 tempPosition = Vector2.zero;

                if (mNormalizeSlotIndex > 3)
                {
                    tempPosition = new Vector2(position.x - 240f, position.y);
                }
                else
                {
                    tempPosition = new Vector2(position.x + 240f, position.y);
                }

                mTooltip.GetComponent<TooltipHud>().IsContainItem = true;
                mTooltip.SetActive(true);
                mTooltip.GetComponent<TooltipHud>().SetTooltip(mItem.Name, mItem.Description);
                mTooltip.GetComponent<RectTransform>().anchoredPosition = tempPosition;
            }
        }
        else if (!isPopup)
        {
            mTooltip.GetComponent<TooltipHud>().IsContainItem = false;
            mTooltip.SetActive(false);
        }
    }

    private void changeCategory(EItemCategory category)
    {
        if(mDragHandler != null)
        {
            switch (category)
            {
                case EItemCategory.Soulite:
                    if (mIsEquipItem)
                    {
                        mDragHandler.EquipObject.SetActive(true);
                    }
                    else
                    {
                        mDragHandler.EquipObject.SetActive(false);
                    }
                    break;
                case EItemCategory.Material:
                    mDragHandler.EquipObject.SetActive(false);
                    break;
                case EItemCategory.Valueable:
                    mDragHandler.EquipObject.SetActive(false);
                    break;
            }
        }
    }
}
