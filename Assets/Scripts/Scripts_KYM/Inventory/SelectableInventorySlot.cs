﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SelectableInventorySlot : MonoBehaviour
{
    public EInventoryType InventoryType { get { return mInventoryType; } private set { } }
    public EItemCategory ItemCategory {  get { return mItemCategory; } private set { } }
    public bool IsSlot { get { return mIsSlot; } private set { } }
    public RectTransform MouseTransform { get { return mMouseTransform; } private set { } }

    [SerializeField]
    private Sprite mTransparentImage;
    [SerializeField]
    private EInventoryType mInventoryType;
    [SerializeField]
    private EItemCategory mItemCategory;
    [SerializeField]
    private ECharacterType mCharactgerType;
    [SerializeField]
    private GameObject mSlots;
    [SerializeField]
    private GameObject mItemSlots;
    [SerializeField]
    private GameObject mSkillSlots;
    [SerializeField]
    private bool mIsSlot;
    [SerializeField]
    private RectTransform mMouseTransform;
    [SerializeField]
    private UIPanel mSlotPanel;
    [SerializeField]
    private GameObject mPopupObject;

    private bool mIsActive;
    private UIButton mButton;
    private Animator[] mInventoryAnimator;
    private ItemSlotInfo[] mItemInfos;
    private SkillSlotInfo[] mSkillInfos;

    private void Awake()
    {
        if (mSlots != null)
        {
            mInventoryAnimator = mSlots.GetComponentsInChildren<Animator>();
            mItemInfos = mSlots.GetComponentsInChildren<ItemSlotInfo>();
        }

        if(mSkillSlots != null)
        {
            mSkillInfos = mSkillSlots.GetComponentsInChildren<SkillSlotInfo>();
        }

        mButton = GetComponent<UIButton>();
        mButton.onConfirm += confirmEvent;
    }

    private void OnEnable()
    {
        StartCoroutine(CheckText());
    }

    private IEnumerator CheckText()
    {
        yield return null;

        for (int i = 0; i < this.transform.childCount; i++)
        {
            if (this.transform.GetChild(i).name == "Select_Image")
            {
                confirmEvent();
            }
        }
    }

    private void confirmEvent()
    {
        if(mInventoryType == EInventoryType.Category)
        {
            if(mItemCategory == EItemCategory.None || mItemCategory == EItemCategory.Material || mItemCategory == EItemCategory.Soulite || mItemCategory == EItemCategory.Valueable)
            {
                mItemSlots.SetActive(true);
                mSkillSlots.SetActive(false);
                mPopupObject.SetActive(false);

                //프레임 변경
                mButton.UIController.SelectInventoryTransform.GetComponent<SelectInventoryButton>().ChangeFrame(mInventoryAnimator, mItemCategory);
                //아이템 변경
                mButton.UIController.SelectInventoryTransform.GetComponent<SelectInventoryButton>().ChangeItem(mItemInfos, mItemCategory);
                //아이템 타입 변경
                mButton.UIController.SelectInventoryTransform.GetComponent<SelectInventoryButton>().ChangeType(mItemInfos, mItemCategory);
                //스크롤바 아이템 타입에 따라 늘어나고 줄어들기
                SlotController.CheckInventorySlotCount(mItemCategory);
            }
            else if(mItemCategory == EItemCategory.Skill)
            {
                mItemSlots.SetActive(false);
                mSkillSlots.SetActive(true);
                mPopupObject.SetActive(false);

                foreach(SkillSlotInfo skillSlot in mSkillInfos)
                {
                    skillSlot.CharacterType = mCharactgerType;
                    skillSlot.ChangeSkillType(mCharactgerType);
                }
            }
        }
        else if(mInventoryType == EInventoryType.EquipSlot)
        {
            mButton.UIController.OpenPanel(mSlotPanel, false);
            mPopupObject.SetActive(false);
        }
    }
}

public enum EInventoryType
{
    None = -1,
    Category,
    Slot,
    EquipSlot,
    Count
}