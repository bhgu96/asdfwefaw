﻿using UnityEngine;
using UnityEngine.UI;

public class InventoryMaterialText : MonoBehaviour
{
    private Text mText;
    [SerializeField]
    private int mRow;
    [SerializeField]
    private int mColumn;
    private AItem mItem;

    private void Awake()
    {
        mText = this.GetComponent<Text>();
        mRow = this.GetComponentInParent<ItemSlotInfo>().Row;
        mColumn = this.GetComponentInParent<ItemSlotInfo>().Column;
    }

    private void Update()
    {
        int count = Fairy_Player.Main.Inventory.Peek(EItemCategory.Material, mRow, mColumn, out mItem);
        mText.text = count.ToString();
    }
}
