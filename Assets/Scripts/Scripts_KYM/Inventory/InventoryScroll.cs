﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class InventoryScroll : MonoBehaviour
{
    private const int INVENTORY_SLOT_1 = 0;
    private const int INVENTORY_SLOT_2 = 430;
    private const int INVENTORY_SLOT_3 = 860;
    private const int INVENTORY_SLOT_4 = 1290;

    public static int MaxSlot { get { return mMaxSlot; } set { mMaxSlot = value; } }
    public bool IsSelect { get { return mIsSelect; } set { mIsSelect = value; } }

    [SerializeField]
    private RectTransform mSlot;

    private static int mMaxSlot = 1;
    private bool mIsSelect = true;
    private Scrollbar mScrollBar;
    private float mScrollValue;

    private void Awake()
    {
        mScrollBar = this.GetComponent<Scrollbar>();
    }

    private void Update()
    {
        ChangeScrollPosition(mMaxSlot);
    }

    private void ChangeScrollPosition(int maxSlot)
    {
        if(mIsSelect)
        {
            switch (maxSlot)
            {
                case 1:
                    mScrollValue = INVENTORY_SLOT_1;
                    mScrollBar.size = 1f;
                    break;
                case 2:
                    mScrollValue = INVENTORY_SLOT_2;
                    mScrollBar.size = 0.5f;
                    break;
                case 3:
                    mScrollValue = INVENTORY_SLOT_3;
                    mScrollBar.size = 0.33f;
                    break;
                case 4:
                    mScrollValue = INVENTORY_SLOT_4;
                    mScrollBar.size = 0.25f;
                    break;
            }
        }
    }

    public void MoveSlot()
    {
        Vector2 rectPos = mSlot.anchoredPosition;
        rectPos.y = mScrollValue * mScrollBar.value;
        mSlot.anchoredPosition = rectPos;
    }

    public static void AddSlot()
    {
        if(mMaxSlot < 4)
        {
            mMaxSlot++;
        }
    }
    
    public static void RemoveSlot()
    {
        if(mMaxSlot > 0)
        {
            mMaxSlot--;
        }
    }
}
