﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SelectInventoryButton : MonoBehaviour
{
    public bool IsSelected { get { return mIsSelected; } private set { } }

    private Image mImage;
    private bool mIsSelected;

    private void Awake()
    {
        mImage = this.GetComponent<Image>();
    }

    public void ChangeFrame(Animator[] animators, EItemCategory itemCategory)
    {
        switch (itemCategory)
        {
            case EItemCategory.Soulite:
                foreach(Animator soulLightAnimator in animators)
                {
                    soulLightAnimator.SetBool("IsSoul", true);
                    soulLightAnimator.SetBool("IsValuable", false);
                    soulLightAnimator.SetBool("IsMaterial", false);
                }
                break;
            case EItemCategory.Material:
                foreach (Animator materialAnimator in animators)
                {
                    materialAnimator.SetBool("IsMaterial", true);
                    materialAnimator.SetBool("IsSoul", false);
                    materialAnimator.SetBool("IsValuable", false);
                }
                break;
            case EItemCategory.Valueable:
                foreach (Animator valueableAnimator in animators)
                {
                    valueableAnimator.SetBool("IsValuable", true);
                    valueableAnimator.SetBool("IsSoul", false);
                    valueableAnimator.SetBool("IsMaterial", false);
                }
                break;
        }

    }

    public void ChangeItem(ItemSlotInfo[] items, EItemCategory itemCategory)
    {
        foreach (ItemSlotInfo item in items)
        {
            item.ChangeCategory(itemCategory);
        }
    }

    public void ChangeType(ItemSlotInfo[] items, EItemCategory itemCategory)
    {
        foreach (ItemSlotInfo item in items)
        {
            item.ItemCategory = itemCategory;
        }
    }
}
