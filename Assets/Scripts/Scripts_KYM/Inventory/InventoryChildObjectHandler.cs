﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class InventoryChildObjectHandler : AHud
{
    private Image[] mImages;
    private Text[] mTexts;
    private GameObject[] mChildObject;

    [SerializeField]
    private GameObject mSoulObject;

    private bool mIsOpen;
    private bool mIsStart;

    private void Awake()
    {
        mImages = this.GetComponentsInChildren<Image>();
        mTexts = this.GetComponentsInChildren<Text>();

        mChildObject = new GameObject[this.transform.childCount];

        for(int i = 0; i < this.transform.childCount; i++)
        {
            mChildObject[i] = this.transform.GetChild(i).gameObject;
        }

    }

    private void Start()
    {
        Deactivate();
    }

    public override void Activate()
    {
        foreach(GameObject childObject in mChildObject)
        {
            childObject.SetActive(true);
        }
        mSoulObject.SetActive(true);
    }

    public override void Deactivate()
    {
        foreach (GameObject childObject in mChildObject)
        {
            childObject.SetActive(false);
        }
        mSoulObject.SetActive(false);
    }

    public override void Express()
    {
        //if(Input.GetKeyDown(KeyCode.I) && !mIsOpen)
        //{
        //    mIsOpen = true;
        //    Activate();
        //}
        //else if(Input.GetKeyDown(KeyCode.I) && mIsOpen)
        //{
        //    mIsOpen = false;
        //    Deactivate();
        //}
    }

    public override void GetInformation()
    {
        ActiveFalse();
    }

    private void ActiveFalse()
    {
        if(!mIsStart)
        {
            mIsStart = true;
            Deactivate();
        }
    }

    private void AddStateEvent()
    {
        StateManager.Pause.AddEnterEvent(false, Activate);
        StateManager.Pause.AddEnterEvent(true, Deactivate);
        StateManager.Pause.AddExitEvent(false, Activate);
        StateManager.Pause.AddExitEvent(true, Deactivate);

        StateManager.GameOver.AddEnterEvent(false, Activate);
        StateManager.GameOver.AddEnterEvent(true, Deactivate);
        StateManager.GameOver.AddExitEvent(false, Activate);
        StateManager.GameOver.AddExitEvent(true, Deactivate);
    }

    private void OnDestroy()
    {
        RemoveStateEvent();
    }

    private void RemoveStateEvent()
    {
        StateManager.Pause.RemoveEnterEvent(false, Activate);
        StateManager.Pause.RemoveEnterEvent(true, Deactivate);
        StateManager.Pause.RemoveExitEvent(false, Activate);
        StateManager.Pause.RemoveExitEvent(true, Deactivate);

        StateManager.GameOver.RemoveEnterEvent(false, Activate);
        StateManager.GameOver.RemoveEnterEvent(true, Deactivate);
        StateManager.GameOver.RemoveExitEvent(false, Activate);
        StateManager.GameOver.RemoveExitEvent(true, Deactivate);
    }
}
