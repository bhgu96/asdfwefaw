﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SlotController : MonoBehaviour
{
    private ItemSlotInfo[] mSlotInfos;
    private static AItem mItem;
    private static int mCount;

    private void Start()
    {
        mSlotInfos = this.GetComponentsInChildren<ItemSlotInfo>();

        int slotIndex = 0;
        int normalizeSlotIndex = 0;

        for (int row = 0; row < Fairy_Player.Main.Inventory.CountOfRows; row++)
        {
            for (int column = 0; column < Fairy_Player.Main.Inventory.CountOfColumns; column++)
            {
                if (normalizeSlotIndex > 7)
                {
                    normalizeSlotIndex = 0;
                }

                mSlotInfos[slotIndex].NormalizeSlotIndex = normalizeSlotIndex;
                mSlotInfos[slotIndex].Row = row;
                mSlotInfos[slotIndex].Column = column;
                slotIndex++;
                normalizeSlotIndex++;
            }
        }
    }

    public static void CheckInventorySlotCount(EItemCategory itemCategory)
    {
        int count = 0;

        for (int row = 0; row < Fairy_Player.Main.Inventory.CountOfRows; row++)
        {
            for (int column = 0; column < Fairy_Player.Main.Inventory.CountOfColumns; column++)
            {
                if ((mCount = Fairy_Player.Main.Inventory.Peek(itemCategory, row, column, out mItem)) > 0)
                {
                    count++;
                }
            }
        }

        if(count/32 > InventoryScroll.MaxSlot - 1)
        {
            int maxCount = count/32 - (InventoryScroll.MaxSlot - 1);

            for(int i = 0; i < maxCount; i++)
            {
                InventoryScroll.AddSlot();
            }
        }
        else if(count/32 < InventoryScroll.MaxSlot - 1)
        {
            int maxCount = (InventoryScroll.MaxSlot - 1) - count/32;

            for(int i = 0; i < maxCount; i++ )
            {
                InventoryScroll.RemoveSlot();
            }
        }
    }
}
