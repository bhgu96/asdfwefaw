﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SetNPCDialogueFont : MonoBehaviour
{
    private Text mText;
    private int mDefaultFontSize;

    private void Awake()
    {
        mText = this.GetComponent<Text>();
        mDefaultFontSize = this.GetComponent<Text>().fontSize;
    }

    private void Update()
    {
        if (mText != null && _FontManager.CurrentFont != null)
        {
            if (_FontManager.FontType == EFont.PixelOperator8)
            {
                if (mDefaultFontSize >= 70)
                {
                    mText.fontSize = mDefaultFontSize - 25;
                }
                else if (mDefaultFontSize >= 50)
                {
                    mText.fontSize = mDefaultFontSize - 25;
                }
                else if (mDefaultFontSize >= 45)
                {
                    mText.fontSize = mDefaultFontSize - 15;
                }
                else if (mDefaultFontSize >= 30)
                {
                    mText.fontSize = mDefaultFontSize - 10;
                }
                else
                {
                    mText.fontSize = mDefaultFontSize - 5;
                }
            }
            else
            {
                mText.fontSize = mDefaultFontSize;
            }
        }
    }
}
