﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ButtonEvent_ExitUI : MonoBehaviour
{
    private UIButton mUIButton;

    private void Awake()
    {
        mUIButton = this.GetComponent<UIButton>();

        mUIButton.onConfirm += ConfirmButton;
    }

    private void ConfirmButton()
    {
        this.transform.parent.gameObject.SetActive(false);
    }
}
