﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SanctuaryNPCScript : MonoBehaviour
{
    [SerializeField]
    private GameObject mSelectObject;
    [SerializeField]
    private Text mText;
    [SerializeField]
    private Text _mTest;
    [SerializeField]
    private string[] mKorDialogue;
    [SerializeField]
    private string[] mEngDialogue;
    [SerializeField]
    private float mMaxVerticalSize;

    private RectTransform mDialogueFrame;

    private void Awake()
    {
        mDialogueFrame = this.GetComponent<RectTransform>();
    }

    private void OnEnable()
    {
        mSelectObject.SetActive(false);
        StartCoroutine(ExpressDialogue());
    }

    private void Update()
    {
        mText.font = _FontManager.CurrentFont;
        _mTest.font = _FontManager.CurrentFont;
    }

    private IEnumerator ExpressDialogue()
    {
        mText.text = null;
        _mTest.text = null;
        mDialogueFrame.sizeDelta = new Vector2(mDialogueFrame.sizeDelta.x, 0f);

        //대화창 열리기(수직)

        for (float h = 0; h <= mMaxVerticalSize; h += Time.deltaTime * 150f)
        {
            mDialogueFrame.sizeDelta = new Vector2(mDialogueFrame.sizeDelta.x, h);
            yield return null;
        }

        //대화 시작
        for(int i = 0; i < mKorDialogue.Length; i++)
        {
            if (_FontManager.LanguageType == ELanguageType.Korean)
            {
                for (int t = 0; t < mKorDialogue[i].Length; t++)
                {
                    mText.text += mKorDialogue[i][t];
                    _mTest.text += mKorDialogue[i][t];
                    yield return new WaitForSeconds(0.05f);
                }
            }
            else if(_FontManager.LanguageType == ELanguageType.English)
            {
                for (int t = 0; t < mEngDialogue[i].Length; t++)
                {
                    mText.text += mEngDialogue[i][t];
                    _mTest.text += mEngDialogue[i][t];
                    yield return new WaitForSeconds(0.05f);
                }
            }

            yield return new WaitForSeconds(1.0f);
            mText.text = null;
            _mTest.text = null;
        }

        for (float h = mMaxVerticalSize; h >= 0; h -= Time.deltaTime * 150f)
        {
            mDialogueFrame.sizeDelta = new Vector2(mDialogueFrame.sizeDelta.x, h);
            yield return null;
        }

        mSelectObject.SetActive(true);
        mSelectObject.transform.parent.gameObject.SetActive(false);
        this.transform.parent.gameObject.SetActive(false);
    }
}
