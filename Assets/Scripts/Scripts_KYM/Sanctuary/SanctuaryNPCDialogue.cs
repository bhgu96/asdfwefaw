﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SanctuaryNPCDialogue : MonoBehaviour
{
    [SerializeField]
    private GameObject mDialogueObject;

    private UIButton mButton;

    private void Awake()
    {
        mButton = this.GetComponent<UIButton>();
        mButton.onConfirm += ConfirmButton;
    }

    private void ConfirmButton()
    {
        mDialogueObject.SetActive(true);
    }
}
