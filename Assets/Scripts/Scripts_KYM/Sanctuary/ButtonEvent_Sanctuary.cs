﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ButtonEvent_Sanctuary : MonoBehaviour
{
    [SerializeField]
    private EGameScene mSceneName;

    private UIButton mButton;

    private void Awake()
    {
        mButton = GetComponent<UIButton>();
        mButton.onConfirm += confirmEvent;
    }

    private void confirmEvent()
    {
        StateManager.Pause.State = false;
        StateManager.GameOver.State = false;
        PlayerData.Save();
        Time.timeScale = 1.0f;

        LoadingController.LoadScene(mSceneName.ToString());
    }
}
