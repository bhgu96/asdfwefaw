﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SanctuaryButtonEvent_Trade : MonoBehaviour
{
    [SerializeField]
    private GameObject mShopObject;
    [SerializeField]
    private GameObject mInventoryObject;
    [SerializeField]
    private GameObject mNPCCanvasObject;

    private UIButton mButton;

    private void Awake()
    {
        mButton = GetComponent<UIButton>();
        mButton.onConfirm += ConfirmButton;
    }

    private void ConfirmButton()
    {
        mShopObject.SetActive(true);
        mInventoryObject.SetActive(true);
        mNPCCanvasObject.SetActive(false);
    }
}
