﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SanctuaryCloudMove : MonoBehaviour
{
    [SerializeField]
    private float mXAccel;
    [SerializeField]
    private float mYAccel;

    private float mMaxX;
    private float mMinX;
    private float mMaxY;
    private float mMinY;

    private bool mIsRight = true;
    private bool mIsDown;

    private void Awake()
    {
        mMaxX = this.transform.localPosition.x + 1f;
        mMinX = this.transform.localPosition.x - 1f;
        mMaxY = this.transform.localPosition.y + 1f;
        mMinY = this.transform.localPosition.y - 1f;
    }

    private void Update()
    {
        if(mIsRight)
        {
            this.transform.Translate(new Vector2(+mXAccel * Time.deltaTime, 0));
        }
        else if(!mIsRight)
        {
            this.transform.Translate(new Vector2(-mXAccel * Time.deltaTime, 0));
        }

        if(mIsDown)
        {
            this.transform.Translate(new Vector2(0, -mYAccel * Time.deltaTime));
        }
        else if(!mIsDown)
        {
            this.transform.Translate(new Vector2(0, +mYAccel * Time.deltaTime));
        }


        if(this.transform.localPosition.x >= mMaxX)
        {
            mIsRight = false;
        }
        else if(this.transform.localPosition.x <= mMinX)
        {
            mIsRight = true;
        }

        if(this.transform.localPosition.y >= mMaxY)
        {
            mIsDown = true;
        }
        else if(this.transform.localPosition.y <= mMinY)
        {
            mIsDown = false;
        }
    }
}
