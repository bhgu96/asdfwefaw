﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SanctuaryShadow : MonoBehaviour
{
    [SerializeField]
    private Transform mAlphaPosition;
    [SerializeField]
    private Transform mFairyPosition;
    [SerializeField]
    private float mWorldY;

    private SpriteRenderer mSpriteRenderer;

    private void Awake()
    {
        mSpriteRenderer = this.GetComponent<SpriteRenderer>();
    }

    private void Update()
    {
        if(mFairyPosition.position.y >= mAlphaPosition.position.y)
        {
            mSpriteRenderer.color = new Color(1, 1, 1, 1 - (mFairyPosition.position.y - mAlphaPosition.position.y) / mWorldY);
        }   
        else
        {
            mSpriteRenderer.color = new Color(1, 1, 1, 0.8f);
        }
    }
}
