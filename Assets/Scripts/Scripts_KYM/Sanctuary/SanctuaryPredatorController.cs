﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SanctuaryPredatorController : MonoBehaviour
{
    private int mGiantWaitCount;
    private Animator mAnimator;

    private void Awake()
    {
        mAnimator = this.GetComponent<Animator>();
    }

    private void GiantWait()
    {
        mGiantWaitCount++;

        if (mGiantWaitCount > 3)
        {
            mGiantWaitCount = 0;
        }
    }

    private void GiantMove()
    {
        mGiantWaitCount = 0;
    }

    private void Update()
    {
        mAnimator.SetInteger("waitCount", mGiantWaitCount);
    }
}
