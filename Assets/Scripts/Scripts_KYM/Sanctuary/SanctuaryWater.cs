﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SanctuaryWater : MonoBehaviour
{
    [SerializeField]
    private Transform[] mWaterTransform;
    [SerializeField]
    private float mWaterSpeed;

    private Vector2 mLeftTransform;
    private Vector2 mMiddleTransform;
    private Vector2 mRightTransform;

    private void Awake()
    {
        mLeftTransform = mWaterTransform[0].position;
        mMiddleTransform = mWaterTransform[1].position;
        mRightTransform = mWaterTransform[2].position;
    }

    private void Update()
    {
        Transform tempLeftTransform = mWaterTransform[0];
        Transform tempMiddleTransform = mWaterTransform[1];
        Transform tempRightTrasnform = mWaterTransform[2];

        for (int i = 0; i < mWaterTransform.Length; i++)
        {
            mWaterTransform[i].Translate(new Vector2(-mWaterSpeed * Time.deltaTime, 0.0f));
        }

        if(mWaterTransform[1].position.x <= mLeftTransform.x)
        {
            mWaterTransform[0] = tempMiddleTransform;
            mWaterTransform[1] = tempRightTrasnform;
            mWaterTransform[2] = tempLeftTransform;

            mWaterTransform[0].position = mLeftTransform;
            mWaterTransform[1].position = mMiddleTransform;
            mWaterTransform[2].position = mRightTransform;
        }
    }
}
