﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SanctuaryPortal : MonoBehaviour
{
    private Animator mAnimator;

    private void Awake()
    {
        mAnimator = this.GetComponentInParent<Animator>();
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        mAnimator.SetBool("IsStart", true);
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        mAnimator.SetBool("IsStart", false);
    }
}
