﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SanctuaryIsland : MonoBehaviour
{
    [SerializeField]
    private float mAccel;
    [SerializeField]
    private float mMax;
    [SerializeField]
    private float mMin;

    private bool mIsUp;
    private float mMaxY;
    private float mMinY;

    private void Awake()
    {
        mMaxY = this.transform.localPosition.y + mMax;
        mMinY = this.transform.localPosition.y + mMin;
    }

    private void Update()
    {
        if(mIsUp)
        {
            this.transform.Translate(new Vector2(0f,+mAccel * Time.deltaTime));
        }
        else if(!mIsUp)
        {
            this.transform.Translate(new Vector2(0f, -mAccel * Time.deltaTime));
        }

        if (this.transform.localPosition.y >= mMaxY)
        {
            mIsUp = false;
        }
        else if (this.transform.localPosition.y <= mMinY)
        {
            mIsUp = true;
        }
    }
}
