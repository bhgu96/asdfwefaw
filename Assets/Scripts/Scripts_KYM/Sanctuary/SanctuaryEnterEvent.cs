﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SanctuaryEnterEvent : MonoBehaviour
{
    [SerializeField]
    private CameraController mCamera;
    [SerializeField]
    private EGameScene mEnterScene;

    [SerializeField]
    private AudioSource mAudio;
    [SerializeField]
    private AudioClip mDoorOpenAudio;
    [SerializeField]
    private AudioClip mDoorCloseAudio;
    [SerializeField]
    private AudioClip mDoorOpeningAudio;

    private void EnterPotal()
    {
        mCamera.BlackOut(0.5f, 1f);
        StartCoroutine(Fade());
    }

    private IEnumerator Fade()
    {
        yield return new WaitUntil(() => !mCamera.IsBlackOutTriggered);
        LoadingController.LoadScene(mEnterScene.ToString());
    }

    private void DoorOpen()
    {
        mAudio.clip = mDoorOpenAudio;
        mAudio.Play();
        mAudio.loop = false;
    }

    private void DoorClose()
    {
        mAudio.clip = mDoorCloseAudio;
        mAudio.Play();
        mAudio.loop = false;
    }

    private void DoorOpening()
    {
        mAudio.clip = mDoorOpeningAudio;
        mAudio.Play();
        mAudio.loop = true;
    }
}
