﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SanctuaryNPC : MonoBehaviour
{
    [SerializeField]
    private ENPCType mNPCType;
    [SerializeField]
    private GameObject mInteractCanvasObject;

    private OutlineController mOutline;

    private void Awake()
    {
        mOutline = this.GetComponent<OutlineController>();
    }

    private void OnMouseDown()
    {
        //옵션들 나온다
        mInteractCanvasObject.SetActive(true);
    }

    private void Update()
    {
        if(mOutline.Toggle == 0)
        {
            if(Input.GetKeyDown(KeyCode.Mouse0))
            {
                mInteractCanvasObject.SetActive(false);
            }
        }
    }
}

public enum ENPCType
{
    None = -1,
    Equipment,
    Skill,
    Count
}

