﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SanctuaryEnterPotal : MonoBehaviour
{
    public bool IsEnter { get { return mIsEnter; } set { mIsEnter = value; } }
    public UIPanel AlterPanel { get { return mAlterPanel; } private set { } }
    public UIController UIController { get { return mUIController; } private set { } }

    [SerializeField]
    private SpriteRenderer mFairySprite;
    [SerializeField]
    private SpriteRenderer mGiantSprite;
    [SerializeField]
    private UIController mUIController;
    [SerializeField]
    private UIPanel mAlterPanel;

    private Animator mAnimator;
    private bool mIsDetectFairy;
    private bool mIsEnter;

    private void Awake()
    {
        mIsEnter = false;
        mAnimator = this.GetComponentInParent<Animator>();
    }

    private void Update()
    {
        if(mIsDetectFairy && !mIsEnter)
        {         
            if(Input.GetKeyDown(KeyCode.Space))
            {
                mAlterPanel.gameObject.SetActive(true);
                mUIController.EnableTitleController();
                mUIController.CurButton = mAlterPanel.HeaderButton;
                mIsEnter = true;
            }
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        mIsDetectFairy = true;
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        mIsDetectFairy = false;
    }

    public void EnterPotalEvent()
    {
        StartCoroutine(FadeFairy());
    }

    private IEnumerator FadeFairy()
    {
        yield return null;
        mFairySprite.transform.parent.gameObject.SetActive(false);
        yield return null;
        mAnimator.SetBool("IsEnter", true);
    }

    private IEnumerator GiantFade()
    {
        yield return null;

        for (float a = 1; a >= 0; a -= Time.deltaTime)
        {
            mGiantSprite.color = new Color(1, 1, 1, a);
            yield return null;
        }

        mGiantSprite.color = new Color(1, 1, 1, 0);

        yield return new WaitUntil(() => mGiantSprite.color.a == 0);
        mGiantSprite.gameObject.SetActive(false);
        mAnimator.SetBool("IsEnter", true);
    }
}
