﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class IntroController : MonoBehaviour
{
    [SerializeField]
    private CameraController mCamera;
    //[SerializeField]
    //private string mNextScene;
    [SerializeField]
    private EGameScene mNextScene;

    private Animator mAnimator;
    private AudioHandler<System.Enum> mAudioTable;

    private void Awake()
    {
        Application.targetFrameRate = 120;
        Screen.SetResolution(1920, 1080, true);
        CustomCursor.InvisibleCursor();
        mAnimator = this.GetComponent<Animator>();
        mAudioTable = this.GetComponent<AudioHandler<System.Enum>>();
    }

    private void Start()
    {
        mAnimator.SetBool("start", true);
    }

    private void animation_Finish()
    {
        mAnimator.SetBool("start", false);
    }

    private IEnumerator animation_Fade()
    {
        mCamera.BlackOut(0.5f, 1.0f);
        yield return new WaitUntil(() => !mCamera.IsBlackOutTriggered);
        CustomCursor.VisibleCursor();
        SceneManager.LoadScene(mNextScene.ToString());
    }

    private void animation_PlayAudio()
    {
        //mAudioTable.PlayAudio(EAudioFlags.Count, 0);
    }
}
