﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIInputController : AInput
{

    public UIInputController() : base(0)
    {

    }
    public override void Check()
    {
        CheckTitleInput();
    }

    private void CheckTitleInput()
    {
        SetMoveHorizontalMenu(InputManager.GetFloat(EInputType.Right_Menu) - InputManager.GetFloat(EInputType.Left_Menu));
        SetMoveVerticalMenu(InputManager.GetFloat(EInputType.Up_Menu) - InputManager.GetFloat(EInputType.Down_Menu));
        SetConfirm(InputManager.GetBool(EInputType.Confirm) || Input.GetKeyDown(KeyCode.Space));
        SetEscape(InputManager.GetBool(EInputType.Escape) || Input.GetKeyDown(KeyCode.LeftShift) || Input.GetKeyDown(KeyCode.RightShift));
    }
}
