﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public enum EGameScene
{
    None = -1,
    Scene_Intro,
    Scene_Loading,
    Scene_Movie,
    Scene_Title,
    Scene_Tutorial,
    Scene_Abyss_Asgard,
    Scene_Abyss_MemoryOfAstral,
    _Scene_Abyss_Santuary_KYM,
    Scene_NewTitle,
    Count
}

public class LoadingController : MonoBehaviour
{
    public static bool IsGameStart { get { return mIsGameStart; } private set { } }
    public static bool IsCompleted { get { return mIsCompleted; } private set { } }
    public static AsyncOperation AsyncOperation { get { return mAsyncOperation; } private set { } }
    public static string NextScene { get { return mNextScene; } private set { } }

    [SerializeField]
    private CameraController mCameraHandler;
    [SerializeField]
    private Text mText;
    [SerializeField]
    private string mStartText;
    [SerializeField]
    private BridgeSpiritAnimation mBridgeFairyAnimation;
    [SerializeField]
    private GameObject mStagePlanObject;

    private static string mNextScene;
    private static AsyncOperation mAsyncOperation;
    private static bool mIsCompleted = false;
    private static bool mIsGameStart = false;

    private bool mIsWait = false;
    private Dictionary<string, bool> mLoadSceneDict;

    /// <summary>
    /// 특정 씬에서 이 메소드를 호출하면 로딩 씬으로 넘어오고, 다음 씬을 로딩한다.
    /// </summary>
    /// <param name="_nextScene">로드할 씬 이름</param>
    public static void LoadScene(string _nextScene)
    {
        mNextScene = _nextScene;
        //SceneManager.LoadScene("Scene_Loading");
        //SceneManager.LoadScene("Scene_Bridge");
        SceneManager.LoadScene("Scene_NewLoading");
    }

    private void Awake()
    {
        mIsCompleted = false;
        mIsGameStart = false;
        CustomCursor.InvisibleCursor();
        mLoadSceneDict = new Dictionary<string, bool>();
        AddScene();
        mCameraHandler.BlackOut(1.0f, 0.0f);
    }

    private void Start()
    {
        mAsyncOperation = null;
        mAsyncOperation = SceneManager.LoadSceneAsync(mNextScene);
        mAsyncOperation.allowSceneActivation = false;


        if(!mLoadSceneDict[mNextScene])
        {
            mStagePlanObject.SetActive(true);
        }
        else
        {
            mStagePlanObject.SetActive(false);
        }
    }

    private void Update()
    {
        if (!mIsCompleted)
        {
            if (mAsyncOperation.progress >= 0.9f - Mathf.Epsilon && !mCameraHandler.IsBlackOutTriggered)
            {
                mIsCompleted = true;
            }
        }
        else if (mIsCompleted)
        {
            if(!mLoadSceneDict[mNextScene])
            {
                if(!mCameraHandler.IsBlackOutTriggered)
                {
                    //CustomCursor.VisibleCursor();
                    //mAsyncOperation.allowSceneActivation = true;

                    //3초 정도 있다가 씬 로드
                    StartCoroutine(WaitNextScene());
                }
            }
            else if(mLoadSceneDict[mNextScene])
            {
                CustomCursor.VisibleCursor();
                mAsyncOperation.allowSceneActivation = true;
                //StartCoroutine(WaitPressAnyKey());
            }
        }
    }

    private void AddScene()
    {
        mLoadSceneDict.Add("_Scene_Abyss_Santuary_KYM", true);
        mLoadSceneDict.Add("Scene_Abyss_MemoryOfAstral", false);
        mLoadSceneDict.Add("Scene_Tutorial", false);
        mLoadSceneDict.Add("_Scene_Abyss_MemoryOfAstral KYM", false);
        mLoadSceneDict.Add("_Scene_Abyss_MemoryOfAstral_JHJ", false);
        mLoadSceneDict.Add("Scene_Title", false);
        mLoadSceneDict.Add("Scene_Movie", false);
        mLoadSceneDict.Add("Scene_Abyss_Asgard", false);
        mLoadSceneDict.Add("Scene_NewTitle", true);
    }

    private IEnumerator WaitPressAnyKey()
    {
        if(mIsWait)
        {
            yield break;
        }
        mIsWait = true;

        yield return null;

        mText.gameObject.SetActive(true);
        mText.text = mStartText;
        yield return new WaitUntil(() => Input.anyKeyDown);

        mIsGameStart = true;

        //임시
        if(mBridgeFairyAnimation != null)
        {
            yield return new WaitUntil(() => mBridgeFairyAnimation.IsEndLoading);
        }

        mCameraHandler.BlackOut(1.0f, 1.0f);
        yield return new WaitUntil(() => !mCameraHandler.IsBlackOutTriggered);
        CustomCursor.VisibleCursor();
        mAsyncOperation.allowSceneActivation = true;
    }

    private IEnumerator WaitNextScene()
    {
        yield return new WaitForSeconds(3.0f);
        CustomCursor.VisibleCursor();
        mAsyncOperation.allowSceneActivation = true;
    }
}
