﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LoadingTutoralAnimation : MonoBehaviour
{
    public Image[] ChildImages { get { return mChildImages; } private set { } }
    public int Index { get { return mIndex; } private set { } }
    public string Description { get { return mDescription; } private set { } }
    public int Count { get { return mCount; } set { mCount = value; } }

    [SerializeField]
    private Image[] mChildImages;
    [SerializeField]
    private int mIndex;
    [SerializeField]
    private string mDescription;
    [SerializeField]
    private int mCount;

    private void Awake()
    {
        if(mIndex != 0)
        {
            for(int i = 0; i < mChildImages.Length; i++)
            {
                mChildImages[i].color = new Color(1, 1, 1, 0);
            }
            this.gameObject.SetActive(false);
        }
    }

    //어두워질때
    public IEnumerator FadeOut()
    {
        for (float a = 1; a >= 0; a -= Time.deltaTime)
        {
            for (int i = 0; i < mChildImages.Length; i++)
            {
                mChildImages[i].color = new Color(1, 1, 1, a);
            }

            yield return null;
        }

        this.gameObject.SetActive(false);
    }

    //밝아질때
    public IEnumerator FadeIn()
    {
        for (float a = 0; a <= 1; a += Time.deltaTime)
        {
            for (int i = 0; i < mChildImages.Length; i++)
            {
                mChildImages[i].color = new Color(1, 1, 1, a);
            }

            yield return null;
        }
    }

    private void AnimationCount()
    {
        mCount++;
    }

    private void OnDisable()
    {
        mCount = 0;
    }
}
