﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public enum EStateFlags_UI
{
    
}

public class UIController : AControllable<EStateFlags_UI>
{
    public UIPanel GameClearPanel { get { return mGameClearPanel; } private set { } }
    public RectTransform SelectInventoryTransform { get { return mSelectInventoryTransform; } private set { } }

    [SerializeField]
    private CameraController mCamera;
    [SerializeField]
    private UIPanel mStartPanel;
    [SerializeField]
    private UIPanel mGameClearPanel;
    [SerializeField]
    private RectTransform mSelectInventoryTransform;

    //타이틀,인게임 구분
    [SerializeField]
    private bool mIsInGame;
    [SerializeField]
    private bool mIsInventory;

    private Dictionary<ETitlePanel, UIPanel> mPanelDict;
    private Stack<UIPanel> mPanelStack;
    private UIButton mCurButton;
    private bool mIsDown = false;
    private bool mIsUp = false;
    private bool mIsLeft = false;
    private bool mIsRight = false;

    public UIButton CurButton
    {
        get { return mCurButton; }
        set
        {
            if (value)
            {
                
                mCurButton?.Deselect();
                mCurButton = value;
                mCurButton.Select();

                if (mSelectInventoryTransform != null)
                {
                    mSelectInventoryTransform.SetParent(value.RectTransform);
                    mSelectInventoryTransform.sizeDelta = CurButton.GetComponent<RectTransform>().sizeDelta;
                    mSelectInventoryTransform.anchorMin = CurButton.GetComponent<RectTransform>().anchorMin;
                    mSelectInventoryTransform.anchorMax = CurButton.GetComponent<RectTransform>().anchorMax;
                    mSelectInventoryTransform.anchoredPosition = Vector2.zero;
                    mSelectInventoryTransform.localPosition = new Vector3(mSelectInventoryTransform.localPosition.x, mSelectInventoryTransform.localPosition.y, 0f);

                    if(CurButton.GetComponent<RectTransform>().anchorMin.y != CurButton.GetComponent<RectTransform>().anchorMax.y)
                    {
                        mSelectInventoryTransform.anchorMin = new Vector2(0, 0);
                        mSelectInventoryTransform.anchorMax = new Vector2(1, 1);
                    }
                }
            }
        }
    }

    protected override void Awake()
    {
        base.Awake();

        mPanelStack = new Stack<UIPanel>();
        mPanelDict = new Dictionary<ETitlePanel, UIPanel>();

        
        UIPanel[] panels = GetComponentsInChildren<UIPanel>(true);
        foreach (UIPanel panel in panels)
        {
            mPanelDict[panel.EPanel] = panel;
        }
        SetInputs(new UIInputController());
    }

    private void Start()
    {
        DisableTitleController();
        EnableTitleController();


        if (!mIsInGame)
        {
            OpenPanel(mStartPanel);
        }
        else if(mIsInventory)
        {
            SetDefaultPanel(mStartPanel);
            EnableTitleController();
            OpenPanel(mStartPanel);
        }
        else
        {
            SetDefaultPanel(mStartPanel);
        }
    }

    protected override void Update()
    {
        base.Update();
        if(!mCamera.IsWhiteOutTriggered)
        {
            CheckCommand();
        }
    }

    private void CheckCommand()
    {
        UIButton nextButton = null;


        if (Input.ConfirmDown && Input.CanConfirm)
        {
            Input.ConfirmDown = false;
            CurButton?.Confirm();
        }
        else if (Input.CancelDown && Input.CanCancel)
        {
            Input.CancelDown = false;

            //게임 오버가 될 경우, 게임 오버 패널이 나오고 x나 esc를 눌러도 패널이 꺼지지 않도록 막아둠
            //게임 클리어 해도 마찬가지
            if(StateManager.GameOver.State /*|| StateManager.GameClearHandler.State == EGameClearFlags.CLEAR*/)
            {
                return;
            }
            else
            {
                ClosePanel();
                
                //스택이 없는 경우 DefaultPanel을 연다.
                if (mPanelStack.Count == 0)
                {
                    OpenPanel(mStartPanel);
                }
                else
                {
                    //이전 스택 가져오기
                    UIPanel prevPanel = mPanelStack.Pop();
                    OpenPanel(prevPanel);
                }
            }
        }
        else if (Input.EscapeDown)
        {
            //게임 오버가 될 경우, 게임 오버 패널이 나오고 x나 esc를 눌러도 패널이 꺼지지 않도록 막아둠
            //게임 클리어해도 마찬가지
            if (StateManager.GameOver.State /*|| StateManager.GameClearHandler.State == EGameClearFlags.CLEAR*/)
            {
                return;
            }
            else
            {
                if(mIsInGame && StateManager.Pause.State && mPanelStack.Count == 1)
                {
                    DisableTitleController();
                    StateManager.Pause.State = false;
                    Time.timeScale = 1.0f;
                    ExitPanel();
                    return;
                }

                ClosePanel();
 
                // 인게임일 경우 esc키를 누르면 메뉴가 나온다.
                if (mIsInGame && mPanelStack.Count == 0)
                {
                    EnableUIMenu();
                    OpenPanel(mStartPanel);
                }
                else
                {
                    if (mPanelStack.Count == 0)
                    {
                        OpenPanel(mStartPanel);
                    }
                    else
                    {
                        //이전 스택 가져오기
                        UIPanel prevPanel = mPanelStack.Pop();
                        OpenPanel(prevPanel);
                    }
                }
            }
        }
        else if (Input.MoveVerticalMenu > 0 && !mIsUp)
        {
            mIsUp = true;
            nextButton = mCurButton?.MoveUp() as UIButton;

            if (nextButton != null)
            {
                CurButton = nextButton;
            }
        }
        else if (Input.MoveVerticalMenu < 0 && !mIsDown)
        {
            mIsDown = true;
            nextButton = mCurButton?.MoveDown() as UIButton;

            if (nextButton != null)
            {
                CurButton = nextButton;
            }
        }
        else if (Input.MoveHorizontalMenu > 0 && !mIsRight)
        {
            mIsRight = true;
            nextButton = mCurButton?.MoveRight() as UIButton;

            if (nextButton != null)
            {
                CurButton = nextButton;
            }
        }
        else if (Input.MoveHorizontalMenu < 0 && !mIsLeft)
        {
            mIsLeft = true;
            nextButton = mCurButton?.MoveLeft() as UIButton;

            if (nextButton != null)
            {
                CurButton = nextButton;
            }
        }

        if (Input.MoveVerticalMenu == 0)
        {
            mIsDown = false;
            mIsUp = false;
        }
        if (Input.MoveHorizontalMenu == 0)
        {
            mIsLeft = false;
            mIsRight = false;
        }
    }

    public void DisableTitleController()
    {
        for (int i = 0; i < Input.CanSkill.Length; i++)
        {
            Input.CanSkill[i] = false;
        }

        Input.CanInteract = false;

        Input.CanMoveHorizontalMenu = false;
        Input.CanMoveVerticalMenu = false;
        Input.CanConfirm = false;
        Input.CanCancel = false;
    }

    public void EnableTitleController()
    {
        for (int i = 0; i < Input.CanSkill.Length; i++)
        {
            Input.CanSkill[i] = true;
        }

        Input.CanInteract = true;

        Input.CanMoveHorizontalMenu = true;
        Input.CanMoveVerticalMenu = true;
        Input.CanConfirm = true;
        Input.CanCancel = true;
    }

    public void OpenPanel(UIPanel panel)
    {
        if (mPanelStack.Count != 0)
        {
            UIPanel prevPanel = mPanelStack.Peek();
            prevPanel.SetEnable(false);
        }

        mPanelStack.Push(panel);
        panel.SetEnable(true);

        CurButton = panel.HeaderButton;
    }

    public void OpenPanel(UIPanel panel, bool isClosePrevPanel)
    {
        if(!isClosePrevPanel)
        {
            mPanelStack.Push(panel);
            panel.SetEnable(true);
            CurButton = panel.HeaderButton;
        }
        else
        {
            if (mPanelStack.Count != 0)
            {
                UIPanel prevPanel = mPanelStack.Peek();
                prevPanel.SetEnable(false);
            }

            mPanelStack.Push(panel);
            panel.SetEnable(true);

            CurButton = panel.HeaderButton;
        }
    }

    public void ClosePanel()
    {
        UIPanel panel = mPanelStack.Pop();
        panel.SetEnable(false);

        if (mPanelStack.Count != 0 /*&& mPanelStack.Count != 1*/)
        {
            panel = mPanelStack.Peek();
            panel.SetEnable(true);
            CurButton = panel.CurButton;
        }
    }

    public void ExitPanel()
    {
        if (mPanelStack.Count != 0)
        {
            UIPanel prevPanel = mPanelStack.Peek();
            prevPanel.SetEnable(false);
        }
    }

    private void SetDefaultPanel(UIPanel panel)
    {
        if (mPanelStack.Count != 0)
        {
            UIPanel prevPanel = mPanelStack.Peek();
            prevPanel.SetEnable(false);
        }

        mPanelStack.Push(panel);
    }

    private void EnableUIMenu()
    {
        EnableTitleController();
        StateManager.Pause.State = true;
        Time.timeScale = 0.0f;
    }
}
