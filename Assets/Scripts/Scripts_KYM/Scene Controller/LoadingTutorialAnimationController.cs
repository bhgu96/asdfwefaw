﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LoadingTutorialAnimationController : MonoBehaviour
{
    public int Index { get { return mIndex; } private set { } }

    [SerializeField]
    private LoadingTutoralAnimation[] mChildTutorialAnimations;
    [SerializeField]
    private Text mDescriptionText;
    private int mIndex;
    private int mNextIndex;

    private void Start()
    {
        StartCoroutine(AnimationSequence());
    }

    private IEnumerator AnimationSequence()
    {
        while(true)
        {
            for(int i = 0; i < mChildTutorialAnimations.Length; i++)
            {
                if(i != mIndex)
                {
                    mChildTutorialAnimations[i].gameObject.SetActive(false);
                }
            }

            if(mIndex >= mChildTutorialAnimations.Length)
            {
                mIndex = 0;
            }

            mChildTutorialAnimations[mIndex].gameObject.SetActive(true);
            mDescriptionText.text = mChildTutorialAnimations[mIndex].Description;

            while(mChildTutorialAnimations[mIndex].Count <= 2)
            {
                if(InputManager.GetFloat(EInputType.Right_Menu) - InputManager.GetFloat(EInputType.Left_Menu) > 0)
                {
                    MoveNextAnimation();
                    break;
                }
                else if(InputManager.GetFloat(EInputType.Right_Menu) - InputManager.GetFloat(EInputType.Left_Menu) < 0)
                {
                    MovePreviousAnimation();
                    break;
                }
                yield return null;
            }

            if(mChildTutorialAnimations[mIndex].Count >= 2)
            {
                MoveNextAnimation();
            }

            yield return new WaitUntil(() => !mChildTutorialAnimations[mIndex].gameObject.activeSelf);
            mIndex = mNextIndex;

            yield return null;
        }
    }

    private void MoveNextAnimation()
    {
        mNextIndex = mIndex + 1;

        if (mNextIndex >= mChildTutorialAnimations.Length)
        {
            mNextIndex = 0;
        }

        mChildTutorialAnimations[mNextIndex].gameObject.SetActive(true);
        StartCoroutine(mChildTutorialAnimations[mIndex].FadeOut());
        StartCoroutine(mChildTutorialAnimations[mNextIndex].FadeIn());
    }

    private void MovePreviousAnimation()
    {
        mNextIndex = mIndex - 1;

        if (mNextIndex < 0)
        {
            mNextIndex = mChildTutorialAnimations.Length - 1;
        }

        mChildTutorialAnimations[mNextIndex].gameObject.SetActive(true);
        StartCoroutine(mChildTutorialAnimations[mIndex].FadeOut());
        StartCoroutine(mChildTutorialAnimations[mNextIndex].FadeIn());
    }
}
