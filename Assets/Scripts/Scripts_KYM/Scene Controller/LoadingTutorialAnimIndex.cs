﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LoadingTutorialAnimIndex : MonoBehaviour
{
    [SerializeField]
    private int mIndex;
    [SerializeField]
    private LoadingTutorialAnimationController mController;

    private RectTransform mRectTransform;
    private Vector2 mInitSize;

    private void Awake()
    {
        mRectTransform = this.GetComponent<RectTransform>();
        mInitSize = mRectTransform.sizeDelta;
    }

    private void Update()
    {
        if(mController.Index == mIndex)
        {
            mRectTransform.sizeDelta = mInitSize * 2f;
        }
        else
        {
            mRectTransform.sizeDelta = mInitSize;
        }
    }
}
