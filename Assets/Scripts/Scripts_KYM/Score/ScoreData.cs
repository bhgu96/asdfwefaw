﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using LitJson;
using System.IO;

public class ScoreData
{
    private static List<GameScore> mGameScore = new List<GameScore>();
    private static List<GameScore> mCurrentGameScore = new List<GameScore>();

    public static void Save()
    {
        if(Score.UserName == null)
        {
            return;
        }

        mGameScore.Clear();
        string jsonString = File.ReadAllText(Application.streamingAssetsPath + " /GameScore.json");
        JsonData ScoreData = JsonMapper.ToObject(jsonString);

        for(int i = 0; i < ScoreData.Count; i++)
        {
            string temScore = ScoreData[i]["Score"].ToString();
            string tmpName = ScoreData[i]["Name"].ToString();
            mGameScore.Add(new GameScore(tmpName, temScore));
        }

        int index = 0;
        for (index = 0; index < ScoreData.Count; index++)
        {
            if(Score.UserName == mGameScore[index].Name)
            {
                mGameScore[index].Score = Score.GameScore.ToString();
                break;
            }
        }

        if(index == ScoreData.Count)
        {
            mGameScore.Add(new GameScore(Score.UserName, Score.GameScore.ToString()));
        }

        JsonData userDataJson = JsonMapper.ToJson(mGameScore);
        File.WriteAllText(Application.streamingAssetsPath + " /GameScore.json", userDataJson.ToString());

        //#region 로컬 저장
        //string saveFilePath = "D:/RED BLACK TREE DATA";
        //DirectoryInfo directory = new DirectoryInfo(saveFilePath);
        //if (directory.Exists == false)
        //{
        //    directory.Create();
        //}
        //File.WriteAllText("D:/RED BLACK TREE DATA/GameScore.json", userDataJson.ToString());
        //#endregion
    }

    public static void Load()
    {
        mCurrentGameScore.Clear();
        Score.HighScoreList.Clear();
        Score.HighScoreNameList.Clear();
        string jsonString = File.ReadAllText(Application.streamingAssetsPath + " /GameScore.json");
        JsonData ScoreData = JsonMapper.ToObject(jsonString);

        if(ScoreData.Count != 0)
        {
            for (int i = 0; i < ScoreData.Count; i++)
            {
                string temScore = ScoreData[i]["Score"].ToString();
                string tmpName = ScoreData[i]["Name"].ToString();
                mCurrentGameScore.Add(new GameScore(tmpName, temScore));
            }

            List<int> scoreList = new List<int>();

            for (int i = 0; i < mCurrentGameScore.Count; i++)
            {
                scoreList.Add(int.Parse(mCurrentGameScore[i].Score));
            }

            scoreList.Sort();



#region 1~5등

            //1등
            for (int i = 0; i < mCurrentGameScore.Count; i++)
            {
                if (scoreList[scoreList.Count - 1] == int.Parse(mCurrentGameScore[i].Score))
                {
                    Score.HighScoreNameList.Add(mCurrentGameScore[i].Name);
                    Score.HighScoreList.Add(scoreList[scoreList.Count - 1]);
                    break;
                }
            }

            if(ScoreData.Count >=2)
            {
                //2등
                for (int i = 0; i < mCurrentGameScore.Count; i++)
                {
                    if (scoreList[scoreList.Count - 2] == int.Parse(mCurrentGameScore[i].Score))
                    {
                        Score.HighScoreNameList.Add(mCurrentGameScore[i].Name);
                        Score.HighScoreList.Add(scoreList[scoreList.Count - 2]);
                        break;
                    }
                }
            }

            if(ScoreData.Count >= 3)
            {
                //3등
                for (int i = 0; i < mCurrentGameScore.Count; i++)
                {
                    if (scoreList[scoreList.Count - 3] == int.Parse(mCurrentGameScore[i].Score))
                    {
                        Score.HighScoreNameList.Add(mCurrentGameScore[i].Name);
                        Score.HighScoreList.Add(scoreList[scoreList.Count - 3]);
                        break;
                    }
                }
            }

            if(ScoreData.Count >= 4)
            {
                //4등
                for (int i = 0; i < mCurrentGameScore.Count; i++)
                {
                    if (scoreList[scoreList.Count - 4] == int.Parse(mCurrentGameScore[i].Score))
                    {
                        Score.HighScoreNameList.Add(mCurrentGameScore[i].Name);
                        Score.HighScoreList.Add(scoreList[scoreList.Count - 4]);
                        break;
                    }
                }
            }

            if(ScoreData.Count >= 5)
            {
                //5등
                for (int i = 0; i < mCurrentGameScore.Count; i++)
                {
                    if (scoreList[scoreList.Count - 5] == int.Parse(mCurrentGameScore[i].Score))
                    {
                        Score.HighScoreNameList.Add(mCurrentGameScore[i].Name);
                        Score.HighScoreList.Add(scoreList[scoreList.Count - 5]);
                        break;
                    }
                }
            }
            #endregion
        }
    }

}

public class GameScore
{
    public string Score;
    public string Name;

    public GameScore(string name, string score)
    {
        Score = score;
        Name = name;
    }
}

