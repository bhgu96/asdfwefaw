﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class InputFieldEvent_InputName : MonoBehaviour
{
    [SerializeField]
    private Text mInputNameText;
    [SerializeField]
    private UIController mUIController;
    [SerializeField]
    private CameraController mCamera;
    [SerializeField]
    private AudioSource mAudio;
    [SerializeField]
    private string mNextSceneName;


    private void Update()
    {
        if(Input.GetKeyDown(KeyCode.Return))
        {
            Score.UserName = null;
            Score.UserName = mInputNameText.text;
            mUIController.DisableTitleController();
            mCamera.BlackOut(1.0f, 1.0f);
            StartCoroutine(waitForFade());
        }
    }

    private IEnumerator waitForFade()
    {
        while (mAudio.volume > 0.0f)
        {
            mAudio.volume -= Time.deltaTime;
            yield return null;
        }

        yield return new WaitUntil(() => !mCamera.IsBlackOutTriggered);
        LoadingController.LoadScene(mNextSceneName);
    }
}
