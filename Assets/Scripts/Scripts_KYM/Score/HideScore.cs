﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HideScore : MonoBehaviour
{
    [SerializeField]
    private List<GameObject> mHighScoreList;

    private void OnEnable()
    {
        for (int i = 0; i < mHighScoreList.Count; i++)
        {
            mHighScoreList[i].SetActive(false);
        }
    }

    private void OnDisable()
    {
        for (int i = 0; i < mHighScoreList.Count; i++)
        {
            mHighScoreList[i].SetActive(true);
        }
    }
}
