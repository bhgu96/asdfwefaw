﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HighScoreText : MonoBehaviour
{
    [SerializeField]
    private int mIndex;
    private Text mText;

    private void Awake()
    {
        mText = this.GetComponent<Text>();
        ScoreData.Load();

        if(mIndex == 0 && Score.HighScoreNameList.Count > 0)
        {
            mText.text = (mIndex + 1).ToString() + "등" + "\n " + "이름 : " + Score.HighScoreNameList[mIndex] + "\n" + "점수 : " + Score.HighScoreList[mIndex].ToString() + "점";
        }
        else if(mIndex == 1 && Score.HighScoreNameList.Count > 1)
        {
            mText.text = (mIndex + 1).ToString() + "등" + "\n " + "이름 : " + Score.HighScoreNameList[mIndex] + "\n" + "점수 : " + Score.HighScoreList[mIndex].ToString() + "점";
        }
        else if (mIndex == 2 && Score.HighScoreNameList.Count > 2)
        {
            mText.text = (mIndex + 1).ToString() + "등" + "\n " + "이름 : " + Score.HighScoreNameList[mIndex] + "\n" + "점수 : " + Score.HighScoreList[mIndex].ToString() + "점";
        }
        else if (mIndex == 3 && Score.HighScoreNameList.Count > 3)
        {
            mText.text = (mIndex + 1).ToString() + "등" + "\n " + "이름 : " + Score.HighScoreNameList[mIndex] + "\n" + "점수 : " + Score.HighScoreList[mIndex].ToString() + "점";
        }
        else if (mIndex == 4 && Score.HighScoreNameList.Count > 4)
        {
            mText.text = (mIndex + 1).ToString() + "등" + "\n " + "이름 : " + Score.HighScoreNameList[mIndex] + "\n" + "점수 : " + Score.HighScoreList[mIndex].ToString() + "점";
        }
        else
        {
            mText.text = (mIndex + 1).ToString() + "등" + "\n " + "이름 : " + "\n" + "점수 : " + 0.ToString() + "점";
        }
    }
}
