﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScoreBoardPredator : MonoBehaviour
{
    private Animator mAnimator;

    private void Awake()
    {
        mAnimator = this.GetComponent<Animator>();
    }

    private void CheckChargeMotion()
    {
        int random = Random.Range(0, 10);

        if(random >= 7)
        {
            mAnimator.SetBool("isCharge", true);
        }
        else
        {
            mAnimator.SetBool("isCharge", false);
        }
    }

    private void EndChargeMotion()
    {
        mAnimator.SetBool("isCharge", false);
    }
}
