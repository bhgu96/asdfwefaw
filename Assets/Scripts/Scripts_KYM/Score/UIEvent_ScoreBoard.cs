﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIEvent_ScoreBoard : MonoBehaviour
{
    [SerializeField]
    private UIController mUIController;
    [SerializeField]
    private GameObject mKillTextObject;
    [SerializeField]
    private GameObject mGrassTextObject;
    [SerializeField]
    private GameObject mGoldTextObject;
    [SerializeField]
    private GameObject mEquipmentTextObject;
    [SerializeField]
    private GameObject mExhaustionTextObject;
    [SerializeField]
    private GameObject mScoreTextObject;
    [SerializeField]
    private GameObject mLogTextObject;
    [SerializeField]
    private Text mNameText;
    [SerializeField]
    private string mReloadScene;

    private Text mKillCountText;
    private Text mGrassCountText;
    private Text mGoldCountText;
    private Text mEquipmentCountText;
    private Text mExhaustionCountText;

    private bool mIsEndCounting;

    private void Awake()
    {
        mKillCountText = mKillTextObject.transform.GetChild(0).GetComponent<Text>();
        mGrassCountText = mGrassTextObject.transform.GetChild(0).GetComponent<Text>();
        mGoldCountText = mGoldTextObject.transform.GetChild(0).GetComponent<Text>();
        mEquipmentCountText = mEquipmentTextObject.transform.GetChild(0).GetComponent<Text>();
        mExhaustionCountText = mExhaustionTextObject.transform.GetChild(0).GetComponent<Text>();
    }

    private void OnEnable()
    {
        mUIController.DisableTitleController();
        StartCoroutine(ScoreEvent());
    }

    private IEnumerator ScoreEvent()
    {
        yield return new WaitForSeconds(1f);

        mKillTextObject.SetActive(true);
        yield return new WaitForSeconds(0.5f);
        StartCoroutine(CountEvent(EScoreCountType.KILL));
        yield return new WaitUntil(() => !mIsEndCounting);

        yield return new WaitForSeconds(0.5f);

        mGrassTextObject.SetActive(true);
        yield return new WaitForSeconds(0.5f);
        StartCoroutine(CountEvent(EScoreCountType.GRASS));
        yield return new WaitUntil(() => !mIsEndCounting);

        yield return new WaitForSeconds(0.5f);

        mGoldTextObject.SetActive(true);
        yield return new WaitForSeconds(0.5f);
        StartCoroutine(CountEvent(EScoreCountType.GOLD));
        yield return new WaitUntil(() => !mIsEndCounting);

        yield return new WaitForSeconds(0.5f);

        mEquipmentTextObject.SetActive(true);
        yield return new WaitForSeconds(0.5f);
        StartCoroutine(CountEvent(EScoreCountType.EQUIPMENT));
        yield return new WaitUntil(() => !mIsEndCounting);

        yield return new WaitForSeconds(0.5f);

        mExhaustionTextObject.SetActive(true);
        yield return new WaitForSeconds(0.5f);
        StartCoroutine(CountEvent(EScoreCountType.EXHAUSTION));
        yield return new WaitUntil(() => !mIsEndCounting);

        yield return new WaitForSeconds(0.7f);

        mScoreTextObject.SetActive(true);
        mScoreTextObject.GetComponent<Text>().text = Score.GameScore.ToString();
        StartCoroutine(BlinkLogText());
    }

    private IEnumerator CountEvent(EScoreCountType scoreCountType)
    {
        mIsEndCounting = true;

        switch (scoreCountType)
        {
            case EScoreCountType.KILL:
                mKillCountText.gameObject.SetActive(true);
                for(int i = 0; i <= Score.KillCount; i++)
                {
                    if(Input.anyKeyDown)
                    {
                        mKillCountText.text = Score.KillCount.ToString();
                        break;
                    }
                    mKillCountText.text = i.ToString();
                    yield return null;
                }
                mIsEndCounting = false;
                yield break;

            case EScoreCountType.GRASS:
                mGrassCountText.gameObject.SetActive(true);
                for (int i = 0; i <= Score.GrassCount; i++)
                {
                    if (Input.anyKeyDown)
                    {
                        mGrassCountText.text = Score.GrassCount.ToString();
                        break;
                    }
                    mGrassCountText.text = i.ToString();
                    yield return null;
                }
                mIsEndCounting = false;
                yield break;

            case EScoreCountType.GOLD:
                mGoldCountText.gameObject.SetActive(true);
                for (int i = 0; i <= Score.GoldCount; i++)
                {
                    if (Input.anyKeyDown)
                    {
                        mGoldCountText.text = Score.GoldCount.ToString();
                        break;
                    }
                    mGoldCountText.text = i.ToString();
                    yield return null;
                }
                mIsEndCounting = false;
                yield break;

            case EScoreCountType.EQUIPMENT:
                mEquipmentCountText.gameObject.SetActive(true);
                for (int i = 0; i <= Score.EquipmentCount; i++)
                {
                    if (Input.anyKeyDown)
                    {
                        mEquipmentCountText.text = Score.EquipmentCount.ToString();
                        break;
                    }
                    mEquipmentCountText.text = i.ToString();
                    yield return null;
                }
                mIsEndCounting = false;
                yield break;

            case EScoreCountType.EXHAUSTION:
                mExhaustionCountText.gameObject.SetActive(true);
                for (int i = 0; i <= Score.ExhaustionCount; i++)
                {
                    if (Input.anyKeyDown)
                    {
                        mExhaustionCountText.text = Score.ExhaustionCount.ToString();
                        break;
                    }
                    mExhaustionCountText.text = i.ToString();
                    yield return null;
                }
                mIsEndCounting = false;
                yield break;
        }

    } 
    
    private IEnumerator BlinkLogText()
    {
        mLogTextObject.SetActive(true);
        Text logText = mLogTextObject.GetComponent<Text>();

        while(true)
        {
            for(float i = 0; i <= 1; i += Time.deltaTime)
            {
                if(Input.GetKeyDown(KeyCode.Return))
                {
                    //StateManager.GameClearHandler.State = EGameClearFlags.Normal;
                    LoadingController.LoadScene(mReloadScene);
                }
                else if(Input.GetKeyDown(KeyCode.Escape))
                {
                    //StateManager.GameClearHandler.State = EGameClearFlags.Normal;
                    LoadingController.LoadScene("Scene_Title");
                }

                logText.color = new Color(1, 1, 1, i);
                yield return null;
            }

            logText.color = new Color(1, 1, 1, 1);

            for (float i = 1; i >= 0; i -= Time.deltaTime)
            {
                if (Input.GetKeyDown(KeyCode.Return))
                {
                    //StateManager.GameClearHandler.State = EGameClearFlags.Normal;
                    LoadingController.LoadScene(mReloadScene);
                }
                else if (Input.GetKeyDown(KeyCode.Escape))
                {
                    //StateManager.GameClearHandler.State = EGameClearFlags.Normal;
                    LoadingController.LoadScene("Scene_Title");
                }

                logText.color = new Color(1, 1, 1, i);
                yield return null;
            }

            logText.color = new Color(1, 1, 1, 0);
        }
    }
}

public enum EScoreCountType
{
    None = -1,
    KILL,
    GRASS,
    GOLD,
    EQUIPMENT,
    EXHAUSTION,
    Count
}
