﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScoreHud : AHud
{
    private const float ANIM_TIME = 0.02f;
    private const float EMPHASIZING_TIME = 0.01f;
    private const float RELEASING_TIME = 0.05f;

    [SerializeField]
    private Text mText;

    private int mCurrentScore;
    private float mRemainTime;
    private float mEmphasizingRemainTime;
    private float mReleasingRemainTime;
    private bool mIsEmphasizing = false;

    private void Awake()
    {
        //StateManager.GamePauseHandler.AddEnterEvent(EPauseFlags.PAUSE, Deactivate);
        //StateManager.GamePauseHandler.AddEnterEvent(EPauseFlags.Play, Activate);
        //StateManager.GameOverHandler.AddEnterEvent(EGameOverFlags.GameOver, Deactivate);
        //StateManager.GameOverHandler.AddEnterEvent(EGameOverFlags.Play, Activate);
        //StateManager.GameStateHandler.AddEnterEvent(EGameStateFlags.Normal, Activate);
        //StateManager.GameStateHandler.AddExitEvent(EGameStateFlags.Normal, Deactivate);
    }

    public override void Activate()
    {
        //if(StateManager.GameStateHandler.State == EGameStateFlags.Normal)
        //{
        //    this.gameObject.SetActive(true);
        //}
    }

    public override void Deactivate()
    {
        this.gameObject.SetActive(false);
    }

    public override void Express()
    {
        mText.text = mCurrentScore.ToString();
    }

    public override void GetInformation()
    {
        if (mRemainTime > 0.0f)
        {
            mRemainTime -= Time.deltaTime;
        }
        else
        {
            int nextAmount = Score.GameScore;
            if (nextAmount > mCurrentScore)
            {
                mCurrentScore += (nextAmount - mCurrentScore) / 5 + 1;
                mRemainTime = ANIM_TIME;
                mReleasingRemainTime = RELEASING_TIME;
                if (!mIsEmphasizing)
                {
                    mIsEmphasizing = true;
                    mEmphasizingRemainTime = EMPHASIZING_TIME;
                }
                else
                {
                    if (mEmphasizingRemainTime > 0.0f)
                    {
                        mEmphasizingRemainTime -= Time.deltaTime;
                        mText.fontSize = (int)Mathf.Lerp(70, 50, mEmphasizingRemainTime / EMPHASIZING_TIME);
                    }
                    else
                    {
                        mText.fontSize = 70;
                    }
                }
            }
            else if (nextAmount < mCurrentScore)
            {
                mCurrentScore += (nextAmount - mCurrentScore) / 5 - 1;
                mRemainTime = ANIM_TIME;
                mReleasingRemainTime = RELEASING_TIME;

                if (!mIsEmphasizing)
                {
                    mIsEmphasizing = true;
                    mEmphasizingRemainTime = EMPHASIZING_TIME;
                    mReleasingRemainTime = RELEASING_TIME;
                }
                else
                {
                    if (mEmphasizingRemainTime > 0.0f)
                    {
                        mEmphasizingRemainTime -= Time.deltaTime;
                        mText.fontSize = (int)Mathf.Lerp(70, 50, mEmphasizingRemainTime / EMPHASIZING_TIME);
                    }
                    else
                    {
                        mText.fontSize = 70;
                    }
                }
            }
            if (mIsEmphasizing)
            {
                if (mReleasingRemainTime > 0.0f)
                {
                    mReleasingRemainTime -= Time.deltaTime;
                    mText.fontSize = (int)Mathf.Lerp(50, mText.fontSize, mReleasingRemainTime / RELEASING_TIME);
                }
                else
                {
                    mText.fontSize = 50;
                    mIsEmphasizing = false;
                }
            }
        }
    }

    private void OnDestroy()
    {
        //StateManager.GamePauseHandler.RemoveEnterEvent(EPauseFlags.PAUSE, Deactivate);
        //StateManager.GamePauseHandler.RemoveEnterEvent(EPauseFlags.Play, Activate);
        //StateManager.GameOverHandler.RemoveEnterEvent(EGameOverFlags.GameOver, Deactivate);
        //StateManager.GameOverHandler.RemoveEnterEvent(EGameOverFlags.Play, Activate);
        //StateManager.GameStateHandler.RemoveEnterEvent(EGameStateFlags.Normal, Activate);
        //StateManager.GameStateHandler.RemoveExitEvent(EGameStateFlags.Normal, Deactivate);
    }
}
