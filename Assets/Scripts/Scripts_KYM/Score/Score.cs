﻿using UnityEngine;
using System.Collections.Generic;

public class Score
{
    public static string UserName { get { return mUserName; } set { mUserName = value; } }
    public static int KillCount { get { return mKillCount; } private set { } }
    public static int GrassCount { get { return mGrassCount; } private set { } }
    public static int GoldCount { get { return mGoldCount; } private set { } }
    public static int EquipmentCount { get { return mEquipmentCount; } private set { } }
    public static int ExhaustionCount { get { return mExhaustionCount; } private set { } }
    public static int GameScore { get { return mGameScore; } private set { } }

    public static List<string> HighScoreNameList = new List<string>();
    public static List<int> HighScoreList = new List<int>();

    private static string mUserName;
    private static int mKillCount;
    private static int mGrassCount;
    private static int mGoldCount;
    private static int mEquipmentCount;
    private static int mExhaustionCount;
    private static int mGameScore;

    public static void InitScore()
    {
        mKillCount = 0;
        mGrassCount = 0;
        mGoldCount = 0;
        mEquipmentCount = 0;
        mExhaustionCount = 0;
        mGameScore = 0;
    }

    public static void RecordScore(EScoreType scoreType)
    {
        switch(scoreType)
        {            
            //구조물들을 박살냈을때
            case EScoreType.DISTRUCTION_STRUCTURE:
                mGameScore += 40;
                break;
            
            //잔디를 심을때
            case EScoreType.CREATE_ENVIROMENT_GRASS:
                mGameScore += 1;
                mGrassCount += 1;
                break;
            
            //덤불 스택 최초 생성
            case EScoreType.CREATE_FIRST_GRASS_STACK:
                mGameScore += 10;
                break;

            //덤불 스택 추가 생성
            case EScoreType.CREATE_GRASS_ADD_STACK:
                mGameScore += 5;
                break;

            //화염 스택이 쌓일때
            case EScoreType.CREATE_FIRE_STACK:
                mGameScore += 10;
                break;
            
            //몬스터 한마리씩 죽일때
            case EScoreType.KILL_ENEMY:
                mGameScore += 20;
                mKillCount += 1;
                break;
            
            ////불꽃을 획득했을 때
            //case EScoreType.GET_FIRE:
            //    mGameScore += 12;
            //    break;

            ////괴수가 탈진이 되고 떠다니는 불씨(Ember)를 획득할때
            //case EScoreType.GET_EMBER:
            //    mGameScore += 1;
            //    break;

            //마나 구슬을 획득할 때
            case EScoreType.GET_MANA_BEAD:
                mGameScore += 30;
                break;

            //생명력 구슬 획득했을 때
            case EScoreType.GET_HEALTH_POWER_BEAD:
                mGameScore += 10;
                break;
            
            //소모 아이템을 얻을때
            case EScoreType.GET_CONSUME_ITEM:
                mGameScore += 50;
                break;

            ////소모 아이템을 사용할 때
            //case EScoreType.USE_CONSUME_ITEM:
            //    mGameScore += 5;
            //    break;

            //무기를 얻을때
            case EScoreType.GET_WEAPON_ITEM:
                mGameScore += 50;
                mEquipmentCount += 1;
                break;

            ////무기를 사용할때
            //case EScoreType.USE_WEAPON_ITEM:
            //    mGameScore += 10;
            //    break;

            //주문서를 얻을 떄
            case EScoreType.GET_SCROLL:
                mGameScore += 100;
                mEquipmentCount += 1;
                break;

            ////주문서를 사용할 때
            //case EScoreType.USE_SCROLL:
            //    mGameScore += 15;
            //    break;
            
            //괴수가 탈진했을 때
            case EScoreType.EXHAUSTION_PREDETOR:
                mGameScore -= 300;
                mGameScore = Mathf.Clamp(mGameScore, 0, mGameScore);
                mExhaustionCount += 1;
                break;
            
            ////괴수가 부활했을 때
            //case EScoreType.REVIVE_PREDETOR:
            //    mGameScore += 50;
            //    break;
        }
    }

    public static void RecordScore(EScoreType scoreType, int goldAmount)
    {
        switch (scoreType)
        {
            //골드를 얻을때
            case EScoreType.GET_GOLD:
                mGameScore += goldAmount / 10;
                mGoldCount += goldAmount;
                break;
        }
    }
}

public enum EScoreType
{
    None = -1,
    DISTRUCTION_STRUCTURE,
    CREATE_ENVIROMENT_GRASS,
    CREATE_FIRST_GRASS_STACK,
    CREATE_GRASS_ADD_STACK,
    CREATE_FIRE_STACK,
    KILL_ENEMY,
    GET_GOLD,
    GET_FIRE,
    GET_EMBER,
    GET_MANA_BEAD,
    GET_HEALTH_POWER_BEAD,
    GET_CONSUME_ITEM,
    USE_CONSUME_ITEM,
    GET_WEAPON_ITEM,
    USE_WEAPON_ITEM,
    GET_SCROLL,
    USE_SCROLL,
    EXHAUSTION_PREDETOR,
    REVIVE_PREDETOR,
    Count
}
