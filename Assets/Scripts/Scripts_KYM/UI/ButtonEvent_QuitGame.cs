﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ButtonEvent_QuitGame : MonoBehaviour
{
    [SerializeField]
    private AudioSource mConfirmAudio;

    private UIButton mButton;

    private void Awake()
    {
        mButton = GetComponent<UIButton>();
        mButton.onConfirm += confirmEvent;
    }

    private void confirmEvent()
    {
        if (mConfirmAudio != null)
        {
            mConfirmAudio.Play();
        }

        Application.Quit();
    }
}
