﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class Event_EnableTitleMenu : MonoBehaviour
{
    private const int Up_TITLE_POSITION_Y = 280;
    private const float DARK_TRANSPARENCY = 0.7f;

    [SerializeField]
    private Image mTitleImage;
    [SerializeField]
    private Image mTransparencyImage;
    [SerializeField]
    private UIPanel mDisableTitlePanel;

    private UIPanel mPanel;
    private bool mIsEnable;

    private void Awake()
    {
        mPanel = this.GetComponent<UIPanel>();

        mPanel.onEnable += enableEvent;
    }

    private void enableEvent()
    {
        mIsEnable = true;
        StartCoroutine(TitleMoveUp());
        StartCoroutine(DarkTransparency());
    }

    private IEnumerator TitleMoveUp()
    {
        RectTransform rectTransform = mTitleImage.rectTransform;

        if(rectTransform.anchoredPosition.y == Up_TITLE_POSITION_Y)
        {
            yield break;
        }

        for(float y = rectTransform.anchoredPosition.y; y <= Up_TITLE_POSITION_Y; y += Time.deltaTime * 700f)
        {
            if(!mIsEnable)
            {
                yield break;
            }
            rectTransform.anchoredPosition = new Vector3(rectTransform.anchoredPosition.x, y);
            yield return null;
        }

        rectTransform.anchoredPosition = new Vector3(rectTransform.anchoredPosition.x, Up_TITLE_POSITION_Y);
    }

    private IEnumerator DarkTransparency()
    {
        float curAlpha = mTitleImage.color.a / 255f;

        for(float a = curAlpha; a <= DARK_TRANSPARENCY; a += Time.deltaTime * 2f)
        {
            if (!mIsEnable)
            {
                yield break;
            }
            mTransparencyImage.color = new Vector4(0, 0, 0, a);
            yield return null;
        }

        mTransparencyImage.color = new Vector4(0, 0, 0, DARK_TRANSPARENCY);
    }
}
