﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SubHealthPointHud : AHud
{
    [SerializeField]
    /// 인스펙터창에서 포식귀 게임 오브젝트(작업했을때는 Prefab_Guardian으로 되어있었음)를 드래그 앤 드랍 시키면 됩니다.
    private GameObject mActorObject;

    private RectTransform mRectTransform;
    private IActor mActor;
    //private CurseSystem mCurseSystem;
    private Animator mAnimator;

    private void Awake()
    {
        if (mActorObject.GetComponent<IActor>() != null)
        {
            mActor = mActorObject.GetComponent<IActor>();
        }

        mAnimator = this.GetComponent<Animator>();
        mRectTransform = this.GetComponent<RectTransform>();
    }

    private void Start()
    {
        //mCurseSystem = mActor.CurseSystem;
    }

    public override void Activate()
    {

    }

    public override void Deactivate()
    {

    }

    public override void Express()
    {
        //if(mCurseSystem.IsTerm && mCurseSystem.TermTime >= 0.4)
        //{
        //    mAnimator.SetBool("isBroken", true);
        //}
    }

    public override void GetInformation()
    {

    }

    private void Damaged_Animation()
    {
        //mCurseSystem.Damaged();
    }

    private void Recovered_Animation()
    {
        mAnimator.SetBool("isBroken", false);
    }

    private void ResetSize_Animation()
    {
        mRectTransform.sizeDelta = new Vector2(280f, 60f);
    }

    private void IncreaseSize_Animation()
    {
        mRectTransform.sizeDelta = new Vector2(800f, 600f);
    }
}
