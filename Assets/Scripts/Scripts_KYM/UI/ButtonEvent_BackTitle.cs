﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ButtonEvent_BackTitle : MonoBehaviour
{
    [SerializeField]
    private UIPanel mPanel;

    private UIButton mButton;

    private void Awake()
    {
        mButton = GetComponent<UIButton>();
        mButton.onConfirm += BackTitleEvent;
    }

    private void BackTitleEvent()
    {
        mButton.UIController.OpenPanel(mPanel);
        //StateManager.Pause.State = false;
        //StateManager.GameOver.State = false;
        //Time.timeScale = 1.0f;
        //ScoreData.Save();
        //LoadingController.LoadScene("Scene_NewTitle");
    }
}
