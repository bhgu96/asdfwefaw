﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ItemSlotHud : AHud
{
    [SerializeField]
    private Sprite mNullImage;
    //[SerializeField]
    //private EItemTypes mSlotType;
    [SerializeField]
    private int mScrollSlotIndex;

    //private Fairy_V2 mFairy;
    private Image mImage;

    private void Awake()
    {
        mImage = this.GetComponent<Image>();
    }

    private void Start()
    {
        //if (Fairy_V2.Instance != null)
        //{
        //    mFairy = Fairy_V2.Instance;
        //}
    }

    public override void Activate()
    {

    }

    public override void Deactivate()
    {

    }

    public override void Express()
    {
        //if (mSlotType == EItemTypes.CONSUMABLE)
        //{
        //    if (mFairy.Inventory.Peek(EItemTypes.CONSUMABLE, 0) is Consumable consumable)
        //    {
        //        mImage.sprite = consumable.Icon;
        //    }
        //    else
        //    {
        //        mImage.sprite = mNullImage;
        //    }
        //}
        //else if (mSlotType == EItemTypes.WEAPON)
        //{
        //    if(mFairy.Inventory.Peek(EItemTypes.WEAPON, 0) is Weapon weapon)
        //    {
        //        mImage.sprite = weapon.Icon;
        //    }
        //    else
        //    {
        //        mImage.sprite = mNullImage;
        //    }
        //}
        //else if (mSlotType == EItemTypes.SCROLL)
        //{
        //    if (mFairy.Inventory.Peek(EItemTypes.SCROLL, mScrollSlotIndex) is Scroll scroll)
        //    {
        //        mImage.sprite = scroll.Icon;
        //    }
        //    else
        //    {
        //        mImage.sprite = mNullImage;
        //    }
        //}
        //else
        //{
        //    mImage.sprite = mNullImage;
        //}
    }

    public override void GetInformation()
    {

    }
}
