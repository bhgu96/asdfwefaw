﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameOverHud : AHud
{
    private const float FADING_TIME = 1.5f;

    [SerializeField]
    private GameObject mActorObject;
    [SerializeField]
    private Image mBackground;
    [SerializeField]
    private RectTransform mBlinkTransform;
    [SerializeField]
    private Image mGameOverLogoImage;
    [SerializeField]
    private Text mPressAnykeyText;
    [SerializeField]
    private UIPanel mGameOverSettingsPanel;
    [SerializeField]
    private UIController mTitleController;

    private IActor mActor;
    private float mCurHP;

    public static bool mIsGameOver;

    private void Awake()
    {
        if(mActorObject.GetComponent<IActor>() != null)
        {
            mActor = mActorObject.GetComponent<IActor>();
        }
    }

    public override void Activate()
    {

    }

    public override void Deactivate()
    {

    }

    public override void Express()
    {
        ///게임오버를 구현하시면 밑에 조건문은 지우시고 해당 조건문의 주석을 푸시면 됩니다.
        if (StateManager.GameOver.State && !mIsGameOver)
        {
            mIsGameOver = true;
            StartCoroutine(GameOver());
        }
    }

    public override void GetInformation()
    {
        //mCurHP = mActor.Status.CurrentHealthPoint;
    }

    private IEnumerator GameOver()
    {
        mBlinkTransform.gameObject.SetActive(true);
        mBackground.gameObject.SetActive(true);

        yield return new WaitForSeconds(0.1f);

        mBlinkTransform.sizeDelta = new Vector2(mBlinkTransform.sizeDelta.x, 50f);

        yield return new WaitForSeconds(0.1f);

        mBlinkTransform.sizeDelta = new Vector2(mBlinkTransform.sizeDelta.x, 300f);

        yield return new WaitForSeconds(0.1f);

        mBlinkTransform.gameObject.SetActive(false);
        mGameOverLogoImage.gameObject.SetActive(true);

        yield return new WaitForSeconds(1.0f);

        mPressAnykeyText.gameObject.SetActive(true);

        yield return new WaitUntil(() => Input.anyKeyDown);

        mGameOverLogoImage.gameObject.SetActive(false);
        mPressAnykeyText.gameObject.SetActive(false);
        mBackground.gameObject.SetActive(false);

        //mTitleController.EnableTitleController();
        mTitleController.OpenPanel(mGameOverSettingsPanel);
    }
}
