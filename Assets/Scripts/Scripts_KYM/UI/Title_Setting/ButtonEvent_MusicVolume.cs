﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Audio;

public class ButtonEvent_MusicVolume : MonoBehaviour
{
    private static float mVolumeCount = 100f;

    [SerializeField]
    private AudioMixer mAudioMixer;
    [SerializeField]
    private AudioSource mAudio;
    [SerializeField]
    private AudioSource mConfirmAudio;
    [SerializeField]
    private Text mText;
    [SerializeField]
    private Image mVolumeImage;

    private UIButton mButton;
    private ButtonEvent_SelectableButtons mSelectButton;
    private float mUpVolumePressTime;
    private float mDownVolumePressTime;

    private void Awake()
    {
        mButton = GetComponent<UIButton>();
        mSelectButton = this.GetComponent<ButtonEvent_SelectableButtons>();
        mButton.onConfirm += CheckConfirm;
    }

    private void Update()
    {
        if(mSelectButton.IsSelect)
        {
            if (Input.GetKeyDown(KeyCode.A) || Input.GetKeyDown(KeyCode.LeftArrow))
            {
                if (mConfirmAudio != null)
                {
                    mConfirmAudio.Play();
                }

                mVolumeCount -= 5f;
            }
            else if (Input.GetKeyDown(KeyCode.D) || Input.GetKeyDown(KeyCode.RightArrow))
            {
                if (mConfirmAudio != null)
                {
                    mConfirmAudio.Play();
                }

                mVolumeCount += 5f;
            }


            if (Input.GetKey(KeyCode.A) || Input.GetKey(KeyCode.LeftArrow))
            {
                if (mDownVolumePressTime > 0.5f)
                {
                    mVolumeCount -= 5f;
                }
                else
                {
                    mDownVolumePressTime += Time.deltaTime;
                }
            }
            else if (Input.GetKey(KeyCode.D) || Input.GetKey(KeyCode.RightArrow))
            {
                if (mUpVolumePressTime > 0.5f)
                {
                    mVolumeCount += 5f;
                }
                else
                {
                    mUpVolumePressTime += Time.deltaTime;
                }
            }
            else
            {
                mUpVolumePressTime = 0.0f;
                mDownVolumePressTime = 0.0f;
            }

            mVolumeCount = Mathf.Clamp(mVolumeCount, 0f, 100f);
            mVolumeImage.fillAmount = mVolumeCount / 100f;
            mAudioMixer.SetFloat("Music", (((float)mVolumeCount / 100f) - 1) * 20f);
            mText.text = "                          -                       + " + "[" + mVolumeCount.ToString() + "]";

            if(mVolumeCount == 0)
            {
                mAudioMixer.SetFloat("Music", -40f);
            }
        }
    }

    private void CheckConfirm()
    {

    }
}
