﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ButtonEvent_WindowMode : MonoBehaviour
{
    [SerializeField]
    private AudioSource mAudio;
    [SerializeField]
    private AudioSource mConfirmAudio;
    [SerializeField]
    private Text mText;

    private UIButton mButton;
    private bool mIsWindowMode = false;
    private static bool mIsCheckWindowMode = false;

    private void Awake()
    {
        mButton = GetComponent<UIButton>();
        mButton.onConfirm += ChangeWindowMode;
        CheckText();
    }

    private void ChangeWindowMode()
    {
        //if(mAudio != null)
        //{
        //    mAudio.Play();
        //}
        if (mConfirmAudio != null)
        {
            mConfirmAudio.Play();
        }

        if (!mIsWindowMode)
        {
            mIsWindowMode = true;
            mIsCheckWindowMode = true;
            Screen.SetResolution(1360, 768, false);
            //CustomCursor.SetMousePosition(1360f, 768f);
            mText.text = "On";
        }
        else
        {
            mIsWindowMode = false;
            mIsCheckWindowMode = false;
            Screen.SetResolution(1920, 1080, true);
            //CustomCursor.SetMousePosition(1920f, 1080f);
            mText.text = "Off";
        }
    }

    private void CheckText()
    {
        if (mIsCheckWindowMode)
        {
            mText.text = "On";
            mIsWindowMode = true;
        }
        else
        {
            mText.text = "Off";
            mIsWindowMode = false;
        }
    }
}
