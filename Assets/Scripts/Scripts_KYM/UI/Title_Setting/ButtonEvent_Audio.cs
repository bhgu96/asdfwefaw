﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ButtonEvent_Audio : MonoBehaviour
{
    [SerializeField]
    private UIPanel mAudioPanel;
    [SerializeField]
    private AudioSource mConfirmAudio;

    private UIPanel mPanel;
    private UIButton mButton;

    private void Awake()
    {
        mButton = this.GetComponent<UIButton>();
        mPanel = this.GetComponentInParent<UIPanel>();
        mButton.onConfirm += StartSetting;
    }

    private void StartSetting()
    {
        if (mConfirmAudio != null)
        {
            mConfirmAudio.Play();
        }

        mPanel.UIController.OpenPanel(mAudioPanel);
    }
}
