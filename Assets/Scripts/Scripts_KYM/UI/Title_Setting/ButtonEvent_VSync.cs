﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ButtonEvent_VSync : MonoBehaviour
{
    [SerializeField]
    private AudioSource mAudio;
    [SerializeField]
    private AudioSource mConfirmAudio;
    [SerializeField]
    private Text mText;

    private UIButton mButton;
    private bool mIsVSync = false;

    private void Awake()
    {
        mButton = GetComponent<UIButton>();
        mButton.onConfirm += ChangeVSync;
    }

    private void ChangeVSync()
    {
        //if(mAudio != null)
        //{
        //    mAudio.Play();
        //}
        if (mConfirmAudio != null)
        {
            mConfirmAudio.Play();
        }

        if (!mIsVSync)
        {
            mIsVSync = true;
            QualitySettings.vSyncCount = 1;
            mText.text = "On";
        }
        else
        {
            mIsVSync = false;
            QualitySettings.vSyncCount = 0;
            mText.text = "Off";
        }
    }
}
