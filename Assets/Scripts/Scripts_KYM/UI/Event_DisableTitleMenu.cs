﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class Event_DisableTitleMenu : MonoBehaviour
{
    private const int INIT_TITLE_POSITION_Y = 0;

    [SerializeField]
    private Image mTitleImage;
    [SerializeField]
    private Text mAnyKeyText;
    [SerializeField]
    private UIPanel mEnableTitlePanel;
    [SerializeField]
    private Image mTransparencyImage;


    private UIPanel mPanel;

    [SerializeField]
    private bool mIsEnable;

    private void Awake()
    {
        mPanel = this.GetComponent<UIPanel>();
        mPanel.onEnable += enableEvent;
    }

    private void Update()
    {
        if(Input.anyKeyDown)
        {
            mIsEnable = false;
        }
    }

    private void enableEvent()
    {
        mIsEnable = true;
        mAnyKeyText.enabled = true;
        mPanel.UIController.DisableTitleController();
        StartCoroutine(TitleMoveDown());
        StartCoroutine(BlinkText());
        StartCoroutine(BrightTransparency());
    }

    private IEnumerator TitleMoveDown()
    {
        RectTransform rectTransform = mTitleImage.rectTransform;

        if (rectTransform.anchoredPosition.y == INIT_TITLE_POSITION_Y)
        {
            yield break;
        }

        for (float y = rectTransform.anchoredPosition.y; y >= INIT_TITLE_POSITION_Y; y -= Time.deltaTime * 700f)
        {
            rectTransform.anchoredPosition = new Vector3(rectTransform.anchoredPosition.x, y);
            yield return null;
        }

        rectTransform.anchoredPosition = new Vector3(rectTransform.anchoredPosition.x, INIT_TITLE_POSITION_Y);
    }

    private IEnumerator BrightTransparency()
    {
        float curAlpha = mTransparencyImage.color.a;
        
        if(curAlpha == 0)
        {
            yield break;
        }

        for (float a = curAlpha; a >= 0; a -= Time.deltaTime * 2f)
        {
            if (!mIsEnable)
            {
                yield break;
            }
            mTransparencyImage.color = new Vector4(0, 0, 0, a);
            yield return null;
        }

        mTransparencyImage.color = new Vector4(0, 0, 0, 0);
    }

    private IEnumerator BlinkText()
    {
        while(mIsEnable)
        {
            for(float a = 1; a >= 0; a -= Time.deltaTime)
            {
                if (!mIsEnable)
                {
                    break;
                }
                mAnyKeyText.color = new Vector4(1, 1, 1, a);
                yield return null;
            }

            for (float a = 0; a <= 1; a += Time.deltaTime)
            {
                if (!mIsEnable)
                {
                    break;
                }
                mAnyKeyText.color = new Vector4(1, 1, 1, a);
                yield return null;
            }
        }

        mAnyKeyText.color = new Vector4(1, 1, 1, 1);
        mAnyKeyText.enabled = false;

        mIsEnable = true;
        mPanel.UIController.EnableTitleController();
        mPanel.UIController.CurButton = null; //처음 시작하는 패널로, 현재 버튼을 초기화함

        yield return null; //바로 키 입력되는 것을 방지(다음 프레임때까지 대기)

        mPanel.UIController.ClosePanel();
        mPanel.UIController.OpenPanel(mEnableTitlePanel);
    }
}
