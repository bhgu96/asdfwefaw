﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class VersionText : MonoBehaviour
{
    private Text mText;

    private void Awake()
    {
        mText = this.GetComponent<Text>();

        mText.text = Application.version;
    }
}
