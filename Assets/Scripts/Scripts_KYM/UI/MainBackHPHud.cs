﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MainBackHPHud : AHud
{
    private const float DECREASE_COEFFICIENT = 700f;

    [SerializeField]
    /// 인스펙터창에서 포식귀 게임 오브젝트(작업했을때는 Prefab_Guardian으로 되어있었음)를 드래그 앤 드랍 시키면 됩니다.
    private GameObject mActorObject;
    [SerializeField]
    /// Main_BackHP_Image 오브젝트를 드래그 앤 드랍 시키면 됩니다.
    private Image mBackHPImage;
    private IActor mActor;
    private float mCurHP;
    private float mMaxHP;

    private float mMaxWidth;
    private float mCurWidth;

    private void Awake()
    {
        if(mActorObject.GetComponent<IActor>() != null)
        {
            mActor = mActorObject.GetComponent<IActor>();
        }

        mMaxWidth = mBackHPImage.rectTransform.sizeDelta.x;
        mCurWidth = mMaxWidth;
    }

    private void Start()
    {
        //mMaxHP = mActor.Status.MaximumHealthPoint;
        //mCurHP = mActor.Status.CurrentHealthPoint;

        //_StateHandler stateHandler = CGameManager.Instance._stateManager.GetStateHandler(EStateHandler.HUD_ACTIVE_STATE);

        //stateHandler.AddHudEnterdEvent(_EHudState.Hud_Activate_All, Activate);
        //stateHandler.AddHudEnterdEvent(_EHudState.Hud_Deactivate_All, Deactivate);
    }

    public override void Activate()
    {
        //this.gameObject.SetActive(true);
    }

    public override void Deactivate()
    {
        //this.gameObject.SetActive(false);
    }

    public override void Express()
    {
        float resultWitdh = mMaxWidth * mCurHP / mMaxHP;

        mCurWidth = mBackHPImage.rectTransform.sizeDelta.x;
        mCurWidth = Mathf.Clamp(mCurWidth, 0, mMaxWidth);

        if (mBackHPImage.rectTransform.sizeDelta.x > resultWitdh)
        {
            mCurWidth -= Time.deltaTime * DECREASE_COEFFICIENT;

            float curAlpha = mBackHPImage.color.a;
            mBackHPImage.color = new Color(1, 1, 1, curAlpha -= Time.deltaTime);

            mBackHPImage.rectTransform.sizeDelta = new Vector3(mCurWidth, mBackHPImage.rectTransform.sizeDelta.y);
        }
        else if (mBackHPImage.rectTransform.sizeDelta.x <= resultWitdh)
        {
            mBackHPImage.rectTransform.sizeDelta = new Vector3(resultWitdh, mBackHPImage.rectTransform.sizeDelta.y);
            mBackHPImage.color = new Color(1, 1, 1, 1);
        }
    }

    public override void GetInformation()
    {
        //mMaxHP = mActor.Status.MaximumHealthPoint;
        //mCurHP = mActor.Status.CurrentHealthPoint;
    }
}
