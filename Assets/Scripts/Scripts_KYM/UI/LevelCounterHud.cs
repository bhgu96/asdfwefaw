﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelCounterHud : AHud
{
    private void Start()
    {
        //_StateHandler stateHandler = CGameManager.Instance._stateManager.GetStateHandler(EStateHandler.HUD_ACTIVE_STATE);

        //stateHandler.AddHudEnterdEvent(_EHudState.Hud_Activate_All, Activate);
        //stateHandler.AddHudEnterdEvent(_EHudState.Hud_Deactivate_All, Deactivate);
    }

    public override void Activate()
    {
        this.gameObject.SetActive(true);
    }

    public override void Deactivate()
    {
        this.gameObject.SetActive(false);
    }

    public override void Express()
    {

    }

    public override void GetInformation()
    {

    }
}
