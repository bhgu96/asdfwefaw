﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameLevelTextHud : MonoBehaviour
{
    [SerializeField]
    //현재 씬의 게임 레벨을 인스펙터 창에서 입력하세요
    private string mLevelName;

    private Text mLevelText;

    private void Awake()
    {
        mLevelText = this.GetComponentInChildren<Text>();
        mLevelText.color = new Color(1, 1, 1, 0);
        mLevelText.text = mLevelName;
    }

    private void Start()
    {
        StartCoroutine(Fade());
    }

    private IEnumerator Fade()
    {
        for(float t = 0; t <= 1; t += Time.deltaTime)
        {
            mLevelText.color = new Color(1, 1, 1, t);
            yield return null;
        }

        mLevelText.color = new Color(1, 1, 1, 1);

        yield return new WaitForSeconds(1.0f);

        for (float t = 1; t >= 0; t -= Time.deltaTime)
        {
            mLevelText.color = new Color(1, 1, 1, t);
            yield return null;
        }

        mLevelText.color = new Color(1, 1, 1, 0);

        this.gameObject.SetActive(false);
    }

}
