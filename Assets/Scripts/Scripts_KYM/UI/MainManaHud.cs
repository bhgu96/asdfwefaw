﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MainManaHud : AHud
{
    [SerializeField]
    /// 인스펙터창에서 포식귀 게임 오브젝트(작업했을때는 Prefab_Guardian으로 되어있었음)를 드래그 앤 드랍 시키면 됩니다.
    private GameObject mActorObject;
    [SerializeField]
    /// 인스펙터창에서 Mana_Image 오브젝트를 드래그 앤 드랍 시키면 됩니다.
    private Image mManaImage;

    private IActor mActor;
    private float mMaxMana;
    private float mCurMana;
    private float mMaxWidth;
    private float mCurWidth;

    public float MaxWidth { get { return mMaxWidth; } private set { } }
    public float CurWidth { get { return mCurWidth; } private set { } }

    private void Awake()
    {
        if (mActorObject.GetComponent<IActor>() != null)
        {
            mActor = mActorObject.GetComponent<IActor>();
        }

        mMaxWidth = mManaImage.rectTransform.sizeDelta.x;
        mCurWidth = mMaxWidth;

        //StateManager.GamePauseHandler.AddEnterEvent(EPauseFlags.PAUSE, Deactivate);
        //StateManager.GamePauseHandler.AddEnterEvent(EPauseFlags.Play, Activate);
        //StateManager.GameOverHandler.AddEnterEvent(EGameOverFlags.GameOver, Deactivate);
    }

    private void Start()
    {
        //mMaxMana = mActor.Status.MaximumManaPoint;
        //mCurMana = mActor.Status.CurrentManaPoint;
    }

    public override void Activate()
    {
        this.gameObject.SetActive(true);
    }

    public override void Deactivate()
    {
        this.gameObject.SetActive(false);
    }

    public override void Express()
    {
        float resultWitdh = mMaxWidth * mCurMana / mMaxMana;

        mCurWidth = mManaImage.rectTransform.sizeDelta.x;
        mCurWidth = Mathf.Clamp(mCurWidth, 0, mMaxWidth);

        mManaImage.rectTransform.sizeDelta = new Vector2(resultWitdh, mManaImage.rectTransform.sizeDelta.y);
    }

    public override void GetInformation()
    {
        //mMaxMana = mActor.Status.MaximumManaPoint;
        //mCurMana = mActor.Status.CurrentManaPoint;
    }

    private void OnDestroy()
    {
        //StateManager.GamePauseHandler.RemoveEnterEvent(EPauseFlags.PAUSE, Deactivate);
        //StateManager.GamePauseHandler.RemoveEnterEvent(EPauseFlags.Play, Activate);
        //StateManager.GameOverHandler.RemoveEnterEvent(EGameOverFlags.GameOver, Deactivate);
    }
}
