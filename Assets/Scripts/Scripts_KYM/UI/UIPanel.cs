﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public enum ETitlePanel
{
   TITLE_SCENE_DISABLE_MENU, TITLE_SCENE_ENABLE_MENU, TITLE_SCENE_SETTING_MENU,
   TITLE_SCENE_SETTING_GAMEPlay, TITLE_SCENE_SETTING_GRAPHIC, TITLE_SCENE_SETTING_AUDIO, TITLE_SCENE_INPUT_NAME,
   INGAME_SCENE_MENU, INGAME_SCENE_STAGE_CLEAR ,INGAME_SCENE_GAMEOVER
}


public class UIPanel : MonoBehaviour
{
    [SerializeField]
    private ETitlePanel mPanel;
    public ETitlePanel EPanel { get { return mPanel; } }
    [SerializeField]
    private UIButton mHeaderButton;
    public UIButton HeaderButton { get { return mHeaderButton; } set { mHeaderButton = value; } }
    private UIButton mCurButton;
    public UIButton CurButton { get { return mCurButton; } set { mCurButton = value; } }

    private UIController mTitleController;
    public UIController UIController { get { return mTitleController; } }
    private RectTransform mRectTransform;
    public RectTransform RectTransform { get { return mRectTransform; } }
    private UIPanel mParentPanel;
    public UIPanel ParentPanel { get { return mParentPanel; } }

    private Dictionary<string, Text> mTextDict;
    private Dictionary<string, UIButton> mButtonDict;
    private Dictionary<string, Image> mImageDict;

    public delegate void OnEvent();
    public event OnEvent onEnable;
    public event OnEvent onDisable;

    private void Awake()
    {
        mTitleController = GetComponentInParent<UIController>();
        mRectTransform = GetComponent<RectTransform>();
        mParentPanel = transform.parent.GetComponentInParent<UIPanel>();

        mTextDict = new Dictionary<string, Text>();
        mButtonDict = new Dictionary<string, UIButton>();
        mImageDict = new Dictionary<string, Image>();

        Text[] texts = GetComponentsInChildren<Text>(true);
        UIButton[] buttons = GetComponentsInChildren<UIButton>(true);
        Image[] images = GetComponentsInChildren<Image>(true);

        foreach (Text text in texts)
        {
            mTextDict[text.name] = text;
        }
        foreach (UIButton button in buttons)
        {
            mButtonDict[button.name] = button;
        }
        foreach (Image image in images)
        {
            mImageDict[image.name] = image;
        }
    }


    public Text GetText(string textName)
    {
        if (mTextDict.ContainsKey(textName))
        {
            return mTextDict[textName];
        }
        return null;
    }

    public UIButton GetButton(string buttonName)
    {
        if (mButtonDict.ContainsKey(buttonName))
        {
            return mButtonDict[buttonName];
        }
        return null;
    }

    public Image GetImage(string imageName)
    {
        if (mImageDict.ContainsKey(imageName))
        {
            return mImageDict[imageName];
        }
        return null;
    }

    public void SetEnable(bool isEnabled)
    {
        gameObject.SetActive(isEnabled);
        if (isEnabled)
        {
            onEnable?.Invoke();
        }
        else
        {
            onDisable?.Invoke();
        }
    }
}
