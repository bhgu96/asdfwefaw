﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HPPostprocessingHud : AHud
{
    [SerializeField]
    private float mSaturation = 0.7f;
    [SerializeField]
    private float mBleedAmount = -0.2f;
    [SerializeField]
    private float mDistortAmount = 0.03f;

    //private ActorStatus mActorStatus;
    private Image mPostprocessingImage;
    private Material mPostprocessing;
    private float mMaxHP;
    private float mCurHP;
    private float mCurSaturation = 1;
    private float mCurBleedAmount = -1;
    private float mCurDistortAmount = 0;

    private void Awake()
    {
        mPostprocessingImage = this.GetComponent<Image>();
        mPostprocessing = this.GetComponent<Image>().material;
    }

    private void Start()
    {
        //if (NewGiant.Instance != null)
        //{
        //    mActorStatus = NewGiant.Instance.Status;
        //}
    }

    public override void Activate()
    {
    
    }

    public override void Deactivate()
    {

    }

    public override void Express()
    {
        if(mPostprocessing != null)
        {
            //현재 생명력이 최대 생명력의 25%이하가 되면 효과가 발동되도록 구현함
            if (mCurHP <= mMaxHP * 0.25f)
            {
                SetPostProcess.LowHealthPointPostProcess(mPostprocessing);
                //mCurSaturation -= Time.deltaTime * 0.5f;
                //mCurBleedAmount += Time.deltaTime * 0.5f;
                //mCurDistortAmount += Time.deltaTime * 0.05f;

                //mCurSaturation = Mathf.Clamp(mCurSaturation, mSaturation, 1);
                //mCurBleedAmount = Mathf.Clamp(mCurBleedAmount, -1, mBleedAmount);
                //mCurDistortAmount = Mathf.Clamp(mCurDistortAmount, 0.0f, mDistortAmount);

                //// (0~1) : 0에 가까울수록 흑백으로 변함
                //mPostprocessing.SetFloat("_Saturation", mCurSaturation);
                //// (-1~0) : -1에 가까울수록 피효과가 사라짐 0일때 최대
                //mPostprocessing.SetFloat("_bleedAmount", mCurBleedAmount);
                //// (-0.06~0.06) : 0에 가까울수록 왜곡이 사라짐 그 이상도 가능하지만 너무 심하게 왜곡됨
                //mPostprocessing.SetFloat("_distortAmount", mCurDistortAmount);
            }
            else
            {
                SetPostProcess.NormalHealthPointPostProcess(mPostprocessing);
                //mCurSaturation += Time.deltaTime * 0.5f;
                //mCurBleedAmount -= Time.deltaTime * 0.5f;
                //mCurDistortAmount -= Time.deltaTime * 0.05f;

                //mCurSaturation = Mathf.Clamp(mCurSaturation, mSaturation, 1);
                //mCurBleedAmount = Mathf.Clamp(mCurBleedAmount, -1, 0);
                //mCurDistortAmount = Mathf.Clamp(mCurDistortAmount, 0.0f, mDistortAmount);

                //// (0~1) : 0에 가까울수록 흑백으로 변함
                //mPostprocessing.SetFloat("_Saturation", mCurSaturation);
                //// (-1~0) : -1에 가까울수록 피효과가 사라짐 0일때 최대
                //mPostprocessing.SetFloat("_bleedAmount", mCurBleedAmount);
                //// (-0.06~0.06) : 0에 가까울수록 왜곡이 사라짐 그 이상도 가능하지만 너무 심하게 왜곡됨
                //mPostprocessing.SetFloat("_distortAmount", mCurDistortAmount);
            }

            if(StateManager.GameOver.State)
            {
                SetPostProcess.GameOverHealthPointPostProcess(mPostprocessing);
                //// (0~1) : 0에 가까울수록 흑백으로 변함
                //mPostprocessing.SetFloat("_Saturation", 0);
                //// (-1~0) : -1에 가까울수록 피효과가 사라짐 0일때 최대
                //mPostprocessing.SetFloat("_bleedAmount", -1);
                //// (-0.06~0.06) : 0에 가까울수록 왜곡이 사라짐 그 이상도 가능하지만 너무 심하게 왜곡됨
                //mPostprocessing.SetFloat("_distortAmount", 0);
            }
        }
    }

    public override void GetInformation()
    {
        //mMaxHP = mActorStatus.MaximumHealthPoint;
        //mCurHP = mActorStatus.CurrentHealthPoint;
    }
}
