﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIBlink : MonoBehaviour
{
    [SerializeField]
    private EBlinks mBlinkKind;
    private Text mText;
    private Image mImage;

    private void Awake()
    {
        if(mBlinkKind == EBlinks.TEXT)
        {
            mText = this.GetComponent<Text>();
        }
        else if(mBlinkKind == EBlinks.IMAGE)
        {
            mImage = this.GetComponent<Image>();
        }
    }

    private void OnEnable()
    {
        StartCoroutine(BlinkText());
    }

    private IEnumerator BlinkText()
    {
        while(true)
        {
            for (float i = 0; i <= 0.6f; i += Time.deltaTime)
            {
                if (mBlinkKind == EBlinks.TEXT)
                {
                    mText.color = new Color(1, 1, 1, i);
                }
                else if (mBlinkKind == EBlinks.IMAGE)
                {
                    mImage.color = new Color(1, 1, 1, i);
                }
                yield return null;
            }

            for (float i = 0.6f; i >= 0f; i -= Time.deltaTime)
            {
                if (mBlinkKind == EBlinks.TEXT)
                {
                    mText.color = new Color(1, 1, 1, i);
                }
                else if (mBlinkKind == EBlinks.IMAGE)
                {
                    mImage.color = new Color(1, 1, 1, i);
                }
                yield return null;
            }
        }
    }
}

public enum EBlinks
{
    None = -1,
    TEXT,
    IMAGE,
    Count
}
