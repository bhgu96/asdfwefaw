﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using UnityEngine.SceneManagement;

public class ButtonEvent_RestartGame : MonoBehaviour
{
    [SerializeField]
    private string mSceneName;

    private UIButton mButton;

    private void Awake()
    {
        mButton = GetComponent<UIButton>();
        mButton.onConfirm += confirmEvent;
    }

    private void confirmEvent()
    {
        StateManager.Pause.State = false;
        StateManager.GameOver.State = false;
        PlayerData.Save();
        Time.timeScale = 1.0f;

        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
        //LoadingController.LoadScene(mSceneName);
    }
}
