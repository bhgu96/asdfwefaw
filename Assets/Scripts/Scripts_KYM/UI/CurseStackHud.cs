﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CurseStackHud : AHud
{
    [SerializeField]
    /// 인스펙터창에서 포식귀 게임 오브젝트(작업했을때는 Prefab_Guardian으로 되어있었음)를 드래그 앤 드랍 시키면 됩니다.
    private GameObject mActorObject;
    [SerializeField]
    /// ex) mIndex = 1이면 활성화된 저주스택이 1일때 저주스택감소 애니메이션이 작동된다.
    private int mIndex;

    private IActor mActor;
    //private CurseSystem m_curseSystem;
    private Animator mAnimator;
    private bool mIsDecreased;

    private void Awake()
    {
        if(mActorObject.GetComponent<IActor>() != null)
        {
            mActor = mActorObject.GetComponent<IActor>();
        }

        mAnimator = this.GetComponent<Animator>();
    }

    private void Start()
    {
        //if(mActor.CurseSystem != null)
        //{
        //    m_curseSystem = mActor.CurseSystem;
        //}

        //_StateHandler stateHandler = CGameManager.Instance._stateManager.GetStateHandler(EStateHandler.HUD_ACTIVE_STATE);

        //stateHandler.AddHudEnterdEvent(_EHudState.Hud_Activate_All, Activate);
        //stateHandler.AddHudEnterdEvent(_EHudState.Hud_Deactivate_All, Deactivate);
    }

    public override void Activate()
    {
        //this.gameObject.SetActive(true);
    }

    public override void Deactivate()
    {
        //this.gameObject.SetActive(false);
    }

    public override void Express()
    {
        ///// 현재 활성화된 저주스택 카운트와 mIndex가 일치했을때, 저주스택감소 애니메이션을 작동
        //if(m_curseSystem.CurCurseCount == mIndex)
        //{
        //    mIsDecreased = true;
        //    mAnimator.SetBool("isDecrease", true); //활성화된 저주 스택으로 저주 스택 감소 애니메이션 재생
        //    mAnimator.SetBool("isRecover", false);
        //    mAnimator.SetBool("isGlitten", false);
        //}
        ///// 활성화된 저주스택이 줄어들었을때 저주스택을 복구하는 애니메이션을 작동
        //else if(m_curseSystem.CurCurseCount < mIndex && mIsDecreased)
        //{
        //    mIsDecreased = false;
        //    mAnimator.SetBool("isRecover", true); //활성화된 저주 스택이 감소했을때 다시 저주 스택 복구 애니메이션 재생
        //    mAnimator.SetBool("isDecrease", false);
        //    mAnimator.SetBool("isGlitten", false);
        //}

        ///// TermTime = 저주스택이 5개가 모두 활성화되고 저주데미지를 주기까지의 약간의 간격을 둔 시간
        ///// MaxTermTime = 최대 Term Time
        ///// TermTime >= MaxTermTime이 되면 다시 모든 스택들이 복구가 되고 스택이 0 인 상태로 저주시스템이 다시 시작된다.
        ///// 활성화 되어 저주스택감소 애니메이션이 모두 재생된 저주 스택들이 모두 복구가 되는 애니메이션 재생
        //if (m_curseSystem.TermTime >= m_curseSystem.MaxTermTime)
        //{
        //    mIsDecreased = false;
        //    mAnimator.SetBool("isGlitten", true); //반짝이면서 모든 저주 스택 복구 애니메이션 재생
        //    mAnimator.SetBool("isDecrease", false);
        //    mAnimator.SetBool("isRecover", false);
        //}
    }

    public override void GetInformation()
    {

    }
}
