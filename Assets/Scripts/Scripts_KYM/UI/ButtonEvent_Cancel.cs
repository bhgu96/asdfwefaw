﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ButtonEvent_Cancel : MonoBehaviour
{
    private UIPanel mPanel;
    private UIButton mButton;

    private void Awake()
    {
        mPanel = this.GetComponentInParent<UIPanel>();
        mButton = GetComponent<UIButton>();
        mButton.onConfirm += CancelMenuEvent;
    }

    private void CancelMenuEvent()
    {
        //mPanel.UIController.DisableTitleController();
        //StateManager.Pause.State = false;
        //Time.timeScale = 1.0f;
        //mPanel.UIController.ExitPanel();

        mPanel.UIController.ClosePanel();
    }
}
