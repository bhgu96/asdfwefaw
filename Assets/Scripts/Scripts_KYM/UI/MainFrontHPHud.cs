﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MainFrontHPHud : AHud
{
    [SerializeField]
    /// 인스펙터창에서 포식귀 게임 오브젝트(작업했을때는 Prefab_Guardian으로 되어있었음)를 드래그 앤 드랍 시키면 됩니다.
    private GameObject mActorObject;
    [SerializeField]
    /// Main_FrontHP_Image 오브젝트를 드래그 앤 드랍 시키면 됩니다.
    private Image mFrontHP;

    private IActor mActor;
    private float mMaxHP;
    private float mCurHP;
    private float mMaxWidth;
    private float mCurWidth;
    private Animator mAnimator;
    private float mPrevHP;

    public float MaxWidth { get { return mMaxWidth; } private set { } }
    public float CurWidth { get { return mCurWidth; } private set { } }

    private void Awake()
    {
        if (mActorObject.GetComponent<IActor>() != null)
        {
            mActor = mActorObject.GetComponent<IActor>();
        }

        mAnimator = this.transform.GetChild(0).GetComponent<Animator>();
        mMaxWidth = mFrontHP.rectTransform.sizeDelta.x;
        mCurWidth = mMaxWidth;
    }

    private void Start()
    {
        //mMaxHP = mActor.Status.MaximumHealthPoint;
        //mCurHP = mActor.Status.CurrentHealthPoint;
        //mPrevHP = mCurHP;

        //_StateHandler stateHandler = CGameManager.Instance._stateManager.GetStateHandler(EStateHandler.HUD_ACTIVE_STATE);

        //stateHandler.AddHudEnterdEvent(_EHudState.Hud_Activate_All, Activate);
        //stateHandler.AddHudEnterdEvent(_EHudState.Hud_Deactivate_All, Deactivate);
    }

    public override void Activate()
    {
        //this.gameObject.SetActive(true);
    }

    public override void Deactivate()
    {
        //this.gameObject.SetActive(false);
    }

    public override void Express()
    {
        mAnimator.SetFloat("prevHP", mCurHP - mPrevHP);
        float resultWitdh = mMaxWidth * mCurHP / mMaxHP;

        mCurWidth = mFrontHP.rectTransform.sizeDelta.x;
        mCurWidth = Mathf.Clamp(mCurWidth, 0, mMaxWidth);

        mFrontHP.rectTransform.sizeDelta = new Vector2(resultWitdh, mFrontHP.rectTransform.sizeDelta.y);
        mPrevHP = mCurHP;
    }

    public override void GetInformation()
    {
        //mMaxHP = mActor.Status.MaximumHealthPoint;
        //mCurHP = mActor.Status.CurrentHealthPoint;
    }
}
