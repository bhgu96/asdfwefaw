﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ActorEffectHud : AHud
{
    [SerializeField]
    /// 인스펙터창에서 포식귀 게임 오브젝트(작업했을때는 Prefab_Guardian으로 되어있었음)를 드래그 앤 드랍 시키면 됩니다.
    private GameObject mActorObject;

    private List<IActorEffect> mActorEffectList;
    private IActor mActor;
    private int childCount;

    private void Awake()
    {
        mActorEffectList = new List<IActorEffect>();
        childCount = this.transform.childCount;

        //StateManager.GamePauseHandler.AddEnterEvent(EPauseFlags.PAUSE, Deactivate);
        //StateManager.GamePauseHandler.AddEnterEvent(EPauseFlags.Play, Activate);
        //StateManager.GameOverHandler.AddEnterEvent(EGameOverFlags.GameOver, Deactivate);
    }

    private void Start()
    {
        if (mActorObject.GetComponent<IActor>() != null)
        {
            mActor = mActorObject.GetComponent<IActor>();
        }
    }

    public override void Activate()
    {
        this.gameObject.SetActive(true);
    }

    public override void Deactivate()
    {
        this.gameObject.SetActive(false);
    }

    public override void Express()
    {
        int index = 0;

        for (; index < mActorEffectList.Count; index++)
        {
            if (index < childCount)
            {
                this.transform.GetChild(index).gameObject.SetActive(true);
                //this.transform.GetChild(index).GetChild(1).GetComponent<Image>().sprite = mActorEffectList[index].Icon;
            }
        }

        for (; index < childCount; index++)
        {
            this.transform.GetChild(index).gameObject.SetActive(false);
        }
    }

    public override void GetInformation()
    {
        mActorEffectList.Clear();

        //for (EBuffFlags b = (EBuffFlags)0; b < EBuffFlags.Count; b++)
        //{
        //    if (mActor.State.BuffDict[b] != null && !mActorEffectList.Contains(mActor.State.BuffDict[b]))
        //    {
        //        mActorEffectList.Add(mActor.State.BuffDict[b]);
        //    }
        //}

        //for (EDebuffFlags d = (EDebuffFlags)0; d < EDebuffFlags.Count; d++)
        //{
        //    if (mActor.State.DebuffDict[d] != null && !mActorEffectList.Contains(mActor.State.DebuffDict[d]))
        //    {
        //        mActorEffectList.Add(mActor.State.DebuffDict[d]);
        //    }
        //}
    }

    private void OnDestroy()
    {
        //StateManager.GamePauseHandler.RemoveEnterEvent(EPauseFlags.PAUSE, Deactivate);
        //StateManager.GamePauseHandler.RemoveEnterEvent(EPauseFlags.Play, Activate);
        //StateManager.GameOverHandler.RemoveEnterEvent(EGameOverFlags.GameOver, Deactivate);
    }
}
