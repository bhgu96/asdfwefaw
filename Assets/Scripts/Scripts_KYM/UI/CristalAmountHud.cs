﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CristalAmountHud : AHud
{
    private Text mCristalAmountText;  //크리스탈량을 나타낼 UI Text
    private float mCristalAmount; //현재 액터가 가진 크리스탈량을 가져올 변수

    private void Awake()
    {
        mCristalAmountText = this.GetComponentInChildren<Text>();

        StateManager.Pause.AddEnterEvent(true, Deactivate);
        StateManager.Pause.AddEnterEvent(false, Activate);
        StateManager.GameOver.AddEnterEvent(true, Deactivate);
    }

    public override void Activate()
    {
        this.gameObject.SetActive(true);
    }

    public override void Deactivate()
    {
        this.gameObject.SetActive(false);
    }

    /// <summary>
    /// AHud 클래스에서 Update에서 호출됨
    /// </summary>
    public override void Express()
    {
        /// Actor의 가져온 크리스탈량 HUD에 띄우기
        /// mGoldAmount.ToString()을 통해서 크리스탈량을 나타냄
        mCristalAmountText.text = ":" + mCristalAmount.ToString();
    }

    /// <summary>
    /// AHud 클래스에서 Update에서 호출됨
    /// </summary>
    public override void GetInformation()
    {
        /// Actor의 크리스탈량 가져오기
        /// ex) mCristalAmount = mActor.CristalAmount;
        /// 

        mCristalAmount = 0;
    }

    private void OnDestroy()
    {
        StateManager.Pause.RemoveEnterEvent(true, Deactivate);
        StateManager.Pause.RemoveEnterEvent(false, Activate);
        StateManager.GameOver.RemoveEnterEvent(true, Deactivate);
    }
}
