﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class UIButton : Button
{
    private RectTransform mRectTransform;
    public RectTransform RectTransform { get { return mRectTransform; } }
    private UIController mTitleController;
    public UIController UIController { get { return mTitleController; } }

    private UIPanel mPanel;
    public UIPanel Panel { get { return mPanel; } }

    public delegate void OnEvent();
    public event OnEvent onConfirm;
    public event OnEvent onCancel;
    public event OnEvent onLeft;
    public event OnEvent onRight;
    public event OnEvent onDown;
    public event OnEvent onUp;
    public event OnEvent onSelect;
    public event OnEvent onDeselect;

    protected override void Awake()
    {
        base.Awake();
        mTitleController = GetComponentInParent<UIController>();
        mPanel = GetComponentInParent<UIPanel>();
        mRectTransform = GetComponent<RectTransform>();
    }

    public Selectable MoveLeft()
    {
        onLeft?.Invoke();
        return FindSelectableOnLeft();
    }

    public Selectable MoveRight()
    {
        onRight?.Invoke();
        return FindSelectableOnRight();
    }

    public Selectable MoveUp()
    {
        onUp?.Invoke();
        return FindSelectableOnUp();
    }

    public Selectable MoveDown()
    {
        onDown?.Invoke();
        return FindSelectableOnDown();
    }

    public void Deselect()
    {
        onDeselect?.Invoke();
    }

    public void Confirm()
    {
        onConfirm?.Invoke();
    }

    public void Cancel()
    {
        onCancel?.Invoke();
    }

    public override void Select()
    {
        if(Panel != null)
        {
            Panel.CurButton = this;
            onSelect?.Invoke();
        }
    }

    public override void OnPointerClick(PointerEventData eventData)
    {
        if (IsInteractable())
        {
            if(eventData.button == PointerEventData.InputButton.Right)
            {
                if(this.gameObject.GetComponent<ItemSlotInfo>() != null)
                {
                    if (this.gameObject.GetComponent<ItemSlotInfo>().ConnectSlot != null)
                    {
                        this.gameObject.GetComponent<DragHandler>().DeselectItem(this.gameObject);
                        this.gameObject.GetComponent<ItemSlotInfo>().PopupTooltip(false);
                    }
                }


                if(this.gameObject.GetComponent<SkillSlotType>() != null)
                {
                    this.gameObject.GetComponent<SkillSlotType>().ConnectSkill.EquipSkill(this.gameObject.GetComponent<SkillSlotType>().CharacterType, false, this.gameObject.GetComponent<SkillSlotType>().EquipIndex);
                    this.gameObject.GetComponent<SkillSlotType>().ConnectSkill = null;
                    this.gameObject.GetComponent<Image>().sprite = this.gameObject.GetComponent<SkillSlotType>().DefaultSprite;
                }
            }

            if(eventData.button != PointerEventData.InputButton.Right)
            {
                if(UIController != null)
                {
                    UIController.CurButton = this;
                }

                Confirm();
            }
        }
    }

    public override void OnPointerEnter(PointerEventData eventData)
    {
        if (IsInteractable())
        {
            if (this.gameObject.GetComponent<ItemSlotInfo>() != null)
            {
                if(this.gameObject.GetComponent<ItemSlotInfo>().mItem != null)
                {
                    this.gameObject.GetComponent<ItemSlotInfo>().PopupTooltip(true, this.gameObject.GetComponent<RectTransform>().parent.localPosition);
                }
                else if(this.gameObject.GetComponent<ItemSlotInfo>().ConnectSlot != null)
                {
                    this.gameObject.GetComponent<ItemSlotInfo>().PopupTooltip(true, this.gameObject.GetComponent<RectTransform>().parent.localPosition, true);
                }
            }
            else if(this.gameObject.GetComponent<SkillSlotInfo>() != null)
            {
                this.gameObject.GetComponent<SkillSlotInfo>().PopupSkillInfo(true, this.gameObject.GetComponent<RectTransform>().parent.localPosition);
            }
            else if(this.gameObject.GetComponent<SkillSlotType>() != null)
            {
                if(this.gameObject.GetComponent<SkillSlotType>().ConnectSkill != null)
                {
                    this.gameObject.GetComponent<SkillSlotType>().PopupSkillInfo(true, this.gameObject.GetComponent<RectTransform>().parent.localPosition);
                }
            }

            if(UIController != null)
            {
                UIController.CurButton = this;
            }
        }
    }

    public override void OnPointerExit(PointerEventData eventData)
    {
        if (IsInteractable())
        {
            if (this.gameObject.GetComponent<ItemSlotInfo>() != null)
            {
                this.gameObject.GetComponent<ItemSlotInfo>().PopupTooltip(false);
            }
            else if(this.gameObject.GetComponent<SkillSlotInfo>() != null)
            {
                this.gameObject.GetComponent<SkillSlotInfo>().PopupSkillInfo(false);
            }
            else if(this.gameObject.GetComponent<SkillSlotType>() != null)
            {
                this.gameObject.GetComponent<SkillSlotType>().PopupSkillInfo(false);
            }

            onDeselect?.Invoke();
        }
    }

    public bool IsButtonNull()
    {
        if(onConfirm != null)
        {
            return false;
        }
        else
        {
            return true;
        }
    }
}
