﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ButtonEvent_SinglePlay : MonoBehaviour
{
    [SerializeField]
    private List<ButtonEvent_SelectableButtons> mSelectButtonList;

    [SerializeField]
    private EGameScene mNextSceneName;
    [SerializeField]
    private CameraController mCamera;
    [SerializeField]
    private AudioSource mBackgroundAudio;
    [SerializeField]
    private AudioSource mConfirmAudio;
    [SerializeField]
    private UIPanel mNamePanel;

    private UIController mTitleController;
    private UIButton mButton;

    private void Awake()
    {
        mButton = GetComponent<UIButton>();
        mTitleController = GetComponentInParent<UIController>();
        mButton.onConfirm += startSinglePlay;
    }

    private void startSinglePlay()
    {
        if(this.gameObject.activeInHierarchy)
        {
            if (mConfirmAudio != null)
            {
                mConfirmAudio.Play();
            }

            mTitleController.DisableTitleController();
            mCamera.BlackOut(0.1f, 1.0f);
            StartCoroutine(waitForFade());
        }
    }

    private IEnumerator waitForFade()
    {
        mConfirmAudio = null;
        for(int i = 0; i < mSelectButtonList.Count; i++)
        {
            mSelectButtonList[i].SelectAudio = null;
        }

        while (mBackgroundAudio.volume > 0.0f)
        {
            mBackgroundAudio.volume -= Time.deltaTime;
            yield return null;
        }

        yield return new WaitUntil(() => !mCamera.IsBlackOutTriggered);
        LoadingController.LoadScene(mNextSceneName.ToString());
    }
}
