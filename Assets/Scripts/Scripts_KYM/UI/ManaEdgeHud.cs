﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ManaEdgeHud : AHud
{
    [SerializeField]
    /// 인스펙터창에서 Mana_Image 오브젝트를 드래그 앤 드랍 시키면 됩니다.
    private MainManaHud mMana;

    private Image mEdgeImage;

    private void Awake()
    {
        mEdgeImage = this.transform.GetChild(0).GetComponent<Image>();

        //StateManager.GamePauseHandler.AddEnterEvent(EPauseFlags.PAUSE, Deactivate);
        //StateManager.GamePauseHandler.AddEnterEvent(EPauseFlags.Play, Activate);
        //StateManager.GameOverHandler.AddEnterEvent(EGameOverFlags.GameOver, Deactivate);
    }

    public override void Activate()
    {
        this.gameObject.SetActive(true);
    }

    public override void Deactivate()
    {
        this.gameObject.SetActive(false);
    }

    public override void Express()
    {
        float resultPosition_X = -(mMana.MaxWidth - mMana.CurWidth);
        resultPosition_X = Mathf.Clamp(resultPosition_X, -mMana.MaxWidth, 0);

        mEdgeImage.rectTransform.anchoredPosition = new Vector3(resultPosition_X, mEdgeImage.rectTransform.anchoredPosition.y);
    }

    public override void GetInformation()
    {

    }

    private void OnDestroy()
    {
        //StateManager.GamePauseHandler.RemoveEnterEvent(EPauseFlags.PAUSE, Deactivate);
        //StateManager.GamePauseHandler.RemoveEnterEvent(EPauseFlags.Play, Activate);
        //StateManager.GameOverHandler.RemoveEnterEvent(EGameOverFlags.GameOver, Deactivate);
    }
}
