﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MPMaskHud : AHud
{
    #region State Events
    //[SerializeField]
    //private EPauseFlags[] mGamePauseEnterActivate;
    //[SerializeField]
    //private EPauseFlags[] mGamePauseEnterDeactivate;
    //[SerializeField]
    //private EPauseFlags[] mGamePauseExitActivate;
    //[SerializeField]
    //private EPauseFlags[] mGamePauseExitDeactivate;

    //[SerializeField]
    //private EGameOverFlags[] mGameOverEnterActivate;
    //[SerializeField]
    //private EGameOverFlags[] mGameOverEnterDeactivate;
    //[SerializeField]
    //private EGameOverFlags[] mGameOverExitActivate;
    //[SerializeField]
    //private EGameOverFlags[] mGameOverExitDeactivate;

    //[SerializeField]
    //private EGameStateFlags[] mGameStateEnterActivate;
    //[SerializeField]
    //private EGameStateFlags[] mGameStateEnterDeactivate;
    //[SerializeField]
    //private EGameStateFlags[] mGameStateExitActivate;
    //[SerializeField]
    //private EGameStateFlags[] mGameStateExitDeactivate;
    #endregion
    
    private Image mImage;

    private void Awake()
    {
        AddStateEvent();
        mImage = this.GetComponent<Image>();
    }

    public override void Activate()
    {
        mImage.enabled = true;
    }

    public override void Deactivate()
    {
        mImage.enabled = false;
    }

    public override void Express()
    {

    }

    public override void GetInformation()
    {

    }

    private void AddStateEvent()
    {
        StateManager.Pause.AddEnterEvent(false, Activate);
        StateManager.Pause.AddEnterEvent(true, Deactivate);
        StateManager.Pause.AddExitEvent(false, Activate);
        StateManager.Pause.AddExitEvent(true, Deactivate);

        StateManager.GameOver.AddEnterEvent(false, Activate);
        StateManager.GameOver.AddEnterEvent(true, Deactivate);
        StateManager.GameOver.AddExitEvent(false, Activate);
        StateManager.GameOver.AddExitEvent(true, Deactivate);

        //if (mGamePauseEnterActivate.Length != 0)
        //{
        //    for (int i = 0; i < mGamePauseEnterActivate.Length; i++)
        //    {
        //        StateManager.GamePauseHandler.AddEnterEvent(mGamePauseEnterActivate[i], Activate);
        //    }
        //}
        //if (mGamePauseEnterDeactivate.Length != 0)
        //{
        //    for (int i = 0; i < mGamePauseEnterDeactivate.Length; i++)
        //    {
        //        StateManager.GamePauseHandler.AddEnterEvent(mGamePauseEnterDeactivate[i], Deactivate);
        //    }
        //}
        //if (mGamePauseExitActivate.Length != 0)
        //{
        //    for (int i = 0; i < mGamePauseExitActivate.Length; i++)
        //    {
        //        StateManager.GamePauseHandler.AddExitEvent(mGamePauseExitActivate[i], Activate);
        //    }
        //}
        //if (mGamePauseExitDeactivate.Length != 0)
        //{
        //    for (int i = 0; i < mGamePauseExitDeactivate.Length; i++)
        //    {
        //        StateManager.GamePauseHandler.AddExitEvent(mGamePauseExitDeactivate[i], Deactivate);
        //    }
        //}

        //if (mGameOverEnterActivate.Length != 0)
        //{
        //    for (int i = 0; i < mGameOverEnterActivate.Length; i++)
        //    {
        //        StateManager.GameOverHandler.AddEnterEvent(mGameOverEnterActivate[i], Activate);
        //    }
        //}
        //if (mGameOverEnterDeactivate.Length != 0)
        //{
        //    for (int i = 0; i < mGameOverEnterDeactivate.Length; i++)
        //    {
        //        StateManager.GameOverHandler.AddEnterEvent(mGameOverEnterDeactivate[i], Deactivate);
        //    }
        //}
        //if (mGameOverExitActivate.Length != 0)
        //{
        //    for (int i = 0; i < mGameOverExitActivate.Length; i++)
        //    {
        //        StateManager.GameOverHandler.AddExitEvent(mGameOverExitActivate[i], Activate);
        //    }
        //}
        //if (mGameOverExitDeactivate.Length != 0)
        //{
        //    for (int i = 0; i < mGameOverExitDeactivate.Length; i++)
        //    {
        //        StateManager.GameOverHandler.AddExitEvent(mGameOverExitDeactivate[i], Deactivate);
        //    }
        //}

        //if (mGameStateEnterActivate.Length != 0)
        //{
        //    for (int i = 0; i < mGameStateEnterActivate.Length; i++)
        //    {
        //        StateManager.GameStateHandler.AddEnterEvent(mGameStateEnterActivate[i], Activate);
        //    }
        //}
        //if (mGameStateEnterDeactivate.Length != 0)
        //{
        //    for (int i = 0; i < mGameStateEnterDeactivate.Length; i++)
        //    {
        //        StateManager.GameStateHandler.AddEnterEvent(mGameStateEnterDeactivate[i], Deactivate);
        //    }
        //}
        //if (mGameStateExitActivate.Length != 0)
        //{
        //    for (int i = 0; i < mGameStateExitActivate.Length; i++)
        //    {
        //        StateManager.GameStateHandler.AddExitEvent(mGameStateExitActivate[i], Activate);
        //    }
        //}
        //if (mGameStateExitDeactivate.Length != 0)
        //{
        //    for (int i = 0; i < mGameStateExitDeactivate.Length; i++)
        //    {
        //        StateManager.GameStateHandler.AddExitEvent(mGameStateExitDeactivate[i], Deactivate);
        //    }
        //}
    }

    private void OnDestroy()
    {
        RemoveStateEvent();
    }

    private void RemoveStateEvent()
    {
        StateManager.Pause.RemoveEnterEvent(false, Activate);
        StateManager.Pause.RemoveEnterEvent(true, Deactivate);
        StateManager.Pause.RemoveExitEvent(false, Activate);
        StateManager.Pause.RemoveExitEvent(true, Deactivate);

        StateManager.GameOver.RemoveEnterEvent(false, Activate);
        StateManager.GameOver.RemoveEnterEvent(true, Deactivate);
        StateManager.GameOver.RemoveExitEvent(false, Activate);
        StateManager.GameOver.RemoveExitEvent(true, Deactivate);

        //if (mGamePauseEnterActivate.Length != 0)
        //{
        //    for (int i = 0; i < mGamePauseEnterActivate.Length; i++)
        //    {
        //        StateManager.GamePauseHandler.RemoveEnterEvent(mGamePauseEnterActivate[i], Activate);
        //    }
        //}
        //if (mGamePauseEnterDeactivate.Length != 0)
        //{
        //    for (int i = 0; i < mGamePauseEnterDeactivate.Length; i++)
        //    {
        //        StateManager.GamePauseHandler.RemoveEnterEvent(mGamePauseEnterDeactivate[i], Deactivate);
        //    }
        //}
        //if (mGamePauseExitActivate.Length != 0)
        //{
        //    for (int i = 0; i < mGamePauseExitActivate.Length; i++)
        //    {
        //        StateManager.GamePauseHandler.RemoveExitEvent(mGamePauseExitActivate[i], Activate);
        //    }
        //}
        //if (mGamePauseExitDeactivate.Length != 0)
        //{
        //    for (int i = 0; i < mGamePauseExitDeactivate.Length; i++)
        //    {
        //        StateManager.GamePauseHandler.RemoveExitEvent(mGamePauseExitDeactivate[i], Deactivate);
        //    }
        //}

        //if (mGameOverEnterActivate.Length != 0)
        //{
        //    for (int i = 0; i < mGameOverEnterActivate.Length; i++)
        //    {
        //        StateManager.GameOverHandler.RemoveEnterEvent(mGameOverEnterActivate[i], Activate);
        //    }
        //}
        //if (mGameOverEnterDeactivate.Length != 0)
        //{
        //    for (int i = 0; i < mGameOverEnterDeactivate.Length; i++)
        //    {
        //        StateManager.GameOverHandler.RemoveEnterEvent(mGameOverEnterDeactivate[i], Deactivate);
        //    }
        //}
        //if (mGameOverExitActivate.Length != 0)
        //{
        //    for (int i = 0; i < mGameOverExitActivate.Length; i++)
        //    {
        //        StateManager.GameOverHandler.RemoveExitEvent(mGameOverExitActivate[i], Activate);
        //    }
        //}
        //if (mGameOverExitDeactivate.Length != 0)
        //{
        //    for (int i = 0; i < mGameOverExitDeactivate.Length; i++)
        //    {
        //        StateManager.GameOverHandler.RemoveExitEvent(mGameOverExitDeactivate[i], Deactivate);
        //    }
        //}

        //if (mGameStateEnterActivate.Length != 0)
        //{
        //    for (int i = 0; i < mGameStateEnterActivate.Length; i++)
        //    {
        //        StateManager.GameStateHandler.RemoveEnterEvent(mGameStateEnterActivate[i], Activate);
        //    }
        //}
        //if (mGameStateEnterDeactivate.Length != 0)
        //{
        //    for (int i = 0; i < mGameStateEnterDeactivate.Length; i++)
        //    {
        //        StateManager.GameStateHandler.RemoveEnterEvent(mGameStateEnterDeactivate[i], Deactivate);
        //    }
        //}
        //if (mGameStateExitActivate.Length != 0)
        //{
        //    for (int i = 0; i < mGameStateExitActivate.Length; i++)
        //    {
        //        StateManager.GameStateHandler.RemoveExitEvent(mGameStateExitActivate[i], Activate);
        //    }
        //}
        //if (mGameStateExitDeactivate.Length != 0)
        //{
        //    for (int i = 0; i < mGameStateExitDeactivate.Length; i++)
        //    {
        //        StateManager.GameStateHandler.RemoveExitEvent(mGameStateExitDeactivate[i], Deactivate);
        //    }
        //}
    }
}
