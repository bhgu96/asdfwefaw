﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GoldAmountHud : AHud
{
    private const float ANIM_TIME = 0.02f;
    private const float EMPHASIZING_TIME = 0.01f;
    private const float RELEASING_TIME = 0.05f;

    #region State Events
    //[SerializeField]
    //private EPauseFlags[] mGamePauseEnterActivate;
    //[SerializeField]
    //private EPauseFlags[] mGamePauseEnterDeactivate;
    //[SerializeField]
    //private EPauseFlags[] mGamePauseExitActivate;
    //[SerializeField]
    //private EPauseFlags[] mGamePauseExitDeactivate;

    //[SerializeField]
    //private EGameOverFlags[] mGameOverEnterActivate;
    //[SerializeField]
    //private EGameOverFlags[] mGameOverEnterDeactivate;
    //[SerializeField]
    //private EGameOverFlags[] mGameOverExitActivate;
    //[SerializeField]
    //private EGameOverFlags[] mGameOverExitDeactivate;

    //[SerializeField]
    //private EGameStateFlags[] mGameStateEnterActivate;
    //[SerializeField]
    //private EGameStateFlags[] mGameStateEnterDeactivate;
    //[SerializeField]
    //private EGameStateFlags[] mGameStateExitActivate;
    //[SerializeField]
    //private EGameStateFlags[] mGameStateExitDeactivate;
    #endregion

    //private Fairy_V2 mFairy;
    private Text mGoldAmountText; //골드량을 나타낼 UI Text
    private Image mGoldImage; // 골드 애니메이션
    private int mGoldAmount; //현재 액터가 가진 골드량을 가져올 변수
    private float mRemainTime;
    private float mEmphasizingRemainTime;
    private float mReleasingRemainTime;
    private bool mIsEmphasizing = false;

    private void Awake()
    {
        AddStateEvent();
        mGoldAmountText = this.GetComponentInChildren<Text>();
        mGoldImage = this.GetComponentInChildren<Image>();
    }

    private void Start()
    {
        //mFairy = Fairy_V2.Instance;
    }

    public override void Activate()
    {
        this.gameObject.SetActive(true);
        //StartCoroutine(HudFade.FadeIn(mGoldImage, mGoldAmountText));
    }

    public override void Deactivate()
    {
        this.gameObject.SetActive(false);
        //StartCoroutine(HudFade.FadeOut(mGoldImage, mGoldAmountText));
    }

    /// <summary>
    /// AHud 클래스에서 Update에서 호출됨
    /// </summary>
    public override void Express()
    {
        /// Actor의 가져온 골드량 HUD에 띄우기
        /// mGoldAmount.ToString()을 통해서 골드량을 나타냄
        mGoldAmountText.text = mGoldAmount.ToString();
    }

    /// <summary>
    /// AHud 클래스에서 Update에서 호출됨
    /// </summary>
    public override void GetInformation()
    {
        /// Actor의 골드량 가져오기
        /// ex) mGoldAmount = mActor.GoldAmount;
        /// 

        //if (mRemainTime > 0.0f)
        //{
        //    mRemainTime -= Time.deltaTime;
        //}
        //else
        //{
        //    int nextAmount = mFairy.Wallet.Gold;
        //    if (nextAmount > mGoldAmount)
        //    {
        //        mGoldAmount += (nextAmount - mGoldAmount) / 5 + 1;
        //        mRemainTime = ANIM_TIME;
        //        mReleasingRemainTime = RELEASING_TIME;
        //        if (!mIsEmphasizing)
        //        {
        //            mIsEmphasizing = true;
        //            mEmphasizingRemainTime = EMPHASIZING_TIME;
        //        }
        //        else
        //        {
        //            if (mEmphasizingRemainTime > 0.0f)
        //            {
        //                mEmphasizingRemainTime -= Time.deltaTime;
        //                mGoldAmountText.fontSize = (int)Mathf.Lerp(100, 60, mEmphasizingRemainTime / EMPHASIZING_TIME);
        //            }
        //            else
        //            {
        //                mGoldAmountText.fontSize = 100;
        //            }
        //        }
        //    }
        //    else if (nextAmount < mGoldAmount)
        //    {
        //        mGoldAmount += (nextAmount - mGoldAmount) / 5 - 1;
        //        mRemainTime = ANIM_TIME;
                
        //        mReleasingRemainTime = RELEASING_TIME;

        //        if (!mIsEmphasizing)
        //        {
        //            mIsEmphasizing = true;
        //            mEmphasizingRemainTime = EMPHASIZING_TIME;
        //            mReleasingRemainTime = RELEASING_TIME;
        //        }
        //        else
        //        {
        //            if (mEmphasizingRemainTime > 0.0f)
        //            {
        //                mEmphasizingRemainTime -= Time.deltaTime;
        //                mGoldAmountText.fontSize = (int)Mathf.Lerp(100, 60, mEmphasizingRemainTime / EMPHASIZING_TIME);
        //            }
        //            else
        //            {
        //                mGoldAmountText.fontSize = 100;
        //            }
        //        }
        //    }
        //    if (mIsEmphasizing)
        //    {
        //        if (mReleasingRemainTime > 0.0f)
        //        {
        //            mReleasingRemainTime -= Time.deltaTime;
        //            mGoldAmountText.fontSize = (int)Mathf.Lerp(60, mGoldAmountText.fontSize, mReleasingRemainTime / RELEASING_TIME);
        //        }
        //        else
        //        {
        //            mGoldAmountText.fontSize = 60;
        //            mIsEmphasizing = false;
        //        }
        //    }
        //}
    }

    private void AddStateEvent()
    {
        StateManager.Pause.AddEnterEvent(false, Activate);
        StateManager.Pause.AddEnterEvent(true, Deactivate);
        StateManager.Pause.AddExitEvent(false, Activate);
        StateManager.Pause.AddExitEvent(true, Deactivate);

        StateManager.GameOver.AddEnterEvent(false, Activate);
        StateManager.GameOver.AddEnterEvent(true, Deactivate);
        StateManager.GameOver.AddExitEvent(false, Activate);
        StateManager.GameOver.AddExitEvent(true, Deactivate);


        //if (mGamePauseEnterActivate.Length != 0)
        //{
        //    for (int i = 0; i < mGamePauseEnterActivate.Length; i++)
        //    {
        //        StateManager.GamePauseHandler.AddEnterEvent(mGamePauseEnterActivate[i], Activate);
        //    }
        //}
        //if (mGamePauseEnterDeactivate.Length != 0)
        //{
        //    for (int i = 0; i < mGamePauseEnterDeactivate.Length; i++)
        //    {
        //        StateManager.GamePauseHandler.AddEnterEvent(mGamePauseEnterDeactivate[i], Deactivate);
        //    }
        //}
        //if (mGamePauseExitActivate.Length != 0)
        //{
        //    for (int i = 0; i < mGamePauseExitActivate.Length; i++)
        //    {
        //        StateManager.GamePauseHandler.AddExitEvent(mGamePauseExitActivate[i], Activate);
        //    }
        //}
        //if (mGamePauseExitDeactivate.Length != 0)
        //{
        //    for (int i = 0; i < mGamePauseExitDeactivate.Length; i++)
        //    {
        //        StateManager.GamePauseHandler.AddExitEvent(mGamePauseExitDeactivate[i], Deactivate);
        //    }
        //}

        //if (mGameOverEnterActivate.Length != 0)
        //{
        //    for (int i = 0; i < mGameOverEnterActivate.Length; i++)
        //    {
        //        StateManager.GameOverHandler.AddEnterEvent(mGameOverEnterActivate[i], Activate);
        //    }
        //}
        //if (mGameOverEnterDeactivate.Length != 0)
        //{
        //    for (int i = 0; i < mGameOverEnterDeactivate.Length; i++)
        //    {
        //        StateManager.GameOverHandler.AddEnterEvent(mGameOverEnterDeactivate[i], Deactivate);
        //    }
        //}
        //if (mGameOverExitActivate.Length != 0)
        //{
        //    for (int i = 0; i < mGameOverExitActivate.Length; i++)
        //    {
        //        StateManager.GameOverHandler.AddExitEvent(mGameOverExitActivate[i], Activate);
        //    }
        //}
        //if (mGameOverExitDeactivate.Length != 0)
        //{
        //    for (int i = 0; i < mGameOverExitDeactivate.Length; i++)
        //    {
        //        StateManager.GameOverHandler.AddExitEvent(mGameOverExitDeactivate[i], Deactivate);
        //    }
        //}

        //if (mGameStateEnterActivate.Length != 0)
        //{
        //    for (int i = 0; i < mGameStateEnterActivate.Length; i++)
        //    {
        //        StateManager.GameStateHandler.AddEnterEvent(mGameStateEnterActivate[i], Activate);
        //    }
        //}
        //if (mGameStateEnterDeactivate.Length != 0)
        //{
        //    for (int i = 0; i < mGameStateEnterDeactivate.Length; i++)
        //    {
        //        StateManager.GameStateHandler.AddEnterEvent(mGameStateEnterDeactivate[i], Deactivate);
        //    }
        //}
        //if (mGameStateExitActivate.Length != 0)
        //{
        //    for (int i = 0; i < mGameStateExitActivate.Length; i++)
        //    {
        //        StateManager.GameStateHandler.AddExitEvent(mGameStateExitActivate[i], Activate);
        //    }
        //}
        //if (mGameStateExitDeactivate.Length != 0)
        //{
        //    for (int i = 0; i < mGameStateExitDeactivate.Length; i++)
        //    {
        //        StateManager.GameStateHandler.AddExitEvent(mGameStateExitDeactivate[i], Deactivate);
        //    }
        //}
    }

    private void OnDestroy()
    {
        RemoveStateEvent();
    }

    private void RemoveStateEvent()
    {
        StateManager.Pause.RemoveEnterEvent(false, Activate);
        StateManager.Pause.RemoveEnterEvent(true, Deactivate);
        StateManager.Pause.RemoveExitEvent(false, Activate);
        StateManager.Pause.RemoveExitEvent(true, Deactivate);

        StateManager.GameOver.RemoveEnterEvent(false, Activate);
        StateManager.GameOver.RemoveEnterEvent(true, Deactivate);
        StateManager.GameOver.RemoveExitEvent(false, Activate);
        StateManager.GameOver.RemoveExitEvent(true, Deactivate);


        //if (mGamePauseEnterActivate.Length != 0)
        //{
        //    for (int i = 0; i < mGamePauseEnterActivate.Length; i++)
        //    {
        //        StateManager.GamePauseHandler.RemoveEnterEvent(mGamePauseEnterActivate[i], Activate);
        //    }
        //}
        //if (mGamePauseEnterDeactivate.Length != 0)
        //{
        //    for (int i = 0; i < mGamePauseEnterDeactivate.Length; i++)
        //    {
        //        StateManager.GamePauseHandler.RemoveEnterEvent(mGamePauseEnterDeactivate[i], Deactivate);
        //    }
        //}
        //if (mGamePauseExitActivate.Length != 0)
        //{
        //    for (int i = 0; i < mGamePauseExitActivate.Length; i++)
        //    {
        //        StateManager.GamePauseHandler.RemoveExitEvent(mGamePauseExitActivate[i], Activate);
        //    }
        //}
        //if (mGamePauseExitDeactivate.Length != 0)
        //{
        //    for (int i = 0; i < mGamePauseExitDeactivate.Length; i++)
        //    {
        //        StateManager.GamePauseHandler.RemoveExitEvent(mGamePauseExitDeactivate[i], Deactivate);
        //    }
        //}

        //if (mGameOverEnterActivate.Length != 0)
        //{
        //    for (int i = 0; i < mGameOverEnterActivate.Length; i++)
        //    {
        //        StateManager.GameOverHandler.RemoveEnterEvent(mGameOverEnterActivate[i], Activate);
        //    }
        //}
        //if (mGameOverEnterDeactivate.Length != 0)
        //{
        //    for (int i = 0; i < mGameOverEnterDeactivate.Length; i++)
        //    {
        //        StateManager.GameOverHandler.RemoveEnterEvent(mGameOverEnterDeactivate[i], Deactivate);
        //    }
        //}
        //if (mGameOverExitActivate.Length != 0)
        //{
        //    for (int i = 0; i < mGameOverExitActivate.Length; i++)
        //    {
        //        StateManager.GameOverHandler.RemoveExitEvent(mGameOverExitActivate[i], Activate);
        //    }
        //}
        //if (mGameOverExitDeactivate.Length != 0)
        //{
        //    for (int i = 0; i < mGameOverExitDeactivate.Length; i++)
        //    {
        //        StateManager.GameOverHandler.RemoveExitEvent(mGameOverExitDeactivate[i], Deactivate);
        //    }
        //}

        //if (mGameStateEnterActivate.Length != 0)
        //{
        //    for (int i = 0; i < mGameStateEnterActivate.Length; i++)
        //    {
        //        StateManager.GameStateHandler.RemoveEnterEvent(mGameStateEnterActivate[i], Activate);
        //    }
        //}
        //if (mGameStateEnterDeactivate.Length != 0)
        //{
        //    for (int i = 0; i < mGameStateEnterDeactivate.Length; i++)
        //    {
        //        StateManager.GameStateHandler.RemoveEnterEvent(mGameStateEnterDeactivate[i], Deactivate);
        //    }
        //}
        //if (mGameStateExitActivate.Length != 0)
        //{
        //    for (int i = 0; i < mGameStateExitActivate.Length; i++)
        //    {
        //        StateManager.GameStateHandler.RemoveExitEvent(mGameStateExitActivate[i], Activate);
        //    }
        //}
        //if (mGameStateExitDeactivate.Length != 0)
        //{
        //    for (int i = 0; i < mGameStateExitDeactivate.Length; i++)
        //    {
        //        StateManager.GameStateHandler.RemoveExitEvent(mGameStateExitDeactivate[i], Deactivate);
        //    }
        //}
    }
}
