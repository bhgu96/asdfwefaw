﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HPEdgeHud : AHud
{
    [SerializeField]
    /// 인스펙터창에서 Main_FrontHP_Panel 오브젝트를 드래그 앤 드랍 시키면 됩니다.
    private MainFrontHPHud mFrontHP;

    private Image mEdgeImage;

    private void Awake()
    {
        mEdgeImage = this.transform.GetChild(0).GetComponent<Image>();
    }

    private void Start()
    {
        //_StateHandler stateHandler = CGameManager.Instance._stateManager.GetStateHandler(EStateHandler.HUD_ACTIVE_STATE);

        //stateHandler.AddHudEnterdEvent(_EHudState.Hud_Activate_All, Activate);
        //stateHandler.AddHudEnterdEvent(_EHudState.Hud_Deactivate_All, Deactivate);
    }

    public override void Activate()
    {
        //this.gameObject.SetActive(true);
    }

    public override void Deactivate()
    {
        //this.gameObject.SetActive(false);
    }

    public override void Express()
    {
        float resultPosition_X = -(mFrontHP.MaxWidth - mFrontHP.CurWidth);
        resultPosition_X = Mathf.Clamp(resultPosition_X, -mFrontHP.MaxWidth, 0);

        mEdgeImage.rectTransform.anchoredPosition = new Vector3(resultPosition_X, mEdgeImage.rectTransform.anchoredPosition.y);
    }

    public override void GetInformation()
    {

    }
}
