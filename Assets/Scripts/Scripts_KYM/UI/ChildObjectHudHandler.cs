﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ChildObjectHudHandler : AHud
{
    private Image[] mImages;

    private void Awake()
    {
        //StateManager.GamePauseHandler.AddEnterEvent(EPauseFlags.PAUSE, Deactivate);
        //StateManager.GamePauseHandler.AddEnterEvent(EPauseFlags.Play, Activate);
        //StateManager.GameOverHandler.AddEnterEvent(EGameOverFlags.GameOver, Deactivate);
        //StateManager.GameOverHandler.AddEnterEvent(EGameOverFlags.Play, Activate);
    }

    private void Start()
    {
        mImages = this.GetComponentsInChildren<Image>();
    }

    public override void Activate()
    {
        foreach(Image image in mImages)
        {
            image.enabled = true;
        }
    }

    public override void Deactivate()
    {
        foreach (Image image in mImages)
        {
            image.enabled = false;
        }
    }

    public override void Express()
    {

    }

    public override void GetInformation()
    {

    }

    private void OnDestroy()
    {
        //StateManager.GamePauseHandler.RemoveEnterEvent(EPauseFlags.PAUSE, Deactivate);
        //StateManager.GamePauseHandler.RemoveEnterEvent(EPauseFlags.Play, Activate);
        //StateManager.GameOverHandler.RemoveEnterEvent(EGameOverFlags.GameOver, Deactivate);
        //StateManager.GameOverHandler.RemoveEnterEvent(EGameOverFlags.Play, Activate);
    }
}
