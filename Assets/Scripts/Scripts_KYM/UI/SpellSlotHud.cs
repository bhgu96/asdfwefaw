﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SpellSlotHud : AHud
{
    /// <summary>
    /// 주문 인덱스 입니다.
    /// 예를들어 현재 mSpellIndex = 0 이면
    /// 0번째 스펠의 아이콘을 띄웁니다.(실제로는 첫번째 스펠이 되겠죠)
    /// 만약 스펠이 배열로 되어있다면
    /// mSpellSprite = mFairy.Spell.spellArray[mSpellIndex].sprite
    /// 이런식으로 GetImformation에서 할당을 할 것입니다.
    /// </summary>
    [SerializeField]
    private int mSpellIndex;

    [SerializeField]
    private Sprite mNullSprite; //신경 쓰지 않아도 됩니다. 아무런 주문도 등록되어 있지 않다면 투명 이미지를 띄울 것입니다.

    private Image mSpellImage; //주문 아이콘을 띄울 Image 컴포넌트
    private Sprite mSpellSprite; //나무 정령이 갖고 있는 주문의 아이콘을 가져올 Sprite 변수

    private void Awake()
    {
        mSpellImage = this.GetComponent<Image>();
    }

    public override void Activate()
    {
        //HUD 활성화 (StateManager에서 이벤트로 등록)
    }

    public override void Deactivate()
    {
        //HUD 비활성화 (StateManager에서 이벤트로 등록)
    }

    public override void Express()
    {
        mSpellImage.sprite = mSpellSprite;
    }

    /// <summary>
    /// 주문 슬롯 HUD에 들어갈 아이콘 이미지를 가져오는 함수입니다.
    /// </summary>
    public override void GetInformation()
    {
        /// 만약 나무 정령의 스펠 슬롯이 null이라면 HUD에 띄울 아이콘 이미지에 투명 이미지를 할당 합니다.
        /// Ex)
        ///if(mFairy.Spell.spellArray[mSpellIndex] != null)
        ///{
        ///  mSpellSprite = mFairy.Spell.spellArray[mSpellIndex].sprite; 
        ///  만약 mSpellIndex = 0이면, 0번째에 할당된 주문의 아이콘을 가져옵니다.
        ///}
        ///else
        ///{
        ///  mSpellSprite = mNullSprite;
        ///}
    }
}