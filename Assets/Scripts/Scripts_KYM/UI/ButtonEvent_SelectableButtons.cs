﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ButtonEvent_SelectableButtons : MonoBehaviour
{
    public AudioSource SelectAudio { get { return mSelectAudio; } set { mSelectAudio = value; } }
    public bool IsSelect { get { return mIsSelect; } private set { } }

    [SerializeField]
    private ESelectButtonType mSelectType;
    [SerializeField]
    private AudioSource mSelectAudio;
    [SerializeField]
    private Text mCheckText;
    [SerializeField]
    private Color mSelectColor;
    [SerializeField]
    private Color mDeselectColor;
    [SerializeField]
    private Image mParentImage;

    private UIButton mButton;
    private Text mText;
    private Image mImage;

    private int mInitFontSize;
    private Vector2 mInitImageSize;

    private bool mIsSelect;
    private bool mIsDeactive;

    private void Awake()
    {
        if (this.GetComponentInChildren<Text>() != null)
        {
            mText = this.GetComponentInChildren<Text>();
            mInitFontSize = mText.fontSize;
        }

        mButton = this.GetComponent<UIButton>();
        mImage = this.GetComponent<Image>();

        SelectButtonType();

        if(mSelectType == ESelectButtonType.Change_Font_Size)
        {
            DeselectChangeFontSize();
        }
    }

    private void OnEnable()
    {
        mIsDeactive = false;
    }

    private void OnDisable()
    {
        mIsDeactive = true;
        StopCoroutine(DeselectFadeText());
        StopCoroutine(SelectFadeText());
    }

    private void SelectButtonType()
    {
        switch (mSelectType)
        {
            case ESelectButtonType.Change_Font_Size:
                mButton.onSelect += SelectChangeFontSize;
                mButton.onDeselect += DeselectChangeFontSize;
                break;
            case ESelectButtonType.Change_Color:
                mButton.onSelect += SelectChangeColor;
                mButton.onDeselect += DeselectChangeColor;
                break;
            case ESelectButtonType.Change_Fade:
                mButton.onSelect += SelectFade;
                mButton.onDeselect += DeselectFade;
                break;
            case ESelectButtonType.Change_ParentColor:
                mButton.onSelect += SelectParent;
                mButton.onDeselect += DeselectParent;
                break;
        }

    }

    #region 색깔 변경 이벤트
    private void SelectChangeColor()
    {
        if (mSelectAudio != null)
        {
            mSelectAudio.Play();
        }

        mIsSelect = true;
        mImage.color = mSelectColor;
    }

    private void DeselectChangeColor()
    {
        mIsSelect = false;
        mImage.color = mDeselectColor;
    }
    #endregion

    #region 폰트 사이즈 변경 이벤트
    private void SelectChangeFontSize()
    {
        if (mSelectAudio != null)
        {
            mSelectAudio.Play();
        }

        mImage.color = new Color(1, 1, 1, 1);
        mIsSelect = true;
        StartCoroutine(BlinkText());
    }

    private void DeselectChangeFontSize()
    {
        mImage.color = new Color(1, 1, 1, 0);
        mText.color = new Color(0.5f, 0.5f, 0.5f, 1f);
        mIsSelect = false;
    }

    #endregion

    #region 회색 -> 흰색 Fade
    private void SelectFade()
    {
        if (mSelectAudio != null)
        {
            mSelectAudio.Play();
        }

        if (!mButton.IsButtonNull())
        {
            mImage.color = new Color(1f, 1f, 1f, 0f);
            mIsSelect = true;
            StartCoroutine(SelectFadeText());
        }
        else
        {
            mImage.color = new Color(1f, 1f, 1f, 0f);
            mText.color = new Color(0.5f, 0.5f, 0.5f, 1f);
        }
    }

    private void DeselectFade()
    {
        mIsSelect = false;

        if(!mIsDeactive)
        {
            StartCoroutine(DeselectFadeText());
        }       
    }

    #endregion

    #region 부모 오브젝트 색깔 변경
    private void SelectParent()
    {
        if (mSelectAudio != null)
        {
            mSelectAudio.Play();
        }

        mIsSelect = true;
        mParentImage.color = mSelectColor;
    }

    private void DeselectParent()
    {
        mIsSelect = false;
        mParentImage.color = mDeselectColor;
    }
    #endregion

    private IEnumerator DeselectFadeText()
    {
        for (float i = 1f; i >= 0.2f; i -= Time.deltaTime * 4f)
        {
            if (mIsSelect)
            {
                yield break;
            }

            mText.color = new Color(i, i, i, 1f);
            yield return null;
        }

        mText.color = new Color(0.2f, 0.2f, 0.2f, 1f);
    }

    private IEnumerator SelectFadeText()
    {
        for (float i = 0.2f; i <= 1f; i += Time.deltaTime * 4f)
        {
            if (!mIsSelect)
            {
                yield break;
            }

            mText.color = new Color(i, i, i, 1f);
            yield return null;
        }

        mText.color = new Color(1f, 1f, 1f, 1f);
    }

    private IEnumerator BlinkText()
    {
        while(mIsSelect)
        {
            for(float i = 0.5f; i <= 1f; i += Time.deltaTime)
            {
                if(!mIsSelect)
                {
                    mText.color = new Color(0.5f, 0.5f, 0.5f, 1f);
                    yield break;
                }

                mText.color = new Color(i, i, i, 1f);
                yield return null;
            }

            mText.color = new Color(1f, 1f, 1f, 1f);

            for (float i = 1f; i >= 0.5f; i -= Time.deltaTime)
            {
                if (!mIsSelect)
                {
                    mText.color = new Color(0.5f, 0.5f, 0.5f, 1f);
                    yield break;
                }

                mText.color = new Color(i, i, i, 1f);
                yield return null;
            }

            mText.color = new Color(0.5f, 0.5f, 0.5f, 1f);

            yield return new WaitForSeconds(0.5f);
        }
    }
}

public enum ESelectButtonType
{
    None = -1,
    Change_Font_Size,
    Change_Color,
    Change_Fade,
    Change_ParentColor,
    Count
}
