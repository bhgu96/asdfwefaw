﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class AHud : MonoBehaviour
{
    private void Awake()
    {
        //임시
        //CGameManager.Instance._stateManager.InitState();
    }

    public virtual void Update()
    {
        GetInformation();
        Express();
    }

    public abstract void Activate(); //HUD 활성화
    public abstract void Deactivate(); //HUD 비활성화
    public abstract void Express(); //HUD에 나타낼 Fields값을 보여줌
    public abstract void GetInformation(); //HUD에 필요할 Fields 정보를 가져옴
}
