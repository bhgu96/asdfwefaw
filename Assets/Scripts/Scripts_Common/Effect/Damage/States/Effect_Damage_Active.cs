﻿using UnityEngine;
using System.Collections;

public class Effect_Damage_Active : State<EStateType_Effect_Damage>
{
    private Effect_Damage mEffect;

    public Effect_Damage_Active(Effect_Damage effect) : base(EStateType_Effect_Damage.Active)
    {
        mEffect = effect;
    }

    public override void Start()
    {
        mEffect.Animator.SetInteger("state", (int)Type);
        mEffect.Animator.SetInteger("type", (int)mEffect.DamageType);
    }

    public override void Update()
    {
        float movement = mEffect.Speed * Time.deltaTime;
        mEffect.transform.Translate(0.0f, movement, movement, Space.World);
    }
}
