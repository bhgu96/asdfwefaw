﻿using UnityEngine;
using System.Collections;

public class Effect_Damage_Idle : State<EStateType_Effect_Damage>
{
    private Effect_Damage mEffect;

    public Effect_Damage_Idle(Effect_Damage effect) : base(EStateType_Effect_Damage.Idle)
    {
        mEffect = effect;
    }

    public override void Start()
    {
        mEffect.Animator.SetInteger("state", (int)Type);
        mEffect.gameObject.SetActive(false);
    }
}
