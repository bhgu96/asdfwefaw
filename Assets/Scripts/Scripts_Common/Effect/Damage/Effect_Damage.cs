﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public enum EStateType_Effect_Damage
{
    None = -1, 

    Idle, 
    Active, 

    Count
}

public enum EDamageType
{
    None = -1, 

    Fairy, 
    Giant, 
    Monster, 
    Critical, 

    Count
}

public class Effect_Damage : AEffect<EStateType_Effect_Damage>
{
    public override EEffectType Type => EEffectType.Damage;

    [SerializeField]
    private float mSpeed;
    public float Speed { get { return mSpeed; } }

    [SerializeField]
    private Text mText;

    private int mDamage;
    public int Damage
    {
        get { return mDamage; }
        set
        {
            if(mDamage != value)
            {
                mDamage = value;
                mText.text = mDamage.ToString();
            }
        }
    }
    /// <summary>
    /// 데미지의 타입
    /// </summary>
    public EDamageType DamageType { get; set; }

    protected override void OnActivate()
    {
        base.OnActivate();
        State = EStateType_Effect_Damage.Active;
    }

    protected override void OnDeactivate()
    {
        base.OnDeactivate();
        State = EStateType_Effect_Damage.Idle;
    }

    private Vector2 mPrevCameraPosition;

    protected override void FixedUpdate()
    {
        base.FixedUpdate();

        Vector2 curCameraPosition = CameraController.Main.transform.position;
        transform.Translate(curCameraPosition - mPrevCameraPosition);
        mPrevCameraPosition = curCameraPosition;
    }

    protected override void Awake()
    {
        base.Awake();
        SetStates(new Effect_Damage_Idle(this)
            , new Effect_Damage_Active(this));
    }

    private void Start()
    {
        mPrevCameraPosition = CameraController.Main.transform.position;
    }

    private void OnEnable()
    {
        if (CameraController.Main != null)
        {
            mPrevCameraPosition = CameraController.Main.transform.position;
        }
    }
}
