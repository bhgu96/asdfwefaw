﻿using UnityEngine;
using System;
using Object;

public enum EEffectType
{
    None = -1, 

    Damage = 0, 
    Hit_Break_00 = 1, 
    Hit_Break_01 = 2, 
    Hit_Impact_00 = 3, 
    Hit_Impact_01 = 4, 
    Hit_Circle_00 = 5, 
    Hit_Circle_01 = 6, 
    Hit_Shards_00 = 7, 
    Hit_Shards_01 = 8, 

    Count
}

public interface IEffect : IPoolable<EEffectType>
{
    /// <summary>
    /// 활성화 여부
    /// </summary>
    bool IsActivated { get; }
    /// <summary>
    /// 활성화 가능 여부
    /// </summary>
    bool CanActivate { get; }
    /// <summary>
    /// 비활성화 가능 여부
    /// </summary>
    bool CanDeactivate { get; }
    /// <summary>
    /// 활성화 메소드
    /// </summary>
    /// <returns></returns>
    bool Activate();
    /// <summary>
    /// 비활성화 메소드
    /// </summary>
    /// <returns></returns>
    bool Deactivate();
}

[RequireComponent(typeof(Animator))]
public abstract class AEffect<EState> : StateBase<EState>, IEffect
    where EState : Enum
{
    /// <summary>
    /// 이펙트 타입
    /// </summary>
    public abstract EEffectType Type { get; }
    /// <summary>
    /// 이펙트 풀링 가능 여부
    /// </summary>
    public virtual bool IsPooled { get { return !gameObject.activeInHierarchy; } }

    /// <summary>
    /// 현재 활성화 상태
    /// </summary>
    public bool IsActivated { get; private set; }

    /// <summary>
    /// 활성화 가능 여부
    /// </summary>
    public virtual bool CanActivate { get { return !gameObject.activeInHierarchy; } }

    /// <summary>
    /// 비활성화 가능 여부
    /// </summary>
    public virtual bool CanDeactivate { get { return IsActivated; } }

    /// <summary>
    /// 애니메이터
    /// </summary>
    public Animator Animator { get; private set; }

    [SerializeField]
    private Transform mMainTransform;
    /// <summary>
    /// 메인 객체
    /// </summary>
    public Transform MainTransform { get { return mMainTransform; } }

    private float mAngle;
    public float Angle
    {
        get { return mAngle; }
        set
        {
            while (value > 180.0f)
            {
                value -= 360.0f;
            }
            while (value < -180.0f)
            {
                value += 360.0f;
            }

            mAngle = value;


            if (!mIsSetDirection)
            {
                Direction = new Vector2(Mathf.Cos(mAngle * Mathf.Deg2Rad), Mathf.Sin(mAngle * Mathf.Deg2Rad));
            }
            else
            {
                mIsSetDirection = false;
            }

            // 오른쪽을 보고있는 경우
            if (mAngle >= -90.0f && mAngle <= 90.0f)
            {
                HorizontalDirection = EHorizontalDirection.Right;

                MainTransform.eulerAngles = new Vector3(0.0f, 0.0f, mAngle);
            }
            // 왼쪽을 보고있는 경우
            else
            {
                HorizontalDirection = EHorizontalDirection.Left;

                MainTransform.eulerAngles = new Vector3(0.0f, 0.0f, mAngle - 180.0f);
            }
        }
    }

    public Vector2 Direction { get; private set; }
    private bool mIsSetDirection = false;

    /// <summary>
    /// 움직인 방향(direction)에 따라 Main 객체의 각도를 조절하는 메소드
    /// </summary>
    /// <param name="direction"></param>
    public void SetDirection(Vector2 direction, bool isNormalized)
    {
        if (!isNormalized)
        {
            direction.Normalize();
        }

        Direction = direction;
        mIsSetDirection = true;

        // 윗방향으로 움직인 경우
        if (direction.y >= 0.0f)
        {
            Angle = Mathf.Acos(direction.x) * Mathf.Rad2Deg;
        }
        // 아랫 방향으로 움직인 경우
        else
        {
            Angle = -Mathf.Acos(direction.x) * Mathf.Rad2Deg;
        }
    }

    /// <summary>
    /// 현재 Effect의 수평 방향
    /// </summary>
    public EHorizontalDirection HorizontalDirection
    {
        get { return mHorizontalDirection; }
        set
        {
            // 현재 방향과 다르다면
            if (mHorizontalDirection != value)
            {
                // 기존 스케일.x 에 -1을 곱함
                Vector3 prevLocalScale = mMainTransform.localScale;
                mMainTransform.localScale = new Vector3(prevLocalScale.x * -1.0f, prevLocalScale.y, prevLocalScale.z);
                mHorizontalDirection = value;
            }
        }
    }
    private EHorizontalDirection mHorizontalDirection;


    /// <summary>
    /// 활성화 메소드
    /// </summary>
    /// <returns></returns>
    public bool Activate()
    {
        if(CanActivate)
        {
            gameObject.SetActive(true);
            IsActivated = true;
            OnActivate();
            return true;
        }
        return false;
    }
    /// <summary>
    /// 비활성화 메소드
    /// </summary>
    /// <returns></returns>
    public bool Deactivate()
    {
        if(CanDeactivate)
        {
            IsActivated = false;
            OnDeactivate();
            return true;
        }
        return false;
    }

    /// <summary>
    /// 활성화 콜백 메소드
    /// </summary>
    protected virtual void OnActivate() { }
    /// <summary>
    /// 비활성화 콜백 메소드
    /// </summary>
    protected virtual void OnDeactivate() { }

    protected override void Awake()
    {
        base.Awake();
        if(mMainTransform.lossyScale.x > 0.0f)
        {
            mHorizontalDirection = EHorizontalDirection.Right;
        }
        else
        {
            mHorizontalDirection = EHorizontalDirection.Left;
        }
        Animator = GetComponent<Animator>();
    }
}
