﻿using UnityEngine;

public class Effect_Hit_Idle : State<EStateType_Effect_Hit>
{
    private Effect_Hit mEffect;

    public Effect_Hit_Idle(Effect_Hit effect) : base(EStateType_Effect_Hit.Idle)
    {
        mEffect = effect;
    }

    public override void Start()
    {
        mEffect.Animator.SetInteger("state", (int)Type);
        mEffect.gameObject.SetActive(false);
    }
}
