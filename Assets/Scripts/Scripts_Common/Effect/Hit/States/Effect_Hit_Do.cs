﻿using UnityEngine;

public class Effect_Hit_Do : State<EStateType_Effect_Hit>
{
    private Effect_Hit mEffect;
    private Vector2 mPrevCameraPosition;

    public Effect_Hit_Do(Effect_Hit effect) : base(EStateType_Effect_Hit.Do)
    {
        mEffect = effect;
    }

    public override void Start()
    {
        mEffect.Animator.SetInteger("state", (int)Type);

        if(mEffect.IsFollowCamera)
        {
            mPrevCameraPosition = CameraController.Main.transform.position;
        }
    }

    public override void FixedUpdate()
    {
        if(mEffect.IsFollowCamera)
        {
            Vector2 curCameraPosition = CameraController.Main.transform.position;
            Vector2 curPosition = mEffect.transform.position;
            mEffect.transform.position = curPosition + (curCameraPosition - mPrevCameraPosition);
            mPrevCameraPosition = curCameraPosition;
        }
    }
}
