﻿using UnityEngine;
using System.Collections;

public enum EStateType_Effect_Hit
{
    None = -1, 

    Idle = 0, 
    Do = 1, 

    Count
}

public class Effect_Hit : AEffect<EStateType_Effect_Hit>
{
    [SerializeField]
    private EEffectType mType;
    public override EEffectType Type { get { return mType; } }

    public bool IsFollowCamera { get; set; } = false;

    protected override void OnActivate()
    {
        base.OnActivate();
        State = EStateType_Effect_Hit.Do;
    }

    protected override void OnDeactivate()
    {
        base.OnDeactivate();
        State = EStateType_Effect_Hit.Idle;
    }

    protected override void Awake()
    {
        base.Awake();

        SetStates(new Effect_Hit_Idle(this)
            , new Effect_Hit_Do(this));
    }
}
