﻿
public class Heal
{
    private int mValue;
    public int Value
    {
        get
        {
            int value = mValue;
            mValue = 0;
            return value;
        }
        set { mValue = value; }
    }
}
