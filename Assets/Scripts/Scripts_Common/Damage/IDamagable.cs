﻿public interface IDamagable
{
    /// <summary>
    /// 데미지 프로퍼티
    /// </summary>
    Damage Damage { get; }
    /// <summary>
    /// 힐 프로퍼티
    /// </summary>
    Heal Heal { get; }
    /// <summary>
    /// 데미지 활성화 메소드
    /// </summary>
    void ActivateDamage();
    /// <summary>
    /// 힐 활성화 메소드
    /// </summary>
    void ActivateHeal();
}
