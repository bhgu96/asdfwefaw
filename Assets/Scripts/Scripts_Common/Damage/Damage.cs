﻿using UnityEngine;

public enum ESrcType
{
    None = -1, 

    Fairy, 
    Giant, 
    Monster, 

    Count
}

public class Damage
{
    private int myOffense;
    public ESrcType SrcType { get; private set; }

    public void Set(int srcOffense, float srcForce, EDamageType damageType, ESrcType srcType, EEffectType effectType, Vector2 srcPosition)
    {
        myOffense = srcOffense;
        SrcForce = srcForce;
        EffectType = effectType;
        DamageType = damageType;
        SrcType = srcType;
        SrcPosition = srcPosition;
    }

    public int Get()
    {
        return myOffense;
    }

    /// <summary>
    /// 서로 반사되는 공격인가
    /// </summary>
    public bool CanReflex { get; private set; }
    /// <summary>
    /// 무시되어서는 안되는 중요한 공격인가
    /// </summary>
    public bool IsImportant { get; private set; }
    /// <summary>
    /// 치명타인가
    /// </summary>
    public bool IsCritical { get; private set; }


    /// <summary>
    /// 전달받은 공격량 (스킬 공격력 x 객체 공격력 x if 치명타 배율)
    /// </summary>
    public float SrcOffense { get; private set; }
    /// <summary>
    /// 데미지의 속성 타입
    /// </summary>
    public EElementalType ElementalType { get; private set; }
    /// <summary>
    /// 송신자의 속성 친화도
    /// </summary>
    public int SrcElementalAffinity { get; private set; }
    /// <summary>
    /// 송신자의 레벨
    /// </summary>
    public int SrcLevel { get; private set; }
    /// <summary>
    /// 송신자의 인스턴스
    /// </summary>
    public object SrcInstance { get; private set; }
    /// <summary>
    /// 송신자의 힘
    /// </summary>
    public float SrcForce { get; private set; }
    /// <summary>
    /// 공격이 가해지는 위치
    /// </summary>
    public Vector3 SrcPosition { get; private set; }

    /// <summary>
    /// 생성되는 이펙트 타입
    /// </summary>
    public EEffectType EffectType { get; private set; }
    /// <summary>
    /// 송신자 타입
    /// </summary>
    private EDamageType mDamageType;
    public EDamageType DamageType
    {
        get
        {
            if(IsCritical)
            {
                return EDamageType.Critical;
            }
            return mDamageType;
        }
        private set { mDamageType = value; }
    }

    /// <summary>
    /// 데미지와 관련된 변수를 설정하는 메소드
    /// </summary>
    /// <param name="canReflex"></param>
    /// <param name="isImportant"></param>
    /// <param name="isCritical"></param>
    /// <param name="effectType"></param>
    /// <param name="damageType"></param>
    /// <param name="elementalType"></param>
    /// <param name="level"></param>
    /// <param name="elementalAffinity"></param>
    /// <param name="offense"></param>
    /// <param name="force"></param>
    /// <param name="position"></param>
    /// <param name="instance"></param>
    public void SetDamage(bool canReflex, bool isImportant, bool isCritical, 
        EEffectType effectType, EDamageType damageType, EElementalType elementalType, 
        int level, int elementalAffinity, float offense, float force, Vector3 position, object instance)
    {
        CanReflex = canReflex;
        IsImportant = isImportant;
        IsCritical = isCritical;
        EffectType = effectType;
        DamageType = damageType;
        ElementalType = elementalType;
        SrcLevel = level;
        SrcElementalAffinity = elementalAffinity;
        SrcOffense = offense;
        SrcForce = force;
        SrcPosition = position;
        SrcInstance = instance;
    }

    /// <summary>
    /// 속성 친화도에 의한 데미지 계수
    /// </summary>
    /// <param name="dstEA"></param>
    /// <returns></returns>
    public float GetCoeficient_EA(float dstEA)
    {
        return 1.0f + (SrcElementalAffinity - dstEA + 11.0f) / 222.0f;
    }

    /// <summary>
    /// 방어력에 의한 데미지 계수
    /// </summary>
    /// <param name="dstDefense"></param>
    /// <returns></returns>
    public float GetCoefficient_Defense(float dstDefense)
    {
        return 1.0f - (dstDefense / (200.0f * SrcLevel + dstDefense));
    }
    
    /// <summary>
    /// 최종 데미지 양
    /// </summary>
    /// <param name="dstEA"></param>
    /// <param name="dstDefense"></param>
    /// <returns></returns>
    public float GetDamage(float dstEA, float dstDefense)
    {
        return SrcOffense * GetCoeficient_EA(dstEA) * GetCoefficient_Defense(dstDefense);
    }
}
