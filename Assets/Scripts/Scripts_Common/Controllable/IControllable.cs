﻿

public interface IControllable
{
    AInput Input { get; }

    void SetInputs(params AInput[] input);
    void SetIndexOfInputs(int index);
}