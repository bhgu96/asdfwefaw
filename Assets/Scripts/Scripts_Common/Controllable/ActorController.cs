﻿using UnityEngine;
using System.Collections;

public class ActorController : AInput
{
    public ActorController() : base(3)
    {

    }
    public override void Check()
    {
        if (StateManager.InputMode.State == EInputMode.Keyboard)
        {
            SetMoveHorizontal(InputManager.GetFloat(EInputType.Right, EInputMode.Keyboard)
                - InputManager.GetFloat(EInputType.Left, EInputMode.Keyboard));
            SetMoveVertical(InputManager.GetFloat(EInputType.Up, EInputMode.Keyboard)
                - InputManager.GetFloat(EInputType.Down, EInputMode.Keyboard));
            SetSkill(InputManager.GetBool(EInputType.Skill_0, EInputMode.Keyboard), 0);
            SetSkill(InputManager.GetBool(EInputType.Skill_1, EInputMode.Keyboard), 1);
            SetSkill(InputManager.GetBool(EInputType.Skill_2, EInputMode.Keyboard), 2);
            SetInteract(InputManager.GetBool(EInputType.Interact, EInputMode.Keyboard));
            SetDash(InputManager.GetBool(EInputType.Dash, EInputMode.Keyboard));

            if (InputManager.GetBool(EInputMode.GamePad))
            {
                StateManager.InputMode.State = EInputMode.GamePad;
                Cursor.visible = false;
            }
        }
        else if (StateManager.InputMode.State == EInputMode.GamePad)
        {
            SetMoveHorizontal(InputManager.GetFloat(EInputType.Right, EInputMode.GamePad)
                - InputManager.GetFloat(EInputType.Left, EInputMode.GamePad));
            SetMoveVertical(InputManager.GetFloat(EInputType.Up, EInputMode.GamePad)
                - InputManager.GetFloat(EInputType.Down, EInputMode.GamePad));
            SetSkill(InputManager.GetBool(EInputType.Skill_0, EInputMode.GamePad), 0);
            SetSkill(InputManager.GetBool(EInputType.Skill_1, EInputMode.GamePad), 1);
            SetSkill(InputManager.GetBool(EInputType.Skill_2, EInputMode.GamePad), 2);
            SetInteract(InputManager.GetBool(EInputType.Interact, EInputMode.GamePad));
            SetDash(InputManager.GetBool(EInputType.Dash, EInputMode.GamePad));

            if (InputManager.GetBool(EInputMode.Keyboard))
            {
                StateManager.InputMode.State = EInputMode.Keyboard;
                Cursor.visible = true;
            }
        }
    }
}
