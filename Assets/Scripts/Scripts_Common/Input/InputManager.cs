﻿using UnityEngine;
using System;
using System.Collections.Generic;
using Csv;

public enum EInputMode
{
    None = -1,

    Keyboard,
    GamePad,

    Count
}

public enum EInputType
{
    None = -1,
    // 이동
    Left,
    Right,
    Up,
    Down,
    // 상호작용
    Interact,
    // 대시
    Dash, 

    // 스킬_0
    Skill_0,
    // 스킬_1
    Skill_1,
    // 스킬_2
    Skill_2,

    // 메뉴 이동
    Left_Menu,
    Right_Menu,
    Up_Menu,
    Down_Menu,
    // 확인
    Confirm,
    // 취소
    Cancel,
    // 탈출
    Escape,
    // 시스템 메뉴
    Menu_System,

    // GamePad 전용
    Left_Aim,
    Right_Aim,
    Up_Aim,
    Down_Aim, 

    Count
}

public enum EAxisCode
{
    None = -1,

    GamePad_LeftStick_Right,
    GamePad_LeftStick_Left,
    GamePad_LeftStick_Up,
    GamePad_LeftStick_Down,
    GamePad_RightTrigger,
    GamePad_LeftTrigger,
    GamePad_Arrow_Right,
    GamePad_Arrow_Left,
    GamePad_Arrow_Up,
    GamePad_Arrow_Down,
    GamePad_RightStick_Right,
    GamePad_RightStick_Left,
    GamePad_RightStick_Up,
    GamePad_RightStick_Down,

    Count
}

public static class InputManager
{
    // 기본 Input 정보
    private static Dictionary<EInputType, Dictionary<EInputMode, AInputUnit>> mBaseInputDict;

    // InputFlag와 입력 모듈을 연결한 Dictionary
    private static Dictionary<EInputType, Dictionary<EInputMode, AInputUnit>> mInputDict;

    static InputManager()
    {
        // 기본 Input 키 불러오기
        InitializeBaseInputUnits();

        mInputDict = new Dictionary<EInputType, Dictionary<EInputMode, AInputUnit>>();
        // 저장된 입력 파일 불러오기
        Dictionary<string, Dictionary<string, string>> stringDict = CsvReader.Read(Application.streamingAssetsPath + "/Input.csv");

        // 모든 입력 타입에 대해
        for (EInputType type = EInputType.None + 1; type < EInputType.Count; type++)
        {
            Dictionary<EInputMode, AInputUnit> unitDict = new Dictionary<EInputMode, AInputUnit>();
            mInputDict.Add(type, unitDict);

            string typeString = type.ToString();
            // 읽어온 입력 파일에 해당 타입이 존재한다면
            if (stringDict.ContainsKey(typeString))
            {
                Dictionary<string, string> subStringDict = stringDict[typeString];
                // 모든 입력 모드에 대해
                for (EInputMode mode = EInputMode.None + 1; mode < EInputMode.Count; mode++)
                {
                    string modeString = mode.ToString();
                    // 읽어온 입력 파일에 해당 입력 모드가 존재한다면
                    if (subStringDict.ContainsKey(modeString))
                    {
                        string inputString = subStringDict[modeString];
                        KeyCode keyCode;
                        EAxisCode axisCode;
                        // 읽어온 입력 값이 KeyCode라면
                        if (Enum.TryParse<KeyCode>(inputString, out keyCode))
                        {
                            unitDict.Add(mode, new KeyUnit(keyCode));
                        } // 읽어온 입력 값이 EAxisCode라면
                        else if (Enum.TryParse<EAxisCode>(inputString, out axisCode))
                        {
                            unitDict.Add(mode, new AxisUnit(axisCode));
                        } // 읽어온 입력 값이 KeyCode도 아니고, EAxisCode도 아니라면
                        else
                        {
                            // 기본 입력값으로 할당
                            unitDict.Add(mode, mBaseInputDict[type][mode]);
                        }
                    } // 읽어온 입력 파일에 해당 입력 모드가 존재하지 않는다면
                    else
                    {
                        // 기본 입력값으로 할당
                        unitDict.Add(mode, mBaseInputDict[type][mode]);
                    }
                }
            } // 읽어온 입력 파일에 해당 타입이 존재하지 않는다면
            else
            {
                // 모든 입력 모드에 대해
                for (EInputMode mode = EInputMode.None + 1; mode < EInputMode.Count; mode++)
                {
                    // 기본 입력값으로 할당
                    unitDict.Add(mode, mBaseInputDict[type][mode]);
                }
            }
        }
        // 입력 값을 저장한다.
        SaveInput();
    }

    /// <summary>
    /// InputFlag 에 대해 InputMode 의 입력값을 Boolean 형태로 반환하는 메소드
    /// </summary>
    /// <param name="inputFlag"></param>
    /// <param name="inputModeFlag"></param>
    /// <returns></returns>
    public static bool GetBool(EInputType inputFlag, EInputMode inputModeFlag)
    {
        return mInputDict[inputFlag][inputModeFlag].BoolValue;
    }
    /// <summary>
    /// Input Mode에 대해 모든 Input Flag의 입력값을 OR 연산을 통해 Boolean 형태로 반환하는 메소드
    /// </summary>
    /// <param name="inputModeFlag"></param>
    /// <returns></returns>
    public static bool GetBool(EInputMode inputModeFlag)
    {
        for (EInputType inputFlag = EInputType.None + 1; inputFlag < EInputType.Count; inputFlag++)
        {
            if (mInputDict[inputFlag][inputModeFlag].BoolValue)
            {
                return true;
            }
        }
        return false;
    }
    /// <summary>
    /// Input Flag에 대해 모든 Input Mode의 입력값을 OR 연산을 통해 Boolean 형태로 반환하는 메소드
    /// </summary>
    /// <param name="inputFlag"></param>
    /// <returns></returns>
    public static bool GetBool(EInputType inputFlag)
    {
        for (EInputMode mode = EInputMode.None + 1; mode < EInputMode.Count; mode++)
        {
            if (mInputDict[inputFlag][mode].BoolValue)
            {
                return true;
            }
        }
        return false;
    }
    /// <summary>
    /// Input Flag에 대해 Input Mode의 입력값을 Float 형태로 반환하는 메소드
    /// </summary>
    /// <param name="inputFlag"></param>
    /// <param name="inputModeFlag"></param>
    /// <returns></returns>
    public static float GetFloat(EInputType inputFlag, EInputMode inputModeFlag)
    {
        return mInputDict[inputFlag][inputModeFlag].FloatValue;
    }
    /// <summary>
    /// Input Flag에 대해 모든 입력값을 Float 형태로 반환하는 메소드
    /// </summary>
    /// <param name="inputFlag"></param>
    /// <returns></returns>
    public static float GetFloat(EInputType inputFlag)
    {
        for (EInputMode mode = EInputMode.None + 1; mode < EInputMode.Count; mode++)
        {
            float value = mInputDict[inputFlag][mode].FloatValue;
            if (value > 0.0f)
            {
                return value;
            }
        }
        return 0.0f;
    }
    /// <summary>
    /// Input Mode에 대해 모든 입력값을 Float 형태로 반환하는 메소드
    /// </summary>
    /// <param name="inputModeFlag"></param>
    /// <returns></returns>
    public static float GetFloat(EInputMode inputModeFlag)
    {
        for (EInputType inputFlag = EInputType.None + 1; inputFlag < EInputType.Count; inputFlag++)
        {
            float value = mInputDict[inputFlag][inputModeFlag].FloatValue;
            if (value > 0.0f)
            {
                return value;
            }
        }
        return 0.0f;
    }

    /// <summary>
    /// 지정된 Input에 대한 입력값을 전달된 KeyCode로 변경하는 메소드
    /// </summary>
    /// <param name="inputFlag"></param>
    /// <param name="inputModeFlag"></param>
    /// <param name="keyCode"></param>
    public static void SetKey(EInputType inputFlag, EInputMode inputModeFlag, KeyCode keyCode)
    {
        mInputDict[inputFlag][inputModeFlag] = new KeyUnit(keyCode);
        SaveInput();
    }
    /// <summary>
    /// 지정된 Input에 대한 입력값을 전달된 Axis String으로 변경하는 메소드
    /// </summary>
    /// <param name="inputFlag"></param>
    /// <param name="inputModeFlag"></param>
    /// <param name="axisString"></param>
    public static void SetAxis(EInputType inputFlag, EInputMode inputModeFlag, EAxisCode axisCode)
    {
        mInputDict[inputFlag][inputModeFlag] = new AxisUnit(axisCode);
        SaveInput();
    }
    /// <summary>
    /// 입력된 Input의 KeyCode를 반환하는 메소드
    /// 키 설정시 None을 반환할때까지 기다린 후 설정해야 한다.
    /// </summary>
    /// <returns></returns>
    public static KeyCode GetAnyKey()
    {
        for (KeyCode code = KeyCode.None + 1; code <= KeyCode.JoystickButton19; code++)
        {
            if (Input.GetKey(code))
            {
                return code;
            }
        }
        return KeyCode.None;
    }
    /// <summary>
    /// 입력된 Input의 Axis를 반환하는 메소드
    /// 키 설정시 None을 반환할때까지 기다린 후 설정해야 한다.
    /// </summary>
    /// <returns></returns>
    public static EAxisCode GetAnyAxis()
    {
        for (EAxisCode code = EAxisCode.None + 1; code < EAxisCode.Count; code++)
        {
            if (Input.GetAxisRaw(code.ToString()) > 0.0f)
            {
                return code;
            }
        }
        return EAxisCode.None;
    }

    /// <summary>
    /// 현재 설정된 입력 값들을 파일로 저장하는 메소드
    /// </summary>
    private static void SaveInput()
    {
        Dictionary<string, Dictionary<string, string>> stringDict = new Dictionary<string, Dictionary<string, string>>();

        for (EInputType type = EInputType.None + 1; type < EInputType.Count; type++)
        {
            Dictionary<string, string> subStringDict = new Dictionary<string, string>();
            stringDict.Add(type.ToString(), subStringDict);

            for (EInputMode mode = EInputMode.None + 1; mode < EInputMode.Count; mode++)
            {
                subStringDict.Add(mode.ToString(), mInputDict[type][mode].ToString());
            }
        }

        CsvWriter.Write(stringDict, Application.streamingAssetsPath + "/Input.csv");
    }

    /// <summary>
    /// 기본 입력값을 할당하는 메소드
    /// </summary>
    private static void InitializeBaseInputUnits()
    {
        mBaseInputDict = new Dictionary<EInputType, Dictionary<EInputMode, AInputUnit>>();
        Dictionary<EInputMode, AInputUnit> inputUnitDict = new Dictionary<EInputMode, AInputUnit>();
        inputUnitDict.Add(EInputMode.Keyboard, new KeyUnit(KeyCode.D));
        inputUnitDict.Add(EInputMode.GamePad, new AxisUnit(EAxisCode.GamePad_LeftStick_Right));
        mBaseInputDict.Add(EInputType.Right, inputUnitDict);

        inputUnitDict = new Dictionary<EInputMode, AInputUnit>();
        inputUnitDict.Add(EInputMode.Keyboard, new KeyUnit(KeyCode.A));
        inputUnitDict.Add(EInputMode.GamePad, new AxisUnit(EAxisCode.GamePad_LeftStick_Left));
        mBaseInputDict.Add(EInputType.Left, inputUnitDict);

        inputUnitDict = new Dictionary<EInputMode, AInputUnit>();
        inputUnitDict.Add(EInputMode.Keyboard, new KeyUnit(KeyCode.W));
        inputUnitDict.Add(EInputMode.GamePad, new AxisUnit(EAxisCode.GamePad_LeftStick_Up));
        mBaseInputDict.Add(EInputType.Up, inputUnitDict);

        inputUnitDict = new Dictionary<EInputMode, AInputUnit>();
        inputUnitDict.Add(EInputMode.Keyboard, new KeyUnit(KeyCode.S));
        inputUnitDict.Add(EInputMode.GamePad, new AxisUnit(EAxisCode.GamePad_LeftStick_Down));
        mBaseInputDict.Add(EInputType.Down, inputUnitDict);

        inputUnitDict = new Dictionary<EInputMode, AInputUnit>();
        inputUnitDict.Add(EInputMode.Keyboard, new KeyUnit(KeyCode.Mouse0));
        inputUnitDict.Add(EInputMode.GamePad, new KeyUnit(KeyCode.JoystickButton5));
        mBaseInputDict.Add(EInputType.Skill_0, inputUnitDict);

        inputUnitDict = new Dictionary<EInputMode, AInputUnit>();
        inputUnitDict.Add(EInputMode.Keyboard, new KeyUnit(KeyCode.Mouse1));
        inputUnitDict.Add(EInputMode.GamePad, new AxisUnit(EAxisCode.GamePad_RightTrigger));
        mBaseInputDict.Add(EInputType.Skill_1, inputUnitDict);

        inputUnitDict = new Dictionary<EInputMode, AInputUnit>();
        inputUnitDict.Add(EInputMode.Keyboard, new KeyUnit(KeyCode.Space));
        inputUnitDict.Add(EInputMode.GamePad, new AxisUnit(EAxisCode.GamePad_LeftTrigger));
        mBaseInputDict.Add(EInputType.Skill_2, inputUnitDict);

        inputUnitDict = new Dictionary<EInputMode, AInputUnit>();
        inputUnitDict.Add(EInputMode.Keyboard, new KeyUnit(KeyCode.E));
        inputUnitDict.Add(EInputMode.GamePad, new KeyUnit(KeyCode.JoystickButton0));
        mBaseInputDict.Add(EInputType.Interact, inputUnitDict);

        inputUnitDict = new Dictionary<EInputMode, AInputUnit>();
        inputUnitDict.Add(EInputMode.Keyboard, new KeyUnit(KeyCode.LeftShift));
        inputUnitDict.Add(EInputMode.GamePad, new KeyUnit(KeyCode.JoystickButton4));
        mBaseInputDict.Add(EInputType.Dash, inputUnitDict);

        inputUnitDict = new Dictionary<EInputMode, AInputUnit>();
        inputUnitDict.Add(EInputMode.Keyboard, new KeyUnit(KeyCode.D));
        inputUnitDict.Add(EInputMode.GamePad, new AxisUnit(EAxisCode.GamePad_Arrow_Right));
        mBaseInputDict.Add(EInputType.Right_Menu, inputUnitDict);

        inputUnitDict = new Dictionary<EInputMode, AInputUnit>();
        inputUnitDict.Add(EInputMode.Keyboard, new KeyUnit(KeyCode.A));
        inputUnitDict.Add(EInputMode.GamePad, new AxisUnit(EAxisCode.GamePad_Arrow_Left));
        mBaseInputDict.Add(EInputType.Left_Menu, inputUnitDict);

        inputUnitDict = new Dictionary<EInputMode, AInputUnit>();
        inputUnitDict.Add(EInputMode.Keyboard, new KeyUnit(KeyCode.W));
        inputUnitDict.Add(EInputMode.GamePad, new AxisUnit(EAxisCode.GamePad_Arrow_Up));
        mBaseInputDict.Add(EInputType.Up_Menu, inputUnitDict);

        inputUnitDict = new Dictionary<EInputMode, AInputUnit>();
        inputUnitDict.Add(EInputMode.Keyboard, new KeyUnit(KeyCode.S));
        inputUnitDict.Add(EInputMode.GamePad, new AxisUnit(EAxisCode.GamePad_Arrow_Down));
        mBaseInputDict.Add(EInputType.Down_Menu, inputUnitDict);

        inputUnitDict = new Dictionary<EInputMode, AInputUnit>();
        inputUnitDict.Add(EInputMode.Keyboard, new KeyUnit(KeyCode.Return));
        inputUnitDict.Add(EInputMode.GamePad, new KeyUnit(KeyCode.JoystickButton0));
        mBaseInputDict.Add(EInputType.Confirm, inputUnitDict);

        inputUnitDict = new Dictionary<EInputMode, AInputUnit>();
        inputUnitDict.Add(EInputMode.Keyboard, new KeyUnit(KeyCode.Escape));
        inputUnitDict.Add(EInputMode.GamePad, new KeyUnit(KeyCode.JoystickButton1));
        mBaseInputDict.Add(EInputType.Cancel, inputUnitDict);
        
        inputUnitDict = new Dictionary<EInputMode, AInputUnit>();
        inputUnitDict.Add(EInputMode.Keyboard, new KeyUnit(KeyCode.Escape));
        inputUnitDict.Add(EInputMode.GamePad, new KeyUnit(KeyCode.None));
        mBaseInputDict.Add(EInputType.Escape, inputUnitDict);

        inputUnitDict = new Dictionary<EInputMode, AInputUnit>();
        inputUnitDict.Add(EInputMode.Keyboard, new KeyUnit(KeyCode.Tab));
        inputUnitDict.Add(EInputMode.GamePad, new KeyUnit(KeyCode.JoystickButton6));
        mBaseInputDict.Add(EInputType.Menu_System, inputUnitDict);

        inputUnitDict = new Dictionary<EInputMode, AInputUnit>();
        inputUnitDict.Add(EInputMode.Keyboard, new KeyUnit(KeyCode.None));
        inputUnitDict.Add(EInputMode.GamePad, new AxisUnit(EAxisCode.GamePad_RightStick_Right));
        mBaseInputDict.Add(EInputType.Right_Aim, inputUnitDict);

        inputUnitDict = new Dictionary<EInputMode, AInputUnit>();
        inputUnitDict.Add(EInputMode.Keyboard, new KeyUnit(KeyCode.None));
        inputUnitDict.Add(EInputMode.GamePad, new AxisUnit(EAxisCode.GamePad_RightStick_Left));
        mBaseInputDict.Add(EInputType.Left_Aim, inputUnitDict);

        inputUnitDict = new Dictionary<EInputMode, AInputUnit>();
        inputUnitDict.Add(EInputMode.Keyboard, new KeyUnit(KeyCode.None));
        inputUnitDict.Add(EInputMode.GamePad, new AxisUnit(EAxisCode.GamePad_RightStick_Up));
        mBaseInputDict.Add(EInputType.Up_Aim, inputUnitDict);

        inputUnitDict = new Dictionary<EInputMode, AInputUnit>();
        inputUnitDict.Add(EInputMode.Keyboard, new KeyUnit(KeyCode.None));
        inputUnitDict.Add(EInputMode.GamePad, new AxisUnit(EAxisCode.GamePad_RightStick_Down));
        mBaseInputDict.Add(EInputType.Down_Aim, inputUnitDict);



    }
}
