﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class TouchTest : MonoBehaviour
{
    private Text mText;

    public void Awake()
    {
        mText = GetComponent<Text>();

    }

    // Update is called once per frame
    public void Update()
    {
        mText.text = "";
        for (int indexOfTouch = 0; indexOfTouch < Input.touchCount; indexOfTouch++)
        {
            Touch touch = Input.GetTouch(indexOfTouch);

            mText.text += string.Format("id : {0}\n position : {1}\n phase : {2}\n", touch.fingerId, touch.position, touch.phase);
        }
    }
}