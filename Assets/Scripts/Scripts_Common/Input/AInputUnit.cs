﻿using UnityEngine;

// 연결된 Input으로 부터 Bool값과 Float 값을 반환할 수 있는 추상 클래스
public abstract class AInputUnit
{
    public abstract bool BoolValue { get; }
    public abstract float FloatValue { get; }
}
// True와 False를 반환하는 Button으로부터 Bool값, Float값을 받아오는 클래스
public class KeyUnit : AInputUnit
{
    private KeyCode mKeyCode;

    public override bool BoolValue
    {
        get
        {
            return Input.GetKey(mKeyCode);
        }
    }
    public override float FloatValue
    {
        get
        {
            if (Input.GetKey(mKeyCode))
            {
                return 1.0f;
            }
            return 0.0f;
        }
    }

    public KeyUnit(KeyCode keyCode)
    {
        mKeyCode = keyCode;
    }

    public override string ToString()
    {
        return mKeyCode.ToString();
    }
}
// -1.0 ~ 1.0 범위의 값을 가지는 Axis로부터 Bool값, Float값을 받아오는 클래스
public class AxisUnit : AInputUnit
{
    private string mAxisCode;

    public override bool BoolValue
    {
        get
        {
            if (Input.GetAxisRaw(mAxisCode) > 0.0f)
            {
                return true;
            }
            return false;
        }
    }
    public override float FloatValue
    {
        get
        {
            float value = Input.GetAxisRaw(mAxisCode);
            if(value > 0.0f)
            {
                return value;
            }
            return 0.0f;
        }
    }

    public AxisUnit(EAxisCode axisCode)
    {
        mAxisCode = axisCode.ToString();
    }

    public override string ToString()
    {
        return mAxisCode;
    }
}
