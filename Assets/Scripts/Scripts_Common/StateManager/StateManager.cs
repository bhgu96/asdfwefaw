﻿using UnityEngine;
using System;
using System.Collections.Generic;
using Csv;

public static class StateManager
{
    /// <summary>
    /// 입력 모드 설정
    /// </summary>
    public static StateHandler<EInputMode> InputMode { get; private set; }
    /// <summary>
    /// 언어 설정
    /// </summary>
    public static StateHandler<ELanguageType> Language { get; private set; }
    /// <summary>
    /// 일시정지 상태
    /// </summary>
    public static StateHandler<bool> Pause { get; private set; }
    /// <summary>
    /// 게임오버 상태
    /// </summary>
    public static StateHandler<bool> GameOver { get; private set; }
    public static StateHandler<bool> GameClear { get; private set; }
    public static StateHandler<EOuterGameFlags> OuterGame { get; private set; }
    public static StateHandler<EInnerGameFlags> InnerGame { get; private set; }

    static StateManager()
    {
        Pause = new StateHandler<bool>();
        Language = new StateHandler<ELanguageType>();
        GameOver = new StateHandler<bool>();
        GameClear = new StateHandler<bool>();
        InputMode = new StateHandler<EInputMode>();
        OuterGame = new StateHandler<EOuterGameFlags>();
        InnerGame = new StateHandler<EInnerGameFlags>();

        Language.State = ELanguageType.Korean;
        //Dictionary<string, Dictionary<string, string>> stateDict = CsvReader.Read(Application.streamingAssetsPath + "/State.csv");
        //ELanguageType language;
        //if(Enum.TryParse(stateDict["Language"]["State"], out language))
        //{
        //    Language.State = language;
        //}
        //else
        //{
        //    Language.State = ELanguageType.Korean;
        //}
    }
}

public class StateHandler<E>
{
    /// <summary>
    /// 이벤트 함수 원형
    /// </summary>
    public delegate void Action();

    /// <summary>
    /// 상태 프로퍼티
    /// </summary>
    public E State
    {
        get { return mCurState; }
        set
        {
            // 기존의 상태값과 다르다면
            if(!mComparer.Equals(value, mCurState))
            {
                // 기존의 상태값 저장
                PrevState = mCurState;
                // 현재 상태값 변경
                mCurState = value;

                // 이전 상태값이 이벤트 객체로 등록되어 있다면
                if(mEventDict.ContainsKey(PrevState))
                {
                    mEventDict[PrevState].Exit();
                }
                // 변경된 상태값이 이벤트 객체로 등록되어 있다면
                if(mEventDict.ContainsKey(mCurState))
                {
                    mEventDict[mCurState].Enter();
                }
            }
        }
    }
    /// <summary>
    /// 이전 상태 프로퍼티
    /// </summary>
    public E PrevState { get; private set; }

    private E mCurState;
    private Dictionary<E, Event> mEventDict;

    private EqualityComparer<E> mComparer;

    public StateHandler()
    {
        mEventDict = new Dictionary<E, Event>();
        mComparer = EqualityComparer<E>.Default;
    }

    /// <summary>
    /// State 진입 이벤트 추가
    /// </summary>
    /// <param name="e"></param>
    /// <param name="action"></param>
    public void AddEnterEvent(E e, Action action)
    {
        if (!mEventDict.ContainsKey(e))
        {
            mEventDict.Add(e, new Event());
        }

        mEventDict[e].OnEnter += action;
    }
    /// <summary>
    /// State 탈출 이벤트 추가
    /// </summary>
    /// <param name="e"></param>
    /// <param name="action"></param>
    public void AddExitEvent(E e, Action action)
    {
        if (!mEventDict.ContainsKey(e))
        {
            mEventDict.Add(e, new Event());
        }

        mEventDict[e].OnExit += action;
    }
    /// <summary>
    /// State 진입 이벤트 제거
    /// </summary>
    /// <param name="e"></param>
    /// <param name="action"></param>
    public void RemoveEnterEvent(E e, Action action)
    {
        mEventDict[e].OnEnter -= action;
    }
    /// <summary>
    /// State 탈출 이벤트 제거
    /// </summary>
    /// <param name="e"></param>
    /// <param name="action"></param>
    public void RemoveExitEvent(E e, Action action)
    {
        mEventDict[e].OnExit -= action;
    }

    /// <summary>
    /// 이벤트 메소드 관리 클래스
    /// </summary>
    private class Event
    {
        public event Action OnEnter;
        public event Action OnExit;

        public void Enter()
        {
            OnEnter?.Invoke();
        }
        public void Exit()
        {
            OnExit?.Invoke();
        }
    }
}
