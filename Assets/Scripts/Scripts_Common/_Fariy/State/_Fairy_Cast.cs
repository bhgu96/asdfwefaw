﻿using UnityEngine;

public class _Fairy_Cast : State<_EStateType_Fairy>
{
    private _Fairy mFairy;

    private bool mIsStarted = false;

    private float mCoolDown;

    public _Fairy_Cast(_Fairy fairy) : base(_EStateType_Fairy.Cast)
    {
        mFairy = fairy;
    }

    public override void Start()
    {
        if (!mIsStarted)
        {
            mIsStarted = true;
            mFairy.Animator.SetInteger("state", (int)Type);
            mFairy.Angle = 0.0f;
        }
        else
        {
            mFairy.Animator.SetTrigger("cast");
        }

        Projectile_FairyAttack projectile = ProjectilePool.Get(EProjectileType.FairyAttack) as Projectile_FairyAttack;

        if(projectile != null)
        {
            projectile.Offense = mFairy.Offense;
            projectile.Force = mFairy.Force;
            projectile.SrcObject = mFairy.gameObject;

            Vector2 position = CameraController.Main.GetCamera(ECameraType.Object).ScreenToWorldPoint(Input.mousePosition);
            projectile.transform.position = position;

            projectile.Activate();
        }

        mCoolDown = 0.1f;
    }

    public override void End()
    {
        mIsStarted = false;
    }

    public override void Update()
    {
        if(mCoolDown > 0.0f)
        {
            mCoolDown -= Time.deltaTime;
        }
        else if(mFairy.Input.SkillDown[0] || Input.GetKeyDown(KeyCode.Z))
        {
            mFairy.State = _EStateType_Fairy.Cast;
            return;
        }
    }
}
