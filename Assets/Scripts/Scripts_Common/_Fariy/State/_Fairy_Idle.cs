﻿using UnityEngine;

public class _Fairy_Idle : State<_EStateType_Fairy>
{
    private _Fairy mFairy;


    public _Fairy_Idle(_Fairy fairy) : base(_EStateType_Fairy.Idle)
    {
        mFairy = fairy;
    }

    public override void Start()
    {
        mFairy.Animator.SetInteger("state", (int)Type);
    }

    public override void Update()
    {
        if(mFairy.Input.SkillDown[0] || Input.GetKeyDown(KeyCode.Z))
        {
            mFairy.State = _EStateType_Fairy.Cast;
            return;
        }
    }

    public override void FixedUpdate()
    {
        Vector2 curPosition = mFairy.transform.position;
        Vector2 prevPosition = mFairy.PrevPosition;

        Vector2 diff = curPosition - prevPosition;

        if(Mathf.Abs(diff.x) > GameConstant.MOVEMENT_THRESHOLD + Mathf.Epsilon
            || Mathf.Abs(diff.y) > GameConstant.MOVEMENT_THRESHOLD + Mathf.Epsilon)
        {
            mFairy.State = _EStateType_Fairy.Move;
            return;
        }
    }
}
