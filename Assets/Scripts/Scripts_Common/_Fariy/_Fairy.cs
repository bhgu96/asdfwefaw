﻿using UnityEngine;
using System.Collections;

public enum _EStateType_Fairy
{
    None = -1, 

    Idle = 0, 
    Move = 1, 

    Cast = 2, 

    Count
}

[RequireComponent(typeof(Animator))]
public class _Fairy : AControllable<_EStateType_Fairy>
{
    public static _Fairy Main { get; private set; } = null;

    public Animator Animator { get; private set; }

    [SerializeField]
    private Transform mMainTransform;
    public Transform MainTransform { get { return mMainTransform; } }

    private EHorizontalDirection mHorizontalDirection;
    public EHorizontalDirection HorizontalDirection
    {
        get { return mHorizontalDirection; }
        set
        {
            if (mHorizontalDirection != value)
            {
                Vector3 localScale = mMainTransform.localScale;
                mMainTransform.localScale = new Vector3(localScale.x * -1.0f, localScale.y, localScale.z);
                mHorizontalDirection = value;
            }
        }
    }

    [SerializeField]
    private int mOffense;
    public int Offense { get { return mOffense; } }

    [SerializeField]
    private float mForce;
    public float Force { get { return mForce; } }


    private bool mIsSetDirection = false;
    private float mAngle;
    public float Angle
    {
        get { return mAngle; }
        set
        {
            while (value > 180.0f)
            {
                value -= 360.0f;
            }
            while (value < -180.0f)
            {
                value += 360.0f;
            }

            mAngle = value;

            if (!mIsSetDirection)
            {
                Direction = new Vector2(Mathf.Cos(mAngle * Mathf.Deg2Rad), Mathf.Sin(mAngle * Mathf.Deg2Rad));
            }
            else
            {
                mIsSetDirection = false;
            }

            // 오른쪽을 보고있는 경우
            if (mAngle >= -90.0f && mAngle <= 90.0f)
            {
                HorizontalDirection = EHorizontalDirection.Right;

                MainTransform.eulerAngles = new Vector3(0.0f, 0.0f, mAngle);
            }
            // 왼쪽을 보고있는 경우
            else
            {
                HorizontalDirection = EHorizontalDirection.Left;

                MainTransform.eulerAngles = new Vector3(0.0f, 0.0f, mAngle - 180.0f);
            }
        }
    }

    public Vector2 Direction { get; private set; }

    public void SetDirection(Vector2 direction, bool isNormalized)
    {
        if (!isNormalized)
        {
            direction.Normalize();
        }

        Direction = direction;
        mIsSetDirection = true;

        // 위 방향으로 움직이는 경우
        if (direction.y >= 0.0f)
        {
            Angle = Mathf.Acos(direction.x) * Mathf.Rad2Deg;
        }
        // 아래 방향으로 움직이는 경우
        else
        {
            Angle = -Mathf.Acos(direction.x) * Mathf.Rad2Deg;
        }
    }

    public Vector2 PrevPosition { get; private set; }

    protected override void Awake()
    {
        base.Awake();

        Animator = GetComponent<Animator>();

        SetInputs(new _FairyInput());

        SetStates(new _Fairy_Idle(this)
            , new _Fairy_Move(this)
            , new _Fairy_Cast(this));
    }

    protected override void FixedUpdate()
    {
        if (!StateManager.Pause.State)
        {
            base.FixedUpdate();

            PrevPosition = transform.position;
        }
    }

    private void OnEnable()
    {
        if (gameObject.tag.Equals(GameTags.Player))
        {
            Main = this;
        }
    }

    private void OnDisable()
    {
        if (gameObject.tag.Equals(GameTags.Player))
        {
            Main = null;
        }
    }
}
