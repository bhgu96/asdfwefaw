﻿using UnityEngine;
using System.Collections;

public class _FairyInput : AInput
{
    public _FairyInput() : base(2)
    {

    }
    public override void Check()
    {
        if (StateManager.InputMode.State == EInputMode.Keyboard)
        {
            SetSkill(InputManager.GetBool(EInputType.Skill_0, EInputMode.Keyboard), 0);
            SetSkill(InputManager.GetBool(EInputType.Skill_1, EInputMode.Keyboard), 1);

            if (InputManager.GetBool(EInputMode.GamePad))
            {
                StateManager.InputMode.State = EInputMode.GamePad;
                Cursor.visible = false;
            }
        }
        else if (StateManager.InputMode.State == EInputMode.GamePad)
        {
            SetSkill(InputManager.GetBool(EInputType.Skill_0, EInputMode.GamePad), 0);
            SetSkill(InputManager.GetBool(EInputType.Skill_1, EInputMode.GamePad), 1);

            if (InputManager.GetBool(EInputMode.Keyboard))
            {
                StateManager.InputMode.State = EInputMode.Keyboard;
                Cursor.visible = true;
            }
        }
    }
}
