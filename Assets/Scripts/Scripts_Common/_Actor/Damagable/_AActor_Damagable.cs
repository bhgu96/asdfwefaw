﻿using UnityEngine;

public interface _IActor_Damagable
    : _IActor, IDamagable
{
    int MaxHp { get; }
    int Hp { get; set; }
    int MaxShield { get; }
    int Shield { get; set; }
}

public abstract class _AActor_Damagable<EState>
    : _AActor<EState>, _IActor_Damagable
    where EState : System.Enum
{
    [SerializeField]
    private int mMaxHp;
    public int MaxHp { get { return mMaxHp; } }

    [SerializeField]
    private int mHp;
    public int Hp
    {
        get { return mHp; }
        set
        {
            if (value > MaxHp)
            {
                value = MaxHp;
            }
            else if (value < 0)
            {
                value = 0;
            }
            mHp = value;
        }
    }

    [SerializeField]
    private int mMaxShield;
    public virtual int MaxShield_Increment { get; }
    public int MaxShield { get { return mMaxShield + MaxShield_Increment; } }

    [SerializeField]
    private int mShield;
    
    public int Shield
    {
        get { return mShield; }
        set
        {
            if(value > MaxShield)
            {
                value = MaxShield;
            }
            else if(value < 0)
            {
                value = 0;
            }

            mShield = value;
        }
    }

    [SerializeField]
    private float mWeight;
    public float Weight { get { return mWeight; } }

    public Damage Damage { get; private set; }
    public Heal Heal { get; private set; }

    private float mInvincibleRemainTime;

    public void ActivateDamage()
    {
        if (!Damage.IsImportant && mInvincibleRemainTime > 0.0f)
        {
            return;
        }

        int damage = Damage.Get();

        if (Damage.SrcType == ESrcType.Monster)
        {
            if (Shield > 0)
            {
                if (Shield >= damage)
                {
                    Shield -= damage;
                    damage = 0;
                }
                else
                {
                    damage -= Shield;
                    Shield = 0;
                }
            }
            Hp -= damage;
        }
        else if(Damage.SrcType == ESrcType.Giant)
        {
            Hp -= damage;
        }
        else if(Damage.SrcType == ESrcType.Fairy)
        {
            if(Shield <= 0)
            {
                return;
            }
            Shield -= damage;
        }

        mInvincibleRemainTime = GameConstant.INVINCIBLE_TIME_BY_HIT;

        // 수신자의 위치
        Vector2 dstPosition = MainCollider.bounds.center;
        // 송신자의 위치
        Vector2 srcPosition = Damage.SrcPosition;
        // 송신자와 수신자의 방향 벡터
        Vector2 forceDirection = dstPosition - srcPosition;
        // 송신자와 수신자의 정규화 방향 벡터
        Vector2 normalizedForce = forceDirection.normalized;

        if (Damage.SrcType != ESrcType.Giant)
        {
            // 데미지의 정보를 표시하는 이펙트 객체를 가져옴
            Effect_Damage effect = EffectPool.Get(EEffectType.Damage) as Effect_Damage;

            if (effect != null)
            {
                effect.Damage = (int)damage;
                effect.DamageType = Damage.DamageType;
                effect.transform.position = new Vector3(dstPosition.x, dstPosition.y + 1.0f, 0.0f);
                effect.Activate();
            }
        }
        // 데미지로 인해 생성되는 이펙트 객체를 하나 가져온다
        EEffectType effectType = Damage.EffectType;
        if (effectType != EEffectType.None)
        {
            Effect_Hit hitEffect = EffectPool.Get(Damage.EffectType) as Effect_Hit;
            if (hitEffect != null)
            {
                hitEffect.IsFollowCamera = false;
                // 이펙트 방향 설정
                hitEffect.SetDirection(normalizedForce, true);
                // 메인 콜라이더 바운드
                Bounds dstBounds = MainCollider.bounds;
                Vector2 dstExtents = dstBounds.extents;
                // 이펙트 위치 설정
                hitEffect.transform.position = new Vector3(dstPosition.x + dstExtents.x * normalizedForce.x * -1.0f, dstPosition.y + dstExtents.y * normalizedForce.y * -1.0f);
                // 이펙트 활성화
                hitEffect.Activate();
            }
        }

        OnDamage();

        if(Damage.SrcType == ESrcType.Fairy)
        {
            if(Shield <= 0)
            {
                OnFaint();
                return;
            }
        }
        // 생명력이 1보다 작다면
        else if (Hp <= 0)
        {
            // 죽은상태
            OnDie();
            return;
        }

        // 최종적인 힘을 구함
        float force = Damage.SrcForce - Weight;
        // 힘이 0보다 크다면
        if (force > 0.0f)
        {
            OnHit();
            // 이전의 속도는 없앰
            Velocity = normalizedForce * force;
        }
    }

    public void ActivateHeal() { }

    protected virtual void OnDamage() { }
    protected virtual void OnDie() { }
    protected virtual void OnHit() { }
    protected virtual void OnFaint() { }

    protected override void Update()
    {
        base.Update();
        if (!StateManager.Pause.State)
        {
            if (mInvincibleRemainTime > 0.0f)
            {
                mInvincibleRemainTime -= Time.deltaTime;
            }
        }
    }

    protected override void Awake()
    {
        base.Awake();

        Damage = new Damage();
        Heal = new Heal();
    }
}
