﻿using UnityEngine;
using System.Collections;

public class _Monster_Bottom_Normal_CastEnd : State<_EStateType_Monster_Bottom_Normal>
{
    private _Monster_Bottom_Normal mMonster;

    public _Monster_Bottom_Normal_CastEnd(_Monster_Bottom_Normal monster) : base(_EStateType_Monster_Bottom_Normal.CastEnd)
    {
        mMonster = monster;
    }

    public override void Start()
    {
        mMonster.Animator.SetInteger("state", (int)Type);
    }

    public override void FixedUpdate()
    {
        mMonster.transform.position = mMonster.ActiveHolder.position;
    }
}
