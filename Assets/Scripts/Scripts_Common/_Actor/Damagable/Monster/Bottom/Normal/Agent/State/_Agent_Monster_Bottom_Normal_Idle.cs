﻿using UnityEngine;
using System.Collections;

public class _Agent_Monster_Bottom_Normal_Idle : State<_EStateType_Agent_Monster_Bottom_Normal>
{
    private _Agent_Monster_Bottom_Normal mAgent;

    public _Agent_Monster_Bottom_Normal_Idle(_Agent_Monster_Bottom_Normal agent) : base(_EStateType_Agent_Monster_Bottom_Normal.Idle)
    {
        mAgent = agent;
    }

    public override void Update()
    {
        if (mAgent.Monster.CanCast && !mAgent.Skill[0])
        {
            mAgent.SetSkill(true, 0);
        }
        else
        {
            mAgent.SetSkill(false, 0);
        }
    }
}
