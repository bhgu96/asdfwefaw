﻿using UnityEngine;
using System.Collections;

public class _Monster_Bottom_Normal_Die : State<_EStateType_Monster_Bottom_Normal>
{
    private _Monster_Bottom_Normal mMonster;

    public _Monster_Bottom_Normal_Die(_Monster_Bottom_Normal monster) : base(_EStateType_Monster_Bottom_Normal.Die)
    {
        mMonster = monster;
    }

    public override void Start()
    {
        mMonster.Animator.SetInteger("state", (int)Type);
        mMonster.MainCollider.enabled = false;
    }

    public override void End()
    {
        mMonster.MainCollider.enabled = true;
    }
}
