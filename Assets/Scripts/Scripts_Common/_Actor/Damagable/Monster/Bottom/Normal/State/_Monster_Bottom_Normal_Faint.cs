﻿using UnityEngine;
using System.Collections;

public class _Monster_Bottom_Normal_Faint : State<_EStateType_Monster_Bottom_Normal>
{
    private _Monster_Bottom_Normal mMonster;

    public _Monster_Bottom_Normal_Faint(_Monster_Bottom_Normal monster) : base(_EStateType_Monster_Bottom_Normal.Faint)
    {
        mMonster = monster;
    }

    public override void Start()
    {
        mMonster.Animator.SetInteger("state", (int)Type);
        mMonster.Rigidbody.constraints = RigidbodyConstraints2D.FreezeRotation;
    }

    public override void End()
    {
        mMonster.Rigidbody.constraints = RigidbodyConstraints2D.FreezeAll;
    }
}
