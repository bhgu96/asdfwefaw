﻿using UnityEngine;
using System.Collections;

public class _Monster_Bottom_Normal_Cast : State<_EStateType_Monster_Bottom_Normal>
{
    private _Monster_Bottom_Normal mMonster;

    public _Monster_Bottom_Normal_Cast(_Monster_Bottom_Normal monster) : base(_EStateType_Monster_Bottom_Normal.Cast)
    {
        mMonster = monster;
    }
    public override void Start()
    {
        mMonster.Animator.SetInteger("state", (int)Type);
        mMonster.OnCast();
    }

    public override void FixedUpdate()
    {
        mMonster.transform.position = mMonster.ActiveHolder.position;
    }

    public override void Update()
    {
        if(mMonster.RemainCount <= 0)
        {
            mMonster.State = _EStateType_Monster_Bottom_Normal.CastEnd;
        }
    }
}
