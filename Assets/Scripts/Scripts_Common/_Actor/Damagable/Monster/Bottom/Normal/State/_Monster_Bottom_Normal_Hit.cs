﻿using UnityEngine;

public class _Monster_Bottom_Normal_Hit : State<_EStateType_Monster_Bottom_Normal>
{
    private _Monster_Bottom_Normal mMonster;
    private bool mIsStarted = false;

    public _Monster_Bottom_Normal_Hit(_Monster_Bottom_Normal monster) : base(_EStateType_Monster_Bottom_Normal.Hit)
    {
        mMonster = monster;
    }

    public override void Start()
    {
        if (!mIsStarted)
        {
            mIsStarted = true;
            mMonster.Animator.SetInteger("state", (int)Type);
        }
        else
        {
            mMonster.Animator.SetTrigger("hit");
        }
    }

    public override void End()
    {
        mIsStarted = false;
    }

    public override void FixedUpdate()
    {
        mMonster.transform.position = mMonster.ActiveHolder.position;
    }
}
