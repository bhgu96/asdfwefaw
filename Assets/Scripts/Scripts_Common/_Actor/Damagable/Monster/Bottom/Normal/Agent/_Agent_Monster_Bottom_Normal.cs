﻿using UnityEngine;
using System.Collections;

public enum _EStateType_Agent_Monster_Bottom_Normal
{
    None = -1, 

    Idle, 

    Count
}

public class _Agent_Monster_Bottom_Normal : Agent<_EStateType_Agent_Monster_Bottom_Normal>
{
    public _Monster_Bottom_Normal Monster { get; private set; }

    public _Agent_Monster_Bottom_Normal(_Monster_Bottom_Normal monster) : base(1)
    {
        Monster = monster;

        SetStates(new _Agent_Monster_Bottom_Normal_Idle(this));
    }
}
