﻿using UnityEngine;
using System.Collections;

public enum _EStateType_Monster_Bottom_Normal
{
    None = -1,

    Idle,
    Spawn,
    Retreat,
    Hit,
    Die,
    CastPrepare,
    Cast,
    CastEnd,

    Faint,

    Count
}

public enum _ETriggerType_Monster_Bottom_Normal
{
    None = -1,

    Enemy,

    Count
}

public enum _EAudioType_Monster_Bottom_Normal
{
    None = -1,

    Count
}

[RequireComponent(typeof(_TriggerHandler_Monster_Bottom_Normal), typeof(_AudioHandler_Monster_Bottom_Normal))]
public class _Monster_Bottom_Normal : _AMonster_Bottom<_EStateType_Monster_Bottom_Normal>
{
    [SerializeField]
    private _EMonsterType mType;
    public override _EMonsterType Type { get { return mType; } }

    public override int MaxShield_Increment
    {
        get { return base.MaxShield_Increment + _Spawner.Main.Level - 1; }
    }

    [SerializeField]
    private int mOffense;
    public int Offense { get { return mOffense; } }

    [SerializeField]
    private float mForce;
    public float Force { get { return mForce; } }

    [SerializeField]
    private float mCastTime;
    public float CastTime { get { return mCastTime * (1.0f - 0.01f * _Spawner.Main.Level); } }

    [SerializeField]
    private int mCountOfCast;
    public int CountOfCast { get { return mCountOfCast + (int)(_Spawner.Main.Level * 0.1f); } }
    public int RemainCount { get; private set; }

    [SerializeField]
    private float mCoolDown;
    public float CoolDown { get { return mCoolDown * (1.0f - 0.01f * _Spawner.Main.Level); } }
    public float RemainTime_CoolDown { get; private set; }

    [SerializeField]
    private float mBulletSpeed;
    public float BulletSpeed { get { return mBulletSpeed; } }

    public _TriggerHandler_Monster_Bottom_Normal TriggerHandler { get; private set; }
    public _AudioHandler_Monster_Bottom_Normal AudioHandler { get; private set; }

    public bool CanCast { get { return RemainTime_CoolDown <= Mathf.Epsilon; } }

    [SerializeField]
    private Transform mProjectileHolder;

    [SerializeField]
    private ParticleSystem mHitParticle;

    public void CastProjectile()
    {
        Projectile_ElementalBullet bullet = ProjectilePool.Get(EProjectileType.FireBullet) as Projectile_ElementalBullet;

        Vector2 startPosition = mProjectileHolder.position;
        Vector2 endPosition = _AGiant.Main.MainCollider.bounds.center;

            if (bullet != null)
        {
            bullet.transform.position = startPosition;
            bullet.SetDirection(endPosition - startPosition, false);

            bullet.Hp = 1 + (int)(_Spawner.Main.Level * 0.2f);

            bullet.IsBasedOnGiant = true;
            bullet.IsTest = true;
            bullet.TestOffense = Offense;
            bullet.Force = Force;
            bullet.Speed = BulletSpeed;
            bullet.Activate();
        }
        RemainCount--;
    }

    public void OnCast()
    {
        RemainTime_CoolDown = CoolDown;
        RemainCount = CountOfCast;
    }

    protected override void OnDie()
    {
        base.OnDie();
        State = _EStateType_Monster_Bottom_Normal.Die;
        mHitParticle.Play();
    }

    protected override void OnDamage()
    {
        base.OnDamage();
        mHitParticle.Play();
        State = _EStateType_Monster_Bottom_Normal.Hit;
    }

    protected override void OnActivate()
    {
        base.OnActivate();
        State = _EStateType_Monster_Bottom_Normal.Spawn;
    }

    protected override void OnFaint()
    {
        base.OnFaint();
        mHitParticle.Play();
        State = _EStateType_Monster_Bottom_Normal.Faint;
    }

    protected override void Awake()
    {
        base.Awake();

        TriggerHandler = GetComponent<_TriggerHandler_Monster_Bottom_Normal>();
        AudioHandler = GetComponent<_AudioHandler_Monster_Bottom_Normal>();

        SetInputs(new _Agent_Monster_Bottom_Normal(this));

        SetStates(new _Monster_Bottom_Normal_Idle(this)
            , new _Monster_Bottom_Normal_Spawn(this)
            , new _Monster_Bottom_Normal_Retreat(this)
            , new _Monster_Bottom_Normal_Hit(this)
            , new _Monster_Bottom_Normal_Die(this)
            , new _Monster_Bottom_Normal_CastPrepare(this)
            , new _Monster_Bottom_Normal_Cast(this)
            , new _Monster_Bottom_Normal_CastEnd(this)
            , new _Monster_Bottom_Normal_Faint(this));
    }

    protected override void Update()
    {
        base.Update();
        if (!StateManager.Pause.State)
        {
            if (RemainTime_CoolDown > 0.0f)
            {
                RemainTime_CoolDown -= Time.deltaTime;
            }
        }
    }
}
