﻿using UnityEngine;

public class _Monster_Bottom_Normal_Idle : State<_EStateType_Monster_Bottom_Normal>
{
    private _Monster_Bottom_Normal mMonster;

    public _Monster_Bottom_Normal_Idle(_Monster_Bottom_Normal monster) : base(_EStateType_Monster_Bottom_Normal.Idle)
    {
        mMonster = monster;
    }

    public override void Start()
    {
        mMonster.Animator.SetInteger("state", (int)Type);
    }

    public override void Update()
    {
        if(mMonster.Input.SkillDown[0] && mMonster.CanCast)
        {
            mMonster.State = _EStateType_Monster_Bottom_Normal.CastPrepare;
            return;
        }
    }

    public override void FixedUpdate()
    {
        Vector2 curPosition = mMonster.transform.position;
        Vector2 activePosition = mMonster.ActiveHolder.position;

        mMonster.transform.position = activePosition;

        if(Mathf.Abs(curPosition.x - activePosition.x) > GameConstant.MOVEMENT_THRESHOLD + Mathf.Epsilon
            || Mathf.Abs(curPosition.y - activePosition.y) > GameConstant.MOVEMENT_THRESHOLD + Mathf.Epsilon)
        {
            mMonster.State = _EStateType_Monster_Bottom_Normal.Retreat;
            return;
        }
    }
}