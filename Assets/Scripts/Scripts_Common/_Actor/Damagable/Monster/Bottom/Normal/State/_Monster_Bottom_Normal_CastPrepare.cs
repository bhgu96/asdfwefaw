﻿using UnityEngine;
using System.Collections;

public class _Monster_Bottom_Normal_CastPrepare : State<_EStateType_Monster_Bottom_Normal>
{
    private _Monster_Bottom_Normal mMonster;
    private float mRemainTime;

    public _Monster_Bottom_Normal_CastPrepare(_Monster_Bottom_Normal monster) : base(_EStateType_Monster_Bottom_Normal.CastPrepare)
    {
        mMonster = monster;
    }
    public override void Start()
    {
        mMonster.Animator.SetInteger("state", (int)Type);
        mRemainTime = Random.Range(mMonster.CastTime - 1.0f, mMonster.CastTime + 2.0f);
    }

    public override void Update()
    {
        if(mRemainTime > 0.0f)
        {
            mRemainTime -= Time.deltaTime;
        }
        else
        {
            mMonster.State = _EStateType_Monster_Bottom_Normal.Cast;
            return;
        }
    }

    public override void FixedUpdate()
    {
        mMonster.transform.position = mMonster.ActiveHolder.position;
    }
}
