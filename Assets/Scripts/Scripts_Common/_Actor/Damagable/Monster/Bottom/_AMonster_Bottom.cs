﻿using UnityEngine;
using System.Collections;

public interface _IMonster_Bottom : _IMonster
{

}

public abstract class _AMonster_Bottom<EState> : _AMonster<EState>, _IMonster_Bottom
    where EState : System.Enum
{
    
}
