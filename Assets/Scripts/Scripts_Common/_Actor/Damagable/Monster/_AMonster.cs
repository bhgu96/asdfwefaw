﻿using UnityEngine;
using UnityEngine.SceneManagement;
using Object;

public enum _EMonsterType
{
    None = -1, 

    Normal_00, 

    Count
}

public interface _IMonster : _IActor_Damagable, IPoolable<_EMonsterType>
{
    Transform ActiveHolder { get; set; }
    bool Activate();
    bool Deactivate();
}

public abstract class _AMonster<EState> : _AActor_Damagable<EState>, _IMonster
    where EState : System.Enum
{
    public abstract _EMonsterType Type { get; }

    public virtual bool IsPooled
    {
        get; set;
    } = true;

    public Transform ActiveHolder { get; set; }

    public bool Activate()
    {
        if(IsPooled)
        {
            gameObject.SetActive(true);
            IsPooled = false;
            OnActivate();
            return true;
        }
        return false;
    }

    public bool Deactivate()
    {
        if(!IsPooled)
        {
            gameObject.SetActive(false);
            IsPooled = true;
            OnDeactivate();
            return true;
        }
        return false;
    }

    protected virtual void OnActivate()
    {
        _Spawner.Main.CountOfMonsters++;
        Hp = MaxHp;
        Shield = MaxShield;
    }
    protected virtual void OnDeactivate()
    {
        _Spawner.Main.CountOfMonsters--;
    }

    protected virtual void Start()
    {
        SceneManager.activeSceneChanged += SceneManager_activeSceneChanged;
    }

    private float mHitRemainTime = 2.0f;

    protected override void Update()
    {
        base.Update();
        if(MainCollider.enabled)
        {
            if(mHitRemainTime > 0.0f)
            {
                mHitRemainTime -= Time.deltaTime;
            }
            else
            {
                mHitRemainTime = 1.0f;
                Damage.Set(1, 0.0f, EDamageType.Fairy, ESrcType.Fairy, EEffectType.None, Vector2.zero);
                ActivateDamage();
            }
        }
    }

    private void SceneManager_activeSceneChanged(Scene arg0, Scene arg1)
    {
        Deactivate();
    }
}
