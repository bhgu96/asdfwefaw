﻿using UnityEngine;
using System.Collections;

public enum _EStateType_Giant
{
    // 대기
    Idle = 0,
    // 이동
    Move = 1,
    // 피격
    Hit = 2,
    // 죽음
    Die = 3,
    // 구속
    Restrain = 4,
    // 방출
    Release = 5,
    // 앞 구르기
    Roll = 6,
    // 손 들어 올리기
    RaiseHand = 7,

    // 소형 몬스터 공격 준비
    Attack_Prepare_Small_00 = 8,
    // 소형 몬스터 공격
    Attack_Small_00 = 9,
    // 소형 몬스터 공격 준비
    Attack_Prepare_Small_01 = 10,
    // 소형 몬스터 공격
    Attack_Small_01 = 11,
    // 중형 몬스터 공격 준비
    Attack_Prepare_Medium_00 = 12,
    // 중형 몬스터 공격
    Attack_Medium_00 = 13,
    // 중형 몬스터 공격 준비
    Attack_Prepare_Medium_01 = 14,
    // 중형 몬스터 공격
    Attack_Medium_01 = 15,
    // 대형 몬스터 공격 준비
    Attack_Prepare_Large_00 = 16,
    // 대형 몬스터 공격
    Attack_Large_00 = 17,
    // 대형 몬스터 공격 준비
    Attack_Prepare_Large_01 = 18,
    // 대형 몬스터 공격
    Attack_Large_01 = 19,
}

public enum _ETriggerType_Giant
{
    None = -1,

    // 공격 범위
    Attack = 0,
    // 긴급 공격 범위
    Attack_Urgent = 1,

    Count
}

public enum _EAudioType_Giant
{
    None = -1, 

    Count
}

public enum _EGiantType
{
    None = -1, 

    Type_00, 

    Count
}

[RequireComponent(typeof(_TriggerHandler_Giant), typeof(_AudioHandler_Giant), typeof(_AttackHandler_Giant))]
public abstract class _AGiant
    : _AActor_Damagable<_EStateType_Giant>
{
    public static _AGiant Main { get; private set; } = null;

    public bool IsInvincible { get; set; } = true;

    [SerializeField]
    private int mOffense;
    public int Offense { get { return mOffense; } }

    [SerializeField]
    private float mForce;
    public float Force { get { return mForce; } }

    private float mHealTime = 5.0f;
    private float mRemainTime_Heal;

    public abstract _EGiantType Type { get; }

    public _TriggerHandler_Giant TriggerHandler { get; private set; }
    public _AudioHandler_Giant AudioHandler { get; private set; }
    public _AttackHandler_Giant AttackHandler { get; private set; }

    public void Attack(_ETriggerType_Giant type)
    {
        Collider2D[] colliders = TriggerHandler.GetColliders(type);

        for (int i = 0; i < colliders.Length && colliders[i] != null; i++)
        {
            IDamagable damagable = colliders[i].GetComponentInParent<IDamagable>();

            if(damagable != null)
            {
                damagable.Damage.Set(Offense, Force, EDamageType.Giant, ESrcType.Giant, EEffectType.Hit_Break_01
                    , AttackHandler.Get(type).transform.position);
                damagable.ActivateDamage();
                CameraController.Main.Shake(0.2f, 1.0f);
            }
        }
    }

    /// <summary>
    /// 공격 가능한 액터를 검사하고 상황과 액터 크기에 따라 적절한 상태로 설정하는 메소드
    /// </summary>
    /// <param name="isAttacking">공격을 시전중에 있는가에 관한 변수</param>
    public bool SetAttack(bool isAttacking)
    {
        ESizeType size = GetDamagableSize(_ETriggerType_Giant.Attack_Urgent);

        int random = (int)Random.Range(0.0f, 2.0f);

        if (size == ESizeType.Small)
        {
            State = _EStateType_Giant.Attack_Small_00 + random * 2;
            return true;
        }
        else if (size == ESizeType.Medium)
        {
            State = _EStateType_Giant.Attack_Medium_00 + random * 2;
            return true;
        }
        else if (size == ESizeType.Large || size == ESizeType.ExtraLarge)
        {
            State = _EStateType_Giant.Attack_Large_00 + random * 2;
            return true;
        }

        // 공격 시전중이 아니라면
        if (!isAttacking)
        {
            size = GetDamagableSize(_ETriggerType_Giant.Attack);

            if (size == ESizeType.Small)
            {
                State = _EStateType_Giant.Attack_Prepare_Small_00 + random * 2;
                return true;
            }
            else if (size == ESizeType.Medium)
            {
                State = _EStateType_Giant.Attack_Prepare_Medium_00 + random * 2;
                return true;
            }
            else if (size == ESizeType.Large || size == ESizeType.ExtraLarge)
            {
                State = _EStateType_Giant.Attack_Prepare_Large_00 + random * 2;
                return true;
            }
        }

        return false;
    }

    /// <summary>
    /// 전달된 트리거에 존재하는 객체 중 액터인 객체의 사이즈를 반환하는 메소드
    /// </summary>
    /// <param name="type"></param>
    /// <returns></returns>
    private ESizeType GetDamagableSize(_ETriggerType_Giant type)
    {
        Collider2D[] colliders = TriggerHandler.GetColliders(type);

        for (int i = 0; i < colliders.Length && colliders[i] != null; i++)
        {
            _IActor_Damagable actor = colliders[i].GetComponentInParent<_IActor_Damagable>();

            if (actor != null)
            {
                return actor.Size;
            }
        }
        return ESizeType.None;
    }

    protected override void OnHit()
    {
        base.OnHit();
        State = _EStateType_Giant.Hit;
    }

    protected override void OnDie()
    {
        base.OnDie();
        if (!IsInvincible)
        {
            State = _EStateType_Giant.Die;
        }
    }

    public override void OnRestrain()
    {
        base.OnRestrain();
        State = _EStateType_Giant.Restrain;
    }

    protected override void Update()
    {
        base.Update();

        if(!StateManager.Pause.State)
        {
            if(mRemainTime_Heal > 0.0f)
            {
                mRemainTime_Heal -= Time.deltaTime;
            }
            else
            {
                mRemainTime_Heal = mHealTime;
                Hp += 1;
            }
        }

        if(UnityEngine.Input.touchCount > 0)
        {
            UnityEngine.SceneManagement.SceneManager.LoadScene(UnityEngine.SceneManagement.SceneManager.GetActiveScene().buildIndex);
        }
    }

    protected override void Awake()
    {
        base.Awake();

        TriggerHandler = GetComponent<_TriggerHandler_Giant>();
        AudioHandler = GetComponent<_AudioHandler_Giant>();
        AttackHandler = GetComponent<_AttackHandler_Giant>();

        // Input 을 설정
        SetInputs(new _Agent_Giant(this));
        // State를 설정
        SetStates(new _AGiant_Idle(this)
            , new _AGiant_Move(this)
            , new _AGiant_Hit(this)
            , new _AGiant_Die(this)
            , new _AGiant_Restrain(this)
            , new _AGiant_Release(this)
            , new _AGiant_Roll(this)
            , new _AGiant_RaiseHand(this)
            , new _AGiant_Attack_Prepare(this, _EStateType_Giant.Attack_Prepare_Small_00)
            , new _AGiant_Attack(this, _EStateType_Giant.Attack_Small_00)
            , new _AGiant_Attack_Prepare(this, _EStateType_Giant.Attack_Prepare_Small_01)
            , new _AGiant_Attack(this, _EStateType_Giant.Attack_Small_01)
            , new _AGiant_Attack_Prepare(this, _EStateType_Giant.Attack_Prepare_Medium_00)
            , new _AGiant_Attack(this, _EStateType_Giant.Attack_Medium_00)
            , new _AGiant_Attack_Prepare(this, _EStateType_Giant.Attack_Prepare_Medium_01)
            , new _AGiant_Attack(this, _EStateType_Giant.Attack_Medium_01)
            , new _AGiant_Attack_Prepare(this, _EStateType_Giant.Attack_Prepare_Large_00)
            , new _AGiant_Attack(this, _EStateType_Giant.Attack_Large_00)
            , new _AGiant_Attack_Prepare(this, _EStateType_Giant.Attack_Prepare_Large_01)
            , new _AGiant_Attack(this, _EStateType_Giant.Attack_Large_01));
    }

    private void OnEnable()
    {
        if (gameObject.tag.Equals(GameTags.Player))
        {
            Main = this;
        }
    }

    private void OnDisable()
    {
        if (gameObject.tag.Equals(GameTags.Player))
        {
            Main = null;
        }
    }
}
