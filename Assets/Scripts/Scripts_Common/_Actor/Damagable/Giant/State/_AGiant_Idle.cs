﻿using UnityEngine;

public class _AGiant_Idle : State<_EStateType_Giant>
{
    private _AGiant mActor;

    public _AGiant_Idle(_AGiant actor) : base(_EStateType_Giant.Idle)
    {
        mActor = actor;
    }

    public override void Start()
    {
        mActor.Animator.SetInteger("state", (int)Type);
    }

    public override void Update()
    {
        if(mActor.SetAttack(false))
        {
            return;
        }

        // 오른쪽으로 이동하는 입력이 감지된 경우
        if(mActor.Input.MoveHorizontal > 0.0f)
        {
            // 상태를 Move로 전이
            mActor.State = _EStateType_Giant.Move;
        }
    }
}
