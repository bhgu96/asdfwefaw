﻿using UnityEngine;

public class _AGiant_Hit : State<_EStateType_Giant>
{
    private _AGiant mActor;

    public _AGiant_Hit(_AGiant actor) : base(_EStateType_Giant.Hit)
    {
        mActor = actor;
    }

    public override void Start()
    {
        mActor.Animator.SetInteger("state", (int)Type);
        // 넉백 도중 넉백 적용시 애니메이션 재적용
        mActor.Animator.SetTrigger("hit");
    }

    public override void FixedUpdate()
    {
        Vector2 velocity = mActor.Velocity;

        // 액터의 넉백 적용 후 속도가 한계값 미만이면
        if(Mathf.Abs(velocity.x) < GameConstant.THRESHOLD_HIT
            && Mathf.Abs(velocity.y) < GameConstant.THRESHOLD_HIT)
        {
            // Idle 상태로 전이
            mActor.State = _EStateType_Giant.Idle;
        }
    }
}
