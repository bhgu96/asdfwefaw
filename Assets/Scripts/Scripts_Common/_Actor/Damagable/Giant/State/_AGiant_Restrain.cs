﻿using UnityEngine;

public class _AGiant_Restrain : State<_EStateType_Giant>
{
    private _AGiant mActor;

    public _AGiant_Restrain(_AGiant actor) : base(_EStateType_Giant.Restrain)
    {
        mActor = actor;
    }

    public override void Start()
    {
        mActor.Animator.SetInteger("state", (int)Type);
    }

    public override void Update()
    {
        // 구속 디버프가 해제된 상태라면
        if (!mActor.DebuffHandler.Get(EDebuffType.Restrain_Bush).IsActivated)
        {
            mActor.State = _EStateType_Giant.Idle;
        }
    }
}
