﻿using UnityEngine;

public class _AGiant_Roll : State<_EStateType_Giant>
{
    private _AGiant mActor;

    public _AGiant_Roll(_AGiant actor) : base(_EStateType_Giant.Roll)
    {
        mActor = actor;
    }

    public override void Start()
    {
        mActor.Animator.SetInteger("state", (int)Type);
    }
}
