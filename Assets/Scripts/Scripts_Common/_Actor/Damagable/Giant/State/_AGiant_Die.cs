﻿using UnityEngine;

public class _AGiant_Die : State<_EStateType_Giant>
{
    private _AGiant mActor;

    public _AGiant_Die(_AGiant actor) : base(_EStateType_Giant.Die)
    {
        mActor = actor;
    }

    public override void Start()
    {
        mActor.Animator.SetInteger("state", (int)Type);
        mActor.Velocity = Vector2.zero;
        StateManager.GameOver.State = true;
    }

    public override void End()
    {
        StateManager.GameOver.State = false;
    }
}
