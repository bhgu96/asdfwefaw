﻿using UnityEngine;

public class _AGiant_RaiseHand : State<_EStateType_Giant>
{
    private _AGiant mActor;
    public _AGiant_RaiseHand(_AGiant actor) : base(_EStateType_Giant.RaiseHand)
    {
        mActor = actor;
    }

    public override void Start()
    {
        mActor.Animator.SetInteger("state", (int)Type);
    }

    public override void FixedUpdate()
    {
        // 오른쪽으로 움직이는 입력이 감지된 경우
        if (mActor.Input.MoveHorizontal > 0.0f)
        {
            // 오른쪽으로 이동
            mActor.MoveHorizontal(mActor.MoveSpeed, EHorizontalDirection.Right);
        }

        if(mActor.SetAttack(false))
        {
            return;
        }
    }
}
