﻿using UnityEngine;

public class _AGiant_Attack_Prepare : State<_EStateType_Giant>
{
    private _AGiant mActor;

    public _AGiant_Attack_Prepare(_AGiant actor, _EStateType_Giant state) : base(state)
    {
        mActor = actor;
    }

    public override void Start()
    {
        mActor.Animator.SetInteger("state", (int)Type);
    }

    public override void FixedUpdate()
    {
        if (mActor.Input.MoveHorizontal > 0.0f)
        {
            mActor.MoveHorizontal(mActor.MoveSpeed, EHorizontalDirection.Right);
        }

        // 애니메이션이 실행중인 상태로 공격 상태 조사
        if (mActor.SetAttack(true))
        {
            return;
        }
    }
}
