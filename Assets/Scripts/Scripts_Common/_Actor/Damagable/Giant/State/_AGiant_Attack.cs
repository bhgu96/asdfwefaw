﻿using UnityEngine;

public class _AGiant_Attack : State<_EStateType_Giant>
{
    private _AGiant mActor;

    private float mRemainTime;
    private bool mIsStarted = false;

    public _AGiant_Attack(_AGiant actor, _EStateType_Giant state) : base(state)
    {
        mActor = actor;
    }

    public override void Start()
    {
        mRemainTime = GameConstant.ONE_FRAME;
        if (!mIsStarted)
        {
            mIsStarted = true;
            mActor.Animator.SetInteger("state", (int)Type);
        }
        else
        {
            mActor.Animator.SetTrigger("attack");
        }
    }

    public override void End()
    {
        mIsStarted = false;
    }

    public override void FixedUpdate()
    {
        if (mActor.Input.MoveHorizontal > 0.0f)
        {
            mActor.MoveHorizontal(mActor.MoveSpeed, EHorizontalDirection.Right);
        }

        // 한프레임 대기 후 공격 체크
        if (mRemainTime > 0.0f)
        {
            mRemainTime -= Time.deltaTime;
        }
        else if(mActor.SetAttack(true))
        {
            return;
        }
    }
}
