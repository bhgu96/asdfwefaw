﻿using UnityEngine;

public class _Giant_Player : _AGiant
{
    [SerializeField]
    private _EGiantType mType;
    public override _EGiantType Type
    {
        get { return mType; }
    }
}
