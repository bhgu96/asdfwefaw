﻿using UnityEngine;
using System.Collections;

public enum _EStateType_Agent_Giant
{
    Idle = 0,
    Move = 1, 
}

public class _Agent_Giant : Agent<_EStateType_Agent_Giant>
{
    public _AGiant Actor { get; private set; }

    public _Agent_Giant(_AGiant actor) : base(0)
    {
        Actor = actor;

        SetStates(new _Agent_Giant_Idle(this)
            , new _Agent_Giant_Move(this));
    }
}
