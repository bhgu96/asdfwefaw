﻿
public class _Agent_Giant_Move : State<_EStateType_Agent_Giant>
{
    private _Agent_Giant mAgent;

    public _Agent_Giant_Move(_Agent_Giant agent) : base(_EStateType_Agent_Giant.Move)
    {
        mAgent = agent;
    }

    public override void Update()
    {
        mAgent.SetMoveHorizontal(1.0f);


        // Move 상태 이외의 상태 조건이 정해지지 않았다
    }
}
