﻿using UnityEngine;
using System.Collections;

public class _Agent_Giant_Idle : State<_EStateType_Agent_Giant>
{
    private _Agent_Giant mAgent;

    public _Agent_Giant_Idle(_Agent_Giant agent) : base(_EStateType_Agent_Giant.Idle)
    {
        mAgent = agent;
    }

    public override void Update()
    {
        mAgent.SetMoveHorizontal(0.0f);
        // 현재 Idle 상태 조건이 정해지지 않음
        mAgent.State = _EStateType_Agent_Giant.Move;
    }
}
