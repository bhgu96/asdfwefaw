﻿using UnityEngine;

public interface _IActor
{
    ESizeType Size { get; }
    float MoveSpeed { get; }
    Animator Animator { get; }
    Rigidbody2D Rigidbody { get; }
    Transform MainTransform { get; }
    SpriteRenderer MainRenderer { get; }
    Collider2D MainCollider { get; }
    TriggerHandler_Terrain TerrainTrigger { get; }
    EHorizontalDirection HorizontalDirection { get; set; }
    Vector2 Velocity { get; set; }
    PhysicsMaterial2D PhysicsMaterial { get; }
    bool IsGround { get; }
    void MoveHorizontal(float speed, EHorizontalDirection direction);

    Transform transform { get; }
    GameObject gameObject { get; }
}

[RequireComponent(typeof(Rigidbody2D), typeof(Animator), typeof(TriggerHandler_Terrain))]
[RequireComponent(typeof(BuffHandler), typeof(DebuffHandler))]
public abstract class _AActor<EState> 
    : AControllable<EState>, _IActor
    where EState : System.Enum
{
    public Animator Animator { get; private set; }

    public Rigidbody2D Rigidbody { get; private set; }

    [SerializeField]
    private Transform mMainTransform;
    public Transform MainTransform
    {
        get { return mMainTransform; }
    }

    [SerializeField]
    private SpriteRenderer mMainRenderer;
    public SpriteRenderer MainRenderer { get { return mMainRenderer; } }

    [SerializeField]
    private Collider2D mMainCollider;
    public Collider2D MainCollider { get { return mMainCollider; } }

    public TriggerHandler_Terrain TerrainTrigger { get; private set; }
    public BuffHandler BuffHandler { get; private set; }
    public DebuffHandler DebuffHandler { get; private set; }

    [SerializeField]
    private ESizeType mSize;
    public ESizeType Size { get { return mSize; } }

    [SerializeField]
    private float mMoveSpeed;
    public float MoveSpeed { get { return mMoveSpeed; } }

    private EHorizontalDirection mHorizontalDirection;
    public EHorizontalDirection HorizontalDirection
    {
        get { return mHorizontalDirection; }
        set
        {
            if (mHorizontalDirection != value)
            {
                Vector3 localScale = mMainTransform.localScale;
                mMainTransform.localScale = new Vector3(localScale.x * -1.0f, localScale.y, localScale.z);
                mHorizontalDirection = value;
            }
        }
    }

    private bool mIsPhysicsUpdated;
    public bool IsZeroFriction { get; private set; }
    public Vector2 Velocity
    {
        get { return Rigidbody.velocity; }
        set
        {
            mIsPhysicsUpdated = true;
            Rigidbody.velocity = value;
        }
    }

    public PhysicsMaterial2D PhysicsMaterial { get; private set; }

    public bool IsGround
    {
        get { return (TerrainTrigger.GetColliders(ETerrainTrigger.Terrain)[0] != null); }
    }

    public void MoveHorizontal(float speed, EHorizontalDirection direction)
    {
        HorizontalDirection = direction;
        Velocity = new Vector2((float)HorizontalDirection * speed, Velocity.y);
    }

    protected override void Awake()
    {
        base.Awake();

        if(mMainTransform.lossyScale.x > 0.0f)
        {
            mHorizontalDirection = EHorizontalDirection.Right;
        }
        else
        {
            mHorizontalDirection = EHorizontalDirection.Left;
        }

        Rigidbody = GetComponent<Rigidbody2D>();
        Animator = GetComponent<Animator>();
        TerrainTrigger = GetComponent<TriggerHandler_Terrain>();
        BuffHandler = GetComponent<BuffHandler>();
        DebuffHandler = GetComponent<DebuffHandler>();

        PhysicsMaterial = Rigidbody.sharedMaterial;
    }

    protected override void FixedUpdate()
    {
        if (!StateManager.Pause.State)
        {
            base.FixedUpdate();

            if (mIsPhysicsUpdated)
            {
                mIsPhysicsUpdated = false;
                if (!IsZeroFriction)
                {
                    IsZeroFriction = true;
                    Rigidbody.sharedMaterial = GameConstant.ZeroMaterial;
                }
            }
            else if (IsZeroFriction)
            {
                IsZeroFriction = false;
                Rigidbody.sharedMaterial = PhysicsMaterial;
            }
        }
    }

    protected override void Update()
    {
        if (!StateManager.Pause.State)
        {
            base.Update();
        }
    }

    /// <summary>
    /// 구속 상태가 되었을때 호출되는 콜백 메소드
    /// </summary>
    public virtual void OnRestrain() { }
}
