﻿using UnityEngine;

public interface _IActor_Flyable
{
    float Acceleration { get; }
    float AirResistance { get; }
    bool IsFlying { get; set; }
    float Angle { get; set; }
    Vector2 Direction { get; }
    void SetDirection(Vector2 direction, bool isNormalized);
    void AddVelocity(float maxSpeed, Vector2 acceleration);
}

public abstract class _AActor_Flyable<EState> 
    : _AActor<EState>
    where EState : System.Enum
{
    [SerializeField]
    private float mAcceleration;
    public float Acceleration
    {
        get { return mAcceleration; }
    }

    [SerializeField]
    private float mAirResistance;
    public float AirResistance
    {
        get { return mAirResistance; }
    }

    public bool IsFlying
    {
        get { return Rigidbody.gravityScale <= Mathf.Epsilon; }
        set
        {
            if(value)
            {
                Rigidbody.gravityScale = 0.0f;
            }
            else
            {
                Rigidbody.gravityScale = 1.0f;
            }
        }
    }

    private bool mIsSetDirection = false;
    private float mAngle;
    public float Angle
    {
        get { return mAngle; }
        set
        {
            while(value > 180.0f)
            {
                value -= 360.0f;
            }
            while(value < -180.0f)
            {
                value += 360.0f;
            }

            mAngle = value;

            if(!mIsSetDirection)
            {
                Direction = new Vector2(Mathf.Cos(mAngle * Mathf.Deg2Rad), Mathf.Sin(mAngle * Mathf.Deg2Rad));
            }
            else
            {
                mIsSetDirection = false;
            }

            // 오른쪽을 보고있는 경우
            if(mAngle >= -90.0f && mAngle <= 90.0f)
            {
                HorizontalDirection = EHorizontalDirection.Right;

                MainTransform.eulerAngles = new Vector3(0.0f, 0.0f, mAngle);
            }
            // 왼쪽을 보고있는 경우
            else
            {
                HorizontalDirection = EHorizontalDirection.Left;

                MainTransform.eulerAngles = new Vector3(0.0f, 0.0f, mAngle - 180.0f);
            }
        }
    }

    public Vector2 Direction { get; private set; }

    public void SetDirection(Vector2 direction, bool isNormalized)
    {
        if(!isNormalized)
        {
            direction.Normalize();
        }

        Direction = direction;
        mIsSetDirection = true;

        // 위 방향으로 움직이는 경우
        if(direction.y >= 0.0f)
        {
            Angle = Mathf.Acos(direction.x) * Mathf.Rad2Deg;
        }
        // 아래 방향으로 움직이는 경우
        else
        {
            Angle -= Mathf.Acos(direction.x) * Mathf.Rad2Deg;
        }
    }

    private bool mIsAccelerated_x = false;
    private bool mIsAccelerated_y = false;

    /// <summary>
    /// 기존 속도에 가속도를 가하는 메소드
    /// Status 객체의 MoveSpeed (최대속도)를 고려하여 작동
    /// </summary>
    /// <param name="acceleration"></param>
    public void AddVelocity(float maxSpeed, Vector2 acceleration)
    {
        Vector2 curVelocity = Velocity;
        float nextVelocity_x = curVelocity.x;
        float nextVelocity_y = curVelocity.y;

        // x 축 속도가 최대 속도를 넘는 경우
        if (Mathf.Abs(nextVelocity_x) > maxSpeed)
        {
            if (nextVelocity_x < -maxSpeed)
            {
                // x 축 가속도가 현재 속도와 다른 방향인 경우
                if (acceleration.x > Mathf.Epsilon)
                {
                    mIsAccelerated_x = true;
                    // 가속도 적용
                    nextVelocity_x += acceleration.x;
                }
            }
            else if (curVelocity.x > maxSpeed)
            {
                // x 축 가속도가 현재 속도와 다른 방향인 경우
                if (acceleration.x < -Mathf.Epsilon)
                {
                    mIsAccelerated_x = true;
                    // 가속도 적용
                    nextVelocity_x += acceleration.x;
                }
            }
        }
        // 현재 속도가 최대 속도를 넘지 않는 경우
        else
        {
            if (Mathf.Abs(acceleration.x) > Mathf.Epsilon)
            {
                mIsAccelerated_x = true;
                nextVelocity_x += acceleration.x;
                // 가속도를 더한 x 축 속도가 최대 속도를 넘는 경우
                if (nextVelocity_x < -maxSpeed)
                {
                    nextVelocity_x = -maxSpeed;
                }
                else if (nextVelocity_x > maxSpeed)
                {
                    nextVelocity_x = maxSpeed;
                }
            }
        }

        // y 축 속도가 최대 속도를 넘는 경우
        if (Mathf.Abs(nextVelocity_y) > maxSpeed)
        {
            if (nextVelocity_y < -maxSpeed)
            {
                // y 축 가속도가 현재 속도와 다른 방향인 경우
                if (acceleration.y > Mathf.Epsilon)
                {
                    mIsAccelerated_y = true;
                    // 가속도 적용
                    nextVelocity_y += acceleration.y;
                }
            }
            else if (nextVelocity_y > maxSpeed)
            {
                // y 축 가속도가 현재 속도와 다른 방향인 경우
                if (acceleration.y < -Mathf.Epsilon)
                {
                    mIsAccelerated_y = true;
                    // 가속도 적용
                    nextVelocity_y += acceleration.y;
                }
            }
        }
        // y 축 속도가 최대 속도를 넘지 않는 경우
        else
        {
            if (Mathf.Abs(acceleration.y) > Mathf.Epsilon)
            {
                mIsAccelerated_y = true;
                nextVelocity_y += acceleration.y;
                // 가속도를 다한 속도가 최대 속도를 넘는 경우
                if (nextVelocity_y < -maxSpeed)
                {
                    nextVelocity_y = -maxSpeed;
                }
                else if (nextVelocity_y > maxSpeed)
                {
                    nextVelocity_y = maxSpeed;
                }
            }
        }

        Velocity = new Vector2(nextVelocity_x, nextVelocity_y);
    }

    protected override void FixedUpdate()
    {
        base.FixedUpdate();

        if (!StateManager.Pause.State)
        {
            // 땅에 존재하지 않는 경우 공기저항 적용
            if (!IsFlying)
            {
                AddResistance();
            }
        }
    }

    /// <summary>
    /// 공기 저항을 고려하여 속도를 조정하는 메소드
    /// </summary>
    private void AddResistance()
    {
        Vector2 curVelocity = Velocity;
        float nextVelocity_x = curVelocity.x;
        float nextVelocity_y = curVelocity.y;

        // x 축 가속이 적용되지 않은 경우
        if (!mIsAccelerated_x)
        {
            // x 축 속도가 양수인 경우
            if (nextVelocity_x > Mathf.Epsilon)
            {
                nextVelocity_x -= AirResistance * Time.fixedDeltaTime;

                // 공기 저항을 고려하여 부호가 달라진 경우
                if (nextVelocity_x < -Mathf.Epsilon)
                {
                    // 속도를 0으로 갱신
                    nextVelocity_x = 0.0f;
                }
            }
            // x 축 속도가 음수인 경우
            else if (nextVelocity_x < -Mathf.Epsilon)
            {
                nextVelocity_x += AirResistance * Time.fixedDeltaTime;

                // 공기 저항을 고려하여 부호가 달라진 경우
                if (nextVelocity_x > Mathf.Epsilon)
                {
                    // 속도를 0으로 갱신
                    nextVelocity_x = 0.0f;
                }
            }
        }
        // x 축 가속이 적용된 경우
        else
        {
            mIsAccelerated_x = false;
        }

        // y 축 가속이 적용되지 않은 경우
        if (!mIsAccelerated_y)
        {
            // y 축 속도가 양수인 경우
            if (nextVelocity_y > Mathf.Epsilon)
            {
                nextVelocity_y -= AirResistance * Time.fixedDeltaTime;

                // 공기 저항을 고려하여 부호가 달라진 경우
                if (nextVelocity_y < -Mathf.Epsilon)
                {
                    nextVelocity_y = 0.0f;
                }
            }
            // y 축 속도가 음수인 경우
            else if (nextVelocity_y < -Mathf.Epsilon)
            {
                nextVelocity_y += AirResistance * Time.fixedDeltaTime;

                // 공기저항을 고려하여 부호가 달라진 경우
                if (nextVelocity_y > Mathf.Epsilon)
                {
                    nextVelocity_y = 0.0f;
                }
            }
        }
        // y축 가속이 적용된 경우
        else
        {
            mIsAccelerated_y = false;
        }

        Velocity = new Vector2(nextVelocity_x, nextVelocity_y);
        print(Velocity);
    }
}
