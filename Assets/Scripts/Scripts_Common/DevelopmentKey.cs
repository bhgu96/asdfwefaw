﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

/// <summary>
/// 추후 콘솔 시스템으로 바뀔 예정
/// </summary>
public class DevelopmentKey : MonoBehaviour
{
    private static bool mIsInstantiated = false;

    public static Transform EndingArea
    {
        get { return mEndingArea; }
        set
        {
            mIsActiveEndingArea = value != null;
            mEndingArea = value;
        }
    }
    private static bool mIsActiveEndingArea = false;
    private static float mTimeOfMoveToEnd;
    private static Transform mEndingArea;

    private void Awake()
    {
        // 개발자 키가 생성되지 않았다면
        if (!mIsInstantiated)
        {
            mIsInstantiated = true;
            Application.targetFrameRate = 120;
            DontDestroyOnLoad(gameObject);
        }
        // 개발자 키가 생성되었다면
        else
        {
            Destroy(gameObject);
        }
    }

    private void Update()
    {
        if(mTimeOfMoveToEnd > 0.0f)
        {
            mTimeOfMoveToEnd -= Time.deltaTime;
            Fairy_Player.Main.transform.position = EndingArea.position;
        }
        
        if (Input.GetKey(KeyCode.LeftControl))
        {
            if (Input.GetKeyDown(KeyCode.R))
            {
                StateManager.GameClear.State = false;
                StateManager.GameOver.State = false;
                StateManager.Pause.State = false;
                // 씬 리로드
                SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
            }
            //else if (mIsActiveEndingArea && Input.GetKeyDown(KeyCode.E))
            //{
            //    // 지정된 지역으로 정령과 괴수 강제 이동
            //    Vector2 predatorPosition = Giant_Player.Main.transform.position;
            //    Vector2 endingAreaPosition = EndingArea.position;

            //    Giant_Player.Main.transform.position = endingAreaPosition;
            //    Fairy_Player.Main.transform.position = endingAreaPosition;
            //    mIsActiveEndingArea = false;
            //    mTimeOfMoveToEnd = 0.3f;
            //}
            //else if(Input.GetKeyDown(KeyCode.D))
            //{
            //    Giant_Player.Main.Damage.SetDamage(false, true, true, EEffectType.None, EDamageType.Critical, EElementalType.Kinematic
            //        , 999, 99999, 99999, 0, Vector3.zero, this);
            //    Giant_Player.Main.ActivateDamage();
            //}
            //else if(Input.GetKeyDown(KeyCode.LeftArrow))
            //{
            //    PlayerData.Difficulty -= 1;
            //}
            //else if(Input.GetKeyDown(KeyCode.RightArrow))
            //{
            //    PlayerData.Difficulty += 1;
            //}
            else if(Input.GetKeyDown(KeyCode.LeftShift))
            {
                //Giant_Player.Main.IsInvincible_Test = !Giant_Player.Main.IsInvincible_Test;
                _AGiant.Main.IsInvincible = !_AGiant.Main.IsInvincible;
            }
            //else if(Input.GetKeyDown(KeyCode.U))
            //{
            //    for(int i=0; i<10; i++)
            //    {
            //        PlayerData.Exp = PlayerData.MaxExp;
            //    }
            //}
            //else if(Input.GetKeyDown(KeyCode.M))
            //{
            //    Fairy_Player.Main.Status.Mana = Fairy_Player.Main.Status.MaxMana;
            //}
        }

        if (InputManager.GetBool(EInputType.Escape))
        {
            // 게임 종료
            Application.Quit();
        }
    }
}
