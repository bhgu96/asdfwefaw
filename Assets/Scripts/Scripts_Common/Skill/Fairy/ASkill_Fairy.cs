﻿using UnityEngine;
using System.Collections;
using System;

public interface ISkill_Fairy : ISkill
{
    /// <summary>
    /// 기본 마나 소모량
    /// </summary>
    float ManaConsumption_Default { get; }
    /// <summary>
    /// 마나 소모 감소량
    /// </summary>
    float ManaConsumption_Decrement { get; }
    /// <summary>
    /// 마나 소모 감소율
    /// </summary>
    float ManaConsumption_Decrease { get; }
    /// <summary>
    /// 최종 마나 소모량
    /// </summary>
    float ManaConsumption { get; }
}


public abstract class ASkill_Fairy<TUser, EState>
    : ASkill<TUser, EState>, ISkill_Fairy
    where TUser : IActor_Fairy
    where EState : Enum
{
    /// <summary>
    /// 기본 마나 소비량
    /// </summary>
    public abstract float ManaConsumption_Default { get; }
    /// <summary>
    /// 마나 소비 감소량
    /// </summary>
    public virtual float ManaConsumption_Decrement { get; }
    /// <summary>
    /// 마나 소비 감소율
    /// </summary>
    public virtual float ManaConsumption_Decrease { get; }
    /// <summary>
    /// 최종 마나 소비량
    /// </summary>
    public float ManaConsumption
    {
        get
        {
            return (ManaConsumption_Default - ManaConsumption_Decrement)
                * (1.0f - ManaConsumption_Decrease);
        }
    }
}
