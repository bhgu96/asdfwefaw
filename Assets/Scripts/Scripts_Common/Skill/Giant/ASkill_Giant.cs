﻿using UnityEngine;
using System.Collections;
using System;

public interface ISkill_Giant : ISkill
{

}

public abstract class ASkill_Giant<TUser, EState> 
    : ASkill<TUser, EState>
    , ISkill_Giant
    where TUser : IActor_Giant
    where EState : Enum
{
    
}
