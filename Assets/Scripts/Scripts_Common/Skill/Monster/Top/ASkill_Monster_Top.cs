﻿using UnityEngine;
using System;

public interface ISkill_Monster_Top
    : ISkill_Monster
{

}

public abstract class ASkill_Monster_Top<TUser, EState>
    : ASkill_Monster<TUser, EState>, ISkill_Monster_Top
    where TUser : IActor_Monster_Top
    where EState : Enum
{

}
