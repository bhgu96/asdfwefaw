﻿using UnityEngine;
using System;

public interface ISkill_Monster_Middle
    : ISkill_Monster
{

}

public abstract class ASkill_Monster_Middle<TUser, EState>
    : ASkill_Monster<TUser, EState>, ISkill_Monster_Middle
    where TUser : IActor_Monster_Middle
    where EState : Enum
{
    
}
