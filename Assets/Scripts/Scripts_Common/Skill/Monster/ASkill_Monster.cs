﻿using UnityEngine;
using System.Collections;
using System;

public interface ISkill_Monster : ISkill
{

}


public abstract class ASkill_Monster<TUser, EState>
    : ASkill<TUser, EState>, ISkill_Monster
    where TUser : IActor_Monster
    where EState : Enum
{
    
}
