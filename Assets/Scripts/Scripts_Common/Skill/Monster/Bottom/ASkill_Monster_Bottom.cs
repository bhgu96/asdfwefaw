﻿using UnityEngine;
using System;

public interface ISkill_Monster_Bottom
    : ISkill_Monster
{

}

public abstract class ASkill_Monster_Bottom<TUser, EState>
    : ASkill_Monster<TUser, EState>, ISkill_Monster_Bottom
    where TUser : IActor_Monster_Bottom
    where EState : Enum
{

}
