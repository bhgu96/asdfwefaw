﻿using UnityEngine;
using System;
using System.Collections;
using Object;

public interface ISkill
{
    /// <summary>
    /// 스킬의 아이콘
    /// </summary>
    Sprite Icon { get; }
    /// <summary>
    /// 이름 태그
    /// </summary>
    string NameTag { get; }
    /// <summary>
    /// 스킬의 이름
    /// </summary>
    string Name { get; }
    /// <summary>
    /// 설명 태그
    /// </summary>
    string DescriptionTag { get; }
    /// <summary>
    /// 스킬의 설명
    /// </summary>
    string Description { get; }
    /// <summary>
    /// 옵션 태그
    /// </summary>
    string OptionTag { get; }
    /// <summary>
    /// 스킬의 옵션
    /// </summary>
    string Option { get; }
    /// <summary>
    /// 부가 옵션 태그
    /// </summary>
    string AdditionalOptionTag { get; }
    /// <summary>
    /// 부가 옵션
    /// </summary>
    string AdditionalOption { get; }
    /// <summary>
    /// 스킬의 가변인자
    /// </summary>
    float[] Parameters { get; }

    /// <summary>
    /// 시전 준비중인가에 관한 프로퍼티
    /// </summary>
    bool IsCasting { get; }
    /// <summary>
    /// 기본 시전시간
    /// </summary>
    float CastTime { get; }
    /// <summary>
    /// 남은 시전 완료 시간
    /// </summary>
    float CastRemainTime { get; }
    /// <summary>
    /// 기본 스킬 재사용 대기시간
    /// </summary>
    float CoolTime_Default { get; }
    /// <summary>
    /// 스킬 재사용 대기시간 감소율
    /// </summary>
    float CoolTime_Decrease { get; }
    /// <summary>
    /// 최종 스킬 재사용 대기시간
    /// </summary>
    float CoolTime { get; }
    /// <summary>
    /// 스킬 쿨링 여부
    /// </summary>
    bool IsCool { get; }

    /// <summary>
    /// 남은 재사용 대기시간
    /// </summary>
    float CoolRemainTime { get; }

    /// <summary>
    /// 재사용 대기시간 감소량 증가율
    /// </summary>
    float CoolDeltaTime_Increase { get; }
    /// <summary>
    /// 재사용 대기시간 감소량
    /// </summary>
    float CoolDeltaTime { get; }

    /// <summary>
    /// 현재 활성화 상태인지 아닌지에 관한 프로퍼티
    /// </summary>
    bool IsActivated { get; }

    /// <summary>
    /// 스킬 활성화 가능 여부 프로퍼티
    /// </summary>
    bool CanActivate { get; }

    /// <summary>
    /// 스킬 비활성화 가능 여부 프로퍼티
    /// </summary>
    bool CanDeactivate { get; }

    /// <summary>
    /// 애니메이터
    /// </summary>
    Animator Animator { get; }

    /// <summary>
    /// 스킬 활성화 메소드
    /// </summary>
    /// <returns></returns>
    bool Activate();
    /// <summary>
    /// 스킬 비활성화 메소드
    /// </summary>
    /// <returns></returns>
    bool Deactivate();

    /// <summary>
    /// 유저의 스킬 캐스팅을 가능하게 만드는 메소드
    /// </summary>
    void SetEnableCast();
    /// <summary>
    /// 유저의 스킬 캐스팅을 불가능하게 만드는 메소드
    /// </summary>
    void SetDisableCast();
}

[RequireComponent(typeof(Animator))]
public abstract class ASkill<TUser, EState>
    : StateBase<EState>, ISkill
    where EState : Enum
    where TUser : IActor
{
    [SerializeField]
    private Sprite mIcon;
    /// <summary>
    /// 스킬 아이콘
    /// </summary>
    public Sprite Icon { get { return mIcon; } }

    /// <summary>
    /// 스킬 이름 태그
    /// </summary>
    public abstract string NameTag { get; }
    /// <summary>
    /// 스킬 이름
    /// </summary>
    public virtual string Name { get { return DataManager.GetScript(NameTag); } }

    /// <summary>
    /// 스킬 설명 태그
    /// </summary>
    public abstract string DescriptionTag { get; }
    /// <summary>
    /// 스킬 설명
    /// </summary>
    public virtual string Description { get { return DataManager.GetScript(DescriptionTag); } }

    /// <summary>
    /// 옵션 태그
    /// </summary>
    public abstract string OptionTag { get; }
    /// <summary>
    /// 스킬 옵션
    /// </summary>
    public virtual string Option { get { return DataManager.GetScript(OptionTag); } }

    /// <summary>
    /// 스킬 부가 옵션 태그
    /// </summary>
    public abstract string AdditionalOptionTag { get; }
    /// <summary>
    /// 스킬 부가 옵션
    /// </summary>
    public virtual string AdditionalOption { get { return DataManager.GetScript(AdditionalOptionTag); } }

    /// <summary>
    /// 스킬을 소유하고 사용하는 객체
    /// </summary>
    public TUser User { get; private set; }

    /// <summary>
    /// 애니메이터
    /// </summary>
    public Animator Animator { get; private set; }

    /// <summary>
    /// 스킬의 가변인자
    /// </summary>
    public abstract float[] Parameters { get; }

    /// <summary>
    /// 스킬이 시전중인지 여부
    /// </summary>
    public bool IsCasting { get; private set; }

    /// <summary>
    /// 스킬 시전 시간
    /// </summary>
    public abstract float CastTime { get; }

    /// <summary>
    /// 스킬 시전 완료까지 남은 시간
    /// </summary>
    public float CastRemainTime { get; private set; }

    /// <summary>
    /// 해당 스킬 시전속도 증가율
    /// </summary>
    public virtual float CastSpeed_Increase { get; }
    /// <summary>
    /// 최종 스킬 시전속도
    /// </summary>
    public float CastSpeed
    {
        get { return User.Status.CastSpeed + CastSpeed_Increase; }
    }

    /// <summary>
    /// 스킬 기본 재사용 대기시간
    /// </summary>
    public abstract float CoolTime_Default { get; }
    /// <summary>
    /// 스킬 재사용 대기시간 감소율
    /// </summary>
    public virtual float CoolTime_Decrease { get; }
    /// <summary>
    /// 최종 스킬 재사용 대기시간
    /// </summary>
    public float CoolTime
    {
        get
        {
            return CoolTime_Default * (1.0f - CoolTime_Decrease);
        }
    }

    /// <summary>
    /// 스킬 쿨링 여부
    /// </summary>
    public bool IsCool { get; private set; }

    /// <summary>
    /// 남은 재사용 대기시간
    /// </summary>
    public float CoolRemainTime { get; private set; }

    /// <summary>
    /// 남은 재사용 대기시간 감소율
    /// </summary>
    public virtual float CoolDeltaTime_Increase { get; }

    /// <summary>
    /// 최종 재사용 대기시간 감소량
    /// </summary>
    public float CoolDeltaTime
    {
        get
        {
            return Time.deltaTime * (1.0f + CoolDeltaTime_Increase);
        }
    }

    /// <summary>
    /// 액티브 스킬 활성화 상태 여부 프로퍼티
    /// </summary>
    public bool IsActivated { get; private set; } = false;

    /// <summary>
    /// 스킬 활성화 가능 여부 프로퍼티
    /// </summary>
    public virtual bool CanActivate { get { return !IsActivated && CoolRemainTime <= 0.0f; } }

    /// <summary>
    /// 스킬 비활성화 가능 여부 프로퍼티
    /// </summary>
    public virtual bool CanDeactivate { get { return IsActivated; } }

    /// <summary>
    /// 스킬 활성화 메소드
    /// </summary>
    /// <returns></returns>
    public bool Activate()
    {
        if (CanActivate)
        {
            IsActivated = true;
            IsCasting = true;
            IsCool = true;
            CastRemainTime = CastTime;
            CoolRemainTime = CoolTime;
            OnActivate();
            return true;
        }
        return false;
    }

    /// <summary>
    /// 스킬 비활성화 메소드
    /// </summary>
    /// <returns></returns>
    public bool Deactivate()
    {
        if (CanDeactivate)
        {
            IsActivated = false;
            IsCasting = false;
            OnDeactivate();
            return true;
        }
        return false;
    }

    /// <summary>
    /// 유저의 스킬 캐스트를 가능하게 하는 메소드
    /// </summary>
    public void SetEnableCast()
    {
        User.SetEnableCast();
    }

    /// <summary>
    /// 유저의 스킬 캐스트를 불가능하게 하는 메소드
    /// </summary>
    public void SetDisableCast()
    {
        User.SetDisableCast(this);
    }

    /// <summary>
    /// 스킬 활성화 성공시 호출되는 콜백 메소드
    /// </summary>
    protected virtual void OnActivate() { }

    /// <summary>
    /// 스킬 비활성화 성공시 호출되는 콜백 메소드
    /// </summary>
    protected virtual void OnDeactivate() { }

    protected override void Update()
    {
        base.Update();

        if (IsCool)
        {
            CoolRemainTime -= CoolDeltaTime;
            if(CoolRemainTime <= 0.0f)
            {
                IsCool = false;
                CoolRemainTime = 0.0f;
            }
        }


        if (IsCasting)
        {
            CastRemainTime -= Time.deltaTime * CastSpeed;
            if (CastRemainTime <= 0.0f)
            {
                IsCasting = false;
                CastRemainTime = 0.0f;
            }
        }
    }

    protected override void Awake()
    {
        base.Awake();
        User = GetComponentInParent<TUser>();
        Animator = GetComponent<Animator>();
    }
}
