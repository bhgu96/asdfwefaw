﻿using UnityEngine;
using System.Collections.Generic;
using Csv;

/// <summary>
/// 레벨 별 최대 경험치를 관리하는 클래스
/// </summary>
public static class ExpTable
{
    public static int MaxLevel { get { return mExpDict.Count; } }

    private static Dictionary<int, int> mExpDict;

    static ExpTable()
    {
        mExpDict = new Dictionary<int, int>();
        Dictionary<string, Dictionary<string, string>> expDict = CsvReader.Read(Application.streamingAssetsPath + "/Exp.csv");
        for(int i=0; i<expDict.Count; i++)
        {
            mExpDict.Add(i + 1, int.Parse(expDict[(i + 1).ToString()]["Exp"]));
        }
    }

    /// <summary>
    /// level 에 따른 최대 경험치를 반환하는 메소드
    /// </summary>
    /// <param name="level"></param>
    /// <returns></returns>
    public static int GetMaxExp(int level)
    {
        if (level <= MaxLevel && level > 0)
        {
            return mExpDict[level];
        }
        return mExpDict[MaxLevel];
    }
}
