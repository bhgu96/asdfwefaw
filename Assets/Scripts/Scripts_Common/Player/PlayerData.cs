﻿using UnityEngine;
using Csv;
using System.Collections.Generic;

public enum EIntAbility
{
    None = -1, 
    // 정화력
    Purification, 
    // 항마력
    Exorcism, 

    Count
}

public enum EFloatAbility
{
    None = -1, 
    // 정령 이동속도
    MoveSpeed_Fairy, 
    // 정령 시전속도
    CastSpeed_Fairy, 
    // 괴수 이동속도
    MoveSpeed_Giant, 
    // 괴수 시전속도
    CastSpeed_Giant, 
    // 최대 마나
    MaxMana, 
    // 마나 재생량
    ManaRps, 
    // 최대 생명력
    MaxLife, 
    // 생명력 재생량
    LifeRps, 
    // 정령 치명타 확률
    CriticalChance_Fairy, 
    // 정령 치명타 배율
    CriticalMag_Fairy, 
    // 괴수 치명타 확률
    CriticalChance_Giant, 
    // 괴수 치명타 배율
    CriticalMag_Giant, 

    Count
}

public class PlayerData
{
    private static int mDifficulty;
    /// <summary>
    /// 난이도
    /// </summary>
    public static int Difficulty
    {
        get { return mDifficulty; }
        set
        {
            if (value < 1)
            {
                value = 1;
            }
            //else if (value > 100)
            //{
            //    value = 100;
            //}

            mDifficulty = value;
        }
    }

    private static int mSouls;
    /// <summary>
    /// 소울 개수
    /// </summary>
    public static int Souls
    {
        get { return mSouls; }
        set
        {
            if(value < 0)
            {
                value = 0;
            }

            mSouls = value;
        }
    }

    /// <summary>
    /// 플레이어 레벨
    /// </summary>
    public static int Level { get; private set; }

    private static int mExp;
    /// <summary>
    /// 플레이어 경험치
    /// </summary>
    public static int Exp
    {
        get { return mExp; }
        set
        {
            while (value >= ExpTable.GetMaxExp(Level))
            {
                value -= ExpTable.GetMaxExp(Level);
                Level++;
            }

            mExp = value;
        }
    }

    /// <summary>
    /// 현재 레벨 기준 최대 경험치
    /// </summary>
    public static int MaxExp
    {
        get { return ExpTable.GetMaxExp(Level); }
    }

    private static Dictionary<EIntAbility, int> mIntAbilityDict_Promotion;
    private static Dictionary<EFloatAbility, int> mFloatAbilityDict_Promotion;
    private static Dictionary<EIntAbility, int> mIntAbilityDict_Coefficient;
    private static Dictionary<EFloatAbility, float> mFloatAbilityDict_Coefficient;
    private static Dictionary<EElementalType, int> mElementalAffinityDict_Fairy_Promotion;
    private static Dictionary<EElementalType, int> mElementalAffinityDict_Giant_Promotion;
    private static Dictionary<EElementalType, int> mElementalAffinityDict_Fairy_Coefficient;
    private static Dictionary<EElementalType, int> mElementalAffinityDict_Giant_Coefficient;

    private static Dictionary<ESkillType_Fairy_Player, bool> mSkillUnSealDict_Fairy;
    private static Dictionary<ESkillType_Fairy_Player, int> mSkillMountDict_Fairy;

    private static Dictionary<ESkillType_Giant_Player, bool> mSkillUnSealDict_Giant;
    private static Dictionary<ESkillType_Giant_Player, int> mSkillMountDict_Giant;

    /// <summary>
    /// 승급 가능한 어빌리티 횟수
    /// </summary>
    public static int Promotion
    {
        get
        {
            int promotion = 0;

            for (EIntAbility e = EIntAbility.None + 1; e < EIntAbility.Count; e++)
            {
                promotion += mIntAbilityDict_Promotion[e];
            }

            for (EFloatAbility e = EFloatAbility.None + 1; e < EFloatAbility.Count; e++)
            {
                promotion += mFloatAbilityDict_Promotion[e];
            }

            for (EElementalType type = EElementalType.None + 1; type < EElementalType.Count; type++)
            {
                promotion += mElementalAffinityDict_Fairy_Promotion[type];
                promotion += mElementalAffinityDict_Giant_Promotion[type];
            }

            return Level - promotion;
        }
    }

    static PlayerData()
    {
        // 플레이어 데이터 파일 읽기
        Dictionary<string, Dictionary<string, string>> stringDict = CsvReader.Read(Application.streamingAssetsPath + "/Player/PlayerData.csv");

        // 플레이어 데이터가 존재한다면
        if(stringDict != null)
        {
            Dictionary<string, string> playerDict = stringDict["Player"];

            mDifficulty = int.Parse(playerDict["Difficulty"]);
            Level = int.Parse(playerDict["Level"]);
            mExp = int.Parse(playerDict["Exp"]);
            mSouls = int.Parse(playerDict["Souls"]);

            mIntAbilityDict_Promotion = new Dictionary<EIntAbility, int>();
            mFloatAbilityDict_Promotion = new Dictionary<EFloatAbility, int>();
            mElementalAffinityDict_Fairy_Promotion = new Dictionary<EElementalType, int>();
            mElementalAffinityDict_Giant_Promotion = new Dictionary<EElementalType, int>();

            mIntAbilityDict_Promotion.Add(EIntAbility.Purification, int.Parse(playerDict["Fairy Purification"]));
            mIntAbilityDict_Promotion.Add(EIntAbility.Exorcism, int.Parse(playerDict["Giant Exorcism"]));

            mFloatAbilityDict_Promotion.Add(EFloatAbility.MoveSpeed_Fairy, int.Parse(playerDict["Fairy Move Speed"]));
            mFloatAbilityDict_Promotion.Add(EFloatAbility.CastSpeed_Fairy, int.Parse(playerDict["Fairy Cast Speed"]));
            mFloatAbilityDict_Promotion.Add(EFloatAbility.MaxMana, int.Parse(playerDict["Fairy Max Mana"]));
            mFloatAbilityDict_Promotion.Add(EFloatAbility.ManaRps, int.Parse(playerDict["Fairy Mana Rps"]));
            mFloatAbilityDict_Promotion.Add(EFloatAbility.CriticalChance_Fairy, int.Parse(playerDict["Fairy Critical Chance"]));
            mFloatAbilityDict_Promotion.Add(EFloatAbility.CriticalMag_Fairy, int.Parse(playerDict["Fairy Critical Mag"]));
            mFloatAbilityDict_Promotion.Add(EFloatAbility.MoveSpeed_Giant, int.Parse(playerDict["Giant Move Speed"]));
            mFloatAbilityDict_Promotion.Add(EFloatAbility.CastSpeed_Giant, int.Parse(playerDict["Giant Cast Speed"]));
            mFloatAbilityDict_Promotion.Add(EFloatAbility.MaxLife, int.Parse(playerDict["Giant Max Life"]));
            mFloatAbilityDict_Promotion.Add(EFloatAbility.LifeRps, int.Parse(playerDict["Giant Life Rps"]));
            mFloatAbilityDict_Promotion.Add(EFloatAbility.CriticalChance_Giant, int.Parse(playerDict["Giant Critical Chance"]));
            mFloatAbilityDict_Promotion.Add(EFloatAbility.CriticalMag_Giant, int.Parse(playerDict["Giant Critical Mag"]));

            for (EElementalType type = EElementalType.None + 1; type < EElementalType.Count; type++)
            {
                string typeString = type.ToString();
                string spiritElement = "Fairy " + typeString;
                string predatorElement = "Giant " + typeString;
                mElementalAffinityDict_Fairy_Promotion.Add(type, int.Parse(playerDict[spiritElement]));
                mElementalAffinityDict_Giant_Promotion.Add(type, int.Parse(playerDict[predatorElement]));
            }
        }
        // 플레이어 데이터가 존재하지 않는다면
        else
        {
            mDifficulty = 1;
            Level = 1;
            mExp = 0;
            mSouls = 0;

            mIntAbilityDict_Promotion = new Dictionary<EIntAbility, int>();
            mFloatAbilityDict_Promotion = new Dictionary<EFloatAbility, int>();
            mElementalAffinityDict_Fairy_Promotion = new Dictionary<EElementalType, int>();
            mElementalAffinityDict_Giant_Promotion = new Dictionary<EElementalType, int>();

            for(EIntAbility e = EIntAbility.None + 1; e < EIntAbility.Count; e++)
            {
                mIntAbilityDict_Promotion.Add(e, 0);
            }

            for(EFloatAbility e = EFloatAbility.None + 1; e < EFloatAbility.Count; e++)
            {
                mFloatAbilityDict_Promotion.Add(e, 0);
            }

            for(EElementalType e = EElementalType.None + 1; e < EElementalType.Count; e++)
            {
                mElementalAffinityDict_Fairy_Promotion.Add(e, 0);
                mElementalAffinityDict_Giant_Promotion.Add(e, 0);
            }
        }

        // 플레이어 데이터 계수 할당
        stringDict = CsvReader.Read(Application.streamingAssetsPath + "/AbilityCoefficient.csv");
        Dictionary<string, string> coefficientDIct = stringDict["Player"];

        mIntAbilityDict_Coefficient = new Dictionary<EIntAbility, int>();
        mFloatAbilityDict_Coefficient = new Dictionary<EFloatAbility, float>();
        mElementalAffinityDict_Fairy_Coefficient = new Dictionary<EElementalType, int>();
        mElementalAffinityDict_Giant_Coefficient = new Dictionary<EElementalType, int>();


        mIntAbilityDict_Coefficient.Add(EIntAbility.Purification, int.Parse(coefficientDIct["Fairy Purification"]));
        mIntAbilityDict_Coefficient.Add(EIntAbility.Exorcism, int.Parse(coefficientDIct["Giant Exorcism"]));

        mFloatAbilityDict_Coefficient.Add(EFloatAbility.MoveSpeed_Fairy, float.Parse(coefficientDIct["Fairy Move Speed"]));
        mFloatAbilityDict_Coefficient.Add(EFloatAbility.CastSpeed_Fairy, float.Parse(coefficientDIct["Fairy Cast Speed"]));
        mFloatAbilityDict_Coefficient.Add(EFloatAbility.MaxMana, float.Parse(coefficientDIct["Fairy Max Mana"]));
        mFloatAbilityDict_Coefficient.Add(EFloatAbility.ManaRps, float.Parse(coefficientDIct["Fairy Mana Rps"]));
        mFloatAbilityDict_Coefficient.Add(EFloatAbility.CriticalChance_Fairy, float.Parse(coefficientDIct["Fairy Critical Chance"]));
        mFloatAbilityDict_Coefficient.Add(EFloatAbility.CriticalMag_Fairy, float.Parse(coefficientDIct["Fairy Critical Mag"]));
        mFloatAbilityDict_Coefficient.Add(EFloatAbility.MoveSpeed_Giant, float.Parse(coefficientDIct["Giant Move Speed"]));
        mFloatAbilityDict_Coefficient.Add(EFloatAbility.CastSpeed_Giant, float.Parse(coefficientDIct["Giant Cast Speed"]));
        mFloatAbilityDict_Coefficient.Add(EFloatAbility.MaxLife, float.Parse(coefficientDIct["Giant Max Life"]));
        mFloatAbilityDict_Coefficient.Add(EFloatAbility.LifeRps, float.Parse(coefficientDIct["Giant Life Rps"]));
        mFloatAbilityDict_Coefficient.Add(EFloatAbility.CriticalChance_Giant, float.Parse(coefficientDIct["Giant Critical Chance"]));
        mFloatAbilityDict_Coefficient.Add(EFloatAbility.CriticalMag_Giant, float.Parse(coefficientDIct["Giant Critical Mag"]));

        for (EElementalType type = EElementalType.None + 1; type < EElementalType.Count; type++)
        {
            string typeString = type.ToString();
            string spiritElement = "Fairy " + typeString;
            string predatorElement = "Giant " + typeString;

            mElementalAffinityDict_Fairy_Coefficient.Add(type, int.Parse(coefficientDIct[spiritElement]));
            mElementalAffinityDict_Giant_Coefficient.Add(type, int.Parse(coefficientDIct[predatorElement]));
        }

        // 스킬 봉인 해제 여부 및 장착 여부 로드
        stringDict = CsvReader.Read(Application.streamingAssetsPath + "/Player/PlayerData_Skill.csv");

        mSkillUnSealDict_Fairy = new Dictionary<ESkillType_Fairy_Player, bool>();
        mSkillMountDict_Fairy = new Dictionary<ESkillType_Fairy_Player, int>();

        mSkillUnSealDict_Giant = new Dictionary<ESkillType_Giant_Player, bool>();
        mSkillMountDict_Giant = new Dictionary<ESkillType_Giant_Player, int>();

        // 스킬 플레이어 데이터가 존재한다면
        if (stringDict != null)
        {
            for (ESkillType_Fairy_Player e = ESkillType_Fairy_Player.None + 1; e < ESkillType_Fairy_Player.Count; e++)
            {
                string typeString = e.ToString();
                // 해당 스킬이 파일에 존재한다면
                if (stringDict.ContainsKey(typeString))
                {
                    Dictionary<string, string> skillDict = stringDict[typeString];
                    bool value = bool.Parse(skillDict["UnSeal"]);
                    mSkillUnSealDict_Fairy.Add(e, value);
                    if (value)
                    {
                        mSkillMountDict_Fairy.Add(e, int.Parse(skillDict["Mount"]));
                    }
                    else
                    {
                        mSkillMountDict_Fairy.Add(e, -1);
                    }
                }
                // 해당 스킬이 파일에 존재하지 않는다면
                else
                {
                    mSkillUnSealDict_Fairy.Add(e, false);
                    mSkillMountDict_Fairy.Add(e, -1);
                }
            }

            for (ESkillType_Giant_Player e = ESkillType_Giant_Player.None + 1; e < ESkillType_Giant_Player.Count; e++)
            {
                string typeString = e.ToString();
                // 해당 스킬이 파일에 존재한다면
                if (stringDict.ContainsKey(typeString))
                {
                    Dictionary<string, string> skillDict = stringDict[typeString];
                    bool value = bool.Parse(skillDict["UnSeal"]);
                    mSkillUnSealDict_Giant.Add(e, value);
                    if (value)
                    {
                        mSkillMountDict_Giant.Add(e, int.Parse(skillDict["Mount"]));
                    }
                    else
                    {
                        mSkillMountDict_Giant.Add(e, -1);
                    }
                }
                // 해당 스킬이 파일에 존재하지 않는다면
                else
                {
                    mSkillUnSealDict_Giant.Add(e, false);
                    mSkillMountDict_Giant.Add(e, -1);
                }
            }
        }
        // 스킬 플레이어 데이터가 존재하지 않는다면
        else
        {
            for(ESkillType_Fairy_Player e = ESkillType_Fairy_Player.None + 1; e < ESkillType_Fairy_Player.Count; e++)
            {
                mSkillUnSealDict_Fairy.Add(e, false);
                mSkillMountDict_Fairy.Add(e, -1);
            }
            for(ESkillType_Giant_Player e = ESkillType_Giant_Player.None + 1; e < ESkillType_Giant_Player.Count; e++)
            {
                mSkillUnSealDict_Giant.Add(e, false);
                mSkillMountDict_Giant.Add(e, -1);
            }
        }
    }

    /// <summary>
    /// 플레이어 데이터를 저장하는 메소드
    /// </summary>
    public static void Save()
    {
        Dictionary<string, Dictionary<string, string>> stringDict = new Dictionary<string, Dictionary<string, string>>();
        Dictionary<string, string> playerDict = new Dictionary<string, string>();
        stringDict.Add("Player", playerDict);

        playerDict.Add("Difficulty", Difficulty.ToString());
        playerDict.Add("Level", Level.ToString());
        playerDict.Add("Exp", Exp.ToString());
        playerDict.Add("Souls", Souls.ToString());
        playerDict.Add("Fairy Moving Speed", mFloatAbilityDict_Promotion[EFloatAbility.MoveSpeed_Fairy].ToString());
        playerDict.Add("Fairy Casting Speed", mFloatAbilityDict_Promotion[EFloatAbility.CastSpeed_Fairy].ToString());
        playerDict.Add("Fairy Maximum Mana", mFloatAbilityDict_Promotion[EFloatAbility.MaxMana].ToString());
        playerDict.Add("Fairy Mana Regeneration Per Second", mFloatAbilityDict_Promotion[EFloatAbility.ManaRps].ToString());
        playerDict.Add("Fairy Purification", mIntAbilityDict_Promotion[EIntAbility.Purification].ToString());
        playerDict.Add("Fairy Critical Hit Chance", mFloatAbilityDict_Promotion[EFloatAbility.CriticalChance_Fairy].ToString());
        playerDict.Add("Fairy Critical Hit Magnification", mFloatAbilityDict_Promotion[EFloatAbility.CriticalMag_Fairy].ToString());

        for(EElementalType type = EElementalType.None + 1; type < EElementalType.Count; type++)
        {
            string typeString = type.ToString();
            string spiritElement = "Fairy " + typeString + " EA";
            playerDict.Add(spiritElement, mElementalAffinityDict_Fairy_Promotion[type].ToString());
        }

        playerDict.Add("Giant Moving Speed", mFloatAbilityDict_Promotion[EFloatAbility.MoveSpeed_Giant].ToString());
        playerDict.Add("Giant Casting Speed", mFloatAbilityDict_Promotion[EFloatAbility.CastSpeed_Giant].ToString());
        playerDict.Add("Giant Maximum Life", mFloatAbilityDict_Promotion[EFloatAbility.MaxLife].ToString());
        playerDict.Add("Giant Life Regeneration Per Second", mFloatAbilityDict_Promotion[EFloatAbility.LifeRps].ToString());
        playerDict.Add("Giant Exorcism", mIntAbilityDict_Promotion[EIntAbility.Exorcism].ToString());
        playerDict.Add("Giant Critical Hit Chance", mFloatAbilityDict_Promotion[EFloatAbility.CriticalChance_Giant].ToString());
        playerDict.Add("Giant Critical Hit Magnification", mFloatAbilityDict_Promotion[EFloatAbility.CriticalMag_Giant].ToString());

        for(EElementalType type = EElementalType.None + 1; type < EElementalType.Count; type++)
        {
            string typeString = type.ToString();
            string predatorElement = "Giant " + typeString + " EA";
            playerDict.Add(predatorElement, mElementalAffinityDict_Giant_Promotion[type].ToString());
        }

        CsvWriter.Write(stringDict, Application.streamingAssetsPath + "/PlayerData.csv");

        stringDict.Clear();

        for(ESkillType_Fairy_Player e = ESkillType_Fairy_Player.None + 1; e < ESkillType_Fairy_Player.Count; e++)
        {
            Dictionary<string, string> skillDict = new Dictionary<string, string>();
            skillDict.Add("UnSeal", mSkillUnSealDict_Fairy[e].ToString());
            skillDict.Add("Mount", mSkillMountDict_Fairy[e].ToString());
            stringDict.Add(e.ToString(), skillDict);
        }
        
        for(ESkillType_Giant_Player e = ESkillType_Giant_Player.None + 1; e < ESkillType_Giant_Player.Count; e++)
        {
            Dictionary<string, string> skillDict = new Dictionary<string, string>();
            skillDict.Add("UnSeal", mSkillUnSealDict_Giant[e].ToString());
            skillDict.Add("Mount", mSkillMountDict_Giant[e].ToString());
            stringDict.Add(e.ToString(), skillDict);
        }

        CsvWriter.Write(stringDict, Application.streamingAssetsPath + "/PlayerData_Skill.csv");
    }

    /// <summary>
    /// 정수형 어빌리티 상승 횟수 반환 메소드
    /// </summary>
    /// <param name="e"></param>
    /// <returns></returns>
    public static int GetAbility_Promotion(EIntAbility e)
    {
        return mIntAbilityDict_Promotion[e];
    }

    /// <summary>
    /// 실수형 어빌리티 상승 횟수 반환 메소드
    /// </summary>
    /// <param name="e"></param>
    /// <returns></returns>
    public static int GetAbility_Promotion(EFloatAbility e)
    {
        return mFloatAbilityDict_Promotion[e];
    }

    /// <summary>
    /// 정수형 어빌리티 계수 반환 메소드
    /// </summary>
    /// <param name="e"></param>
    /// <returns></returns>
    public static int GetAbility_Coefficient(EIntAbility e)
    {
        return mIntAbilityDict_Coefficient[e];
    }

    /// <summary>
    /// 실수형 어빌리티 계수 반환 메소드
    /// </summary>
    /// <param name="e"></param>
    /// <returns></returns>
    public static float GetAbility_Coefficient(EFloatAbility e)
    {
        return mFloatAbilityDict_Coefficient[e];
    }
    
    /// <summary>
    /// 정수형 어빌리티 반환 메소드
    /// </summary>
    /// <param name="e"></param>
    /// <returns></returns>
    public static int GetAbility(EIntAbility e)
    {
        return GetAbility_Promotion(e) * GetAbility_Coefficient(e);
    }

    /// <summary>
    /// 실수형 어빌리티 반환 메소드
    /// </summary>
    /// <param name="e"></param>
    /// <returns></returns>
    public static float GetAbility(EFloatAbility e)
    {
        return GetAbility_Promotion(e) * GetAbility_Coefficient(e);
    }

    /// <summary>
    /// 정수형 어빌리티 상승 설정 메소드
    /// </summary>
    /// <param name="e"></param>
    /// <param name="value"></param>
    public static void SetAbility_Promotion(EIntAbility e, int value)
    {
        if(value < 0)
        {
            return;
        }

        int increase = value - mIntAbilityDict_Promotion[e];
        int promotion = Promotion;

        if(increase > promotion)
        {
            value = increase - promotion;
        }

        mIntAbilityDict_Promotion[e] = value;
    }

    /// <summary>
    /// 실수형 어빌리티 상승 설정 메소드
    /// </summary>
    /// <param name="e"></param>
    /// <param name="value"></param>
    public static void SetAbility_Promotion(EFloatAbility e, int value)
    {
        if (value < 0)
        {
            return;
        }

        int increase = value - mFloatAbilityDict_Promotion[e];
        int promotion = Promotion;

        if (increase > promotion)
        {
            value = increase - promotion;
        }

        mFloatAbilityDict_Promotion[e] = value;
    }

    /// <summary>
    /// 괴수의 속성 친화도 상승 횟수 반환 메소드
    /// </summary>
    /// <param name="e"></param>
    /// <returns></returns>
    public static int GetEA_Giant_Promotion(EElementalType e)
    {
        return mElementalAffinityDict_Giant_Promotion[e];
    }
    /// <summary>
    /// 괴수의 속성 친화도 계수 반환 메소드
    /// </summary>
    /// <param name="e"></param>
    /// <returns></returns>
    public static int GetEA_Giant_Coefficient(EElementalType e)
    {
        return mElementalAffinityDict_Giant_Coefficient[e];
    }
    /// <summary>
    /// 괴수의 속성 친화도 반환 메소드
    /// </summary>
    /// <param name="e"></param>
    /// <returns></returns>
    public static int GetEA_Giant(EElementalType e)
    {
        return GetEA_Giant_Promotion(e) * GetEA_Giant_Coefficient(e);
    }

    /// <summary>
    /// 괴수의 속성 친화도 상승 횟수 설정 메소드
    /// </summary>
    /// <param name="e"></param>
    /// <param name="value"></param>
    public static void SetEA_Giant_Promotion(EElementalType e, int value)
    {
        if (value < 0)
        {
            return;
        }

        int increase = value - mElementalAffinityDict_Giant_Promotion[e];
        int promotion = Promotion;

        if (increase > promotion)
        {
            value -= increase - promotion;
        }

        mElementalAffinityDict_Giant_Promotion[e] = value;
    }

    /// <summary>
    /// 정령의 속성 친화도 상승 횟수 반환 메소드
    /// </summary>
    /// <param name="e"></param>
    /// <returns></returns>
    public static int GetEA_Fairy_Promotion(EElementalType e)
    {
        return mElementalAffinityDict_Giant_Promotion[e];
    }
    /// <summary>
    /// 정령의 속성 친화도 계수 반환 메소드
    /// </summary>
    /// <param name="e"></param>
    /// <returns></returns>
    public static int GetEA_Fairy_Coefficient(EElementalType e)
    {
        return mElementalAffinityDict_Fairy_Coefficient[e];
    }
    /// <summary>
    /// 정령의 속성 친화도 반환 메소드
    /// </summary>
    /// <param name="e"></param>
    /// <returns></returns>
    public static int GetEA_Fairy(EElementalType e)
    {
        return GetEA_Fairy_Promotion(e) * GetEA_Fairy_Coefficient(e);
    }
    /// <summary>
    /// 정령의 속성 친화도 상승 횟수 설정 메소드
    /// </summary>
    /// <param name="e"></param>
    /// <param name="value"></param>
    public static void SetEA_Fairy_Promotion(EElementalType e, int value)
    {
        if(value < 0)
        {
            return;
        }

        int increase = value - mElementalAffinityDict_Fairy_Promotion[e];
        int promotion = Promotion;
        
        if(increase > promotion)
        {
            value -= increase - promotion;
        }

        mElementalAffinityDict_Fairy_Promotion[e] = value;
    }

    /// <summary>
    /// 요정 스킬이 봉인 해제된 상태인지 반환하는 메소드
    /// </summary>
    /// <param name="e"></param>
    /// <returns></returns>
    public static bool GetSkillUnSeal(ESkillType_Fairy_Player e)
    {
        return mSkillUnSealDict_Fairy[e];
    }

    /// <summary>
    /// 요정 스킬 봉인 해제 여부를 설정하는 메소드
    /// </summary>
    /// <param name="e"></param>
    /// <param name="value"></param>
    public static void SetSkillUnSeal(ESkillType_Fairy_Player e, bool value)
    {
        mSkillUnSealDict_Fairy[e] = value;
    }

    /// <summary>
    /// 자이언트 스킬이 봉인 해제된 상태인지 반환하는 메소드
    /// </summary>
    /// <param name="e"></param>
    /// <returns></returns>
    public static bool GetSkillUnSeal(ESkillType_Giant_Player e)
    {
        return mSkillUnSealDict_Giant[e];
    }

    /// <summary>
    /// 자이언트 스킬 봉인 해제 여부를 설정하는 메소드
    /// </summary>
    /// <param name="e"></param>
    /// <param name="value"></param>
    public static void SetSkillUnSeal(ESkillType_Giant_Player e, bool value)
    {
        mSkillUnSealDict_Giant[e] = value;
    }

    /// <summary>
    /// 요정 스킬이 어디에 장착중인지 반환하는 메소드
    /// </summary>
    /// <param name="e"></param>
    /// <returns></returns>
    public static int GetSkillMount(ESkillType_Fairy_Player e)
    {
        return mSkillMountDict_Fairy[e];
    }

    /// <summary>
    /// 요정 스킬 장착을 설정하는 메소드
    /// </summary>
    /// <param name="e"></param>
    /// <param name="value"></param>
    public static void SetSkillMount(ESkillType_Fairy_Player e, int value)
    {
        mSkillMountDict_Fairy[e] = value;
    }

    /// <summary>
    /// 자이언트 스킬이 어디에 장착중인지 반환하는 메소드
    /// </summary>
    /// <param name="e"></param>
    /// <returns></returns>
    public static int GetSkillMount(ESkillType_Giant_Player e)
    {
        return mSkillMountDict_Giant[e];
    }

    /// <summary>
    /// 자이언트 스킬 장착을 설정하는 메소드
    /// </summary>
    /// <param name="e"></param>
    /// <param name="value"></param>
    public static void SetSkillMount(ESkillType_Giant_Player e, int value)
    {
        mSkillMountDict_Giant[e] = value;
    }
}
