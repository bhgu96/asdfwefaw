﻿using UnityEngine;
using System.Collections;

/// <summary>
/// 이벤트 큐에 저장되는 이벤트 정보
/// </summary>
public struct TimeEventInfo
{
    public TimeEventInfo(float maintainTime, bool isScaledTime, TimeEvent.Action onStay, TimeEvent.Action onExit)
    {
        RemainTime = maintainTime;
        IsScaledTime = isScaledTime;
        OnStay = onStay;
        OnExit = onExit;
    }
    /// <summary>
    /// 남은 시간
    /// </summary>
    public float RemainTime;
    /// <summary>
    /// 지속시 이벤트
    /// </summary>
    public TimeEvent.Action OnStay;
    /// <summary>
    /// 탈출시 이벤트
    /// </summary>
    public TimeEvent.Action OnExit;
    /// <summary>
    /// ScaledTime 사용 유무
    /// </summary>
    public bool IsScaledTime;
}
