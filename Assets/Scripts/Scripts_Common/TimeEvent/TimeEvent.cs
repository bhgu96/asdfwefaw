﻿using UnityEngine;
using System.Collections.Generic;

public class TimeEvent : MonoBehaviour
{
    public static float DeltaTime
    {
        get
        {
            return Time.deltaTime;
        }
    }
    public static float UnscaledDeltaTime
    {
        get
        {
            if(Time.timeScale > 0.0f)
            {
                return Time.unscaledDeltaTime;
            }
            return 0.0f;
        }
    }

    /// <summary>
    /// 이벤트 메소드 원형
    /// </summary>
    public delegate void Action();

    // 예약된 모든 이벤트를 저장하는 큐
    private static Queue<TimeEventInfo> mEventQueue;

    /// <summary>
    /// 시간 이벤트를 설정하는 메소드
    /// </summary>
    /// <param name="maintainTime">지속 시간</param>
    /// <param name="onStay">시간 유지시 호출</param>
    /// <param name="onExit">시간 만료시 호출</param>
    public static TimeEventInfo SetTimeEvent(float maintainTime, bool isScaledTime, Action onStay, Action onExit)
    {
        TimeEventInfo eventInfo = new TimeEventInfo(maintainTime, isScaledTime, onStay, onExit);
        mEventQueue.Enqueue(eventInfo);
        return eventInfo;
    }

    private void Awake()
    {
        if (mEventQueue == null)
        {
            DontDestroyOnLoad(this);
            mEventQueue = new Queue<TimeEventInfo>();
        }
        else
        {
            Destroy(this);
        }
    }

    private void Update()
    {
        if (Time.timeScale > 0.0f)
        {
            int countOfEvent = mEventQueue.Count;
            for (int i = 0; i < countOfEvent; i++)
            {
                TimeEventInfo currentEvent = mEventQueue.Dequeue();
                if (currentEvent.RemainTime > 0.0f)
                {
                    if (!currentEvent.IsScaledTime)
                    {
                        currentEvent.RemainTime -= Time.unscaledDeltaTime;
                    }
                    else
                    {
                        currentEvent.RemainTime -= Time.deltaTime;
                    }
                    currentEvent.OnStay?.Invoke();
                    mEventQueue.Enqueue(currentEvent);
                }
                else
                {
                    currentEvent.OnExit?.Invoke();
                }
            }
        }
    }
}
