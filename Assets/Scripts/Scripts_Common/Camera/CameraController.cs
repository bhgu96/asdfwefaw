﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections.Generic;
using System.Collections;

public enum ECameraType
{
    None = -1,

    // 텅 빈 공간
    Void,
    // 배경
    Background,
    // 터레인
    Terrain,
    // 주 대상
    Object,
    // 포스트 프로세스
    PostProcess,
    // 인터페이스
    UI,
    // 오버레이
    Overlay,

    Count
}

public class CameraController : MonoBehaviour
{
    #region 용모 성역에서 사용할 임시 추가
    [SerializeField]
    private float mSmoother;
    [SerializeField]
    private bool mIsSanctuary;
    private bool mIsMove;
    #endregion

    /// <summary>
    /// 씬 내의 메인 카메라
    /// </summary>
    public static CameraController Main { get; private set; } = null;

    [SerializeField]
    [Tooltip("Camera의 수직 크기")]
    private float mVerticalSize = 12.0f;
    [SerializeField]
    [Tooltip("Horizontal Trigger의 Vertical 크기 비율")]
    [Range(0.0f, 1.0f)]
    private float mVerticalSizeOfHorizontalTrigger;
    [SerializeField]
    [Tooltip("Vectical Trigger의 Horizontal 크기 비율")]
    [Range(0.0f, 1.0f)]
    private float mHorizontalSizeOfVerticalTrigger;

    /// <summary>
    /// Camera의 수직 크기
    /// HorizontalSize에 의해 수정될 수 있다.
    /// </summary>
    public float VerticalSize
    {
        get { return mVerticalSize; }
        set
        {
            float coefficient = value / mVerticalSize;
            mTarget.BiasThreshold *= coefficient;
            mTarget.AreaLeftBottom *= coefficient;
            mTarget.AreaRightTop *= coefficient;
                
            mVerticalSize = value;

            // 모든 카메라 요소의 수직 크기를 갱신
            for (int i = 0; i < mCameraComponents.Length; i++)
            {
                mCameraComponents[i].Camera.orthographicSize = mVerticalSize;
            }

            float horizontalSize = HorizontalSize;

            // Target의 Trigger 갱신
            mTarget.ColliderHorizontal.size = new Vector2(horizontalSize * 2.0f, mVerticalSize * mVerticalSizeOfHorizontalTrigger);
            mTarget.ColliderVertical.size = new Vector2(horizontalSize * mHorizontalSizeOfVerticalTrigger, mVerticalSize * 2.0f);
        }
    }
    /// <summary>
    /// Camera의 수평 크기
    /// VerticalSize에 의해 수정될 수 있다. 
    /// </summary>
    public float HorizontalSize
    {
        get
        {
            Camera camera = mCameraComponents[0].Camera;
            // 수평 크기 = 수직 크기 / 카메라 수직 픽셀 수 x 카메라 수평 픽셀 수
            // 카메라의 수직, 수평의 비율은 픽셀 수의 수직, 수평 비율과 같다.
            return mVerticalSize / camera.pixelHeight * camera.pixelWidth;
        }
        set
        {
            Camera camera = mCameraComponents[0].Camera;
            // 수직 크기 = 수평 크기 / 카메라 수평 픽셀 수 x 카메라 수직 픽셀 수
            // 카메라의 수직, 수평의 비율은 픽셀 수의 수직, 수평 비율과 같다.
            VerticalSize = value / camera.pixelWidth * camera.pixelHeight;
        }
    }

    [SerializeField]
    [Tooltip("Camera 이동의 부드러움 정도 \n값을 높게 설정하면 Target Area와 충돌 현상이 발생할 수 있음")]
    private float mSmoothness;
    /// <summary>
    /// Camera 이동의 부드러움 정도
    /// 값을 높게 설정하면 Target Anchor와 충돌 현상이 발생할 수 있음
    /// </summary>
    public float Smoothness
    {
        get { return mSmoothness; }
        set
        {
            if (value >= 1.0f)
            {
                mSmoothness = value;
            }
            else
            {
                mSmoothness = 1.0f;
            }
        }
    }

    [SerializeField]
    [Tooltip("카메라의 타겟")]
    private CameraTarget mTarget;
    /// <summary>
    /// 카메라의 타겟
    /// </summary>
    public CameraTarget Target { get { return mTarget; } }

    private CameraComponent[] mCameraComponents;
    private Dictionary<ECameraType, CameraComponent> mCameraDict;
    public Vector2 PrevPosition { get; set; }

    #region Shake Field
    /// <summary>
    /// Shake 기능을 수행중인가
    /// </summary>
    public bool IsShakeTriggered { get; private set; }
    private float mShakeRemainTime;
    private float mShakePower;
    private Vector2 mShakeDstPosition;
    private float mShakeCycleRemainTime;
    /// <summary>
    /// 카메라 진동 주기
    /// </summary>
    [SerializeField]
    [Tooltip("카메라 진동 주기")]
    private float mShakeCycle = 0.05f;
    #endregion

    #region BlackOut Field
    [SerializeField]
    [Tooltip("Black Out 용 패널")]
    private Image mBlackOutPanel;
    /// <summary>
    /// Black Out 기능을 수행중인가
    /// </summary>
    public bool IsBlackOutTriggered { get; private set; }
    private float mBlackOutRemainTime;
    private float mBlackOutTime;
    private float mBlackOutSrcIntensity;
    private float mBlackOutDstIntensity;
    #endregion

    #region WhiteOut Field
    [SerializeField]
    [Tooltip("White Out 용 패널")]
    private Image mWhiteOutPanel;
    /// <summary>
    /// White Out 기능을 수행중인가
    /// </summary>
    public bool IsWhiteOutTriggered { get; private set; }
    private float mWhiteOutRemainTime;
    private float mWhiteOutTime;
    private float mWhiteOutSrcIntensity;
    private float mWhiteOutDstIntensity;
    #endregion

    #region LetterBox Field
    /// <summary>
    /// LetterBox 기능을 수행중인가
    /// </summary>
    public bool IsLetterBoxTriggered { get; private set; }
    private float mLetterBoxRemainTime;
    private float mLetterBoxTime;
    private Rect mLetterBoxSrcRect;
    private Rect mLetterBoxDstRect;
    private float mLetterBoxSrcVerticalSize;
    private float mLetterBoxDstVerticalSize;
    #endregion

    #region Zoom Field
    /// <summary>
    /// Zoom 기능을 수행중인가
    /// </summary>
    public bool IsZoomTriggered { get; private set; }
    private float mZoomTime;
    private float mZoomRemainTime;
    private float mZoomSrcVerticalSize;
    private float mZoomDstVerticalSize;
    #endregion

    /// <summary>
    /// 전달된 타입의 카메라를 반환하는 메소드
    /// </summary>
    /// <param name="type"></param>
    /// <returns></returns>
    public Camera GetCamera(ECameraType type)
    {
        return mCameraDict[type].Camera;
    }

    /// <summary>
    /// Shake 기능을 실행시키는 메소드
    /// </summary>
    /// <param name="time"></param>
    /// <param name="power"></param>
    public void Shake(float time, float power)
    {
        IsShakeTriggered = true;
        
        mShakeRemainTime = time;
        mShakeCycleRemainTime = mShakeCycle;

        mShakePower = power;
    }

    /// <summary>
    /// BlackOut 기능을 실행시키는 메소드
    /// </summary>
    /// <param name="time"></param>
    /// <param name="intensity"></param>
    public void BlackOut(float time, float intensity)
    {
        IsBlackOutTriggered = true;

        mBlackOutTime = time;
        mBlackOutRemainTime = mBlackOutTime;

        mBlackOutSrcIntensity = mBlackOutPanel.color.a;
        mBlackOutDstIntensity = intensity;
    }

    /// <summary>
    /// WhiteOut 기능을 실행시키는 메소드
    /// </summary>
    /// <param name="time"></param>
    /// <param name="intensity"></param>
    public void WhiteOut(float time, float intensity)
    {
        IsWhiteOutTriggered = true;

        mWhiteOutTime = time;
        mWhiteOutRemainTime = mWhiteOutTime;

        mWhiteOutSrcIntensity = mWhiteOutPanel.color.a;
        mWhiteOutDstIntensity = intensity;
    }

    /// <summary>
    /// LetterBox 기능을 실행시키는 메소드
    /// </summary>
    /// <param name="time"></param>
    /// <param name="cut"></param>
    public void LetterBox(float time, float boxSize)
    {
        IsLetterBoxTriggered = true;

        mLetterBoxTime = time;
        mLetterBoxRemainTime = mLetterBoxTime;

        Camera camera = mCameraDict[ECameraType.Background].Camera;
        mLetterBoxSrcRect = camera.rect;
        mLetterBoxDstRect = new Rect(mLetterBoxSrcRect.x, (1.0f - boxSize) * 0.5f, mLetterBoxSrcRect.width, boxSize);

        mLetterBoxSrcVerticalSize = VerticalSize;
        mLetterBoxDstVerticalSize = mLetterBoxSrcVerticalSize * mLetterBoxDstRect.height / mLetterBoxSrcRect.height;
    }

    /// <summary>
    /// Zoom 기능을 실행시키는 메소드
    /// </summary>
    /// <param name="time"></param>
    /// <param name="verticalSize"></param>
    public void Zoom(float time, float verticalSize)
    {
        IsZoomTriggered = true;

        mZoomTime = time;
        mZoomRemainTime = mZoomTime;

        mZoomSrcVerticalSize = VerticalSize;
        mZoomDstVerticalSize = verticalSize;
    }

    /// <summary>
    /// Shake 기능이 활성화되면 매 프레임마다 Shake 좌표를 반환하는 메소드
    /// </summary>
    /// <returns></returns>
    private Vector2 ExecuteShake()
    {
        Vector2 shakePosition = Vector2.zero;

        // Cycle 시간이 남았다면
        if(mShakeCycleRemainTime > 0.0f)
        {
            mShakeCycleRemainTime -= Time.deltaTime;
            float shakeCycleHalf = mShakeCycle * 0.5F;
            // Cycle 시간이 주기의 반보다 크다면
            if(mShakeCycleRemainTime > shakeCycleHalf)
            {
                shakePosition = Vector2.Lerp(Vector2.zero, mShakeDstPosition, 
                    // (주기 - 남은 시간) / (주기 x 0.5)
                    (mShakeCycle - mShakeCycleRemainTime) / shakeCycleHalf);
            }
            // Cycle 시간이 주기의 반보다 작다면
            else
            {
                shakePosition = Vector2.Lerp(mShakeDstPosition, Vector2.zero,
                    // (주기 x 0.5 - 남은 시간) / (주기 x 0.5)
                    (shakeCycleHalf - mShakeCycleRemainTime) / shakeCycleHalf);
            }
        }
        // Cycle 시간이 0이하라면
        else
        {
            // cycle 남는 시간은 0 이하의 값을 가지기 때문에 주기에 [-] 연산을 취함
            mShakeRemainTime -= mShakeCycle - mShakeCycleRemainTime;
            // Shake 시간이 남았다면
            if(mShakeRemainTime > 0.0f)
            {
                mShakeCycleRemainTime = mShakeCycle;
                mShakeDstPosition = new Vector2(Random.Range(-1.0f, 1.0f), Random.Range(-1.0f, 1.0f));
                mShakeDstPosition.Normalize();
                mShakeDstPosition *= mShakePower;
            }
            // Shake 시간이 남지 않았다면
            else
            {
                // Shake 기능을 끝낸다.
                IsShakeTriggered = false;
            }
        }
        
        return shakePosition;
    }

    /// <summary>
    /// BlackOut 기능이 활성화되면 매 프레임마다 BlackOut 기능을 수행하는 메소드
    /// </summary>
    private void ExecuteBlackOut()
    {
        // BlackOut 시간이 남아있다면
        if(mBlackOutRemainTime > 0.0f)
        {
            // 남은 시간을 감소
            mBlackOutRemainTime -= Time.deltaTime;
            // BlackOut 패널의 알파값 갱신
            mBlackOutPanel.color = new Color(0.0f, 0.0f, 0.0f,
                Mathf.Lerp(mBlackOutSrcIntensity, mBlackOutDstIntensity, 
                (mBlackOutTime - mBlackOutRemainTime) / mBlackOutTime));
        }
        // BlackOut 시간이 남아있지 않다면
        else
        {
            // BlackOut 패널의 알파값 갱신
            mBlackOutPanel.color = new Color(0.0f, 0.0f, 0.0f, mBlackOutDstIntensity);
            // BlackOut 완료
            IsBlackOutTriggered = false;
        }
    }
    /// <summary>
    /// WhiteOut 기능이 활성화되면 매 프레임마다 WhiteOut 기능을 수행하는 메소드
    /// </summary>
    private void ExecuteWhiteOut()
    {
        // WhiteOut 시간이 남아있다면
        if(mWhiteOutRemainTime > 0.0f)
        {
            // 남은 시간을 감소
            mWhiteOutRemainTime -= Time.deltaTime;
            // WhiteOut 패널의 알파값 갱신
            mWhiteOutPanel.color = new Color(1.0f, 1.0f, 1.0f,
                Mathf.Lerp(mWhiteOutSrcIntensity, mWhiteOutDstIntensity,
                (mWhiteOutTime - mWhiteOutRemainTime) / mWhiteOutTime));
        }
        // WhiteOut 시간이 남아닜지 않다면
        else
        {
            // Whiteout 패널의 알파값 갱신
            mWhiteOutPanel.color = new Color(1.0f, 1.0f, 1.0f, mWhiteOutDstIntensity);
            // WhiteOut 완료
            IsWhiteOutTriggered = false;
        }
    }
    /// <summary>
    /// LetterBox 기능이 활성화되면 매 프레임마다 LetterBox 기능을 수행하는 메소드
    /// 반드시 Zoom 기능을 수행한 뒤에 선언되어야 한다.
    /// </summary>
    private void ExecuteLetterBox()
    {
        // LetterBox 시간이 남았다면
        if(mLetterBoxRemainTime > 0.0f)
        {
            // 남은 시간을 감소
            mLetterBoxRemainTime -= Time.deltaTime;
            // Lerp 연산을 하기위한 Delta값
            float delta = (mLetterBoxTime - mLetterBoxRemainTime) / mLetterBoxTime;
            // 현재 Rect
            Rect curRect = mCameraDict[ECameraType.Background].Camera.rect;
            // 갱신할 Rect
            Rect nextRect = new Rect(curRect.x, 
                Mathf.Lerp(mLetterBoxSrcRect.y, mLetterBoxDstRect.y, delta),
                curRect.width, 
                Mathf.Lerp(mLetterBoxSrcRect.height, mLetterBoxDstRect.height, delta));

            // Void 카메라를 제외한 모든 카메라에 대해
            for(ECameraType type = ECameraType.Void + 1; type < ECameraType.Count; type++)
            {
                // Rect를 갱신한다.
                mCameraDict[type].Camera.rect = nextRect;
            }
            // Rect 만 갱신하면 화면에 비춰지는 요소들이 작아져 보이기 때문에 VerticalSize를 갱신한다.
            VerticalSize = Mathf.Lerp(mLetterBoxSrcVerticalSize, mLetterBoxDstVerticalSize, delta);
        }
        // LetterBox 시간이 남아있지 않다면
        else
        {
            // 현재 Rect
            Rect curRect = mCameraDict[ECameraType.Background].Camera.rect;
            // 갱신할 Rect
            Rect nextRect = new Rect(curRect.x, mLetterBoxDstRect.y, curRect.width, mLetterBoxDstRect.height);

            // Void 카메라를 제외한 모든 카메라에 대해
            for(ECameraType type = ECameraType.Void + 1; type < ECameraType.Count; type++)
            {
                // Rect 를 갱신한다.
                mCameraDict[type].Camera.rect = nextRect;
            }
            
            VerticalSize = mLetterBoxDstVerticalSize;
            // LetterBox 기능 완료
            IsLetterBoxTriggered = false;
        }
    }
    /// <summary>
    /// Zoom 기능이 활성화되면 매 프레임마다 Zoom 기능을 수행하는 메소드
    /// 반드시 LetterBox 기능을 수행하기 전에 선언되어야 한다.
    /// </summary>
    private void ExecuteZoom()
    {
        // Zoom 기능 수행 시간이 남았다면
        if(mZoomRemainTime > 0.0f)
        {
            // 시간 감소
            mZoomRemainTime -= Time.deltaTime;
            // VerticalSize를 갱신
            VerticalSize = Mathf.Lerp(mZoomSrcVerticalSize, mZoomDstVerticalSize, (mZoomTime - mZoomRemainTime) / mZoomTime);
            // LetterBox 기능을 수행중이라면
            if(IsLetterBoxTriggered)
            {
                // LetterBox의 Src, Dst VerticalSize 를 갱신
                mLetterBoxSrcVerticalSize = VerticalSize;
                mLetterBoxDstVerticalSize = mLetterBoxSrcVerticalSize * mLetterBoxDstRect.height / mLetterBoxSrcRect.height;
            }
        }
        // Zoom 기능 수행 시간이 끝났다면
        else
        {
            // VerticalSize 갱신
            VerticalSize = mZoomDstVerticalSize;
            // LetterBox 기능을 수행중이라면
            if(IsLetterBoxTriggered)
            {
                // LetterBox의 Src, Dst VerticalSize를 갱신
                mLetterBoxSrcVerticalSize = VerticalSize;
                mLetterBoxDstVerticalSize = mLetterBoxSrcVerticalSize * mLetterBoxDstRect.height / mLetterBoxSrcRect.height;
            }
            // Zoom 기능 완료
            IsZoomTriggered = false;
        }
    }

    private void Awake()
    {
        // Tag 가 MainCamera 라면 Main 이 참조할 수 있도록 한다.
        if (gameObject.tag.Equals(GameTags.MainCamera))
        {
            Main = this;
        }

        mCameraComponents = GetComponentsInChildren<CameraComponent>(true);
        mCameraDict = new Dictionary<ECameraType, CameraComponent>();
        // 모든 Camera Component를 Camera Dictionary에 할당한다.
        for (int i = 0; i < mCameraComponents.Length; i++)
        {
            mCameraDict.Add(mCameraComponents[i].CameraType, mCameraComponents[i]);
        }
        
        PrevPosition = transform.position;
    }

    private void Start()
    {
        VerticalSize = mVerticalSize;
    }
    private void FixedUpdate()
    {
        // 목적지
        Vector2 destination = mTarget.Destination;

        #region Target Area를 고려하여 목적지 갱신
        // Target의 실제 좌표
        Vector2 targetPosition = mTarget.transform.position;
        // Target Area 의 좌측 하단 좌표
        Vector2 areaLeftBottom = targetPosition + mTarget.AreaLeftBottom;
        // TargetArea 의 우측 상단 좌표
        Vector2 areaRightTop = targetPosition + mTarget.AreaRightTop;
        // 이전 카메라 좌표
        Vector2 prevPosition = PrevPosition;
        // 다음 카메라 위치
        Vector2 nextPosition = prevPosition + (destination - prevPosition) / mSmoothness;

        // 다음 카메라 위치가 Area 좌측의 왼쪽에 위치하는 경우
        if (nextPosition.x < areaLeftBottom.x)
        {
            // Area 좌측의 x좌표로 갱신
            nextPosition = new Vector2(areaLeftBottom.x, nextPosition.y);
        }
        // 다음 카메라 위치가 Area 우측의 오른쪽에 위치하는 경우
        else if(nextPosition.x > areaRightTop.x)
        {
            // Area 우측의 x 좌표로 갱신
            nextPosition = new Vector2(areaRightTop.x, nextPosition.y);
        }

        // 다음 카메라 위치가 Area 하단 보다 더 밑에 위치하는 경우
        if(nextPosition.y < areaLeftBottom.y)
        {
            // Area 하단의 y좌표로 갱신
            nextPosition = new Vector2(nextPosition.x, areaLeftBottom.y);
        }
        // 다음 카메라 위치가 Area 상단 보다 더 위에 위치하는 경우
        else if(nextPosition.y > areaRightTop.y)
        {
            // Area 상단의 y 좌표로 갱신
            nextPosition = new Vector2(nextPosition.x, areaRightTop.y);
        }
        #endregion

        // Shake 기능이 적용되지 않은 좌표를 저장한다.
        PrevPosition = nextPosition;

        // Shake 기능을 수행중이라면
        if (IsShakeTriggered)
        {
            // 현재 좌표에 Shake 좌표를 추가한다.
            nextPosition += ExecuteShake();
        }

        //if(Mathf.Abs(mTarget.transform.position.y - this.transform.position.y) > 8f)
        //{
        //    transform.position = nextPosition;
        //}
        //else
        //{
        //    this.transform.position = new Vector3(nextPosition.x, this.transform.position.y);
        //}

        if(mIsSanctuary)
        {
            if(Mathf.Abs(mTarget.transform.position.x - this.transform.position.x) > 6f || Mathf.Abs(mTarget.transform.position.y - this.transform.position.y) > 6f)
            {
                mIsMove = true;
                this.transform.position = Vector3.Lerp(this.transform.position, nextPosition, mSmoother * Time.fixedDeltaTime);
            }
            else if(mIsMove)
            {
                if(Mathf.Abs(this.transform.position.x - nextPosition.x) < 2f)
                {
                    mIsMove = false;
                }
                
                this.transform.position = Vector3.Lerp(this.transform.position, nextPosition, mSmoother * Time.fixedDeltaTime);
            }
        }
        else
        {
            // 카메라 좌표를 갱신한다.
            transform.position = nextPosition;
        }

        // BlackOut 기능을 수행중이라면
        if (IsBlackOutTriggered)
        {
            ExecuteBlackOut();
        }
        // WhiteOut 기능을 수행중이라면
        if (IsWhiteOutTriggered)
        {
            ExecuteWhiteOut();
        }
        //LetterBox 기능을 수행중이라면
        if (IsLetterBoxTriggered)
        {
            ExecuteLetterBox();
        }
        // Zoom 기능을 수행중이라면
        if (IsZoomTriggered)
        {
            ExecuteZoom();
        }
    }

    private void OnDestroy()
    {
        // Tag가 MainCamera 라면 Main이 참조하지 못하도록 한다.
        if (gameObject.tag.Equals(GameTags.MainCamera))
        {
            Main = null;
        }
    }

    private void OnDrawGizmos()
    {
        Camera[] cameras = GetComponentsInChildren<Camera>();
        for(int i=0; i<cameras.Length; i++)
        {
            cameras[i].orthographicSize = mVerticalSize;
        }
        if (cameras.Length > 0)
        {
            Camera camera = cameras[0];
            float horizontalSize = mVerticalSize / camera.pixelHeight * camera.pixelWidth;

            if(mTarget != null)
            {
                mTarget.ColliderHorizontal.size = new Vector2(horizontalSize * 2.0f, mVerticalSize * mVerticalSizeOfHorizontalTrigger);
                mTarget.ColliderVertical.size = new Vector2(horizontalSize * mHorizontalSizeOfVerticalTrigger, mVerticalSize * 2.0f);
            }
        }
    }
}
