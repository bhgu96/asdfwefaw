﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraTest : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        CameraController.Main.Shake(5.0f, 1.0f);
    }
    private bool mIsShakeEnd;
    private bool mIsBlackEnd0;
    private bool mIsBlackEnd1;
    private bool mIsWhiteEnd0;
    private bool mIsWhiteEnd1;
    private bool mIsLetterEnd0;
    private bool mIsLetterEnd1;
    private bool mIsZoomEnd0;
    private bool mIsZoomEnd1;

    
    // Update is called once per frame
    void Update()
    {
        if(!mIsShakeEnd)
        {
            if(!CameraController.Main.IsShakeTriggered)
            {
                mIsShakeEnd = true;
                CameraController.Main.BlackOut(2.0f, 1.0f);
            }
        }
        else if(!mIsBlackEnd0)
        {
            if(!CameraController.Main.IsBlackOutTriggered)
            {
                mIsBlackEnd0 = true;
                CameraController.Main.BlackOut(2.0f, 0.0f);
            }
        }
        else if(!mIsBlackEnd1)
        {
            if(!CameraController.Main.IsBlackOutTriggered)
            {
                mIsBlackEnd1 = true;
                CameraController.Main.WhiteOut(2.0f, 1.0f);
            }
        }
        else if(!mIsWhiteEnd0)
        {
            if(!CameraController.Main.IsWhiteOutTriggered)
            {
                mIsWhiteEnd0 = true;
                CameraController.Main.WhiteOut(2.0f, 0.0f);
            }
        }
        else if(!mIsWhiteEnd1)
        {
            if(!CameraController.Main.IsWhiteOutTriggered)
            {
                mIsWhiteEnd1 = true;
                CameraController.Main.LetterBox(2.0f, 0.8f);
            }
        }
        else if(!mIsLetterEnd0)
        {
            if(!CameraController.Main.IsLetterBoxTriggered)
            {
                mIsLetterEnd0 = true;
                CameraController.Main.LetterBox(2.0f, 1.0f);
            }
        }
        else if(!mIsLetterEnd1)
        {
            if(!CameraController.Main.IsLetterBoxTriggered)
            {
                mIsLetterEnd1 = true;
                CameraController.Main.Zoom(2.0f, 6.0f);
            }
        }
        else if(!mIsZoomEnd0)
        {
            if(!CameraController.Main.IsZoomTriggered)
            {
                mIsZoomEnd0 = true;
                CameraController.Main.Zoom(2.0f, 12.0f);
            }
        }
        else if(!mIsZoomEnd1)
        {
            if(!CameraController.Main.IsZoomTriggered)
            {
                mIsZoomEnd1 = true;
                gameObject.SetActive(false);
            }
        }
    }
}
