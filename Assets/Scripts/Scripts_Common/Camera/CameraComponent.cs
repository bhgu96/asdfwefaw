﻿using UnityEngine;
using System.Collections;

public class CameraComponent : MonoBehaviour
{ 
    public ECameraType CameraType { get { return mCameraType; } }
    [SerializeField]
    private ECameraType mCameraType;

    public Camera Camera { get; private set; }

    private void Awake()
    {
        Camera = GetComponent<Camera>();
        enabled = false;
    }
}
