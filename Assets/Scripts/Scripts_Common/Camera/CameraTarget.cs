﻿using UnityEngine;
using System.Collections;

public enum ETriggerType_CameraTarget
{
    Horizontal, Vertical
}

[RequireComponent(typeof(Rigidbody2D))]
public class CameraTarget : MonoBehaviour
{
    public Trigger_CameraTarget TriggerHorizontal { get; private set; }
    public BoxCollider2D ColliderHorizontal { get { return mColliderHorizontal; } }
    public Trigger_CameraTarget TriggerVertical { get; private set; }
    public BoxCollider2D ColliderVertical { get { return mColliderVertical; } }

    [SerializeField]
    private BoxCollider2D mColliderHorizontal;
    [SerializeField]
    private BoxCollider2D mColliderVertical;

    public Vector2 Destination { get; private set; }

    [SerializeField]
    [Tooltip("카메라가 따라다니는 대상의 Transform")]
    private Transform mFollow;
    public Transform Follow
    {
        get { return mFollow; }
        set { mFollow = value; }
    }
    private Vector2 mFollowLocalPosition;

    [SerializeField]
    [Tooltip("치우침의 한계")]
    private float mBiasThreshold;
    /// <summary>
    /// 치우침의 한계
    /// </summary>
    public float BiasThreshold
    {
        get { return mBiasThreshold; }
        set { mBiasThreshold = value; }
    }

    [SerializeField]
    [Tooltip("치우침이 적용되기 위한 최소의 움직임")]
    private float mBiasCondition;

    [SerializeField]
    [Tooltip("치우침의 속도")]
    private float mBiasSpeed;

    [SerializeField]
    [Tooltip("실제 카메라의 타겟이 되는 Transform\nBias에 의해 좌표가 변한다")]
    private Transform mBias;
    // Bias 방향
    private EHorizontalDirection mBiasDirection;
    // 치우침의 조건을 만족하기 위한 움직임
    private float mBiasMovement;
    // 이전 x좌표
    private float mPrevPosition_x;

    [SerializeField]
    [Tooltip("Target을 기준으로 절대 벗어나선 안되는 영역의 좌측 하단 좌표")]
    private Vector2 mAreaLeftBottom;
    /// <summary>
    /// Target을 기준으로 절대 벗어나선 안되는 영역의 좌측 하단 좌표
    /// </summary>
    public Vector2 AreaLeftBottom
    {
        get { return mAreaLeftBottom; }
        set { mAreaLeftBottom = value; }
    }
    [SerializeField]
    [Tooltip("Target을 기준으로 절대 벗어나선 안되는 영역의 우측 상단 좌표")]
    private Vector2 mAreaRightTop;
    /// <summary>
    /// Target을 기준으로 절대 벗어나선 안되는 영역의 우측 상단 좌표
    /// </summary>
    public Vector2 AreaRightTop
    {
        get { return mAreaRightTop; }
        set { mAreaRightTop = value; }
    }

    private void OnDrawGizmos()
    {
        Vector3 curPosition = transform.position;
        Gizmos.color = new Color(0.0f, 0.5f, 0.0f, 0.5f);
        Gizmos.DrawCube(new Vector3(curPosition.x + (mAreaLeftBottom.x + mAreaRightTop.x) * 0.5f,
            curPosition.y + (mAreaLeftBottom.y + mAreaRightTop.y) * 0.5f, 0.0f),
            new Vector3(mAreaRightTop.x - mAreaLeftBottom.x,
            mAreaRightTop.y - mAreaLeftBottom.y, 0.0f));
        Gizmos.color = new Color(0.5f, 0.0f, 0.0f, 0.5f);
        Gizmos.DrawLine(new Vector3(curPosition.x - mBiasThreshold, curPosition.y, 0.0f), new Vector3(curPosition.x + mBiasThreshold, curPosition.y, 0.0f));
    }

    private void Awake()
    {
        TriggerHorizontal = mColliderHorizontal.GetComponent<Trigger_CameraTarget>();
        TriggerVertical = mColliderVertical.GetComponent<Trigger_CameraTarget>();

        mPrevPosition_x = transform.position.x;
        Destination = transform.position;
    }

    private void FixedUpdate()
    {
        // 충돌시 갱신 값
        float position_x = 0.0f;
        // 충돌시 갱신 값
        float position_y = 0.0f;

        Collider2D minCollider = null;
        Collider2D maxCollider = null;
        Vector2 curPosition = mBias.position;
        Collider2D[] colliders = TriggerHorizontal.GetColliders();
        float minPosition = 0.0f;
        float maxPosition = 0.0f;

        #region 수평 콜라이더 갱신
        // 모든 감지된 수평 콜라이더에 대해
        for (int i = 0; i < colliders.Length && colliders[i] != null; i++)
        {
            position_x = colliders[i].transform.position.x;
            // 감지된 콜라이더가 좌측에 위치하는 경우
            if (position_x < curPosition.x)
            {
                // 좌측에서 가장 가까운 콜라이더보다 더 가까운 경우
                if (minCollider == null || minPosition < position_x)
                {
                    // 좌측의 가장 가까운 콜라이더와 위치를 갱신
                    minCollider = colliders[i];
                    minPosition = position_x;
                }
            }
            // 감지된 콜라이더가 우측에 위치하는 경우
            else
            {
                // 우측에서 가장 가까운 콜라이더보다 더 가까운 경우
                if(maxCollider == null || position_x < maxPosition)
                {
                    // 우측의 가장 가까운 콜라이더와 위치를 갱신
                    maxCollider = colliders[i];
                    maxPosition = position_x;
                }
            }
        }

        // 좌측에 콜라이더가 감지된 경우
        if(minCollider != null)
        {
            // 우측에 콜라이더가 감지된 경우
            if(maxCollider != null)
            {
                // 양쪽 콜라이더의 중간 지점
                position_x = (minCollider.bounds.max.x + maxCollider.bounds.min.x) * 0.5f;
            }
            // 우측에 콜라이더가 감지되지 않은 경우
            else
            {
                // 좌측 콜라이더의 오른쪽 bound.x + 트리거 사이즈 x * 0.5
                position_x = minCollider.bounds.max.x + ColliderHorizontal.size.x * 0.5f;
            }
        }
        // 좌측에 콜라이더가 감지되지 않았고, 우측에 콜라이더가 감지된 경우
        else if(maxCollider != null)
        {
            // 우측 콜라이더의 왼쪽 bound.x - 트리거 사이즈 x *0.5
            position_x = maxCollider.bounds.min.x - ColliderHorizontal.size.x * 0.5f;
        }
        // 우측, 좌측에 콜라이더가 감지되지 않은 경우
        else
        {
            // 현재 위치로 갱신
            position_x = curPosition.x;
        }
        #endregion

        #region 수직 콜래이더 갱신
        minCollider = null;
        maxCollider = null;
        colliders = TriggerVertical.GetColliders();
        // 모든 감지된 수직 콜라이더에 대해
        for(int i=0; i<colliders.Length && colliders[i] != null; i++)
        {
            position_y = colliders[i].transform.position.y;
            // 감지된 콜라이더가 하단에 위치한 경우
            if(position_y < curPosition.y)
            {
                // 하단의 가장 가까운 콜라이더보다 더 가까운 경우
                if(minCollider == null || minPosition < position_y)
                {
                    // 하단의 가장 가까운 콜라이더와 위치를 갱신
                    minCollider = colliders[i];
                    minPosition = position_y;
                }
            }
            // 감지된 콜라이더가 상단에 위치한 경우
            else
            {
                // 상단의 가장 가까운 콜라이더보다 더 가까운 경우
                if (maxCollider == null || position_y < maxPosition)
                {
                    // 상단위 가장 가까운 콜라이더와 위치를 갱신
                    maxCollider = colliders[i];
                    maxPosition = position_y;
                }
            }
        }

        // 하단에 콜라이더가 존재하는 경우
        if(minCollider != null)
        {
            // 상단에 콜라이더가 존재하는 경우
            if (maxCollider != null)
            {
                position_y = (minCollider.bounds.max.y + maxCollider.bounds.min.y) * 0.5f;
            }
            // 상단에 콜라이더가 존재하지 않는 경우
            else
            {
                position_y = minCollider.bounds.max.y + ColliderVertical.size.y * 0.5f;
            }
        }
        // 하단에 콜라이더가 존재하지 않고, 상단에 콜라이더가 존재하는 경우
        else if(maxCollider != null)
        {
            position_y = maxCollider.bounds.min.y - ColliderVertical.size.y * 0.5f;
        }
        // 상단과 하단에 콜라이더가 감지되지 않은 경우
        else
        {
            position_y = curPosition.y;
        }
        #endregion

        // 목적지 좌표 갱신
        Destination = new Vector2(position_x, position_y);


        // 따라가야되는 대상이 존재한다면
        if (mFollow != null)
        {
            // 현재 좌표를 Follow로 이동
            transform.position = mFollow.position;
        }

        #region 치우침 갱신
        curPosition = transform.position;
        // 현재 치우침
        float bias = mBias.localPosition.x;

        // Target이 오른쪽으로 이동한 경우
        if (mPrevPosition_x < curPosition.x)
        {
            // 치우침의 방향이 오른쪽인 경우
            if (mBiasDirection == EHorizontalDirection.Right)
            {
                // 치우침을 적용하기 위한 최소의 움직임을 만족하는 경우
                if (mBiasMovement > mBiasCondition)
                {
                    // 치우침 갱신
                    bias += mBiasSpeed * Time.deltaTime;
                }
                // 치우침을 적용하기 위한 움직임을 만족하지 못하는 경우
                else
                {
                    // 움직임 값 갱신
                    mBiasMovement += curPosition.x - mPrevPosition_x;
                }
            }
            // 치우침의 방향이 오른쪽이 아닌 경우
            else
            {
                mBiasDirection = EHorizontalDirection.Right;
                mBiasMovement = 0.0f;
                bias = 0.0f;
            }
        }
        // Target이 왼쪽으로 이동한 경우
        else if (mPrevPosition_x > curPosition.x)
        {
            // 치우침의 방향이 왼쪽인 경우
            if (mBiasDirection == EHorizontalDirection.Left)
            {
                // 치우침을 적용하기 위한 최소의 움직임을 만족하는 경우
                if (mBiasMovement > mBiasCondition)
                {
                    // 치우침 갱신
                    bias -= mBiasSpeed * Time.deltaTime;
                }
                // 치우침을 적용하기 위한 움직임을 만족하지 못하는 경우
                else
                {
                    // 움직임 값 갱신
                    mBiasMovement += mPrevPosition_x - curPosition.x;
                }
            }
            // 치우침의 방향이 왼쪽이 아닌 경우
            else
            {
                mBiasDirection = EHorizontalDirection.Left;
                mBiasMovement = 0.0f;
                bias = 0.0f;
            }
        }

        mPrevPosition_x = curPosition.x;

        // 치우침이 오른쪽 한계를 넘어서는 경우
        if (bias > mBiasThreshold)
        {
            bias = mBiasThreshold;
        }
        // 치우침이 왼쪽 한계를 넘어서는 경우
        else if (bias < -mBiasThreshold)
        {
            bias = -mBiasThreshold;
        }

        // 실제 치우침 값 갱신
        mBias.localPosition = new Vector3(bias, 0.0f, 0.0f);
        #endregion
    }
}
