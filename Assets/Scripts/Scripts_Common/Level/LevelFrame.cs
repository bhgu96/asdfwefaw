﻿using System.Collections.Generic;
using UnityEngine;

namespace Level
{
    public class LevelFrame : LevelLayer
    {
        [SerializeField]
        [Tooltip("소켓 정보")]
        private LevelSocket[] mSockets;

        private Dictionary<EDirection, Dictionary<ESocketType, Queue<LevelSocket>>> mSocketDict;

        public Transform Anchor_LeftBottom { get { return mAnchor_LeftBottom; } }
        [SerializeField]
        [Tooltip("좌측 하단 앵커")]
        private Transform mAnchor_LeftBottom;

        public Transform Anchor_RightTop { get { return mAnchor_RightTop; } }
        [SerializeField]
        [Tooltip("우측 상단 앵커")]
        private Transform mAnchor_RightTop;

        /// <summary>
        /// 현재 가리키는 소켓을 큐의 맨 뒷부분에 넣고 반환하는 메소드
        /// </summary>
        /// <param name="direction"></param>
        /// <param name="type"></param>
        /// <returns></returns>
        public LevelSocket MoveSocket(EDirection direction, ESocketType type)
        {
            Queue<LevelSocket> socketQueue = mSocketDict[direction][type];
            if (socketQueue.Count > 0)
            {
                LevelSocket socket = socketQueue.Dequeue();
                socketQueue.Enqueue(socket);
                return socket;
            }
            return null;
        }
        /// <summary>
        /// 현재 가리키는 소켓을 반환하는 메소드
        /// </summary>
        /// <param name="direction"></param>
        /// <param name="type"></param>
        /// <returns></returns>
        public LevelSocket GetSocket(EDirection direction, ESocketType type)
        {
            Queue<LevelSocket> socketQueue = mSocketDict[direction][type];
            if (socketQueue.Count > 0)
            {
                return socketQueue.Peek();
            }
            return null;
        }
        /// <summary>
        /// 해당하는 소켓을 가지고 있는지를 반환하는 메소드
        /// </summary>
        /// <param name="direction"></param>
        /// <param name="type"></param>
        /// <param name="count"></param>
        /// <returns></returns>
        public bool HasSocket(EDirection direction, ESocketType type, out int count)
        {
            Queue<LevelSocket> socketQueue = mSocketDict[direction][type];
            count = socketQueue.Count;
            return count > 0;
        }

        public LevelSocket DequeueSocket(EDirection direction, ESocketType type)
        {
            Queue<LevelSocket> socketQueue = mSocketDict[direction][type];
            if (socketQueue.Count > 0)
            {
                return socketQueue.Dequeue();
            }
            return null;
        }

        public LevelSocket DequeueSocket()
        {
            for (EDirection direction = EDirection.None + 1; direction < EDirection.Count; direction++)
            {
                for (ESocketType type = ESocketType.None + 1; type < ESocketType.Count; type++)
                {
                    Queue<LevelSocket> socketQueue = mSocketDict[direction][type];
                    if (socketQueue.Count > 0)
                    {
                        return socketQueue.Dequeue();
                    }
                }
            }

            return null;
        }

        public LevelSocket DequeueSocket(int tag)
        {
            Queue<LevelSocket> socketQueue = mSocketDict[mSockets[tag].Direction][mSockets[tag].Type];
            for (int i = 0; i < socketQueue.Count; i++)
            {
                LevelSocket socket = socketQueue.Dequeue();
                if (socket.Tag == tag)
                {
                    return socket;
                }
                socketQueue.Enqueue(socket);
            }
            return null;
        }

        public override void Initialize()
        {
            base.Initialize();

            mSocketDict = new Dictionary<EDirection, Dictionary<ESocketType, Queue<LevelSocket>>>();

            for (EDirection direction = EDirection.None + 1; direction < EDirection.Count; direction++)
            {
                Dictionary<ESocketType, Queue<LevelSocket>> socketTypeDict = new Dictionary<ESocketType, Queue<LevelSocket>>();
                mSocketDict.Add(direction, socketTypeDict);
                for (ESocketType type = ESocketType.None + 1; type < ESocketType.Count; type++)
                {
                    socketTypeDict.Add(type, new Queue<LevelSocket>());
                }
            }

            for (int i = 0; i < mSockets.Length; i++)
            {
                mSockets[i].Frame = this;
                mSockets[i].Tag = i;
                mSocketDict[mSockets[i].Direction][mSockets[i].Type].Enqueue(mSockets[i]);
            }
        }

        private void OnDrawGizmosSelected()
        {
            if (mAnchor_LeftBottom != null && mAnchor_RightTop != null)
            {
                Gizmos.color = new Color(1.0f, 0.7f, 0.0f, 0.2f);
                Vector3 leftBottom = mAnchor_LeftBottom.position;
                Vector3 rightTop = mAnchor_RightTop.position;
                Gizmos.DrawCube(new Vector3((leftBottom.x + rightTop.x) * 0.5f, (leftBottom.y + rightTop.y) * 0.5f), new Vector3(rightTop.x - leftBottom.x, rightTop.y - leftBottom.y));
            }

            
            for(int i=0; i<mSockets.Length; i++)
            {
                if (mSockets[i].Direction == EDirection.Right)
                {
                    Gizmos.color = new Color(0.0f, 1.0f, 0.0f, 0.6f);
                    Gizmos.DrawSphere(mSockets[i].Transform.position, 5.0f);
                }
                else if (mSockets[i].Direction == EDirection.Left)
                {
                    Gizmos.color = new Color(1.0f, 0.0f, 0.0f, 0.6f);
                    Gizmos.DrawSphere(mSockets[i].Transform.position, 5.0f);
                }
                else if(mSockets[i].Direction == EDirection.Up)
                {
                    Gizmos.color = new Color(0.0f, 0.0f, 1.0f, 0.6f);
                    Gizmos.DrawSphere(mSockets[i].Transform.position, 5.0f);
                }
                else if(mSockets[i].Direction == EDirection.Down)
                {
                    Gizmos.color = new Color(1.0f, 0.0f, 1.0f, 0.6f);
                    Gizmos.DrawSphere(mSockets[i].Transform.position, 5.0f);
                }
            }
        }

        private void Awake()
        {
            for (int i = 0; i < mSockets.Length; i++)
            {
                mSockets[i].Transform.gameObject.SetActive(false);
            }

            mAnchor_LeftBottom.gameObject.SetActive(false);
            mAnchor_RightTop.gameObject.SetActive(false);
            enabled = false;
        }
    }
}