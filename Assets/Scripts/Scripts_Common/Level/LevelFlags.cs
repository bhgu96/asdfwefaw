﻿
namespace Level
{
    public enum ELayerType
    {
        None = -1,

        Beginning = 0,
        Ending = 1,

        Normal_00 = 2, Normal_01, Normal_02, Normal_03, Normal_04, Normal_05, 


        Count
    }

    public enum ESocketType
    {
        None = -1,

        Normal_00, Normal_01, Normal_02, Normal_03, Normal_04, Normal_05, Normal_06, Normal_07, Normal_08, Normal_09, 

        Count
    }

    public enum EDirection
    {
        None = -1,

        Left, Right, Down, Up,

        Count
    }
}