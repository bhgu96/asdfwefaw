﻿using UnityEngine;
using System.Collections.Generic;

/* 요구사항
 * 1. 소켓의 인덱스를 Direction, Number 별로 분류해야 함(같은 종류의 인덱스가 존재할 수 있음)
 * 2. Generator에 의해 사용가능하게 되는 소켓의 인덱스값을 따로 분류할 수 있어야 함(
 * 3. 
 * 
 * 
 * 
 */


namespace DynamicLevel
{
    /// <summary>
    /// 프레임 소켓 방향
    /// </summary>
    public enum ESocketDirection
    {
        None = -1,

        Left, Right, Down, Up,

        Count
    }

    /// <summary>
    /// 동적 레벨 프레임 클래스
    /// </summary>
    public class DynamicLevelFrame
        : MonoBehaviour, ISinglePoolable
    {
        public bool CanPool { get { return !gameObject.activeSelf; } }

        [SerializeField]
        private int mFrameType;
        /// <summary>
        /// 프레임 ID
        /// </summary>
        public int FrameType { get { return mFrameType; } }

        [SerializeField]
        private int[] mStateTypes;
        public int CountOfStateTypes { get { return mStateTypes.Length; } }

        [SerializeField]
        private int mMaxCount;
        /// <summary>
        /// 최대 생성 및 최대 풀링 갯수
        /// </summary>
        public int MaxCount { get { return mMaxCount; } }

        [SerializeField]
        private float mWeight;
        /// <summary>
        /// 가중치
        /// </summary>
        public float Weight { get { return mWeight; } }

        // 소켓 정보
        [SerializeField]
        private DynamicLevelSocket[] mSockets;

        /// <summary>
        /// 등록된 소켓의 개수
        /// </summary>
        public int CountOfSockets { get { return mSockets.Length; } }

        [SerializeField]
        private Transform mLeftBottomAnchor;
        /// <summary>
        /// 왼쪽 하단 앵커 위치
        /// </summary>
        public Transform LeftBottomAnchor { get { return mLeftBottomAnchor; } }

        [SerializeField]
        private Transform mRightTopAnchor;
        /// <summary>
        /// 오른쪽 상단 앵커 위치
        /// </summary>
        public Transform RightTopAnchor { get { return mRightTopAnchor; } }

        public event System.Action OnDisconnect;
        public event System.Action OnConnect;

        // 레이어 리스트
        private List<DynamicLevelLayer> mLayerList;

        private List<int> mConnectedSocketIndexList;
        // 사용 가능한 소켓 인덱스 리스트
        private List<int> mAvailableSocketIndexList;
        public int CountOfAvailableSocketIndex { get { return mAvailableSocketIndexList.Count; } }

        private Dictionary<ESocketDirection, Dictionary<int, List<int>>> mSocketIndexDict;
        private List<int> mSelectedSocketIndexList;

        public virtual void Awake()
        {
            mLayerList = new List<DynamicLevelLayer>();
            mSelectedSocketIndexList = new List<int>(mSockets.Length);
            mAvailableSocketIndexList = new List<int>(mSockets.Length);
            mConnectedSocketIndexList = new List<int>(mSockets.Length);
            for (int indexOfSockets = 1; indexOfSockets <= mSockets.Length; indexOfSockets++)
            {
                mAvailableSocketIndexList.Add(indexOfSockets);
            }
            // 소켓 방향, 번호 별로 소켓 인덱스를 분류하는 코드
            mSocketIndexDict = new Dictionary<ESocketDirection, Dictionary<int, List<int>>>();

            for(int indexOfSocket = 0; indexOfSocket < mSockets.Length; indexOfSocket++)
            {
                if(!mSocketIndexDict.ContainsKey(mSockets[indexOfSocket].Direction))
                {
                    mSocketIndexDict.Add(mSockets[indexOfSocket].Direction, new Dictionary<int, List<int>>());
                }

                Dictionary<int, List<int>> subSocketIndexDict = mSocketIndexDict[mSockets[indexOfSocket].Direction];
                if(!subSocketIndexDict.ContainsKey(mSockets[indexOfSocket].Type))
                {
                    subSocketIndexDict.Add(mSockets[indexOfSocket].Type, new List<int>());
                }
                // List.Find 호출 시, int 형은 기본값이 0을 반환하므로 +1
                subSocketIndexDict[mSockets[indexOfSocket].Type].Add(indexOfSocket + 1);
            }
        }

        /// <summary>
        /// 전달된 상태를 가지고 있는가를 반환하는 메소드
        /// </summary>
        public bool HasStateType(int type)
        {
            for (int indexOfStateType = 0; indexOfStateType < CountOfStateTypes; indexOfStateType++)
            {
                if (mStateTypes[indexOfStateType] == type)
                {
                    return true;
                }
            }
            return false;
        }

        /// <summary>
        /// 소켓을 반환하는 메소드
        /// </summary>
        public DynamicLevelSocket GetSocket(int indexOfSockets)
        {
            return mSockets[indexOfSockets];
        }

        /// <summary>
        /// 전달된 direction과 socketType에 일치하는 소켓들에 대해 일련의 select 검사 후, 
        /// 선택된 소켓의 인덱스 값을 리스트에 저장하는 메소드
        /// </summary>
        public bool SelectSocket(ESocketDirection direction, int socketType, System.Predicate<int> select)
        {
            mSelectedSocketIndexList.Clear();
            bool isSelected = false;
            if(mSocketIndexDict.ContainsKey(direction))
            {
                Dictionary<int, List<int>> subSocketIndexDict = mSocketIndexDict[direction];
                if(subSocketIndexDict.ContainsKey(socketType))
                {
                    subSocketIndexDict[socketType].ForEach(delegate (int indexOfSocket)
                    {
                        if (select(indexOfSocket - 1))
                        {
                            isSelected = true;
                            mSelectedSocketIndexList.Add(indexOfSocket);
                        }
                    });
                }
            }
            return isSelected;
        }

        /// <summary>
        /// 선택된 소켓 인덱스 중 무작위로 하나를 골라 반환하는 메소드
        /// </summary>
        /// <returns></returns>
        public int GetRandomIndexOfSelectedSockets()
        {
            if (mSelectedSocketIndexList.Count > 0)
            {
                int random = (int)Random.Range(0.0f, mSelectedSocketIndexList.Count);
                return mSelectedSocketIndexList[random] - 1;
            }
            return -1;
        }

        /// <summary>
        /// 배치 가능한 상태 ID를 반환하는 메소드
        /// </summary>
        public int GetStateType(int indexOfStateType)
        {
            return mStateTypes[indexOfStateType];
        }

        /// <summary>
        /// 소켓에 전달된 프레임을 연결하는 메소드
        /// </summary>
        public void ConnectAt(int indexOfSocket, DynamicLevelFrame connectedFrame, int indexOfConnectedSocket)
        {
            // 블로킹 해제
            mSockets[indexOfSocket].Connect(this, indexOfSocket, connectedFrame, indexOfConnectedSocket);

            // 대상 프레임을 적절한 위치로 이동
            connectedFrame.transform.position = mSockets[indexOfSocket].Transform.position - (connectedFrame.mSockets[indexOfConnectedSocket].Transform.position - connectedFrame.transform.position);

            connectedFrame.gameObject.SetActive(true);
            connectedFrame.OnConnect?.Invoke();
            mAvailableSocketIndexList.Remove(indexOfSocket + 1);
            mConnectedSocketIndexList.Add(indexOfSocket + 1);
            connectedFrame.mAvailableSocketIndexList.Remove(indexOfConnectedSocket + 1);
            connectedFrame.mConnectedSocketIndexList.Add(indexOfConnectedSocket + 1);
        }
        /// <summary>
        /// 소켓에 연결된 프레임을 해제하는 메소드
        /// </summary>
        public void DisconnectAt_Recursive(int indexOfSockets)
        {
            if(mSockets[indexOfSockets].ConnectedFrame == null)
            {
                mSockets[indexOfSockets].Clear();
                return;
            }

            DynamicLevelFrame connectedFrame = mSockets[indexOfSockets].ConnectedFrame;
            // 연결된 프레임에 존재하는 모든 레이어를 제거
            connectedFrame.mLayerList.ForEach(delegate (DynamicLevelLayer layer)
            {
                layer.Remove();
            });
            connectedFrame.mLayerList.Clear();

            // 연결된 프레임에 연결된 모든 프레임의 연결을 제거
            for (int indexOfConnectedSocket = 0; indexOfConnectedSocket < connectedFrame.mSockets.Length; indexOfConnectedSocket++)
            {
                if (!connectedFrame.mSockets[indexOfConnectedSocket].IsBlocked
                    && indexOfConnectedSocket != mSockets[indexOfSockets].IndexOfConnectedSocket)
                {
                    connectedFrame.DisconnectAt_Recursive(indexOfConnectedSocket);
                }
                connectedFrame.mSockets[indexOfConnectedSocket].Clear();
            }

            // 연결된 프레임 초기화
            connectedFrame.OnDisconnect?.Invoke();
            connectedFrame.gameObject.SetActive(false);
            connectedFrame.mAvailableSocketIndexList.Add(mSockets[indexOfSockets].IndexOfConnectedSocket + 1);
            connectedFrame.mConnectedSocketIndexList.Remove(mSockets[indexOfSockets].IndexOfConnectedSocket + 1);
            mSockets[indexOfSockets].Clear();
            mAvailableSocketIndexList.Add(indexOfSockets + 1);
            mConnectedSocketIndexList.Remove(indexOfSockets + 1);
        }

        /// <summary>
        /// 프레임의 소켓 중 연결되어 있는 일련의 소켓에 대한 작업
        /// </summary>
        public void ForEachConnectedSocket(System.Action<int> action)
        {
            mConnectedSocketIndexList.ForEach(delegate (int indexOfSocket)
            {
                action(indexOfSocket - 1);
            });
        }

        /// <summary>
        /// 프레임에 레이어를 추가하는 메소드
        /// </summary>
        public void AddLayer(DynamicLevelLayer layer)
        {
            mLayerList.Add(layer);
            layer.PlaceOn(this);
        }

        /// <summary>
        /// 소켓을 블로킹하는 메소드
        /// </summary>
        public void BlockAt(int indexOfSockets)
        {
            mSockets[indexOfSockets].Block();
            mAvailableSocketIndexList.Remove(indexOfSockets + 1);
            // 프레임에 배치된 레이어의 블로커도 설정
            mLayerList.ForEach(delegate (DynamicLevelLayer layer)
            {
                layer.SetBlocker(indexOfSockets, true);
            });
        }

        /// <summary>
        /// 사용 가능한 소켓들 중 랜덤한 소켓 인덱스를 반환
        /// </summary>
        public int GetRandomIndexOfAvailableSockets()
        {
            if (mAvailableSocketIndexList.Count > 0)
            {
                int random = (int)Random.Range(0.0f, mAvailableSocketIndexList.Count);
                int index = mAvailableSocketIndexList[random];
                return index - 1;
            }
            return -1;
        }

        private void OnDrawGizmos()
        {
            if (mLeftBottomAnchor != null && mRightTopAnchor != null)
            {
                Gizmos.color = new Color(1.0f, 0.7f, 0.0f, 0.1f);
                Vector3 leftBottom = mLeftBottomAnchor.position;
                Vector3 rightTop = mRightTopAnchor.position;
                Gizmos.DrawCube(new Vector3((leftBottom.x + rightTop.x) * 0.5f, (leftBottom.y + rightTop.y) * 0.5f), new Vector3(rightTop.x - leftBottom.x, rightTop.y - leftBottom.y));
            }

            if (mSockets != null)
            {
                for (int i = 0; i < mSockets.Length; i++)
                {
                    if (mSockets[i].Transform != null)
                    {
                        if (mSockets[i].Direction == ESocketDirection.Right)
                        {
                            Gizmos.color = new Color(0.0f, 1.0f, 0.0f, 0.6f);
                            Gizmos.DrawSphere(mSockets[i].Transform.position, 2.0f);
                        }
                        else if (mSockets[i].Direction == ESocketDirection.Left)
                        {
                            Gizmos.color = new Color(1.0f, 0.0f, 0.0f, 0.6f);
                            Gizmos.DrawSphere(mSockets[i].Transform.position, 2.0f);
                        }
                        else if (mSockets[i].Direction == ESocketDirection.Up)
                        {
                            Gizmos.color = new Color(0.0f, 0.0f, 1.0f, 0.6f);
                            Gizmos.DrawSphere(mSockets[i].Transform.position, 2.0f);
                        }
                        else if (mSockets[i].Direction == ESocketDirection.Down)
                        {
                            Gizmos.color = new Color(1.0f, 0.0f, 1.0f, 0.6f);
                            Gizmos.DrawSphere(mSockets[i].Transform.position, 2.0f);
                        }
                    }
                }
            }
        }
    }

    /// <summary>
    /// 동적 레벨 소켓 클래스
    /// </summary>
    [System.Serializable]
    public class DynamicLevelSocket
    {
        [SerializeField]
        private int  mType;
        /// <summary>
        /// 소켓 타입
        /// </summary>
        public int Type { get { return mType; } }
        [SerializeField]
        private ESocketDirection mDirection;
        /// <summary>
        /// 소켓 방향
        /// </summary>
        public ESocketDirection Direction { get { return mDirection; } }
        [SerializeField]
        private Transform mTransform;
        /// <summary>
        /// 소켓 위치 정보
        /// </summary>
        public Transform Transform { get { return mTransform; } }

        [SerializeField]
        // 소켓 연결이 되지 않으면 대신 생성되는 블로킹 오브젝트
        private GameObject[] mBlockers;

        /// <summary>
        ///블로킹된 상태 여부
        /// </summary>
        public bool IsBlocked { get; private set; }

        // mConnectedFrame == null이고, mIsBlocked == false 라면, 아무것도 연결되어 있지 않은 상태
        public DynamicLevelFrame ConnectedFrame { get; private set; }
        public int IndexOfConnectedSocket { get; private set; }

        /// <summary>
        /// 소켓 초기화
        /// </summary>
        public void Clear()
        {
            ConnectedFrame = null;
            if (IsBlocked)
            {
                ToggleBlocker();
                IsBlocked = false;
            }
        }

        /// <summary>
        /// 블로킹 설정 메소드
        /// </summary>
        /// <param name="isEnabled"></param>
        public void Block()
        {
            // 현재 블로킹 상태와 다르다면
            if (!IsBlocked)
            {
                // 블로킹 상태 변경
                IsBlocked = true;
                ConnectedFrame = null;
                ToggleBlocker();
            }
        }

        public void Connect(DynamicLevelFrame frame, int indexOfSocket, DynamicLevelFrame connectedFrame, int indexOfConnectedSocket)
        {
            if (IsBlocked)
            {
                IsBlocked = false;
                ToggleBlocker();
            }

            ConnectedFrame = connectedFrame;
            IndexOfConnectedSocket = indexOfConnectedSocket;
            DynamicLevelSocket connectedSocket = ConnectedFrame.GetSocket(IndexOfConnectedSocket);
            connectedSocket.ConnectedFrame = frame;
            connectedSocket.IndexOfConnectedSocket = indexOfSocket;
        }

        private void ToggleBlocker()
        {
            // 모든 블로킹 오브젝트에 대해 활성화 된 오브젝트는 비활성화, 비활성화 된 오브젝트는 활성화한다.
            for (int indexOfBlockers = 0; indexOfBlockers < mBlockers.Length; indexOfBlockers++)
            {
                if (mBlockers[indexOfBlockers].activeSelf)
                {
                    mBlockers[indexOfBlockers].SetActive(false);
                }
                else
                {
                    mBlockers[indexOfBlockers].SetActive(true);
                }
            }
        }
    }
}