﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DownwardLevelTest : MonoBehaviour
{
    private static int mCountOfConnecting;

    public void Awake()
    {
        GetComponent<DynamicLevel.DynamicLevelFrame>().OnConnect += OnConnect;
    }

    private void OnConnect()
    {
        mCountOfConnecting++;
        if(mCountOfConnecting == 2)
        {
            mCountOfConnecting = 0;
            DynamicLevel.DynamicLevelGenerator.Main.State = 0;
        }
    }
}
