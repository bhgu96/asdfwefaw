﻿using System;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

namespace Level
{
    public class LevelGenerator : MonoBehaviour
    {
        public Vector2 LeftBottomBoundary { get { return mLeftBottomBoundary; } }
        [SerializeField]
        [Tooltip("레벨 생성 좌측 하단 범위")]
        private Vector2 mLeftBottomBoundary;
        public Vector2 RightTopBoundary { get { return mRightTopBoundary; } }
        [SerializeField]
        [Tooltip("레벨 생성 우측 상단 범위")]
        private Vector2 mRightTopBoundary;
        [SerializeField]
        [Tooltip("모든 레벨의 부모 오브젝트")]
        private Transform mLevelHolder;

        [SerializeField]
        [Tooltip("레벨 생성 터레인")]
        private LevelFrame[] mFrames;

        [SerializeField]
        [Tooltip("레이어 정보")]
        private LevelLayerInfo[] mLayerInfo;

        /// <summary>
        /// 맵생성 메소드
        /// </summary>
        private void Generate()
        {
            // Level Frame을 생성하는 인스턴스
            FrameGenerator frameGenerator = new FrameGenerator(mLevelHolder, mLeftBottomBoundary, mRightTopBoundary);
            // Frame 위에 Layer를 생성하는 인스턴스
            LayerGenerator layerGenerator = new LayerGenerator();

            // DFS 방식으로 맵생성을 위해 사용되는 스택
            Stack<LevelFrame> frameStack = new Stack<LevelFrame>();
            // 생성될 수 있는 프레임 추가
            frameGenerator.Add(mFrames);
            // 생성될 수 있는 레이어 추가
            for (int i = 0; i < mLayerInfo.Length; i++)
            {
                layerGenerator.Add(mLayerInfo[i].Layers);
            }
            // Start Frame을 생성
            LevelFrame frame = frameGenerator.Generate(0, ELayerType.Beginning, 0, Vector3.zero, mLevelHolder) as LevelFrame;

            // 생성된 Frame이 있다면
            if (frame != null)
            {
                frame.Initialize();
                // Layer를 순차적으로 생성
                for (int i = 0; i < mLayerInfo.Length; i++)
                {
                    LevelLayer layer = layerGenerator.Generate(i, frame.Type, 0, frame.transform.position, frame.transform);
                }
                // DFS Stack에 Push
                frameStack.Push(frame);
            }

            LevelSocket socket = null;

            // DFS (종료 조건 : 스택이 빌때까지)
            while (frameStack.Count > 0)
            {
                frame = frameStack.Peek();
                socket = frame.DequeueSocket();
                // 다음 Frame이 연결될 수 있는 소켓이 존재한다면
                if (socket != null)
                {
                    // 다음 Frame을 생성
                    LevelFrame nextFrame = frameGenerator.Generate(socket, frameStack.Count);
                    if (nextFrame != null)
                    {
                        for(int i=0; i<mLayerInfo.Length; i++)
                        {
                            layerGenerator.Generate(i, frame.Type, frameStack.Count, frame.transform.position, frame.transform);
                        }
                        frameStack.Push(nextFrame);
                    }
                    else
                    {
                        socket.Block();
                    }
                }
                else
                {
                    frameStack.Pop();
                }
            }
        }

        private List<EDirection> GetDirectionList()
        {
            List<EDirection> directionList = new List<EDirection>();
            for (EDirection direction = EDirection.None + 1; direction < EDirection.Count; direction++)
            {
                directionList.Add(direction);
            }
            return directionList;
        }

        private void Awake()
        {
            Generate();
            gameObject.SetActive(false);
        }

        private void OnDrawGizmosSelected()
        {
            Gizmos.color = new Color(0.4f, 1.0f, 0.4f, 0.5f);
            Gizmos.DrawCube(new Vector3((mLeftBottomBoundary.x + mRightTopBoundary.x) * 0.5f, (mLeftBottomBoundary.y + mRightTopBoundary.y) * 0.5f), new Vector3(mRightTopBoundary.x - mLeftBottomBoundary.x, mRightTopBoundary.y - mLeftBottomBoundary.y));
        }

        /// <summary>
        /// Level의 Frame을 생성하는 클래스
        /// </summary>
        private class FrameGenerator : LayerGenerator
        {
            private Dictionary<EDirection, Dictionary<ESocketType, List<LevelFrame>>> mFrameDict;
            private List<LevelSocket> mPossibleSocketList;
            private Transform mLevelHolder;
            private List<LevelFrame> mActiveList;
            private Vector3 mLeftBottomBoundary;
            private Vector3 mRightTopBoundary;
            private bool mIsEndGenerated;

            public FrameGenerator(Transform levelHolder, Vector3 leftBottomBoundary, Vector3 rightTopBoundary)
            {
                mIsEndGenerated = false;
                mLevelHolder = levelHolder;
                mPossibleSocketList = new List<LevelSocket>();
                mActiveList = new List<LevelFrame>();
                mFrameDict = new Dictionary<EDirection, Dictionary<ESocketType, List<LevelFrame>>>();
                mLeftBottomBoundary = leftBottomBoundary;
                mRightTopBoundary = rightTopBoundary;

                for (EDirection direction = EDirection.None + 1; direction < EDirection.Count; direction++)
                {
                    Dictionary<ESocketType, List<LevelFrame>> frameTypeDict = new Dictionary<ESocketType, List<LevelFrame>>();
                    mFrameDict.Add(direction, frameTypeDict);
                    for (ESocketType type = ESocketType.None + 1; type < ESocketType.Count; type++)
                    {
                        frameTypeDict.Add(type, new List<LevelFrame>());
                    }
                }
            }
            /// <summary>
            /// 생성될 수 있는 프레임을 추가하는 메소드
            /// </summary>
            /// <param name="frames"></param>
            public void Add(LevelFrame[] frames)
            {
                base.Add(frames);
                for (int i = 0; i < frames.Length; i++)
                {
                    frames[i].Initialize();
                    for (EDirection direction = EDirection.None + 1; direction < EDirection.Count; direction++)
                    {
                        for (ESocketType type = ESocketType.None + 1; type < ESocketType.Count; type++)
                        {
                            int countOfSocket;
                            if (frames[i].HasSocket(direction, type, out countOfSocket))
                            {
                                for (int j = 0; j < countOfSocket; j++)
                                {
                                    mFrameDict[direction][type].Add(frames[i]);
                                }
                            }
                        }
                    }
                }
            }

            /// <summary>
            /// 생성될 수 있는 프레임을 선택해서 생성하는 메소드
            /// </summary>
            /// <param name="direction"></param>
            /// <param name="type"></param>
            /// <param name="depth"></param>
            /// <param name="socketPosition"></param>
            /// <returns></returns>
            public LevelFrame Generate(LevelSocket socket, int depth)
            {
                // 전달받은 소켓의 반대 방향을 선택
                EDirection direction = EDirection.None;
                if (socket.Direction == EDirection.Right)
                {
                    direction = EDirection.Left;
                }
                else if (socket.Direction == EDirection.Down)
                {
                    direction = EDirection.Up;
                }
                else if (socket.Direction == EDirection.Up)
                {
                    direction = EDirection.Down;
                }
                else if (socket.Direction == EDirection.Left)
                {
                    direction = EDirection.Right;
                }
                // 전달받은 소켓의 반대방향에 위치하고 같은 타입의 소켓을 가진 Frame들을 선택한다.
                List<LevelFrame> frameList = mFrameDict[direction][socket.Type];
                // 전달받은 소켓의 위치를 받아온다.
                Vector3 socketPosition = socket.Transform.position;
                // 배치 가능한 Frame을 임시로 저장하는 리스트를 초기화한다.
                mPossibleSocketList.Clear();
                // 배치 가능할 수 있는 모든 Frame에 대해
                for (int i = 0; i < frameList.Count; i++)
                {
                    LevelFrame frame = frameList[i];

                    // 선택된 프레임이 배치될 수 있다면
                    if (frame.CountOfLevel > 0)
                    {
                        // 선택된 프레임이 배치될 수 있는 깊이라면
                        if (depth >= frame.DepthOfLevel)
                        {
                            // 선택된 프레임에서 소켓 하나를 받는다.
                            LevelSocket frameSocket = frame.MoveSocket(direction, socket.Type);

                            // 해당 소켓을 기준으로 좌측 하단, 우측 상단의 WorldPosition을 구한다.
                            Vector3 frame_rightTop = socketPosition - frameSocket.Transform.localPosition + frame.Anchor_RightTop.localPosition;
                            Vector3 frame_leftBottom = socketPosition - frameSocket.Transform.localPosition + frame.Anchor_LeftBottom.localPosition;
                            // End 프레임이 생성되지 않았거나, 생성 가능 경계를 넘어가지 않는다면, 
                            if (!mIsEndGenerated || (mLeftBottomBoundary.x <= frame_leftBottom.x && mRightTopBoundary.x >= frame_rightTop.x
                                && mLeftBottomBoundary.y <= frame_leftBottom.y && mRightTopBoundary.y >= frame_rightTop.y))
                            {
                                bool isOverlapped = false;
                                // 모든 활성화된 프레임에 대해서
                                for (int j = 0; j < mActiveList.Count; j++)
                                {
                                    LevelFrame activeFrame = mActiveList[j];
                                    // 서로 겹치는 부분이 있는가에 관한 여부를 계산한다.
                                    // (Ax - Bx) * (Ay - By) < 0 이라면 서로 겹침 (Ax : A의 왼쪽, Ay : A의 오른쪽 좌표)
                                    Vector3 active_rightTop = activeFrame.Anchor_RightTop.position;
                                    Vector3 active_leftBottom = activeFrame.Anchor_LeftBottom.position;
                                    float overlap_x = (frame_rightTop.x - active_leftBottom.x) * (frame_leftBottom.x - active_rightTop.x);
                                    float overlap_y = (frame_rightTop.y - active_leftBottom.y) * (frame_leftBottom.y - active_rightTop.y);

                                    if (overlap_x < 0.0f && overlap_y < 0.0f)
                                    {
                                        isOverlapped = true;
                                        break;
                                    }
                                }
                                // 겹치지 않는다면
                                if (!isOverlapped)
                                {
                                    // 생성 가능한 프레임 임시 리스트에 저장
                                    mPossibleSocketList.Add(frameSocket);
                                }
                            }
                        }
                    } // 선택된 프레임이 배치될 수 있는 횟수가 없다면
                    else
                    {
                        // 기존 프레임 리스트에서 삭제
                        frameList.RemoveAt(i--);
                    }
                }

                float weightSum = 0.0f;
                // 모든 생성 가능한 프레임들의 가중치 합을 구함
                for (int i = 0; i < mPossibleSocketList.Count; i++)
                {
                    LevelFrame frame = mPossibleSocketList[i].Frame;
                    weightSum += frame.WeightOfLevel;
                }
                // 랜덤하게 한 숫자를 고름
                float weight = Random.Range(0.0f, weightSum);
                // 모든 생성 가능한 프레임에 대해
                for (int i = 0; i < mPossibleSocketList.Count; i++)
                {
                    socket = mPossibleSocketList[i];
                    LevelFrame frame = socket.Frame;
                    // 선택된 숫자가 해당 프레임의 가중치 범위 내에 있는 경우
                    if (weight < frame.WeightOfLevel)
                    {
                        // 해당 프레임을 생성
                        frame.CountOfLevel--;
                        // 생성되는 프레임이 End 프레임인 경우
                        if (frame.Type == ELayerType.Ending)
                        {
                            // End 프레임 생성 여부 갱신
                            mIsEndGenerated = true;
                        }
                        // 프레임을 활성화 시키고, 활성화된 프레임을 초기화
                        LevelFrame activeFrame = Instantiate(frame.gameObject, socketPosition - socket.Transform.localPosition, Quaternion.identity, mLevelHolder).GetComponent<LevelFrame>();
                        activeFrame.Initialize();
                        // 활성화된 프레임의 사용된 소켓 제거
                        activeFrame.DequeueSocket(socket.Tag);
                        //activeFrame.DequeueSocket(direction, socket.Type);
                        mActiveList.Add(activeFrame);
                        return activeFrame;
                    }
                    // 해당 프레임의 가중치 범위 밖에 있는 경우에는 가중치를 갱신
                    weight -= frame.WeightOfLevel;
                }

                return null;
            }
        }

        /// <summary>
        /// Level의 Layer를 관리하는 클래스
        /// </summary>
        private class LayerGenerator
        {
            private List<Dictionary<ELayerType, List<LevelLayer>>> mLayerDictList;
            private List<LevelLayer> mPossibleLayerList;

            public LayerGenerator()
            {
                mLayerDictList = new List<Dictionary<ELayerType, List<LevelLayer>>>();
                mPossibleLayerList = new List<LevelLayer>();
            }

            /// <summary>
            /// 생성될 수 있는 레이어를 추가하는 메소드
            /// </summary>
            /// <param name="layers"></param>
            public void Add(LevelLayer[] layers)
            {
                Dictionary<ELayerType, List<LevelLayer>> layerDict = new Dictionary<ELayerType, List<LevelLayer>>();
                mLayerDictList.Add(layerDict);
                // Layer를 타입별로 분류
                for (ELayerType type = ELayerType.None + 1; type < ELayerType.Count; type++)
                {
                    layerDict.Add(type, new List<LevelLayer>());
                }

                for (int i = 0; i < layers.Length; i++)
                {
                    layers[i].Initialize();
                    layerDict[layers[i].Type].Add(layers[i]);
                }
            }

            /// <summary>
            /// 생성될 수 있는 레이어를 선택해서 생성하는 메소드
            /// </summary>
            /// <param name="index"></param>
            /// <param name="type"></param>
            /// <param name="depth"></param>
            /// <param name="generatingPosition"></param>
            /// <param name="layerHolder"></param>
            /// <returns></returns>
            public LevelLayer Generate(int index, ELayerType type, int depth, Vector3 generatingPosition, Transform layerHolder)
            {
                List<LevelLayer> layerList = mLayerDictList[index][type];

                float weightSum = 0.0f;
                // 생성 가능한 레이어를 분류
                mPossibleLayerList.Clear();
                for (int i = 0; i < layerList.Count; i++)
                {
                    LevelLayer layer = layerList[i];
                    if (layer.CountOfLevel > 0)
                    {
                        if (depth >= layer.DepthOfLevel)
                        {
                            mPossibleLayerList.Add(layer);
                            weightSum += layer.WeightOfLevel;
                        }
                    }
                    else
                    {
                        layerList.RemoveAt(i--);
                    }
                }

                float weight = Random.Range(0.0f, weightSum);

                // 선택된 레이어를 배치
                for (int i = 0; i < mPossibleLayerList.Count; i++)
                {
                    LevelLayer layer = mPossibleLayerList[i];
                    if (weight < layer.WeightOfLevel)
                    {
                        layer.CountOfLevel--;
                        LevelLayer activeLayer = Instantiate(layer.gameObject, generatingPosition, Quaternion.identity, layerHolder).GetComponent<LevelFrame>();
                        return activeLayer;
                    }
                    weight -= layer.WeightOfLevel;
                }

                return null;
            }
        }
    }

    [Serializable]
    public class LevelLayerInfo
    {
        public LevelLayer[] Layers;
    }

    [Serializable]
    public class LevelFrameInfo
    {
        public LevelFrame[] Frames;
    }
}