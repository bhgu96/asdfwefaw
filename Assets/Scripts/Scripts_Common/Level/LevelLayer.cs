﻿using UnityEngine;

namespace Level
{
    public class LevelLayer : MonoBehaviour
    {
        public ELayerType Type { get { return mType; } }
        /// <summary>
        /// 배치될 수 있는 레벨의 수
        /// </summary>
        public int CountOfLevel { get { return mRemainOfLevel; } set { mRemainOfLevel = value; } }
        /// <summary>
        /// 배치될 레벨 선택 시의 가중치
        /// </summary>
        public float WeightOfLevel { get { return mWeightOfLevel; } }
        /// <summary>
        /// 배치될 레벨의 깊이
        /// </summary>
        public int DepthOfLevel { get { return mDepthOfLevel; } }

        [SerializeField]
        private ELayerType mType;
        [SerializeField]
        [Tooltip("배치될 수 있는 레벨의 수")]
        private int mCountOfLevel;
        private int mRemainOfLevel;
        [SerializeField]
        [Tooltip("배치될 레벨 선택시의 가중치")]
        private float mWeightOfLevel;
        [SerializeField]
        [Tooltip("배치될 레벨의 깊이")]
        private int mDepthOfLevel;


        public virtual void Initialize()
        {
            mRemainOfLevel = mCountOfLevel;
        }
    }
}