﻿using UnityEngine;
using System.Collections;

/// <summary>
/// DetectLayer가 감지되면 다음 방을 생성하는 클래스
/// 생성될 수 있는 방의 종류가 한개이고, 연결될 수 있는 소켓이 하나뿐인 단점이 있다
/// </summary>
public class EndlessArea : MonoBehaviour
{
    [SerializeField]
    private EndlessArea mNextArea;

    [SerializeField]
    private Transform mSocket;

    [SerializeField]
    private Transform mNextSocket;

    [SerializeField]
    private ELayerFlags mDetectLayer;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.gameObject.layer == (int)mDetectLayer)
        {
            mNextArea.gameObject.SetActive(true);
            mNextArea.transform.position = mNextSocket.position - mNextArea.mSocket.localPosition;
        }
    }
}
