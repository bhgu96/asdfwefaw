﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EndingArea : MonoBehaviour
{
    private void Awake()
    {
        DevelopmentKey.EndingArea = transform;
    }

    private void OnDestroy()
    {
        DevelopmentKey.EndingArea = null;
    }
}
