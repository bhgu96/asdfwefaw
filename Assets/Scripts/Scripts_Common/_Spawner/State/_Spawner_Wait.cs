﻿using UnityEngine;

public class _Spawner_Wait : State<_EStateType_Spawner>
{
    private _Spawner mSpawner;
    private float mRemainTime;


    public _Spawner_Wait(_Spawner spawner) :  base(_EStateType_Spawner.Wait)
    {
        mSpawner = spawner;
        mRemainTime = mSpawner.WaitTime;
    }

    public override void Start()
    {
        mRemainTime = mSpawner.WaitTime;
    }

    public override void Update()
    {
        if (DynamicLevel.DynamicLevelGenerator.Main.State == 0)
        {
            if (mRemainTime > 0.0f)
            {
                mRemainTime -= Time.deltaTime;
            }
            else
            {
                mSpawner.Spawn();
                mSpawner.State = _EStateType_Spawner.Check;
                return;
            }
        }
    }
}
