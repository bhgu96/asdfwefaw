﻿using UnityEngine;

public class _Spawner_Check : State<_EStateType_Spawner>
{
    private _Spawner mSpawner;
    private bool mIsTriggered = false;
    private float mWaitTime;

    public _Spawner_Check(_Spawner spawner) : base(_EStateType_Spawner.Check)
    {
        mSpawner = spawner;
    }

    public override void Start()
    {
        mIsTriggered = false;
    }

    public override void Update()
    {
        if(mSpawner.CountOfMonsters <= 0)
        {
            if (!mIsTriggered && mSpawner.Level % 2 == 0)
            {
                mIsTriggered = true;
                DynamicLevel.DynamicLevelGenerator.Main.State = 1;
                mWaitTime = 6.0f;
            }
            else if (DynamicLevel.DynamicLevelGenerator.Main.State == 0)
            {
                if(mWaitTime > 0.0f)
                {
                    mWaitTime -= Time.deltaTime;
                }
                else
                {
                    mSpawner.State = _EStateType_Spawner.Wait;
                }
            }
        }
    }
}
