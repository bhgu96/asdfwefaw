﻿using UnityEngine;
using System.Collections.Generic;

public enum _EPhaseType
{
    None = -1,

    Phase_00,

    Count
}

public enum _EStateType_Spawner
{
    None = -1,

    Wait,
    Check, 

    Count
}

public class _Spawner : StateBase<_EStateType_Spawner>
{
    public static _Spawner Main { get; private set; } = null;

    [SerializeField]
    private float mWaitTime;
    public float WaitTime { get { return mWaitTime; } set { mWaitTime = value; } }

    private int mCountOfMonsters;
    public int CountOfMonsters
    {
        get { return mCountOfMonsters; }
        set
        {
            if(value < 0)
            {
                value = 0;
            }
            mCountOfMonsters = value;
        }
    }

    public int Level { get; private set; } = 0;

    [SerializeField]
    private Transform[] mSpawnHolders;
    public Transform[] SpawnHolders { get { return mSpawnHolders; } }

    [SerializeField]
    private _SpawnStateInfo[] mSpawnInfo;
    private Dictionary<_EPhaseType, _SpawnPattern[]> mSpawnPatternDict;

    public StateHandler<_EPhaseType> PhaseState { get; private set; }

    [SerializeField]
    private Transform mLeftBottom;
    [SerializeField]
    private Transform mRightTop;

    private Vector2[] mSrcPositions;
    private Vector2[] mDstPositions;
    private float[] mTimes;
    private float[] mRemainTimes;
    

    public void Spawn()
    {
        Level++;
        _SpawnPattern[] pattern = mSpawnPatternDict[PhaseState.State];

        for(int i=0; i< SpawnHolders.Length && i < pattern.Length; i++)
        {
            _SpawnInfo[] info = pattern[i].Info;

            float weightSum = 0.0f;
            for(int j=0; j<info.Length; j++)
            {
                weightSum += info[j].Weight;
            }

            float selection = Random.Range(0.0f, weightSum);

            for(int j=0; j<info.Length; j++)
            {
                if(selection <= info[j].Weight)
                {
                    if (info[j].Type != _EMonsterType.None)
                    {
                        _IMonster monster = _Pool_Monster.Get(info[j].Type);
                        monster.transform.position = SpawnHolders[i].position;
                        monster.HorizontalDirection = EHorizontalDirection.Left;
                        monster.ActiveHolder = SpawnHolders[i];
                        monster.Activate();
                    }
                    break;

                }
                selection -= info[j].Weight;
            }
        }
    }

    protected override void Update()
    {
        base.Update();

        if(!StateManager.Pause.State)
        {
            for(int i=0; i<mSpawnHolders.Length; i++)
            {
                if(mRemainTimes[i] > Mathf.Epsilon)
                {
                    mRemainTimes[i] -= Time.deltaTime;
                    mSpawnHolders[i].localPosition = Vector2.Lerp(mDstPositions[i], mSrcPositions[i], mRemainTimes[i] / mTimes[i]);
                }
                else
                {
                    mSrcPositions[i] = mSpawnHolders[i].localPosition;
                    mDstPositions[i] = new Vector2(Random.Range(mLeftBottom.localPosition.x, mRightTop.localPosition.x), Random.Range(mLeftBottom.localPosition.y, mRightTop.localPosition.y));
                    mTimes[i] = Random.Range(1.0f, 3.0f);
                    mRemainTimes[i] = mTimes[i];
                }
            }
        }
    }

    protected override void Awake()
    {
        base.Awake();

        PhaseState = new StateHandler<_EPhaseType>();
        mSpawnPatternDict = new Dictionary<_EPhaseType, _SpawnPattern[]>();

        for(int i=0; i<mSpawnInfo.Length; i++)
        {
            mSpawnPatternDict.Add(mSpawnInfo[i].State, mSpawnInfo[i].Pattern);
        }

        mSrcPositions = new Vector2[mSpawnHolders.Length];
        mDstPositions = new Vector2[mSpawnHolders.Length];
        mTimes = new float[mSpawnHolders.Length];
        mRemainTimes = new float[mSpawnHolders.Length];

        SetStates(new _Spawner_Wait(this)
            , new _Spawner_Check(this));
    }

    private void OnEnable()
    {
        Main = this;
    }

    private void OnDisable()
    {
        Main = null;
    }
}


[System.Serializable]
public class _SpawnStateInfo
{
    public _EPhaseType State;
    public _SpawnPattern[] Pattern;
}

[System.Serializable]
public class _SpawnPattern
{
    public _SpawnInfo[] Info;
}

[System.Serializable]
public class _SpawnInfo
{
    public _EMonsterType Type;
    public float Weight;
}
