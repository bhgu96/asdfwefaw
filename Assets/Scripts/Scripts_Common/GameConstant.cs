﻿
using UnityEngine;

public static class GameConstant
{
    /// <summary>
    /// 데미지로 인한 렌더러 컬러 갱신 시간
    /// </summary>
    public const float DAMAGE_EFFECT_TIME = 0.1F;
    /// <summary>
    /// 데미지로 인한 무적 시간
    /// </summary>
    public const float INVINCIBLE_TIME_BY_HIT = 0.1f;
    /// <summary>
    /// 넉백 처리가 끝나기 위한 속도의 한계
    /// </summary>
    public const float THRESHOLD_HIT = 0.1F;

    public const float DASH_COOL_TIME = 0.2F;

    /// <summary>
    /// 공격력의 정화력 보정값
    /// </summary>
    public const float OFFENSE_PURIFICATION_CALIBRATION = 0.004f;
    /// <summary>
    /// 공격력의 항마력 보정값
    /// </summary>
    public const float OFFENSE_EXORCISM_CALIBRATION = 0.004F;
    /// <summary>
    /// 방어력의 항마력 보정값
    /// </summary>
    public const float DEFENSE_EXORCISM_CALIBRATION = 0.004f;
    /// <summary>
    /// 속성 친화도 데미지 보정값
    /// </summary>
    public const float DAMAGE_ELEMENTAL_AFFINITY_CALIBRATION = 222.0F;
    /// <summary>
    /// 속성 친화도 데미지 추가값
    /// </summary>
    public const float DAMAGE_ELEMENTAL_AFFINITY_ADDITION = 11.0F;
    /// <summary>
    /// 데미지의 레벨 보정 값
    /// </summary>
    public const float DAMAGE_LEVEL_CALIBRATION = 200.0F;

    public const float ONE_SECOND = 1.0F;

    public const float MOVEMENT_THRESHOLD = 0.01F;

    /// <summary>
    /// 거인의 앞 거리
    /// </summary>
    public const float GIANT_FRONT = 3.0F;
    public const float GIANT_FRONT_ERROR = 2.0F;
    public const float GIANT_UP = 4.0f;
    public const float GIANT_UP_ERROR = 2.0F;

    /// <summary>
    /// 애니메이션 한 프레임
    /// </summary>
    public const float ONE_FRAME = 0.1F;

    /// <summary>
    /// 최대 스킬 가변 인자 개수
    /// </summary>
    public const int MAX_SKILL_PARAMETERS = 10;


    /// <summary>
    /// 마찰력이 0인 Material
    /// </summary>
    public static PhysicsMaterial2D ZeroMaterial { get; private set; }

    public delegate void Action();
    [System.Serializable]
    public class MinMax { public float Min; public float Max; }

    static GameConstant()
    {
        ZeroMaterial = Resources.Load<PhysicsMaterial2D>("Commons/ZeroMaterial2D");
    }
}