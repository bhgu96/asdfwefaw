﻿using UnityEngine;
using System;

public interface IActor : IControllable
{
    /// <summary>
    /// 액터 아이콘
    /// </summary>
    Sprite Icon { get; }
    /// <summary>
    /// 액터 이름
    /// </summary>
    string Name { get; }
    /// <summary>
    /// 플레이버 텍스트
    /// </summary>
    string FlavorText { get; }
    /// <summary>
    /// 액터 능력치
    /// </summary>
    AStatus Status { get; }
    /// <summary>
    /// 액터의 크기
    /// </summary>
    ESizeType Size { get; }
    /// <summary>
    /// 애니메이터
    /// </summary>
    Animator Animator { get; }
    /// <summary>
    /// 강체 정보
    /// </summary>
    Rigidbody2D Rigidbody { get; }
    /// <summary>
    /// 메인 객체
    /// </summary>
    Transform MainTransform { get; }
    /// <summary>
    /// 메인 렌더러
    /// </summary>
    SpriteRenderer MainRenderer { get; }
    /// <summary>
    /// 메인 콜라이더
    /// </summary>
    Collider2D MainCollider { get; }
    /// <summary>
    /// 터레인 트리거
    /// </summary>
    TriggerHandler_Terrain TerrainTrigger { get; }
    /// <summary>
    /// 버프 핸들러
    /// </summary>
    BuffHandler BuffHandler { get; }
    /// <summary>
    /// 디버프 핸들러
    /// </summary>
    DebuffHandler DebuffHandler { get; }
    /// <summary>
    /// 수평 방향
    /// </summary>
    EHorizontalDirection HorizontalDirection { get; set; }
    /// <summary>
    /// 속도
    /// </summary>
    Vector2 Velocity { get; set; }
    /// <summary>
    /// 표면 정보
    /// </summary>
    PhysicsMaterial2D PhysicsMaterial { get; set; }
    /// <summary>
    /// 땅 부딪힘 정보
    /// </summary>
    bool IsGround { get; }
    /// <summary>
    /// 중력 계수
    /// </summary>
    float GravityScale { get; set; }

    /// <summary>
    /// 객체
    /// </summary>
    Transform transform { get; }

    /// <summary>
    /// 현재 시전중인 스킬
    /// </summary>
    ISkill CastingSkill { get; }
    /// <summary>
    /// 스킬 시전 가능 여부
    /// </summary>
    bool CanCast { get; }

    /// <summary>
    /// 스킬 시전 가능 설정 메소드
    /// </summary>
    void SetEnableCast();
    /// <summary>
    /// 스킬 시전 불가능 설정 메소드
    /// </summary>
    /// <param name="skill"></param>
    void SetDisableCast(ISkill skill);

    /// <summary>
    /// 수평 이동
    /// </summary>
    /// <param name="speed"></param>
    /// <param name="direction"></param>
    void MoveHorizontal(float speed, EHorizontalDirection direction);

    /// <summary>
    /// 수직 이동
    /// </summary>
    /// <param name="speed"></param>
    /// <param name="direction"></param>
    void MoveVertical(float speed, EVerticalDirection direction);

    /// <summary>
    /// 구속 상태가 되었을때 호출되는 메소드
    /// </summary>
    void OnRestrain();

    T GetComponent<T>();
    T GetComponentInChildren<T>();
    T[] GetComponentsInChildren<T>();
    T GetComponentInParent<T>();
}

[RequireComponent(typeof(Rigidbody2D), typeof(Animator), typeof(TriggerHandler_Terrain))]
[RequireComponent(typeof(BuffHandler), typeof(DebuffHandler))]
public abstract class AActor<EState>
    : AControllable<EState>, IActor
    where EState : Enum
{
    [SerializeField]
    private Sprite mIcon;
    /// <summary>
    /// 아이콘 프로퍼티
    /// </summary>
    public Sprite Icon { get { return mIcon; } }

    /// <summary>
    /// 이름
    /// </summary>
    public virtual string Name { get { return DataManager.GetScript(Status.NameTag); } }

    /// <summary>
    /// 플레이버 텍스트
    /// </summary>
    public virtual string FlavorText { get { return DataManager.GetScript(Status.FlavorTag); } }

    [SerializeField]
    private ESizeType mSize;
    /// <summary>
    /// 액터의 크기
    /// </summary>
    public ESizeType Size { get { return mSize; } }

    /// <summary>
    /// 액터의 능력치
    /// </summary>
    public AStatus Status { get; protected set; }

    /// <summary>
    /// 액터의 최상위 애니메이터
    /// </summary>
    public Animator Animator { get; private set; }

    /// <summary>
    /// 액터의 최상위 강체
    /// </summary>
    public Rigidbody2D Rigidbody { get; private set; }

    /// <summary>
    /// 액터의 몸체를 이루는 Transform
    /// </summary>
    public Transform MainTransform { get { return mMainTransform; } }
    [SerializeField]
    [Tooltip("Transform의 Scale.x 가 0보다 클 때 오른쪽을 바라보고 있어야 한다.")]
    private Transform mMainTransform;

    [SerializeField]
    private SpriteRenderer mMainRenderer;
    /// <summary>
    /// 몸체의 렌더러
    /// </summary>
    public SpriteRenderer MainRenderer { get { return mMainRenderer; } }

    [SerializeField]
    private Collider2D mMainCollider;
    /// <summary>
    /// 몸체의 콜라이더
    /// </summary>
    public Collider2D MainCollider { get { return mMainCollider; } }

    /// <summary>
    /// Actor 주변 환경 감지를 위한 TriggerHandler
    /// </summary>
    public TriggerHandler_Terrain TerrainTrigger { get; private set; }

    /// <summary>
    /// 버프 핸들러
    /// </summary>
    public BuffHandler BuffHandler { get; private set; }

    /// <summary>
    /// 디버프 핸들러
    /// </summary>
    public DebuffHandler DebuffHandler { get; private set; }

    /// <summary>
    /// 현재 시전중인 스킬
    /// </summary>
    public ISkill CastingSkill { get; protected set; }
    /// <summary>
    /// 스킬 시전 가능 여부
    /// </summary>
    public bool CanCast { get; private set; } = true;

    /// <summary>
    /// 현재 Actor가 보고 있는 방향
    /// </summary>
    public EHorizontalDirection HorizontalDirection
    {
        get { return mHorizontalDirection; }
        set
        {
            // 현재 방향과 다르다면
            if (mHorizontalDirection != value)
            {
                // 기존 스케일.x 에 -1을 곱함
                Vector3 prevLocalScale = mMainTransform.localScale;
                mMainTransform.localScale = new Vector3(prevLocalScale.x * -1.0f, prevLocalScale.y, prevLocalScale.z);
                mHorizontalDirection = value;
            }
        }
    }
    private EHorizontalDirection mHorizontalDirection;

    /// <summary>
    /// 액터의 속도 프로퍼티
    /// </summary>
    public Vector2 Velocity
    {
        get { return Rigidbody.velocity; }
        set
        {
            // 다음 Fixed Update 한번 마찰력을 0으로 만듬
            mIsPhysicsUpdated = true;
            Rigidbody.velocity = value;
        }
    }
    private bool mIsPhysicsUpdated = false;
    public bool IsZeroFriction { get; private set; } = false;

    /// <summary>
    /// 액터의 물체 특성 프로퍼티
    /// </summary>
    public PhysicsMaterial2D PhysicsMaterial { get; set; }

    /// <summary>
    /// 액터가 땅에 서있는 상태에 관한 프로퍼티
    /// </summary>
    public bool IsGround
    {
        get { return (TerrainTrigger.GetColliders(ETerrainTrigger.Terrain)[0] != null); }
    }

    /// <summary>
    /// 액터의 중력 계수 프로퍼티
    /// </summary>
    public float GravityScale
    {
        get { return Rigidbody.gravityScale; }
        set { Rigidbody.gravityScale = value; }
    }

    /// <summary>
    /// 스킬 시전 가능 설정 메소드
    /// </summary>
    public void SetEnableCast()
    {
        CanCast = true;
    }
    /// <summary>
    /// 스킬 시전 불가능 설정 메소드
    /// </summary>
    /// <param name="castingSkill"></param>
    public void SetDisableCast(ISkill castingSkill)
    {
        CastingSkill = castingSkill;
        CanCast = false;
    }

    /// <summary>
    /// 액터를 수평이동 시키는 메소드
    /// </summary>
    /// <param name="speed"></param>
    /// <param name="direction"></param>
    public void MoveHorizontal(float speed, EHorizontalDirection direction)
    {
        HorizontalDirection = direction;
        Velocity = new Vector2((float)HorizontalDirection * speed, Velocity.y);
    }

    /// <summary>
    /// 액터를 수직이동 시키는 메소드
    /// </summary>
    /// <param name="speed"></param>
    /// <param name="direction"></param>
    public void MoveVertical(float speed, EVerticalDirection direction)
    {
        Velocity = new Vector2(Velocity.x, (float)direction * speed);
    }

    /// <summary>
    /// 구속 상태가 되었을때 호출되는 콜백 메소드
    /// </summary>
    public abstract void OnRestrain();

    protected override void Awake()
    {
        base.Awake();
        //Actor 방향 설정
        if(mMainTransform.lossyScale.x > 0.0f)
        {
            mHorizontalDirection = EHorizontalDirection.Right;
        }
        else
        {
            mHorizontalDirection = EHorizontalDirection.Left;
        }

        Rigidbody = GetComponent<Rigidbody2D>();
        Animator = GetComponent<Animator>();
        TerrainTrigger = GetComponent<TriggerHandler_Terrain>();

        BuffHandler = GetComponent<BuffHandler>();
        DebuffHandler = GetComponent<DebuffHandler>();

        PhysicsMaterial = Rigidbody.sharedMaterial;
    }

    protected override void FixedUpdate()
    {
        base.FixedUpdate();

        if(mIsPhysicsUpdated)
        {
            mIsPhysicsUpdated = false;
            if(!IsZeroFriction)
            {
                IsZeroFriction = true;
                Rigidbody.sharedMaterial = GameConstant.ZeroMaterial;
            }
        }
        else if(IsZeroFriction)
        {
            IsZeroFriction = false;
            Rigidbody.sharedMaterial = PhysicsMaterial;
        }
    }
}