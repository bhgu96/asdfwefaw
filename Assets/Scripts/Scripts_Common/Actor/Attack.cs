﻿using UnityEngine;
using System;
using Object;

public class Attack<EAttackFlags> : MonoBehaviour, IHandleable<EAttackFlags>
    where EAttackFlags : Enum
{
    [SerializeField]
    private EAttackFlags mType;
    public EAttackFlags Type { get { return mType; } }
}
