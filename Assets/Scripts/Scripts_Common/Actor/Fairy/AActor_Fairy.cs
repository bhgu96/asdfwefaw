﻿using UnityEngine;
using System;

public interface IActor_Fairy : IActor
{
    new AStatus_Fairy Status { get; }
}


public abstract class AActor_Fairy<EState> 
    : AActor<EState>, IActor_Fairy
    where EState : Enum
{
    private AStatus_Fairy mStatus;
    /// <summary>
    /// 정령의 능력치
    /// </summary>
    public new AStatus_Fairy Status
    {
        get { return mStatus; }
        protected set { base.Status = value; mStatus = value; }
    }

    /// <summary>
    /// 정령이 이동중임을 판단하는 최소 속도
    /// </summary>
    public const float MOVEMENT_THRESHOLD = 0.001F;

    /// <summary>
    /// 정령의 가속도
    /// </summary>
    public const float ACCELERATION = 200.0F;

    /// <summary>
    /// 정령의 속도 저항
    /// </summary>
    public const float AIR_FORCE = 50.0F;

    private float mAngle;
    /// <summary>
    /// 정령 몸의 각도
    /// </summary>
    public float Angle
    {
        get { return mAngle; }
        set
        {
            while (value > 180.0f)
            {
                value -= 360.0f;
            }
            while (value < -180.0f)
            {
                value += 360.0f;
            }

            mAngle = value;

            if (!mIsInitDirection)
            {
                Direction = new Vector2(Mathf.Cos(mAngle), Mathf.Sin(mAngle));
            }
            else
            {
                mIsInitDirection = false;
            }

            // 오른쪽을 보고있는 경우
            if (mAngle >= -90.0f && mAngle <= 90.0f)
            {
                HorizontalDirection = EHorizontalDirection.Right;

                MainTransform.eulerAngles = new Vector3(0.0f, 0.0f, mAngle);
            }
            // 왼쪽을 보고있는 경우
            else
            {
                HorizontalDirection = EHorizontalDirection.Left;

                MainTransform.eulerAngles = new Vector3(0.0f, 0.0f, mAngle - 180.0f);
            }
        }
    }

    /// <summary>
    /// 각도에 따른 벡터 방향
    /// </summary>
    public Vector2 Direction { get; private set; }
    private bool mIsInitDirection = false;

    /// <summary>
    /// 움직인 방향(direction)에 따라 Main 객체의 각도를 조절하는 메소드
    /// </summary>
    /// <param name="direction"></param>
    public void SetDirection(Vector2 direction, bool isNormalized)
    {
        if (!isNormalized)
        {
            direction.Normalize();
        }

        Direction = direction;
        mIsInitDirection = true;

        // 윗방향으로 움직인 경우
        if (direction.y >= 0.0f)
        {
            Angle = Mathf.Acos(direction.x) * Mathf.Rad2Deg;
        }
        // 아랫 방향으로 움직인 경우
        else
        {
            Angle = -Mathf.Acos(direction.x) * Mathf.Rad2Deg;
        }
    }

    /// <summary>
    /// 전달된 패러미터를 이용해서 기존 속도에 추가하는 메소드
    /// </summary>
    /// <param name="horizontal"></param>
    /// <param name="vertical"></param>
    public void AddVelocity(float horizontal, float vertical)
    {
        float maxSpeed = Status.MoveSpeed;
        float nextVelocity_x = Velocity.x + horizontal * ACCELERATION * Time.fixedDeltaTime;
        float nextVelocity_y = Velocity.y + vertical * ACCELERATION * Time.fixedDeltaTime;
        float airForce = AIR_FORCE * Time.fixedDeltaTime;

        if (nextVelocity_x > 0.0f)
        {
            nextVelocity_x -= airForce;
            if (nextVelocity_x > maxSpeed)
            {
                nextVelocity_x = maxSpeed;
            }
            else if (nextVelocity_x < 0.0f)
            {
                nextVelocity_x = 0.0f;
            }
        }
        else if (nextVelocity_x < 0.0f)
        {
            nextVelocity_x += airForce;
            if (nextVelocity_x < -maxSpeed)
            {
                nextVelocity_x = -maxSpeed;
            }
            else if (nextVelocity_x > 0.0f)
            {
                nextVelocity_x = 0.0f;
            }
        }
        if (nextVelocity_y > 0.0f)
        {
            nextVelocity_y -= airForce;
            if (nextVelocity_y > maxSpeed)
            {
                nextVelocity_y = maxSpeed;
            }
            else if (nextVelocity_y < 0.0f)
            {
                nextVelocity_y = 0.0f;
            }
        }
        else if (nextVelocity_y < 0.0f)
        {
            nextVelocity_y += airForce;
            if (nextVelocity_y < -maxSpeed)
            {
                nextVelocity_y = -maxSpeed;
            }
            else if (nextVelocity_y > 0.0f)
            {
                nextVelocity_y = 0.0f;
            }
        }
        Velocity = new Vector2(nextVelocity_x, nextVelocity_y);
    }
}
