﻿
public abstract class AStatus_Fairy : AStatus
{
    public new IActor_Fairy Actor { get; }

    /// <summary>
    /// 기본 치명타 확률
    /// </summary>
    public abstract float CriticalChance_Default
    {
        get;
    }
    /// <summary>
    /// 치명타 확률 증가율
    /// </summary>
    public virtual float CriticalChance_Increase
    {
        get;
    }
    /// <summary>
    /// 최종 치명타 확률
    /// </summary>
    public float CriticalChance
    {
        get { return CriticalChance_Default + CriticalChance_Increase; }
    }

    /// <summary>
    /// 기본 치명타 배율
    /// </summary>
    public abstract float CriticalMag_Default
    {
        get;
    }
    /// <summary>
    /// 치명타 배율 증가율
    /// </summary>
    public virtual float CriticalMag_Increase
    {
        get;
    }
    /// <summary>
    /// 최종 치명타 배율
    /// </summary>
    public float CriticalMag
    {
        get { return 1.0f + CriticalMag_Default + CriticalMag_Increase; }
    }

    public AStatus_Fairy(IActor_Fairy actor) : base(actor)
    {
        Actor = actor;
    }
}