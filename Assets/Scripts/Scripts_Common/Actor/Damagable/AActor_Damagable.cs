﻿using UnityEngine;
using System;

public interface IActor_Damagable : IActor, IDamagable
{
    new AStatus_Damagable Status { get; }
}

public abstract class AActor_Damagable<EState>
    : AActor<EState>, IActor_Damagable
    where EState : Enum
{
    private AStatus_Damagable mStatus;
    public new AStatus_Damagable Status
    {
        get { return mStatus; }
        protected set { base.Status = value; mStatus = value; }
    }

    /// <summary>
    /// 데미지 프로퍼티
    /// </summary>
    public Damage Damage { get; private set; }

    /// <summary>
    /// 힐 프로퍼티
    /// </summary>
    public Heal Heal { get; private set; }

    // 데미지로 인한 무적 남은 시간
    private float mInvincibleRemainTime;
    // 데미지로 인한 이펙트 남은 시간
    private float mEffectRemainTime;
    // 데미지로 인한 이펙트 활성화 여부
    private bool mIsEffectActive;

    /// <summary>
    /// 데미지 활성화 메소드
    /// </summary>
    public void ActivateDamage()
    {
        // 임시 무적 상태를 공격할 수 있는 데미지가 아니고, 임시 무적 상태인 경우
        if (!Damage.IsImportant && mInvincibleRemainTime > 0.0f)
        {
            return;
        }

        // 데미지 산출
        float damage = Damage.GetDamage(Status.GetEA(Damage.ElementalType), Status.Defense);

        // 데미지가 1보다 작다면
        if (damage < 1.0f)
        {
            // 1로 만듬, 0인 데미지는 무시
            damage = 1.0f;
        }

        // 생명력 감소
        Status.Life -= damage;

        // 임시 무적시간과 이펙트 효과 시간 갱신
        mInvincibleRemainTime = GameConstant.INVINCIBLE_TIME_BY_HIT;
        mEffectRemainTime = GameConstant.DAMAGE_EFFECT_TIME;
        mIsEffectActive = true;
        float prevAlpha = MainRenderer.color.a;
        MainRenderer.color = new Color(0.8f, 0.0f, 0.0f, prevAlpha);

        // 수신자의 위치
        Vector2 dstPosition = MainCollider.bounds.center;
        // 송신자의 위치
        Vector2 srcPosition = Damage.SrcPosition;
        // 송신자와 수신자의 방향 벡터
        Vector2 forceDirection = dstPosition - srcPosition;
        // 송신자와 수신자의 정규화 방향 벡터
        Vector2 normalizedForce = forceDirection.normalized;

        // 데미지의 정보를 표시하는 이펙트 객체를 가져옴
        Effect_Damage effect = EffectPool.Get(EEffectType.Damage) as Effect_Damage;

        if(effect != null)
        {
            effect.Damage = (int)damage;
            effect.DamageType = Damage.DamageType;
            effect.transform.position = new Vector3(dstPosition.x, dstPosition.y + 1.0f, 0.0f);
            effect.Activate();
        }

        // 데미지로 인해 생성되는 이펙트 객체를 하나 가져온다
        EEffectType effectType = Damage.EffectType;
        if (effectType != EEffectType.None)
        {
            Effect_Hit hitEffect = EffectPool.Get(Damage.EffectType) as Effect_Hit;
            if (hitEffect != null)
            {
                hitEffect.IsFollowCamera = false;
                // 이펙트 방향 설정
                hitEffect.SetDirection(normalizedForce, true);
                // 메인 콜라이더 바운드
                Bounds dstBounds = MainCollider.bounds;
                Vector2 dstExtents = dstBounds.extents;
                // 이펙트 위치 설정
                hitEffect.transform.position = new Vector3(dstPosition.x + dstExtents.x * normalizedForce.x * -1.0f, dstPosition.y + dstExtents.y * normalizedForce.y * -1.0f);
                // 이펙트 활성화
                hitEffect.Activate();
            }
        }

        OnDamage();

        // 생명력이 1보다 작다면
        if (Status.Life < 1.0f)
        {
            // 죽은상태
            OnDie();
            return;
        }

        // 최종적인 힘을 구함
        float force = Damage.SrcForce - Status.Weight;
        // 힘이 0보다 크다면
        if (force > 0.0f)
        {
            if (!DebuffHandler.Get(EDebuffType.Restrain_Bush).IsActivated)
            {
                // Hit 모션과 넉백 효과
                OnHit();
            }
            //Vector2 velocity = Velocity;
            //Velocity = velocity + (normalizedForce) * force;
            // 이전의 속도는 없앰
            Velocity = normalizedForce * force;
        }
    }

    /// <summary>
    /// 힐 활성화 메소드
    /// </summary>
    public void ActivateHeal()
    {

    }

    protected virtual void OnDamage() { }
    protected virtual void OnDie()
    {
        if(CastingSkill != null && CastingSkill.IsActivated)
        {
            CastingSkill.Deactivate();
        }

        for(EBuffType e = EBuffType.None + 1; e < EBuffType.Count; e++)
        {
            IBuff buff = BuffHandler.Get(e);
            buff.Deactivate();
        }

        for(EDebuffType e = EDebuffType.None + 1; e < EDebuffType.Count; e++)
        {
            IDebuff debuff = DebuffHandler.Get(e);
            debuff.Deactivate();
        }
    }
    protected abstract void OnHit();

    protected override void Awake()
    {
        base.Awake();

        Damage = new Damage();
        Heal = new Heal();
    }

    protected override void Update()
    {
        base.Update();

        // 데미지로 인한 무적 유지 시간이 남아있는 경우
        if(mInvincibleRemainTime > 0.0f)
        {
            mInvincibleRemainTime -= Time.deltaTime;
        }

        // 데미지로 인한 이펙트가 발생한 경우
        if (mIsEffectActive)
        {
            // 데미지로 인한 이펙트 유지 시간이 남아있는 경우
            if (mEffectRemainTime > 0.0f)
            {
                mEffectRemainTime -= Time.deltaTime;
            }
            // 데미지로 인한 이펙트 유지 시간이 다된 경우
            else
            {
                mIsEffectActive = false;
                float alpha = MainRenderer.color.a;
                MainRenderer.color = new Color(1.0f, 1.0f, 1.0f, alpha);
            }
        }
    }
}
