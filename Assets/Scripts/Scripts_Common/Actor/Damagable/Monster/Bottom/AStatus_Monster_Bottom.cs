﻿using UnityEngine;

public abstract class AStatus_Monster_Bottom : AStatus_Monster
{
    public new IActor_Monster_Bottom Actor { get; }

    public AStatus_Monster_Bottom(IActor_Monster_Bottom actor) : base(actor)
    {
        Actor = actor;
    }
}
