﻿using UnityEngine;
using System;

public interface IActor_Monster_Bottom 
    : IActor_Monster
{
    new AStatus_Monster_Bottom Status { get; }
}

public abstract class AActor_Monster_Bottom<EState>
    : AActor_Monster<EState>, IActor_Monster_Bottom
    where EState : Enum
{
    private AStatus_Monster_Bottom mStatus;
    /// <summary>
    /// 몬스터 속성
    /// </summary>
    public new AStatus_Monster_Bottom Status
    {
        get { return mStatus; }
        protected set { base.Status = value; mStatus = value; }
    }

    /// <summary>
    /// 몬스터 위치 타입
    /// </summary>
    public override EPositionType PositionType => EPositionType.Bottom;
}
