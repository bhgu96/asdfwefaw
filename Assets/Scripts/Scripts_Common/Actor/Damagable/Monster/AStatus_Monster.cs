﻿using UnityEngine;
using System.Collections;

public abstract class AStatus_Monster : AStatus_Damagable
{
    public new IActor_Monster Actor { get; }

    /// <summary>
    /// 기본 레벨
    /// </summary>
    public abstract int Level_Default { get; }
    /// <summary>
    /// 난이도에 의한 레벨 증가량
    /// </summary>
    public int Level_Increment_Difficulty
    {
        get { return (PlayerData.Difficulty - 1) * 10; }
    }
    /// <summary>
    /// 레벨 증가량
    /// </summary>
    public virtual int Level_Increment
    {
        get { return Level_Increment_Difficulty; }
    }
    /// <summary>
    /// 최종 레벨
    /// </summary>
    public sealed override int Level
    {
        get { return Level_Default + Level_Increment; }
    }

    /// <summary>
    /// 기본 경험치 획득량
    /// </summary>
    public abstract int Exp_Default { get; }
    /// <summary>
    /// 난이도에 의한 경험지 증가량
    /// </summary>
    public int Exp_Increment_Difficulty
    {
        get { return (PlayerData.Difficulty - 1) * 100; }
    }
    /// <summary>
    /// 경험치 증가량
    /// </summary>
    public virtual int Exp_Increment
    {
        get { return Exp_Increment_Difficulty; }
    }
    /// <summary>
    /// 난이도에 의한 경험치 증가율
    /// </summary>
    public float Exp_Increase_Difficulty
    {
        get { return (PlayerData.Difficulty - 1) * 0.02f; }
    }
    /// <summary>
    /// 경험치 증가율
    /// </summary>
    public virtual float Exp_Increase
    {
        get { return Exp_Increase_Difficulty; }
    }
    /// <summary>
    /// 최종 경험치 획득량
    /// </summary>
    public int Exp
    {
        get { return (int)((Exp_Default + Exp_Increment) * (1.0f + Exp_Increase)); }
    }

    /// <summary>
    /// 기본 소울량
    /// </summary>
    public abstract int Souls_Default { get; }
    /// <summary>
    /// 난이도에 의한 소울 증가량
    /// </summary>
    public int Souls_Increment_Difficulty
    {
        get { return (PlayerData.Difficulty - 1) * 10; }
    }
    /// <summary>
    /// 소울 증가량
    /// </summary>
    public virtual int Souls_Increment
    {
        get { return Souls_Increment_Difficulty; }
    }
    /// <summary>
    /// 소울 증가율
    /// </summary>
    public virtual int Souls_Increase { get; }
    /// <summary>
    /// 최종 소울량
    /// </summary>
    public int Souls
    {
        get { return (int)((Souls_Default + Souls_Increment) * (1.0f + Souls_Increase)); }
    }

    /// <summary>
    /// 난이도에 의한 최대 생명력 증가량
    /// </summary>
    public float MaxLife_Increment_Difficulty
    {
        get { return (PlayerData.Difficulty - 1) * 100.0f; }
    }
    /// <summary>
    /// 생명력 증가량
    /// </summary>
    public override float MaxLife_Increment
    {
        get { return base.MaxLife_Increment + MaxLife_Increment_Difficulty; }
    }
    /// <summary>
    /// 난이도에 의한 최대 생명력 증가율
    /// </summary>
    public float MaxLife_Increase_Difficulty
    {
        get { return (PlayerData.Difficulty - 1) * 0.02f; }
    }
    /// <summary>
    /// 최대 생명력 증가율
    /// </summary>
    public override float MaxLife_Increase
    {
        get { return base.MaxLife_Increase + MaxLife_Increase_Difficulty; }
    }

    /// <summary>
    /// 난이도에 의한 공격력 증가량
    /// </summary>
    public float Offense_Increment_Difficulty
    {
        get { return (PlayerData.Difficulty - 1) * 10.0f; }
    }
    /// <summary>
    /// 공격력 증가량
    /// </summary>
    public override float Offense_Increment
    {
        get { return base.Offense_Increment + Offense_Increment_Difficulty; }
    }
    /// <summary>
    /// 난이도에 의한 공격력 증가율
    /// </summary>
    public float Offense_Increase_Difficulty
    {
        get { return (PlayerData.Difficulty - 1) * 0.02f; }
    }
    /// <summary>
    /// 공격력 증가율
    /// </summary>
    public override float Offense_Increase
    {
        get { return base.Offense_Increase + Offense_Increase_Difficulty; }
    }
    
    /// <summary>
    /// 난이도에 의한 방어력 증가량
    /// </summary>
    public float Defense_Increment_Difficulty
    {
        get { return (PlayerData.Difficulty - 1) * 10.0f; }
    }
    /// <summary>
    /// 방어력 증가량
    /// </summary>
    public override float Defense_Increment
    {
        get { return base.Defense_Increment + Defense_Increment_Difficulty; }
    }
    /// <summary>
    /// 난이도에 의한 방어력 증가율
    /// </summary>
    public float Defense_Increase_Difficulty
    {
        get { return (PlayerData.Difficulty - 1) * 0.02f; }
    }
    /// <summary>
    /// 방어력 증가율
    /// </summary>
    public override float Defense_Increase
    {
        get { return base.Defense_Increase + Defense_Increase_Difficulty; }
    }

    public override float Weight_Increase
    {
        get
        {
            if(Actor.DebuffHandler.Get(EDebuffType.Restrain_Bush).IsActivated)
            {
                return base.Weight_Increase - 0.9f;
            }
            return base.Weight_Increase;
        }
    }

    /// <summary>
    /// 난이도에 의한 친화도 증가량
    /// </summary>
    /// <returns></returns>
    public int GetEA_Increment_Difficulty()
    {
        return (PlayerData.Difficulty - 1) * 10;
    }
    /// <summary>
    /// 속성 친화도 증가량
    /// </summary>
    /// <param name="e"></param>
    /// <returns></returns>
    public override int GetEA_Increment(EElementalType e)
    {
        return base.GetEA_Increment(e) + GetEA_Increment_Difficulty();
    }

    public AStatus_Monster(IActor_Monster actor) : base(actor)
    {
        Actor = actor;
    }
}
