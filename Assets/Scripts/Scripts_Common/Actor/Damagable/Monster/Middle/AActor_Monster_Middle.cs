﻿using UnityEngine;
using System;

public interface IActor_Monster_Middle
    : IActor_Monster
{
    /// <summary>
    /// 몬스터 속성
    /// </summary>
    new AStatus_Monster_Middle Status { get; }

    /// <summary>
    /// 기존 속도에 가속도를 가하는 메소드
    /// Status 객체의 MoveSpeed (최대 속도)를 고려하여 작동
    /// </summary>
    /// <param name="acceleration"></param>
    void AddVelocity(float maxSpeed, Vector2 acceleration);
}

public abstract class AActor_Monster_Middle<EState>
    : AActor_Monster<EState>, IActor_Monster_Middle
    where EState : Enum
{
    private AStatus_Monster_Middle mStatus;
    /// <summary>
    /// 몬스터의 속성
    /// </summary>
    public new AStatus_Monster_Middle Status
    {
        get { return mStatus; }
        protected set { base.Status = value; mStatus = value; }
    }

    public EPositionType PositionType_Default
    {
        get { return EPositionType.Middle; }
    }
    public virtual EPositionType PositionType_Change
    {
        get; private set;
    } = EPositionType.None;
    
    /// <summary>
    /// 몬스터의 위치
    /// </summary>
    public override EPositionType PositionType
    {
        get
        {
            if (DebuffHandler.Get(EDebuffType.Restrain_Bush).IsActivated)
            {
                if(PositionType_Change != EPositionType.Bottom)
                {
                    PositionType_Change = EPositionType.Bottom;
                    Rigidbody.gravityScale = 1.0f;
                }

                return PositionType_Change;
            }

            if (PositionType_Change != EPositionType.None)
            {
                PositionType_Change = EPositionType.None;
                Rigidbody.gravityScale = 0.0f;
            }

            return PositionType_Default;
        }
    }

    private bool mIsAccelerated_x = false;
    private bool mIsAccelerated_y = false;

    /// <summary>
    /// 수직 이동 입력을 검사하는 메소드
    /// </summary>
    public void CheckMoveVertical()
    {
        if (PositionType == EPositionType.Middle)
        {
            // 위쪽 이동 입력시 위쪽 가속도 적용
            if (Input.MoveVertical > Mathf.Epsilon)
            {
                AddVelocity(Status.MoveSpeed, new Vector2(0.0f, Status.Acceleration * Time.fixedDeltaTime));
            }
            // 아래쪽 이동 입력시 아래쪽 가속도 적용
            else if (Input.MoveVertical < -Mathf.Epsilon)
            {
                AddVelocity(Status.MoveSpeed, new Vector2(0.0f, -Status.Acceleration * Time.fixedDeltaTime));
            }
        }
    }

    /// <summary>
    /// 기존 속도에 가속도를 가하는 메소드
    /// Status 객체의 MoveSpeed (최대속도)를 고려하여 작동
    /// </summary>
    /// <param name="acceleration"></param>
    public void AddVelocity(float maxSpeed, Vector2 acceleration)
    {
        Vector2 curVelocity = Velocity;
        float nextVelocity_x = curVelocity.x;
        float nextVelocity_y = curVelocity.y;

        // x 축 속도가 최대 속도를 넘는 경우
        if (Mathf.Abs(nextVelocity_x) > maxSpeed)
        {
            if (nextVelocity_x < -maxSpeed)
            {
                // x 축 가속도가 현재 속도와 다른 방향인 경우
                if (acceleration.x > Mathf.Epsilon)
                {
                    mIsAccelerated_x = true;
                    // 가속도 적용
                    nextVelocity_x += acceleration.x;
                }
            }
            else if (curVelocity.x > maxSpeed)
            {
                // x 축 가속도가 현재 속도와 다른 방향인 경우
                if (acceleration.x < -Mathf.Epsilon)
                {
                    mIsAccelerated_x = true;
                    // 가속도 적용
                    nextVelocity_x += acceleration.x;
                }
            }
        }
        // 현재 속도가 최대 속도를 넘지 않는 경우
        else
        {
            if (Mathf.Abs(acceleration.x) > Mathf.Epsilon)
            {
                mIsAccelerated_x = true;
                nextVelocity_x += acceleration.x;
                // 가속도를 더한 x 축 속도가 최대 속도를 넘는 경우
                if (nextVelocity_x < -maxSpeed)
                {
                    nextVelocity_x = -maxSpeed;
                }
                else if (nextVelocity_x > maxSpeed)
                {
                    nextVelocity_x = maxSpeed;
                }
            }
        }

        // y 축 속도가 최대 속도를 넘는 경우
        if (Mathf.Abs(nextVelocity_y) > maxSpeed)
        {
            if (nextVelocity_y < -maxSpeed)
            {
                // y 축 가속도가 현재 속도와 다른 방향인 경우
                if (acceleration.y > Mathf.Epsilon)
                {
                    mIsAccelerated_y = true;
                    // 가속도 적용
                    nextVelocity_y += acceleration.y;
                }
            }
            else if (nextVelocity_y > maxSpeed)
            {
                // y 축 가속도가 현재 속도와 다른 방향인 경우
                if (acceleration.y < -Mathf.Epsilon)
                {
                    mIsAccelerated_y = true;
                    // 가속도 적용
                    nextVelocity_y += acceleration.y;
                }
            }
        }
        // y 축 속도가 최대 속도를 넘지 않는 경우
        else
        {
            if (Mathf.Abs(acceleration.y) > Mathf.Epsilon)
            {
                mIsAccelerated_y = true;
                nextVelocity_y += acceleration.y;
                // 가속도를 다한 속도가 최대 속도를 넘는 경우
                if (nextVelocity_y < -maxSpeed)
                {
                    nextVelocity_y = -maxSpeed;
                }
                else if (nextVelocity_y > maxSpeed)
                {
                    nextVelocity_y = maxSpeed;
                }
            }
        }
        
        Velocity = new Vector2(nextVelocity_x, nextVelocity_y);
    }

    protected override void FixedUpdate()
    {
        base.FixedUpdate();
        if (PositionType == EPositionType.Middle)
        {
            AddResistance();
        }
    }

    /// <summary>
    /// 공기 저항을 고려하여 속도를 조정하는 메소드
    /// </summary>
    private void AddResistance()
    {
        Vector2 curVelocity = Velocity;
        float nextVelocity_x = curVelocity.x;
        float nextVelocity_y = curVelocity.y;

        // x 축 가속이 적용되지 않은 경우
        if (!mIsAccelerated_x)
        {
            // x 축 속도가 양수인 경우
            if (nextVelocity_x > Mathf.Epsilon)
            {
                nextVelocity_x -= Status.AirResistance * Time.fixedDeltaTime;

                // 공기 저항을 고려하여 부호가 달라진 경우
                if (nextVelocity_x < -Mathf.Epsilon)
                {
                    // 속도를 0으로 갱신
                    nextVelocity_x = 0.0f;
                }
            }
            // x 축 속도가 음수인 경우
            else if (nextVelocity_x < -Mathf.Epsilon)
            {
                nextVelocity_x += Status.AirResistance * Time.fixedDeltaTime;

                // 공기 저항을 고려하여 부호가 달라진 경우
                if (nextVelocity_x > Mathf.Epsilon)
                {
                    // 속도를 0으로 갱신
                    nextVelocity_x = 0.0f;
                }
            }
        }
        // x 축 가속이 적용된 경우
        else
        {
            mIsAccelerated_x = false;
        }

        // y 축 가속이 적용되지 않은 경우
        if (!mIsAccelerated_y)
        {
            // y 축 속도가 양수인 경우
            if (nextVelocity_y > Mathf.Epsilon)
            {
                nextVelocity_y -= Status.AirResistance * Time.fixedDeltaTime;

                // 공기 저항을 고려하여 부호가 달라진 경우
                if (nextVelocity_y < -Mathf.Epsilon)
                {
                    nextVelocity_y = 0.0f;
                }
            }
            // y 축 속도가 음수인 경우
            else if (nextVelocity_y < -Mathf.Epsilon)
            {
                nextVelocity_y += Status.AirResistance * Time.fixedDeltaTime;

                // 공기저항을 고려하여 부호가 달라진 경우
                if (nextVelocity_y > Mathf.Epsilon)
                {
                    nextVelocity_y = 0.0f;
                }
            }
        }
        // y축 가속이 적용된 경우
        else
        {
            mIsAccelerated_y = false;
        }

        Velocity = new Vector2(nextVelocity_x, nextVelocity_y);
    }
}
