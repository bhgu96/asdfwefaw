﻿

public abstract class AStatus_Monster_Middle
    : AStatus_Monster
{
    public new IActor_Monster_Middle Actor { get; }

    /// <summary>
    /// 기본 가속도
    /// </summary>
    public abstract float Acceleration_Default { get; }
    /// <summary>
    /// 가속도 증가율
    /// </summary>
    public virtual float Acceleration_Increase { get; }
    /// <summary>
    /// 최종 가속도
    /// </summary>
    public float Acceleration
    {
        get { return Acceleration_Default * (1.0f + Acceleration_Increase); }
    }

    /// <summary>
    /// 기본 공기저항
    /// </summary>
    public abstract float AirResistance_Default { get; }
    /// <summary>
    /// 공기저항 증가율
    /// </summary>
    public virtual float AirResistance_Increase { get; }
    /// <summary>
    /// 최종 공기저항
    /// </summary>
    public float AirResistance
    {
        get { return AirResistance_Default * (1.0f + AirResistance_Increase); }
    }

    /// <summary>
    /// 기본 최소 거리
    /// </summary>
    public abstract float MinDistance_Default { get; }
    /// <summary>
    /// 최소 거리 감소율
    /// </summary>
    public virtual float MinDistance_Decrease { get; }
    /// <summary>
    /// 최종 최소 거리
    /// </summary>
    public float MinDistance
    {
        get { return MinDistance_Default * (1.0f - MinDistance_Decrease); }
    }
    /// <summary>
    /// 기본 최대 거리
    /// </summary>
    public abstract float MaxDistance_Default { get; }
    /// <summary>
    /// 최대 거리 감소율
    /// </summary>
    public virtual float MaxDistance_Decrease { get; }
    /// <summary>
    /// 최종 최대 거리
    /// </summary>
    public float MaxDistance
    {
        get { return MaxDistance_Default * (1.0f - MaxDistance_Decrease); }
    }
    /// <summary>
    /// 기본 최소 높이
    /// </summary>
    public abstract float MinHeight_Default { get; }
    /// <summary>
    /// 최소 높이 감소율
    /// </summary>
    public virtual float MinHeight_Decrease { get; }
    /// <summary>
    /// 최종 최소 높이
    /// </summary>
    public float MinHeight
    {
        get { return MinHeight_Default * (1.0f - MinHeight_Decrease); }
    }
    /// <summary>
    /// 기본 최대 높이
    /// </summary>
    public abstract float MaxHeight_Default { get; }
    /// <summary>
    /// 최대 높이 감소율
    /// </summary>
    public virtual float MaxHeight_Decrease { get; }
    /// <summary>
    /// 기본 최대 높이
    /// </summary>
    public float MaxHeight
    {
        get { return MaxHeight_Default * (1.0f - MaxHeight_Decrease); }
    }

    public AStatus_Monster_Middle(IActor_Monster_Middle actor) : base(actor)
    {
        Actor = actor;
    }
}
