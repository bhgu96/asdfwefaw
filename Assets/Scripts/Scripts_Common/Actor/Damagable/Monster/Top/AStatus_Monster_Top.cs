﻿
public abstract class AStatus_Monster_Top : AStatus_Monster
{
    public new IActor_Monster_Top Actor { get; }

    public AStatus_Monster_Top(IActor_Monster_Top actor) : base(actor)
    {
        Actor = actor;
    }

    public abstract float Friction_Default { get; }
    public virtual float Friction_Increase { get; }
    public float Friction
    {
        get { return Friction_Default * (1.0f + Friction_Increase); }
    }
}
