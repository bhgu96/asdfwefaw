﻿using UnityEngine;
using System;

public interface IActor_Monster_Top
    : IActor_Monster
{
    /// <summary>
    /// 몬스터의 속성
    /// </summary>
    new AStatus_Monster_Top Status { get; }
}

public abstract class AActor_Monster_Top<EState>
    : AActor_Monster<EState>, IActor_Monster_Top
    where EState : Enum
{
    private AStatus_Monster_Top mStatus;
    /// <summary>
    /// 몬스터의 속성
    /// </summary>
    public new AStatus_Monster_Top Status
    {
        get { return mStatus; }
        protected set { base.Status = value; mStatus = value; }
    }
    /// <summary>
    /// 몬스터의 위치
    /// </summary>
    public override EPositionType PositionType => EPositionType.Top;

    protected override void FixedUpdate()
    {
        base.FixedUpdate();

        if (!IsZeroFriction)
        {
            AddFriction();
        }
    }

    /// <summary>
    /// 공기 저항을 고려하여 속도를 조정하는 메소드
    /// </summary>
    private void AddFriction()
    {
        Vector2 curVelocity = Velocity;
        float nextVelocity_x = curVelocity.x;

        // x 축 속도가 양수인 경우
        if (nextVelocity_x > Mathf.Epsilon)
        {
            nextVelocity_x -= Status.Friction * Time.fixedDeltaTime;

            // 공기 저항을 고려하여 부호가 달라진 경우
            if (nextVelocity_x < -Mathf.Epsilon)
            {
                // 속도를 0으로 갱신
                nextVelocity_x = 0.0f;
            }
        }
        // x 축 속도가 음수인 경우
        else if (nextVelocity_x < -Mathf.Epsilon)
        {
            nextVelocity_x += Status.Friction * Time.fixedDeltaTime;

            // 공기 저항을 고려하여 부호가 달라진 경우
            if (nextVelocity_x > Mathf.Epsilon)
            {
                // 속도를 0으로 갱신
                nextVelocity_x = 0.0f;
            }
        }

        Velocity = new Vector2(nextVelocity_x, curVelocity.y);
    }
}
