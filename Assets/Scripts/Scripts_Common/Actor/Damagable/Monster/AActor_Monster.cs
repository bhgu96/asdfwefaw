﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System;
using Object;

public interface IActor_Monster : IActor_Damagable, IPoolable<EMonsterType>
{
    /// <summary>
    /// 액터의 기본 속성
    /// </summary>
    EElementalType ElementalType { get; }
    /// <summary>
    /// 몬스터 등급
    /// </summary>
    EMonsterRank MonsterRank { get; }
    /// <summary>
    /// 몬스터 전투 타입
    /// </summary>
    ECombatType CombatType { get; }
    /// <summary>
    /// 위치 타입
    /// </summary>
    EPositionType PositionType { get; }
    /// <summary>
    /// 몬스터의 Status
    /// </summary>
    new AStatus_Monster Status { get; }

    /// <summary>
    /// 몬스터 풀에서 활성화시키는 메소드
    /// </summary>
    /// <returns></returns>
    bool Activate();
    /// <summary>
    /// 몬스터를 풀링하도록 비활성화 시키는 메소드
    /// </summary>
    /// /// <returns></returns>
    bool Deactivate();
}


public abstract class AActor_Monster<EState>
    : AActor_Damagable<EState>, IActor_Monster
    where EState : Enum
{
    [SerializeField]
    [Tooltip("몬스터 타입")]
    private EMonsterType mMonsterType;
    /// <summary>
    /// 몬스터 풀링 전용 타입
    /// </summary>
    public EMonsterType Type { get { return mMonsterType; } }

    [SerializeField]
    [Tooltip("기본 속성")]
    private EElementalType mElementalType;
    /// <summary>
    /// 액터의 기본 공격 속성
    /// </summary>
    public EElementalType ElementalType { get { return mElementalType; } }

    /// <summary>
    /// 몬스터 풀링 가능 여부
    /// </summary>
    public virtual bool IsPooled { get { return mIsPooled && !gameObject.activeInHierarchy; } }
    private bool mIsPooled = true;
    /// <summary>
    /// 몬스터 등급
    /// </summary>
    public abstract EMonsterRank MonsterRank { get; }

    /// <summary>
    /// 전투 방식
    /// </summary>
    public abstract ECombatType CombatType { get; }

    /// <summary>
    /// 위치 타입
    /// </summary>
    public abstract EPositionType PositionType { get; }

    private AStatus_Monster mStatus;
    /// <summary>
    /// 몬스터 속성
    /// </summary>
    public new AStatus_Monster Status
    {
        get { return mStatus; }
        protected set { base.Status = value; mStatus = value; }
    }

    /// <summary>
    /// 몬스터 활성화 메소드
    /// </summary>
    /// <returns></returns>
    public bool Activate()
    {
        if (IsPooled)
        {
            gameObject.SetActive(true);
            mIsPooled = false;
            OnActivate();
            return true;
        }
        return false;
    }

    /// <summary>
    /// 몬스터 비활성화 메소드
    /// </summary>
    /// <returns></returns>
    public bool Deactivate()
    {
        if (!IsPooled)
        {
            gameObject.SetActive(false);
            mIsPooled = true;
            OnDeactivate();
            return true;
        }
        return false;
    }

    protected virtual void OnActivate() { }
    protected virtual void OnDeactivate() { }

    //protected override void OnDamage()
    //{
    //    if (Damage.CanReflex && Damage.SrcInstance is IDamagable damagable)
    //    {
    //        Damage damage = damagable.Damage;

    //        damage.SetDamage(false, true, false
    //            , EEffectType.None, EDamageType.Monster, ElementalType
    //            , Status.Level, Status.GetEA(ElementalType), Status.Offense, 0.0f
    //            , transform.position, this);

    //        damagable.ActivateDamage();
    //    }
    //}

    private void Start()
    {
        SceneManager.activeSceneChanged += SceneManager_activeSceneChanged;
    }

    private void SceneManager_activeSceneChanged(Scene arg0, Scene arg1)
    {
        Deactivate();
    }
}
