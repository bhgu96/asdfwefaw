﻿using UnityEngine;
using System;

public interface IActor_Giant : IActor_Damagable
{
    new AStatus_Giant Status { get; }
}


public abstract class AActor_Giant<EState>
    : AActor_Damagable<EState>, IActor_Giant
    where EState : Enum
{
    private AStatus_Giant mStatus;
    public new AStatus_Giant Status
    {
        get { return mStatus; }
        protected set { base.Status = value; mStatus = value; }
    }
}