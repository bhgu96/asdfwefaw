﻿using UnityEngine;
using System.Collections.Generic;

public abstract class AStatus_Giant : AStatus_Damagable
{
    public new IActor_Giant Actor { get; }

    /// <summary>
    /// 기본 생명력 재생량
    /// </summary>
    public abstract float LifeRps_Default
    {
        get;
    }
    /// <summary>
    /// 생명력 재생 증가량
    /// </summary>
    public virtual float LifeRps_Increment
    {
        get;
    }
    /// <summary>
    /// 생명력 재생 증가율
    /// </summary>
    public virtual float LifeRps_Increase
    {
        get;
    }
    /// <summary>
    /// 최종 생명력 재생
    /// </summary>
    public float LifeRps
    {
        get { return (LifeRps_Default + LifeRps_Increment) * (1.0f + LifeRps_Increase); }
    }

    /// <summary>
    /// 기본 치명타 확률
    /// </summary>
    public abstract float CriticalChance_Default
    {
        get;
    }
    /// <summary>
    /// 치명타 확률 증가율
    /// </summary>
    public virtual float CriticalChance_Increase
    {
        get;
    }
    /// <summary>
    /// 최종 치명타 확률
    /// </summary>
    public float CriticalChance
    {
        get { return CriticalChance_Default + CriticalChance_Increase; }
    }

    /// <summary>
    /// 기본 치명타 배율
    /// </summary>
    public abstract float CriticalMag_Default
    {
        get;
    }
    /// <summary>
    /// 치명타 배율 증가율
    /// </summary>
    public virtual float CriticalMag_Increase
    {
        get;
    }
    /// <summary>
    /// 최종 치명타 배율
    /// </summary>
    public float CriticalMag
    {
        get { return 1.0f + CriticalMag_Default + CriticalMag_Increase; }
    }

    public AStatus_Giant(IActor_Giant actor) : base(actor)
    {
        Actor = actor;
    }
}
