﻿using UnityEngine;
using System.Collections;

public abstract class AStatus_Damagable : AStatus
{
    public new IActor_Damagable Actor { get; }

    private float mLife;
    /// <summary>
    /// 생명력
    /// </summary>
    public float Life
    {
        get
        {
            float maxLife = MaxLife;
            if(mLife > maxLife)
            {
                mLife = maxLife;
            }
            return mLife;
        }
        set
        {
            float maxLife = MaxLife;
            if(value < 1.0f)
            {
                value = 0.0f;
            }
            else if(value > maxLife)
            {
                value = maxLife;
            }
            mLife = value;
        }
    }

    /// <summary>
    /// 기본 최대 생명력
    /// </summary>
    public abstract float MaxLife_Default { get; }
    /// <summary>
    /// 최대 생명력 증가량
    /// </summary>
    public virtual float MaxLife_Increment { get; }
    /// <summary>
    /// 최대 생명력 증가율
    /// </summary>
    public virtual float MaxLife_Increase { get; }
    /// <summary>
    /// 최종 최대 생명력
    /// </summary>
    public float MaxLife
    {
        get
        {
            return (MaxLife_Default + MaxLife_Increment) * (1.0f + MaxLife_Increase);
        }
    }

    /// <summary>
    /// 기본 방어력
    /// </summary>
    public abstract float Defense_Default { get; }
    /// <summary>
    /// 방어력 증가량
    /// </summary>
    public virtual float Defense_Increment { get; }
    /// <summary>
    /// 방어력 증가율
    /// </summary>
    public virtual float Defense_Increase { get; }
    /// <summary>
    /// 최종 방어력
    /// </summary>
    public float Defense
    {
        get
        {
            return (Defense_Default + Defense_Increment) * (1.0f + Defense_Increase);
        }
    }

    /// <summary>
    /// 기본 무게
    /// </summary>
    public abstract float Weight_Default { get; }
    /// <summary>
    /// 무게 증가량
    /// </summary>
    public virtual float Weight_Increment { get; }
    /// <summary>
    /// 무게 증가율
    /// </summary>
    public virtual float Weight_Increase { get; }
    /// <summary>
    /// 최종 무게
    /// </summary>
    public float Weight
    {
        get { return (Weight_Default + Weight_Increment) * (1.0f + Weight_Increase); }
    }

    public AStatus_Damagable(IActor_Damagable actor) : base(actor)
    {
        Actor = actor;
    }
}
