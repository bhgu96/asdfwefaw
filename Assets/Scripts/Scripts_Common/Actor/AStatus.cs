﻿using UnityEngine;
using System;
using System.Collections;

public abstract class AStatus
{
    public IActor Actor { get; }

    /// <summary>
    /// 이름 태그
    /// </summary>
    public abstract string NameTag { get; }

    /// <summary>
    /// 플레이버 텍스트 태그
    /// </summary>
    public abstract string FlavorTag { get; }

    /// <summary>
    /// 레벨
    /// </summary>
    public abstract int Level { get; }

    /// <summary>
    /// 기본 이동 속도
    /// </summary>
    public abstract float MoveSpeed_Default { get; }

    /// <summary>
    /// 이동 속도 증가율
    /// </summary>
    public virtual float MoveSpeed_Increase { get { return -MoveSpeed_Decrease_Debuff; } }

    /// <summary>
    /// 디버프에 의한 이동속도 감소율
    /// </summary>
    public float MoveSpeed_Decrease_Debuff
    {
        get
        {
            float decrease = 0.0f;
            Debuff_Slowing slowing = Actor.DebuffHandler.Get(EDebuffType.Slowing_Bush) as Debuff_Slowing;
            if(slowing != null && slowing.IsActivated)
            {
                decrease += slowing.MoveSpeed_Decrease;
            }

            return decrease;
        }
    }

    /// <summary>
    /// 최종 이동 속도
    /// </summary>
    public float MoveSpeed
    {
        get { return MoveSpeed_Default * (1.0f + MoveSpeed_Increase); }
    }

    /// <summary>
    /// 시전 속도 증가율
    /// </summary>
    public virtual float CastSpeed_Increase { get { return -CastSpeed_Decrease_Debuff; } }

    /// <summary>
    /// 디버프에 의한 시전속도 감소율
    /// </summary>
    public virtual float CastSpeed_Decrease_Debuff
    {
        get
        {
            float decrease = 0.0f;
            Debuff_Slowing slowing = Actor.DebuffHandler.Get(EDebuffType.Slowing_Bush) as Debuff_Slowing;
            if(slowing != null && slowing.IsActivated)
            {
                decrease += slowing.CastSpeed_Decrease;
            }

            return decrease;
        }
    }

    /// <summary>
    /// 최종 시전 속도
    /// </summary>
    public float CastSpeed
    {
        get { return 1.0f + CastSpeed_Increase; }
    }

    /// <summary>
    /// 기본 공격력
    /// </summary>
    public abstract float Offense_Default { get; }
    /// <summary>
    /// 공격력 증가량
    /// </summary>
    public virtual float Offense_Increment { get; }
    /// <summary>
    /// 공격력 증가율
    /// </summary>
    public virtual float Offense_Increase { get; }
    /// <summary>
    /// 최종 공격력
    /// </summary>
    public float Offense
    {
        get
        {
            return (Offense_Default + Offense_Increment) * (1.0f + Offense_Increase);
        }
    }
    /// <summary>
    /// 기본 밀어내는 힘
    /// </summary>
    public abstract float Force_Default { get; }
    /// <summary>
    /// 힘 증가량
    /// </summary>
    public virtual float Force_Increment { get; }
    /// <summary>
    /// 힘 증가율
    /// </summary>
    public virtual float Force_Increase { get; }
    /// <summary>
    /// 최종 힘
    /// </summary>
    public float Force
    {
        get { return (Force_Default + Force_Increment) * (1.0f + Force_Increase); }
    }

    /// <summary>
    /// 기본 속성 친화도
    /// </summary>
    /// <param name="type"></param>
    /// <returns></returns>
    public abstract int GetEA_Default(EElementalType e);
    /// <summary>
    /// 속성 친화도 증가량
    /// </summary>
    /// <param name="type"></param>
    /// <returns></returns>
    public virtual int GetEA_Increment(EElementalType e) { return 0; }
    /// <summary>
    /// 최종 속성 친화도
    /// </summary>
    /// <param name="type"></param>
    /// <returns></returns>
    public int GetEA(EElementalType e)
    {
        return GetEA_Default(e) + GetEA_Increment(e);
    }

    public AStatus(IActor actor)
    {
        Actor = actor;
    }
}
