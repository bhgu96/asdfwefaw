﻿
using System;
using System.Collections.Generic;

namespace FSM
{
    public abstract class AState<E> where E : Enum
    {
        public E State { get; private set; }

        private AState<E>[] mNextStates;
        private bool mIsStarted = false;

        public AState(E state)
        {
            State = state;
        }

        public void SetTransitions(params AState<E>[] nextStates)
        {
            mNextStates = nextStates;
        }

        /// <summary>
        /// 만족하는 다음 조건이 존재하지 않는 State를 실행하고,
        /// 해당 State를 반환하는 메소드
        /// </summary>
        /// <returns></returns>
        public AState<E> Execute()
        {
            if(!mIsStarted)
            {
                Awake();
            }

            for(int i=0; i<mNextStates.Length; i++)
            {
                if(mNextStates[i].Check())
                {
                    if(mIsStarted)
                    {
                        mIsStarted = false;
                        End();
                    }
                    return mNextStates[i].Execute();
                }
            }

            if(!mIsStarted)
            {
                mIsStarted = true;
                Start();
            }

            Behave();
            return this;
        }
        /// <summary>
        /// 행동 실행 메소드
        /// </summary>
        public virtual void Behave() { }

        /// <summary>
        /// 행동 조건 검사 전 준비 메소드
        /// </summary>
        public virtual void Awake() { }

        /// <summary>
        /// 행동 준비 메소드
        /// </summary>
        public virtual void Start() { }
        /// <summary>
        /// 행동 마무리 메소드
        /// </summary>
        public virtual void End() { }

        public override string ToString()
        {
            return State.ToString();
        }

        /// <summary>
        /// 전이 조건 검사 메소드
        /// </summary>
        /// <returns></returns>
        public abstract bool Check();
    }
}