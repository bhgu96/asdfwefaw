﻿
using System;
using System.Collections.Generic;

namespace FSM
{
    public class FiniteStateMachine<E> where E :Enum
    {
        public E State { get { return mState.State; } }

        private AState<E> mState;

        public void SetState(AState<E> state)
        {
            mState = state;
        }

        /// <summary>
        /// FSM 실행 메소드
        /// </summary>
        public void Execute()
        {
            mState = mState.Execute();
        }
    }
}