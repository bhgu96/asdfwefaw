﻿using UnityEngine;
using System.Collections;
using Object;

public abstract class ASoulite : AItem, IMountable<EItemType>
{
    public sealed override EItemCategory Category => EItemCategory.Soulite;
    public sealed override int MaxCount => 1;

    /// <summary>
    /// 장착 여부 프로퍼티
    /// </summary>
    public bool IsMounted { get; private set; }

    /// <summary>
    /// 풀링 가능 프로퍼티
    /// </summary>
    public override bool IsPooled => base.IsPooled && !IsMounted;

    /// <summary>
    /// 영혼석이 습득될 수 있는가에 관한 메소드
    /// </summary>
    public override bool CanBeOwned
    {
        get { return base.CanBeOwned && !IsMounted; }
    }

    /// <summary>
    /// 영혼석을 장착할 수 있는가에 관한 메소드
    /// </summary>
    /// <param name="user">영혼석을 장착하려 하는 대상</param>
    public virtual bool CanBeMounted
    {
        get { return IsOwned && !IsMounted; }
    }

    /// <summary>
    /// 영혼석을 장착 해제 할 수 있는가에 관한 메소드
    /// </summary>
    /// <returns></returns>
    public virtual bool CanBeUnMounted
    {
        get { return IsMounted; }
    }

    /// <summary>
    /// 장착되는 메소드
    /// </summary>
    /// <returns>장착 성공 여부</returns>
    public bool BeMounted()
    {
        // 영혼석이 장착 가능하다면
        if (CanBeMounted)
        {
            // 장착 상태로 전환
            IsMounted = true;
            OnBeMounted();
            return true;
        }
        return false;
    }
    /// <summary>
    /// 장착 해제되는 메소드
    /// </summary>
    /// <returns>장착 해제 성공 여부</returns>
    public bool BeUnMounted()
    {
        if(CanBeUnMounted)
        {
            // 비장착 상태로 전환
            IsMounted = false;
            OnBeUnMounted();
            return true;
        }
        return false;
    }

    /// <summary>
    /// 영혼석 장착시 호출되는 콜백 메소드
    /// </summary>
    protected virtual void OnBeMounted()
    { }

    /// <summary>
    /// 영혼석 장착 해제시 호출되는 콜백 메소드
    /// </summary>
    protected virtual void OnBeUnMounted()
    { }
}
