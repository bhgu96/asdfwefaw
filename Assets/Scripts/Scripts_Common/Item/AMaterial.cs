﻿using UnityEngine;
using System.Collections;

public abstract class AMaterial : AItem
{
    public sealed override EItemCategory Category => EItemCategory.Material;
}
