﻿using UnityEngine;
using System.Collections;

public class Pocket
{
    /// <summary>
    /// 아이템의 개수
    /// </summary>
    public int Count { get; private set; }
    /// <summary>
    /// 현재 아이템
    /// </summary>
    public AItem Item { get; private set; }
    public bool IsFull
    {
        get
        {
            if (Count > 0 && Count >= Item.MaxCount)
            {
                return true;
            }
            return false;
        }
    }

    public IActor Owner { get; private set; }

    public Pocket(IActor owner)
    {
        Owner = owner;
    }

    /// <summary>
    /// 아이템을 포켓에 넣는 메소드
    /// </summary>
    /// <param name="item">넣을 아이템</param>
    /// <param name="count">넣을 아이템의 수</param>
    /// <param name="prevItem">기존의 아이템</param>
    /// <returns>넣고 남거나 기존에 있던 아이템의 수</returns>
    public int PutIn(AItem item, int count, out AItem existingItem)
    {
        // 아이템이 null이거나 넣을 아이템 수가 0 이하인 경우
        if (item == null || count <= 0)
        {
            // 기존의 아이템을 해제
            Item.BeUnOwned();
            existingItem = Item;
            Item = null;
            int existingCount = Count;
            Count = 0;
            return existingCount;
        }
        else
        {
            // 기존의 아이템이 존재하지 않는 경우
            if (Count == 0)
            {
                // 아이템을 습득한다
                Item = item;
                Item.BeOwned(Owner);
                // 아이템 중첩 개수 이하라면
                if (count <= Item.MaxCount)
                {
                    Count = count;
                    // 기존의 아이템이 없음을 표시
                    existingItem = null;
                    return 0;
                }
                // 아이템이 중첩 개수 이상이라면
                else
                {
                    Count = Item.MaxCount;
                    existingItem = ItemPool.Get(Item.Type);
                    return count - Count;
                }
            }
            // 기존의 아이템과 넣는 아이템의 종류가 다른 경우
            else if (item.Type != Item.Type)
            {
                // 기존의 아이템을 소유하지 않음을 표시
                existingItem = Item;
                existingItem.BeUnOwned();
                // 넣을 아이템을 소유상태로 만듬
                Item = item;
                Item.BeOwned(Owner);
                int prevCount = Count;
                Count = count;
                // 기존의 아이템 개수 반환
                return prevCount;
            }

            // 아이템 개수의 합이 최대 개수 이하인 경우
            if (Count + count <= Item.MaxCount)
            {
                existingItem = null;
                // 기존의 아이템 개수 증가
                Count += count;
                return 0;
            }
            // 아이템 개수의 합이 최대 개수를 초과하는 경우
            else
            {
                existingItem = item;
                int prevCount = Count;
                // 최대 개수로 넣고
                Count = Item.MaxCount;
                // 남은 개수를 반환
                return count - (Item.MaxCount - prevCount);
            }
        }
    }

    public AItem TakeOut(int count)
    {
        // 빼낼 아이템의 개수가 1 이상 현재 개수 이하라면
        if (count > 0 && count <= Count)
        {
            AItem item = Item;
            // 아이템 개수 감소
            Count -= count;
            // 아이템 개수가 0이라면
            if (Count == 0)
            {
                // 아이템을 비소유 상태로 만든다.
                Item.BeUnOwned();
                Item = null;
                return item;
            }

            return ItemPool.Get(item.Type);
        }
        // 그 외의 경우는 꺼내지 못함
        else
        {
            return null;
        }
    }
}