﻿using UnityEngine;
using System.Collections;

public abstract class AValueable : AItem
{
    public sealed override EItemCategory Category => EItemCategory.Valueable;

}
