﻿using UnityEngine;
using System.Collections.Generic;

public class Inventory
{
    /// <summary>
    /// 용량
    /// </summary>
    public int Capacity { get; private set; }
    public int CountOfRows { get; private set; }
    public int CountOfColumns { get; private set; }

    private Dictionary<EItemCategory, Pocket[,]> mPocketDict;

    public IActor Owner { get; private set; }

    /// <summary>
    /// 기본 생성자
    /// </summary>
    /// <param name="countOfRows">행의 수</param>
    /// <param name="countOfColumns">열의 수</param>
    public Inventory(IActor owner, int countOfRows, int countOfColumns)
    {
        Owner = owner;

        // 최대 용량 할당
        Capacity = countOfRows * countOfColumns;
        CountOfRows = countOfRows;
        CountOfColumns = countOfColumns;

        mPocketDict = new Dictionary<EItemCategory, Pocket[,]>();

        for(EItemCategory group = EItemCategory.None + 1; group < EItemCategory.Count; group++)
        {
            Pocket[,] pockets = new Pocket[countOfRows, countOfColumns];
            for (int row = 0; row < countOfRows; row++)
            {
                for (int column = 0; column < countOfColumns; column++)
                {
                    pockets[row, column] = new Pocket(Owner);
                }
            }
            mPocketDict.Add(group, pockets);
        }
    }

    /// <summary>
    /// 인벤토리의 주머니 정보를 보는 메소드
    /// </summary>
    /// <param name="category">아이템 범주</param>
    /// <param name="row">행</param>
    /// <param name="column">열</param>
    /// <param name="item">주머니에 있는 아이템</param>
    /// <returns>주머니에 있는 아이템 개수</returns>
    public int Peek(EItemCategory category, int row, int column, out AItem item)
    {
        Pocket[,] pockets = mPocketDict[category];
        item = pockets[row, column].Item;
        return pockets[row, column].Count;
    }
    /// <summary>
    /// 인벤토리의 주머니에 아이템을 넣는 메소드
    /// </summary>
    /// <param name="row">행</param>
    /// <param name="column">열</param>
    /// <param name="item">넣을 아이템</param>
    /// <param name="count">넣을 아이템의 개수</param>
    /// <param name="existingItem">기존에 주머니에 있던 아이템 혹은 넣고 남은 아이템</param>
    /// <returns>넣고 남은 개수 혹은 기존의 주머니에 있던 남은 아이템</returns>
    public int PutIn(int row, int column, AItem item, int count, out AItem existingItem)
    {
        Pocket[,] pockets = mPocketDict[item.Category];
        return pockets[row, column].PutIn(item, count, out existingItem);
    }

    /// <summary>
    /// 아이템을 위치지정없이 그냥 넣는 메소드
    /// </summary>
    /// <param name="item"></param>
    /// <param name="count"></param>
    /// <returns></returns>
    public int PutIn(AItem item, int count, out AItem existingItem)
    {
        Pocket[,] pockets = mPocketDict[item.Category];
        int blankRow = -1;
        int blankColumn = -1;

        for(int row = 0; row < pockets.GetLength(0); row++)
        {
            for(int column = 0; column < pockets.GetLength(1); column++)
            {
                // 주머니에 무언가 들어있는데
                if (pockets[row, column].Count > 0)
                {
                    //  그것이 같은 종류의 아이템이고, 아이템을 다 넣은 경우
                    if (pockets[row, column].Item.Type == item.Type
                        && (count = pockets[row, column].PutIn(item, count, out item)) == 0)
                    {
                        existingItem = item;
                        return 0;
                    }
                    // 다 넣지 못한 경우 이어서 반복문 시작
                }
                // 주머니에 아무것도 들어있지 않은 경우
                else if(blankRow == -1 || blankColumn == -1)
                {
                    blankRow = row;
                    blankColumn = column;
                }
            }
        }
        // 반복문을 탈출했다면 가득 찼거나 빈곳을 찾았을 때이다.
        // 가득 찬 경우
        if(blankRow == -1 || blankColumn == -1)
        {
            existingItem = item;
            return count;
        }
        // 빈 곳이 있는 경우
        else
        {
            // 아이템을 그자리에 넣는다. 그런데 아이템이 남은 경우
            if ((count = pockets[blankRow, blankColumn].PutIn(item, count, out item)) > 0)
            {
                // 다시 인벤토리를 조사하는데, 같은 종류의 아이템은 이미 다 조사했으므로 빈곳만 조사한다.
                for(int row = 0; row < pockets.GetLength(0); row++)
                {
                    for(int column = 0; column < pockets.GetLength(1); column++)
                    {
                        // 주머니가 비었다면
                        if(pockets[row, column].Count == 0)
                        {
                            // 아이템을 넣는다
                            count = pockets[row, column].PutIn(item, count, out item);
                            // 아이템이 남지 않았다면
                            if(count == 0)
                            {
                                existingItem = item;
                                return 0;
                            }
                            // 아이템이 여전히 남았다면 계속 탐색
                        }
                    }
                }

                // 모든 주머니를 탐색했는데 아이템이 남았다면 반환
                if(count > 0)
                {
                    existingItem = item;
                    return count;
                }
            }
            existingItem = item;
            return 0;
        }
    }

    /// <summary>
    /// 인벤토리의 주머니에서 아이템을 개수만큼 꺼내는 메소드
    /// </summary>
    /// <param name="category">아이템 범주</param>
    /// <param name="row">행</param>
    /// <param name="column">열</param>
    /// <param name="count">꺼내는 개수</param>
    /// <returns>꺼낸 아이템</returns>
    public AItem TakeOut(EItemCategory category, int row, int column, int count)
    {
        Pocket[,] pockets = mPocketDict[category];
        return pockets[row, column].TakeOut(count);
    }
}
