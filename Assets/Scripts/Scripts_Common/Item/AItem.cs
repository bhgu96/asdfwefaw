﻿using UnityEngine;
using System.Collections;
using Object;



public abstract class AItem : MonoBehaviour, IPoolable<EItemType>
{
    [SerializeField]
    private EItemType mType;
    /// <summary>
    /// 아이템 종류
    /// </summary>
    public EItemType Type
    {
        get { return mType; }
    }

    /// <summary>
    /// Pool 관리를 위해 사용되는 변수
    /// 비활성화 되어있고 소유 상태가 아니면 Pool에 보관된 상태이다.
    /// </summary>
    public virtual bool IsPooled
    {
        get
        {
            return !gameObject.activeInHierarchy
              && !IsOwned;
        }
    }

    /// <summary>
    /// 액터에 습득되었는가에 관한 변수
    /// </summary>
    public bool IsOwned { get; private set; } = false;

    /// <summary>
    /// 아이템의 분류
    /// </summary>
    public abstract EItemCategory Category { get; }

    /// <summary>
    /// 아이템을 중첩시킬 수 있는 최대 개수
    /// </summary>
    public abstract int MaxCount { get; }

    [SerializeField]
    private Sprite mIcon;
    /// <summary>
    /// 아이템의 아이콘 프로퍼티
    /// </summary>
    public Sprite Icon { get { return mIcon; } }

    /// <summary>
    /// 아이템 이름을 반환하는 프로퍼티
    /// </summary>
    public virtual string Name
    {
        get { return DataManager.GetScript(mNameTag); }
    }

    [SerializeField]
    private string mNameTag;
    public string NameTag
    {
        get { return mNameTag; }
    }

    /// <summary>
    /// 아이템 설명을 반환하는 프로퍼티
    /// </summary>
    public virtual string Description
    {
        get { return DataManager.GetScript(mDescriptionTag); }
    }

    [SerializeField]
    private string mDescriptionTag;
    public string DescriptionTag
    {
        get { return mDescriptionTag; }
    }

    public IActor Owner { get; private set; }

    /// <summary>
    /// 아이템 소유 가능 여부 프로퍼티
    /// </summary>
    public virtual bool CanBeOwned
    {
        get { return !IsOwned; }
    }

    /// <summary>
    /// 아이템 비소유 가능 여부 프로퍼티
    /// </summary>
    /// <returns></returns>
    public virtual bool CanBeUnOwned
    {
        get { return IsOwned; }
    }

    /// <summary>
    /// 아이템을 습득하는 메소드
    /// </summary>
    /// <returns>습득 여부 반환</returns>
    public bool BeOwned(IActor owner)
    {
        if(CanBeOwned)
        {
            IsOwned = true;
            Owner = owner;
            OnBeOwned();
            return true;
        }
        return false;
    }
    /// <summary>
    /// 아이템을 버리는 메소드
    /// </summary>
    /// <returns>버리기 여부 반환</returns>
    public bool BeUnOwned()
    {
        if(CanBeUnOwned)
        {
            IsOwned = false;
            OnBeUnOwned();
            return true;
        }
        return false;
    }

    /// <summary>
    /// 아이템 소유 메소드 호출시 호출되는 콜백 메소드
    /// </summary>
    protected virtual void OnBeOwned()
    { }

    /// <summary>
    /// 아이템 비소유 메소드 호출시 호출되는 콜백 메소드
    /// </summary>
    protected virtual void OnBeUnOwned()
    { }
}
