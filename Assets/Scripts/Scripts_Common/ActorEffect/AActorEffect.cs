﻿using System;
using UnityEngine;
using Object;

public interface IActorEffect
{
    /// <summary>
    /// 액터 효과 아이콘
    /// </summary>
    Sprite Icon { get; }

    /// <summary>
    /// 액터 효과 이름 태그
    /// </summary>
    string NameTag { get; }
    /// <summary>
    /// 액터 효과 이름
    /// </summary>
    string Name { get; }

    /// <summary>
    /// 액터 효과 설명 태그
    /// </summary>
    string DescriptionTag { get; }
    /// <summary>
    /// 액터 효과 설명
    /// </summary>
    string Description { get; }

    /// <summary>
    /// 지속 시간 유무
    /// </summary>
    bool HasDuration { get; }

    /// <summary>
    /// 지속 시간
    /// </summary>
    float Duration { get; }

    /// <summary>
    /// 액터 효과의 대상이 되는 액터
    /// </summary>
    IActor Target { get; }

    /// <summary>
    /// 활성화 여부
    /// </summary>
    bool IsActivated { get; }

    /// <summary>
    /// 액터 효과 활성화
    /// </summary>
    void Activate();
    /// <summary>
    /// 제한 시간이 있는 액터 효과 활성화
    /// </summary>
    /// <param name="duration"></param>
    void Activate(float duration);

    /// <summary>
    /// 액터 효과 비활성화
    /// </summary>
    void Deactivate();
}

[RequireComponent(typeof(Animator))]
public abstract class AActorEffect<EStateType> : StateBase<EStateType>, IActorEffect
    where EStateType : Enum
{

    [SerializeField]
    private Sprite mIcon;
    /// <summary>
    /// 액터 효과 아이콘
    /// </summary>
    public Sprite Icon { get { return mIcon; } }

    /// <summary>
    /// 액터 효과 이름 태그
    /// </summary>
    public abstract string NameTag { get; }
    /// <summary>
    /// 액터 효과 이름
    /// </summary>
    public virtual string Name { get { return DataManager.GetScript(NameTag); } }

    /// <summary>
    /// 액터 효과 설명 태그
    /// </summary>
    public abstract string DescriptionTag { get; }
    /// <summary>
    /// 액터 효과 설명
    /// </summary>
    public virtual string Description { get { return DataManager.GetScript(DescriptionTag); } }

    /// <summary>
    /// 애니메이터
    /// </summary>
    public Animator Animator { get; private set; }

    /// <summary>
    /// 액터 효과가 적용되는 대상
    /// </summary>
    public IActor Target { get; private set; }

    /// <summary>
    /// 지속 시간 유무
    /// </summary>
    public bool HasDuration { get; private set; }

    /// <summary>
    /// 남은 지속시간
    /// </summary>
    public float Duration { get; private set; }

    /// <summary>
    /// 활성화 여부
    /// </summary>
    public bool IsActivated { get; private set; }

    /// <summary>
    /// 액터 효과 활성화 메소드
    /// </summary>
    /// <param name="receiver"></param>
    public void Activate()
    {
        if (!IsActivated)
        {
            if(!gameObject.activeInHierarchy)
            {
                gameObject.SetActive(true);
            }

            IsActivated = true;
            HasDuration = false;
            OnActivate();
        }
    }

    /// <summary>
    /// 제한시간이 있는 액터 효과 활성화 메소드
    /// </summary>
    /// <param name="duration"></param>
    public void Activate(float duration)
    {
        HasDuration = true;
        Duration = duration;

        if (!IsActivated)
        {
            if(!gameObject.activeInHierarchy)
            {
                gameObject.SetActive(true);
            }

            IsActivated = true;
            OnActivate();
        }
    }

    /// <summary>
    /// 액터 효과 비활성화 메소드
    /// </summary>
    public void Deactivate()
    {
        if (IsActivated)
        {
            IsActivated = false;
            OnDeactivate();
        }
    }

    /// <summary>
    /// 활성화시에 호출되는 콜백 메소드
    /// </summary>
    /// <param name="receiver"></param>
    protected virtual void OnActivate() { }

    /// <summary>
    /// 비활성화시에 호출되는 콜백메소드
    /// </summary>
    protected virtual void OnDeactivate() { }

    protected override void Awake()
    {
        base.Awake();
        Animator = GetComponent<Animator>();
        Target = GetComponentInParent<IActor>();

        gameObject.SetActive(false);
    }

    protected override void Update()
    {
        base.Update();

        if (IsActivated && HasDuration)
        {
            if (Duration > 0.0f)
            {
                Duration -= Time.deltaTime;
            }
            else
            {
                Deactivate();
            }
        }
    }
}