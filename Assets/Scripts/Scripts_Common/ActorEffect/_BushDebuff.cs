﻿//using UnityEngine;
//using System.Collections;

//public enum EStateType_BushDebuff
//{

//}

//public class BushDebuff : AActorEffect<EStateType_BushDebuff>
//{
//    /// <summary>
//    /// 덩굴 디버프 소형 사이즈 최대 레벨
//    /// </summary>
//    public const int MAX_LEVEL_Small = 3;
//    /// <summary>
//    /// 덩굴 디버프 중형 사이즈 최대 레벨
//    /// </summary>
//    public const int MAX_LEVEL_Medium = 4;
//    /// <summary>
//    /// 레벨이  최대가 아닐때 레벨 감소 시간
//    /// </summary>
//    public const float DECREASE_LEVEL_TIME = 4.0F;
//    /// <summary>
//    /// 레벨이 최대일때 레벨 감소 시간
//    /// </summary>
//    public const float DECREASE_MAX_TIME = 6.0F;
//    /// <summary>
//    /// 레벨이 증가하기 위한 경험치
//    /// </summary>
//    public const int EXP_LEVEL_Up = 1;

//    /// <summary>
//    /// Bush의 Level 프로퍼티
//    /// </summary>
//    public int Level
//    {
//        get { return mLevel; }
//        set
//        {
//            // 레벨 감소 시간 갱신
//            mLevelRemainTime = DECREASE_LEVEL_TIME;
//            // 액터 크기가 소형인 경우
//            if (mSize == EActorSize.Small)
//            {
//                // 갱신 값이 최대 레벨 이상인 경우
//                if (value > MAX_LEVEL_Small)
//                {
//                    // 갱신 값을 최대 레벨로 수정
//                    value = MAX_LEVEL_Small;
//                } // 갱신 값이 0보다 작은 경우
//                else if (value < 0)
//                {
//                    // 갱신 값을 0으로 수정
//                    value = 0;
//                    Deactivate();
//                } // 현재 레벨과 갱신 레벨의 값이 다른 경우
//                if (mLevel != value)
//                {
//                    // 현재 레벨이 최대 레벨인 경우
//                    if (mLevel == MAX_LEVEL_Small)
//                    {
//                        Color prevColor = Target.MainRenderer.color;
//                        // 렌더러의 알파값을 최대로 설정
//                        Target.MainRenderer.color = new Color(prevColor.r, prevColor.g, prevColor.b, 1.0f);
//                        // 현재 레벨이 최대 레벨이 아님을 설정
//                        mIsMaxLevel = false;
//                    } // 갱신 값이 최대 레벨인 경우
//                    else if (value == MAX_LEVEL_Small)
//                    {
//                        // 레벨 감소 시간을 최대 레벨 감소 시간으로 변경
//                        mLevelRemainTime = DECREASE_MAX_TIME;
//                        Color prevColor = Target.MainRenderer.color;
//                        // 렌더러의 알파값을 0으로 설정
//                        Target.MainRenderer.color = new Color(prevColor.r, prevColor.g, prevColor.b, 0.0f);
//                        // 현재 레벨이 최대 레벨임을 설정
//                        mIsMaxLevel = true;
//                    }
//                }

//                // 현재 레벨 갱신
//                mLevel = value;
//                mAnimator.SetInteger("level", mLevel);
//            } // 액터 크기가 중형인 경우
//            else if (mSize == EActorSize.Medium)
//            {
//                // 갱신 값이 최대 레벨보다 큰 경우
//                if (value > MAX_LEVEL_Medium)
//                {
//                    // 갱신 값을 최대 레벨로 수정
//                    value = MAX_LEVEL_Medium;
//                }
//                // 갱신 값이 0보다 작은 경우
//                else if (value < 0)
//                {
//                    Deactivate();
//                    // 갱신 값을 0으로 수정
//                    value = 0;
//                }
//                // 갱신 값이 현재 값과 다른 경우
//                if (mLevel != value)
//                {
//                    // 현재 레벨이 최대 레벨인 경우
//                    if (mLevel == MAX_LEVEL_Medium)
//                    {
//                        Color prevColor = Target.MainRenderer.color;
//                        // 렌더러의 알파 값을 1로 설정
//                        Target.MainRenderer.color = new Color(prevColor.r, prevColor.g, prevColor.b, 1.0f);
//                        // 현재 레벨이 최대 레벨임을 표시
//                        mIsMaxLevel = false;
//                    }
//                    // 갱신 레벨이 최대 레벨인 경우
//                    else if (value == MAX_LEVEL_Medium)
//                    {
//                        // 레벨 감소 시간을 최대 레벨 감소 시간으로 변경
//                        mLevelRemainTime = DECREASE_MAX_TIME;
//                        Color prevColor = Target.MainRenderer.color;
//                        // 렌더러의 알파 값을 0으로 설정
//                        Target.MainRenderer.color = new Color(prevColor.r, prevColor.g, prevColor.b, 0.0f);
//                        // 현재 레벨이 최대임을 표시
//                        mIsMaxLevel = true;
//                    }
//                }
//                mLevel = value;
//                mAnimator.SetInteger("level", mLevel);
//            }
//        }
//    }
//    private int mLevel;

//    /// <summary>
//    /// Bush의 Burning 프로퍼티
//    /// </summary>
//    public bool IsBurning
//    {
//        get
//        {
//            return mIsBurning;
//        }
//        set
//        {
//            if (mIsBurning != value)
//            {
//                mIsBurning = value;
//                mAnimator.SetBool("burn", mIsBurning);

//                if (mSize == EActorSize.Small)
//                {
//                    if (mLevel == MAX_LEVEL_Small)
//                    {
//                        mAnimator.SetInteger("anim", (int)Random.Range(0.0f, mCountOfExplosion_Small));
//                    }
//                }
//                else if (mSize == EActorSize.Medium)
//                {
//                    if (mLevel == MAX_LEVEL_Medium)
//                    {
//                        mAnimator.SetInteger("anim", (int)Random.Range(0.0f, mCountOfExplosion_Medium));
//                    }
//                }
//            }
//        }
//    }
//    private bool mIsBurning;

//    /// <summary>
//    /// Burning 상태일때의 데미지
//    /// </summary>
//    public int BurningDamage
//    {
//        get
//        {
//            // 액터가 소형인 경우
//            if (mSize == EActorSize.Small)
//            {
//                // 레벨이 1인 경우
//                if (mLevel == 1)
//                {
//                    return 2;
//                }
//                // 레벨이 2인 경우
//                else if (mLevel == 2)
//                {
//                    return 5;
//                }
//            }
//            // 액터가 중형인 경우
//            else if (mSize == EActorSize.Medium)
//            {
//                // 레벨이 1인 경우
//                if (mLevel == 1)
//                {
//                    return 2;
//                }
//                // 레벨이 2인 경우
//                else if (mLevel == 2)
//                {
//                    return 5;
//                }
//                // 레벨이 3인 경우
//                else if (mLevel == 3)
//                {
//                    return 10;
//                }
//            }
//            return 0;
//        }
//    }
//    [SerializeField]
//    private Transform[] mBushEmberHolders;
//    [SerializeField]
//    [Tooltip("소형 폭발시 재생 가능한 애니메이션 개수")]
//    private int mCountOfExplosion_Small;
//    [SerializeField]
//    [Tooltip("중형 폭발시 재생 가능한 애니메이션 개수")]
//    private int mCountOfExplosion_Medium;

//    private EActorSize mSize;

//    private Animator mAnimator;
//    private float mLevelRemainTime;
//    private bool mIsMaxLevel;
//    private int mExp;
//    private int mPrevHp;
//    private bool mIsEnd;

//    private void Awake()
//    {
//        mAnimator = GetComponent<Animator>();
//    }

//    protected override void Initialize()
//    {
//        mSize = Target.ActorState.Size;
//        // 애니메이터를 액터 크기로 설정
//        mAnimator.SetInteger("size", (int)mSize);
//        // 레벨 감소 시간 설정
//        mLevelRemainTime = DECREASE_LEVEL_TIME;
//        // 최초 생성시 Burning은 false
//        IsBurning = false;
//        // 최초 생성시 Level 은 1
//        Level = 1;
//        // 최초 생성시 경험치는 0
//        mExp = 0;
//        mIsMaxLevel = false;
//        mIsEnd = false;
//    }

//    protected override bool End()
//    {
//        transform.position = Target.transform.position;
//        if (Level > 0)
//        {
//            Level = 0;
//        }
//        if (IsBurning)
//        {
//            IsBurning = false;
//        }
//        return mIsEnd;
//    }
//    protected override bool Execute()
//    {
//        transform.position = Target.transform.position;
//        // 최대 레벨이고, Burning 상태라면
//        if (mIsMaxLevel && mIsBurning)
//        {
//            // 액터를 Die 상태로 만든다.
//            DamageInfo damage = Target.Damage;
//            damage.Damage = 99999;
//            damage.Force = 0.0f;
//            damage.SrcInstance = this;
//            Target.InvokeDamage();

//            Color prevColor = Target.MainRenderer.color;
//            // 렌더러의 알파 값을 1로 설정
//            Target.MainRenderer.color = new Color(prevColor.r, prevColor.g, prevColor.b, 1.0f);
//            return false;
//        }
//        // 액터가 죽은 상태인 경우
//        if (Target.ActorState.HealthPoint == 0)
//        {
//            return false;
//        }

//        if (mLevelRemainTime > 0.0f)
//        {
//            mLevelRemainTime -= Time.deltaTime;
//        }
//        else
//        {
//            Level--;
//        }

//        if (IsBurning)
//        {
//            DamageInfo damage = Target.Damage;
//            damage.Damage = BurningDamage;
//            damage.Force = 0.0f;
//            damage.SrcInstance = this;
//            Target.InvokeDamage();
//        }

//        return true;
//    }
//    protected override void Merge(AActorEffect actorEffect)
//    {
//        mExp++;
//        if (mExp >= EXP_LEVEL_Up)
//        {
//            mExp = 0;
//            Level++;
//        }
//    }

//    private void End_Animation()
//    {
//        mIsEnd = true;
//    }

//    private void GenerateBigBushEmber_Animation(int indexOfHolders)
//    {
//        GenerateBushEmber(indexOfHolders, 1);
//    }
//    private void GenerateSmallBushEmber_Animation(int indexOfHolders)
//    {
//        GenerateBushEmber(indexOfHolders, 0);
//    }
//    private void GenerateBushEmber(int indexOfHolders, int size)
//    {
//        //AProjectile projectile = ProjectileHandler.Fire(EProjectileFlags.BUSH_EMBER, mBushEmberHolders[indexOfHolders].position, new Vector3(1.0f, 1.0f, 1.0f), new Vector3(1.0f, 1.0f, 1.0f));
//        //if (projectile is BushEmberProjectile bushEmber)
//        //{
//        //    bushEmber.Type = size;
//        //}
//    }
//    private void Explode_Animation()
//    {
//    }
//}
