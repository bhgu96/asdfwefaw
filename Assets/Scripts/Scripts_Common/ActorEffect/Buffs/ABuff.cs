﻿using UnityEngine;
using System.Collections.Generic;
using System;
using Object;

public interface IBuff : IActorEffect, IHandleable<EBuffType>
{

}

public abstract class ABuff<EState> : AActorEffect<EState>, IBuff
    where EState : Enum
{
    [SerializeField]
    private EBuffType mType;
    /// <summary>
    /// 버프의 타입
    /// </summary>
    public EBuffType Type { get { return mType; } }

    /// <summary>
    /// 이름 태그
    /// </summary>
    public override string NameTag { get { return mInfo.NameTag; } }

    /// <summary>
    /// 설명 태그
    /// </summary>
    public override string DescriptionTag { get { return mInfo.DescriptionTag; } }

    private BuffInfo mInfo;

    protected override void Awake()
    {
        base.Awake();
        mInfo = DataManager.GetBuffInfo(mType);
    }
}

/// <summary>
/// 버프 정보를 저장하는 클래스
/// </summary>
public class BuffInfo
{
    public string NameTag { get; }
    public string DescriptionTag { get; }

    public BuffInfo(Dictionary<string, string> buffDict)
    {
        NameTag = buffDict["Name Tag"];
        DescriptionTag = buffDict["Desc Tag"];
    }
}
