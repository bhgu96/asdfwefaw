﻿using UnityEngine;
using System.Collections.Generic;
using System;
using Object;

public interface IDebuff : IActorEffect, IHandleable<EDebuffType>
{

}

public abstract class ADebuff<EState> : AActorEffect<EState>, IDebuff
    where EState : Enum
{
    /// <summary>
    /// 디버프 타입
    /// </summary>
    public abstract EDebuffType Type { get; }

    /// <summary>
    /// 이름 태그
    /// </summary>
    public override string NameTag { get { return mInfo.NameTag; } }

    /// <summary>
    /// 설명 태그
    /// </summary>
    public override string DescriptionTag { get { return mInfo.DescriptionTag; } }

    private DebuffInfo mInfo;

    protected override void Awake()
    {
        base.Awake();
        mInfo = DataManager.GetDebuffInfo(Type);
    }
}

/// <summary>
/// 디버프 정보를 저장하는 클래스
/// </summary>
public class DebuffInfo
{
    public string NameTag { get; }
    public string DescriptionTag { get; }

    public DebuffInfo(Dictionary<string, string> debuffDict)
    {
        NameTag = debuffDict["Name Tag"];
        DescriptionTag = debuffDict["Desc Tag"];
    }
}