﻿using UnityEngine;
using System.Collections;

public enum EStateType_Debuff_Restrain
{
    Idle, 
    Activate, 
    Active, 
    Deactivate, 
}

public class Debuff_Restrain : ADebuff<EStateType_Debuff_Restrain>
{
    [SerializeField]
    private EDebuffType mType;
    public override EDebuffType Type { get { return mType; } }

    protected override void OnActivate()
    {
        base.OnActivate();
        State = EStateType_Debuff_Restrain.Activate;

        // Bush 타입의 구속 디버프라면
        if (Type == EDebuffType.Restrain_Bush)
        {
            Target.MainRenderer.enabled = false;
        }
    }

    protected override void OnDeactivate()
    {
        base.OnDeactivate();
        State = EStateType_Debuff_Restrain.Deactivate;

        // Bush 타입의 구속 디버프라면
        if (Type == EDebuffType.Restrain_Bush)
        {
            Target.MainRenderer.enabled = true;
        }
    }

    protected override void Awake()
    {
        base.Awake();
        SetStates(new Debuff_Restrain_Idle(this)
            , new Debuff_Restrain_Activate(this)
            , new Debuff_Restrain_Active(this)
            , new Debuff_Restrain_Deactivate(this));
    }
}
