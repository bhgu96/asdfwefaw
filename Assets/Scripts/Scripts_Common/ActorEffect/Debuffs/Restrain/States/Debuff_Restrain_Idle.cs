﻿using UnityEngine;
using System.Collections;

public class Debuff_Restrain_Idle : State<EStateType_Debuff_Restrain>
{
    private Debuff_Restrain mDebuff;

    public Debuff_Restrain_Idle(Debuff_Restrain debuff) : base(EStateType_Debuff_Restrain.Idle)
    {
        mDebuff = debuff;
    }

    public override void Start()
    {
        mDebuff.Animator.SetInteger("state", (int)Type);
        mDebuff.gameObject.SetActive(false);
    }
}
