﻿using UnityEngine;
using System.Collections;

public class Debuff_Restrain_Deactivate : State<EStateType_Debuff_Restrain>
{
    private Debuff_Restrain mDebuff;

    public Debuff_Restrain_Deactivate(Debuff_Restrain debuff) : base(EStateType_Debuff_Restrain.Deactivate)
    {
        mDebuff = debuff;
    }

    public override void Start()
    {
        mDebuff.Animator.SetInteger("state", (int)Type);
    }
}
