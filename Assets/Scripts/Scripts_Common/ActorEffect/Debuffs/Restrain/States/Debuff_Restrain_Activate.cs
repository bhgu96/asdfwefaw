﻿using UnityEngine;
using System.Collections;

public class Debuff_Restrain_Activate : State<EStateType_Debuff_Restrain>
{
    private Debuff_Restrain mDebuff;
    private AStatus_Damagable mStatus;

    public Debuff_Restrain_Activate(Debuff_Restrain debuff) : base(EStateType_Debuff_Restrain.Activate)
    {
        mDebuff = debuff;
        mStatus = mDebuff.Target.Status as AStatus_Damagable;
    }

    public override void Start()
    {
        // 타겟 객체가 죽은 상태라면
        if (mStatus != null && mStatus.Life < 1.0f)
        {
            mDebuff.Deactivate();
            return;
        }
        mDebuff.Target.OnRestrain();
        mDebuff.Animator.SetInteger("state", (int)Type);
        mDebuff.Animator.SetInteger("size", (int)mDebuff.Target.Size);
    }
}
