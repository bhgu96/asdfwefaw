﻿using UnityEngine;
using System.Collections;

public class Debuff_Restrain_Active : State<EStateType_Debuff_Restrain>
{
    private Debuff_Restrain mDebuff;
    private AStatus_Damagable mStatus;

    public Debuff_Restrain_Active(Debuff_Restrain debuff) : base(EStateType_Debuff_Restrain.Active)
    {
        mDebuff = debuff;
        mStatus = debuff.Target.Status as AStatus_Damagable;
    }

    public override void Start()
    {
        mDebuff.Animator.SetInteger("state", (int)Type);
    }
}
