﻿using UnityEngine;
using System.Collections;

public enum EStateType_Debuff_Slowing
{
    None = -1, 

    Idle, 

    Activate, 
    Active, 
    Deactivate, 

    Count
}

public class Debuff_Slowing : ADebuff<EStateType_Debuff_Slowing>
{
    [SerializeField]
    private EDebuffType mType;
    public override EDebuffType Type { get { return mType; } }

    /// <summary>
    /// 이동 속도 감소율
    /// </summary>
    public float MoveSpeed_Decrease { get; set; }

    /// <summary>
    /// 시전 속도 감소율
    /// </summary>
    public float CastSpeed_Decrease { get; set; }

    protected override void OnActivate()
    {
        base.OnActivate();
        State = EStateType_Debuff_Slowing.Activate;
    }

    protected override void OnDeactivate()
    {
        base.OnDeactivate();
        State = EStateType_Debuff_Slowing.Deactivate;
    }

    protected override void Awake()
    {
        base.Awake();

        SetStates(new Debuff_Slowing_Idle(this)
            , new Debuff_Slowing_Activate(this)
            , new Debuff_Slowing_Active(this)
            , new Debuff_Slowing_Deactivate(this));
    }
}
