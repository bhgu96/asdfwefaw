﻿using UnityEngine;
using System.Collections;

public class Debuff_Slowing_Activate : State<EStateType_Debuff_Slowing>
{
    private Debuff_Slowing mDebuff;
    private AStatus_Damagable mStatus;

    public Debuff_Slowing_Activate(Debuff_Slowing debuff) : base(EStateType_Debuff_Slowing.Activate)
    {
        mDebuff = debuff;
        mStatus = mDebuff.Target.Status as AStatus_Damagable;
    }

    public override void Start()
    {
        mDebuff.Animator.SetInteger("state", (int)Type);
        mDebuff.Animator.SetInteger("size", (int)mDebuff.Target.Size);
    }

    public override void Update()
    {
        // 대상이 되는 액터의 생명력이 0이라면
        if (mStatus != null && mStatus.Life < 1.0f)
        {
            // 디버프 비활성화
            mDebuff.Deactivate();
        }
    }
}
