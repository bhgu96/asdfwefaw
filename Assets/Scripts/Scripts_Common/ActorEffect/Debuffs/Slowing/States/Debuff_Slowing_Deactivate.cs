﻿using UnityEngine;
using System.Collections;

public class Debuff_Slowing_Deactivate : State<EStateType_Debuff_Slowing>
{
    private Debuff_Slowing mDebuff;

    public Debuff_Slowing_Deactivate(Debuff_Slowing debuff) : base(EStateType_Debuff_Slowing.Deactivate)
    {
        mDebuff = debuff;
    }

    public override void Start()
    {
        mDebuff.Animator.SetInteger("state", (int)Type);
    }
}
