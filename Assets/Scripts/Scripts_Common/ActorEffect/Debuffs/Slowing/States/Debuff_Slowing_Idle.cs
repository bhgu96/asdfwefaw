﻿using UnityEngine;

public class Debuff_Slowing_Idle : State<EStateType_Debuff_Slowing>
{
    private Debuff_Slowing mDebuff;

    public Debuff_Slowing_Idle(Debuff_Slowing debuff) : base(EStateType_Debuff_Slowing.Idle)
    {
        mDebuff = debuff;
    }

    public override void Start()
    {
        mDebuff.Animator.SetInteger("state", (int)Type);
        mDebuff.gameObject.SetActive(false);
    }
}
