﻿using UnityEngine;
using System.Collections;

public class Debuff_Slowing_Active : State<EStateType_Debuff_Slowing>
{
    private Debuff_Slowing mDebuff;
    private AStatus_Damagable mStatus;


    public Debuff_Slowing_Active(Debuff_Slowing debuff) : base(EStateType_Debuff_Slowing.Active)
    {
        mDebuff = debuff;
        mStatus = debuff.Target.Status as AStatus_Damagable;
    }

    public override void Start()
    {
        mDebuff.Animator.SetInteger("state", (int)Type);
    }

    public override void Update()
    {
        // 대상이 되는 액터의 생명력이 0이라면
        if(mStatus != null && mStatus.Life < 1.0f)
        {
            // 디버프 비활성화
            mDebuff.Deactivate();
        }
    }
}
