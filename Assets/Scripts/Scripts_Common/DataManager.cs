﻿using UnityEngine;
using System;
using System.Collections.Generic;
using Csv;

public static class DataManager
{
    private static Dictionary<EGiantType_Player, DefaultStatus_Giant_Player> mDefaultStatusDict_Giant_Player;
    private static Dictionary<EFairyType_Player, DefaultStatus_Fairy_Player> mDefaultStatusDict_Fairy_Player;

    private static Dictionary<EMonsterType_Bottom_Normal, DefaultStatus_Monster_Bottom_Normal> mDefaultStatusDict_Monster_Bottom_Normal;
    private static Dictionary<EMonsterType_Bottom_Melee, DefaultStatus_Monster_Bottom_Melee> mDefaultStatusDict_Monster_Bottom_Melee;
    private static Dictionary<EMonsterType_Bottom_Ranged, DefaultStatus_Monster_Bottom_Ranged> mDefaultStatusDict_Monster_Bottom_Ranged;
    private static Dictionary<EMonsterType_Bottom_Tank, DefaultStatus_Monster_Bottom_Tank> mDefaultStatusDict_Monster_Bottom_Tank;
    private static Dictionary<EMonsterType_Middle_Melee, DefaultStatus_Monster_Middle_Melee> mDefaultStatusDict_Monster_Middle_Melee;
    private static Dictionary<EMonsterType_Middle_Summoner, DefaultStatus_Monster_Middle_Summoner> mDefaultStatusDict_Monster_Middle_Summoner;
    private static Dictionary<EMonsterType_Middle_Ranged, DefaultStatus_Monster_Middle_Ranged> mDefaultStatusDict_Monster_Middle_Ranged;
    private static Dictionary<EMonsterType_Top_SelfDestructive, DefaultStatus_Monster_Top_SelfDestructive> mDefaultStatusDict_Monster_Top_SelfDestructive;

    private static Dictionary<ESkillType_Fairy_Player, SkillInfo_Fairy_Player> mSkillDict_Fairy_Player;
    private static Dictionary<ESkillType_Giant_Player, SkillInfo_Giant_Player> mSkillDict_Giant_Player;
    private static Dictionary<ESkillType_Monster_Bottom_Normal, SkillInfo_Monster_Bottom_Normal> mSkillDict_Monster_Bottom_Normal;
    private static Dictionary<ESkillType_Monster_Bottom_Melee, SkillInfo_Monster_Bottom_Melee> mSkillDict_Monster_Bottom_Melee;
    private static Dictionary<ESkillType_Monster_Bottom_Ranged, SkillInfo_Monster_Bottom_Ranged> mSkillDict_Monster_Bottom_Ranged;
    private static Dictionary<ESkillType_Monster_Bottom_Tank, SkillInfo_Monster_Bottom_Tank> mSkillDict_Monster_Bottom_Tank;
    private static Dictionary<ESkillType_Monster_Middle_Melee, SkillInfo_Monster_Middle_Melee> mSkillDict_Monster_Middle_Melee;
    private static Dictionary<ESkillType_Monster_Middle_Summoner, SkillInfo_Monster_Middle_Summoner> mSkillDict_Monster_Middle_Summoner;
    private static Dictionary<ESkillType_Monster_Middle_Ranged, SkillInfo_Monster_Middle_Ranged> mSkillDict_Monster_Middle_Ranged;
    private static Dictionary<ESkillType_Monster_Top_SelfDestructive, SkillInfo_Monster_Top_SelfDestructive> mSkillDict_Monster_Top_SelfDestructive;

    private static Dictionary<EBuffType, BuffInfo> mBuffDict;
    private static Dictionary<EDebuffType, DebuffInfo> mDebuffDict;

    private static Dictionary<EProjectileType, ProjectileInfo> mProjectileDict;

    private static Dictionary<string, Dictionary<string, string>> mScriptDict;

    static DataManager()
    {
        Dictionary<string, Dictionary<string, string>> stringDict = CsvReader.Read(Application.streamingAssetsPath + "/Giant/Player/DefaultStatus_Giant_Player.csv");
        mDefaultStatusDict_Giant_Player = new Dictionary<EGiantType_Player, DefaultStatus_Giant_Player>();

        for(EGiantType_Player e = EGiantType_Player.None + 1; e < EGiantType_Player.Count; e++)
        {
            mDefaultStatusDict_Giant_Player.Add(e, new DefaultStatus_Giant_Player(stringDict[e.ToString()]));
        }

        stringDict = CsvReader.Read(Application.streamingAssetsPath + "/Fairy/Player/DefaultStatus_Fairy_Player.csv");
        mDefaultStatusDict_Fairy_Player = new Dictionary<EFairyType_Player, DefaultStatus_Fairy_Player>();

        for(EFairyType_Player e = EFairyType_Player.None + 1; e < EFairyType_Player.Count; e++)
        {
            mDefaultStatusDict_Fairy_Player.Add(e, new DefaultStatus_Fairy_Player(stringDict[e.ToString()]));
        }

        stringDict = CsvReader.Read(Application.streamingAssetsPath + "/Monster/Bottom/Normal/DefaultStatus_Monster_Bottom_Normal.csv");
        mDefaultStatusDict_Monster_Bottom_Normal = new Dictionary<EMonsterType_Bottom_Normal, DefaultStatus_Monster_Bottom_Normal>();

        for(EMonsterType_Bottom_Normal e = EMonsterType_Bottom_Normal.None + 1; e < EMonsterType_Bottom_Normal.Count; e++)
        {
            mDefaultStatusDict_Monster_Bottom_Normal.Add(e, new DefaultStatus_Monster_Bottom_Normal(stringDict[e.ToString()]));
        }

        stringDict = CsvReader.Read(Application.streamingAssetsPath + "/Monster/Bottom/Melee/DefaultStatus_Monster_Bottom_Melee.csv");
        mDefaultStatusDict_Monster_Bottom_Melee = new Dictionary<EMonsterType_Bottom_Melee, DefaultStatus_Monster_Bottom_Melee>();

        for(EMonsterType_Bottom_Melee e = EMonsterType_Bottom_Melee.None + 1; e < EMonsterType_Bottom_Melee.Count; e++)
        {
            mDefaultStatusDict_Monster_Bottom_Melee.Add(e, new DefaultStatus_Monster_Bottom_Melee(stringDict[e.ToString()]));
        }

        stringDict = CsvReader.Read(Application.streamingAssetsPath + "/Monster/Bottom/Ranged/DefaultStatus_Monster_Bottom_Ranged.csv");
        mDefaultStatusDict_Monster_Bottom_Ranged = new Dictionary<EMonsterType_Bottom_Ranged, DefaultStatus_Monster_Bottom_Ranged>();

        for(EMonsterType_Bottom_Ranged e = EMonsterType_Bottom_Ranged.None + 1; e < EMonsterType_Bottom_Ranged.Count; e++)
        {
            mDefaultStatusDict_Monster_Bottom_Ranged.Add(e, new DefaultStatus_Monster_Bottom_Ranged(stringDict[e.ToString()]));
        }

        stringDict = CsvReader.Read(Application.streamingAssetsPath + "/Monster/Bottom/Tank/DefaultStatus_Monster_Bottom_Tank.csv");
        mDefaultStatusDict_Monster_Bottom_Tank = new Dictionary<EMonsterType_Bottom_Tank, DefaultStatus_Monster_Bottom_Tank>();

        for(EMonsterType_Bottom_Tank e = EMonsterType_Bottom_Tank.None + 1; e < EMonsterType_Bottom_Tank.Count; e++)
        {
            mDefaultStatusDict_Monster_Bottom_Tank.Add(e, new DefaultStatus_Monster_Bottom_Tank(stringDict[e.ToString()]));
        }

        stringDict = CsvReader.Read(Application.streamingAssetsPath + "/Monster/Middle/Melee/DefaultStatus_Monster_Middle_Melee.csv");
        mDefaultStatusDict_Monster_Middle_Melee = new Dictionary<EMonsterType_Middle_Melee, DefaultStatus_Monster_Middle_Melee>();

        for(EMonsterType_Middle_Melee e = EMonsterType_Middle_Melee.None + 1; e < EMonsterType_Middle_Melee.Count; e++)
        {
            mDefaultStatusDict_Monster_Middle_Melee.Add(e, new DefaultStatus_Monster_Middle_Melee(stringDict[e.ToString()]));
        }

        stringDict = CsvReader.Read(Application.streamingAssetsPath + "/Monster/Middle/Summoner/DefaultStatus_Monster_Middle_Summoner.csv");
        mDefaultStatusDict_Monster_Middle_Summoner = new Dictionary<EMonsterType_Middle_Summoner, DefaultStatus_Monster_Middle_Summoner>();

        for (EMonsterType_Middle_Summoner e = EMonsterType_Middle_Summoner.None + 1; e < EMonsterType_Middle_Summoner.Count; e++)
        {
            mDefaultStatusDict_Monster_Middle_Summoner.Add(e, new DefaultStatus_Monster_Middle_Summoner(stringDict[e.ToString()]));
        }

        stringDict = CsvReader.Read(Application.streamingAssetsPath + "/Monster/Middle/Ranged/DefaultStatus_Monster_Middle_Ranged.csv");
        mDefaultStatusDict_Monster_Middle_Ranged = new Dictionary<EMonsterType_Middle_Ranged, DefaultStatus_Monster_Middle_Ranged>();

        for(EMonsterType_Middle_Ranged e = EMonsterType_Middle_Ranged.None + 1; e < EMonsterType_Middle_Ranged.Count; e++)
        {
            mDefaultStatusDict_Monster_Middle_Ranged.Add(e, new DefaultStatus_Monster_Middle_Ranged(stringDict[e.ToString()]));
        }

        stringDict = CsvReader.Read(Application.streamingAssetsPath + "/Monster/Top/SelfDestructive/DefaultStatus_Monster_Top_SelfDestructive.csv");
        mDefaultStatusDict_Monster_Top_SelfDestructive = new Dictionary<EMonsterType_Top_SelfDestructive, DefaultStatus_Monster_Top_SelfDestructive>();

        for(EMonsterType_Top_SelfDestructive e = EMonsterType_Top_SelfDestructive.None + 1; e < EMonsterType_Top_SelfDestructive.Count; e++)
        {
            mDefaultStatusDict_Monster_Top_SelfDestructive.Add(e, new DefaultStatus_Monster_Top_SelfDestructive(stringDict[e.ToString()]));
        }

        stringDict = CsvReader.Read(Application.streamingAssetsPath + "/Fairy/Player/Skill_Fairy_Player.csv");
        mSkillDict_Fairy_Player = new Dictionary<ESkillType_Fairy_Player, SkillInfo_Fairy_Player>();

        for(ESkillType_Fairy_Player e = ESkillType_Fairy_Player.None + 1; e < ESkillType_Fairy_Player.Count; e++)
        {
            mSkillDict_Fairy_Player.Add(e, new SkillInfo_Fairy_Player(stringDict[e.ToString()]));
        }

        stringDict = CsvReader.Read(Application.streamingAssetsPath + "/Giant/Player/Skill_Giant_Player.csv");
        mSkillDict_Giant_Player = new Dictionary<ESkillType_Giant_Player, SkillInfo_Giant_Player>();

        for(ESkillType_Giant_Player e = ESkillType_Giant_Player.None + 1; e < ESkillType_Giant_Player.Count; e++)
        {
            mSkillDict_Giant_Player.Add(e, new SkillInfo_Giant_Player(stringDict[e.ToString()]));
        }

        stringDict = CsvReader.Read(Application.streamingAssetsPath + "/Monster/Bottom/Normal/Skill_Monster_Bottom_Normal.csv");
        mSkillDict_Monster_Bottom_Normal = new Dictionary<ESkillType_Monster_Bottom_Normal, SkillInfo_Monster_Bottom_Normal>();

        for(ESkillType_Monster_Bottom_Normal e = ESkillType_Monster_Bottom_Normal.None + 1; e < ESkillType_Monster_Bottom_Normal.Count; e++)
        {
            mSkillDict_Monster_Bottom_Normal.Add(e, new SkillInfo_Monster_Bottom_Normal(stringDict[e.ToString()]));
        }

        stringDict = CsvReader.Read(Application.streamingAssetsPath + "/Monster/Bottom/Melee/Skill_Monster_Bottom_Melee.csv");
        mSkillDict_Monster_Bottom_Melee = new Dictionary<ESkillType_Monster_Bottom_Melee, SkillInfo_Monster_Bottom_Melee>();

        for(ESkillType_Monster_Bottom_Melee e = ESkillType_Monster_Bottom_Melee.None + 1; e < ESkillType_Monster_Bottom_Melee.Count; e++)
        {
            mSkillDict_Monster_Bottom_Melee.Add(e, new SkillInfo_Monster_Bottom_Melee(stringDict[e.ToString()]));
        }

        stringDict = CsvReader.Read(Application.streamingAssetsPath + "/Monster/Bottom/Ranged/Skill_Monster_Bottom_Ranged.csv");
        mSkillDict_Monster_Bottom_Ranged = new Dictionary<ESkillType_Monster_Bottom_Ranged, SkillInfo_Monster_Bottom_Ranged>();
        
        for(ESkillType_Monster_Bottom_Ranged e = ESkillType_Monster_Bottom_Ranged.None + 1; e < ESkillType_Monster_Bottom_Ranged.Count; e++)
        {
            mSkillDict_Monster_Bottom_Ranged.Add(e, new SkillInfo_Monster_Bottom_Ranged(stringDict[e.ToString()]));
        }

        stringDict = CsvReader.Read(Application.streamingAssetsPath + "/Monster/Bottom/Tank/Skill_Monster_Bottom_Tank.csv");
        mSkillDict_Monster_Bottom_Tank = new Dictionary<ESkillType_Monster_Bottom_Tank, SkillInfo_Monster_Bottom_Tank>();

        for(ESkillType_Monster_Bottom_Tank e = ESkillType_Monster_Bottom_Tank.None + 1; e < ESkillType_Monster_Bottom_Tank.Count; e++)
        {
            mSkillDict_Monster_Bottom_Tank.Add(e, new SkillInfo_Monster_Bottom_Tank(stringDict[e.ToString()]));
        }

        stringDict = CsvReader.Read(Application.streamingAssetsPath + "/Monster/Middle/Melee/Skill_Monster_Middle_Melee.csv");
        mSkillDict_Monster_Middle_Melee = new Dictionary<ESkillType_Monster_Middle_Melee, SkillInfo_Monster_Middle_Melee>();

        for(ESkillType_Monster_Middle_Melee e = ESkillType_Monster_Middle_Melee.None + 1; e < ESkillType_Monster_Middle_Melee.Count; e++)
        {
            mSkillDict_Monster_Middle_Melee.Add(e, new SkillInfo_Monster_Middle_Melee(stringDict[e.ToString()]));
        }

        stringDict = CsvReader.Read(Application.streamingAssetsPath + "/Monster/Middle/Summoner/Skill_Monster_Middle_Summoner.csv");
        mSkillDict_Monster_Middle_Summoner = new Dictionary<ESkillType_Monster_Middle_Summoner, SkillInfo_Monster_Middle_Summoner>();

        for (ESkillType_Monster_Middle_Summoner e = ESkillType_Monster_Middle_Summoner.None + 1; e < ESkillType_Monster_Middle_Summoner.Count; e++)
        {
            mSkillDict_Monster_Middle_Summoner.Add(e, new SkillInfo_Monster_Middle_Summoner(stringDict[e.ToString()]));
        }

        stringDict = CsvReader.Read(Application.streamingAssetsPath + "/Monster/Middle/Ranged/Skill_Monster_Middle_Ranged.csv");
        mSkillDict_Monster_Middle_Ranged = new Dictionary<ESkillType_Monster_Middle_Ranged, SkillInfo_Monster_Middle_Ranged>();

        for(ESkillType_Monster_Middle_Ranged e = ESkillType_Monster_Middle_Ranged.None +1; e < ESkillType_Monster_Middle_Ranged.Count; e++)
        {
            mSkillDict_Monster_Middle_Ranged.Add(e, new SkillInfo_Monster_Middle_Ranged(stringDict[e.ToString()]));
        }

        stringDict = CsvReader.Read(Application.streamingAssetsPath + "/Monster/Top/SelfDestructive/Skill_Monster_Top_SelfDestructive.csv");
        mSkillDict_Monster_Top_SelfDestructive = new Dictionary<ESkillType_Monster_Top_SelfDestructive, SkillInfo_Monster_Top_SelfDestructive>();

        for(ESkillType_Monster_Top_SelfDestructive e = ESkillType_Monster_Top_SelfDestructive.None + 1; e < ESkillType_Monster_Top_SelfDestructive.Count; e++)
        {
            mSkillDict_Monster_Top_SelfDestructive.Add(e, new SkillInfo_Monster_Top_SelfDestructive(stringDict[e.ToString()]));
        }

        stringDict = CsvReader.Read(Application.streamingAssetsPath + "/Buff.csv");
        mBuffDict = new Dictionary<EBuffType, BuffInfo>();

        for(EBuffType e = EBuffType.None + 1; e < EBuffType.Count; e++)
        {
            mBuffDict.Add(e, new BuffInfo(stringDict[e.ToString()]));
        }

        stringDict = CsvReader.Read(Application.streamingAssetsPath + "/Debuff.csv");
        mDebuffDict = new Dictionary<EDebuffType, DebuffInfo>();

        for(EDebuffType e = EDebuffType.None + 1; e < EDebuffType.Count; e++)
        {
            mDebuffDict.Add(e, new DebuffInfo(stringDict[e.ToString()]));
        }

        stringDict = CsvReader.Read(Application.streamingAssetsPath + "/Projectile.csv");
        mProjectileDict = new Dictionary<EProjectileType, ProjectileInfo>();

        for(EProjectileType e = EProjectileType.None + 1; e < EProjectileType.Count; e++)
        {
            mProjectileDict.Add(e, new ProjectileInfo(stringDict[e.ToString()]));
        }

        mScriptDict = CsvReader.Read(Application.streamingAssetsPath + "/Script.csv");
    }

    /// <summary>
    /// 괴수의 기본 능력치를 저장한 객체를 반환하는 메소드
    /// </summary>
    /// <param name="e"></param>
    /// <returns></returns>
    public static DefaultStatus_Giant_Player GetDefaultStatus(EGiantType_Player e)
    {
        return mDefaultStatusDict_Giant_Player[e];
    }

    /// <summary>
    /// 정령의 기본 능력치를 저장한 객체를 반환하는 메소드
    /// </summary>
    /// <param name="e"></param>
    /// <returns></returns>
    public static DefaultStatus_Fairy_Player GetDefaultStatus(EFairyType_Player e)
    {
        return mDefaultStatusDict_Fairy_Player[e];
    }

    /// <summary>
    /// 바닥 일반 몬스터의 기본 능력치를 저장한 객체를 반환하는 메소드
    /// </summary>
    /// <param name="e"></param>
    /// <returns></returns>
    public static DefaultStatus_Monster_Bottom_Normal GetDefaultStatus(EMonsterType_Bottom_Normal e)
    {
        return mDefaultStatusDict_Monster_Bottom_Normal[e];
    }
    /// <summary>
    /// 바닥 근접 몬스터의 기본 속성을 저장한 객체를 반환하는 메소드
    /// </summary>
    /// <param name="e"></param>
    /// <returns></returns>
    public static DefaultStatus_Monster_Bottom_Melee GetDefaultStatus(EMonsterType_Bottom_Melee e)
    {
        return mDefaultStatusDict_Monster_Bottom_Melee[e];
    }

    /// <summary>
    /// 바닥 원거리 몬스터의 기본 속성을 저장한 객체를 반환하는 메소드
    /// </summary>
    /// <param name="e"></param>
    /// <returns></returns>
    public static DefaultStatus_Monster_Bottom_Ranged GetDefaultStatus(EMonsterType_Bottom_Ranged e)
    {
        return mDefaultStatusDict_Monster_Bottom_Ranged[e];
    }

    public static DefaultStatus_Monster_Bottom_Tank GetDefaultStatus(EMonsterType_Bottom_Tank e)
    {
        return mDefaultStatusDict_Monster_Bottom_Tank[e];
    }

    public static DefaultStatus_Monster_Middle_Melee GetDefaultStatus(EMonsterType_Middle_Melee e)
    {
        return mDefaultStatusDict_Monster_Middle_Melee[e];
    }

    public static DefaultStatus_Monster_Middle_Summoner GetDefaultStatus(EMonsterType_Middle_Summoner e)
    {
        return mDefaultStatusDict_Monster_Middle_Summoner[e];
    }

    public static DefaultStatus_Monster_Middle_Ranged GetDefaultStatus(EMonsterType_Middle_Ranged e)
    {
        return mDefaultStatusDict_Monster_Middle_Ranged[e];
    }

    public static DefaultStatus_Monster_Top_SelfDestructive GetDefaultStatus(EMonsterType_Top_SelfDestructive e)
    {
        return mDefaultStatusDict_Monster_Top_SelfDestructive[e];
    }

    /// <summary>
    /// 플레이어 정령 스킬 정보를 저장한 객체를 반환하는 메소드
    /// </summary>
    /// <param name="e"></param>
    /// <returns></returns>
    public static SkillInfo_Fairy_Player GetSkillInfo(ESkillType_Fairy_Player e)
    {
        return mSkillDict_Fairy_Player[e];
    }

    /// <summary>
    /// 플레이어 괴수 스킬 정보를 저장한 객체를 반환하는 메소드
    /// </summary>
    /// <param name="e"></param>
    /// <returns></returns>
    public static SkillInfo_Giant_Player GetSkillInfo(ESkillType_Giant_Player e)
    {
        return mSkillDict_Giant_Player[e];
    }

    /// <summary>
    /// 바닥 일반 몬스터 스킬 정보를 저장한 객체를 반환하는 메소드
    /// </summary>
    /// <param name="e"></param>
    /// <returns></returns>
    public static SkillInfo_Monster_Bottom_Normal GetSkillInfo(ESkillType_Monster_Bottom_Normal e)
    {
        return mSkillDict_Monster_Bottom_Normal[e];
    }
    /// <summary>
    /// 바닥 근접 몬스터 스킬 정보를 저장한 객체를 반환하는 메소드
    /// </summary>
    /// <param name="e"></param>
    /// <returns></returns>
    public static SkillInfo_Monster_Bottom_Melee GetSkillInfo(ESkillType_Monster_Bottom_Melee e)
    {
        return mSkillDict_Monster_Bottom_Melee[e];
    }
    /// <summary>
    /// 바닥 원거리 몬스터 스킬 정보를 저장한 객체를 반환하는 메소드
    /// </summary>
    /// <param name="e"></param>
    /// <returns></returns>
    public static SkillInfo_Monster_Bottom_Ranged GetSkillInfo(ESkillType_Monster_Bottom_Ranged e)
    {
        return mSkillDict_Monster_Bottom_Ranged[e];
    }

    public static SkillInfo_Monster_Bottom_Tank GetSkillInfo(ESkillType_Monster_Bottom_Tank e)
    {
        return mSkillDict_Monster_Bottom_Tank[e];
    }

    public static SkillInfo_Monster_Middle_Melee GetSkillInfo(ESkillType_Monster_Middle_Melee e)
    {
        return mSkillDict_Monster_Middle_Melee[e];
    }

    public static SkillInfo_Monster_Middle_Summoner GetSkillInfo(ESkillType_Monster_Middle_Summoner e)
    {
        return mSkillDict_Monster_Middle_Summoner[e];
    }

    public static SkillInfo_Monster_Middle_Ranged GetSkillInfo(ESkillType_Monster_Middle_Ranged e)
    {
        return mSkillDict_Monster_Middle_Ranged[e];
    }

    public static SkillInfo_Monster_Top_SelfDestructive GetSkillInfo(ESkillType_Monster_Top_SelfDestructive e)
    {
        return mSkillDict_Monster_Top_SelfDestructive[e];
    }

    /// <summary>
    /// 버프 정보를 저장한 객체를 반환하는 메소드
    /// </summary>
    /// <param name="e"></param>
    /// <returns></returns>
    public static BuffInfo GetBuffInfo(EBuffType e)
    {
        return mBuffDict[e];
    }

    /// <summary>
    /// 디버프 정보를 저장한 객체를 반환하는 메소드
    /// </summary>
    /// <param name="e"></param>
    /// <returns></returns>
    public static DebuffInfo GetDebuffInfo(EDebuffType e)
    {
        return mDebuffDict[e];
    }

    /// <summary>
    /// 투사체 정보를 저장한 객체를 반환하는 메소드
    /// </summary>
    /// <param name="e"></param>
    /// <returns></returns>
    public static ProjectileInfo GetProjectileInfo(EProjectileType e)
    {
        return mProjectileDict[e];
    }


    /// <summary>
    /// Script.csv에서 읽어온 Script 중 tag에 위치한 script를 반환하는 메소드
    /// </summary>
    /// <param name="tag"></param>
    /// <returns></returns>
    public static string GetScript(string tag)
    {
        return mScriptDict[tag][StateManager.Language.State.ToString()];
    }
    /// <summary>
    /// Script.csv에서 읽어온 Script 중 tag에 위치한 script에 parameters를 포매팅해서 반환하는 메소드
    /// </summary>
    /// <param name="tag"></param>
    /// <param name="parameters"></param>
    /// <returns></returns>
    public static string GetScript(string tag, params object[] parameters)
    {
        return string.Format(mScriptDict[tag][StateManager.Language.State.ToString()], parameters);
    }
}
