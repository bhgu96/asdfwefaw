﻿using System;
using UnityEngine;
using System.Collections.Generic;
using Random = UnityEngine.Random;

public class Agent<EState> : AInput
    where EState : Enum
{
    private Dictionary<EState, State<EState>> mStateDict;
    private State<EState> mState;
    /// <summary>
    /// 상태 설명 및 반환
    /// </summary>
    public EState State
    {
        get { return mState.Type; }
        set
        {
            State<EState> prevState = mState;
            mState = mStateDict[value];
            if (!mComparer.Equals(prevState.Type, value))
            {
                prevState.End();
            }
            mState.Start();
        }
    }

    private EqualityComparer<EState> mComparer;

    private float mRemainTime;
    public const float TIME_TERM = 0.1f;

    public Agent(int countOfSkill)
        : base(countOfSkill)
    {
        mStateDict = new Dictionary<EState, State<EState>>();
        mComparer = EqualityComparer<EState>.Default;
        mRemainTime = Random.Range(0.0f, TIME_TERM);
    }

    /// <summary>
    /// 상태를 설정
    /// </summary>
    /// <param name="states"></param>
    public void SetStates(params State<EState>[] states)
    {
        mState = states[0];
        for(int i=0; i< states.Length; i++)
        {
            mStateDict.Add(states[i].Type, states[i]);
        }
    }

    /// <summary>
    /// 시간이 되면 센서를 실행하고 FSM 을 실행하는 메소드
    /// </summary>
    public override void Check()
    {
        if (mRemainTime > 0.0f)
        {
            mRemainTime -= Time.deltaTime;
            SetMoveHorizontal(MoveHorizontal);
            SetMoveVertical(MoveVertical);
            for(int i=0; i<Skill.Length; i++)
            {
                SetSkill(Skill[i], i);
            }
        }
        else
        {
            mRemainTime += TIME_TERM;
            mState.Update();
        }
    }
}
