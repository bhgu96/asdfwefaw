﻿using UnityEngine;
using System.Collections.Generic;
using Object;

public class AudioHandler<EAudioFlags> : Handler<EAudioFlags, Audio<EAudioFlags>>
    where EAudioFlags : System.Enum
{
    public void Play(EAudioFlags audioFlag, float time = 0.0f)
    {
        Get(audioFlag).Play(time);
    }
    public void Stop(EAudioFlags audioFlag)
    {
        Get(audioFlag).Stop();
    }
    public void StopAll()
    {
        Dictionary<EAudioFlags, Audio<EAudioFlags>>.Enumerator enumerator =  GetEnumerator();
        while (enumerator.MoveNext())
        {
            enumerator.Current.Value.Stop();
        }
    }
    public void Pause(EAudioFlags audioFlag)
    {
        Get(audioFlag).Pause();
    }
    public void UnPause(EAudioFlags audioFlag)
    {
        Get(audioFlag).UnPause();
    }

    public void AddOnPlayEvent(EAudioFlags audioFlag, Audio<EAudioFlags>.Action action)
    {
        Get(audioFlag).OnPlay += action;
    }
    public void RemoveOnPlayEvent(EAudioFlags audioFlag, Audio<EAudioFlags>.Action action)
    {
        Get(audioFlag).OnPlay -= action;
    }
    public void AddOnPauseEvent(EAudioFlags audioFlag, Audio<EAudioFlags>.Action action)
    {
        Get(audioFlag).OnPause += action;
    }
    public void RemoveOnPauseEvent(EAudioFlags audioFlag, Audio<EAudioFlags>.Action action)
    {
        Get(audioFlag).OnPause -= action;
    }
    public void AddOnStopEvent(EAudioFlags audioFlag, Audio<EAudioFlags>.Action action)
    {
        Get(audioFlag).OnStop += action;
    }
    public void RemoveOnStopEvent(EAudioFlags audioFlag, Audio<EAudioFlags>.Action action)
    {
        Get(audioFlag).OnStop -= action;
    }
}