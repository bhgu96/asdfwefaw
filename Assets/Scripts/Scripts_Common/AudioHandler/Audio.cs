﻿using UnityEngine;
using Object;

public class Audio<EAudioFlags> : MonoBehaviour, IHandleable<EAudioFlags>
    where EAudioFlags : System.Enum
{
    public delegate void Action();

    public EAudioFlags Type { get { return mType; } }
    [SerializeField]
    private EAudioFlags mType;

    public bool IsPlaying { get { return mAudioSource.isPlaying; } }
    public bool IsPaused { get; private set; } = false;
    public bool IsLoop { get { return mAudioSource.loop; } set { mAudioSource.loop = value; } }

    /// <summary>
    /// 재생중일때 호출되는 이벤트 메소드
    /// </summary>
    public event Action OnPlay;
    /// <summary>
    /// 일시정지중일때 호출되는 이벤트 메소드
    /// </summary>
    public event Action OnPause;
    /// <summary>
    /// 정지시 호출되는 콜백 메소드
    /// </summary>
    public event Action OnStop;

    private AudioSource mAudioSource;

    private float mStandardVolume;

    public void Play(float time = 0.0f)
    {
        /*
         * 추후, 환경설정에 설정된 오디오 음량을 가져오는 기능 추가예정
         * mAudioSource.volume = mStandardVolume * 볼륨 계산 식
         */
        gameObject.SetActive(true);
        IsPaused = false;
        mAudioSource.time = time;
        mAudioSource.Play();
    }

    public void Pause()
    {
        if (mAudioSource.isPlaying)
        {
            IsPaused = true;
            mAudioSource.Pause();
        }
    }

    public void UnPause()
    {
        IsPaused = false;
        mAudioSource.UnPause();
    }

    public void Stop()
    {
        mAudioSource.Stop();
        gameObject.SetActive(false);
        IsPaused = false;
        OnStop?.Invoke();
    }

    private void Awake()
    {
        mAudioSource = GetComponent<AudioSource>();
        mStandardVolume = mAudioSource.volume;
        gameObject.SetActive(false);
    }

    private void Update()
    {
        // 오디오가 재생 중이라면
        if (mAudioSource.isPlaying)
        {
            OnPlay?.Invoke();
        } // 오디오가 일시정지 중이라면
        else if (IsPaused)
        {
            OnPause?.Invoke();
        } // 오디오가 정지되었다면
        else
        {
            gameObject.SetActive(false);
            OnStop?.Invoke();
        }
    }
}
