﻿using System;
using UnityEngine;
using System.Collections.Generic;

/// <summary>
/// 상태 기반으로 작동하는 클래스
/// </summary>
/// <typeparam name="E"></typeparam>
public class StateBase<E> : MonoBehaviour where E : Enum
{
    /// <summary>
    /// 현재 State를 설정하거나 반환하는 프로퍼티
    /// </summary>
    public E State
    {
        get
        {
            return mState.Type;
        }
        set
        {
            State<E> prevState = mState;
            mState = mStateDict[value];
            if (!mComparer.Equals(prevState.Type, value))
            {
                prevState.End();
            }
            mState.Start();
        }
    }

    private State<E> mState;

    private Dictionary<E, State<E>> mStateDict;

    private EqualityComparer<E> mComparer;

    /// <summary>
    /// 일련의 상태 객체들을 설정하는 메소드
    /// </summary>
    /// <param name="states"></param>
    public void SetStates(params State<E>[] states)
    {
        mState = states[0];
        for (int i = 0; i < states.Length; i++)
        {
            mStateDict.Add(states[i].Type, states[i]);
        }
    }

    protected virtual void Awake()
    {
        mStateDict = new Dictionary<E, State<E>>();
        mComparer = EqualityComparer<E>.Default;
    }

    protected virtual void Update()
    {
        mState?.Update();
    }

    protected virtual void FixedUpdate()
    {
        mState?.FixedUpdate();
    }
}
