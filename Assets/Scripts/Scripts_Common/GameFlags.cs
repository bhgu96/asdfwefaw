﻿

public enum ELayerFlags
{
    Default = 0,
    TransparentFX = 1, 
    IgnoreRaycast = 2, 
    Water = 4, 
    UI = 5, 
    Terrain = 8, 
    TerrainTrigger = 9, 
    CameraBoundary = 10, 
    CameraBoundaryTrigger = 11, 
    Fairy = 12, 
    FairyAttack = 13, 
    Giant = 14, 
    GiantAttack = 15, 
    Monster = 16, 
    MonsterAttack = 17, 
    Projectile = 19, 
    WallLight = 31
}

public enum EOuterGameFlags
{
    None = -1, 

    Intro, 
    Title, 
    Sanctuary, 
    Abyss, 

    Count
}

public enum EInnerGameFlags
{
    None = -1, 

    Normal, 

    Count
}

public enum ELanguageType
{
    None = -1, 

    Korean, 
    English, 

    Count
}

public enum EFairyType_Player
{
    None = -1, 

    Type_00, 

    Count, 
}

public enum EGiantType_Player
{
    None = -1, 

    Type_00, 

    Count
}
/// <summary>
/// 모든 몬스터 타입
/// </summary>
public enum EMonsterType
{
    None = -1, 
    // 강철아귀 거북
    IronMouthTurtle = 0, 
    // 핏빛이리
    Bloodwolf = 1, 
    // 용암요람 가재 전투병
    LavacradleLobsterSoldier = 2,
    // 제국 기계 순찰자
    EmpireMachineRanger = 3, 
    // 새끼 실바리자드
    BabySilvalizard = 4, 
    // 새끼 젤루리자드
    BabyJelulizard = 5, 
    // 눈보라 창병
    BlizzardSpearman = 6, 
    // 천둥염소 부족 파괴자
    ThundergoatTribeDestroyer = 7, 
    // 짓무른 복수자
    CrushedAvenger = 8, 
    // 타락한 아스트라 화염 마법사
    CorruptedAstranFlameMage = 9, 
    // 천둥염소 부족 주술사
    ThundergoatTribeShaman = 10,
    // 짓무른 지하 요술사
    CrushedUndergroundWarlock = 11,
    // 플라스크 속의 망령
    WraithInTheFlask = 12, 
    // 낮은 숲 습격자
    LowForestAttacker = 13, 
    // 버려진 요툰헤임 기사
    AbandonedJotunheimKnight = 14, 
    // 아스트라 방패병
    AstranShieldman = 15, 
    // 썩은 밑동 나무 정령
    RottenBaseTreeSpirit = 16, 
    // 암흑 수호병
    DarkGuard = 17,
    // 제국 기계 돌격수
    EmpireMachineAssault = 18,
    // 서리이빨 늑대
    FrosteethWolf = 19,
    // 실패한 키메라 교수
    FailedChimeraProfessor = 20,
    // 흉포한 원시림 고릴라
    ViciousPrimordialGorilla = 21,
    // 얼음발톱 곰
    IceclawBear = 22, 
    // 홍염 거인
    RedFlameGiant = 23, 
    // 밤그늘 까마귀
    NightshadeCrow = 24,
    // 제국 기계 가고일
    EmpireMachineGargoyle = 25,
    // 지옥 마귀 약탈자
    FelDevilMarauder = 26,
    // 오염된 마법 슬라임
    ContaminatedMagicSlime = 27, 

    Count
}

/// <summary>
/// 바닥에 위치하는 일반 몬스터 타입
/// </summary>
public enum EMonsterType_Bottom_Normal
{
    None = -1, 
    // 강철아귀 거북
    IronMouthTurtle = 0, 
    // 핏빛이리
    Bloodwolf = 1, 
    // 용암요람 가재 전투병
    LavacradleLobsterSoldier = 2, 
    // 플라스크 속의 망령
    WraithInTheFlask = 3, 
    // 낮은 숲 습격자
    LowForestAttacker = 4, 
    // 버려진 요툰헤임 기사
    AbandonedJotunheimKnight = 5, 
    // 실패한 키메라 교수
    FailedChimeraProfessor = 6, 
    // 흉포한 원시림 고릴라
    ViciousPrimordialGorilla = 7, 
    // 얼음발톱 곰
    IceclawBear = 8, 
    // 홍염 거인
    RedFlameGiant = 9, 

    Count
}
/// <summary>
/// 바닥에 위치하는 근접 몬스터 타입
/// </summary>
public enum EMonsterType_Bottom_Melee
{
    None = -1, 

    // 제국 기계 순찰자
    EmpireMachineRanger = 0, 
    // 새끼 실바리자드
    BabySilvalizard = 1, 
    // 새끼 젤루리자드
    BabyJelulizard = 2, 
    // 눈보라 창병
    BlizzardSpearman = 3,
    // 천둥염소 부족 파괴자
    ThundergoatTribeDestroyer = 4, 
    // 짓무른 복수자
    CrushedAvenger = 5, 
    // 제국 기계 돌격수
    EmpireMachineAssault = 6, 
    // 서리이빨 늑대
    FrosteethWolf = 7, 

    Count
}

/// <summary>
/// 바닥에 위치하는 원거리 몬스터 타입
/// </summary>
public enum EMonsterType_Bottom_Ranged
{
    None = -1, 
    // 타락한 아스트라 화염 마법사
    CorruptedAstranFlameMage = 0, 
    // 천둥염소 부족 주술사
    ThundergoatTribeShaman = 1, 
    // 짓무른 지하 요술사
    CrushedUndergroundWarlock = 2, 

    Count
}
/// <summary>
/// 바닥에 위치하는 탱커 몬스터 타입
/// </summary>
public enum EMonsterType_Bottom_Tank
{
    None = -1, 

    // 아스트라 방패병
    AstranShieldman = 0, 
    // 썩은 밑동 나무 정령
    RottenBaseTreeSpirit = 1, 
    // 암흑 수호병
    DarkGuard = 2, 

    Count
}

public enum EMonsterType_Middle_Melee
{
    None = -1, 

    // 밤그늘 까마귀
    NightshadeCrow = 0, 

    Count
}

public enum EMonsterType_Middle_Summoner
{
    None = -1, 

    // 제국 기계 가고일
    EmpireMachineGargoyle = 0, 

    Count
}

public enum EMonsterType_Middle_Ranged
{
    None = -1, 

    // 지옥 마귀 약탈자
    FelDevilMarauder = 0, 

    Count
}

public enum EMonsterType_Top_SelfDestructive
{
    None = -1, 

    // 오염된 마법 슬라임
    ContaminatedMagicSlime = 0, 

    Count
}

public enum ECombatType
{
    None = -1, 

    Normal = 0, 
    SelfDestructive = 1, 
    Summoner = 2, 
    Melee = 3, 
    Ranged = 4, 
    Tank = 5, 
    Support = 6, 
    Ultimate = 7, 

    Count
}


/// <summary>
/// 위치 타입
/// 
/// </summary>
public enum EPositionType
{
    None = -1, 

    Bottom, 
    Middle, 
    Top, 

    Count
}

/// <summary>
/// 액터 크기 타입
/// </summary>
public enum ESizeType
{
    None = -1,

    Small, 
    Medium, 
    Large, 
    ExtraLarge, 

    Count
}
/// <summary>
/// 몬스터 등급
/// </summary>
public enum EMonsterRank
{
    None = -1, 

    Grunt, 
    Champion, 
    Elite, 
    Boss, 

    Count
}

public enum EBuffType
{
    None = -1, 

    Count
}

public enum EDebuffType
{
    None = -1, 

    // 덤불에 의한 둔화
    Slowing_Bush, 
    // 덤불에 의한 구속
    Restrain_Bush, 

    Count
}

public enum EItemCategory
{
    None = -1, 

    Material, 
    Valueable, 
    Soulite,
    Skill,

    Count
}

public enum EItemType
{
    None = -1, 

    TestMaterial_0, 
    TestSoulLight_0, 
    TestValueable_0, 

    TestMaterial_1, 
    TestSoulLight_1, 
    TestValueable_1, 

    Count
}

public enum EElementalType
{
    None = -1, 
    // 물리 속성
    Kinematic, 
    // 신비 속성
    Arcane, 
    // 화 속성
    Fire, 
    // 수 속성
    Water, 
    // 풍 속성
    Wind, 
    // 지 속성
    Earth, 
    // 광 속성
    Light, 
    // 암 속성
    Darkness, 

    Count
}

public enum EHorizontalDirection
{
    Left = -1, None = 0, Right = 1
}

public enum EVerticalDirection
{
    Down = -1, None = 0, Up = 1
}

public enum EDirectionFlags
{
    None = -1,

    Horizontal,
    Vertical,

    Count
}