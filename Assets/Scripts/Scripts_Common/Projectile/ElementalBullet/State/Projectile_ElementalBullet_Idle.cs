﻿using UnityEngine;

public class Projectile_ElementalBullet_Idle : State<EStateType_Projectile_ElementalBullet>
{
    private Projectile_ElementalBullet mProjectile;

    public Projectile_ElementalBullet_Idle(Projectile_ElementalBullet projectile) : base(EStateType_Projectile_ElementalBullet.Idle)
    {
        mProjectile = projectile;
    }

    public override void Start()
    {
        mProjectile.Animator.SetInteger("state", (int)Type);
        mProjectile.gameObject.SetActive(false);
    }
}
