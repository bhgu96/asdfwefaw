﻿using UnityEngine;

public class Projectile_ElementalBullet_Do : State<EStateType_Projectile_ElementalBullet>
{
    private Projectile_ElementalBullet mProjectile;

    public Projectile_ElementalBullet_Do(Projectile_ElementalBullet projectile) : base(EStateType_Projectile_ElementalBullet.Do)
    {
        mProjectile = projectile;
    }

    public override void Start()
    {
        mProjectile.Animator.SetInteger("state", (int)Type);

        Vector2 direction = mProjectile.Direction;

        if (mProjectile.IsBasedOnGiant)
        {
            if (!mProjectile.IsTest)
            {
                mProjectile.Velocity = new Vector2(direction.x * mProjectile.Speed + Giant_Player.Main.Status.MoveSpeed, direction.y * mProjectile.Speed);
            }
            else
            {
                mProjectile.Velocity = new Vector2(direction.x * mProjectile.Speed + _AGiant.Main.MoveSpeed, direction.y * mProjectile.Speed);
            }
        }
        else
        {
            mProjectile.Velocity = direction * mProjectile.Speed;
        }
    }

    public override void FixedUpdate()
    {
        if(mProjectile.Attack(ETriggerType_Projectile_ElementalBullet.Detect))
        {
            mProjectile.Deactivate();
            return;
        }
    }
}
