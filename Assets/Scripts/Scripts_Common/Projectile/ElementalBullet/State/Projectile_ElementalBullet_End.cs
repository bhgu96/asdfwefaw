﻿using UnityEngine;

public class Projectile_ElementalBullet_End : State<EStateType_Projectile_ElementalBullet>
{
    private Projectile_ElementalBullet mProjectile;

    public Projectile_ElementalBullet_End(Projectile_ElementalBullet projectile) : base(EStateType_Projectile_ElementalBullet.End)
    {
        mProjectile = projectile;
    }

    public override void Start()
    {
        mProjectile.Animator.SetInteger("state", (int)Type);
        mProjectile.Velocity = Vector2.zero;
    }
}
