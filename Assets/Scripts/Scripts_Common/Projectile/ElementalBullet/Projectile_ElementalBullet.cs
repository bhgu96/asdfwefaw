﻿using UnityEngine;

public enum EStateType_Projectile_ElementalBullet
{
    None = -1, 

    Idle = 0, 
    Do = 1, 
    End = 2, 

    Count
}

public enum ETriggerType_Projectile_ElementalBullet
{
    None = -1, 

    Detect = 0, 

    Count
}

public enum EAudioType_Projectile_ElementalBullet
{
    None = -1, 

    Count
}


[RequireComponent(typeof(TriggerHandler_Projectile_ElementalBullet), typeof(AudioHandler_Projectile_ElementalBullet), typeof(AttackHandler_Projectile_ElementalBullet))]
public class Projectile_ElementalBullet : AProjectile<EStateType_Projectile_ElementalBullet>, IDamagable
{
    public Damage Damage { get; private set; }
    public Heal Heal { get; private set; }

    [SerializeField]
    private EProjectileType mType;
    /// <summary>
    /// 투사체 타입
    /// </summary>
    public override EProjectileType Type
    {
        get { return mType; }
    }

    public TriggerHandler_Projectile_ElementalBullet TriggerHandler { get; private set; }
    public AudioHandler_Projectile_ElementalBullet AudioHandler { get; private set; }
    public AttackHandler_Projectile_ElementalBullet AttackHandler { get; private set; }

    [SerializeField]
    private EElementalType mElementalType;
    public EElementalType ElementalType
    {
        get { return mElementalType; }
    }


    public int Hp { get; set; }

    public int ElementalAffinity { get; set; }
    public float Offense { get; set; }
    public float Speed { get; set; }
    public float Force { get; set; }
    public int Level { get; set; }
    public EDamageType DamageType { get; set; }
    public float CriticalChance { get; set; }
    public float CriticalMag { get; set; }

    public bool IsTest { get; set; }
    public int TestOffense { get; set; }

    public bool IsBasedOnGiant { get; set; }

    public bool Attack(ETriggerType_Projectile_ElementalBullet type)
    {
        Collider2D[] colliders = TriggerHandler.GetColliders(type);

        for(int i=0; i<colliders.Length && colliders[i] != null; i++)
        {
            IDamagable damagable = colliders[i].GetComponentInParent<IDamagable>();

            if(damagable != null)
            {
                if (!IsTest)
                {
                    float random = Random.Range(0.0f, 1.0f);
                    if (random <= CriticalChance)
                    {
                        damagable.Damage.SetDamage(false, true, true
                            , EEffectType.None, DamageType, ElementalType
                            , Level, ElementalAffinity, Offense * CriticalMag, Force
                            , AttackHandler.Get(type).transform.position, this);
                    }
                    else
                    {
                        damagable.Damage.SetDamage(false, true, false
                            , EEffectType.None, DamageType, ElementalType
                            , Level, ElementalAffinity, Offense, Force
                            , AttackHandler.Get(type).transform.position, this);
                    }
                }
                else
                {
                    damagable.Damage.Set(TestOffense, Force, 
                        EDamageType.Monster, ESrcType.Monster, EEffectType.None,
                        AttackHandler.Get(type).transform.position);
                }

                damagable.ActivateDamage();
                return true;
            }
        }

        return false;
    }

    public void ActivateDamage()
    {
        if (Damage.SrcType == ESrcType.Fairy && IsActivated)
        {
            int damage = Damage.Get();
            Hp -= damage;

            Vector2 direction = Direction;
            Speed *= 0.8f;
            Velocity = new Vector2(direction.x * Speed + _AGiant.Main.MoveSpeed, direction.y * Speed);

            Vector2 dstPosition = transform.position;

            // 데미지의 정보를 표시하는 이펙트 객체를 가져옴
            Effect_Damage effect = EffectPool.Get(EEffectType.Damage) as Effect_Damage;

            if (effect != null && Hp > 0)
            {
                effect.Damage = (int)Hp;
                effect.DamageType = EDamageType.Critical;
                effect.transform.position = new Vector3(dstPosition.x, dstPosition.y + 1.0f, 0.0f);
                effect.Activate();
            }
            if (Hp <= 0)
            {
                Deactivate();
            }
        }
    }

    public void ActivateHeal()
    {

    }

    protected override void OnActivate()
    {
        base.OnActivate();
        State = EStateType_Projectile_ElementalBullet.Do;
    }

    protected override void OnDeactivate()
    {
        base.OnDeactivate();
        State = EStateType_Projectile_ElementalBullet.End;
    }

    protected override void Awake()
    {
        base.Awake();

        Damage = new Damage();

        TriggerHandler = GetComponent<TriggerHandler_Projectile_ElementalBullet>();
        AudioHandler = GetComponent<AudioHandler_Projectile_ElementalBullet>();
        AttackHandler = GetComponent<AttackHandler_Projectile_ElementalBullet>();

        SetStates(new Projectile_ElementalBullet_Idle(this)
            , new Projectile_ElementalBullet_Do(this)
            , new Projectile_ElementalBullet_End(this));
    }
}
