﻿using UnityEngine;
using System.Collections;

public enum EStateType_Projectile_ElementalArrow
{
    Idle, 
    Active, 
    Deactivate, 
}

public enum ETriggerType_Projectile_ElementalArrow
{
    None = -1, 

    Hit, 
    Terrain, 

    Count
}

public enum EAudioType_Projectile_ElementalArrow
{
    None = -1, 


    Count
}

[RequireComponent(typeof(TriggerHandler_Projectile_ElementalArrow), typeof(AudioHandler_Projectile_ElementalArrow), typeof(AttackHandler_Projectile_ElementalArrow))]
public class Projectile_ElementalArrow : AProjectile<EStateType_Projectile_ElementalArrow>
{
    [SerializeField]
    private EProjectileType mType;
    public override EProjectileType Type { get { return mType; } }

    public AudioHandler_Projectile_ElementalArrow AudioHandler { get; private set; }
    public TriggerHandler_Projectile_ElementalArrow TriggerHandler { get; private set; }
    public AttackHandler_Projectile_ElementalArrow AttackHandler { get; private set; }

    [SerializeField]
    private EElementalType mElementalType;
    /// <summary>
    /// 속성 타입
    /// </summary>
    public EElementalType ElementalType { get { return mElementalType; } }

    /// <summary>
    /// 데미지 타입
    /// </summary>
    public EDamageType DamageType { get; set; }

    /// <summary>
    /// 투사체 속도
    /// </summary>
    public float Speed { get; set; }

    /// <summary>
    /// 투사체 발사 거리
    /// </summary>
    public float Distance { get; set; }

    /// <summary>
    /// 공격력
    /// </summary>
    public float Offense { get; set; }
    /// <summary>
    /// 속성 친화도
    /// </summary>
    public int ElementalAffinity { get; set; }
    /// <summary>
    /// 레벨
    /// </summary>
    public int Level { get; set; }
    /// <summary>
    /// 치명타 확률
    /// </summary>
    public float CriticalChance { get; set; }
    /// <summary>
    /// 치명타 배율
    /// </summary>
    public float CriticalMag { get; set; }
    /// <summary>
    /// 힘
    /// </summary>
    public float Force { get; set; }

    public bool IsFollowCamera { get; set; }

    public Vector2 PrevCameraPosition { get; set; }

    protected override void Awake()
    {
        base.Awake();

        AudioHandler = GetComponent<AudioHandler_Projectile_ElementalArrow>();
        TriggerHandler = GetComponent<TriggerHandler_Projectile_ElementalArrow>();
        AttackHandler = GetComponent<AttackHandler_Projectile_ElementalArrow>();

        SetStates(new Projectile_ElementalArrow_Idle(this)
            , new Projectile_ElementalArrow_Active(this)
            , new Projectile_ElementalArrow_Deactivate(this));
    }

    public bool Attack(ETriggerType_Projectile_ElementalArrow type)
    {
        Collider2D[] terrains = TriggerHandler.GetColliders(ETriggerType_Projectile_ElementalArrow.Terrain);
        if(terrains[0] != null)
        {
            return true;
        }

        Collider2D[] colliders = TriggerHandler.GetColliders(type);

        for(int i=0; i<colliders.Length && colliders[i] != null; i++)
        {
            IDamagable damagable = colliders[i].GetComponentInParent<IDamagable>();

            if(damagable != null)
            {
                Damage damage = damagable.Damage;

                float random = Random.Range(0.0f, 1.0f);

                if(random <= CriticalChance)
                {
                    damage.SetDamage(false, true, true, EEffectType.None, DamageType, ElementalType
                        , Level, ElementalAffinity, Offense * CriticalMag, Force, AttackHandler.Get(type).transform.position, this);
                }
                else
                {
                    damage.SetDamage(false, true, false, EEffectType.None, DamageType, ElementalType
                        , Level, ElementalAffinity, Offense, Force, AttackHandler.Get(type).transform.position, this);
                }

                damagable.ActivateDamage();
                return true;
            }
        }

        return false;
    }

    protected override void OnActivate()
    {
        base.OnActivate();
        State = EStateType_Projectile_ElementalArrow.Active;
        if (IsFollowCamera)
        {
            PrevCameraPosition = CameraController.Main.transform.position;
        }
    }

    protected override void OnDeactivate()
    {
        base.OnDeactivate();
        State = EStateType_Projectile_ElementalArrow.Deactivate;
    }
}
