﻿using UnityEngine;
using System.Collections;

public class Projectile_ElementalArrow_Idle : State<EStateType_Projectile_ElementalArrow>
{
    private Projectile_ElementalArrow mProjectile;

    public Projectile_ElementalArrow_Idle(Projectile_ElementalArrow projectile) : base(EStateType_Projectile_ElementalArrow.Idle)
    {
        mProjectile = projectile;
    }

    public override void Start()
    {
        mProjectile.Animator.SetInteger("state", (int)Type);
        mProjectile.gameObject.SetActive(false);
    }
}
