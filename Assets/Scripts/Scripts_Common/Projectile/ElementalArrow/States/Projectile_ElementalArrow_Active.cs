﻿using UnityEngine;
using System.Collections;

public class Projectile_ElementalArrow_Active : State<EStateType_Projectile_ElementalArrow>
{
    private Projectile_ElementalArrow mProjectile;

    private float distance;

    public Projectile_ElementalArrow_Active(Projectile_ElementalArrow projectile) : base(EStateType_Projectile_ElementalArrow.Active)
    {
        mProjectile = projectile;
    }

    public override void Start()
    {
        mProjectile.Animator.SetInteger("state", (int)Type);
        mProjectile.RigidBody.velocity = mProjectile.Direction * mProjectile.Speed;
        distance = 0f;
    }

    public override void End()
    {
        mProjectile.RigidBody.velocity = Vector2.zero;
    }

    public override void FixedUpdate()
    {
        distance += mProjectile.Speed * Time.fixedDeltaTime;
        // 최대 발사거리가 되거나 어떤 물체에 부딪히면 비활성화
        if(distance >= mProjectile.Distance
            || mProjectile.Attack(ETriggerType_Projectile_ElementalArrow.Hit))
        {
            mProjectile.Deactivate();
        }
        if(mProjectile.IsFollowCamera)
        {
            Vector2 curPosition = CameraController.Main.transform.position;
            mProjectile.transform.Translate(curPosition - mProjectile.PrevCameraPosition, Space.World);
            mProjectile.PrevCameraPosition = curPosition;
        }
    }
}
