﻿using UnityEngine;
using System.Collections;

public class Projectile_ElementalArrow_Deactivate : State<EStateType_Projectile_ElementalArrow>
{
    private Projectile_ElementalArrow mProjectile;

    public Projectile_ElementalArrow_Deactivate(Projectile_ElementalArrow projectile) : base(EStateType_Projectile_ElementalArrow.Deactivate)
    {
        mProjectile = projectile;
    }

    public override void Start()
    {
        mProjectile.Animator.SetInteger("state", (int)Type);
    }
}
