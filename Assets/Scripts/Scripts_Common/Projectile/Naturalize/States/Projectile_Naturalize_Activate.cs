﻿using UnityEngine;
using System.Collections;

public class Projectile_Naturalize_Activate : State<EStateType_Projectile_Naturalize>
{
    private Projectile_Naturalize mProjectile;

    public Projectile_Naturalize_Activate(Projectile_Naturalize projectile) : base(EStateType_Projectile_Naturalize.Activate)
    {
        mProjectile = projectile;
    }

    public override void Start()
    {
        mProjectile.Animator.SetInteger("state", (int)Type);
        mProjectile.Initialize();
    }

    public override void FixedUpdate()
    {
        mProjectile.Attack(ETriggerType_Projectile_Naturalize.Hit_Left);
        mProjectile.Attack(ETriggerType_Projectile_Naturalize.Hit_Right);
    }
}
