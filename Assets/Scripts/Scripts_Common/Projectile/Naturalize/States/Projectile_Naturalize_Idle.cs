﻿using UnityEngine;
using System.Collections;

public class Projectile_Naturalize_Idle : State<EStateType_Projectile_Naturalize>
{
    private Projectile_Naturalize mProjectile;

    public Projectile_Naturalize_Idle(Projectile_Naturalize projectile) : base(EStateType_Projectile_Naturalize.Idle)
    {
        mProjectile = projectile;
    }

    public override void Start()
    {
        mProjectile.Animator.SetInteger("state", (int)Type);
        mProjectile.gameObject.SetActive(false);
    }
}
