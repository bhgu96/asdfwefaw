﻿using UnityEngine;
using System.Collections;

public enum EStateType_Projectile_Naturalize
{
    Idle, 
    Activate, 
}

public enum ETriggerType_Projectile_Naturalize
{
    None = -1, 

    Hit_Left, 
    Hit_Right, 

    Count
}

public enum EAudioType_Projectile_Naturalize
{
    None = -1, 

    Count
}

[RequireComponent(typeof(TriggerHandler_Projectile_Naturalize), typeof(AudioHandler_Projectile_Naturalize), typeof(AttackHandler_Projectile_Naturalize))]
public class Projectile_Naturalize : AProjectile<EStateType_Projectile_Naturalize>
{
    public override EProjectileType Type => EProjectileType.Naturalize;

    [SerializeField]
    [Tooltip("왼쪽 방향으로 진행하는 이펙트")]
    private Transform mEffectLeft;

    [SerializeField]
    [Tooltip("오른쪽 방향으로 진행하는 이펙트")]
    private Transform mEffectRight;

    [SerializeField]
    private EElementalType mElementalType;
    /// <summary>
    /// 속성 타입
    /// </summary>
    public EElementalType ElementalType { get { return mElementalType; } }

    private int mTerrainLayer = 1 << (int)ELayerFlags.Terrain;

    public TriggerHandler_Projectile_Naturalize TriggerHandler { get; private set; }
    public AudioHandler_Projectile_Naturalize AudioHandler { get; private set; }
    public AttackHandler_Projectile_Naturalize AttackHandler { get; private set; }

    /// <summary>
    /// 수직 최대 크기
    /// </summary>
    public const float VERTICAL_SIZE_LIMIT = 36.0f;
    /// <summary>
    /// 이펙트가 이동하는 횟수
    /// </summary>
    public const int FRAME_COUNT = 3;

    /// <summary>
    /// 기본 이펙트 수직 크기
    /// </summary>
    public const float VERTICAL_SIZE_NORMAL = 12.0f;

    /// <summary>
    /// 기본 이펙트 수평 크기
    /// </summary>
    public const float HORIZONTAL_SIZE_NORMAL = 5.0F;

    /// <summary>
    /// 데미지 타입
    /// </summary>
    public EDamageType DamageType { get; set; }

    /// <summary>
    /// 투사체의 공격력
    /// </summary>
    public float Offense { get; set; }

    /// <summary>
    /// 속성 친화도
    /// </summary>
    public int ElementalAffinity { get; set; }
    /// <summary>
    /// 레벨
    /// </summary>
    public int Level { get; set; }
    /// <summary>
    /// 치명타 확률
    /// </summary>
    public float CriticalChance { get; set; }
    /// <summary>
    /// 치명타 배율
    /// </summary>
    public float CriticalMag { get; set; }
    /// <summary>
    /// 힘
    /// </summary>
    public float Force { get; set; }

    /// <summary>
    /// 시전 거리
    /// </summary>
    public float Distance { get; set; }

    /// <summary>
    /// 구속 시간
    /// </summary>
    public float RestrainTime { get; set; }

    private float mHorizontalSize;

    /// <summary>
    /// 투사체를 초기화하는 메소드
    /// </summary>
    public void Initialize()
    {
        mEffectLeft.localPosition = Vector3.zero;
        mEffectRight.localPosition = Vector3.zero;

        mHorizontalSize = Distance / FRAME_COUNT / HORIZONTAL_SIZE_NORMAL;

        mEffectLeft.localScale = new Vector3(mHorizontalSize, 1.0f, 1.0f);
        mEffectRight.localScale = new Vector3(mHorizontalSize, 1.0f, 1.0f);
    }

    /// <summary>
    /// 공격 메소드
    /// </summary>
    /// <param name="triggerType"></param>
    public void Attack(ETriggerType_Projectile_Naturalize triggerType)
    {
        Collider2D[] colliders = TriggerHandler.GetColliders(triggerType);

        for(int i=0; i<colliders.Length && colliders[i] != null; i++)
        {
            IActor_Damagable actor = colliders[i].GetComponentInParent<IActor_Damagable>();

            IActor_Monster monster = actor as IActor_Monster;
            if (monster != null && monster.CombatType == ECombatType.Tank)
            {
                // 탱커형 몬스터인 경우 비활성화
                Deactivate();
                return;
            }

            if (actor != null)
            {
                Debuff_Restrain restrain = actor.DebuffHandler.Get(EDebuffType.Restrain_Bush) as Debuff_Restrain;
                if (!restrain.IsActivated)
                {
                    Damage damage = actor.Damage;

                    float random = Random.Range(0.0f, 1.0f);

                    if (random <= CriticalChance)
                    {
                        damage.SetDamage(false, true, true, EEffectType.None, DamageType, ElementalType
                            , Level, ElementalAffinity, Offense * CriticalMag, Force, AttackHandler.Get(triggerType).transform.position, this);
                    }
                    else
                    {
                        damage.SetDamage(false, true, false, EEffectType.None, DamageType, ElementalType
                            , Level, ElementalAffinity, Offense, Force, AttackHandler.Get(triggerType).transform.position, this);
                    }

                    actor.ActivateDamage();
                }

                restrain.Activate(RestrainTime);
            }
        }
    }

    /// <summary>
    /// 이펙트를 이동시키는 메소드
    /// </summary>
    /// <param name="distance"></param>
    public void MoveEffect()
    {
        // 각 이펙트가 한번 호출시 이동할 최대 움직임
        float movement = Distance / FRAME_COUNT;

        Vector2 effectPosition = mEffectLeft.position;
        // 왼쪽 이펙트가 왼쪽으로 움직일 수 있는 최대 거리를 구함
        RaycastHit2D hit = Physics2D.Raycast(effectPosition, Vector2.left, movement, mTerrainLayer);

        float effectPosition_x;
        // 터레인에 부딪혔다면
        if(hit)
        {
            // x 좌표를 부딪힌 좌표로 설정
            effectPosition_x = hit.point.x + 0.1f;
        }
        // 부딪히지 않았다면
        else
        {
            // x 좌표를 최대 움직임 만큼 왼쪽으로 이동
            effectPosition_x = effectPosition.x - movement;
        }

        // 왼쪽 이펙트가 위, 아래로 늘어날 수 있는 지 구함
        effectPosition = new Vector2(effectPosition_x, effectPosition.y);

        // 왼쪽 이펙트의 위쪽 방향 충돌 체크
        hit = Physics2D.Raycast(effectPosition, Vector2.up, VERTICAL_SIZE_LIMIT, mTerrainLayer);
        float hitUp_y;
        // 위쪽 방향에 충돌이 발생했다면
        if(hit)
        {
            hitUp_y = hit.point.y;
        }
        // 위쪽 방향에 충돌이 발생하지 않았다면
        else
        {
            hitUp_y = effectPosition.y + VERTICAL_SIZE_LIMIT;
        }

        // 왼쪽 이펙트의 아래쪽 방향 충돌 체크
        hit = Physics2D.Raycast(effectPosition, Vector2.down, VERTICAL_SIZE_LIMIT, mTerrainLayer);
        float hitDown_y;
        // 아래쪽 방향에 충돌이 발생했다면
        if(hit)
        {
            hitDown_y = hit.point.y;
        }
        // 아래쪽 방향에 충돌이 발생하지 않았다면
        else
        {
            hitDown_y = effectPosition.y - VERTICAL_SIZE_LIMIT;
        }

        // 왼쪽 이펙트의 수직 중심 좌표
        float effectPosition_y = (hitUp_y + hitDown_y) * 0.5f;
        // 왼쪽 이펙트의 수직 크기
        float effectSize_y = (hitUp_y - hitDown_y);
        // 왼쪽 이펙트의 중심 좌표를 설정
        mEffectLeft.position = new Vector3(effectPosition.x, effectPosition_y);
        // 왼쪽 이펙트의 수직 크기를 설정
        mEffectLeft.localScale = new Vector3(mHorizontalSize, effectSize_y / VERTICAL_SIZE_NORMAL * 0.5f, 1.0f);

        // 오른쪽 이펙트가 오른쪽으로 움직일 수 있는 최대 움직임을 구함
        effectPosition = mEffectRight.position;
        // 오른쪽 이펙트가 오른쪽으로 움직일 수 있는 최대 거리를 구함
        hit = Physics2D.Raycast(effectPosition, Vector2.right, movement, mTerrainLayer);

        // 터레인에 부딪혔다면
        if (hit)
        {
            // x 좌표를 부딪힌 좌표로 설정
            effectPosition_x = hit.point.x - 0.1f;
        }
        // 부딪히지 않았다면
        else
        {
            // x 좌표를 최대 움직임 만큼 오른쪽으로 이동
            effectPosition_x = effectPosition.x + movement;
        }

        // 오른쪽 이펙트가 위, 아래로 늘어날 수 있는 지 구함
        effectPosition = new Vector2(effectPosition_x, effectPosition.y);

        // 오른쪽 이펙트의 위쪽 방향 충돌 체크
        hit = Physics2D.Raycast(effectPosition, Vector2.up, VERTICAL_SIZE_LIMIT, mTerrainLayer);
        // 위쪽 방향에 충돌이 발생했다면
        if (hit)
        {
            hitUp_y = hit.point.y;
        }
        // 위쪽 방향에 충돌이 발생하지 않았다면
        else
        {
            hitUp_y = effectPosition.y + VERTICAL_SIZE_LIMIT;
        }

        // 오른쪽 이펙트의 아래쪽 방향 충돌 체크
        hit = Physics2D.Raycast(effectPosition, Vector2.down, VERTICAL_SIZE_LIMIT, mTerrainLayer);
        // 아래쪽 방향에 충돌이 발생했다면
        if (hit)
        {
            hitDown_y = hit.point.y;
        }
        // 아래쪽 방향에 충돌이 발생하지 않았다면
        else
        {
            hitDown_y = effectPosition.y - VERTICAL_SIZE_LIMIT;
        }

        // 오른쪽 이펙트의 수직 중심 좌표
        effectPosition_y = (hitUp_y + hitDown_y) * 0.5f;
        // 오른쪽 이펙트의 수직 크기
        effectSize_y = (hitUp_y - hitDown_y);
        // 오른쪽 이펙트의 중심 좌표를 설정
        mEffectRight.position = new Vector3(effectPosition.x, effectPosition_y);
        // 오른쪽 이펙트의 수직 크기를 설정
        mEffectRight.localScale = new Vector3(mHorizontalSize, effectSize_y / VERTICAL_SIZE_NORMAL * 0.5f, 1.0f);
    }

    protected override void Awake()
    {
        base.Awake();

        TriggerHandler = GetComponent<TriggerHandler_Projectile_Naturalize>();
        AudioHandler = GetComponent<AudioHandler_Projectile_Naturalize>();
        AttackHandler = GetComponent<AttackHandler_Projectile_Naturalize>();

        SetStates(new Projectile_Naturalize_Idle(this)
            , new Projectile_Naturalize_Activate(this));
    }

    protected override void OnActivate()
    {
        base.OnActivate();
        State = EStateType_Projectile_Naturalize.Activate;
    }

    protected override void OnDeactivate()
    {
        base.OnDeactivate();
        State = EStateType_Projectile_Naturalize.Idle;
    }
}
