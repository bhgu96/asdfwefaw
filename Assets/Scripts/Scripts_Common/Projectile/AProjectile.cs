﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections.Generic;
using System;
using Object;

public enum EProjectileType
{
    None = -1,

    // 땅속성 화살
    EarthElementalArrow = 0,
    // 자연화
    Naturalize = 1,
    // 신비 속성 탄
    ArcaneShell = 2,
    // 그림자 분신
    ShadowAlterEgo = 3, 
    // 화속성 탄환
    FireBullet = 4, 
    // 풍속성 탄환
    WindBullet = 5, 
    // 암속성 탄환
    DarknessBullet = 6, 

    FairyAttack = 7, 


    Count
}

public interface IProjectile : IPoolable<EProjectileType>
{
    /// <summary>
    /// 이름
    /// </summary>
    string Name { get; }
    /// <summary>
    /// 이름 태그
    /// </summary>
    string NameTag { get; }
    /// <summary>
    /// 설명
    /// </summary>
    string Description { get; }
    /// <summary>
    /// 설명 태그
    /// </summary>
    string DescriptionTag { get; }

    /// <summary>
    /// 메인 객체
    /// </summary>
    Transform MainTransform { get; }

    /// <summary>
    /// 강체
    /// </summary>
    Rigidbody2D RigidBody { get; }

    /// <summary>
    /// 애니메이터
    /// </summary>
    Animator Animator { get; }

    /// <summary>
    /// 투사체의 각도
    /// </summary>
    float Angle { get; set; }

    /// <summary>
    /// 벡터 방향
    /// </summary>
    Vector2 Direction { get; }

    /// <summary>
    /// 활성화 여부
    /// </summary>
    bool IsActivated { get; }
    /// <summary>
    /// 활성화 가능 여부
    /// </summary>
    bool CanActivate { get; }
    /// <summary>
    /// 비활성화 가능 여부
    /// </summary>
    bool CanDeactivate { get; }

    /// <summary>
    /// 벡터를 이용해 투사체의 각도를 설정하는 메소드
    /// </summary>
    /// <param name="direction"></param>
    /// <param name="isNormalized"></param>
    void SetDirection(Vector2 direction, bool isNormalized);

    /// <summary>
    /// 활성화 메소드
    /// </summary>
    /// <returns></returns>
    bool Activate();

    /// <summary>
    /// 비활성화 메소드
    /// </summary>
    /// <returns></returns>
    bool Deactivate();
}

[RequireComponent(typeof(Rigidbody2D), typeof(Animator))]
public abstract class AProjectile<EStateType> : StateBase<EStateType>, IProjectile
    where EStateType : Enum
{
    /// <summary>
    /// 투사체 타입
    /// </summary>
    public abstract EProjectileType Type { get; }

    public bool IsPooled { get { return !gameObject.activeInHierarchy; } }

    [SerializeField]
    private Sprite mIcon;
    public Sprite Icon { get { return mIcon; } }

    /// <summary>
    /// 이름 태그
    /// </summary>
    public string NameTag { get { return mInfo.NameTag; } }

    /// <summary>
    /// 이름
    /// </summary>
    public virtual string Name { get { return DataManager.GetScript(NameTag); } }

    /// <summary>
    /// 설명 태그
    /// </summary>
    public string DescriptionTag { get { return mInfo.DescriptionTag; } }

    /// <summary>
    /// 설명
    /// </summary>
    public virtual string Description { get { return DataManager.GetScript(DescriptionTag); } }

    [SerializeField]
    private Transform mMainTransform;
    /// <summary>
    /// 메인 객체
    /// </summary>
    public Transform MainTransform { get { return mMainTransform; } }

    /// <summary>
    /// 물리 객체
    /// </summary>
    public Rigidbody2D RigidBody { get; private set; }

    /// <summary>
    /// 애니메이터
    /// </summary>
    public Animator Animator { get; private set; }

    /// <summary>
    /// 현재 Projectile이 보고 있는 방향
    /// </summary>
    public EHorizontalDirection HorizontalDirection
    {
        get { return mHorizontalDirection; }
        set
        {
            // 현재 방향과 다르다면
            if (mHorizontalDirection != value)
            {
                // 기존 스케일.x 에 -1을 곱함
                Vector3 prevLocalScale = mMainTransform.localScale;
                mMainTransform.localScale = new Vector3(prevLocalScale.x * -1.0f, prevLocalScale.y, prevLocalScale.z);
                mHorizontalDirection = value;
            }
        }
    }
    private EHorizontalDirection mHorizontalDirection;

    /// <summary>
    /// 객체의 속도 프로퍼티
    /// </summary>
    public Vector2 Velocity
    {
        get { return RigidBody.velocity; }
        set
        {
            // 다음 Fixed Update 한번 마찰력을 0으로 만듬
            mIsPhysicsUpdated = true;
            RigidBody.velocity = value;
        }
    }
    private bool mIsPhysicsUpdated = false;

    /// <summary>
    /// 객체의 물체 특성 프로퍼티
    /// </summary>
    public PhysicsMaterial2D PhysicsMaterial { get; set; }

    private float mAngle;
    public float Angle
    {
        get { return mAngle; }
        set
        {
            while(value > 180.0f)
            {
                value -= 360.0f;
            }
            while(value < -180.0f)
            {
                value += 360.0f;
            }

            mAngle = value;


            if (!mIsSetDirection)
            {
                Direction = new Vector2(Mathf.Cos(mAngle * Mathf.Deg2Rad), Mathf.Sin(mAngle * Mathf.Deg2Rad));
            }
            else
            {
                mIsSetDirection = false;
            }

            // 오른쪽을 보고있는 경우
            if (mAngle >= -90.0f && mAngle <= 90.0f)
            {
                HorizontalDirection = EHorizontalDirection.Right;

                MainTransform.eulerAngles = new Vector3(0.0f, 0.0f, mAngle);
            }
            // 왼쪽을 보고있는 경우
            else
            {
                HorizontalDirection = EHorizontalDirection.Left;

                MainTransform.eulerAngles = new Vector3(0.0f, 0.0f, mAngle - 180.0f);
            }
        }
    }

    /// <summary>
    /// 활성화 여부
    /// </summary>
    public bool IsActivated { get; private set; }

    /// <summary>
    /// 활성화 가능 여부
    /// </summary>
    public virtual bool CanActivate { get { return !gameObject.activeInHierarchy; } }

    /// <summary>
    /// 활성화 불가능 여부
    /// </summary>
    public virtual bool CanDeactivate { get { return IsActivated; } }

    /// <summary>
    /// 각도에 따른 벡터 방향
    /// </summary>
    public Vector2 Direction { get; private set; }
    private bool mIsSetDirection = false;

    private ProjectileInfo mInfo;

    /// <summary>
    /// 객체를 수평이동 시키는 메소드
    /// </summary>
    /// <param name="speed"></param>
    /// <param name="direction"></param>
    public void MoveHorizontal(float speed, EHorizontalDirection direction)
    {
        HorizontalDirection = direction;
        Velocity = new Vector2((float)HorizontalDirection * speed, Velocity.y);
    }

    /// <summary>
    /// 움직인 방향(direction)에 따라 Main 객체의 각도를 조절하는 메소드
    /// </summary>
    /// <param name="direction"></param>
    public void SetDirection(Vector2 direction, bool isNormalized)
    {
        if (!isNormalized)
        {
            direction.Normalize();
        }

        Direction = direction;
        mIsSetDirection = true;

        // 윗방향으로 움직인 경우
        if (direction.y >= 0.0f)
        {
            Angle = Mathf.Acos(direction.x) * Mathf.Rad2Deg;
        }
        // 아랫 방향으로 움직인 경우
        else
        {
            Angle = -Mathf.Acos(direction.x) * Mathf.Rad2Deg;
        }
    }

    /// <summary>
    /// 활성화 메소드
    /// </summary>
    public bool Activate()
    {
        if (CanActivate)
        {
            gameObject.SetActive(true);
            IsActivated = true;
            OnActivate();
            return true;
        }
        return false;
    }

    /// <summary>
    /// 비활성화 메소드
    /// </summary>
    /// <returns></returns>
    public bool Deactivate()
    {
        if(CanDeactivate)
        {
            IsActivated = false;
            OnDeactivate();
            return true;
        }
        return false;
    }

    /// <summary>
    /// 활성화 메소드 호출시 호출되는 콜백 메소드
    /// </summary>
    protected virtual void OnActivate() { }

    /// <summary>
    /// 비활성화 메소드 호출시 호출되는 콜백 메소드
    /// </summary>
    protected virtual void OnDeactivate() { }

    protected override void FixedUpdate()
    {
        base.FixedUpdate();

        if (mIsPhysicsUpdated)
        {
            mIsPhysicsUpdated = false;
            RigidBody.sharedMaterial = GameConstant.ZeroMaterial;
        }
        else if (RigidBody.sharedMaterial != PhysicsMaterial)
        {
            RigidBody.sharedMaterial = PhysicsMaterial;
        }
    }

    protected override void Awake()
    {
        base.Awake();

        SceneManager.activeSceneChanged += SceneManager_activeSceneChanged;

        if(transform.eulerAngles.z >= 0.0f)
        {
            mHorizontalDirection = EHorizontalDirection.Right;
        }
        else
        {
            mHorizontalDirection = EHorizontalDirection.Left;
        }

        mInfo = DataManager.GetProjectileInfo(Type);

        Animator = GetComponent<Animator>();
        RigidBody = GetComponent<Rigidbody2D>();

        PhysicsMaterial = RigidBody.sharedMaterial;
    }

    private void SceneManager_activeSceneChanged(Scene arg0, Scene arg1)
    {
        Deactivate();
        gameObject.SetActive(false);
    }
}

public class ProjectileInfo
{
    public string NameTag { get; }
    public string DescriptionTag { get; }

    public ProjectileInfo(Dictionary<string, string> projectileDict)
    {
        NameTag = projectileDict["Name Tag"];
        DescriptionTag = projectileDict["Desc Tag"];
    }
}