﻿using UnityEngine;

public enum EStateType_Projectile_ElementalShell
{
    None = -1,

    Idle = 0, 
    Active = 1, 
    Search = 2, 
    Deactivate = 3, 

    Count
}

public enum ETriggerType_Projectile_ElementalShell
{
    None = -1,

    Hit, 
    Search, 

    Count
}

public enum EAudioType_Projectile_ElementalShell
{
    None = -1,


    Count
}

[RequireComponent(typeof(TriggerHandler_Projectile_ElementalShell), typeof(AudioHandler_Projectile_ElementalShell), typeof(AttackHandler_Projectile_ElementalShell))]
public class Projectile_ElementalShell : AProjectile<EStateType_Projectile_ElementalShell>
{
    [SerializeField]
    private EProjectileType mType;
    public override EProjectileType Type { get { return mType; } }

    public TriggerHandler_Projectile_ElementalShell TriggerHandler { get; private set; }
    public AudioHandler_Projectile_ElementalShell AudioHandler { get; private set; }
    public AttackHandler_Projectile_ElementalShell AttackHandler { get; private set; }

    [SerializeField]
    private EElementalType mElementalType;
    /// <summary>
    /// 속성 타입
    /// </summary>
    public EElementalType ElementalType { get { return mElementalType; } }

    [SerializeField]
    private float mDuration;
    /// <summary>
    /// 지속 시간
    /// </summary>
    public float Duration { get { return mDuration; } }
    /// <summary>
    /// 남은 지속시간
    /// </summary>
    public float RemainDuration { get; private set; }

    /// <summary>
    /// 목표 액터
    /// </summary>
    public IActor_Damagable Target { get; set; }

    /// <summary>
    /// 투사체를 발사한 객체의 타입
    /// </summary>
    public EDamageType DamageType { get; set; }

    /// <summary>
    /// 초기 가속도
    /// </summary>
    public float Acceleration_Start { get; set; }

    /// <summary>
    /// 지속 가속도
    /// </summary>
    public float Acceleration_Update { get; set; }

    /// <summary>
    /// 공격력
    /// </summary>
    public float Offense { get; set; }

    /// <summary>
    /// 레벨
    /// </summary>
    public int Level { get; set; }

    /// <summary>
    /// 투사체의 속성 친화도
    /// </summary>
    public int ElementalAffinity { get; set; }

    /// <summary>
    /// 치명타 확률
    /// </summary>
    public float CriticalChance { get; set; }

    /// <summary>
    /// 치명타 배율
    /// </summary>
    public float CriticalMag { get; set; }

    /// <summary>
    /// 힘
    /// </summary>
    public float Force { get; set; }

    /// <summary>
    /// 카메라를 따라갈 것인가
    /// </summary>
    public bool IsFollowCamera { get; set; }

    /// <summary>
    /// 카메라의 이전 좌표
    /// 해당 객체의 Active 상태에서 초기화
    /// </summary>
    public Vector2 PrevCameraPosition { get; set; }

    /// <summary>
    /// 공격 검사 후 적용 메소드
    /// </summary>
    /// <param name="type"></param>
    public bool Attack(ETriggerType_Projectile_ElementalShell type)
    {
        bool isHit = false;

        Collider2D[] colliders = TriggerHandler.GetColliders(type);

        for(int i=0; i<colliders.Length && colliders[i] != null; i++)
        {
            IDamagable damagable = colliders[i].GetComponentInParent<IDamagable>();
            if(damagable != null)
            {
                Damage damage = damagable.Damage;

                float gamble = Random.Range(0.0f, 1.0f);
                // 치명타 발생
                if(gamble <= CriticalChance)
                {
                    damage.SetDamage(false, true, true, EEffectType.Hit_Impact_01, DamageType, ElementalType
                        , Level, ElementalAffinity, Offense * CriticalMag, Force, AttackHandler.Get(type).transform.position, this);
                }
                else
                {
                    damage.SetDamage(false, true, false, EEffectType.Hit_Impact_01, DamageType, ElementalType
                        , Level, ElementalAffinity, Offense, Force, AttackHandler.Get(type).transform.position, this);
                }

                isHit = true;
                damagable.ActivateDamage();
            }
        }

        return isHit;
    }

    /// <summary>
    /// 목표 객체를 탐색하는 메소드
    /// </summary>
    /// <param name="type"></param>
    /// <returns></returns>
    public bool Detect(ETriggerType_Projectile_ElementalShell type)
    {
        Collider2D[] colliders = TriggerHandler.GetColliders(type);

        Vector2 curPosition = transform.position;

        IActor_Damagable minTarget = null;
        float minMagnitude = 9999.0f;

        for (int i = 0; i < colliders.Length && colliders[i] != null; i++)
        {
            IActor_Damagable target = colliders[i].GetComponentInParent<IActor_Damagable>();
            if(target != null)
            {
                Vector2 targetPosition = target.transform.position;
                float magnitude = (targetPosition - curPosition).magnitude;
                // 더 가까이 있는 목표 객체가 존재한다면
                if(magnitude < minMagnitude)
                {
                    // 해당 객체를 목표 객체로 설정
                    minMagnitude = magnitude;
                    minTarget = target;
                }
            }
        }

        // 감지된 목표 객체가 존재하는 경우
        if(minTarget != null)
        {
            Target = minTarget;
            return true;
        }
        return false;
    }

    protected override void Update()
    {
        base.Update();

        // 지속시간 검사
        RemainDuration -= Time.deltaTime;
        if(RemainDuration <= 0.0f)
        {
            State = EStateType_Projectile_ElementalShell.Deactivate;
        }
    }

    protected override void FixedUpdate()
    {
        base.FixedUpdate();

        // 방향 갱신
        SetDirection(RigidBody.velocity, false);
    }

    protected override void Awake()
    {
        base.Awake();

        TriggerHandler = GetComponent<TriggerHandler_Projectile_ElementalShell>();
        AudioHandler = GetComponent<AudioHandler_Projectile_ElementalShell>();
        AttackHandler = GetComponent<AttackHandler_Projectile_ElementalShell>();

        SetStates(new Projectile_ElementalShell_Idle(this)
            , new Projectile_ElementalShell_Active(this)
            , new Projectile_ElementalShell_Search(this)
            , new Projectile_ElementalShell_Deactivate(this));
    }

    protected override void OnActivate()
    {
        base.OnActivate();
        // 지속시간 초기화
        RemainDuration = Duration;
        // 초기 속도 설정
        RigidBody.velocity = Direction * Acceleration_Start;
        // 활성화 상태로 전이
        State = EStateType_Projectile_ElementalShell.Active;
    }

    protected override void OnDeactivate()
    {
        base.OnDeactivate();
        // 비활성화 상태로 전이
        State = EStateType_Projectile_ElementalShell.Deactivate;
    }
}
