﻿using UnityEngine;

public class Projectile_ElementalShell_Search : State<EStateType_Projectile_ElementalShell>
{
    private Projectile_ElementalShell mProjectile;

    public Projectile_ElementalShell_Search(Projectile_ElementalShell projectile) : base(EStateType_Projectile_ElementalShell.Search)
    {
        mProjectile = projectile;
    }

    public override void FixedUpdate()
    {
        // 투사체가 카메라를 따라간다면
        if(mProjectile.IsFollowCamera)
        {
            // 투사체 좌표를 카메라 이동 좌표에 따라 갱신
            Vector2 curPosition = mProjectile.transform.position;
            Vector2 prevCameraPosition = mProjectile.PrevCameraPosition;
            Vector2 curCameraPosition = CameraController.Main.transform.position;
            curPosition += (curCameraPosition - prevCameraPosition);
            mProjectile.PrevCameraPosition = curCameraPosition;
            mProjectile.transform.position = curPosition;
        }

        // 목표 객체가 탐지된 경우
        if(mProjectile.Detect(ETriggerType_Projectile_ElementalShell.Search))
        {
            // Active 상태로 전이
            mProjectile.State = EStateType_Projectile_ElementalShell.Active;
        }
    }
}
