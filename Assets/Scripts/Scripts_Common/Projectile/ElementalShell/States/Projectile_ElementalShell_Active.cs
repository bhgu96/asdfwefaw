﻿using UnityEngine;

public class Projectile_ElementalShell_Active : State<EStateType_Projectile_ElementalShell>
{
    private Projectile_ElementalShell mProjectile;

    public Projectile_ElementalShell_Active(Projectile_ElementalShell projectile) : base(EStateType_Projectile_ElementalShell.Active)
    {
        mProjectile = projectile;
    }

    public override void Start()
    {
        mProjectile.Animator.SetInteger("state", (int)Type);

        // 카메라를 따라간다면
        if (mProjectile.IsFollowCamera)
        {
            // 카메라 초기 좌표 설정
            mProjectile.PrevCameraPosition = CameraController.Main.transform.position;
        }

        // 목표 객체가 설정되지 않은 경우
        if (mProjectile.Target == null)
        {
            // Search 상태로 전이
            mProjectile.State = EStateType_Projectile_ElementalShell.Search;
        }
    }

    public override void FixedUpdate()
    {
        // 목표 객체를 포함한 어떤 객체를 감지해서 공격했다면
        if(mProjectile.Attack(ETriggerType_Projectile_ElementalShell.Hit))
        {
            // 투사체 비활성화
            mProjectile.Deactivate();
            return;
        }

        Vector2 curPosition = mProjectile.transform.position;

        //투사체가 카메라를 따라간다면
        if (mProjectile.IsFollowCamera)
        {
            // 카메라의 이동에 따른 좌표 갱신
            Vector2 curCameraPosition = CameraController.Main.transform.position;
            Vector2 prevCameraPosition = mProjectile.PrevCameraPosition;
            curPosition += (curCameraPosition - prevCameraPosition);
            mProjectile.PrevCameraPosition = curCameraPosition;
            mProjectile.transform.position = curPosition;
        }

        // 투사체 속도 갱신
        Vector2 targetPosition = mProjectile.Target.MainCollider.bounds.center;
        Vector2 curVelocity = mProjectile.RigidBody.velocity;
        mProjectile.RigidBody.velocity = (curVelocity.normalized * 0.85f + (targetPosition - curPosition).normalized * 0.15f) * (curVelocity.magnitude + mProjectile.Acceleration_Update);
          
    }

    public override void Update()
    {
        // 목표 객체가 죽은 상태인 경우
        if(mProjectile.Target.Status.Life < 1.0f)
        {
            // Search 상태로 전이
            mProjectile.State = EStateType_Projectile_ElementalShell.Search;
        }
    }
}
