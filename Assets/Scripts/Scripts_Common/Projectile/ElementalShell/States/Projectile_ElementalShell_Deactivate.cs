﻿using UnityEngine;

public class Projectile_ElementalShell_Deactivate : State<EStateType_Projectile_ElementalShell>
{
    private Projectile_ElementalShell mProjectile;

    public Projectile_ElementalShell_Deactivate(Projectile_ElementalShell projectile) : base(EStateType_Projectile_ElementalShell.Deactivate)
    {
        mProjectile = projectile;
    }

    public override void Start()
    {
        mProjectile.Animator.SetInteger("state", (int)Type);
        // 투사체 정지
        mProjectile.RigidBody.velocity = Vector2.zero;
    }
}
