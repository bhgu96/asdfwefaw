﻿using UnityEngine;

public class Projectile_ElementalShell_Idle : State<EStateType_Projectile_ElementalShell>
{
    private Projectile_ElementalShell mProjectile;

    public Projectile_ElementalShell_Idle(Projectile_ElementalShell projectile) : base(EStateType_Projectile_ElementalShell.Idle)
    {
        mProjectile = projectile;
    }

    public override void Start()
    {
        mProjectile.Animator.SetInteger("state", (int)Type);
        mProjectile.gameObject.SetActive(false);
    }
}
