﻿using UnityEngine;
using System.Collections.Generic;

public enum EStateType_Projectile_ShadowAlterEgo
{
    None = -1, 

    Idle = 0, 
    Active = 1, 
    Deactivate = 2, 

    Count
}

public enum ETriggerType_Projectile_ShadowAlterEgo
{
    None = -1, 

    Hit = 0, 

    Count
}

public enum EAudioType_Projectile_ShadowAlterEgo
{
    None = -1, 

    Count
}

[RequireComponent(typeof(TriggerHandler_Projectile_ShadowAlterEgo), typeof(AudioHandler_Projectile_ShadowAlterEgo), typeof(AttackHandler_Projectile_ShadowAlterEgo))]
public class Projectile_ShadowAlterEgo : AProjectile<EStateType_Projectile_ShadowAlterEgo>
{
    /// <summary>
    /// 투사체 타입
    /// </summary>
    public override EProjectileType Type => EProjectileType.ShadowAlterEgo;

    /// <summary>
    /// 속성 타입
    /// </summary>
    public EElementalType ElementalType { get { return EElementalType.Darkness; } }

    /// <summary>
    /// 송신자 객체 타입
    /// </summary>
    public EDamageType DamageType { get; set; }

    /// <summary>
    /// 활성화 시간
    /// </summary>
    public float ActiveTime { get; set; }

    /// <summary>
    /// 투사체 속도
    /// </summary>
    public float Speed { get; set; }

    /// <summary>
    /// 공격력
    /// </summary>
    public float Offense { get; set; }

    /// <summary>
    /// 속성 친화도
    /// </summary>
    public int ElementalAffinity { get; set; }

    /// <summary>
    /// 레벨
    /// </summary>
    public int Level { get; set; }

    /// <summary>
    /// 힘
    /// </summary>
    public float Force { get; set; }

    /// <summary>
    /// 치명타 확률
    /// </summary>
    public float CriticalChance { get; set; }

    /// <summary>
    /// 치명타 배율
    /// </summary>
    public float CriticalMag { get; set; }

    /// <summary>
    /// 카메라 위치 동기화 여부
    /// </summary>
    public bool IsFollowCamera { get; set; }
    
    public TriggerHandler_Projectile_ShadowAlterEgo TriggerHandler { get; private set; }
    public AudioHandler_Projectile_ShadowAlterEgo AudioHandler { get; private set; }
    public AttackHandler_Projectile_ShadowAlterEgo AttackHandler { get; private set; }

    private List<IDamagable> mDamagedList;

    /// <summary>
    /// 데미지를 줄 수 있는 객체를 감지하고 데미지를 전달하는 메소드
    /// 한번의 활성화에 하나의 객체에 한번만 데미지를 줄 수 있다.
    /// </summary>
    /// <param name="type"></param>
    public void Attack(ETriggerType_Projectile_ShadowAlterEgo type)
    {
        Collider2D[] colliders = TriggerHandler.GetColliders(type);

        for(int i=0; i<colliders.Length && colliders[i] != null; i++)
        {
            IDamagable damagable = colliders[i].GetComponentInParent<IDamagable>();

            // 해당 투사체에 대해 한번도 데미지를 받지 않은 객체라면
            if(damagable != null && !mDamagedList.Contains(damagable))
            {
                mDamagedList.Add(damagable);

                Damage damage = damagable.Damage;

                float chance = Random.Range(0.0f, 1.0f);
                if (chance <= CriticalChance)
                {
                    damage.SetDamage(false, true, true, EEffectType.Hit_Circle_01, DamageType, ElementalType
                        , Level, ElementalAffinity, Offense * CriticalMag, Force, AttackHandler.Get(type).transform.position, this);
                }
                else
                {
                    damage.SetDamage(false, true, false, EEffectType.Hit_Circle_01, DamageType, ElementalType
                        , Level, ElementalAffinity, Offense, Force, AttackHandler.Get(type).transform.position, this);
                }

                damagable.ActivateDamage();

                mDamagedList.Add(damagable);

                IActor_Monster monster = damagable as IActor_Monster;

                if(monster != null && monster.CombatType == ECombatType.Tank)
                {
                    // 탱커형 몬스터인 경우 비활성화
                    Deactivate();
                    return;
                }
            }
        }
    }

    protected override void OnActivate()
    {
        base.OnActivate();
        State = EStateType_Projectile_ShadowAlterEgo.Active;
        mDamagedList.Clear();
    }

    protected override void OnDeactivate()
    {
        base.OnDeactivate();
        State = EStateType_Projectile_ShadowAlterEgo.Deactivate;
    }
    protected override void Awake()
    {
        base.Awake();

        mDamagedList = new List<IDamagable>();

        TriggerHandler = GetComponent<TriggerHandler_Projectile_ShadowAlterEgo>();
        AudioHandler = GetComponent<AudioHandler_Projectile_ShadowAlterEgo>();
        AttackHandler = GetComponent<AttackHandler_Projectile_ShadowAlterEgo>();

        SetStates(new Projectile_ShadowAlterEgo_Idle(this)
            , new Projectile_ShadowAlterEgo_Active(this)
            , new Projectile_ShadowAlterEgo_Deactivate(this));
    }
}
