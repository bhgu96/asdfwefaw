﻿using UnityEngine;

public class Projectile_ShadowAlterEgo_Deactivate : State<EStateType_Projectile_ShadowAlterEgo>
{
    private Projectile_ShadowAlterEgo mProjectile;

    public Projectile_ShadowAlterEgo_Deactivate(Projectile_ShadowAlterEgo projectile) : base(EStateType_Projectile_ShadowAlterEgo.Deactivate)
    {
        mProjectile = projectile;
    }

    public override void Start()
    {
        mProjectile.Animator.SetInteger("state", (int)Type);
    }
}
