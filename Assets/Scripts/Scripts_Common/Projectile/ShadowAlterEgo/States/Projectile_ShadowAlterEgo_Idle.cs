﻿using UnityEngine;

public class Projectile_ShadowAlterEgo_Idle : State<EStateType_Projectile_ShadowAlterEgo>
{
    private Projectile_ShadowAlterEgo mProjectile;

    public Projectile_ShadowAlterEgo_Idle(Projectile_ShadowAlterEgo projectile) : base(EStateType_Projectile_ShadowAlterEgo.Idle)
    {
        mProjectile = projectile;
    }

    public override void Start()
    {
        mProjectile.Animator.SetInteger("state", (int)Type);
        mProjectile.gameObject.SetActive(false);
    }
}
