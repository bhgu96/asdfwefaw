﻿using UnityEngine;
using System.Collections;

public class Projectile_ShadowAlterEgo_Active : State<EStateType_Projectile_ShadowAlterEgo>
{
    private Projectile_ShadowAlterEgo mProjectile;
    private float mRemainTime;
    private Vector2 mPrevCameraPosition;

    public Projectile_ShadowAlterEgo_Active(Projectile_ShadowAlterEgo projectile) : base(EStateType_Projectile_ShadowAlterEgo.Active)
    {
        mProjectile = projectile;
    }

    public override void Start()
    {
        mProjectile.Animator.SetInteger("state", (int)Type);
        mRemainTime = mProjectile.ActiveTime;

        if(mProjectile.IsFollowCamera)
        {
            mPrevCameraPosition = CameraController.Main.transform.position;
        }
    }

    public override void FixedUpdate()
    {
        if(mProjectile.IsFollowCamera)
        {
            Vector2 curCameraPosition = CameraController.Main.transform.position;
            Vector2 curPosition = mProjectile.transform.position;
            mProjectile.transform.position = curPosition + (curCameraPosition - mPrevCameraPosition);
            mPrevCameraPosition = curCameraPosition;
        }

        if(mRemainTime > 0.0f)
        {
            mRemainTime -= Time.deltaTime;
            mProjectile.Attack(ETriggerType_Projectile_ShadowAlterEgo.Hit);
            mProjectile.MoveHorizontal(mProjectile.Speed, mProjectile.HorizontalDirection);
        }
        else
        {
            mProjectile.Deactivate();
        }
    }
}
