﻿using UnityEngine;

public class Projectile_FairyAttack_Cast : State<EStateType_Projectile_FairyAttack>
{
    private Projectile_FairyAttack mProjectile;

    public Projectile_FairyAttack_Cast(Projectile_FairyAttack projectile) : base(EStateType_Projectile_FairyAttack.Cast)
    {
        mProjectile = projectile;
    }

    public override void Start()
    {
        mProjectile.Animator.SetInteger("state", (int)Type);
    }
}
