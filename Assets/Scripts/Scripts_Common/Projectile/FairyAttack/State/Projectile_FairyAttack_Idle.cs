﻿using UnityEngine;

public class Projectile_FairyAttack_Idle : State<EStateType_Projectile_FairyAttack>
{
    private Projectile_FairyAttack mProjectile;

    public Projectile_FairyAttack_Idle(Projectile_FairyAttack projectile) : base(EStateType_Projectile_FairyAttack.Idle)
    {
        mProjectile = projectile;
    }

    public override void Start()
    {
        mProjectile.Animator.SetInteger("state", (int)Type);
        mProjectile.gameObject.SetActive(false);
    }
}
