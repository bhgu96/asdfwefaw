﻿using UnityEngine;
using System.Collections;

public enum EStateType_Projectile_FairyAttack
{
    None = -1, 

    Idle = 0, 
    Cast = 1, 

    Count
}

public enum ETriggerType_Projectile_FairyAttack
{
    None = -1, 

    FairyAttack = 0, 

    Count
}

public enum EAudioType_Projectile_FairyAttack
{
    None = -1, 


    Count
}

[RequireComponent(typeof(TriggerHandler_Projectile_FairyAttack), typeof(AudioHandler_Projectile_FairyAttack), typeof(AttackHandler_Projectile_FairyAttack))]
public class Projectile_FairyAttack : AProjectile<EStateType_Projectile_FairyAttack>
{
    public override EProjectileType Type
    {
        get { return EProjectileType.FairyAttack; }
    }

    public TriggerHandler_Projectile_FairyAttack TriggerHandler { get; private set; }
    public AudioHandler_Projectile_FairyAttack AudioHandler { get; private set; }
    public AttackHandler_Projectile_FairyAttack AttackHandler { get; private set; }

    [SerializeField]
    private DigitalRuby.LightningBolt.LightningBoltScript mLightning;
    public DigitalRuby.LightningBolt.LightningBoltScript Lightning { get { return mLightning; } }

    public int Offense { get; set; }
    public float Force { get; set; }
    public GameObject SrcObject { get; set; }

    public void Attack(ETriggerType_Projectile_FairyAttack type)
    {
        Collider2D[] colliders = TriggerHandler.GetColliders(type);

        for(int i=0; i<colliders.Length && colliders[i] != null; i++)
        {
            IDamagable damagable = colliders[i].GetComponentInParent<IDamagable>();

            if(damagable != null)
            {
                damagable.Damage.Set(Offense, Force, EDamageType.Fairy, ESrcType.Fairy, EEffectType.None, AttackHandler.Get(type).transform.position);
                damagable.ActivateDamage();
                CameraController.Main.Shake(0.1f, 0.2f);
            }
        }
    }

    protected override void Awake()
    {
        base.Awake();

        TriggerHandler = GetComponent<TriggerHandler_Projectile_FairyAttack>();
        AudioHandler = GetComponent<AudioHandler_Projectile_FairyAttack>();
        AttackHandler = GetComponent<AttackHandler_Projectile_FairyAttack>();

        SetStates(new Projectile_FairyAttack_Idle(this)
            , new Projectile_FairyAttack_Cast(this));
    }

    protected override void OnActivate()
    {
        base.OnActivate();
        State = EStateType_Projectile_FairyAttack.Cast;
        mLightning.StartObject = SrcObject;
    }

    protected override void OnDeactivate()
    {
        base.OnDeactivate();
        State = EStateType_Projectile_FairyAttack.Idle;
    }
}
