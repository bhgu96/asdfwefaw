﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace Object
{
    // Inspector에 노출시킬 정보
    [Serializable]
    public class ObjectInfo
    {
        public GameObject Prefab;
        public int CountOfObjects;
    }

    /// <summary>
    /// 등록된 프리팹을 개수만큼 생성해서 비활성화된 오브젝트를 하나씩 반환해주는 클래스
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <typeparam name="E"></typeparam>
    public class Pool<E, T> : MonoBehaviour where T : IPoolable<E>
    {
        // 종류별로 Object가 저장되는 클래스
        private class Objects
        {
            private T[] mObjects;
            private int mIndex;

            public Objects(GameObject prefab, int countOfObjects, Transform parent)
            {
                mObjects = new T[countOfObjects];

                for (int i = 0; i < countOfObjects; i++)
                {
                    GameObject instObject = Instantiate(prefab, parent);
                    instObject.SetActive(false);
                    mObjects[i] = instObject.GetComponent<T>();
                }
            }

            /// <summary>
            /// Active 되지 않은 다음 Object를 반환하는 메소드
            /// </summary>
            /// <returns></returns>
            public T Get()
            {
                T nextObject = default;

                for (int i = 0; i < mObjects.Length; i++)
                {
                    int curIndex = mIndex++;
                    mIndex = (mIndex < mObjects.Length ? mIndex : 0);
                    if (mObjects[curIndex].IsPooled)
                    {
                        nextObject = mObjects[curIndex];
                        break;
                    }
                }
                return nextObject;
            }
        }

        private static Dictionary<E, Objects> mPoolDict_Type;
        private static Dictionary<int, Objects> mPoolDict_Int;

        [SerializeField]
        private bool mIsStatic;

        [SerializeField]
        [Tooltip("생성할 오브젝트")]
        private ObjectInfo[] mObjectInfo;

        public int Count { get { return mObjectInfo.Length; } }

        /// <summary>
        /// 오브젝트 풀에서 비활성화된 오브젝트 하나를 반환하는 메소드
        /// 비활성화된 오브젝트가 존재하지 않는다면 null 반환
        /// </summary>
        /// <param name="objectType"></param>
        /// <returns></returns>
        public static T Get(E objectType)
        {
            if (mPoolDict_Type.ContainsKey(objectType))
            {
                return mPoolDict_Type[objectType].Get();
            }
            return default;
        }

        public static T Get(int index)
        {
            if(mPoolDict_Int.ContainsKey(index))
            {
                return mPoolDict_Int[index].Get();
            }
            return default;
        }

        public virtual void Awake()
        {
            if (mIsStatic)
            {
                DontDestroyOnLoad(gameObject);
            }

            mPoolDict_Type = new Dictionary<E, Objects>();
            mPoolDict_Int = new Dictionary<int, Objects>();

            for (int i = 0; i < mObjectInfo.Length; i++)
            {
                T prefabInfo = mObjectInfo[i].Prefab.GetComponent<T>();
                Objects objects = new Objects(mObjectInfo[i].Prefab, mObjectInfo[i].CountOfObjects, transform);
                
                mPoolDict_Type.Add(prefabInfo.Type, objects);
                mPoolDict_Int.Add(i, objects);
            }
        }
    }
}