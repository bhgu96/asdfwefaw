﻿using UnityEngine;
using System;
using System.Collections.Generic;

namespace Object
{
    public class Frame<TType, EType>
        where TType : IMountable<EType>
        where EType : Enum
    {
        public int Capacity { get { return mMountables.Length; } }
        private TType[] mMountables;
        private EqualityComparer<EType> mComparer;

        public Frame(int capacity)
        {
            mMountables = new TType[capacity];
            mComparer = EqualityComparer<EType>.Default;
        }

        /// <summary>
        /// 전달된 객체가 해당 위치에 장착 가능 여부를 반환하는 메소드
        /// </summary>
        /// <param name="mountable">장착할 객체</param>
        /// <param name="index">장착 위치</param>
        /// <returns></returns>
        public bool CanMount(TType mountable, int index)
        {
            // 장착할 위치가 범위를 벗어나지 않는 경우
            if (index < mMountables.Length)
            {
                // 장착 해제하는 경우
                if (mountable == null)
                {
                    return true;
                }
                // 장착이 가능한 경우
                if (mountable.CanBeMounted)
                {
                    // 모든 장착 가능한 틀에 대해
                    for (int i = 0; i < mMountables.Length; i++)
                    {
                        // 중복된 장착이 존재한다면
                        // 착용 불가능
                        if(i != index && mMountables[i] != null
                            && mComparer.Equals(mMountables[i].Type, mountable.Type))
                        {
                            return false;
                        }
                    }
                    return true;
                }
            }
            return false;
        }

        /// <summary>
        /// 전달된 객체를 전달된 위치에 장착하는 메소드
        /// </summary>
        /// <param name="mountable">장착할 객체</param>
        /// <param name="index">장착할 위치</param>
        /// <returns></returns>
        public bool Mount(TType mountable, int index)
        {
            // 해당 객체가 해당 위치에 장착 가능하다면
            if(CanMount(mountable, index))
            {
                // 기존에 장착된 객체가 있다면 해당 객체를 장착 해제한다.
                // 만약 장착 해제를 실패할 경우
                if(mMountables[index] != null
                    && !mMountables[index].BeUnMounted())
                {
                    // 장착 해제 실패를 반환
                    return false;
                }
                OnUnMount(index);
                mMountables[index] = default;

                // 기존에 장착된 객체가 없거나 해당 객체를 장착 해제한 경우
                // 전달된 객체를 장착한다.
                // 만약 장착에 실패할 경우
                if (mountable != null && !mountable.BeMounted())
                {
                    // 장착 해제 실패를 반환한다.
                    return false;
                }
                // 장착에 성공한 경우
                mMountables[index] = mountable;
                OnMount(index);
                return true;
            }
            // 전달된 객체가 해당 위치에 장착 불가능한 경우
            return false;
        }

        /// <summary>
        /// 해당 위치의 객체의 장착 해제 가능 여부를 반환하는 메소드
        /// </summary>
        /// <param name="index">장착된 객체의 위치</param>
        /// <returns></returns>
        public bool CanUnMount(int index)
        {
            return index < mMountables.Length
                && mMountables[index] != null
                && mMountables[index].CanBeUnMounted;
        }

        /// <summary>
        /// 전달된 위치의 객체를 장착 해제하는 메소드
        /// </summary>
        /// <param name="index">장착 해제할 객체의 위치</param>
        /// <returns></returns>
        public bool UnMount(int index)
        {
            // 해당 위치에 존재하는 객체가 장착 해제 가능하고 
            // 장착 해제 성공한 경우
            if(CanUnMount(index) && mMountables[index].BeUnMounted())
            {
                OnUnMount(index);
                mMountables[index] = default;
                return true;
            }
            return false;
        }


        /// <summary>
        /// 장착중인 객체를 반환하는 메소드
        /// </summary>
        /// <param name="index">장착된 객체의 위치</param>
        /// <returns></returns>
        public TType Peek(int index)
        {
            if(index < mMountables.Length)
            {
                return mMountables[index];
            }
            return default;
        }

        protected virtual void OnUnMount(int index) { }
        protected virtual void OnMount(int index) { }
    }
}