﻿using System;
using UnityEngine;
using System.Collections.Generic;


namespace Object
{
    /// <summary>
    /// 자식 오브젝트를 태그를 지정해서 관리하고 반환해주는 클래스
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <typeparam name="E"></typeparam>
    public class Handler<E, T> : MonoBehaviour where T : IHandleable<E> where E : Enum
    {
        private Dictionary<E, T> mObjectDict;

        public int Count
        {
            get { return mObjectDict.Count; }
        }

        public T Get(E objectType)
        {
            if (mObjectDict.ContainsKey(objectType))
            {
                return mObjectDict[objectType];
            }
            return default;
        }

        public Dictionary<E, T>.Enumerator GetEnumerator()
        {
            return mObjectDict.GetEnumerator();
        }

        protected virtual void Awake()
        {
            mObjectDict = new Dictionary<E, T>();
            T[] entities = GetComponentsInChildren<T>(true);

            for(int i=0; i<entities.Length; i++)
            {
                mObjectDict.Add(entities[i].Type, entities[i]);
            }
        }
    }
}