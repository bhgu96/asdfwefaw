﻿using System;
using UnityEngine;

namespace Object
{
    public interface IHandleable<EType>
    {
        EType Type { get; }
    }

    public interface IPoolable<EType> : IHandleable<EType>
    {
        bool IsPooled { get; }
        GameObject gameObject { get; }
    }

    public interface IMountable<EType> : IHandleable<EType> where EType : Enum
    {
        bool IsMounted { get; }

        bool CanBeMounted { get; }
        bool CanBeUnMounted { get; }

        bool BeMounted();
        bool BeUnMounted( );
    }
}