﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class ShaderPropertyM
{
    public static int _MainTexID = Shader.PropertyToID("_MainTex");

    // clip shader
    public static int clipAmount = Shader.PropertyToID("clipAmount");
    public static int maxClipAmount = Shader.PropertyToID("maxClipAmount");

    // bloom shader
    public static int _BloomTex = Shader.PropertyToID("_BloomTex");
}
