﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Video;
using UnityEngine.SceneManagement;

public class IntroVideoPlayer : MonoBehaviour
{
    //[SerializeField]
    //private string mNextScene;
    [SerializeField]
    private EGameScene mNextScene;

    private bool mIsPlaying = false;
    private VideoPlayer vp;

    private void Awake()
    {
        vp = GetComponent<VideoPlayer>();
        vp.Play();
        CustomCursor.InvisibleCursor();
    }

    private void Update()
    {
        if(Input.anyKeyDown)
        {
            CustomCursor.VisibleCursor();
            LoadingController.LoadScene(mNextScene.ToString());
        }

        //if(!mIsPlaying)
        //{
        //    mIsPlaying = vp.isPlaying;
        //}
        //else if(Input.anyKeyDown || !vp.isPlaying)
        //{
        //    CustomCursor.VisibleCursor();
        //    LoadingController.LoadScene(mNextScene);
        //}
    }
}
